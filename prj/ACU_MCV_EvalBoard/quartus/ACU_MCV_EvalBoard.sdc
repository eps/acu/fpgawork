############################################
#       ACU_MonitoringModule SDC file      #
############################################

# Clock Constraints

  # Base Clock
  create_clock -name InputPinClock50MHz -period 20.000 [get_ports {Board50MHz_CLK}]
  # Virtual Clock (used to evaluate the I/O timing uncertainties)
  create_clock -name VirtualClockIn -period 20.000
  
  # Generated Clock
  # System clock
  create_generated_clock -name SystemClock100MHz -source [get_ports {Board50MHz_CLK}] -divide_by 1 -multiply_by 2 [get_pins {i_acu_PLL_100_10MHz_CyclonV|acu_pll_100_10mhz_cyclonv_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT|clkout}]
  #create_generated_clock -name SystemClock100MHz -source [get_ports {Board50MHz_CLK}] -divide_by 1 -multiply_by 2 [get_pins {inst_PLL_100MHz|altpll_component|auto_generated|pll1|clk[0]}]
  create_generated_clock -name SerialComClock -source [get_pins {i_acu_PLL_100_10MHz_CyclonV|acu_pll_100_10mhz_cyclonv_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT|clkout}] -divide_by 5 -multiply_by 6 -duty_cycle 50 -phase 0 [get_pins {i_ACU_PLL_120_cV|acu_pll_120_cv_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT|clkout}]
  
  create_generated_clock -name Clock_bbpd -source     [get_pins {i_acu_PLL_100_10MHz_CyclonV|acu_pll_100_10mhz_cyclonv_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT|clkout}] -divide_by 1 -multiply_by 1 -duty_cycle 50 -phase 0    [get_pins {inst7|acu_pll_seialcom_cv_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]}]
  create_generated_clock -name Clock_bbpd90 -source   [get_pins {i_acu_PLL_100_10MHz_CyclonV|acu_pll_100_10mhz_cyclonv_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT|clkout}] -divide_by 1 -multiply_by 1 -duty_cycle 50 -phase 90  [get_pins {inst7|acu_pll_seialcom_cv_inst|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]}]


 
  set_clock_groups -asynchronous -group {SystemClock100MHz} -group {SerialComClock} -group {Clock_bbpd} -group {Clock_bbpd90}
  
  # Clock uncertainty (change this constraint with SDC original constraints (set_clock_latency and set_clock_uncertainty))
  derive_clock_uncertainty

 
 # Multicycle paths
  
  set_multicycle_path -from [get_pins {inst_FSP12|sOutputToPeripheral[*]|q}]  -to [*] -setup -end 4
  set_multicycle_path -from [get_pins {inst_FSP60|sOutputToPeripheral[*]|q}]  -to [*] -setup -end 4
  set_multicycle_path -from [get_pins {inst_FSP61|sOutputToPeripheral[*]|q}]  -to [*] -setup -end 4
  set_multicycle_path -from [get_pins {inst_FSP63|sOutputToPeripheral[*]|q}]  -to [*] -setup -end 4
  set_multicycle_path -from [get_pins {inst_FSP65|sOutputToPeripheral[*]|q}]  -to [*] -setup -end 4
  set_multicycle_path -from [get_pins {inst_FSP75|sOutputToPeripheral[*]|q}]  -to [*] -setup -end 4
