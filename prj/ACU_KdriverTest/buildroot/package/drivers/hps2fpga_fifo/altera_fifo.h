#pragma once

#include <linux/types.h>

typedef union
{
    uint32_t reg;
    struct
    {
        uint8_t full : 1;
        uint8_t empty : 1;
        uint8_t almost_full : 1;
        uint8_t almost_empty : 1;
        uint8_t overflow : 1;
        uint8_t underflow : 1;
    };
} altera_fifo_status_t;

typedef struct
{
    uint32_t fill_level;
    altera_fifo_status_t status;           /**< immediate status*/
    altera_fifo_status_t event;            /**< Stores pending interrupts, cleared by writing 1 to it*/
    altera_fifo_status_t interrupt_enable; /**< Masks which event bits (set high) raise an interrupt*/
    uint32_t almost_full_threshold;
    uint32_t almost_empty_threshold;
} altera_fifo_csr_t;