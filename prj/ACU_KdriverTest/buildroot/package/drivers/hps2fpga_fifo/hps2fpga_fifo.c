#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <linux/kfifo.h>

#include "hps2fpga_fifo.h"

#define DEVNAME "hps2fpga_fifo"
MODULE_DESCRIPTION(DEVNAME" driver");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Davide Rodomonti, GSI");
MODULE_ALIAS("platform:"DEVNAME);

#define ALMOSTFULL 4
#define EMPTY 2
#define W1C 34

//#define FIFO_LOC 4096
//#define FIFO_LOC 8192
//#define FIFO_LOC 3
#define THRS_ALMOSTFULL 1024
#define THRS_ALMOSTEMPTY 10
#define FIFO_SIZE (16384 * 32) // with write action @1MSps, the FIFO will be full after 4,096 ms
DECLARE_KFIFO(samples_fifo, __u32, FIFO_SIZE);

// NOTE: the device structure is defined in the header file.

// misc file operation functions
int hps2fpga_fifo_open(struct inode *nodep, struct file *filep);
int hps2fpga_fifo_release(struct inode *nodep, struct file *filep);
ssize_t hps2fpga_fifo_write(struct file *, const char __user *, size_t, loff_t *);

// driver functions
static int hps2fpga_fifo_probe(struct platform_device* pdev);
static int hps2fpga_fifo_remove(struct platform_device *pdev);

// driver compatibility setting
static const struct of_device_id hps2fpga_fifo_id[] = {
	{.compatible = "gsi-eps,fifo_hps2fpga"}, {}
};

// Inform kernel about the devices this driver supports
MODULE_DEVICE_TABLE(of, hps2fpga_fifo_id);

// Attribute section

ssize_t attr_fill_level_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);

	__u32 value = (__u32) ioread32(&(mydev->fifo_csr->fill_level));
	len = sprintf(buf, "%u\n", value);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

DEVICE_ATTR(fill_level, 0664, attr_fill_level_show, NULL);

ssize_t attr_status_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);

	altera_fifo_status_t value = (altera_fifo_status_t) ioread32(& mydev->fifo_csr->status);
	len = sprintf(buf, "%u\n", value.reg);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

DEVICE_ATTR(status, 0664, attr_status_show, NULL);

ssize_t attr_event_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);

	altera_fifo_status_t value = (altera_fifo_status_t) ioread32(&(mydev->fifo_csr-> event));
	len = sprintf(buf, "%u\n", value.reg);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

ssize_t attr_event_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);
	__u32 value;

	// convert user input
	if((strnstr(buf, "0x", 2) != NULL) && (kstrtouint(buf, 16, &value) == 0)){ // if input is hex then interpret value as unsigned integer
		iowrite32(value, &(mydev->fifo_csr->event));
	}else if(kstrtoint(buf, 10, &value) == 0){
		iowrite32(value, &(mydev->fifo_csr->event));
	}else{
		pr_err("%s: kstrtouint failed\n", __PRETTY_FUNCTION__);
		return -EINVAL;
	}
	return count;
}

DEVICE_ATTR(event, 0664, attr_event_show, attr_event_store);

ssize_t attr_interrupt_enable_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);

	altera_fifo_status_t value = (altera_fifo_status_t) ioread32(&(mydev->fifo_csr->interrupt_enable));
	len = sprintf(buf, "%u\n", value.reg);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

ssize_t attr_interrupt_enable_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);
	__u32 value;

	// convert user input
	if((strnstr(buf, "0x", 2) != NULL) && (kstrtouint(buf, 16, &value) == 0)){ // if input is hex then interpret value as unsigned integer
		iowrite32(value, &(mydev->fifo_csr->interrupt_enable));
	}else if(kstrtoint(buf, 10, &value) == 0){
		iowrite32(value, &(mydev->fifo_csr->interrupt_enable));
	}else{
		pr_err("%s: kstrtouint failed\n", __PRETTY_FUNCTION__);
		return -EINVAL;
	}
	return count;
}

DEVICE_ATTR(interrupt_enable, 0664, attr_interrupt_enable_show, attr_interrupt_enable_store);

ssize_t attr_almost_full_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);

	__u32 value = (__u32) ioread32(&(mydev->fifo_csr->almost_full_threshold));
	len = sprintf(buf, "%u\n", value);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

ssize_t attr_almost_full_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);
	__u32 value;

	// convert user input
	if((strnstr(buf, "0x", 2) != NULL) && (kstrtouint(buf, 16, &value) == 0)){ // if input is hex then interpret value as unsigned integer
		iowrite32(value, &(mydev->fifo_csr->almost_full_threshold));
	}else if(kstrtoint(buf, 10, &value) == 0){
		iowrite32(value, &(mydev->fifo_csr->almost_full_threshold));
	}else{
		pr_err("%s: kstrtouint failed\n", __PRETTY_FUNCTION__);
		return -EINVAL;
	}
	return count;
}

DEVICE_ATTR(almost_full, 0664, attr_almost_full_show, attr_almost_full_store);

ssize_t attr_almost_empty_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);

	__u32 value = (__u32) ioread32(&(mydev->fifo_csr->almost_empty_threshold));
	len = sprintf(buf, "%u\n", value);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

ssize_t attr_almost_empty_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);
	__u32 value;

	// convert user input
	if((strnstr(buf, "0x", 2) != NULL) && (kstrtouint(buf, 16, &value) == 0)){ // if input is hex then interpret value as unsigned integer
		iowrite32(value, &(mydev->fifo_csr->almost_empty_threshold) );
	}else if(kstrtoint(buf, 10, &value) == 0){
		iowrite32(value, &(mydev->fifo_csr->almost_empty_threshold));
	}else{
		pr_err("%s: kstrtouint failed\n", __PRETTY_FUNCTION__);
		return -EINVAL;
	}
	return count;
}

DEVICE_ATTR(almost_empty, 0664, attr_almost_empty_show, attr_almost_empty_store);

// kfifo_avail gives you ne number of 32b locations available.
// Maybe in the KFIFO_SIZE it is enough to specify the location number and automatically they will be 32b each.
ssize_t attr_kfifo_avail_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	//struct hps2fpga_fifo_dev *mydev = dev_get_drvdata(dev);

	//altera_fifo_status_t value = (altera_fifo_status_t) ioread32(& mydev->fifo_csr->status);
	size_t kfifoAvail=kfifo_avail(&samples_fifo);
	len = sprintf(buf, "%u\n", kfifoAvail);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

DEVICE_ATTR(kfifo_avail, 0664, attr_kfifo_avail_show, NULL);

// kfifo_size gives you back the overall number of locations (used and not used)
ssize_t attr_kfifo_size_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	size_t kfifoSize=kfifo_size(&samples_fifo);
	len = sprintf(buf, "%u\n", kfifoSize);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

DEVICE_ATTR(kfifo_size, 0664, attr_kfifo_size_show, NULL);

// kfifo_len gives you back the number of used locations.
ssize_t attr_kfifo_len_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	size_t kfifoLen=kfifo_len(&samples_fifo);
	len = sprintf(buf, "%u\n", kfifoLen);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

DEVICE_ATTR(kfifo_len, 0664, attr_kfifo_len_show, NULL);

ssize_t attr_kfifo_get_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len=0;	
	uint32_t tmp;	
	int ret=kfifo_get(&samples_fifo,&tmp);
	if (ret==0) {
	  pr_info("Kfifo empty!\n");
	} else {
	  //pr_info("Read from kfifo %d bits\n",ret);
	  len = sprintf(buf, "%u\n", tmp);
	  if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	}
        return len;
}

ssize_t attr_kfifo_put_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	__u32 value;

	// convert user input
	if((strnstr(buf, "0x", 2) != NULL) && (kstrtouint(buf, 16, &value) == 0)){ // if input is hex then interpret value as unsigned integer
                kfifo_put(&samples_fifo, value);
	}else if(kstrtoint(buf, 10, &value) == 0){
                kfifo_put(&samples_fifo, value);
	}else{
		pr_err("%s: kstrtouint failed\n", __PRETTY_FUNCTION__);
		return -EINVAL;
	}
	return count;
}

DEVICE_ATTR(kfifo_element, 0664, attr_kfifo_get_show, attr_kfifo_put_store);

struct attribute *dev_value_attrs[] = {
	&dev_attr_fill_level.attr, 
	&dev_attr_status.attr, 
	&dev_attr_event.attr, 
	&dev_attr_interrupt_enable.attr, 
	&dev_attr_almost_full.attr, 
	&dev_attr_almost_empty.attr, 
        &dev_attr_kfifo_avail.attr,
        &dev_attr_kfifo_size.attr, 
        &dev_attr_kfifo_len.attr, 
        &dev_attr_kfifo_element.attr, 
	NULL
};

struct attribute_group dev_attr_group = {
	.attrs = dev_value_attrs
};

const struct attribute_group *dev_attr_groups[] = {
	&dev_attr_group,
	NULL
};

// Platform device structure definition
static struct platform_driver hps2fpga_fifo_driver = {
	.driver = {
		.name = DEVNAME,
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(hps2fpga_fifo_id),
		.dev_groups = dev_attr_groups
	},
	.probe = hps2fpga_fifo_probe,
	.remove = hps2fpga_fifo_remove
};

// misc file operation structure
struct file_operations fops = {
	.owner = THIS_MODULE,
	.write = hps2fpga_fifo_write,
	.open = hps2fpga_fifo_open,
	.release = hps2fpga_fifo_release
};

int hps2fpga_fifo_open(struct inode *nodep, struct file *file){
	pr_debug("%s\n", __PRETTY_FUNCTION__);
	return 0;
}
int hps2fpga_fifo_release(struct inode *nodep, struct file *file){
	uint32_t tmp;
	int ret;
	
	pr_debug("%s\n", __PRETTY_FUNCTION__);
	// Maybe there is a better way to do that
        size_t kfifoLen=kfifo_len(&samples_fifo);
	
	pr_info("%s: Flush %d samples from the kfifo \n", __PRETTY_FUNCTION__, kfifoLen); 
	for (size_t i=0; i<kfifoLen;i++){
	  ret=kfifo_get(&samples_fifo,&tmp);
        }

	pr_info("%s: Check if the kfifo is empty..", __PRETTY_FUNCTION__);		
	if(!kfifo_is_empty(&samples_fifo)) {
          pr_err("kfifo not empty after flushing.\n");
        } else {
          pr_info("Empty! Have a nice day!\n");		
        }
	return 0;
}

ssize_t hps2fpga_fifo_write(struct file * file, const char __user *buffer, size_t count, loff_t *position) {
	int status = 0;
	
	struct miscdevice *misc = file->private_data;
        struct hps2fpga_fifo_dev *mydev = container_of(misc, struct hps2fpga_fifo_dev, miscdev);
        
        uint32_t tmp;	
	int ret;
	pr_debug("%s\n", __PRETTY_FUNCTION__);
	__u32 bytes_copied;

	pr_debug("%s: count= %d .\n", __PRETTY_FUNCTION__,count);	
	status = kfifo_from_user(&samples_fifo, buffer, count, &bytes_copied);
	pr_debug("%s: Pushed in %u Byte into the kfifo.\n", __PRETTY_FUNCTION__,bytes_copied);	
	size_t kfifoLen=kfifo_len(&samples_fifo);
	if(status == 0){
		// push the value from the kfifo out and to the hps2fpga fifo in
	        pr_debug("%s: Move %u Byte from the kfifo to the hps2fpga fifo.\n", __PRETTY_FUNCTION__,bytes_copied);	
	        for (size_t i=0; i<kfifoLen;i++){
			// read from KFIFO
			ret=kfifo_get(&samples_fifo,&tmp);
				  
			// write to hps2fpga fifo
			iowrite32(tmp, &(mydev->fifo_data)[0]); 
		}
	        
	        pr_debug("%s: End misc write.\n", __PRETTY_FUNCTION__);	
		return bytes_copied;
		
	}else {
          	pr_err("%s: Error reading the kfifo. Status=%d\n", __PRETTY_FUNCTION__,status);
		return status;
	}
}

static int hps2fpga_fifo_probe(struct platform_device* pdev){
	int status;
	struct hps2fpga_fifo_dev *dev = NULL;
	struct resource *r = NULL;

	// Anything that needs to be allocated should be allocated via the devm_* functions
	// devm_* functions create "managed resources" which are freed/released automatically on release/exit function
	//pr_info("%s: Entering to the probe function\n", __PRETTY_FUNCTION__);
	// Allocate space to store the device structure
	dev = devm_kzalloc(&pdev->dev, sizeof(struct hps2fpga_fifo_dev), GFP_KERNEL);
	if(dev != NULL){
		// retrieve resource information from the device tree by name: memory base address and length
		// fifo_csr
		r = platform_get_resource_byname(pdev, IORESOURCE_MEM, "fifo-csr");
		if (!r) {
			pr_err("%s: Failed to get fifo-csr resources\n", __PRETTY_FUNCTION__);
			return -EINVAL;	
		}
		dev->fifo_csr = devm_ioremap_resource(&pdev->dev, r);
		if (!dev->fifo_csr) {
			pr_err("%s: Failed to remap fifo-csr config space\n", __PRETTY_FUNCTION__);
			return -ENOMEM;
		}
		
		// fifo_data
		r = platform_get_resource_byname(pdev, IORESOURCE_MEM, "fifo-data");
		if (!r) {
			pr_err("%s: Failed to get fifo-data resources\n", __PRETTY_FUNCTION__);
			return -EINVAL;	
		}
		dev->fifo_data = devm_ioremap_resource(&pdev->dev, r);
		if (!dev->fifo_data) {
			pr_err("%s: Failed to remap fifo-data config space\n", __PRETTY_FUNCTION__);
			return -ENOMEM;
		}
		
		// retrieve the interrupt number from the device tree 				   
		dev->irqnum = platform_get_irq(pdev, 0);
		if (dev->irqnum < 0) {
		  pr_err("%s: platform_get_irq failed  = %d)\n", __PRETTY_FUNCTION__, dev->irqnum);
		  return -ENXIO;		
		} 
 		
		//Populate the misc structure items 
		dev->miscdev.minor = MISC_DYNAMIC_MINOR;
		dev->miscdev.name = DEVNAME;
		dev->miscdev.fops = &fops;

		// create a misc device
		status = misc_register(&dev->miscdev);
		if(status == 0) {
			platform_set_drvdata(pdev, (void*)dev);
			INIT_KFIFO(samples_fifo);
			
			if(kfifo_initialized(&samples_fifo)){
				pr_info("%s: probe successful\n", __PRETTY_FUNCTION__);
		   
			}else{
				pr_err("%s: fifo not initialized.\n", __PRETTY_FUNCTION__);
			}			
		}else {
			pr_err("%s: misc_register failed\n", __PRETTY_FUNCTION__);
		}
		return status;
	}else{
		pr_err("%s: devm_kzalloc failed\n", __PRETTY_FUNCTION__);
		return -EBUSY;
	}
}

static int hps2fpga_fifo_remove(struct platform_device *pdev){
	// no need to free any resources manually here
	// it's done by the kernel (managed resources / devm_*)
	struct hps2fpga_fifo_dev *dev = (struct hps2fpga_fifo_dev*) platform_get_drvdata(pdev);
	misc_deregister(&dev->miscdev);

	return 0;
}

module_platform_driver(hps2fpga_fifo_driver);

