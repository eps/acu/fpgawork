////////////////////////////////////////////

#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>

#include <linux/kernel.h>    /* printk() */
#include <linux/sched.h>
#include <linux/slab.h>        /* kmalloc() */
#include <linux/fs.h>        /* everything… */
#include <linux/errno.h>    /* error codes */
#include <linux/types.h>    /* size_t */
#include <linux/proc_fs.h>
#include <linux/fcntl.h>    /* O_ACCMODE */
#include <linux/seq_file.h>
#include <linux/cdev.h>
#include <linux/device.h>   /* sysfs/class interface             */
#include <linux/kdev_t.h>   /* sysfs/class interface             */
#include <linux/kfifo.h>

#include <asm/system.h>        /* cli(), *_flags */
#include <asm/uaccess.h>    /* copy_*_user */

#include <linux/ioctl.h> /* needed for the _IOW etc stuff used later */

//#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19))
//    #include <asm/io.h>
//    #include <asm/semaphore.h>
//#else
#include <linux/io.h>
#include <linux/semaphore.h>
//#endif

#include <linux/serial.h>
#include <linux/serialP.h>
#include <linux/serial_reg.h>
#include <asm/serial.h>

#define ser_dev 0x3f8
#define BUFFER_SIZE 2048
#define DEVICE_NAME “net9”

#define UART_IER_DISABLE    0x00    /*disable interrupts, is missing from serial-reg.h */
#define UART_FCR_DISABLE_FIFO    0x00    /*disable fifo */
#define LCR_ADDR  (UART_LCR_WLEN8 | UART_LCR_PARITY | UART_LCR_SPAR)
#define LCR_DATA  (UART_LCR_WLEN8 | UART_LCR_PARITY | UART_LCR_SPAR | UART_LCR_EPAR)
#define UART_FIFO_SETUP (UART_FCR_ENABLE_FIFO | UART_FCR_CLEAR_RCVR | UART_FCR_CLEAR_XMIT | UART_FCR_TRIGGER_1)

#define uint32    __u32
#define uint16    __u16
#define uint8    __u8
#define sint32    __s32
#define sint16    __s16
#define sint8    __s8
#define byte     __u8

/* Macros */
/*————————————————————————*/
#define inportb(port) (uint)(inb(ser_dev+port))
#define outportb(port,value) outb(value,ser_dev+port)
#define setbit(port,bits) outportb(port,(inportb(port)|bits))
#define clrbit(port,bits) outportb(port,inportb(port) & ~bits)

#define send_byte(b) {outportb(UART_TX,b);};
#define set_addr_bit() {setbit(UART_LCR,LCR_ADDR);};
#define init_mode_addr() {outportb(UART_LCR,LCR_ADDR);};
#define init_mode_data() {outportb(UART_LCR,LCR_DATA);};
#define is_tx_line_empty() (sint16)((inportb(UART_LSR)&UART_LSR_TEMT)==UART_LSR_TEMT)? 1 : 0
#define enable_rx_interrupt() {setbit(UART_IER,UART_IER_RDI);};
#define disable_rx_interrupt() {clrbit(UART_IER,UART_IER_RDI);};
#define enable_tx_interrupt() {setbit(UART_IER,UART_IER_THRI);};
#define diable_tx_interrupt() {clrbit(UART_IER,UART_IER_THRI);};
#define enable_rx_handshake() {outportb(UART_IER,UART_IER_RDI);    setbit(UART_MCR,UART_MCR_OUT2);};
#define request_to_send() {setbit(UART_MCR,UART_MCR_RTS);};
#define clr_request_to_send() {clrbit(UART_MCR,UART_MCR_RTS);};
#define diable_serial_interrupt() {outportb(UART_IER, 0); clr_request_to_send();};
/*————————————————————————*/

struct rs485_dev {

	wait_queue_head_t inq,outq;
	struct kfifo rx_fifo;//receive buffer
	struct kfifo tx_fifo;//transmission buffer

	struct semaphore sem;     // when will it be used?
	struct cdev my_dev;      // Char device structure
};

struct rs485_dev * rs485_device;
static struct class * rs485_device_class = 0;

static unsigned int irqnum;
static int irqnumber;

static int rs485_major;

MODULE_AUTHOR(“Lewis Liu, yyiu002@hotmail.com”);
MODULE_LICENSE(“Dual BSD/GPL”);

static void rs485_exit(void);

static unsigned rs485_detect_uart_irq (void){

int irq;
unsigned long irqs;
unsigned char save_mcr, save_ier;

probe_irq_off (probe_irq_on ());
save_mcr = inportb (UART_MCR);
save_ier = inportb (UART_IER);
outportb (UART_MCR, (UART_MCR_DTR | UART_MCR_OUT1 | UART_MCR_OUT2));
irqs = probe_irq_on (); //get a mask of possible interrupts
outportb (UART_MCR, 0);
udelay (10);
//generate interrupt!
outportb (UART_MCR, UART_MCR_DTR | UART_MCR_RTS | UART_MCR_OUT2);
outportb (UART_IER, 0x0f);
(void) inportb (UART_LSR);
(void) inportb (UART_RX);
(void) inportb (UART_IIR);
(void) inportb (UART_MSR);
udelay (20);
irq = probe_irq_off (irqs);
outportb (UART_MCR, save_mcr);
outportb (UART_IER, save_ier);
//irq<0 then interrupt maybe shared, can’t use, has to be bigger than 0
return (irq > 0) ? irq : 0;
}

/*
* Open and close
*/

int rs485_open(struct inode *inode, struct file *filp)
{
	struct rs485_dev *dev; /* device information */

	printk(KERN_ALERT “net9 about to open\n”);

	dev = container_of(inode->i_cdev, struct rs485_dev, my_dev);
	filp->private_data = dev; /* for other methods */

	/* now trim to 0 the length of the device if open was write-only */
	if ( (filp->f_flags & O_ACCMODE) == O_WRONLY) {
		if (down_interruptible(&dev->sem))
			return -ERESTARTSYS;
		//scull_trim(dev); /* ignore errors */
		up(&dev->sem);
	}
	return 0;          /* success */
}

int rs485_release(struct inode *inode, struct file *filp)
{
	return 0;
}

ssize_t rs485_read(struct file *filp, char __user *buf, size_t count,loff_t *f_pos)
{
	ssize_t ret = 0;
	unsigned char c;
	struct rs485_dev *dev = filp->private_data;

	if (down_interruptible(&dev->sem))
		return -ERESTARTSYS;

	//if ( ( kfifo_len ( &dev->rx_fifo ) == 0 ) &&
	//        ( filp->f_flags & O_NONBLOCK ) )
	{
	//    ret= -EAGAIN;
	//    goto out;
	}

	ret  = wait_event_interruptible ( dev->inq, kfifo_len ( &dev->rx_fifo ) != 0 );
	if ( ret ){
		goto out;
	}

	while ( ret < count &&	( kfifo_out ( &dev->rx_fifo, &c, sizeof ( c ) ) == sizeof ( c ) ) )
	{
		if ( put_user ( c, buf++ ) ){
			ret= -EFAULT;
			goto out;
		}
		ret++;
	}

	out:
		up(&dev->sem);

	return ret;
}

ssize_t rs485_write(struct file *filp, const char __user *buf, size_t count,loff_t *f_pos)
{
	//struct rs485_dev *dev = filp->private_data;
	ssize_t retval = -ENOMEM; /* value used in “goto out” statements */
	return retval;
}

/*
* The ioctl() implementation
*/

int rs485_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{
	return -ENOTTY;
}

loff_t rs485_llseek(struct file *filp, loff_t off, int whence)
{
	//struct rs485_dev *dev = filp->private_data;
	loff_t newpos;

	switch(whence) {
		case 0: /* SEEK_SET */
			newpos =0;// off;
			break;

		case 1: /* SEEK_CUR */
			newpos = 0;//filp->f_pos + off;
			break;

		case 2: /* SEEK_END */
			newpos =0;// dev->size + off;
			break;

		default: /* can’t happen */
			return -EINVAL;
	}
	
	if (newpos < 0) return -EINVAL;
	filp->f_pos = newpos;
	return newpos;
}

static u8 sending;

static irqreturn_t rs485_interrupt (int irq, void *dev_id, struct pt_regs *regs)
{
	static u8 iir;
	static u8 rec_char;
	static u8 rec_lstatus;
	char buffer[5];

	iir = inportb (UART_IIR);//0xC (line status change);0x4 (received data)
	
	while ((iir & 0x01) == 0) { //interrupt pending?
		rec_lstatus = inportb (UART_LSR); //0x4 parity error;0x1 data;
		rec_char = inportb (UART_RX);
		iir = inportb (UART_IIR);
		
		switch(sending){
			case 0:
				if ((rec_lstatus & 0x04) == 0x04) //data is address
				{
					sprintf(buffer, “/%02x “,rec_char);
					kfifo_in ( &rs485_device->rx_fifo, buffer,4);
				} else {
					sprintf(buffer, “%02x “,rec_char);
					kfifo_in ( &rs485_device->rx_fifo, buffer,3);
				}
				wake_up_interruptible ( &rs485_device->inq );
				break;
			case 1:
				break;
			case 2:
				break;
			default:
				break;
		}

	}
	return ( IRQ_HANDLED );
}

struct file_operations rs485_fops = {
.owner =    THIS_MODULE,
.llseek =   rs485_llseek,
.read =     rs485_read,
.write =    rs485_write,
.ioctl =    rs485_ioctl,
.open =     rs485_open,
.release =  rs485_release,
};

/*
* Set up the char_dev structure for this device.
*/
static void rs485_setup_cdev(struct rs485_dev *dev)
{
	int err, devno = MKDEV(rs485_major, 0);

	cdev_init(&dev->my_dev, &rs485_fops);

	dev->my_dev.owner = THIS_MODULE;
	dev->my_dev.ops = &rs485_fops;
	err = cdev_add (&dev->my_dev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE “Error %d adding net9”, err);
}

void rs485_create_dev_node(struct rs485_dev *dev)
{
	dev_t devno = MKDEV(rs485_major, 0);
	/* Dynamic /dev creation
	*
	* This creates entries in /dev
	*/
	rs485_device_class = class_create(THIS_MODULE, DEVICE_NAME);

	if (rs485_device_class == NULL) {
		printk(KERN_NOTICE “create /dev/net9 failed”);
		rs485_exit();
		//return -ENODEV;
	}

	/* class creation */
	device_create(rs485_device_class, NULL, devno, NULL, DEVICE_NAME);

}

static void rs485_exit(void)
{
	dev_t devno = MKDEV(rs485_major, 0);

	if (rs485_device_class){
		device_destroy(rs485_device_class,devno);
		class_unregister(rs485_device_class);
		class_destroy(rs485_device_class);
		printk(KERN_ALERT “net9 class removed”);
	}
	/* cleanup_module is never called if registering failed */

	if (irqnumber == 0) {
		diable_serial_interrupt ();
		free_irq (irqnum, NULL);
	};

	if (&rs485_device->my_dev)
		cdev_del(&rs485_device->my_dev);

	unregister_chrdev_region(devno, 1);

	printk(KERN_ALERT “net9 major removed”);

	if (&rs485_device->rx_fifo){
		kfifo_free ( &rs485_device->rx_fifo );
		printk(KERN_ALERT “net9 rx removed”);
	}

	if (&rs485_device->tx_fifo){
		kfifo_free ( &rs485_device->tx_fifo );
		printk(KERN_ALERT “net9 tx removed”);
	}

	if (rs485_device)
		kfree(rs485_device);

	printk(KERN_ALERT “net9 removed\n”);
}

/*————————————————————————*/
void rs485_init_baud(uint32 baud)
{
	uint32 divisor;
	divisor = BASE_BAUD / baud;
	setbit (UART_LCR, UART_LCR_DLAB);
	outportb (UART_DLL, (divisor & 0xff));
	outportb (UART_DLM, ((divisor >> 8) & 0xff));
	clrbit (UART_LCR, UART_LCR_DLAB);
};

/*————————————————————————*/
void init_serial_port(void)
{
	outportb (UART_FCR, UART_FIFO_SETUP);
	rs485_init_baud (9600);
	request_to_send ();
	init_mode_addr ();
	enable_rx_interrupt ();
	setbit (UART_MCR, (UART_MCR_OUT1 | UART_MCR_OUT2));
	setbit (UART_MCR, UART_MCR_DTR);
};

int init_kernel_buffer(void)
{
	int result;

	/*
	* allocate the device
	*/
	rs485_device = kmalloc(sizeof(struct rs485_dev), GFP_KERNEL);
	if (!rs485_device) {
		result = -ENOMEM;
		goto fail;  /* Make this more graceful */
	}
	memset(rs485_device, 0, sizeof(struct rs485_dev));

	init_MUTEX(&(rs485_device->sem));

	if (kfifo_alloc(&rs485_device->rx_fifo, BUFFER_SIZE, GFP_KERNEL)) {
		printk(KERN_WARNING “net 9 error kfifo_alloc rx\n”);
		return -ENOMEM;
	}

	if (kfifo_alloc(&rs485_device->tx_fifo, BUFFER_SIZE, GFP_KERNEL)) {
		printk(KERN_WARNING “net 9 error kfifo_alloc tx\n”);
		return -ENOMEM;
	}

	init_waitqueue_head ( &rs485_device->inq );
	init_waitqueue_head ( &rs485_device->outq );

	return 0;

	fail:
		return result;
}

static int rs485_init(void)
{
	int result;
	dev_t devno = MKDEV(0, 0);

	result = init_kernel_buffer();
	if (result){
		goto fail;
	}

	/*
	* Register your major, and accept a dynamic number.
	*/
	if (rs485_major)
		result = register_chrdev_region(devno, 1, DEVICE_NAME);
	else {
		result = alloc_chrdev_region(&devno, 0, 1, DEVICE_NAME);
		rs485_major = MAJOR(devno);//let kernel allocate a major for us
		printk(KERN_ALERT “net9 major %d\n”,rs485_major);
	}

	if (result < 0)
		goto fail;

	rs485_setup_cdev(rs485_device);
	rs485_create_dev_node(rs485_device);

	irqnumber=100;

	irqnum = rs485_detect_uart_irq ();
	if (irqnum > 0) {
		irqnumber = request_irq (irqnum, ( void * ) rs485_interrupt, IRQF_DISABLED, DEVICE_NAME,NULL);
		if (irqnumber < 0) {
			printk (“net9 can’t get assigned irq %i\n”, irqnum);
			goto fail;
		};
	};

	printk(KERN_ALERT “net9 interrupt no = %d\n”,irqnum);

	init_serial_port();
	printk(KERN_ALERT “net9 registered\n”);
	return 0;

	fail:
		rs485_exit();
		return result;
}

module_init(rs485_init);
module_exit(rs485_exit);


