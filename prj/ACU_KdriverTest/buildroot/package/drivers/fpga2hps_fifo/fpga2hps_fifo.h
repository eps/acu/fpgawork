#include "altera_fifo.h"
#include <linux/miscdevice.h>


#ifndef FPGA2HPS_FIFO_H_
#define FPGA2HPS_FIFO_H_

#ifdef MODULE
    #include <linux/types.h>
    typedef __u8 uint8_t ;
    typedef __s8 int8_t;
    typedef __u16 uint16_t ;
    typedef __s16 int16_t;
    typedef __u32 uint32_t ;
    typedef __s32 int32_t;
    typedef __u64 uint64_t ;
    typedef __s64 int64_t;
#else
    #include <stdint.h>
#endif

 
struct fpa2hps_fifo_dev
{
    struct miscdevice miscdev;
    int irqnum;
    
    spinlock_t lock;
    
    altera_fifo_csr_t __iomem *fifo_csr;
    __u32 __iomem *fifo_data;
};

#endif // FPGA2HPS_FIFO_H_
