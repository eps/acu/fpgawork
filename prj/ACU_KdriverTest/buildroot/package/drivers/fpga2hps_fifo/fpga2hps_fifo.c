#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <linux/kfifo.h>

#include "fpga2hps_fifo.h"

#define DEVNAME "fpga2hps_fifo"
MODULE_DESCRIPTION(DEVNAME" driver");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Davide Rodomonti, GSI");
MODULE_ALIAS("platform:"DEVNAME);

/*
	This driver is intended to react on an interrupt coming from a almost-full FPGA to HPS FIFO, 
	read up to 4096 samples and push them to a KFIFO. The KFIFO is interfaced to the user space through a misc framework.
*/
#define ALMOSTFULL 4
#define EMPTY 2

//#define FIFO_LOC 4096
//#define FIFO_LOC 8192
//#define FIFO_LOC 3
#define THRS_ALMOSTFULL 1024
#define THRS_ALMOSTEMPTY 10
#define FIFO_SIZE (16384 * 32) // with write action @1MSps, the FIFO will be full after 4,096 ms
DECLARE_KFIFO(samples_fifo, __u32, FIFO_SIZE);

// NOTE: the device structure is defined in the header file.

// misc file operation functions
int fpga2hps_fifo_open(struct inode *nodep, struct file *filep);
int fpga2hps_fifo_release(struct inode *nodep, struct file *filep);
ssize_t fpga2hps_fifo_read(struct file *, char __user *, size_t, loff_t *);

// driver functions
static int fpga2hps_fifo_probe(struct platform_device* pdev);
static int fpga2hps_fifo_remove(struct platform_device *pdev);

// driver compatibility setting
static const struct of_device_id fpga2hps_fifo_id[] = {
	{.compatible = "gsi-eps,fifo_fpga2hps"}, {}
};

// Inform kernel about the devices this driver supports
MODULE_DEVICE_TABLE(of, fpga2hps_fifo_id);

// Attribute section

ssize_t attr_fill_level_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);

	__u32 value = (__u32) ioread32(&(mydev->fifo_csr->fill_level));
	len = sprintf(buf, "%u\n", value);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

DEVICE_ATTR(fill_level, 0664, attr_fill_level_show, NULL);

ssize_t attr_status_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);

	altera_fifo_status_t value = (altera_fifo_status_t) ioread32(& mydev->fifo_csr->status);
	len = sprintf(buf, "%u\n", value.reg);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

DEVICE_ATTR(status, 0664, attr_status_show, NULL);

ssize_t attr_event_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);

	altera_fifo_status_t value = (altera_fifo_status_t) ioread32(&(mydev->fifo_csr-> event));
	len = sprintf(buf, "%u\n", value.reg);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

ssize_t attr_event_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);
	__u32 value;

	// convert user input
	if((strnstr(buf, "0x", 2) != NULL) && (kstrtouint(buf, 16, &value) == 0)){ // if input is hex then interpret value as unsigned integer
		iowrite32(value, &(mydev->fifo_csr->event));
	}else if(kstrtoint(buf, 10, &value) == 0){
		iowrite32(value, &(mydev->fifo_csr->event));
	}else{
		pr_err("%s: kstrtouint failed\n", __PRETTY_FUNCTION__);
		return -EINVAL;
	}
	return count;
}

DEVICE_ATTR(event, 0664, attr_event_show, attr_event_store);

ssize_t attr_interrupt_enable_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);

	altera_fifo_status_t value = (altera_fifo_status_t) ioread32(&(mydev->fifo_csr->interrupt_enable));
	len = sprintf(buf, "%u\n", value.reg);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

ssize_t attr_interrupt_enable_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);
	__u32 value;

	// convert user input
	if((strnstr(buf, "0x", 2) != NULL) && (kstrtouint(buf, 16, &value) == 0)){ // if input is hex then interpret value as unsigned integer
		iowrite32(value, &(mydev->fifo_csr->interrupt_enable));
	}else if(kstrtoint(buf, 10, &value) == 0){
		iowrite32(value, &(mydev->fifo_csr->interrupt_enable));
	}else{
		pr_err("%s: kstrtouint failed\n", __PRETTY_FUNCTION__);
		return -EINVAL;
	}
	return count;
}

DEVICE_ATTR(interrupt_enable, 0664, attr_interrupt_enable_show, attr_interrupt_enable_store);

ssize_t attr_almost_full_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);

	__u32 value = (__u32) ioread32(&(mydev->fifo_csr->almost_full_threshold));
	len = sprintf(buf, "%u\n", value);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

ssize_t attr_almost_full_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);
	__u32 value;

	// convert user input
	if((strnstr(buf, "0x", 2) != NULL) && (kstrtouint(buf, 16, &value) == 0)){ // if input is hex then interpret value as unsigned integer
		iowrite32(value, &(mydev->fifo_csr->almost_full_threshold));
	}else if(kstrtoint(buf, 10, &value) == 0){
		iowrite32(value, &(mydev->fifo_csr->almost_full_threshold));
	}else{
		pr_err("%s: kstrtouint failed\n", __PRETTY_FUNCTION__);
		return -EINVAL;
	}
	return count;
}

DEVICE_ATTR(almost_full, 0664, attr_almost_full_show, attr_almost_full_store);

ssize_t attr_almost_empty_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);

	__u32 value = (__u32) ioread32(&(mydev->fifo_csr->almost_empty_threshold));
	len = sprintf(buf, "%u\n", value);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

ssize_t attr_almost_empty_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
	struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);
	__u32 value;

	// convert user input
	if((strnstr(buf, "0x", 2) != NULL) && (kstrtouint(buf, 16, &value) == 0)){ // if input is hex then interpret value as unsigned integer
		iowrite32(value, &(mydev->fifo_csr->almost_empty_threshold) );
	}else if(kstrtoint(buf, 10, &value) == 0){
		iowrite32(value, &(mydev->fifo_csr->almost_empty_threshold));
	}else{
		pr_err("%s: kstrtouint failed\n", __PRETTY_FUNCTION__);
		return -EINVAL;
	}
	return count;
}

DEVICE_ATTR(almost_empty, 0664, attr_almost_empty_show, attr_almost_empty_store);


ssize_t attr_kfifo_status_show(struct device *dev, struct device_attribute *attr, char *buf){
	int len;
	//struct fpa2hps_fifo_dev *mydev = dev_get_drvdata(dev);

	//altera_fifo_status_t value = (altera_fifo_status_t) ioread32(& mydev->fifo_csr->status);
	size_t kfifo_level=kfifo_avail(&samples_fifo);
	len = sprintf(buf, "%u\n", kfifo_level);
	if(len <= 0) dev_err(dev, "%s: Invalid sprintf len: %d\n",__PRETTY_FUNCTION__, len);
	return len;
}

DEVICE_ATTR(kfifo_status, 0664, attr_kfifo_status_show, NULL);


struct attribute *dev_value_attrs[] = {
	&dev_attr_fill_level.attr, 
	&dev_attr_status.attr, 
	&dev_attr_event.attr, 
	&dev_attr_interrupt_enable.attr, 
	&dev_attr_almost_full.attr, 
	&dev_attr_almost_empty.attr, 
        &dev_attr_kfifo_status.attr, 
	NULL
};

struct attribute_group dev_attr_group = {
	.attrs = dev_value_attrs
};

const struct attribute_group *dev_attr_groups[] = {
	&dev_attr_group,
	NULL
};

// Platform device structure definition
static struct platform_driver fpga2hps_fifo_driver = {
	.driver = {
		.name = DEVNAME,
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(fpga2hps_fifo_id),
		.dev_groups = dev_attr_groups
	},
	.probe = fpga2hps_fifo_probe,
	.remove = fpga2hps_fifo_remove
};

// misc file operation structure
struct file_operations fops = {
	.owner = THIS_MODULE,
	.read = fpga2hps_fifo_read,
	.open = fpga2hps_fifo_open,
	.release = fpga2hps_fifo_release
};

int fpga2hps_fifo_open(struct inode *nodep, struct file *file){
	pr_debug("%s\n", __PRETTY_FUNCTION__);
	return 0;
}
int fpga2hps_fifo_release(struct inode *nodep, struct file *file){
	uint32_t tmp;
	int ret;
	
	pr_debug("%s\n", __PRETTY_FUNCTION__);
	// Maybe there is a better way to do that
	//pr_info("%s:kfifo avail before get=%d \n", __PRETTY_FUNCTION__, kfifo_avail(&samples_fifo)); 
	size_t iter=(16384 * 32)-kfifo_avail(&samples_fifo);
	//pr_info("%s:Nr of read necessary to make the kfifo empty=%d \n", __PRETTY_FUNCTION__, iter); 
	
	pr_info("%s: Flush %d samples from the kfifo \n", __PRETTY_FUNCTION__, iter>>5); // (Total bit - avail bit)/32 = Nr of simbol inside the kfifo
	for (size_t i=0; i<iter;i++){
	  ret=kfifo_get(&samples_fifo,&tmp);
        }
        //pr_info("%s:kfifo avail after gets=%d \n", __PRETTY_FUNCTION__, kfifo_avail(&samples_fifo)); 
	pr_info("%s: Check if the kfifo is empty..", __PRETTY_FUNCTION__);		
	if(!kfifo_is_empty(&samples_fifo)) {
          pr_err("kfifo not empty after flushing.\n");
        } else {
          pr_info("Empty! Have a nice day!\n");		
        }
	return 0;
}

ssize_t fpga2hps_fifo_read(struct file * file, char __user *buffer, size_t count, loff_t *position) {
	int status = 0;

	__u32 bytes_copied;
	pr_debug("%s: Start reading kfifo \n", __PRETTY_FUNCTION__);	
	pr_debug("%s: kfifo avail bit/s before read= %d \n", __PRETTY_FUNCTION__,kfifo_avail(&samples_fifo));	
	status = kfifo_to_user(&samples_fifo, buffer, count, &bytes_copied);
	if(status == 0){
		pr_debug("%s: bytes_copied = %d\n", __PRETTY_FUNCTION__, bytes_copied);
		pr_debug("%s: kfifo avail bit/s after read= %d \n", __PRETTY_FUNCTION__,kfifo_avail(&samples_fifo));	
		return bytes_copied;
	}else {
          	pr_err("%s: Error reading the kfifo. Status=%d\n", __PRETTY_FUNCTION__,status);
		return status;
	}
}


/*
// Interrupt service routine
static irq_handler_t fpga2hps_fifo_irq_isr(int irq, void *dev_id, struct pt_regs *regs){
	struct fpa2hps_fifo_dev *dev = (struct fpa2hps_fifo_dev*) dev_id;
	//__u32 sample = ioread32(&(dev->regs)[3]);
	
	__u32 sample =0;
	
//	__u32 rd_chunkSize = (__u32) ioread32(&(dev->fifo_csr->almost_full_threshold));
	__u32 rd_chunkSize = (__u32) ioread32(&(dev->fifo_csr->fill_level));
	pr_debug("%s: fill level before read= (%d) \n", __PRETTY_FUNCTION__, rd_chunkSize);
	
	if(kfifo_initialized(&samples_fifo)){
		if(kfifo_avail(&samples_fifo) > 0) {
		
			//for (size_t i=0; i<FIFO_LOC;i++){
			for (size_t i=0; i<rd_chunkSize;i++){
			  // read from fpga2hps FIFO
			  sample = ioread32(&(dev->fifo_data)[0]);
			  
			  // write to KFIFO
			  kfifo_put(&samples_fifo, sample);
			  //pr_debug("%s: new sample (%d) in fifo (%d)\n", __PRETTY_FUNCTION__, sample, kfifo_len(&samples_fifo)); 
			}
		
		}
		else pr_err("%s: fifo full. Sample (%d) dropped\n", __PRETTY_FUNCTION__, sample);
	}else{
		pr_err("%s: fifo not initialized. Sample (%d) dropped\n", __PRETTY_FUNCTION__, sample);
	}
	
 	// Clear the event bit
 	iowrite32(ALMOSTFULL, &(dev->fifo_csr->event) );
 	
 	pr_debug("%s: fill level after read= (%d) \n", __PRETTY_FUNCTION__, (__u32) ioread32(&(dev->fifo_csr->fill_level)));

	// pr_debug("%s(irq: %d)\n", __PRETTY_FUNCTION__, irq);
	return (irq_handler_t) IRQ_HANDLED;
}

*/


// Interrupt service routine
static irq_handler_t fpga2hps_fifo_irq_isr(int irq, void *dev_id, struct pt_regs *regs){
	struct fpa2hps_fifo_dev *dev = (struct fpa2hps_fifo_dev*) dev_id;
	
	unsigned long flags;
	__u32 sample =0;
	
	//Locking
	pr_debug("%s: Spin locking \n", __PRETTY_FUNCTION__);	
	spin_lock_irqsave(&(dev->lock), flags);

	// for debug purpose only
	__u32 stayThrs =(__u32) ioread32(&(dev->fifo_csr->almost_empty_threshold));
	
	__u32 rd_chunkSize = (__u32) ioread32(&(dev->fifo_csr->fill_level));
	pr_debug("%s: fill level @ isr start= (%d) \n", __PRETTY_FUNCTION__, rd_chunkSize);
	
	while (rd_chunkSize > stayThrs) {
		if(kfifo_avail(&samples_fifo) > 0) {
		
			for (size_t i=0; i<rd_chunkSize;i++){
				// read from fpga2hps FIFO
				sample = ioread32(&(dev->fifo_data)[0]);
				  
				// write to KFIFO
				kfifo_put(&samples_fifo, sample);
				//pr_debug("%s: new sample (%d) in fifo (%d)\n", __PRETTY_FUNCTION__, sample, kfifo_len(&samples_fifo)); 
			}
		
		} else {
			pr_err("%s: fifo full. Sample (%d) dropped\n", __PRETTY_FUNCTION__, sample);
			break;
		}
		
		rd_chunkSize = (__u32) ioread32(&(dev->fifo_csr->fill_level));
		//pr_debug("%s: fill level @ isr while loop= (%d) \n", __PRETTY_FUNCTION__, rd_chunkSize);
	}
	
 	// Clear the event bit
 	//iowrite32(EMPTY, &(dev->fifo_csr->event) );
 	iowrite32(ALMOSTFULL, &(dev->fifo_csr->event) );
 	
 	pr_debug("%s: fill level @ isr end= (%d) \n", __PRETTY_FUNCTION__, (__u32) ioread32(&(dev->fifo_csr->fill_level)));
 	altera_fifo_status_t value = (altera_fifo_status_t) ioread32(& (dev->fifo_csr->event));
	pr_debug("%s: Event @ isr end= (%d) \n", __PRETTY_FUNCTION__, value.reg);

	//Unlocking
        pr_debug("%s: Spin Unlocking \n", __PRETTY_FUNCTION__);	
	spin_unlock_irqrestore(&(dev->lock), flags);

	// pr_debug("%s(irq: %d)\n", __PRETTY_FUNCTION__, irq);
	return (irq_handler_t) IRQ_HANDLED;
}

static int fpga2hps_fifo_probe(struct platform_device* pdev){
	int status;
	struct fpa2hps_fifo_dev *dev = NULL;
	struct resource *r = NULL;

	// Anything that needs to be allocated should be allocated via the devm_* functions
	// devm_* functions create "managed resources" which are freed/released automatically on release/exit function
	//pr_info("%s: Entering to the probe function\n", __PRETTY_FUNCTION__);
	// Allocate space to store the device structure
	dev = devm_kzalloc(&pdev->dev, sizeof(struct fpa2hps_fifo_dev), GFP_KERNEL);
	if(dev != NULL){
		// retrieve resource information from the device tree by name: memory base address and length
		// fifo_csr
		r = platform_get_resource_byname(pdev, IORESOURCE_MEM, "fifo-csr");
		if (!r) {
			pr_err("%s: Failed to get fifo-csr resources\n", __PRETTY_FUNCTION__);
			return -EINVAL;	
		}
		dev->fifo_csr = devm_ioremap_resource(&pdev->dev, r);
		if (!dev->fifo_csr) {
			pr_err("%s: Failed to remap fifo-csr config space\n", __PRETTY_FUNCTION__);
			return -ENOMEM;
		}
		
		// fifo_data
		r = platform_get_resource_byname(pdev, IORESOURCE_MEM, "fifo-data");
		if (!r) {
			pr_err("%s: Failed to get fifo-data resources\n", __PRETTY_FUNCTION__);
			return -EINVAL;	
		}
		dev->fifo_data = devm_ioremap_resource(&pdev->dev, r);
		if (!dev->fifo_data) {
			pr_err("%s: Failed to remap fifo-data config space\n", __PRETTY_FUNCTION__);
			return -ENOMEM;
		}
		
		// retrieve the interrupt number from the device tree 				   
		dev->irqnum = platform_get_irq(pdev, 0);
		if (dev->irqnum < 0) {
		  pr_err("%s: platform_get_irq failed  = %d)\n", __PRETTY_FUNCTION__, dev->irqnum);
		  return -ENXIO;		
		} 
		// link the interrupt to the interrupt service routine
		status = request_irq(dev->irqnum, (irq_handler_t)fpga2hps_fifo_irq_isr, 0, DEVNAME, dev);
		if(status == 0) {
			pr_debug("%s: IRQ [%d] registered\n", __PRETTY_FUNCTION__, dev->irqnum);
		}else{
			pr_err("%s: IRQ failed to register (error = %d)\n", __PRETTY_FUNCTION__, status);
		}
 		
 		// Initialize the spinlock
 		spin_lock_init(&(dev->lock)); 
 		
		//Populate the misc structure items 
		dev->miscdev.minor = MISC_DYNAMIC_MINOR;
		dev->miscdev.name = DEVNAME;
		dev->miscdev.fops = &fops;

		// create a misc device
		status = misc_register(&dev->miscdev);
		if(status == 0) {
			platform_set_drvdata(pdev, (void*)dev);
			INIT_KFIFO(samples_fifo);
			
			if(kfifo_initialized(&samples_fifo)){
				// Set the almost full threshold to 1024
				iowrite32((__u32) THRS_ALMOSTFULL, &(dev->fifo_csr->almost_full_threshold));
				// Set the almost empty threshold to 10
				// NOTE: the almost empty threshold value is used here not to generate an interrupt but to keep the driver more time inside the isr.
				iowrite32((__u32) THRS_ALMOSTEMPTY, &(dev->fifo_csr->almost_empty_threshold));

				// Enable the interlock on ALMOST FULL bit
 				iowrite32((__u32) ALMOSTFULL, &(dev->fifo_csr->interrupt_enable));
 				// Clear the event bit
 				iowrite32(255, &(dev->fifo_csr->event));

				pr_info("%s: probe successful\n", __PRETTY_FUNCTION__);
		   
			}else{
				pr_err("%s: fifo not initialized.\n", __PRETTY_FUNCTION__);
			}			
		}else {
			pr_err("%s: misc_register failed\n", __PRETTY_FUNCTION__);
		}
		return status;
	}else{
		pr_err("%s: devm_kzalloc failed\n", __PRETTY_FUNCTION__);
		return -EBUSY;
	}
}

static int fpga2hps_fifo_remove(struct platform_device *pdev){
	// no need to free any resources manually here
	// it's done by the kernel (managed resources / devm_*)
	struct fpa2hps_fifo_dev *dev = (struct fpa2hps_fifo_dev*) platform_get_drvdata(pdev);
        // Disable the interlock on almost full bit
	iowrite32(0, &(dev->fifo_csr->interrupt_enable));
        free_irq(dev->irqnum, dev);
	misc_deregister(&dev->miscdev);

	return 0;
}

module_platform_driver(fpga2hps_fifo_driver);

