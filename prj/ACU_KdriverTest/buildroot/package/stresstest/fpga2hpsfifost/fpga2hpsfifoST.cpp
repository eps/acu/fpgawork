//============================================================================
// Name        : fpga2hpsfifoST.cpp
// Author      : drodomon
// Version     :
// Copyright   : Your copyright notice
// Description : This program runs the fpga2hpsfifoApp SW n-times checking 
//               for error conditions (FIFO FULL) and storing the generated 
//               data file.

//               NOTE: It would be better to send the data file to the host 
//                     and remove it afterwards because I can't store in the
//                     target device more than 2 files 8MB each
//============================================================================

#include <sys/stat.h>
#include <iostream>
#include <numeric>
#include <cassert>
#include <sstream>
#include <algorithm>
#include <sys/mman.h>				// POSIX: memory maping
#include <fcntl.h>					// POSIX: "PROT_WRITE", "MAP_SHARED", ...
#include <unistd.h>					// POSIX: for closing the Linux driver access

#include <string.h>
#include <fstream>
#include <sstream>
#include "bRegAcc.h"

#define H2F_AXI_BASE			0xC0000000
#define FPGA2HPS_FIFO_CSR_EVENT_OFFSET	0x8
#define FIFO_FULL_MASK                  0x1

// argument help function
std::string printHelp() {
    std::string msg="fpga2hpsfifoST  <program with arguments to run>(s) nr_iterations(h) storeDataFile(true/false)(b)\n";
    return msg;
}

bool checkArgs(int argc, char *argv[]){
       // Arguments check
    if ((argc < 3) || (std::string(argv[1]) == "-h")|| (std::string(argv[1]) == "--h") || (argc > 4)) {
      std::cout << printHelp() << std::endl;
    } else {
      return true; 
    }
    return false;
}

bool Check4Full(){
  
  // This function checks if the FIFO ip full event bit is set and in that case it returns true
    int fd{};           						// file descriptor
    void* pMemMap_v;						  	// void pointer returning from the mmap

    // Open Linux Memory Driver port
   	fd = open("/dev/mem", (O_RDWR | O_SYNC));

   	// Check that the opening was successful
   	if (fd < 0)
   	{
   		std::cout << "Error: Failed to open the memory driver!\n";
   	}

   	// I map the complete HW memory (1G) from 0xc0000000 to 0xffffffff.
   	pMemMap_v = mmap( NULL, MAP_SIZE, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE & ~MAP_MASK);

   	if (pMemMap_v == MAP_FAILED)
   	{
   		std::cout << "\n Error:  Failed to open the memory mapped interface\n";
   		close(fd);
   	}

   	close(fd);

   	// Object instances
   	bRegAcc regObject{};			// Basic Register Access class(bRegAcc)
   	
   	uint32_t RdValue{0};			// tmp variable for the regObject.getReadValue() return value


    	//Setup the OCR_DT_TRANS_CFG_OFFSET polling in order to check when a new data transfer has to start
    	regObject.setCmd("rd");
    	regObject.setAddr((uint32_t)(H2F_AXI_BASE+FPGA2HPS_FIFO_CSR_EVENT_OFFSET));
   	regObject.ExeRdWr(false, pMemMap_v);
   	RdValue= regObject.getReadValue();
   	
   	//std::cout << "deinit\n";
   	// deinit
   	munmap((void*) pMemMap_v, MAP_SIZE);
   	
    	//if ((RdValue & FIFO_FULL_MASK)==1){
    	if (RdValue & FIFO_FULL_MASK){
      		printf("FIFO ip full event detected. Event reg value= %u\n",RdValue);
      		return true;
    	} else {
      		return false;
    	}
  /*
  uint32_t RdValue{9};
  uint32_t tmpMask=RdValue & FIFO_FULL_MASK;
  uint32_t tmpMask_n=RdValue & ~FIFO_FULL_MASK;
  
  std::cout << "RdValue & FIFO_FULL_MASK= "<< tmpMask<< std::endl;
  std::cout << "RdValue & ~FIFO_FULL_MASK= "<< tmpMask_n<< std::endl;
  
  if ((RdValue & FIFO_FULL_MASK)==1){
    printf("FIFO ip full event detected. Event reg value= %u\n",RdValue);
    return true;
  } else {
    return false;
  }
*/
}

int main (int argc, char **argv) {
    
    std::string tmpStr;
    std::string sub0, sub1;
    std::string del = ".";
    int start, end = -1*del.size();
    long int iter;
    char *p;
    if (checkArgs(argc, argv)) {
        // string to long int conversion of the iteration number
        iter = strtol( argv[2], &p, 10);
        
        // char to boolean conversion with argument validity check.
        std::stringstream ss(argv[3]);
        bool b;

        if(!(ss >> std::boolalpha >> b)) {
          printf("Error in the storeDataFile argument\n");
          std::cout << printHelp() << std::endl;
          
        } else {
          tmpStr= (std::string) argv[1];
          if (b) {
            // Data file name split
            start = end + del.size();
            end = tmpStr.find(del, start);
            //printf("start index= %d; end index=%d\n",start,end);
            sub0=tmpStr.substr(start, end - start);
            sub1=tmpStr.substr(end - start,tmpStr.length());
            //std::cout << "sub0= " << sub0 << std::endl;
            //std::cout << "sub1= " << sub1 << std::endl;
          } else {
            std::cout << "Not necessary to store the data file" << std::endl;
          }

          //iterator
          for (int i=0;i<iter;i++) {
            // Build the SW string to call
            if (b) {
              // In this case a data file is generated for each iteration and marked with <_iterNr>
              tmpStr= sub0+"_"+std::to_string(i)+sub1;
            } else {
              // In this case the data file is overwritten at each iteration 
              tmpStr=(std::string) argv[1];
            }

            //Extract the data file name
            // A quick way to split strings separated via spaces.
            std::stringstream ss2(tmpStr);
            std::string DataFileName;
            bool fileName = false;
            size_t FileNamePos;
            while (ss2 >> DataFileName) {
              FileNamePos=DataFileName.find(del);
              
              if (FileNamePos != (size_t)std::string::npos) {
                fileName=true;
                break;
              }
            }
            //std::cout<< "DataFileName="<< DataFileName <<std::endl; 

            // Call the SW
            //std::cout<< "call nr"<< i<< ": "<< tmpStr<<std::endl; //Debug
            (void)  ((int) system(tmpStr.c_str()));  //On the target machine only

            //Wait till the data file is detected
            // NOTE: The wait below works, but it can stay there forever and consumes CPU resources.
            struct stat buffer;   
            while(1){
              // it is necessary to check if the fifo is full also here, but I don't want to interfear with the data transfer!!
              if(stat (DataFileName.c_str(), &buffer) == 0) {
                //std::cout << "File created" <<std::endl;
                break;
              }  
            };

            //Call the check full function and check the return value.If higher than zero, break the loop and exit. 
            //if(Check4Full()) {
            //  break;
            //}

            bool checkRes = Check4Full();
            
            // Check the event register
            (void)  ((int) system( (const char*) "bregacc rd 0xC0000008"));

            // Clear the event register (maybe not necessary)
            (void)  ((int) system( (const char*) "bregacc wr 0xC0000008 0x000000FF 0x000000FF"));
            
            // Send the data file to the host
            // TO DO!!
            
            // Remove the data file
            tmpStr="rm -rf " + DataFileName;
            (void)  ((int) system(tmpStr.c_str()));
            
            if (checkRes) {            
              std::cout << "Stopping the loop @ iteration number "<< i << std::endl;
              break;
            }
          }
        }

        
    }

    return 0;
}
