//============================================================================
// Name        : hps2fpgafifoST.cpp
// Author      : drodomon
// Version     :
// Copyright   : Your copyright notice
// Description : This program runs the hps2fpgafifoApp SW n-times checking 
//               for payload alarm/overflow.
//============================================================================

#include <sys/stat.h>
#include <iostream>
#include <numeric>
#include <cassert>
#include <sstream>
#include <algorithm>
#include <sys/mman.h>				// POSIX: memory maping
#include <fcntl.h>					// POSIX: "PROT_WRITE", "MAP_SHARED", ...
#include <unistd.h>					// POSIX: for closing the Linux driver access

#include <string.h>
#include <fstream>
#include <sstream>
//#include "bRegAcc.h"
/*
#define H2F_AXI_LWL_BASE	        0xFF200000
#define GPIO_IN			        0x20
#define PLYD_ALARM_MASK                 0x1
#define PLYD_OVF_MASK                   0x2
*/


// argument help function
std::string printHelp() {
    std::string msg="hps2fpgafifoST  <program with arguments to run>(s) nr_iterations(d)\n";
    return msg;
}

bool checkArgs(int argc, char *argv[]){
       // Arguments check
    if ((argc < 2) || (std::string(argv[1]) == "-h")|| (std::string(argv[1]) == "--h") || (argc > 3)) {
      std::cout << printHelp() << std::endl;
    } else {
      return true; 
    }
    return false;
}

int main (int argc, char **argv) {
    
    std::string tmpStr;
    long int iter;
    char *p;
    
    int ret {0};
    
    if (checkArgs(argc, argv)) {
        // string to long int conversion of the iteration number
        iter = strtol( argv[2], &p, 10);    
        tmpStr= (std::string) argv[1];
        printf("tmpStr= %s\n",tmpStr);
        for (int i=0;i<iter;i++) {
          ret=  (int) system(tmpStr.c_str());
        
          //Check for returned value
          if (ret != 0) {
            printf("Error: Iteration nr %d returned with %d\n",i,ret);
            break;
          }
        
        }
        
        printf("End of the program\n Bye!\n");
    }
    
    return 0;
}
