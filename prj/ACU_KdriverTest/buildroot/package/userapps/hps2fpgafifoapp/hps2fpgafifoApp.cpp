//============================================================================
// Name        : hps2fpgafifoApp.cpp
// Author      : drodomon
// Version     :
// Copyright   : Your copyright notice
// Description : This program selects the hps2fpga read speed on the fpga 
//               side, clear the payload alarm and sends a file (in 8192*4 B chunks) 
//               to the kfifo(or directly to the hps2fpga fifo) monitoring 
//               the fill level.
//============================================================================


#include <iostream>
#include <numeric>
#include <cassert>
#include <sstream>
#include <algorithm>
#include <sys/mman.h>				        // POSIX: memory maping
#include <fcntl.h>					// POSIX: "PROT_WRITE", "MAP_SHARED", ...
#include <unistd.h>					// POSIX: for closing the Linux driver access

#include <string.h>
#include <fstream>
#include <sstream>
#include "bRegAcc.h"

//#define NSAMPLES ((size_t)5)

#define H2F_AXI_LWL_BASE		            0xFF200000
#define GPIO_IN				                  0x20
#define PLYD_ALARM_MASK                 0x1
#define PLYD_OVF_MASK                   0x2

#define H2F_AXI_BASE			              0xC0000000
#define HPS2FPGA_FIFO_CSR               0x40
#define CSR_STATUS                      0x4
#define CSR_EVENT                       0x8
#define CSR_AL_EMPTY_MASK               0x8
#define CSR_EMPTY_MASK                  0x2
#define CSR_FULL_MASK                   0x1
#define CSR_WAIT_MASK                   0xA

#define FIFO_SIZE                       0x2000


bool bCheckReg (void * ptr, uint32_t addr, int mask ) {
  // Object instances
  bRegAcc regObject{};			// Basic Register Access class(bRegAcc)
  uint32_t RdValue{0};			// tmp variable for the regObject.getReadValue() return value

  regObject.setCmd("rd");
  regObject.setAddr(addr);
  regObject.ExeRdWr(false, ptr);
  RdValue= regObject.getReadValue();
 
  if (RdValue & mask){
    return true;
  } else {
    return false;
  }
}

uint32_t CheckReg (void * ptr, uint32_t addr) {
  // Object instances
  bRegAcc regObject{};			// Basic Register Access class(bRegAcc)   	
  
  regObject.setCmd("rd");
  regObject.setAddr(addr);
  regObject.ExeRdWr(false, ptr);
  return regObject.getReadValue();
}

// argument help function
std::string printHelp() {
  std::string msg="hps2fpgafifoApp file2sendPath/Name(s) source_speed(h)";
  return msg;
}

bool checkArgs(int argc, char *argv[]){
  // Arguments check
  if ((argc < 2) || (std::string(argv[1]) == "-h")|| (std::string(argv[1]) == "--h") || (argc > 3)) {
    std::cout << printHelp() << std::endl;
  } else {
    return true; 
  }
  return false;
}

int main (int argc, char **argv) {
    
  int fd{};                                                             //Kernel module file descriptor
  int fd_LinuxMemDrv{};                                                 // Linux memory drive file descriptor
  void* pMemMap_v;                                                      // void pointer returning from the mmap

  size_t readByte{0};
  size_t tmpReadByte{0};      	                                        // I had to place a tmpReadByte because the read action can return also 0 even if there are still samples to fetch. 
                                                                        // Checking that allows me to avoid the unwanted read_buf index shift.
  char * read_buf;                                                      // Buffer where the complete file to send is stored.
  uint32_t sourceSpeed;                                                 // uint32_t converted third argument
  char * buffer;                                                        // char pointer used to find out the file to send size
  int file_size;                                                        // file to send size

  int chunk{0};                                                         // Number of byte written in one shot
  int iter{0};                                                          // Number of iteration necessary to write all the file
  int left{0};                                                          // Byte left to write after the iteration

  if (checkArgs(argc, argv)) {
    	
    /////////////// Memory mapping ///////////////
    // Open Linux Memory Driver port
    fd_LinuxMemDrv = open("/dev/mem", (O_RDWR | O_SYNC));

    // Check that the opening was successful
    if (fd_LinuxMemDrv < 0)
    {
      std::cout << "Error: Failed to open the memory driver!\n";
      return -1;
    }

    // I map the complete HW memory (1G) from 0xc0000000 to 0xffffffff.
    pMemMap_v = mmap( NULL, MAP_SIZE, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd_LinuxMemDrv, HW_REGS_BASE & ~MAP_MASK);

    if (pMemMap_v == MAP_FAILED)
    {
      std::cout << "\n Error:  Failed to open the memory mapped interface\n";
      close(fd_LinuxMemDrv);
      return -1;
    }

    close(fd_LinuxMemDrv);
    //////////////////////////////////////////////
           
    // char to uint32_t conversion
    std::istringstream buffer2(argv[2]);
    buffer2 >> std::hex >> sourceSpeed;
    // it can be done better/safer the source speed selection
    std::string tmpStr= "bregacc wr 0xFF200000 0x0000004" + std::to_string(sourceSpeed) +" 0x000000FF";

    // Open the driver
    fd = open("/dev/hps2fpga_fifo", (O_RDWR));
    // Check that the opening was successful
    if (fd < 0)
    {
      std::cout << "Error: Failed to open fpga2hps_fifo driver file!\n";
      return 0;
    }
        
    // Check the file size       
    std::ifstream in_file(std::string(argv[1]), std::ios::binary);
    in_file.seekg(0, std::ios::end);
    file_size = in_file.tellg();
    in_file.seekg(0);
    std::cout<<"Size of the file is"<<" "<< file_size<<" "<<"bytes\n";
        
    read_buf = new char [file_size];
    // read content of infile
    in_file.read (read_buf,file_size);
      
    // Release the fifo reset and select the 125KHz data source
    (void)  ((int) system(tmpStr.c_str()));
    
    // Clear the payload alarm
    printf("Clear the payload alarm\n");
    tmpStr= "bregacc wr 0xFF200000 0x00000020 0x00000020";
    (void)  ((int) system(tmpStr.c_str()));
    tmpStr= "bregacc wr 0xFF200000 0x00000000 0x00000020";
    (void)  ((int) system(tmpStr.c_str()));
             
    // Clear the FIFO CSR EVENT register
    // NOTE: to doublecheck the hps2fpgafifoApp program, make the hps2fpga FIFO full before running the program.
    //       Don't forget before to comment the three code lines below.
    printf("Clear the FIFO CSR EVENT register\n");
    tmpStr= "bregacc wr 0xC0000048 0x000000FF 0x000000FF";
    (void)  ((int) system(tmpStr.c_str()));
 
    tmpReadByte=0;
    //Check for FIFO EMPTY state 
    if (bCheckReg(pMemMap_v, H2F_AXI_BASE+HPS2FPGA_FIFO_CSR+CSR_STATUS, CSR_EMPTY_MASK)) {
      printf("FIFO empty..let's start\n");
      if (file_size < (FIFO_SIZE*4)) {
        printf("\t Small file %d B",file_size);
        chunk = file_size;
        tmpReadByte=write(fd, &read_buf[readByte], chunk); 

        // Check the write action
        if (tmpReadByte != (size_t)chunk) {
          printf("Error: Expected to write %d B, but only %d were written\n",chunk,tmpReadByte);
          return -1;
        }
      } else {
        printf("\t Big file %d B\n",file_size);
        chunk = (FIFO_SIZE*4);
        size_t index;

        iter = file_size / chunk;
        left = file_size % chunk;
        for (int i=0;i<iter;i++) {
          index=readByte>> 2;
          tmpReadByte=write(fd, &read_buf[readByte], chunk);

          //Check for empty and/or almost empty EVENT/s
          while (!(bCheckReg(pMemMap_v, H2F_AXI_BASE+HPS2FPGA_FIFO_CSR+CSR_EVENT, CSR_WAIT_MASK))) {
            usleep(50);
          }
          // Clear the event register (empty and almost empty bit)
          tmpStr= "bregacc wr 0xC0000048 0x0000000A 0x000000FF"; 
          (void)  ((int) system(tmpStr.c_str()));

          // Update the buffer index
          readByte = readByte + tmpReadByte;
        }
        
        // Write the left
        tmpReadByte=write(fd, &read_buf[readByte], left);

        // Update the buffer index
        readByte = readByte + tmpReadByte;
        
        // Check the write action
        if (readByte != (size_t)file_size) {
          printf("Error: Expected to write %d B, but only %d were written\n",file_size,readByte);
          return -1;
        }
      }
        
      printf("Sent %d Byte\n",file_size);
        
      //Check for a full EVENT detected
      if(bCheckReg(pMemMap_v, H2F_AXI_BASE+HPS2FPGA_FIFO_CSR+CSR_EVENT, CSR_FULL_MASK)) {
        printf("\t ERROR:a FIFO full event was detected\n");
        return -1;
      } else {
        // Check for payload alarm
        if(bCheckReg(pMemMap_v, H2F_AXI_LWL_BASE+GPIO_IN, PLYD_ALARM_MASK)) {
          printf("\t ERROR:Payload alarm detected\n");
          // Check for overflow
          if (bCheckReg(pMemMap_v, H2F_AXI_LWL_BASE+GPIO_IN, PLYD_OVF_MASK)) {
            printf("\t\t Payload alarm counter overflow detected\n");
            return 255;
          }
          // Read the payload alarm counter
          uint32_t cntAlm = CheckReg(pMemMap_v, H2F_AXI_LWL_BASE+GPIO_IN);
          printf("\t\t Counted %d payload alarms\n",cntAlm >>4 );
          return cntAlm >>4;
        } else {
          printf("\t without errors!\n");
        }
      }
    } else {
      printf("ERROR: The FIFO has already some items inside. Remove them and try again!\n");
      return -1;
    }
        
    // Reset the hps2fpga FIFO
    tmpStr= "bregacc wr 0xFF200000 0x00000000 0x000000FF";
    (void)  ((int) system(tmpStr.c_str()));
    
    // deinit
    munmap((void*) pMemMap_v, MAP_SIZE);

    // Close the driver
    close(fd);
      
    // free read_buf
    free(read_buf);
       
    printf("Program ends here!! Bye!\n ");
  }
  
  return 0;
}
