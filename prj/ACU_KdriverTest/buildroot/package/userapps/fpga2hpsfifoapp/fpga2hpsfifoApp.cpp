//============================================================================
// Name        : fpga2hpsfifoApp.cpp
// Author      : drodomon
// Version     :
// Copyright   : Your copyright notice
// Description : This program gets a programmable number of 32b samples 
//               from the fpga2hps FIFO and saves them in a file.
//============================================================================


#include <iostream>
#include <numeric>
#include <cassert>
#include <sstream>
#include <algorithm>
#include <sys/mman.h>				// POSIX: memory maping
#include <fcntl.h>					// POSIX: "PROT_WRITE", "MAP_SHARED", ...
#include <unistd.h>					// POSIX: for closing the Linux driver access

#include <string.h>
#include <fstream>
#include <sstream>
#include "bRegAcc.h"

//#define NSAMPLES ((size_t)5)

#define H2F_AXI_BASE			0xC0000000
#define FPGA2HPS_FIFO_CSR_EVENT_OFFSET	0x8
#define FIFO_FULL_MASK                  0x1


bool Check4Full(){
  
  // This function checks if the FIFO ip full event bit is set and in that case it returns true
    int fd{};           						// file descriptor
    void* pMemMap_v;						  	// void pointer returning from the mmap

    // Open Linux Memory Driver port
   	fd = open("/dev/mem", (O_RDWR | O_SYNC));

   	// Check that the opening was successful
   	if (fd < 0)
   	{
   		std::cout << "Error: Failed to open the memory driver!\n";
   	}

   	// I map the complete HW memory (1G) from 0xc0000000 to 0xffffffff.
   	pMemMap_v = mmap( NULL, MAP_SIZE, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE & ~MAP_MASK);

   	if (pMemMap_v == MAP_FAILED)
   	{
   		std::cout << "\n Error:  Failed to open the memory mapped interface\n";
   		close(fd);
   	}

   	close(fd);

   	// Object instances
   	bRegAcc regObject{};			// Basic Register Access class(bRegAcc)
   	
   	uint32_t RdValue{0};			// tmp variable for the regObject.getReadValue() return value


    	//Setup the OCR_DT_TRANS_CFG_OFFSET polling in order to check when a new data transfer has to start
    	regObject.setCmd("rd");
    	regObject.setAddr((uint32_t)(H2F_AXI_BASE+FPGA2HPS_FIFO_CSR_EVENT_OFFSET));
   	regObject.ExeRdWr(false, pMemMap_v);
   	RdValue= regObject.getReadValue();
   	
   	//std::cout << "deinit\n";
   	// deinit
   	munmap((void*) pMemMap_v, MAP_SIZE);
   	
    	//if ((RdValue & FIFO_FULL_MASK)==1){
    	if (RdValue & FIFO_FULL_MASK){
      		printf("FIFO ip full event detected. Event reg value= %u\n",RdValue);
      		return true;
    	} else {
      		return false;
    	}
  /*
  uint32_t RdValue{9};
  uint32_t tmpMask=RdValue & FIFO_FULL_MASK;
  uint32_t tmpMask_n=RdValue & ~FIFO_FULL_MASK;
  
  std::cout << "RdValue & FIFO_FULL_MASK= "<< tmpMask<< std::endl;
  std::cout << "RdValue & ~FIFO_FULL_MASK= "<< tmpMask_n<< std::endl;
  
  if ((RdValue & FIFO_FULL_MASK)==1){
    printf("FIFO ip full event detected. Event reg value= %u\n",RdValue);
    return true;
  } else {
    return false;
  }
*/
}



// argument help function
std::string printHelp() {
    std::string msg="fpga2hpsfifoApp nr_samples(h) file2savePath/Name(s) source_speed(h)";
    return msg;
}

bool checkArgs(int argc, char *argv[]){
       // Arguments check
    if ((argc < 3) || (std::string(argv[1]) == "-h")|| (std::string(argv[1]) == "--h") || (argc > 4)) {
      std::cout << printHelp() << std::endl;
    } else {
      return true; 
    }
    return false;
}
int main (int argc, char **argv) {
    
    int fd{};           						 // Kernel module file descriptor

    
    std::ofstream myfile;                                                // Application space file variable definition

    size_t readByte{0};
    size_t tmpReadByte{0};    	                                        // I had to place a tmpReadByte because the read action can return also 0 even if there are still samples to fetch. 
                                                                        // Checking that allows me to avoid the unwanted read_buf index shift.
    size_t Byte2rd{0};                                                  // Maybe it is not necessary. I place it to compare two variables af the same type in the while loop below.
        
    if (checkArgs(argc, argv)) {

 
        uint32_t nSamples;
        std::istringstream buffer(argv[1]);
	buffer >> std::hex >> nSamples;
	
	uint32_t sourceSpeed;
	std::istringstream buffer2(argv[3]);
	buffer2 >> std::hex >> sourceSpeed;
	// it can be done better/safer the source speed selection
	std::string tmpStr= "bregacc wr 0xFF200000 0x0000008" + std::to_string(sourceSpeed) +" 0x000000FF";
	//std::cout <<tmpStr << std::endl;

	// I had to increase more than the nSamples the read_buf array size because the last read action can fetch more than the remaining samples resulting in a segmentation fault.
	// With the write action to file (see below) only the wanted amount of samples is stored.
	//uint32_t read_buf[4*nSamples];                                     
	uint32_t *read_buf = (uint32_t*) malloc(4*nSamples* sizeof(*read_buf));                                     	

    	// Open the driver
        fd = open("/dev/fpga2hps_fifo", (O_RDWR));
        // Check that the opening was successful
        if (fd < 0)
        {
          std::cout << "Error: Failed to open fpga2hps_fifo driver file!\n";
          return 0;
        }
        
        // Open the storage file
        myfile.open(std::string(argv[2]), std::ios::out|std::ios::binary);
       
        // Release the fifo reset and select the 125KHz data source
        //(void)  ((int) system( (const char*) "bregacc wr 0xFF200000 0x00000085 0x000000FF"));
        (void)  ((int) system(tmpStr.c_str()));
    
    
/*       // Read till the number of samples are stored in the file
       do{
         readByte+=read(fd, &read_buf[readByte>> 2], nSamples * 4);
       }while(readByte < nSamples * 4);
       
*/

        tmpReadByte=0;
        Byte2rd= (size_t) (nSamples * 4);         
        //printf("Byte2rd= %lu\n",Byte2rd);
        while(readByte < Byte2rd)
        {
          tmpReadByte=read(fd, &read_buf[readByte>> 2], nSamples * 4); 
         
          //printf("tmpReadByte= %lu\n",tmpReadByte);     
          //printf("readByte>> 2= %lu\n",readByte>> 2);     
          //printf("readByte= %lu\n",readByte);
                  
          if (tmpReadByte > 0) {
           readByte = readByte + tmpReadByte;           
          }
          //printf("readByte= %lu\n\n",readByte);
          
          ////////////////////////////////////////////////////////////////////////////////////
          // Check for full condition
          if (Check4Full()) {
            break;
          }
          ///////////////////////////////////////////////////////////////////////////////////
        }
        
       // Stop the source and reset the fifo IP
       //(void)  ((int) system( (const char*) "bregacc wr 0xFF200000 0x00000000 0x0000000F"));
       (void)  ((int) system( (const char*) "bregacc wr 0xFF200000 0x00000000 0x000000FF"));
       
       myfile.write((char *) read_buf,nSamples * 4);
    
       //// set the fifo reset
       //(void)  ((int) system( (const char*) "bregacc wr 0xFF200000 0x00000000 0x00000080"));
  
       // Close the storage file  
       myfile.close();
    
       // Close the driver
       close(fd);
       
       // free read_buf
       free(read_buf);
       
       printf("%d samples saved in the file\n ", nSamples);
    }
    
    return 0;
}
