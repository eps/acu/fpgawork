TIMERIRQAPP_VERSION = 0.1
TIMERIRQAPP_SITE = $(BR2_EXTERNAL_ACUKDT_PATH)/package/userapps/timerirqapp
TIMERIRQAPP_SITE_METHOD = local
TIMERIRQAPP_LICENSE = MIT
TIMERIRQAPP_LICENSE_FILES = COPYING


#define TIMERIRQAPP_BUILD_CMDS
#	$(MAKE) $(TARGET_CONFIGURE_OPTS) -C $(@D) all
#endef

define TIMERIRQAPP_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/timerirqapp $(TARGET_DIR)/usr/bin
endef

#$(eval $(generic-package))
$(eval $(cmake-package))

