# ACU_KDRIVERTEST

This project folder contains FW and SW designs developed to learn and test Linux kernel drivers on a CyclonV SoC.

Please don't take any solution here implemented as gold. There are several things that can be improved/be done better than what shown here.


>The topic is really huge and not so easy, so there could be omitting, partial info and errors. In case you find one (or even more), please get in contact with Davide Rodomonti (d.rodomonti@gsi.de) or Tim Kruse (T.Kruse@gsi.de). 

[[_TOC_]]
<!-- 
- [ACU Kernel Driver Test project](# acu_kdrivertest)
  - [Agenda](#agenda)
    - [FW implementation.](#fw_implementation)
    - [Kernel drivers.](#kernel-drivers)
      - [Timer irq.](#timer-irq)
      - [FPGA to HPS FIFO.](#fpga-to-hps-fifo)
      - [HPS to FPGA FIFO.](#hps-to-fpga-fifo)
      - [GPIO IN irq.](#gpio-in-irq)
    - [Application SWs.](#application-sws)
      - [Timer irq.](#timer-irq)
      - [FPGA to HPS FIFO.](#fpga-to-hps-fifo)
        - [FPGA to HPS FIFO stress test.](#fpga-to-hps-fifo-stress-test)
      - [HPS to FPGA FIFO.](#hps-to-fpga-fifo)
        - [HPS to FPGA FIFO stress test.](#hps to fpga-fifo-stress-test)
      - [GPIO IN irq.](#gpio-in-irq)
    - [Open points.](#open-points) -->

## FW implementation.

The block diagram below shows all the components implemented in the ACU_KDRIVERTEST project. All of them, except the Timer irq one, are altera IPs.
![FW_BlockDiagram](doc/ACU_KdriverTest_Diagram.JPG)
It is possible to distinguish two separate areas from the diagram above: the FPGA one on the left and the HPS one on the right.

The components with an higher performance request (the two FIFOs) are connected to the HPS via the h2f_axi_master bus, while the others are connected to the HPS using the h2f_lw_axi_master bus.

The single components are described below in a top down sequence:
  
  1) GPIO-IN (0xFF200020-0xFF20002F): it is a simple 8b register that takes the "status" signals from the FPGA side and report them on the HPS one. 

   	 The idea is to attach here the alarm coming from a _payload checker_ module, generate an interrupt when the alarm is active (high) and send the content of the register to the application SW.
	 
	 On the interrupt detection it could be also possible to stop/clear a test that involves one or more other components in the design (the FIFOs for example).

  2) SysID (0xFF201110-0xFF20111F): system identification register. It contains the following 32b value: 0x1a2b3c4d.
  3) GPIO-OUT (0xFF200000-0xFF20001F): this is a 32b register used to set "commands" from the HPS to the FPGA side. The 32b drive the following signals:
      - [31..8] Dt2FIFO (24b)-> this is a data value that can be pushed into the FPGA2HPS FIFO. It is one of the 7 possible FPGA2HPS FIFO input sources.
      - [7]     FIFO reset-> FIFO reset signal active low used for both FIFOs in the design.
      - [6]     n.u.
      - [5]     Payload alarm reset -> Payload alarm checker module reset signal (active high).
      - [4]     NewDt2FIFO -> The toggling of this bit from 0 to 1, pushes the Dt2FIFO into the FPGA2HPS FIFO (when the Dt2FIFO signal is selected as FPGA2HPS FIFO input).
      - [3..0]  Dt2FIFO_Sel -> FPGA2HPS FIFO input selector:
        - 0x0 -> No data is pushed into the FIFO.
        - 0x1 -> 2Msps free running counter.
        - 0x2 -> 1Msps free running counter.
        - 0x3 -> 500Ksps free running counter.
        - 0x4 -> 250Ksps free running counter.
        - 0x5 -> 125Ksps free running counter.
  	    - 0x6 -> No data is pushed into the FIFO.
        - 0x7 -> Dt2FIFO is pushed into the FIFO.
  
  4) FPGA2HPS FIFO (0xC0000000-0xC0000001F;0xC0000020-0xC00000023): This FIFO is used to send a data stream from the FPGA side to the HPS one. It is a dual clock FIFO written by the FPGA with the data source speed (up to 2MHz) and read by the HPS. In the expected behavior, the HPS is able to read this FIFO fast enough to avoid any missing sample.
	 The address range is split in two regions: the control status register _csr_ (0xC0000000-0xC0000001F) [click here for more info](https://www.intel.com/content/www/us/en/docs/programmable/683130/24-3/software-control.html) and the data one (0xC0000020-0xC00000023).
	 This FIFO generates an interrupt on the almost full event used by the __fpga2hps_fifo__ kernel driver to start the read action (more details on the __fpga2hps_fifo__ driver chapter). 

  5) HPS2FPGA FIFO (0xC0000040-0xC0000005F;0xC0000060-0xC00000063): This FIFO is used to send a data stream from the user application SW, through the HPS, to the FPGA side. It is a dual clock FIFO written by the HPS  and read by the FPGA with a data rate up to 2MHz. In the expected behavior, the HPS is able to write this FIFO fast enough to avoid any missing sample with a fixed data rate.
	 The address range is split in two regions: the control status register _csr_ (0xC0000040-0xC0000005F) [click here for more info](https://www.intel.com/content/www/us/en/docs/programmable/683130/24-3/software-control.html) and the data one (0xC0000060-0xC00000063).

## Kernel drivers

### Timer irq
The [_timer_irq.c_](https://git.gsi.de/eps/acu/buildroot-eps/-/blob/main/package/drivers/timer_irq/timer_irq.c?ref_type=heads) driver was already described in the [StepByStep](https://git.gsi.de/eps/acu/wiki/-/blob/main/HowTo/StepByStep.md?ref_type=heads) wiki chapter.

### FPGA to HPS FIFO
The [_fpga2hps_fifo.c_](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/package/drivers/fpga2hps_fifo/fpga2hps_fifo.c?ref_type=heads) driver has the following tasks:
1) Configure the FIFO _csr_ register in order to generate an interrupt on the almost full event. This step is accomplished in the _probe_ function.
2) Read the FIFO content on the interrupt and push the data fetched on a KFIFO interfaced by the _misc_ framework as well. This step is accomplished in the _isr_.
3) Define a _misc_ read function used to transfer the samples from the KFIFO to the user application space.
4) Clear the KFIFO on the _misc_ release function.  
   
It is possible to identify 6 regions in the driver:

1) Platform device structure:
	
   In the [_fpga2hps_fifo.h_](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/package/drivers/fpga2hps_fifo/fpga2hps_fifo.h?ref_type=heads) header file is defined the platform device structure:

   ```c
   	struct fpa2hps_fifo_dev
	{
    	struct miscdevice miscdev;
    	int irqnum;
    
    	spinlock_t lock;
    
    	altera_fifo_csr_t __iomem *fifo_csr;
    	__u32 __iomem *fifo_data;
	};
   ```
   It collects the _misc_ framework structure, the interrupt number, the spinlock variable used to make the _isr_ atomic, a pointer to a nested [altera](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/package/drivers/fpga2hps_fifo/altera_fifo.h?ref_type=heads) data structure describing the _FIFO csr_ and a pointer to the FIFO data address.

2) Attributes:
   
   For each _csr_ field there is one(if the field is read only) or two attributes defined. These attributes are useful during the driver debug action. 
   
   There is an extra attribute _kfifo_status_ that shows the amount of available bits in the KFIFO.
3) Misc functions:
   
   There are three _misc_ functions:
	- _fpga2hps_fifo_open_: There is not so much here to do.
	- _fpga2hps_fifo_read_ : In this function the data inside the KFIFO are sent to the user application space.
	- _fpga2hps_fifo_release_ : Here the KFIFO content is flushed in order to have consistent data transferred between two consecutive transmissions. 
                                
	NOTE: Without this clean action, the data received  on a second/third transmission could have some residual samples of the previous one.
	The driver has no idea of which transmission is ongoing or which source data rate is selected. All this actions are managed by the application SW.  

4) Interrupt Service Routine (isr):
   
   The interrupt service routine is executed when the number of fpga2hps FIFO used locations is higher than the _almost full_ threshold (set by the probe function to 1024).
   
   The interrupt is cleared at the end as usual for the isr functions.
   
   All the actions inside this function are atomic, due to the spinlock set/unset:
   
   ```c
   	static irq_handler_t fpga2hps_fifo_irq_isr(int irq, void *dev_id, struct pt_regs *regs){
		struct fpa2hps_fifo_dev *dev = (struct fpa2hps_fifo_dev*) dev_id;
	
		 long flags;
		__u32 sample =0;
	
		//Locking
		pr_debug("%s: Spin locking \n", __PRETTY_FUNCTION__);	
		spin_lock_irqsave(&(dev->lock), flags);
		:
		:
		//Unlocking
        pr_debug("%s: Spin Unlocking \n", __PRETTY_FUNCTION__);	
		spin_unlock_irqrestore(&(dev->lock), flags);

		// pr_debug("%s(irq: %d)\n", __PRETTY_FUNCTION__, irq);
		return (irq_handler_t) IRQ_HANDLED;
	}
   ```
   The idea is to read the fpga2hps FIFO _fill level_ register and perform the same amount of read actions.
   Each read value is then pushed into the KFIFO.

   The problem is that the write speed can be so fast that at the end of the isr the FIFO _fill level_ is already over the almost full threshold.
   In this case we have another interrupt active without having yet cleared the one under execution. 
   The interrupt clearing action will be not referred to the interrupt just served, but to the latest one appeared even if it was never handled.
   
   It is not possible to increase too much the almost full threshold otherwise the fpga2hps fifo gets full.

   One possible solution consists to keep on going with the fpga2hps FIFO read at the end of the isr if, before clearing the interrupt, there are already an amount of samples (_stayThrs_) available.

   ```c
		// for debug purpose only
		__u32 stayThrs =(__u32) ioread32(&(dev->fifo_csr->almost_empty_threshold));
	
		__u32 rd_chunkSize = (__u32) ioread32(&(dev->fifo_csr->fill_level));
		pr_debug("%s: fill level @ isr start= (%d) \n", __PRETTY_FUNCTION__, rd_chunkSize);
	
		while (rd_chunkSize > stayThrs) {
			if(kfifo_avail(&samples_fifo) > 0) {
		
				for (size_t i=0; i<rd_chunkSize;i++){
					// read from fpga2hps FIFO
					sample = ioread32(&(dev->fifo_data)[0]);
				  
					// write to KFIFO
					kfifo_put(&samples_fifo, sample);
					//pr_debug("%s: new sample (%d) in fifo (%d)\n", __PRETTY_FUNCTION__, sample, kfifo_len(&samples_fifo)); 
				}
		
			} else {
				pr_err("%s: fifo full. Sample (%d) dropped\n", __PRETTY_FUNCTION__, sample);
				break;
			}
		
			rd_chunkSize = (__u32) ioread32(&(dev->fifo_csr->fill_level));
			//pr_debug("%s: fill level @ isr while loop= (%d) \n", __PRETTY_FUNCTION__, rd_chunkSize);
		}

   ```
   Please note that the _almost_empty_threshold_ register is used here (not in a proper way) to store the _stayThrs_ value.


5) Probe function
   
   In the probe function we have the following actions executed:
   - The platform device structure memory allocation
    ```c
		// Allocate space to store the device structure
		dev = devm_kzalloc(&pdev->dev, sizeof(struct fpa2hps_fifo_dev), GFP_KERNEL);
	``` 
   - The resource information fetched from the [device tree](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/board/mcvevp/ACU_KdriverTest.dts?ref_type=heads) *by name*.
    ```c

		// retrieve resource information from the device tree by name: memory base address and length
		// fifo_csr
		r = platform_get_resource_byname(pdev, IORESOURCE_MEM, "fifo-csr");
		if (!r) {
			pr_err("%s: Failed to get fifo-csr resources\n", __PRETTY_FUNCTION__);
			return -EINVAL;	
		}
		dev->fifo_csr = devm_ioremap_resource(&pdev->dev, r);
		if (!dev->fifo_csr) {
			pr_err("%s: Failed to remap fifo-csr config space\n", __PRETTY_FUNCTION__);
			return -ENOMEM;
		}
		
		// fifo_data
		r = platform_get_resource_byname(pdev, IORESOURCE_MEM, "fifo-data");
		if (!r) {
			pr_err("%s: Failed to get fifo-data resources\n", __PRETTY_FUNCTION__);
			return -EINVAL;	
		}
		dev->fifo_data = devm_ioremap_resource(&pdev->dev, r);
		if (!dev->fifo_data) {
			pr_err("%s: Failed to remap fifo-data config space\n", __PRETTY_FUNCTION__);
			return -ENOMEM;
		}
    ```
	Please note that this method is very efficient for those devices with spread address memory space. 

  - The interrupt number fetched always from the device tree and the link to the _isr_
    ```c
		// retrieve the interrupt number from the device tree 				   
		dev->irqnum = platform_get_irq(pdev, 0);
		if (dev->irqnum < 0) {
		  pr_err("%s: platform_get_irq failed  = %d)\n", __PRETTY_FUNCTION__, dev->irqnum);
		  return -ENXIO;		
		} 
		// link the interrupt to the interrupt service routine
		status = request_irq(dev->irqnum, (irq_handler_t)fpga2hps_fifo_irq_isr, 0, DEVNAME, dev);
		if(status == 0) {
			pr_debug("%s: IRQ [%d] registered\n", __PRETTY_FUNCTION__, dev->irqnum);
		}else{
			pr_err("%s: IRQ failed to register (error = %d)\n", __PRETTY_FUNCTION__, status);
		}

    ```
  - The spinlock initialization
    ```c
     	// Initialize the spinlock
 		spin_lock_init(&(dev->lock)); 
    ```

  - The misc structure populated and registered
    ```c
	    //Populate the misc structure items 
		dev->miscdev.minor = MISC_DYNAMIC_MINOR;
		dev->miscdev.name = DEVNAME;
		dev->miscdev.fops = &fops;

		// create a misc device
		status = misc_register(&dev->miscdev);

    ```
  - The KFIFO initialization  
	```c
    	INIT_KFIFO(samples_fifo);
    ```
  - Finally, if no errors were detected in the steps above, the almost full/empty threshold and the interrupt enable are set.
	```c
		// Set the almost full threshold to 1024
		iowrite32((__u32) THRS_ALMOSTFULL, &(dev->fifo_csr->almost_full_threshold));
		// Set the almost empty threshold to 10
		// NOTE: the almost empty threshold value is used here not to generate an interrupt but to keep the driver more time inside the isr.
		iowrite32((__u32) THRS_ALMOSTEMPTY, &(dev->fifo_csr->almost_empty_threshold));

		// Enable the interlock on ALMOST FULL bit
 		iowrite32((__u32) ALMOSTFULL, &(dev->fifo_csr->interrupt_enable));
 		// Clear the event bit
 		iowrite32(255, &(dev->fifo_csr->event));

		pr_info("%s: probe successful\n", __PRETTY_FUNCTION__);
    ```
6) Remove function
   
   The remove function is executed when a kernel module is "un-installed". 
   For this reason it is possible to find inside all the un-registration, un-mapping, etc..
   
   Please note that the interrupt is disabled. 
   ```c  
	static int fpga2hps_fifo_remove(struct platform_device *pdev){
		// no need to free any resources manually here
		// it's done by the kernel (managed resources / devm_*)
		struct fpa2hps_fifo_dev *dev = (struct fpa2hps_fifo_dev*) platform_get_drvdata(pdev);
        // Disable the interlock on almost full bit
		iowrite32(0, &(dev->fifo_csr->interrupt_enable));
        free_irq(dev->irqnum, dev);
		misc_deregister(&dev->miscdev);

		return 0;
	}

   ```

### HPS to FPGA FIFO
The [_hps2fpga_fifo.c_](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/package/drivers/hps2fpga_fifo/hps2fpga_fifo.c?ref_type=heads) driver has the following tasks:

1) Define a _misc_ write function used to transfer a file from the user application space through the KFIFO to the hps2fpga FIFO altera IP.
2) Clear the KFIFO on the _misc_ release function.  
   
It is possible to identify 5 regions in the driver:

1) Platform device structure:
	
   It has the same platform device structure used already in the _fpga2hps_fifo.c_ kernel driver (we are interfacing always a FIFO IP at the end!) even if not all the struct members (like irqnr) are used in the driver.

2) Attributes:
   
   For each _csr_ field there is one(if the field is read only) or two attributes defined. These attributes are useful during the driver debug action. 
   
   There are extra attributes relative to the KFIFO:
   * kfifo_avail gives you ne number of 32b locations available. 
   * kfifo_size  gives you back the overall number of locations (used and not used).
   * kfifo_len gives you back the number of used locations.
   * kfifo_element pushes values in/out the KFIFO.

3) Misc functions:
   
   There are three _misc_ functions:
	- _fpga2hps_fifo_open_: There is not so much here to do.
	- _fpga2hps_fifo_write_ : In this function the data from the user application space if first pushed into the KFIFO and afterward are sent to the hps2fpga FIFO altera IP.
	- _fpga2hps_fifo_release_ : Here the KFIFO content is flushed in order to have consistent data transferred between two consecutive transmissions. 
                                
	NOTE: Without this clean action, the data received  on a second/third transmission could have some residual samples of the previous one.
	The driver has no idea of which transmission is ongoing or which source data rate is selected. All this actions are managed by the application SW.  

4) Probe function
   
   In the probe function we have the following actions executed:
   - The platform device structure memory allocation
    ```c
		// Allocate space to store the device structure
		dev = devm_kzalloc(&pdev->dev, sizeof(struct hps2fpga_fifo_dev), GFP_KERNEL);
	``` 
   - The resource information fetched from the [device tree](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/board/mcvevp/ACU_KdriverTest.dts?ref_type=heads) *by name*.
    ```c

		// retrieve resource information from the device tree by name: memory base address and length
		// fifo_csr
		r = platform_get_resource_byname(pdev, IORESOURCE_MEM, "fifo-csr");
		if (!r) {
			pr_err("%s: Failed to get fifo-csr resources\n", __PRETTY_FUNCTION__);
			return -EINVAL;	
		}
		dev->fifo_csr = devm_ioremap_resource(&pdev->dev, r);
		if (!dev->fifo_csr) {
			pr_err("%s: Failed to remap fifo-csr config space\n", __PRETTY_FUNCTION__);
			return -ENOMEM;
		}
		
		// fifo_data
		r = platform_get_resource_byname(pdev, IORESOURCE_MEM, "fifo-data");
		if (!r) {
			pr_err("%s: Failed to get fifo-data resources\n", __PRETTY_FUNCTION__);
			return -EINVAL;	
		}
		dev->fifo_data = devm_ioremap_resource(&pdev->dev, r);
		if (!dev->fifo_data) {
			pr_err("%s: Failed to remap fifo-data config space\n", __PRETTY_FUNCTION__);
			return -ENOMEM;
		}
    ```
	Please note that this method is very efficient for those devices with spread address memory space. 

  - The interrupt number fetched always from the device tree and the link to the _isr_
    ```c
		// retrieve the interrupt number from the device tree 				   
		dev->irqnum = platform_get_irq(pdev, 0);
		if (dev->irqnum < 0) {
		  pr_err("%s: platform_get_irq failed  = %d)\n", __PRETTY_FUNCTION__, dev->irqnum);
		  return -ENXIO;		
		} 
		// link the interrupt to the interrupt service routine
		status = request_irq(dev->irqnum, (irq_handler_t)fpga2hps_fifo_irq_isr, 0, DEVNAME, dev);
		if(status == 0) {
			pr_debug("%s: IRQ [%d] registered\n", __PRETTY_FUNCTION__, dev->irqnum);
		}else{
			pr_err("%s: IRQ failed to register (error = %d)\n", __PRETTY_FUNCTION__, status);
		}

    ```

  - The misc structure populated and registered
    ```c
	    //Populate the misc structure items 
		dev->miscdev.minor = MISC_DYNAMIC_MINOR;
		dev->miscdev.name = DEVNAME;
		dev->miscdev.fops = &fops;

		// create a misc device
		status = misc_register(&dev->miscdev);

    ```
  - The KFIFO initialization  
	```c
    	INIT_KFIFO(samples_fifo);
    ```
5) Remove function
   
   The remove function is executed when a kernel module is "un-installed". 
   For this reason it is possible to find inside all the un-registration, un-mapping, etc..
   
   ```c  
	static int hps2fpga_fifo_remove(struct platform_device *pdev){
	  // no need to free any resources manually here
	  // it's done by the kernel (managed resources / devm_*)
	  struct hps2fpga_fifo_dev *dev = (struct hps2fpga_fifo_dev*) platform_get_drvdata(pdev);
	  misc_deregister(&dev->miscdev);

	  return 0;
    }

   ```
### GPIO IN irq
_TO BE DEVELOPED_

## Application SWs

### Timer irq
The [_timerirqapp.cpp_](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/package/userapps/timerirqapp/timerirqApp.cpp?ref_type=heads) application SW was already described in the [StepByStep](https://git.gsi.de/eps/acu/wiki/-/blob/main/HowTo/StepByStep.md?ref_type=heads) wiki chapter.

### FPGA to HPS FIFO
The [_fpga2hpsfifoapp.cpp_](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/package/userapps/fpga2hpsfifoapp/fpga2hpsfifoApp.cpp?ref_type=heads) application SW needs 3 arguments:
   1) The umber of samples to read/store (in hex).
   2) The file name where the samples are stored (as string).
   3) The source speed (in hex) selectable from the one described in the FW implementation above.

The concept sequence inside this application SW is:
   1) Call the misc _open_ function to access to the shared KFIFO.
   2) Open the file where the samples are collected.
   3) Release the FIFO reset and select the source speed.
   4) Call recursively the misc _read_ function till the received amount of samples are the same as the requested (via argument) one or till a FPGA2HPS_FIFO full condition is detected.
   5) Remove the FIFO input selection set in the step 3 above and put the FIFO under reset.
   6) Write the fetched samples to the "store" file.
   7) Close the store file.
   8) Call the misc _release_ function to flush the KFIFO.

The worst case scenario succesfully tested was with 2MS to sore and a source @ 2MSps.

```bash
	fpga2hpsfifoapp 200000 tmpTest.txt 1 #2097152 samples / 8388608 B
```

#### FPGA to HPS FIFO stress test
The idea behind the [_fpga2hpsfifost.cpp_](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/package/stresstest/fpga2hpsfifost/fpga2hpsfifoST.cpp?ref_type=heads) application SW is to run the _fpga2hpsfifoapp.cpp_ application program for a configurable number of time checking at the end of each iteration the execution result (if the fpga2hps FIFO detected a full event).

It requires three arguments:
   1) The application sw to run with its arguments (as string).
   2) The number of iterations (as dec).
   3) _true/false_ boolean variable to store a sample data file for each iteration or not.

NOTE: Due to the storege partition seting at the moment of the first test, not more than 2 files 8MB each can be saved, so for iteration number values higher than 2, please set the boolean variable to false.

The worst case scenario succesfully tested was with 1000 iterations.

```bash 
	fpga2hpsfifost "fpga2hpsfifoapp 200000 tmpTest.txt 1" 1000 false
```
NOTE: To force an error to be detected, and stop the stress test execution, it is possible to set the _almost full threshold_ to 8112 (d).
      
ex:

	```bash 
		fpga2hpsfifost "fpga2hpsfifoapp 200000 tmpTest.txt 1" 1000 false &
		# Wait some iterations
		bregacc wr 0xc0000010 0x00001f71 0x0000ffff   #8112
	```	     

   After changing the threshold it is expected that a full condition is detected and that the stress test stops.

### HPS to FPGA FIFO
The [_hps2fpgafifoapp.cpp_](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/package/userapps/hps2fpgafifoapp/hps2fpgafifoApp.cpp?ref_type=heads) application SW needs 2 arguments:
   1) The file name to send (as string).
   3) The read speed (in hex) selectable from the one described in the FW implementation above.
   
```bash 
hps2fpgafifoapp tmpFileH2F_2MS.txt 1
```

This application SW sends a file in 8192*4 Byte chunks to the KFIFO implemented in the [hps2fpga kernel driver](package/drivers/hps2fpga_fifo/hps2fpga_fifo.c).

The file is divided into chunks and sent every time the _hps2fpga_ FIFO Altera IP is empty/almost empty. If the FIFO is not empty/almost empty, the program waits 50 us and checks the status again. In 50 us, up to 100 read actions can take place considering the 500ns period of the fastest data rate on FPGA side.

The correct data transfer is evaluated with two steps:

   * Checking for _hps2fpga_ FIFO Altera IP **full** condition.
   * Checking the data transferred.

The first step is independent from the data file content. The _hps2fpga_ FIFO Altera IP should never get full. This means that the application SW can't write too fast compared to the read action performed on the FPGA side.

The second step is instead data dependent and it is valid for debug purposes only. The idea is to store 32b symbols generated by a counter. This allows us to check the data stream knowing that the difference between two read symbols has to be 1. In the FPGA side there is a vhdl module that performs the delta between two read symbols (_PayloadChecker.vhd_). If the difference is not 1 a latched alarm is generated and an alarm counter is incremented. The latched alarm, the alarm counter overflow and the counter alarm value are readable from GPIO input register at address 0xFF200020.

The test strategy used consists of sending a 8MB file containing 2M counter symbols and enabling the fastest read speed (2MHz) on the FPGA side.

At the end of the data transfer, no alarms were detected in the _payload alarm_ register and the _hps2fpga_ FIFO Altera IP was not getting full.

To prove that the SW was really checking and detecting the alarms/errors mentioned above, the following tests were performed:

* The _hps2fpga_ FIFO Altera IP was prefilled with random values (using the following command 4 times _**dd if=/dev/random of=/dev/hps2fpga_fifo bs=8192 count=1**_) and from the application SW code, the FIFO IP event register clear action was commented out (lines 172 to 174). This generated a FIFO full event detected and reported by the program.
* From the same file used in the "main" test, three different files were generated where the symbol/s value was manually corrupted. The first file was with the first symbol corrupted, the second file with the last one corrupted and the third file with more than 16 symbols corrupted in order to trigger the overflow alarm.

The results were in line with the expected one.

#### HPS to FPGA FIFO stress test
The idea behind the [_hps2fpgafifost.cpp_](https://git.gsi.de/eps/acu/fpgawork/-/blob/main/prj/ACU_KdriverTest/buildroot/package/stresstest/hps2fpgafifost/hps2fpgafifoST.cpp?ref_type=heads) application SW is to run the _hps2fpgafifoapp.cpp_ application program for a configurable number of time checking at each iteration for alarms.

It requires two arguments:
   1) The application sw to run with its arguments (as string).
   2) The number of iterations (as dec).
   
The worst case scenario succesfully tested was with 250 iterations.
   
```bash 
hps2fpgafifost "hps2fpgafifoapp tmpFileH2F_2MS.txt 1" 100
```

### GPIO IN irq
_TO BE DEVELOPED_

## Open points

1) (@ fpga2hpsfifo kdriver): Try to flush the KFIFO with a KINIT instead of a for loop read.
2) (@ fpga2hpsfifo kdriver):
   One (or maybe all) of the pr_debug commented in the isr extract below made (maybe) the driver working at 2Msps.
   ```c
		// for debug purpose only
		__u32 stayThrs =(__u32) ioread32(&(dev->fifo_csr->almost_empty_threshold));
	
		__u32 rd_chunkSize = (__u32) ioread32(&(dev->fifo_csr->fill_level));
		pr_debug("%s: fill level @ isr start= (%d) \n", __PRETTY_FUNCTION__, rd_chunkSize);
	
		while (rd_chunkSize > stayThrs) {
			if(kfifo_avail(&samples_fifo) > 0) {
		
				for (size_t i=0; i<rd_chunkSize;i++){
					// read from fpga2hps FIFO
					sample = ioread32(&(dev->fifo_data)[0]);
				  
					// write to KFIFO
					kfifo_put(&samples_fifo, sample);
					//pr_debug("%s: new sample (%d) in fifo (%d)\n", __PRETTY_FUNCTION__, sample, kfifo_len(&samples_fifo)); 
				}
		
			} else {
				pr_err("%s: fifo full. Sample (%d) dropped\n", __PRETTY_FUNCTION__, sample);
				break;
			}
		
			rd_chunkSize = (__u32) ioread32(&(dev->fifo_csr->fill_level));
			//pr_debug("%s: fill level @ isr while loop= (%d) \n", __PRETTY_FUNCTION__, rd_chunkSize);
		}

   ```
3) In the _fpga2hpsfifost.cpp_ there is a while(1) loop that waits the conclusion of the fpga2hpsfifoapp program checking the generation of the data file. This sems to work, but it is not the safiest and nicer solution ever.
4) How can I be sure that the kfifo read and write actions are executed in parallel?
5) How could be explained the _dmesg_ print out with the isr prints interleaved with the read ones? Is it because of the dual core ARM or because the spinlok is not working?
6) (@ fpga2hpsfifo kdriver): Think to rewrite the isr with the bottom half strategy suggested by Tim.
7) (@ fpga2hpsfifo kdriver): Think to reduce the KFIFO size to 8192 or even 4096 locations.
8) (@ hps2fpgafifo overall): The data transfer performed with this SW/kernel driver is not continuous and it takes quite a bit of time. The read action in the FPGA is performed only when there is something to read in the _hps2fpga_ FIFO Altera IP. The write action is performed by the hps only when the _hps2fpga_ FIFO Altera IP is empty. This generates gaps in the data transfer on the FPGA side that would be better to remove.
9) (@ hps2fpgafifo kdriver): a KFIFO is used as interface between the misc write function and the _hps2fpga_ FIFO Altera IP. It has not so much sense in the misc write function to write first the KFIFO and in the same iteration read it and push the value in the _hps2fpga_ FIFO Altera IP. Maybe it would be possible to write directly the _hps2fpga_ FIFO Altera IP removing the KFIFO.
10) (@ hps2fpgafifo app SW): The 50us "wait for empty" sleep time could be maybe reduced to reduce the data transfer period.
   
