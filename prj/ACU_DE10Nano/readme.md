# ACU_DE10Nano

The project inside this folder is compliant with the [Terasic DE10-nano development board](https://www.terasic.com.tw/cgi-bin/page/archive.pl?Language=English&CategoryNo=205&No=1046&PartNo=1#contents) HW. 

This evaluation board is used to develop FW and SW (both k-drivers and apps) modules that will be reversed into the [mcv Aries board](https://www.aries-embedded.com/system-on-module/fpga/cyclone-v-intel-fpga-mcv-som-hps-altera-soc-pcie-transceiver) (after the small necessary adaptations).

[[_TOC_]]
<!-- 
- [ACU DE10-Nano project](# acu_de10nano)
  - [Agenda](#agenda)
    - [FW implementation.](#fw_implementation)
    - [Kernel drivers.](#kernel-drivers)
    - [Application SWs.](#application-sws)
    - [Open points.](#open-points) -->

## FW implementation.
_**TO BE DEVELOPED**_

## Kernel drivers


## Application SWs


## Open points

