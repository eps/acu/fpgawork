###############################################################################
# PinAssignment.tcl
# ==========================
#
# BOARD : MCV Aries Embedded
# Author : Davide Rodomonti
# Revision : 1.0
# Last updated : 2020-04-07
#
# Syntax Rule : GROUP_NAME_N[bit]
#
# GROUP  : specify a particular interface (ex: SDR_)
# NAME   : signal name (ex: CONFIG, D, ...)
# bit    : signal index
# _N     : to specify an active-low signal
#
# You can run this script from Quartus by observing the following steps:
# 1. Place this TCL script in your project directory
# 2. Open your project in Quartus
# 3. Go to the View > Utility Windows -> Tcl Console
# 4. In the Tcl Console type:
#        source LED_Test_PinAssignment.tcl
#
# 5. The script will assign pins and return an "assignment made" message.
###############################################################################

set_global_assignment -name FAMILY "Cyclone V"
set_global_assignment -name DEVICE 5CSXFC6C6U23C7
##set_global_assignment -name DEVICE_FILTER_PACKAGE FBGA
##set_global_assignment -name DEVICE_FILTER_PIN_COUNT 896
##set_global_assignment -name DEVICE_FILTER_SPEED_GRADE 6


#============================================================
# CLOCK
#============================================================

set_location_assignment PIN_D12 -to Board50MHz_CLK

set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to Board50MHz_CLK

##set_location_assignment PIN_AF14 -to CLOCK_50
##set_location_assignment PIN_AA16 -to CLOCK2_50
##set_location_assignment PIN_Y26  -to CLOCK3_50
##set_location_assignment PIN_K14  -to CLOCK4_50

##set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to CLOCK_50
##set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to CLOCK2_50
##set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to CLOCK3_50
##set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to CLOCK4_50

#============================================================
# Reset
#============================================================

set_location_assignment PIN_AB25 -to PowerUpRST_n

set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to PowerUpRST_n


#============================================================
# HPS
# 		=> hps Direct Driven LED
#============================================================
set_location_assignment PIN_B19 -to hps_io_hps_io_gpio_inst_GPIO65 
set_location_assignment PIN_B16 -to hps_io_hps_io_gpio_inst_GPIO64
set_location_assignment PIN_C19 -to hps_io_hps_io_gpio_inst_GPIO63  
set_location_assignment PIN_C21 -to hps_io_hps_io_gpio_inst_GPIO48  

set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_gpio_inst_GPIO65 
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_gpio_inst_GPIO64
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_gpio_inst_GPIO63
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_gpio_inst_GPIO48

#============================================================
# HPS
# 		=> eMMC
#============================================================
set_location_assignment PIN_D14 -to hps_io_hps_io_sdio_inst_CMD
set_location_assignment PIN_B8  -to hps_io_hps_io_sdio_inst_CLK
set_location_assignment PIN_C13 -to hps_io_hps_io_sdio_inst_D0
set_location_assignment PIN_B6  -to hps_io_hps_io_sdio_inst_D1
set_location_assignment PIN_B11 -to hps_io_hps_io_sdio_inst_D2
set_location_assignment PIN_B9  -to hps_io_hps_io_sdio_inst_D3
set_location_assignment PIN_H13 -to hps_io_hps_io_sdio_inst_D4
set_location_assignment PIN_A4  -to hps_io_hps_io_sdio_inst_D5
set_location_assignment PIN_H12 -to hps_io_hps_io_sdio_inst_D6
set_location_assignment PIN_B4  -to hps_io_hps_io_sdio_inst_D7
                                    

set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_sdio_inst_CMD
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_sdio_inst_CLK
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_sdio_inst_D0
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_sdio_inst_D1
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_sdio_inst_D2
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_sdio_inst_D3
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_sdio_inst_D4
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_sdio_inst_D5
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_sdio_inst_D6
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_sdio_inst_D7

#============================================================
# HPS
# 		=> UART0
#============================================================
set_location_assignment PIN_A22 -to hps_io_hps_io_uart0_inst_RX
set_location_assignment PIN_B21 -to hps_io_hps_io_uart0_inst_TX
                                    

set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_uart0_inst_RX
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_uart0_inst_TX

#============================================================
# UART1
#============================================================
#set_location_assignment PIN_AE12 -to hps_0_uart1_rxd
#set_location_assignment PIN_AD12 -to hps_0_uart1_txd

#set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_0_uart1_rxd
#set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_0_uart1_txd

#============================================================
# HPS
# 		=> USB
#============================================================
set_location_assignment PIN_J16 -to hps_io_hps_io_usb1_inst_NXT
set_location_assignment PIN_A7  -to hps_io_hps_io_usb1_inst_DIR
set_location_assignment PIN_A8  -to hps_io_hps_io_usb1_inst_CLK
set_location_assignment PIN_H16 -to hps_io_hps_io_usb1_inst_STP

set_location_assignment PIN_A16 -to hps_io_hps_io_usb1_inst_D0
set_location_assignment PIN_J14 -to hps_io_hps_io_usb1_inst_D1
set_location_assignment PIN_A15 -to hps_io_hps_io_usb1_inst_D2
set_location_assignment PIN_D17 -to hps_io_hps_io_usb1_inst_D3
set_location_assignment PIN_J13 -to hps_io_hps_io_usb1_inst_D4
set_location_assignment PIN_A12 -to hps_io_hps_io_usb1_inst_D5
set_location_assignment PIN_J12 -to hps_io_hps_io_usb1_inst_D6
set_location_assignment PIN_A11 -to hps_io_hps_io_usb1_inst_D7

set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_NXT
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_DIR
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_CLK
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_STP


set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_D0
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_D1
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_D2
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_D3
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_D4
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_D5
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_D6
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_usb1_inst_D7

#============================================================
# HPS
# 		=> DDR3
#============================================================

set_location_assignment PIN_C28 -to memory_mem_a[0]
#set_location_assignment PIN_C28 -to memory_mem_a_0
set_location_assignment PIN_B28 -to memory_mem_a[1]
#set_location_assignment PIN_B28 -to memory_mem_a_1
set_location_assignment PIN_E26 -to memory_mem_a[2]
#set_location_assignment PIN_E26 -to memory_mem_a_2
set_location_assignment PIN_D26 -to memory_mem_a[3]
#set_location_assignment PIN_D26 -to memory_mem_a_3
set_location_assignment PIN_J21 -to memory_mem_a[4]
#set_location_assignment PIN_J21 -to memory_mem_a_4
set_location_assignment PIN_J20 -to memory_mem_a[5]
#set_location_assignment PIN_J20 -to memory_mem_a_5
set_location_assignment PIN_C26 -to memory_mem_a[6]
#set_location_assignment PIN_C26 -to memory_mem_a_6
set_location_assignment PIN_B26 -to memory_mem_a[7]
#set_location_assignment PIN_B26 -to memory_mem_a_7
set_location_assignment PIN_F26 -to memory_mem_a[8]
#set_location_assignment PIN_F26 -to memory_mem_a_8
set_location_assignment PIN_F25 -to memory_mem_a[9]
#set_location_assignment PIN_F25 -to memory_mem_a_9
set_location_assignment PIN_A24 -to memory_mem_a[10]
#set_location_assignment PIN_A24 -to memory_mem_a_10
set_location_assignment PIN_B24 -to memory_mem_a[11]
#set_location_assignment PIN_B24 -to memory_mem_a_11
set_location_assignment PIN_D24 -to memory_mem_a[12]
#set_location_assignment PIN_D24 -to memory_mem_a_12
set_location_assignment PIN_C24 -to memory_mem_a[13]
#set_location_assignment PIN_C24 -to memory_mem_a_13
set_location_assignment PIN_G23 -to memory_mem_a[14]
#set_location_assignment PIN_G23 -to memory_mem_a_14
set_location_assignment PIN_A27 -to memory_mem_ba[0]
#set_location_assignment PIN_A27 -to memory_mem_ba_0
set_location_assignment PIN_H25 -to memory_mem_ba[1]
#set_location_assignment PIN_H25 -to memory_mem_ba_1
set_location_assignment PIN_G25 -to memory_mem_ba[2]
#set_location_assignment PIN_G25 -to memory_mem_ba_2
set_location_assignment PIN_G28  -to memory_mem_dm[0]
#set_location_assignment PIN_G28  -to memory_mem_dm_0
set_location_assignment PIN_P28  -to memory_mem_dm[1]
#set_location_assignment PIN_P28  -to memory_mem_dm_1
set_location_assignment PIN_W28  -to memory_mem_dm[2]
#set_location_assignment PIN_W28  -to memory_mem_dm_2
set_location_assignment PIN_AB28 -to memory_mem_dm[3]
#set_location_assignment PIN_AB28 -to memory_mem_dm_3
set_location_assignment PIN_J25   -to memory_mem_dq[0] 
#set_location_assignment PIN_J25   -to memory_mem_dq_0
set_location_assignment PIN_J24   -to memory_mem_dq[1]
#set_location_assignment PIN_J24   -to memory_mem_dq_1
set_location_assignment PIN_E28   -to memory_mem_dq[2]
#set_location_assignment PIN_E28   -to memory_mem_dq_2
set_location_assignment PIN_D27   -to memory_mem_dq[3]
#set_location_assignment PIN_D27   -to memory_mem_dq_3
set_location_assignment PIN_J26   -to memory_mem_dq[4]
#set_location_assignment PIN_J26   -to memory_mem_dq_4
set_location_assignment PIN_K26   -to memory_mem_dq[5]
#set_location_assignment PIN_K26   -to memory_mem_dq_5
set_location_assignment PIN_G27   -to memory_mem_dq[6]
#set_location_assignment PIN_G27   -to memory_mem_dq_6
set_location_assignment PIN_F28   -to memory_mem_dq[7]
#set_location_assignment PIN_F28   -to memory_mem_dq_7
set_location_assignment PIN_K25   -to memory_mem_dq[8]
#set_location_assignment PIN_K25   -to memory_mem_dq_8
set_location_assignment PIN_L25   -to memory_mem_dq[9]
#set_location_assignment PIN_L25   -to memory_mem_dq_9
set_location_assignment PIN_J27  -to memory_mem_dq[10]
#set_location_assignment PIN_J27  -to memory_mem_dq_10 
set_location_assignment PIN_J28  -to memory_mem_dq[11]
#set_location_assignment PIN_J28  -to memory_mem_dq_11 
set_location_assignment PIN_M27  -to memory_mem_dq[12]
#set_location_assignment PIN_M27  -to memory_mem_dq_12 
set_location_assignment PIN_M26  -to memory_mem_dq[13]
#set_location_assignment PIN_M26  -to memory_mem_dq_13 
set_location_assignment PIN_M28  -to memory_mem_dq[14]
#set_location_assignment PIN_M28  -to memory_mem_dq_14 
set_location_assignment PIN_N28  -to memory_mem_dq[15]
#set_location_assignment PIN_N28  -to memory_mem_dq_15 
set_location_assignment PIN_N24  -to memory_mem_dq[16]
#set_location_assignment PIN_N24  -to memory_mem_dq_16 
set_location_assignment PIN_N25  -to memory_mem_dq[17]
#set_location_assignment PIN_N25  -to memory_mem_dq_17 
set_location_assignment PIN_T28  -to memory_mem_dq[18]
#set_location_assignment PIN_T28  -to memory_mem_dq_18 
set_location_assignment PIN_U28  -to memory_mem_dq[19]
#set_location_assignment PIN_U28  -to memory_mem_dq_19 
set_location_assignment PIN_N26  -to memory_mem_dq[20]
#set_location_assignment PIN_N26  -to memory_mem_dq_20 
set_location_assignment PIN_N27  -to memory_mem_dq[21]
#set_location_assignment PIN_N27  -to memory_mem_dq_21 
set_location_assignment PIN_R27  -to memory_mem_dq[22]
#set_location_assignment PIN_R27  -to memory_mem_dq_22 
set_location_assignment PIN_V27  -to memory_mem_dq[23]
#set_location_assignment PIN_V27  -to memory_mem_dq_23 
set_location_assignment PIN_R26  -to memory_mem_dq[24]
#set_location_assignment PIN_R26  -to memory_mem_dq_24 
set_location_assignment PIN_R25  -to memory_mem_dq[25]
#set_location_assignment PIN_R25  -to memory_mem_dq_25 
set_location_assignment PIN_AA28 -to memory_mem_dq[26]
#set_location_assignment PIN_AA28 -to memory_mem_dq_26
set_location_assignment PIN_W26  -to memory_mem_dq[27]
#set_location_assignment PIN_W26  -to memory_mem_dq_27 
set_location_assignment PIN_R24  -to memory_mem_dq[28]
#set_location_assignment PIN_R24  -to memory_mem_dq_28 
set_location_assignment PIN_T24  -to memory_mem_dq[29]
#set_location_assignment PIN_T24  -to memory_mem_dq_29 
set_location_assignment PIN_Y27  -to memory_mem_dq[30]
#set_location_assignment PIN_Y27  -to memory_mem_dq_30 
set_location_assignment PIN_AA27 -to memory_mem_dq[31]
#set_location_assignment PIN_AA27 -to memory_mem_dq_31 
set_location_assignment PIN_R17  -to memory_mem_dqs[0]                
#set_location_assignment PIN_R17  -to memory_mem_dqs_0                
set_location_assignment PIN_R19  -to memory_mem_dqs[1]
#set_location_assignment PIN_R19  -to memory_mem_dqs_1
set_location_assignment PIN_T19  -to memory_mem_dqs[2]
#set_location_assignment PIN_T19  -to memory_mem_dqs_2
set_location_assignment PIN_U19  -to memory_mem_dqs[3]
#set_location_assignment PIN_U19  -to memory_mem_dqs_3
set_location_assignment PIN_R16  -to memory_mem_dqs_n[0]
#set_location_assignment PIN_R16  -to memory_mem_dqs_n_0
set_location_assignment PIN_R18  -to memory_mem_dqs_n[1]
#set_location_assignment PIN_R18  -to memory_mem_dqs_n_1
set_location_assignment PIN_T18  -to memory_mem_dqs_n[2]
#set_location_assignment PIN_T18  -to memory_mem_dqs_n_2
set_location_assignment PIN_T20  -to memory_mem_dqs_n[3]
#set_location_assignment PIN_T20  -to memory_mem_dqs_n_3
set_location_assignment PIN_A26  -to memory_mem_cas_n
#set_location_assignment PIN_L28  -to memory_mem_cke
set_location_assignment PIN_L21  -to memory_mem_cs_n              
#set_location_assignment PIN_D28  -to memory_mem_odt
set_location_assignment PIN_A25  -to memory_mem_ras_n
#set_location_assignment PIN_E25  -to memory_mem_we_n
set_location_assignment PIN_V28  -to memory_mem_reset_n
#set_location_assignment PIN_D25  -to memory_oct_rzqin
set_location_assignment PIN_N21  -to memory_mem_ck
#set_location_assignment PIN_N20  -to memory_mem_ck_n              


set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[0]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[1]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_1
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[2]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_2
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[3]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_3
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[4]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_4
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[5]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_5
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[6]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_6
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[7]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_7
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[8]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_8
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[9]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_9
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[10]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_10
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[11]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_11
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[12]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_12
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[13]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_13
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a[14]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_a_14
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_ba[0]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_ba_0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_ba[1]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_ba_1
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_ba[2]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_ba_2
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dm[0]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dm_0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dm[1]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dm_1
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dm[2]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dm_2
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dm[3]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dm_3
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq[0] 
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq_0
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq[1]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq_1
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq[2]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq_2
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq[3]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq_3
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq[4]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq_4
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq[5]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq_5
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq[6]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq_6
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq[7]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq_7
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq[8]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq_8
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq[9]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"   -to memory_mem_dq_9
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[10]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_10 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[11]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_11 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[12]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_12 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[13]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_13 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[14]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_14 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[15]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_15 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[16]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_dq_16 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[17]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_17 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[18]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_18 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[19]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_19 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[20]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_20 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[21]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_21 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[22]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_22 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[23]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_23 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[24]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_24 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[25]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_25 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_dq[26]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_dq_26
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[27]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_27 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[28]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_28 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[29]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_29 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq[30]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_dq_30 
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_dq[31]
#set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I" -to memory_mem_dq_31 
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs[0]                
#set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_0                
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs[1]
#set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_1
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs[2]
#set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_2
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs[3]
#set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_3
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_n[0]
#set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_n_0
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_n[1]
#set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_n_1
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_n[2]
#set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_n_2
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_n[3]
#set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_dqs_n_3
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_cas_n
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_cke
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_cs_n              
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_odt
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_ras_n
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_we_n
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_mem_reset_n
set_instance_assignment -name IO_STANDARD "SSTL-15 CLASS I"  -to memory_oct_rzqin
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to memory_mem_ck
set_instance_assignment -name IO_STANDARD "DIFFERENTIAL 1.5-V SSTL CLASS I"   -to  memory_mem_ck_n              

#============================================================
# HPS
# 		=> EMAC0
#============================================================

set_location_assignment PIN_R21  -to hps_io_hps_io_gpio_inst_HLGPI3
                                    
set_location_assignment PIN_G4  -to hps_io_hps_io_emac0_inst_RX_CLK
set_location_assignment PIN_F4  -to hps_io_hps_io_emac0_inst_RX_CTL
set_location_assignment PIN_C8  -to hps_io_hps_io_emac0_inst_RXD0
set_location_assignment PIN_C5  -to hps_io_hps_io_emac0_inst_RXD1
set_location_assignment PIN_E5  -to hps_io_hps_io_emac0_inst_RXD2
set_location_assignment PIN_D5  -to hps_io_hps_io_emac0_inst_RXD3
set_location_assignment PIN_E4  -to hps_io_hps_io_emac0_inst_TX_CLK
set_location_assignment PIN_C6  -to hps_io_hps_io_emac0_inst_TX_CTL
set_location_assignment PIN_C10 -to hps_io_hps_io_emac0_inst_TXD0
set_location_assignment PIN_F5  -to hps_io_hps_io_emac0_inst_TXD1
set_location_assignment PIN_C9  -to hps_io_hps_io_emac0_inst_TXD2
set_location_assignment PIN_C4  -to hps_io_hps_io_emac0_inst_TXD3
set_location_assignment PIN_C7  -to hps_io_hps_io_emac0_inst_MDC
set_location_assignment PIN_D4  -to hps_io_hps_io_emac0_inst_MDIO

############# NOTE!!! the hps_io_hps_io_gpio_inst_HLGPI* CAN'T BE 3.3V INPUTS!! But the EMAC0/1 Interrupt signal
#############         is 3.3V on the schematic!!!
set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI3

set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_TX_CLK
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_MDC
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_MDIO
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_RX_CLK
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_RXD0
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_RXD1
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_RXD2
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_RXD3
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_RX_CTL
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_TXD0
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_TXD1
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_TXD2
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_TXD3
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_emac0_inst_TX_CTL

##============================================================
## HPS
## 		=> SPI Master1
##============================================================
#set_location_assignment PIN_C19  -to hps_io_hps_io_spim1_inst_CLK
#set_location_assignment PIN_B16  -to hps_io_hps_io_spim1_inst_MOSI
#set_location_assignment PIN_B19  -to hps_io_hps_io_spim1_inst_MISO
#set_location_assignment PIN_C16  -to hps_io_hps_io_spim1_inst_SS0

#set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_spim1_inst_CLK
#set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_spim1_inst_MOSI
#set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_spim1_inst_MISO
#set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_spim1_inst_SS0

#============================================================
# HPS
# 		=> I2C0
#============================================================
#set_location_assignment PIN_A19  -to hps_io_hps_io_i2c0_inst_SDA
#set_location_assignment PIN_C18  -to hps_io_hps_io_i2c0_inst_SCL

#set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_i2c0_inst_SDA
#set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_i2c0_inst_SCL

#============================================================
# HPS
# 		=> CAN0
#============================================================
set_location_assignment PIN_A17  -to hps_io_hps_io_can0_inst_RX
set_location_assignment PIN_H17  -to hps_io_hps_io_can0_inst_TX
                                     
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_can0_inst_RX
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_can0_inst_TX

#============================================================
# HPS
# 		=> CAN1
#============================================================
#set_location_assignment PIN_A20  -to hps_io_hps_io_can1_inst_RX
#set_location_assignment PIN_J18  -to hps_io_hps_io_can1_inst_TX

#set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_can1_inst_RX
#set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to hps_io_hps_io_can1_inst_TX

#============================================================
# HPS
# 		=> Inputs only
#============================================================
#set_location_assignment PIN_M25  -to hps_io_hps_io_gpio_inst_HLGPI0
#set_location_assignment PIN_K27  -to hps_io_hps_io_gpio_inst_HLGPI1
#set_location_assignment PIN_R20  -to hps_io_hps_io_gpio_inst_HLGPI2
#set_location_assignment PIN_R28  -to hps_io_hps_io_gpio_inst_HLGPI4
#set_location_assignment PIN_P26  -to hps_io_hps_io_gpio_inst_HLGPI5
#set_location_assignment PIN_T17  -to hps_io_hps_io_gpio_inst_HLGPI6
#set_location_assignment PIN_T16  -to hps_io_hps_io_gpio_inst_HLGPI7
#set_location_assignment PIN_Y28  -to hps_io_hps_io_gpio_inst_HLGPI8
#set_location_assignment PIN_Y26  -to hps_io_hps_io_gpio_inst_HLGPI9
#set_location_assignment PIN_U15  -to hps_io_hps_io_gpio_inst_HLGPI10
#set_location_assignment PIN_U16  -to hps_io_hps_io_gpio_inst_HLGPI11
#set_location_assignment PIN_AC27  -to hps_io_hps_io_gpio_inst_HLGPI12
	   
############# NOTE!!! the hps_io_hps_io_gpio_inst_HLGPI* CAN'T BE 3.3V INPUTS!!
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI0
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI1
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI2
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI4
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI5
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI6
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI7
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI8
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI9
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI10
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI11
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI12
#set_instance_assignment -name IO_STANDARD "SSTL-135" -to hps_io_hps_io_gpio_inst_HLGPI13

#============================================================
# USI
#============================================================

set_location_assignment PIN_AG26 -to USI_SLAVE_RX
set_location_assignment PIN_AH26 -to USI_SLAVE_TX

set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to USI_SLAVE_RX
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to USI_SLAVE_TX

#============================================================
# FPGA LED
#============================================================

set_location_assignment PIN_D8 -to FPGAHeartBeat_LED


set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to FPGAHeartBeat_LED


#============================================================
# FPGA
#	=> 4b5b
#============================================================

set_location_assignment PIN_AF18 -to ACU_SerialCom_Rx[0]
set_location_assignment PIN_AG21 -to ACU_SerialCom_Rx[1]
set_location_assignment PIN_AH12 -to ACU_SerialCom_Tx[0]
set_location_assignment PIN_AH21 -to ACU_SerialCom_Tx[1]

set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to ACU_SerialCom_Rx[0]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to ACU_SerialCom_Rx[1]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to ACU_SerialCom_Tx[0]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to ACU_SerialCom_Tx[1]

