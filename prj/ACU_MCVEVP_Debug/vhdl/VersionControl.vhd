LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;

ENTITY VersionControl IS
GENERIC
(
   VC_DateTimeHost        : string    := "191023 082003 drodomon-Latitude-5510"
);
PORT
(
   VerNumber: OUT STD_LOGIC_VECTOR(23 downto 0);             --bei 'Testversion' immer 0xFFFFFF
   VerTime  : OUT STD_LOGIC_VECTOR(23 downto 0);
   VerDate  : OUT STD_LOGIC_VECTOR(23 downto 0)
);
END VersionControl;

ARCHITECTURE rtl OF VersionControl IS

constant FWMajorVersion : integer := 7;                --Hauptversionsnummer
constant FWMinorVersion : integer := 0;
constant FWMinorSubVersion : integer := 255;

BEGIN
  VerNumber <= std_logic_vector(to_unsigned(FWMajorVersion, 8)) & std_logic_vector(to_unsigned(FWMinorVersion, 8))& std_logic_vector(to_unsigned(FWMinorSubVersion, 8));
   VerTime   <= X"082003";
   VerDate   <= X"191023";
END rtl;
