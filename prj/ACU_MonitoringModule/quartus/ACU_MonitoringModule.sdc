############################################
#       ACU_MonitoringModule SDC file      #
############################################

# Clock Constraints

  # Base Clock
  create_clock -name InputPinClock50MHz -period 20.000 [get_ports {Board50MHz_CLK}]
  # Virtual Clock (used to evaluate the I/O timing uncertainties)
  create_clock -name VirtualClockIn -period 20.000
  # Clock uncertainty (change this constraint with SDC original constraints (set_clock_latency and set_clock_uncertainty))
  derive_clock_uncertainty
 
  # Generated Clock
  # System clock 100MHz
  create_generated_clock -name SystemClock100MHz -source [get_ports {Board50MHz_CLK}] -divide_by 1 -multiply_by 2 [get_pins {i_acu_PLL_100_10MHz_CyclonV|acu_pll_100_10mhz_cyclonv_inst|altera_pll_i|general[0].gpll~PLL_REFCLK_SELECT|clkout}] 
  