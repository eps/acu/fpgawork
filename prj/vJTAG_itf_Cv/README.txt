This project implements the vJTAG interface on a CycloneV FPGA.
It is used to transfer parameters from/to the FPGA through a JTAG connection.

This data transfer mode is OBSOLETE and has several LIMITATIONS!!! 
Please don't use it for future FW designs. 

The idea is to transfer up to 2048 parameter bit and store them on a EPCQ flash.
On power up those data are read back from the flash and made available for the FPGA desing and for the SW (FF_RdWrParam_GUI.py). 
