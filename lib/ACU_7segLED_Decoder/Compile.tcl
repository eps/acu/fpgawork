###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work







#vmap altera_mf work




pwd
         

vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/SimFolder/vhdl/up_dw_cntCmdInDriven.vhd

vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_InputFilter/vhdl/ram64wxb_CV.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_InputFilter/vhdl/ACU_InputFilter.vhd

vcom -93 ../../SFRS_FW/vhdl/SFRS_ButtonsMngr.vhd

vcom -93 $path/ACU_7segLED_Decoder.vhd

vcom -93 $path/Tb_ACU_7segLED_Decoder.vhd
