restart -f

run 1 us
force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/Reset 0 0

run 110 us
for { set c 0}  {$c < 5} {incr c} {
	###############################################################################
	# Increment
	###############################################################################
	for { set a 0}  {$a < 10} {incr a} {
   		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 0 0
   		run $a us
   		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 1 0
	   	run $a us   
	}

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 1 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 0 0

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 0 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 1 0
	run 110 us

	for { set b 0}  {$b < 35} {incr b} {
	  for { set a 0}  {$a < 10} {incr a} {
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 0 0
	     run $a us
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 1 0
	     run $a us   
	  }

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 0 0

	  run 100 us

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn($c) 1 0
 	 run 110 us

	}	

	###############################################################################
	# Decrement
	###############################################################################
	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 1 0
	   run $a us   
	}

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 1 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 0 0

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 0 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 1 0
	run 110 us

	for { set b 0}  {$b < 35} {incr b} {
	  for { set a 0}  {$a < 10} {incr a} {
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 0 0
	     run $a us
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 1 0
	     run $a us   
	  }

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 0 0

	  run 100 us

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn([expr $c+1]) 1 0
	  run 110 us

	}
}	
































restart -f

run 1 us
force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/Reset 0 0

run 110 us


## LEMO
        ###############################################################################
	# Decrement
	###############################################################################
	for { set a 0}  {$a < 10} {incr a} {
   		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 0 0
   		run $a us
   		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 1 0
	   	run $a us   
	}

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 1 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 0 0

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 0 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 1 0
	run 110 us

	for { set b 0}  {$b < 35} {incr b} {
	  for { set a 0}  {$a < 10} {incr a} {
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 0 0
	     run $a us
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 1 0
	     run $a us   
	  }

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 0 0

	  run 100 us
	  
	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 0 0
	   run $a us   
	}


	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(0) 1 0
 	 run 110 us

	}	

	###############################################################################
	# Increment
	###############################################################################
	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 1 0
	   run $a us   
	}

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 1 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 0 0

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 0 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 1 0
	run 110 us

	for { set b 0}  {$b < 35} {incr b} {
	  for { set a 0}  {$a < 10} {incr a} {
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 0 0
	     run $a us
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 1 0
	     run $a us   
	  }

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 0 0

	  run 100 us
	  
	  for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 0 0
	   run $a us   
	  }


	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(1) 1 0
	  run 110 us

	}
	
## cmd
        ###############################################################################
	# Decrement
	###############################################################################
	for { set a 0}  {$a < 10} {incr a} {
   		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 0 0
   		run $a us
   		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 1 0
	   	run $a us   
	}

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 1 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 0 0

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 0 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 1 0
	run 110 us

	for { set b 0}  {$b < 35} {incr b} {
	  for { set a 0}  {$a < 10} {incr a} {
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 0 0
	     run $a us
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 1 0
	     run $a us   
	  }

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 0 0

	  run 100 us
	  
	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 0 0
	   run $a us   
	}


	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 1 0
 	 run 110 us

	}	

	###############################################################################
	# Increment
	###############################################################################
	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 1 0
	   run $a us   
	}

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 1 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 0 0

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 0 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 1 0
	run 110 us

	for { set b 0}  {$b < 35} {incr b} {
	  for { set a 0}  {$a < 10} {incr a} {
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 0 0
	     run $a us
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 1 0
	     run $a us   
	  }

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 0 0

	  run 100 us
	  
	  for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 0 0
	   run $a us   
	  }


	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(5) 1 0
	  run 110 us

	}	
	
## ch
        ###############################################################################
	# Decrement
	###############################################################################
	for { set a 0}  {$a < 10} {incr a} {
   		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 0 0
   		run $a us
   		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 1 0
	   	run $a us   
	}

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 1 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 0 0

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 0 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 1 0
	run 110 us

	for { set b 0}  {$b < 35} {incr b} {
	  for { set a 0}  {$a < 10} {incr a} {
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 0 0
	     run $a us
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 1 0
	     run $a us   
	  }

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 0 0

	  run 100 us
	  
	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 0 0
	   run $a us   
	}


	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 1 0
 	 run 110 us

	}	

	###############################################################################
	# Increment
	###############################################################################
	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 1 0
	   run $a us   
	}

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 1 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 0 0

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 0 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 1 0
	run 110 us

	for { set b 0}  {$b < 35} {incr b} {
	  for { set a 0}  {$a < 10} {incr a} {
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 0 0
	     run $a us
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 1 0
	     run $a us   
	  }

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 0 0

	  run 100 us
	  
	  for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 0 0
	   run $a us   
	  }


	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(7) 1 0
	  run 110 us

	}		
	
## DAC
        ###############################################################################
	# Decrement
	###############################################################################
	for { set a 0}  {$a < 10} {incr a} {
   		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 0 0
   		run $a us
   		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 1 0
	   	run $a us   
	}

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 1 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 0 0

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 0 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 1 0
	run 110 us

	for { set b 0}  {$b < 35} {incr b} {
	  for { set a 0}  {$a < 10} {incr a} {
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 0 0
	     run $a us
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 1 0
	     run $a us   
	  }

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 0 0

	  run 100 us
	  
	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 0 0
	   run $a us   
	}


	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(8) 1 0
 	 run 110 us

	}	

	###############################################################################
	# Increment
	###############################################################################
	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 1 0
	   run $a us   
	}

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 0 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 1 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 0 0

	run 100 us

	for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 0 0
	   run $a us   
	}

	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 1 0
	run 110 us

	for { set b 0}  {$b < 35} {incr b} {
	  for { set a 0}  {$a < 10} {incr a} {
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 0 0
	     run $a us
	     force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 1 0
	     run $a us   
	  }

	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 0 0

	  run 100 us
	  
	  for { set a 0}  {$a < 10} {incr a} {
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 1 0
	   run $a us
	   force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 0 0
	   run $a us   
	  }


	  force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(9) 1 0
	  run 110 us

	}		

# cmd pro ch

for { set d 0}  {$d < 7} {incr d} {
	# incr ch
	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 0 0
	run 120 us
	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 1 0
	run 120 us
	for { set a 0}  {$a < 35} {incr a} {
		# incr cmd
		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 0 0
		run 120 us
		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 1 0
		run 120 us
		#send cmd
		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(3) 0 0
		run 120 us
		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(3) 1 0
		run 120 us
	}
}

# cmd on all ch

for { set d 0}  {$d < 7} {incr d} {
	# incr ch
	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 0 0
	run 120 us
	force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(6) 1 0
	run 120 us
	for { set a 0}  {$a < 35} {incr a} {
		# incr cmd
		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 0 0
		run 120 us
		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(4) 1 0
		run 120 us
		#send cmd
		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(2) 0 0
		run 120 us
		force -freeze sim:/tb_acu_7segled_decoder/i_ACU_InputFilter/SigIn(2) 1 0
		run 120 us
	}
}


