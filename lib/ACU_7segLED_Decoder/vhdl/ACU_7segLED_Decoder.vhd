---------------------------------------------------------------------------------------
-- File name: ACU_7segLED_Decoder.vhd                                                --
--                                                                                   --
-- Author   : D.Rodomonti                                                            --
-- Date     : 27/06/2024                                                             --
--                                                                                   --
-- Comments : 7 segments LED decoder.                                                --
--                                                                                   --
-- History  : Start up version 27/06/2024                                            --
---------------------------------------------------------------------------------------

library ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity ACU_7segLED_Decoder is

  Port (
       Clock            : in std_logic;
       Reset            : in std_logic;
       Input2display    : in std_logic_vector(3 downto 0);                  -- From 0 to 9
       LED7segOut       : out std_logic_vector(6 downto 0)                  -- (6)  LED0 g;(5)  LED0 f;(4)  LED0 e;(3)  LED0 d;(2) LED0 c;(1) LED0 b;(0) LED0 a;
    );
end entity ACU_7segLED_Decoder;
 

architecture beh of ACU_7segLED_Decoder is

-- Consatnt declaration
constant cZero          : std_logic_vector(6 downto 0):="1000000";
constant cOne           : std_logic_vector(6 downto 0):="1111001";
constant cTwo           : std_logic_vector(6 downto 0):="0100100";
constant cThree         : std_logic_vector(6 downto 0):="0110000";
constant cFour          : std_logic_vector(6 downto 0):="0011001";
constant cFive          : std_logic_vector(6 downto 0):="0010010";
constant cSix           : std_logic_vector(6 downto 0):="0000010";
constant cSeven         : std_logic_vector(6 downto 0):="1111000";
constant cEight         : std_logic_vector(6 downto 0):="0000000";
constant cNine          : std_logic_vector(6 downto 0):="0010000";

begin
p_decoder: process(Clock,Reset)
begin
  if (Reset='1') then
    LED7segOut  <= cZero;
  elsif(Clock'event and Clock='1') then
    case Input2display is
      when "0000" =>
        LED7segOut  <= cZero;
      when "0001" =>
        LED7segOut  <= cOne;
      when "0010" =>
        LED7segOut  <= cTwo;
      when "0011" =>
        LED7segOut  <= cThree;
      when "0100" =>
        LED7segOut  <= cFour;
      when "0101" =>
        LED7segOut  <= cFive;
      when "0110" =>
        LED7segOut  <= cSix;
      when "0111" =>
        LED7segOut  <= cSeven;
      when "1000" =>
        LED7segOut  <= cEight;
      when "1001" =>
        LED7segOut  <= cNine;
      when others =>
        LED7segOut  <= cNine;
    end case;
  end if;
  
end process;

end beh;
