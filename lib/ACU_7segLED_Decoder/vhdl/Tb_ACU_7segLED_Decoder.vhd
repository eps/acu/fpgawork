---------------------------------------------------------------------------------------
-- File name: Tb_ACU_7segLED_Decoder.vhd                                             --
--                                                                                   --
-- Author   : D.Rodomonti                                                            --
-- Date     : 27/06/2024                                                             --
--                                                                                   --
-- Comments : 7 segments LED decoder test bench.                                     --
--                                                                                   --
-- History  : Start up version 27/06/2024                                            --
---------------------------------------------------------------------------------------

library ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity Tb_ACU_7segLED_Decoder is
end entity Tb_ACU_7segLED_Decoder;
 

architecture beh of Tb_ACU_7segLED_Decoder is
--
-- Consatnt declaration

--
-- Signal declaration
signal Clock                     : std_logic := '0';
signal Reset                     : std_logic := '1';
signal SigIn                     : std_logic_vector(9 downto 0):="1111111111";
signal DelayValue                : std_logic_vector(15 downto 0):=x"000A";             -- 100 us filtering
signal SigOut                    : std_logic_vector(9 downto 0);
signal nSigOut                   : std_logic_vector(9 downto 0);
signal decrCmdLED1               : std_logic;
signal incrCmdLED1               : std_logic;
signal LED0                      : std_logic_vector(3 downto 0);
signal LED1                      : std_logic_vector(3 downto 0);
signal DAC_Sel                   : std_logic_vector(4 downto 0);
signal ResetCntLED               : std_logic;
signal incrDt2DAC_SelLED0        : std_logic;
signal decrDt2DAC_SelLED0        : std_logic;

signal intResetDt2DAC_SelLED     : std_logic;
signal SPI_x_Dt2DAC              : std_logic_vector(479 downto 0) :=x"666666666666666666665555555555555555555544444444444444444444333333333333333333332222222222222222222211111111111111111111";
signal SPI_x_Dt2LEMO             : std_logic_vector(23 downto 0):=x"654321";

--
-- Component declaration
Component ACU_InputFilter is
generic  
   (
      gACU_InputFilterVersion    : string := "23.04.21";
      gSignInLenght              : integer range 1 to 64 := 10;  
      gDelayFE_h_RE_l            : std_logic := '1';
      gCyclonIII_n_CyclonV       : std_logic := '0';
      gDelWidth                  : integer range 1 to 20 := 16
   );
  Port (
         Clock                   : in std_logic;                                   -- Clock signal (usually @100MHz)
         Reset                   : in std_logic;                                   -- Asynchronous reset signal active high
         SigIn                   : in std_logic_vector(gSignInLenght-1 downto 0);  -- Input signal to filter
	      MaskIn                  : in std_logic_vector(gSignInLenght-1 downto 0);  -- Input signal mask: when MaskIn(y)='1' then SigOut(y)= SigIn(y). (No filter)
         DelayValue              : in std_logic_vector(gDelWidth-1 downto 0);      -- max=(2^gDelWidth)*10us (with 100MHz clock signal)
         SigOut                  : out std_logic_vector(gSignInLenght-1 downto 0)  -- Filtered output signal 
  );
End component ACU_InputFilter;

Component up_dw_cntCmdInDriven is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       incrCmd          : in std_logic;
       decrCmd          : in std_logic;
       incrCmd_out      : out std_logic;
       decrCmd_out      : out std_logic;
       cntOutValid      : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component up_dw_cntCmdInDriven;

Component ACU_7segLED_Decoder is

  Port (
       Clock            : in std_logic;
       Reset            : in std_logic;
       Input2display    : in std_logic_vector(3 downto 0);                  -- From 0 to 9
       LED7segOut       : out std_logic_vector(6 downto 0)                  -- (6)  LED0 g;(5)  LED0 f;(4)  LED0 e;(3)  LED0 d;(2) LED0 c;(1) LED0 b;(0) LED0 a;
    );
end component ACU_7segLED_Decoder;

Component SFRS_ButtonsMngr is
  Generic(
       gPortNr          : integer range 1 to 12:=6;                                    -- Ports number to manage
       gDt2LEMO_Width   : integer range 1 to 16:=4;                                    -- Data to LEMO width pro channel
       gDt2DAC_Width    : integer range 1 to 32:=20                                    -- Data to DAC width pro channel
       
  );
  Port (
       Clock            : in std_logic;
       Reset            : in std_logic;
       Buttons          : in std_logic_vector(9 downto 0);                             -- Active low
                                                                                       -- (9..8) DAC_Sel_UP,DAC_Sel_DW
                                                                                       -- (7..6) CH_UP,CH_DW
                                                                                       -- (5..4) CMD_UP,CMD_DW
                                                                                       -- (3) Send CMD to the selected channel
                                                                                       -- (2) Send CMD to all channels
                                                                                       -- (1..0) LEMO_Sel_UP,LEMO_Sel_DW
       Rx_Dt2LEMO       : in std_logic_vector((gPortNr*gDt2LEMO_Width)-1 downto 0);    --  With default generics: (23..20) Rx_Dt2LEMO_5
                                                                                       --                         (19..16) Rx_Dt2LEMO_4
                                                                                       --                         (15..12) Rx_Dt2LEMO_3
                                                                                       --                          (11..8) Rx_Dt2LEMO_2
                                                                                       --                           (7..4) Rx_Dt2LEMO_1
                                                                                       --                           (3..0) Rx_Dt2LEMO_0                                                                                                                                                                                                                                                                                                                
       Rx_Dt2DAC        : in std_logic_vector((gPortNr*4*gDt2DAC_Width)-1 downto 0);   -- With default generics:  (479..400) Rx_Dt2DAC_5
                                                                                       --                         (399..320) Rx_Dt2DAC_4
                                                                                       --                         (319..240) Rx_Dt2DAC_3
                                                                                       --                         (239..160) Rx_Dt2DAC_2
                                                                                       --                          (159..80) Rx_Dt2DAC_1
                                                                                       --                            (79..0) Rx_Dt2DAC_0                                                                                                                                                                                                                                                                                                               
       SerialCom_x_Cmd  : out std_logic_vector((gPortNr*4)-1  downto 0);               -- With default generics:    (23..20) SerialCom_5_Cmd
                                                                                       --                           (19..16) SerialCom_4_Cmd
                                                                                       --                           (15..12) SerialCom_3_Cmd
                                                                                       --                            (11..8) SerialCom_2_Cmd
                                                                                       --                             (7..4) SerialCom_1_Cmd
                                                                                       --                             (3..0) SerialCom_0_Cmd
       Data2DAC_Sel     : out std_logic_vector(4 downto 0);                            -- Selector to send back                                                                                                                                                                                                                                                                                                                
       Data2LEMO_Sel    : out std_logic_vector(4 downto 0);                            -- Selector to send back
       Data2DAC         : out std_logic_vector((4*gDt2DAC_Width)-1 downto 0);          -- Selected DAC data                                                                                                                                                                                                                                                                                                                
       Data2LEMO        : out std_logic_vector(gDt2LEMO_Width-1 downto 0);             -- Selected LEMO data
       
       DAC_Sel_7SegIn   : out std_logic_vector(7 downto 0);                            -- 7-seg Driver input (2LEDs)
       Ch_Sel_7SegIn    : out std_logic_vector(3 downto 0);                            -- 7-seg Driver input (1LEDs)
       Cmd_Sel_7SegIn   : out std_logic_vector(7 downto 0);                            -- 7-seg Driver input (2LEDs)
       LEMO_Sel_7SegIn  : out std_logic_vector(7 downto 0)                             -- 7-seg Driver input (2LEDs)       
    );
end Component SFRS_ButtonsMngr;
 

begin

Clock  <= not Clock after 5 ns;

i_ACU_InputFilter:ACU_InputFilter      
  generic map(
         gCyclonIII_n_CyclonV    => '1'
  )
  Port map (
         Clock                   => Clock,
         Reset                   => Reset,
         SigIn                   => SigIn,
	 MaskIn                  => "0000000000",
         DelayValue              => DelayValue,
         SigOut                  => SigOut
  );
  
nSigOut  <= not (SigOut);

i_Dt2DAC_Sel:up_dw_cntCmdInDriven
  generic map (
       gCntOutWidth     => 5,
       gEnTrigValue     => 32
    )
  port map(
       clock            => Clock,
       reset            => Reset,
       incrCmd          => nSigOut(0),
       decrCmd          => nSigOut(1),
       incrCmd_out      => open,
       decrCmd_out      => open,
       cntOutValid      => open,
       cntOut           => DAC_Sel
    
    );
   
   
p_Dt2DAC_SelLED0_Mngr: process(Clock,Reset)
begin
  if (Reset='1') then
    LED0      <= (others=>'0');
    LED1      <= (others=>'0');
  elsif(Clock'event and Clock='1') then
    case DAC_Sel is
      when "00000" => 
        LED0      <= x"0";
        LED1      <= x"0";
      when "00001" => 
        LED0      <= x"1";
        LED1      <= x"0";
      when "00010" => 
        LED0      <= x"2";
        LED1      <= x"0";
      when "00011" => 
        LED0      <= x"3";
        LED1      <= x"0";
      when "00100" => 
        LED0      <= x"4";
        LED1      <= x"0";
      when "00101" => 
        LED0      <= x"5";
        LED1      <= x"0";
      when "00110" => 
        LED0      <= x"6";
        LED1      <= x"0";
      when "00111" => 
        LED0      <= x"7";
        LED1      <= x"0";
      when "01000" => 
        LED0      <= x"8";
        LED1      <= x"0";
      when "01001" => 
        LED0      <= x"9";
        LED1      <= x"0";
      when "01010" => 
        LED0      <= x"0";
        LED1      <= x"1";
      when "01011" => 
        LED0      <= x"1";
        LED1      <= x"1";
      when "01100" => 
        LED0      <= x"2";
        LED1      <= x"1";
      when "01101" => 
        LED0      <= x"3";
        LED1      <= x"1";
      when "01110" => 
        LED0      <= x"4";
        LED1      <= x"1";
      when "01111" => 
        LED0      <= x"5";
        LED1      <= x"1";
      when "10000" => 
        LED0      <= x"6";
        LED1      <= x"1";
      when "10001" => 
        LED0      <= x"7";
        LED1      <= x"1";
      when "10010" => 
        LED0      <= x"8";
        LED1      <= x"1";
      when "10011" => 
        LED0      <= x"9";
        LED1      <= x"1";
      when "10100" => 
        LED0      <= x"0";
        LED1      <= x"2";
      when "10101" => 
        LED0      <= x"1";
        LED1      <= x"2";
      when "10110" => 
        LED0      <= x"2";
        LED1      <= x"2";
      when "10111" => 
        LED0      <= x"3";
        LED1      <= x"2";
      when "11000" => 
        LED0      <= x"4";
        LED1      <= x"2";
      when "11001" => 
        LED0      <= x"5";
        LED1      <= x"2";
      when "11010" => 
        LED0      <= x"6";
        LED1      <= x"2";
      when "11011" => 
        LED0      <= x"7";
        LED1      <= x"2";
      when "11100" => 
        LED0      <= x"8";
        LED1      <= x"2";
      when "11101" => 
        LED0      <= x"9";
        LED1      <= x"2";
      when "11110" => 
        LED0      <= x"0";
        LED1      <= x"3";
      when "11111" => 
        LED0      <= x"1";
        LED1      <= x"3";
      when others => 
        LED0      <= x"0";
        LED1      <= x"0";

    end case;
  end if;
end process p_Dt2DAC_SelLED0_Mngr;
   
   
   
   
   
    

i_ACU_7segLED_Decoder_0: ACU_7segLED_Decoder
  Port map(
       Clock            => Clock,
       Reset            => Reset,
       Input2display    => LED0,
       LED7segOut       => open
    );


i_ACU_7segLED_Decoder_1: ACU_7segLED_Decoder
  Port map(
       Clock            => Clock,
       Reset            => Reset,
       Input2display    => LED1,
       LED7segOut       => open
    );
 
 
 
i_SFRS_ButtonsMngr :SFRS_ButtonsMngr
  Port map (
       Clock            => Clock,
       Reset            => Reset,
       Buttons          => SigIn,
        
       Rx_Dt2LEMO       => SPI_x_Dt2LEMO,                                                                                                                                                                                                                                                                       
       Rx_Dt2DAC        => SPI_x_Dt2DAC,                                                                                                                                                                                                                                                                                                                
       SerialCom_x_Cmd  => open,
       Data2DAC_Sel     => open,                                                                                                                                                                                                                                                                                                               
       Data2LEMO_Sel    => open,
       Data2DAC         => open,                                                                                                                                                                                                                                                                                    
       Data2LEMO        => open,
       
       DAC_Sel_7SegIn   => open,
       Ch_Sel_7SegIn    => open,
       Cmd_Sel_7SegIn   => open,
       LEMO_Sel_7SegIn  => open
    );  
    
end beh;
