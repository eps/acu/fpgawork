--------------------------------------------------------------------------------------
-- File name: Tb_EPCQ_ParamMngr.vhd                                                 --
--                                                                                  --
-- Author   : D.Rodomonti                                                           --
-- Date     : 15/12/2020                                                            --
--                                                                                  --
-- Comments : EPCQ_ParamMngr test bench file                                        -- 
--                                                                                  --
--                                                                                  --
-- History  : Start up version 15/12/2020                                           --
--------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity Tb_EPCQ_ParamMngr is
End entity Tb_EPCQ_ParamMngr;

Architecture beh of Tb_EPCQ_ParamMngr is
--
-- Constants declaration
constant RdFlag                : std_logic_vector(7 downto 0) :=x"70";
constant RdPage                : std_logic_vector(39 downto 0):=x"0C00000000";

--
-- Signals declaration
signal Clock                   : std_logic:='0';
signal Reset                   : std_logic:='1';
signal vJTAGdt2Wr              : std_logic_vector(15 downto 0):=(others=>'0');
signal vJTAGAddr               : std_logic_vector(6 downto 0):=(others=>'0');
signal vJTAGwr                 : std_logic:='0';
signal EPCQrd                  : std_logic:='0';
signal EPCQwr                  : std_logic:='0';
signal EPCQaddr                : std_logic_vector(31 downto 0);
signal EPCQ_pgRd               : std_logic;                    
signal EPCQ_pgWr               : std_logic;                    
signal EPCQ_writePg            : std_logic_vector(2047 downto 0);
signal EPCQ_readPg             : std_logic_vector(2047 downto 0);
signal SentProt                : std_logic;
signal EPCQ_SectEr             : std_logic;
signal SDT_in                  : std_logic_vector(3 downto 0);
signal EmulFlash               : std_logic_vector(2103 downto 0);
signal FlashBusy               : std_logic:='0';
signal SCLK                    : std_logic;
signal SDT_out                 : std_logic_vector(3 downto 0);
--
-- Component declaration
Component EPCQ_ParamMngr is
  Port (
         Clock                   : in  std_logic;                             -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                             -- Asynchronous reset signal active high

	      -- vJTAG_itf
	      vJTAGdt2Wr              : in  std_logic_vector(15 downto 0);         -- Parameter to write 
	      vJTAGAddr               : in  std_logic_vector(6 downto 0);          -- Parameter address (from 0 to 127) 
	      vJTAGwr                 : in  std_logic;                             -- Write command
	      vJTAGdtRead             : out std_logic_vector(15 downto 0);         -- Read parameter
 
         -- EPCQ Driver
         EPCQrd                  : in  std_logic;                             -- It triggers the EPCQ reading action of 8 pages
         EPCQwr                  : in  std_logic;                             -- It triggers the EPCQ writing action of 8 pages
         EPCQaddr                : out std_logic_vector(31 downto 0);         -- EPCQ page address
         EPCQ_pgRd               : out std_logic;                             -- EPCQ page read command
         EPCQ_pgWr               : out std_logic;                             -- EPCQ page write command
         EPCQ_SectEr             : out std_logic;                             -- EPCQ sector erase command
         EPCQ_writePg            : out std_logic_vector(2047 downto 0);       -- Data to EPCQ
         EPCQ_readPg             : in  std_logic_vector(2047 downto 0);       -- Data from EPCQ
	      SentProt                : in  std_logic;                             -- It is a pulse active high when the EPCQ Driver sent a protocol	 

         -- Debug only
         param2                  : out std_logic_vector(15 downto 0);
         param127                : out std_logic_vector(15 downto 0)
	  
  );
End component EPCQ_ParamMngr;

Component EPCQ_DriverParam is
  Generic (
    gSCLK_High          : unsigned(7 downto 0):="00000001";                  -- SCLK high semiperiod length (in number of Clock pulses) 
    gSCLK_Low           : unsigned(7 downto 0):="00000001";                  -- SCLK low  semiperiod length (in number of Clock pulses)
    gCSn2SCLK_SetupTime : unsigned(7 downto 0):="00000010";                  -- nrSCLK_pulses signal value.
    gFlashAddrWidth     : integer range 8 to 32 :=32                         -- Flash address length
  );
  Port(
    Clock               : in  std_logic;                                     -- Clock signal
    Reset               : in  std_logic;                                     -- Asynchronous reset signal active high

    EPCQaddr            : in  std_logic_vector(gFlashAddrWidth-1 downto 0);  -- EPCQ page address
    EPCQ_pgRd           : in  std_logic;                                     -- EPCQ page read command
    EPCQ_pgWr           : in  std_logic;                                     -- EPCQ page write command
    EPCQ_SectEr         : in  std_logic;                                     -- EPCQ erase sector command
    EPCQ_writePg        : in  std_logic_vector(2047 downto 0);               -- Data to EPCQ
    EPCQ_readPg         : out std_logic_vector(2047 downto 0);               -- Data from EPCQ
    SentProt            : out std_logic;                                     -- It is a pulse active high when the EPCQ Driver sent a protocol	 

    Busy                : out std_logic;                                     -- It is high during actions on SPI bus
    Tx_h_Rx_l           : out std_logic_vector(3 downto 0);                  -- It defines the 4 SPI data lines direction.

    -- SPI
    SCLK                : out std_logic;                                     -- SPI clock signal
    SCS_n               : out std_logic;                                     -- Active low SPI Chip Select signal
    SDT_in              : in  std_logic_vector(3 downto 0);                  -- Serial SPI incoming data 
    SDT_out             : out std_logic_vector(3 downto 0)                   -- Serial SPI outgoing data 
  );

end component EPCQ_DriverParam;
begin
Clock        <= not (Clock) after 5 ns;


--EPCQ_readPg  <= (others=>'0');
--SentProt     <= '0';

i_EPCQ_ParamMngr: EPCQ_ParamMngr 
  Port map (
         Clock                   => Clock,
         Reset                   => Reset,

	      -- vJTAG_itf
	      vJTAGdt2Wr              => vJTAGdt2Wr,
	      vJTAGAddr               => vJTAGAddr,
	      vJTAGwr                 => vJTAGwr,
	      vJTAGdtRead             => open,
 
         -- EPCQ Driver
         EPCQrd                  => EPCQrd,
         EPCQwr                  => EPCQwr,
         EPCQaddr                => EPCQaddr,
         EPCQ_pgRd               => EPCQ_pgRd,
         EPCQ_pgWr               => EPCQ_pgWr,
         EPCQ_SectEr             => EPCQ_SectEr,
         EPCQ_writePg            => EPCQ_writePg,
         EPCQ_readPg             => EPCQ_readPg,
	      SentProt                => SentProt,

         -- Debug only
         param2                  => open,
         param127                => open
	  
  );

SDT_in  <= EmulFlash(2103 downto 2100);

i_EPCQ_DriverParam : EPCQ_DriverParam
  Port map(
    Clock               => Clock,
    Reset               => Reset,

    EPCQaddr            => EPCQaddr,
    EPCQ_pgRd           => EPCQ_pgRd,
    EPCQ_pgWr           => EPCQ_pgWr,
    EPCQ_SectEr         => EPCQ_SectEr,
    EPCQ_writePg        => EPCQ_writePg,
    EPCQ_readPg         => EPCQ_readPg,
    SentProt            => SentProt,	 

    Busy                => open,
    Tx_h_Rx_l           => open,

    -- SPI
    SCLK                => SCLK,
    SCS_n               => open,
    SDT_in              => SDT_in,
    SDT_out             => SDT_out 
  );

p_EmulFlash: process(Reset,SCLK)
begin
  if (Reset='1') then
    EmulFlash <= (others=>'0');
  elsif (SCLK'event and SCLK='1') then

    EmulFlash(3 downto 0)     <= SDT_out;
    EmulFlash(2103 downto 4)  <= EmulFlash(2099 downto 0);

    if (EmulFlash(7 downto 0) = RdFlag) then
      EmulFlash(2103 downto 2096) <= FlashBusy & "0000101";
    elsif (EmulFlash(39 downto 0) = RdPage) then
      EmulFlash(2087 downto 40)   <= EPCQ_writePg;
    end if;

  end if;
end process p_EmulFlash;

end beh;
 
