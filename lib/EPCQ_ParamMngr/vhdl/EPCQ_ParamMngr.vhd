--------------------------------------------------------------------------------------
-- File name: EPCQ_ParamMngr.vhd                                                    --
--                                                                                  --
-- Author   : D.Rodomonti                                                           --
-- Date     : 15/12/2020                                                            --
--                                                                                  --
-- Comments : this module gives the possibility to exchange data (parameters)       --
--            between a virtual JTAG interface and an EPCQ flash.                   --
--            At the moment the parameter length is limited to 2048 bit, but it can --
--            be easily increased (it is necessary to change the vJTAG and          --
--            the vJTAG_itf files as well).                                         --
--            The module's core is a register that acts as a buffer:                --
--            it is completely written when the EPCQ is read                        --
--            (8 pages read actions=2048 bit),it is completely read when the EPCQ   --
--            is written (8 pages write actions=2048 bit), the vJTAG_itf can read   --
--            and write 16 bits of it and the FPGA modules can access to it         --
--            (in read mode).                                                       --
--            The EPCQ read and write actions are triggered by the rising edge of   --
--            EPCQrd and EPCQwr, but the read action is also automatically executed --
--            at power up.                                                          --
--            The vJTAG acts on the register via three signals vJTAGAddr, vJTAGdt2Wr--
--            and vJTAGwr. vJTAGAddr selects the register portion where the 16 bit  --
--            vJTAGdt2Wr are written and at the same time it selects also the       --
--            16 bits to send back to the vJTAG_itf module.                         --
--            During EPCQ rd/wr actions, the vJTAG_itf is disabled to write to the  --
--            register(the write action is ignored)                                 -- 
--                                                                                  --
--                                                                                  --
-- History  : Start up version 15/12/2020                                           --
--------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity EPCQ_ParamMngr is
  Port (
         Clock                   : in  std_logic;                             -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                             -- Asynchronous reset signal active high

	      -- vJTAG_itf
	      vJTAGdt2Wr              : in  std_logic_vector(15 downto 0);         -- Parameter to write 
	      vJTAGAddr               : in  std_logic_vector(6 downto 0);          -- Parameter address (from 0 to 127) 
	      vJTAGwr                 : in  std_logic;                             -- Write command
	      vJTAGdtRead             : out std_logic_vector(15 downto 0);         -- Read parameter
 
         -- EPCQ Driver
         EPCQrd                  : in  std_logic;                             -- It triggers the EPCQ reading action of 8 pages
         EPCQwr                  : in  std_logic;                             -- It triggers the EPCQ writing action of 8 pages
         EPCQaddr                : out std_logic_vector(31 downto 0);         -- EPCQ page address
         EPCQ_pgRd               : out std_logic;                             -- EPCQ page read command
         EPCQ_pgWr               : out std_logic;                             -- EPCQ page write command
         EPCQ_SectEr             : out std_logic;                             -- EPCQ sector erase command
         EPCQ_writePg            : out std_logic_vector(2047 downto 0);       -- Data to EPCQ
         EPCQ_readPg             : in  std_logic_vector(2047 downto 0);       -- Data from EPCQ
	      SentProt                : in  std_logic;                             -- It is a pulse active high when the EPCQ Driver sent a protocol	 

         -- Debug only
         param2                  : out std_logic_vector(15 downto 0);
         param127                : out std_logic_vector(15 downto 0)
	  
  );
End entity EPCQ_ParamMngr;

Architecture beh of EPCQ_ParamMngr is
--
-- Constants declaration
--constant Tc_cntPage             : unsigned (3 downto 0):=to_unsigned(8,4);    -- 8 pages = 2048 bits

--
-- Signals declaration
signal tmpParam                 : std_logic_vector(2047 downto 0);            -- parameter register
signal vJTAGdt2Wr_s0            : std_logic_vector(15 downto 0);              -- vJTAGdt2Wr sampled signal
signal vJTAGAddr_s0             : std_logic_vector(6 downto 0);               -- vJTAGAddr sampled signal 
signal vJTAGwr_s0               : std_logic;                                  -- vJTAGwr sampled signal
signal vJTAGwr_s1               : std_logic;                                  -- vJTAGwr_s0 sampled signal
signal vJTAGwr_RE               : std_logic;                                  -- vJTAGwr rising edge
signal EPCQrd_s0                : std_logic;                                  -- EPCQrd sampled signal
signal EPCQrd_s1                : std_logic;                                  -- EPCQrd_s0 sampled signal
signal EPCQrd_RE                : std_logic;                                  -- EPCQrd rising edge
signal EPCQwr_s0                : std_logic;                                  -- EPCQwr sampled signal
signal EPCQwr_s1                : std_logic;                                  -- EPCQwr_s0 sampled signal
signal EPCQwr_RE                : std_logic;                                  -- EPCQwr rising edge
signal EPCQ_readPg_s0           : std_logic_vector(2047 downto 0);            -- EPCQ_readPg sampled signal
signal SentProt_s0              : std_logic;                                  -- SentProt sampled signal	 
signal SentProt_s1              : std_logic;                                  -- SentProt_s0 sampled signal	 
signal SentProt_RE              : std_logic;                                  -- SentProt rising edge

--signal cntPage                  : unsigned (3 downto 0);                      -- EPCQ page counter
signal EPCQ_cgf                 : std_logic;                                  -- It is high when the EPCQ is configured (4 lines)
signal EraseSectorFlag          : std_logic;                                  -- It is high when the erase command has to be generated.

type fsmstate is (WaitSentProtCfg, SetEPCQ_Cmd, WaitSentProt, WaitEPCQ_Cmd);
signal fsm_state                : fsmstate;

begin

--
-- Inputs sampling process
p_InSamp: process (Reset,Clock)
begin
  if (Reset='1') then

    vJTAGdt2Wr_s0   <= (others=> '0');
    vJTAGAddr_s0    <= (others=> '0'); 
    vJTAGwr_s0      <= '0';
    vJTAGwr_s1      <= '0';
    vJTAGwr_RE      <= '0';    

    EPCQrd_s0       <= '0';
    EPCQrd_s1       <= '0';
    EPCQrd_RE       <= '0';
    EPCQwr_s0       <= '0';
    EPCQwr_s1       <= '0';
    EPCQwr_RE       <= '0';
    EPCQ_readPg_s0  <= (others=> '0'); 
    SentProt_s0     <= '0';
    SentProt_s1     <= '0';
    SentProt_RE     <= '0';

  elsif (Clock'event and Clock='1') then

    vJTAGdt2Wr_s0   <= vJTAGdt2Wr;
    vJTAGAddr_s0    <= vJTAGAddr; 
    vJTAGwr_s0      <= vJTAGwr;
    vJTAGwr_s1      <= vJTAGwr_s0;
    vJTAGwr_RE      <= vJTAGwr_s0 and not vJTAGwr_s1;    

    EPCQrd_s0       <= EPCQrd;
    EPCQrd_s1       <= EPCQrd_s0;
    EPCQrd_RE       <= EPCQrd_s0 and not EPCQrd_s1;
    EPCQwr_s0       <= EPCQwr;
    EPCQwr_s1       <= EPCQwr_s0;
    EPCQwr_RE       <= EPCQwr_s0 and not EPCQwr_s1;
    EPCQ_readPg_s0  <= EPCQ_readPg; 
    SentProt_s0     <= SentProt;
    SentProt_s1     <= SentProt_s0;
    SentProt_RE     <= SentProt_s0 and not SentProt_s1;
    
  end if;
end process;


--
-- tmpParam process
p_tmpParam: process(Reset,Clock)
begin
  if (Reset='1') then

    tmpParam   <= (others=>'0'); 

  elsif (Clock'event and Clock='1') then
    
    if (EPCQrd_s0 ='1' and SentProt_RE='1') then

       -- tmpParam written by EPCQ driver
       --tmpParam(255 + to_integer(cntPage)*256 downto to_integer(cntPage)*256)  <= EPCQ_readPg_s0;
       tmpParam  <= EPCQ_readPg_s0;

    elsif (vJTAGwr_RE='1' and EPCQrd_s0 ='0' and EPCQwr_s0 ='0' and EPCQ_cgf = '0') then

       -- tmpParam written by vJTAG_itf
       tmpParam(15 + to_integer(unsigned(vJTAGAddr_s0))*16 downto to_integer(unsigned(vJTAGAddr_s0))*16)  <= vJTAGdt2Wr_s0;

    end if;    
  end if;
end process p_tmpParam;

--
-- FSM Process
pfsm: process (Reset,Clock)
begin
  if (Reset='1') then
    -- State evolution
    fsm_state          <= WaitSentProtCfg;
    
    -- Signals evolution
    EPCQ_cgf           <= '0';
    --cntPage            <= (others=>'0');
    EPCQ_pgRd          <= '0';
    EPCQ_pgWr          <= '0';
    EPCQ_SectEr        <= '0';
    EraseSectorFlag    <= '0';
  elsif (Clock'event and Clock='1') then

    EPCQ_pgRd          <= '0';
    EPCQ_pgWr          <= '0';
    EPCQ_SectEr        <= '0';

    case fsm_state is
  
      when WaitSentProtCfg =>

        if (SentProt_RE='1') then

          fsm_state          <= SetEPCQ_Cmd;

          EPCQ_cgf           <= '1';
        end if;
	
      when SetEPCQ_Cmd =>
        
        fsm_state          <= WaitSentProt;

        if (EPCQ_cgf = '1' or EPCQrd_s0 ='1') then
          EPCQ_pgRd          <= '1';
        elsif (EraseSectorFlag = '1') then
          EPCQ_SectEr        <= '1';
        elsif (EPCQwr_s0 ='1') then
          EPCQ_pgWr          <= '1';
        end if;
 	
      when WaitSentProt =>

         if (SentProt_RE='1') then
--           if (EraseSectorFlag = '1') then
--             fsm_state          <= WaitEPCQ_Cmd;
--           else

--             if (cntPage < Tc_cntPage-1) then
--               fsm_state          <= SetEPCQ_Cmd;
--               cntPage            <= cntPage + 1;
--             else
--               fsm_state          <= WaitEPCQ_Cmd;
--             end if;

--          end if;

          fsm_state          <= WaitEPCQ_Cmd;

        end if;

      when WaitEPCQ_Cmd =>

       if (EPCQrd_RE = '1' or EPCQwr_RE = '1' or EraseSectorFlag = '1') then
         fsm_state          <= SetEPCQ_Cmd;
       end if;

       if (EPCQwr_RE = '1') then
         EraseSectorFlag    <= '1';
       else
         EraseSectorFlag    <= '0';
       end if;

       
       EPCQ_cgf           <= '0';
       --cntPage            <= (others=>'0');
	
      when others => 
        -- State evolution
        fsm_state          <= WaitSentProtCfg;
    
        -- Signals evolution
        EPCQ_cgf           <= '0';
        --cntPage            <= (others=>'0');
        EPCQ_pgRd          <= '0';
        EPCQ_pgWr          <= '0';
    end case;
  
  end if;
end process;


--
-- outputs
vJTAGdtRead  <= tmpParam(15 + to_integer(unsigned(vJTAGAddr_s0))*16 downto to_integer(unsigned(vJTAGAddr_s0))*16);

--EPCQ_writePg <= tmpParam(255 + to_integer(cntPage)*256 downto to_integer(cntPage)*256);
--EPCQaddr     <= std_logic_vector(to_unsigned(to_integer(cntPage)*256,32));

EPCQ_writePg <= tmpParam;
EPCQaddr     <= (others=>'0');

param2       <= tmpParam(47 downto 32);
param127     <= tmpParam(2047 downto 2032);

end beh;

