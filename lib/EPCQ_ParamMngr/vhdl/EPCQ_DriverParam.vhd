------------------------------------------------------------------------------------------------------------------------
-- File name: EPCQ_DriverParam.vhd                                                                                    --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 16/12/2020                                                                                              --
--                                                                                                                    --
-- Comments : This module is based on ACU_EPCQ_Driver one. In this case it is not required to write in the EPCQ a     --
--            FPGA configuration file, but just standard data(parameters).                                            --
--            The instruction set is reduce to 4 commands (configuration, read page, write page and erase sector).    --
--            It is up to the EPCQ_ParamMngr to manage them.                                                          --
--            After power up, the EPCQ is configured to operate with 4 data lines, after that it waits for read or    --
--            write commands.                                                                                         --
--            When the EPCQ_DriverParam is acting on the SPI bus, a busy signal is set to one and all the recived     --
--            commands are ignored.                                                                                   --
--            Also in this module, the chip protocol interface is managed by an instance of                           --
--            ACU_Generic_6lines_SPI_Master component.                                                                --
--                                                                                                                    --
-- History  : Start up version 16/12/2020                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

USE work.ACU_package.all;

Entity EPCQ_DriverParam is
  Generic (
    gSCLK_High          : unsigned(7 downto 0):="00000001";                  -- SCLK high semiperiod length (in number of Clock pulses) 
    gSCLK_Low           : unsigned(7 downto 0):="00000001";                  -- SCLK low  semiperiod length (in number of Clock pulses)
    gCSn2SCLK_SetupTime : unsigned(7 downto 0):="00000010";                  -- nrSCLK_pulses signal value.
    gFlashAddrWidth     : integer range 8 to 32 :=32                         -- Flash address length
  );
  Port(
    Clock               : in  std_logic;                                     -- Clock signal
    Reset               : in  std_logic;                                     -- Asynchronous reset signal active high

    EPCQaddr            : in  std_logic_vector(gFlashAddrWidth-1 downto 0);  -- EPCQ page address
    EPCQ_pgRd           : in  std_logic;                                     -- EPCQ page read command
    EPCQ_pgWr           : in  std_logic;                                     -- EPCQ page write command
    EPCQ_SectEr         : in  std_logic;                                     -- EPCQ erase sector command

    EPCQ_Cfg            : in  std_logic;                                     -- EPCQ configuration command

    EPCQ_writePg        : in  std_logic_vector(2047 downto 0);               -- Data to EPCQ
    EPCQ_readPg         : out std_logic_vector(2047 downto 0);               -- Data from EPCQ
    SentProt            : out std_logic;                                     -- It is a pulse active high when the EPCQ Driver sent a protocol	 

    Busy                : out std_logic;                                     -- It is high during actions on SPI bus
    Tx_h_Rx_l           : out std_logic_vector(3 downto 0);                  -- It defines the 4 SPI data lines direction.

    -- SPI
    SCLK                : out std_logic;                                     -- SPI clock signal
    SCS_n               : out std_logic;                                     -- Active low SPI Chip Select signal
    SDT_in              : in  std_logic_vector(3 downto 0);                  -- Serial SPI incoming data 
    SDT_out             : out std_logic_vector(3 downto 0)                   -- Serial SPI outgoing data 
  );

end Entity EPCQ_DriverParam;

architecture beh of EPCQ_DriverParam is

--
-- Constants declaration
constant PwrUpAutoCfgTriggerCond : std_logic_vector(15 downto 0):=x"8000";

constant SetWrEN          : array12bits(3 downto 0):= (x"006",x"000",x"008",x"008");  -- Set write enable command. It is necessary before every
                                                                                      -- write or erase action.
constant ClrWrEN          : array12bits(3 downto 0):= (x"004",x"000",x"008",x"008");  -- Clear write enable command. It is necessary after 
                                                                                      -- every write or erase action.
constant WrSVR            : array12bits(3 downto 0):= (x"081",x"04B",x"010",x"010");  -- Write set volatile register: The dummy clock cycle 
                                                                                      -- number is set to 4.
constant WrSEVR           : array12bits(3 downto 0):= (x"061",x"06F",x"010",x"010");  -- Write set enhanced volatile register: The DQ3 data line
                                                                                      -- is enabled and the protocol is changed to quad mode.
constant SetEs            : array12bits(3 downto 0):= (x"0D8",x"000",x"028",x"028");  -- Set erase sector command.

constant SetWrPg          : array12bits(3 downto 0):= (x"012",x"000",x"828",x"828");  -- Set Write Page command.
constant SetRdPg          : array12bits(3 downto 0):= (x"00C",x"000",x"838",x"038");  -- Set Read Page command.
constant SetRdFlag        : array12bits(3 downto 0):= (x"070",x"000",x"010",x"008");  -- Set Read Flag status register command.


constant Tc_cntSleep      : unsigned (7 downto 0):= to_unsigned(12,8);                -- 120 ns SCS_n high between two protocols
--constant Tc_cntSleep            : unsigned (7 downto 0):= to_unsigned(25,8);           -- 250 ns SCS_n high between two protocols


constant zeros            : std_logic_vector(2087 downto 0):=(others=>'0');

--
-- Signals declaration

signal PwrUpAutoCfg       : std_logic_vector (15 downto 0);                    -- This is a shift vector used to trigger an EPCQ configuration
                                                                               -- afterthe FPGA power up. It basically delays the Reset signal
                                                                               -- falling edge of 16 clock cycles.
		
signal cntSleep           : unsigned (7 downto 0);                             -- Sleep counter between two protocols
signal Tc_cntProt         : unsigned (2 downto 0);                             -- Protocol terminal counter for each command
signal cntProt            : unsigned (2 downto 0);                             -- Protocol counter for each command

signal StartProt          : std_logic;                                           -- Start single protocol transmission.
signal Dt1Line_l_4Lines_h : std_logic;                                           -- It defines the protocol mode: 
                                                                                 --   0 => Extended SPI
                                                                                 --   1 => Quad SPI
signal SFL_DtLineDir      : std_logic;                                           -- When high, the SPI module is receiving data
                                                                                 -- When low, the SPI module is sending data

signal Prot2Send          : std_logic_vector(2087 downto 0);                     -- Protocol to send
signal nrSCLK_Tx_pulses   : std_logic_vector (11 downto 0);                      -- Number of SCLK pulses Relative to the data to send
signal nrSCLK_pulses      : std_logic_vector (11 downto 0);                      -- Number of SCLK pulses for each NewVal received

signal StopProt           : std_logic;                                           -- It is high when the SCS_n signal has to be set to one.
signal ProtTransmitted    : std_logic;                                           -- It is high evey time a protocol is transmetted. It is used
                                                                                 -- to increment cntProt.
signal Dt_Received        : std_logic_vector (2047 downto 0);                       -- Data received from flash: it can be both the flash busy signal 
                                                                                 -- (whenS etRdFlag command is executed) and the read flash Byte.
                                            
signal EPCQ_Cfg_s0        : std_logic;                                           -- EPCQ_Cfg sampled signal
signal EPCQ_Cfg_RE        : std_logic;                                           -- EPCQ_Cfg rising edge

type matrix6x4 is array (5 downto 0) of array12bits(3 downto 0);
signal protTable                : matrix6x4;

type fsmState is (WaitForSt, StartProtocol, WaitProtEnd, SleepBetween2Prots);
signal fsm_state                : fsmState;

--
-- Component declaration
Component ACU_Generic_6lines_SPI_Master is
  Generic (
         gSCLK_High              : unsigned(7 downto 0):="00000001";              -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0):="00000001";              -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0):="00000010";              -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
	      gDataWidthTx            : integer range 0 to 2104:= 48;                   -- Data to send width. It has to be lower or equal to 
	                                                                               -- nrSCLK_pulses signal value.
	      gDataWidthRx            : integer range 0 to 2104:=8                      -- Data to receive width. It has to be lower or equal to 
	                                                                               -- nrSCLK_pulses signal value.
  );
  Port(
         Clock                   : in  std_logic;                                 -- Clock signal
         Reset                   : in  std_logic;                                 -- Asynchronous reset signal active high
         StartProt               : in  std_logic;                                 -- One clock cycle pulse active high when there is a new protocol
	                                                                               -- to send.
         StopProt                : in  std_logic;                                 -- One clock cycle pulse active high when the under transmission 
                                                                                  -- protocol has to be interrupted.
											     
         Dt_In                   : in std_logic_vector(gDataWidthTx-1 downto 0);  -- Data to send.
         
	      nrSCLK_Tx_pulses        : in std_logic_vector (11 downto 0);             -- Number of SCLK pulses Relative to the data to send
         nrSCLK_pulses           : in std_logic_vector (11 downto 0);             -- Number of SCLK pulses for each NewVal received
         Dt1Line_l_4Lines_h      : in std_logic;                                  -- It defines the number of SPI data line to drive:
                                                                                  -- When 0 => 1 data line out, 1 data line in
                                                                                  -- When 1 => 4 data line out, 4 data line in
         NewDt_Received          : out std_logic;                                 -- One clock cycle pulse active high when there is a new
                                                                                  -- Dt_Received 
         Dt_Received             : out std_logic_vector(gDataWidthRx-1 downto 0); -- Data to send.

         SFL_DtLineDir           : out std_logic;                                 -- SDT_out driver direction.
	      ProtTransmitted         : out std_logic;                                 -- It is high when all the nrSCLK_pulses have been sent.
	 	 
         SCLK                    : out std_logic;                                 -- SPI clock signal
         SCS_n                   : out std_logic;                                 -- Active low SPI Chip Select signal
         SDT_in                  : in std_logic_vector(3 downto 0);               -- Serial SPI incoming data 
         SDT_out                 : out std_logic_vector(3 downto 0)               -- Serial SPI outgoing data 
  );
end component ACU_Generic_6lines_SPI_Master;

begin

p_dtSamp:process(Clock,Reset)
begin
  if (Reset='1') then

    PwrUpAutoCfg              <= (others=>'1');

    EPCQ_Cfg_s0               <= '0';
    EPCQ_Cfg_RE               <= '0';
  elsif (Clock'event and Clock='1') then
    
    EPCQ_Cfg_s0               <= EPCQ_Cfg;
    EPCQ_Cfg_RE               <= EPCQ_Cfg and not EPCQ_Cfg_s0;

    PwrUpAutoCfg(0)           <= '0';
    PwrUpAutoCfg(15 downto 1) <= PwrUpAutoCfg(14 downto 0);

    
  end if;
end process p_dtSamp;

--
-- Protocol terminal counter and Fill protTable
p_Tc_cntProt: process(Clock,Reset)
begin
  if (Reset='1') then

--    Tc_cntProt  <= (others=>'0');
--    protTable   <= (others=>(others=>(others=>'0')));
      -- EPCQ cfg
      Tc_cntProt    <= to_unsigned(6,3); 
      
      protTable(0)  <= SetWrEN;
      protTable(1)  <= WrSVR;
      protTable(2)  <= ClrWrEN;
      protTable(3)  <= SetWrEN;
      protTable(4)  <= WrSEVR;
      protTable(5)  <= ClrWrEN;

    
  elsif (Clock'event and Clock='1') then

    if (EPCQ_Cfg_RE = '1') then

      Tc_cntProt    <= to_unsigned(6,3); 
      
      protTable(0)  <= SetWrEN;
      protTable(1)  <= WrSVR;
      protTable(2)  <= ClrWrEN;
      protTable(3)  <= SetWrEN;
      protTable(4)  <= WrSEVR;
      protTable(5)  <= ClrWrEN;

    elsif (EPCQ_SectEr = '1') then
      -- Erase sector
	   Tc_cntProt    <= to_unsigned(4,3);
      
      protTable(0)  <= SetWrEN;
	   protTable(1)  <= SetEs;
	   protTable(2)  <= SetRdFlag;
	   protTable(3)  <= ClrWrEN;

      protTable(4)  <= WrSEVR;
      protTable(5)  <= ClrWrEN;

    elsif (EPCQ_pgWr = '1') then
      -- Write page
	   Tc_cntProt    <= to_unsigned(4,3);

      protTable(0)  <= SetWrEN;
	   protTable(1)  <= SetWrPg;
	   protTable(2)  <= SetRdFlag;
	   protTable(3)  <= ClrWrEN;

      protTable(4)  <= WrSEVR;
      protTable(5)  <= ClrWrEN;
          
    elsif(EPCQ_pgRd = '1') then
      -- Read page
	   Tc_cntProt    <= to_unsigned(1,3);    

	   protTable(0)  <= SetRdPg;

	   protTable(1)  <= SetWrPg;
	   protTable(2)  <= SetRdFlag;
	   protTable(3)  <= ClrWrEN;
      protTable(4)  <= WrSEVR;
      protTable(5)  <= ClrWrEN;
          
--    else
--      -- EPCQ cfg
--      Tc_cntProt    <= to_unsigned(6,3); 
      
--      protTable(0)  <= SetWrEN;
--      protTable(1)  <= WrSVR;
--      protTable(2)  <= ClrWrEN;
--      protTable(3)  <= SetWrEN;
--      protTable(4)  <= WrSEVR;
--      protTable(5)  <= ClrWrEN;

    end if;

  end if;
end process p_Tc_cntProt;

--
-- FSM
p_FSM:process(Clock,Reset)
begin
  if (Reset='1') then
    -- State evolution
    fsm_state           <= WaitForSt;

    -- Signals evolution
    cntProt             <= (others=>'0');
    cntSleep            <= (others=>'0');
    Busy                <= '0';
    StartProt           <= '0';
    Dt1Line_l_4Lines_h  <= '0';
    SentProt            <= '0';
    StopProt            <= '0';
  elsif(Clock'event and Clock='1') then
    
    case fsm_state is
      when WaitForSt =>

        -- State evolution
        if (PwrUpAutoCfg = PwrUpAutoCfgTriggerCond or EPCQ_SectEr = '1' or EPCQ_pgWr = '1' or EPCQ_pgRd = '1' or EPCQ_Cfg_RE = '1') then
          fsm_state  <= StartProtocol;
          
        end if;

        -- Signals evolution
        cntSleep            <= (others=>'0');
	     Busy                <= '0';
        SentProt            <= '0';
      when StartProtocol  =>
      
	     -- State evolution
	     fsm_state  <= WaitProtEnd;

	
        -- Signals evolution
        StartProt           <= '1';
	     Busy                <= '1';
      when WaitProtEnd  =>
	     -- State evolution
        if (ProtTransmitted = '1') then
	       fsm_state  <= SleepBetween2Prots;
	       StopProt   <= '1';
	     end if;
	 
        -- Signals evolution
        StartProt           <= '0';
		
      when SleepBetween2Prots  =>

        -- State evolution
        if (cntSleep < Tc_cntSleep-1) then
	       cntSleep <= cntSleep + 1;
	     else
	
	       if (cntProt < Tc_cntProt-1) then
	  
	         if (protTable(to_integer(cntProt))(3)(7 downto 0) /= x"70") then
	           cntProt     <= cntProt + 1;
	         else
              --!!!!!!!!!!!!! CHECK THIS!!!!!!!!!!!!!! it can be Dt_Received(2047)
	           if (Dt_Received(7)='1') then
	             cntProt     <= cntProt + 1;
	           end if;
	         end if;
	  
	         fsm_state   <= StartProtocol;
	       else
	         fsm_state   <= WaitForSt;
	         cntProt     <= (others => '0');
            SentProt    <= '1';
	       end if;
          
	       if (cntProt = "100") then
	        -- From extended to quad switch
	        Dt1Line_l_4Lines_h  <= '1';
	       end if;
          
	       cntSleep <= (others => '0');
	  
	     end if;

        StopProt  <= '0';

      when others =>

        -- State evolution
        fsm_state           <= WaitForSt;

        -- Signals evolution
        cntProt             <= (others=>'0');
        cntSleep            <= (others=>'0');
        Busy                <= '0';
        StartProt           <= '0';
        StopProt            <= '0';

    end case;

  end if;

end process p_FSM;

--
-- protocol to send
p_prt2send:process(Clock,Reset)
begin
  if (Reset='1') then
    Prot2Send         <= (others=>'0');
    nrSCLK_Tx_pulses  <= (others=>'0');
    nrSCLK_pulses     <= (others=>'0');
  elsif(Clock'event and Clock='1') then
    
    nrSCLK_Tx_pulses  <= protTable(to_integer(cntProt))(0);
    nrSCLK_pulses     <= protTable(to_integer(cntProt))(1);
    
    if (Tc_cntProt > "101") then
      --Flash cfg dt
      Prot2Send  <= protTable(to_integer(cntProt))(3)(7 downto 0) & protTable(to_integer(cntProt))(2)(7 downto 0) & zeros(2071 downto 0);
    else
      -- Flash rd/wr/erase
      Prot2Send  <= protTable(to_integer(cntProt))(3)(7 downto 0) & EPCQaddr & EPCQ_writePg;   
    end if;
    
  end if;
end process p_prt2send;

--
-- SPI instance

i_ACU_Generic_6lines_SPI_Master: ACU_Generic_6lines_SPI_Master
  Generic map(
         gSCLK_High              => gSCLK_High,
         gSCLK_Low               => gSCLK_Low,
         gCSn2SCLK_SetupTime     => gCSn2SCLK_SetupTime,
	      gDataWidthTx            => 2088,
	      gDataWidthRx            => 2048
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         StartProt               => StartProt,
	      StopProt                => StopProt,
	 
         Dt_In                   => Prot2Send,
         
	      nrSCLK_Tx_pulses        => nrSCLK_Tx_pulses,
         nrSCLK_pulses           => nrSCLK_pulses,
         Dt1Line_l_4Lines_h      => Dt1Line_l_4Lines_h,
	 
         NewDt_Received          => open,
         Dt_Received             => Dt_Received,

         SFL_DtLineDir           => SFL_DtLineDir,
	      ProtTransmitted         => ProtTransmitted,
	 	 
         SCLK                    => SCLK,
         SCS_n                   => SCS_n,
         SDT_in                  => SDT_in,
         SDT_out                 => SDT_out
  );


--
-- Outputs
Tx_h_Rx_l    <= "1101" when (Dt1Line_l_4Lines_h = '0') else
                SFL_DtLineDir & SFL_DtLineDir & SFL_DtLineDir & SFL_DtLineDir;

EPCQ_readPg  <= Dt_Received;

end beh;
