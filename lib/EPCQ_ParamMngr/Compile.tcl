###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work
pwd

vcom -93 ../../ACU_package.vhd
vcom -93 ../../ACU_SPI/vhdl/ACU_Generic_6lines_SPI_Master.vhd
vcom -93 $path/EPCQ_ParamMngr.vhd
vcom -93 $path/EPCQ_DriverParam.vhd

vcom -93 $path/Tb_EPCQ_ParamMngr.vhd
