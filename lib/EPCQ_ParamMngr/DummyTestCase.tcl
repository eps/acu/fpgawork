restart -f
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/Reset 0 0
run 3 us

# EPCQ configured

# Automatic read after cfg
run 5 us

# Ignored vJTAG wr action
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 1100101011111110 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us

run 5 us

# Read done; wait for EPCQ rd/wr cmds or vJTAG actions

# vJTAG wr action addr=0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGAddr 0000000 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 1100101011111110 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us

# vJTAG wr action addr=1
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGAddr 0000001 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 1011111011101111 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us

# vJTAG wr action addr=2
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGAddr 0000010 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 0001001000110100 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us

# vJTAG wr action addr=3
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGAddr 0000011 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 1010101111001101 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us

# vJTAG wr action addr=127
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGAddr 1111111 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 1111000111001010 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us


# Write EPCQ
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/EPCQwr 1 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/FlashBusy 1 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/FlashBusy 0 0
run 12 us

force -freeze sim:/tb_epcq_parammngr/FlashBusy 1 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/FlashBusy 0 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/EPCQwr 0 0
run 10 us

# Read EPCQ
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/EPCQrd 1 0
run 15 us
force -freeze sim:/tb_epcq_parammngr/FlashBusy 1 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/FlashBusy 0 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/EPCQrd 0 0
run 10 us

# vJTAG wr action addr=0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGAddr 0000000 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 0001001000110100 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us

# vJTAG wr action addr=1
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGAddr 0000001 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 0101011001111000 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us

# vJTAG wr action addr=2
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGAddr 0000010 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 1001101010111100 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us

# vJTAG wr action addr=3
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGAddr 0000011 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 1101111011110000 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us

# vJTAG wr action addr=127
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGAddr 1111111 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGdt2Wr 1100101011111110 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 1 0
run 20 ns
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/vJTAGwr 0 0
run 1 us

force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/EPCQ_writePg 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 0

run 1 us
# Read EPCQ
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/EPCQrd 1 0
run 15 us
force -freeze sim:/tb_epcq_parammngr/FlashBusy 1 0
run 1 us
force -freeze sim:/tb_epcq_parammngr/FlashBusy 0 0
force -freeze sim:/tb_epcq_parammngr/i_EPCQ_ParamMngr/EPCQrd 0 0
run 10 us



