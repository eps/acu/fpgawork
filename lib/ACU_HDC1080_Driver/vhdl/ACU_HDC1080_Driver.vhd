-----------------------------------------------------------------------------------
-- File name: ACU_HDC1080_Driver.vhd                                             --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 01/02/2021                                                         --
--                                                                               --
-- Comments : HDC1080 humidity and temperature chip i2c driven.                  --
--            HDC1080_Temp and  HDC1080_Hum have 14 bits resolution (MSB).       --
--            The SCLK clock frequency is 10 KHz.                                --
--            This driver executes the actions below in loop as soon has the     --
--            Enable signal is set to 1:                                         --
--              * Configure the chip: + operation mode                           --
--                                    + Heater disabled                          --
--                                    + Temperature and Humidity monitored       --
--                                    + 14 bit resolution for both               --
--              * Read temperature and humidity registers.                       --
--              * Read chip status.                                              --
--                                                                               --
--            During the read temperature and humidity action, the chip triggers --
--            the measurements when the register address 0x00 is written in the  --
--            pointer. After that 6,5 ms are necessary to validate and read      --
--            after that the temperature and humidity registers values.          --
--            The data in output are updated every 21,6251 ms.                   --           
--                                                                               --
-- History  : Start up version 01/02/2021                                        --
-----------------------------------------------------------------------------------
--            Redesigned the ACU_HDC1080_Driver:                                 --
--              * ACU_I2C redesign included.                                     --
--              * Removed Chip configuration (the default one is in line to get  --
--                humidity and temperature from the chip.                        --
--              * Removed read chip status.                                      --
--              * The read action is driven by an external timer. With 6.5ms it  --
--                doesn't work, but with 1 second yes. Maybe we can reduce this  --
--                latency (if it is necessary) otherwise I would drive this      --
--                from SW(and save FPGA resources).                              --
--            DR 02/01/2024.                                                     --
-----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.ACU_package_numericLib.all;

Entity ACU_HDC1080_Driver is
  Port (
         Clock                   : in  std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                               -- Asynchronous reset signal active high
         
         Enable                  : in  std_logic;                              -- HDC1080_Driver enable signal active high

         -- i2c pins
         SCLK                    : out std_logic;                              -- i2c clock signal
         SDA                     : inout std_logic_vector(0 downto 0);         -- i2c data signal
         SDA_dir                 : out std_logic;                              -- for debug only	 
         
         -- outputs
         NewHDC1080_Dt           : out std_logic;
         HDC1080_Temp            : out std_logic_vector(15 downto 0);          -- Measured temperature
         HDC1080_Humidity        : out std_logic_vector(15 downto 0)           -- Measured humidity 
	 
  );
End entity ACU_HDC1080_Driver;

Architecture beh of ACU_HDC1080_Driver is
--
-- Constants declaration
constant Tc_cntProt             : unsigned(1 downto 0) :=to_unsigned(2,2);
--constant Tc_cntDtFromHDC1080    : unsigned(2 downto 0) :=to_unsigned(2,3);
constant Tc_cntDtFromHDC1080    : unsigned(2 downto 0) :=to_unsigned(4,3);
--constant Tc_cntAddr_Dt_2_WR     : unsigned(3 downto 0) :=to_unsigned(5,4);
constant Tc_cntAddr_Dt_2_WR     : unsigned(3 downto 0) :=to_unsigned(7,4);

--constant Tc_cntWaitMeas         : unsigned(19 downto 0):=to_unsigned(650000,20);
--constant Tc_cntWaitMeas         : unsigned(19 downto 0):=to_unsigned(1048575,20);
--constant Tc_cntWaitMeas         : unsigned(19 downto 0):=to_unsigned(6500,20);


--constant Addr_Dt_2_WR_array     : array8bits(4 downto 0):=(x"00",x"00",x"81",
--                                                           x"00",x"80");
							    
--constant WordPerProt            : array8bits(1 downto 0):=(x"03",
--                                                           x"02");

constant Addr_Dt_2_WR_array     : array8bits(6 downto 0):=(x"00",x"00",x"00",x"00",x"81",
                                                           x"00",x"80");
							    
constant WordPerProt            : array8bits(1 downto 0):=(x"05",
                                                           x"02");
							   
--
-- Signals declaration
signal cntProt                  : unsigned(1 downto 0);
signal cntDtFromHDC1080         : unsigned(2 downto 0);
signal cntAddr_Dt_2_WR          : unsigned(3 downto 0);
--signal cntWaitMeas              : unsigned(19 downto 0);

--signal DtFromHDC1080            : array8bits(1 downto 0);
signal DtFromHDC1080            : array8bits(3 downto 0);
signal intAddr_Dt_2_WR          : std_logic_vector(7 downto 0);
signal WxProt                   : std_logic_vector(7 downto 0);
signal intRdWr_l                : std_logic;

signal RdData                   : std_logic_vector(7 downto 0);
signal NewRdData                : std_logic;
signal Dt2WrReq                 : std_logic;
signal EndProtocol              : std_logic;	 

--signal StartEn                  : std_logic;                          -- it is high for one clock pulse when cntWaitMeas=Tc_cntWaitMeas-1 starting the i2c master instance.


signal Enable_s0                : std_logic;
signal Enable_s1                : std_logic;
signal Enable_RE                : std_logic;

signal SDA_out                  : std_logic_vector(0 downto 0);
signal SDA_in                   : std_logic_vector(0 downto 0);
signal SDA_BuffDir              : std_logic_vector(0 downto 0);
--
-- Component
Component  ACU_I2C is
generic  
   (
         gSCLK_Period            : unsigned(15 downto 0):=x"1388"              -- SCLK period length (in number of Clock pulses) 50us(20KHz) with Clock @ 10ns(100MHz)
									       
         );
  Port (
         Clock                   : in std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in std_logic;                               -- Asynchronous reset signal active high
         StartEn                 : in std_logic;                               -- Start enable command(1 Clock pulse active high)
         
         RdWr_l                  : in std_logic;                               -- command type 1= RD; 0= WR.
         Addr_Dt_2_WR            : in std_logic_vector(7 downto 0);            -- Address/Data to write
         nrWord2Send             : in std_logic_vector(7 downto 0);            -- Number of word(8bits) pro i2c package (2 to set the register to read; 3 to read the register content).
         
         NewRdData               : out std_logic;                              -- New read data pulse.									       
         RdData                  : out std_logic_vector(7 downto 0);           -- Read data
         EndProtocol             : out std_logic;                              -- Single clock pulse signal active high when the stop is sent or when a NACK from slave is received.
         
         Dt2WrReq                : out std_logic;                              -- in Wr mode, it asks for new data to write.
         -- i2c pins
         SCLK                    : out std_logic;                              -- i2c clock signal
         SDA_in                  : in std_logic;                               -- i2c incoming data line
         SDA_out                 : out std_logic;                              -- i2c outgoing data line
         SDA_dir                 : out std_logic                               -- buffer three states driver line: 0=> slave acting on the i2c SDA line
                                                                               --                                  1=> master acting on the i2c SDA line
         
  );

end component ACU_I2C;

Component ACU_BidirBuffer IS
	PORT
	(
		datain		: IN STD_LOGIC_VECTOR (0 DOWNTO 0);
		oe		: IN STD_LOGIC_VECTOR (0 DOWNTO 0);
		dataio		: INOUT STD_LOGIC_VECTOR (0 DOWNTO 0);
		dataout		: OUT STD_LOGIC_VECTOR (0 DOWNTO 0)
	);
END component ACU_BidirBuffer;

begin


--
-- Data from HDC1080
p_DtFromHDC1080: process (Reset,Clock)
begin
  if (Reset='1') then
    cntDtFromHDC1080  <= (others=>'0');
    DtFromHDC1080     <= (others=>(others=>'0'));
    
    
    Enable_s0  <= '0';
    Enable_s1  <= '0';
    Enable_RE  <= '0';
  elsif (Clock'event and Clock='1') then

    Enable_s0  <= Enable;
    Enable_s1  <= Enable_s0;
    Enable_RE  <= Enable_s0 and not Enable_s1;

    if (NewRdData= '1') then
      DtFromHDC1080(to_integer(cntDtFromHDC1080))  <= RdData;
      
      if (cntDtFromHDC1080 < Tc_cntDtFromHDC1080 - 1) then
        cntDtFromHDC1080  <= cntDtFromHDC1080 + 1;
      else
        cntDtFromHDC1080  <= (others=>'0');
      end if;
      
    end if;
   
  end if;
end process;

--
-- Addr_Dt_2_WR process
p_Addr_Dt_2_WR: process (Reset,Clock)
begin
  if (Reset='1') then
    cntAddr_Dt_2_WR  <= (others=>'0');
    intAddr_Dt_2_WR  <= (others=>'0');
  elsif (Clock'event and Clock='1') then
  
    intAddr_Dt_2_WR  <= Addr_Dt_2_WR_array(to_integer(cntAddr_Dt_2_WR));
    
    if (Dt2WrReq= '1') then
      
      if (cntAddr_Dt_2_WR < Tc_cntAddr_Dt_2_WR - 1) then
        cntAddr_Dt_2_WR  <= cntAddr_Dt_2_WR + 1;
      else
        cntAddr_Dt_2_WR  <= (others=>'0');
      end if;
     
    end if;
        
  end if;
end process;

--
-- protocol counter
p_cntProt: process (Reset,Clock)
begin
  if (Reset='1') then
    cntProt         <= (others=>'0');
    WxProt          <= (others=>'0');
    intRdWr_l       <= '0';
    NewHDC1080_Dt   <= '0';

  elsif (Clock'event and Clock='1') then

    WxProt         <= WordPerProt(to_integer(cntProt));
    NewHDC1080_Dt  <= '0';

    if (EndProtocol= '1') then
      if (cntProt < Tc_cntProt - 1) then
        cntProt        <= cntProt + 1;
      else
        cntProt        <= (others=>'0');
        NewHDC1080_Dt  <= '1';
      end if;      
    end if;
    
    if (cntProt > 0) then
      intRdWr_l  <= '1';
    else
      intRdWr_l  <= '0';  
    end if;
    
  end if;
end process;

--
-- enable i2c process
--p_i2cEn: process (Reset,Clock)
--begin
--  if (Reset='1') then

--    cntWaitMeas  <= (others=>'0');
--    StartEn      <= '0';

--  elsif (Clock'event and Clock='1') then
--    if (Enable='1') then
--      StartEn      <= '0';
--      if (cntWaitMeas < Tc_cntWaitMeas-1) then
--        cntWaitMeas  <= cntWaitMeas + 1;
--      else
--        cntWaitMeas  <= (others=>'0');
--        StartEn      <= '1';
--      end if;
--    else
--      cntWaitMeas  <= (others=>'0');      
--      StartEn      <= '0';
--    end if;
--  end if;
--end process;

--
-- i2c Instance
i_ACU_I2C : ACU_I2C
  Port map (
  
         Clock                   => Clock,
         Reset                   => Reset,
         StartEn                 => Enable_RE,        
--         StartEn                 => StartEn,                 

         RdWr_l                  => intRdWr_l,
         EndProtocol             => EndProtocol,
         
         Addr_Dt_2_WR            => intAddr_Dt_2_WR,
         nrWord2Send             => WxProt,

         NewRdData               => NewRdData,
         RdData                  => RdData,

         Dt2WrReq                => Dt2WrReq,

	 	 
         -- i2c pins
         SCLK                    => SCLK,
         SDA_in                  => SDA_in(0),
         SDA_out                 => SDA_out(0),
         
         SDA_dir                 => SDA_BuffDir(0)

  );

i_ACU_BidirBuffer:ACU_BidirBuffer
  port map(
    datain   => SDA_out,
    oe       => SDA_BuffDir,
    dataio   => SDA,
    dataout  => SDA_in	
  );
  
  
--
-- outputs
HDC1080_Temp	  <= DtFromHDC1080(0) & DtFromHDC1080(1);
HDC1080_Humidity  <= DtFromHDC1080(2) & DtFromHDC1080(3);

SDA_Dir           <= SDA_BuffDir(0);

end beh;
