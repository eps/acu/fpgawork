-----------------------------------------------------------------------------------
-- File name: ACU_I2C.vhd                                                         --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 04/05/2018                                                         --
--                                                                               --
-- Comments : generic 2 wires i2c                                                -- 
--                                                                               --
-- History  : Start up version 04/05/2018                                        --
-----------------------------------------------------------------------------------
--            Added IncomingStartRd signal: The read action is usually made of 2 --
--            steps                                                              --
--                * Write the address to read                                    --
--                * Send the read command                                        --
--            In case the write address is also triggering a measuring action,   --
--            it could be necessary to wait till the read action is complete     --
--            before accessing the register.                                     --
--            With IncomingStartRd it is known when the start read protocol is   --
--            going to be generated and acting on StartEn signal, it is possible --
--            to freeze the dr_i2c for a time configurable on the wrap module.   --
--            The IncomingStartRd signal can be ignored on the wrap module if it --
--            is not usefull. DR 01/02/2021                                      --
-----------------------------------------------------------------------------------
--            Redesigned the driver:                                             --
--              * Removed gCtrlByte-> the info is redundant.                     --
--              * SCLK period defined in number of Clock pulses.                 --
--              * Added nrWord2Send signal-> it defines how many word(8bits)     --
--                have to be generated pro i2c package protocol (between start   --
--                and stop commands).                                            --
--              * Removed NACK_Alarm state (redundant)                           --
--              * Splitted SDA bidin in SDA_in amd SDA_out signals. This will    --
--                require a open drain buffer in the wrap.                       --
--              * During a read action, the NACK from master is generated after  --
--                the last byte read (nrWord2Send).                              --
--            DR 02/01/2024.                                                     --
-----------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_I2C is
generic  
   (
         gSCLK_Period            : unsigned(15 downto 0):=x"1388"              -- SCLK period length (in number of Clock pulses) 50us(20KHz) with Clock @ 10ns(100MHz)
									       
         );
  Port (
         Clock                   : in std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in std_logic;                               -- Asynchronous reset signal active high
         StartEn                 : in std_logic;                               -- Start enable command(1 Clock pulse active high)
         
         RdWr_l                  : in std_logic;                               -- command type 1= RD; 0= WR.
         Addr_Dt_2_WR            : in std_logic_vector(7 downto 0);            -- Address/Data to write
         nrWord2Send             : in std_logic_vector(7 downto 0);            -- Number of word(8bits) pro i2c package.
         
         NewRdData               : out std_logic;                              -- New read data pulse.									       
         RdData                  : out std_logic_vector(7 downto 0);           -- Read data
         
         Dt2WrReq                : out std_logic;                              -- in Wr mode, it asks for new data to write.
         EndProtocol             : out std_logic;                              -- Single clock pulse signal active high when the stop is sent or when a NACK from slave is received.
         
         -- i2c pins
         SCLK                    : out std_logic;                              -- i2c clock signal
         SDA_in                  : in std_logic;                               -- i2c incoming data line
         SDA_out                 : out std_logic;                              -- i2c outgoing data line
         SDA_dir                 : out std_logic                               -- buffer three states driver line: 0=> slave acting on the i2c SDA line
                                                                               --                                  1=> master acting on the i2c SDA line
         
  );
End entity ACU_I2C;

Architecture beh of ACU_I2C is
--
-- Constants declaration

constant SCLK_H_Period          : unsigned(15 downto 0):='0' & gSCLK_Period(15 downto 1);
constant SCLK_Q_Period          : unsigned(15 downto 0):="00" & gSCLK_Period(15 downto 2);
constant SCLK_3Q_Period         : unsigned(15 downto 0):=SCLK_H_Period+SCLK_Q_Period;


--
-- Signals declaration

signal cnt_SCLK                 : unsigned(15 downto 0);                   -- SCLK period counter
signal cnt_Byte                 : unsigned(2 downto 0);                    -- Byte (8 bit) counter
signal cnt_Word                 : unsigned(7 downto 0);                    -- Word (8 bit) per pakage to send counter
signal NACK_fromSlv             : std_logic;                               -- it is high when a NACK is received
signal EnSCLK_flag              : std_logic;                               -- Active high, it enables the SCLK generation.
signal SDA_in_sx                : std_logic_vector(3 downto 0);            -- i2c incoming data line sampling vector


type fsmstate is (WaitForStart, SetStart, ManageByte, ManageACK, SetStop);
signal fsm_state                : fsmstate;

begin

--
-- SCLK process
p_SCLKgen: process (Reset,Clock)
begin
  if (Reset='1') then
    cnt_SCLK    <= (others=>'0');
    SCLK        <= '1';
    
  elsif (Clock'event and Clock='1') then
  
    if (EnSCLK_flag='1') then
    
      if (cnt_SCLK < gSCLK_Period-1) then
        cnt_SCLK  <= cnt_SCLK + 1;
      else
        cnt_SCLK  <= (others=>'0');
      end if;
      
      if (cnt_SCLK < SCLK_H_Period) then
        SCLK        <= '1';
      else
        SCLK        <= '0';      
      end if;
    else
      cnt_SCLK    <= (others=>'0');
      SCLK        <= '1';
    end if;
    
  end if;
end process;

--
-- FSM Process
pfsm: process (Reset,Clock)
begin
  if (Reset='1') then
    -- State evolution
    fsm_state     <= WaitForStart;
    
    -- Signals evolution
    EnSCLK_flag   <= '0';    
    cnt_Byte      <= (others=>'1');
    cnt_Word      <= (others=>'0');
    NewRdData     <= '0';
    RdData        <= (others=>'0');
    Dt2WrReq      <= '0';
    SDA_in_sx     <= (others=>'0');
    NACK_fromSlv  <= '0';        
    SDA_out       <= '1';         
    SDA_dir       <= '1';   
    EndProtocol   <= '0';
    
  elsif (Clock'event and Clock='1') then
    SDA_in_sx    <= SDA_in_sx(2 downto 0) & SDA_in;
    
    case fsm_state is
    
      when WaitForStart =>
        EnSCLK_flag   <= '0';
        SDA_out       <= '1';    
        SDA_dir       <= '1';
        cnt_Byte      <= (others=>'1');
        cnt_Word      <= (others=>'0'); 
        NewRdData     <= '0';       
        Dt2WrReq      <= '0';                  
        NACK_fromSlv  <= '0';
        EndProtocol   <= '0';
        
        if (StartEn='1') then
          fsm_state    <= SetStart;
          EnSCLK_flag  <= '1';
        end if;
        
      when SetStart =>
        -- Signals evolution
        SDA_out      <= '0';
        
        if (cnt_SCLK = SCLK_3Q_Period-1) then
          fsm_state    <= ManageByte;
        end if;    

      when ManageByte =>
	
        -- Signals evolution
        SDA_out      <= Addr_Dt_2_WR(to_integer(cnt_Byte));
        NewRdData    <= '0';            
        Dt2WrReq     <= '0'; 
        
        if (cnt_SCLK = SCLK_3Q_Period-1) then
          if (cnt_Byte > 0) then
            cnt_Byte   <= cnt_Byte - 1;
          else
            cnt_Byte   <= (others=>'1');
            fsm_state  <= ManageACK;
            Dt2WrReq   <= '1';      
            
                             
            if (cnt_Word < unsigned(nrWord2Send)) then
              cnt_Word  <= cnt_Word + 1;
            --else
            --  cnt_Word  <= (others=>'0');
            end if;
            
            if (RdWr_l='0') then
              SDA_dir      <= '0';
            else
              --NewRdData    <= '1';
              
              if (cnt_Word > 0) then
                SDA_dir      <= '1';
                NewRdData    <= '1';
              else
                SDA_dir      <= '0'; 
                NewRdData    <= '0';            
              end if;
            end if;
          end if;
        end if;   
       
        if (cnt_SCLK = SCLK_Q_Period-1) then
          RdData(to_integer(cnt_Byte)) <= SDA_in_sx(1);
        end if;
 
      when ManageACK =>
        
        -- Signals evolution
        NewRdData      <= '0';
        Dt2WrReq       <= '0';
        
        --if (cnt_Word > 2) then
        if ((cnt_Word > unsigned(nrWord2Send)-1) and (RdWr_l='1')) then
          -- NACK from master
          SDA_out        <= '1';        
        else 
          -- ACK from master
          SDA_out        <= '0';
        end if;
        
        -- Check for slave ACK
        if ((cnt_Word < 2) or (RdWr_l='0')) then
          if (cnt_SCLK = SCLK_Q_Period-1) then
            if (SDA_in_sx(1) = '1') then
              NACK_fromSlv  <= '1';
            else
              NACK_fromSlv  <= '0';
            end if;
          end if;
        end if;
        
        if (cnt_SCLK = SCLK_3Q_Period-1) then
          if (NACK_fromSlv='1') then
            fsm_state  <= WaitForStart;
            SDA_dir       <= '1';
            EndProtocol   <= '1';
          else
            if (cnt_Word < unsigned(nrWord2Send)) then
              fsm_state    <= ManageByte; 
              if(RdWr_l='0') then
                SDA_dir      <= '1';
              else
                --if (cnt_Word < 3) then
                if (cnt_Word < unsigned(nrWord2Send)) then
                  SDA_dir      <= '0';
                else
                  SDA_dir      <= '1';                
                end if;
              end if;          
            else
              fsm_state    <= SetStop;
              SDA_dir      <= '1';
            end if;
          end if;
        end if;
        
      when SetStop =>
        -- Signals evolution          
        SDA_out        <= '0';
        
        --
        -- Check point for state/signals evolution  
        if (cnt_SCLK = SCLK_H_Period-1 ) then
          -- State evolution          
          EnSCLK_flag  <= '0';
          fsm_state    <= WaitForStart;	  
          EndProtocol  <= '1';
        end if;
	  
      when others =>    
        -- State evolution
        fsm_state     <= WaitForStart;
    
        -- Signals evolution
        EnSCLK_flag   <= '0';    
        cnt_Byte      <= (others=>'1');
        cnt_Word      <= (others=>'0');
        NewRdData     <= '0';
        RdData        <= (others=>'0');
        Dt2WrReq      <= '0';
        SDA_in_sx     <= (others=>'0');
        NACK_fromSlv  <= '0';
        SDA_out       <= '1';         
        SDA_dir       <= '1';   
        EndProtocol   <= '0';
        
    end case;
  
  end if;
end process;

end beh;
