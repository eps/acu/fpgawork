-----------------------------------------------------------------------------------
-- File name: Tb_ACU_HDC1080_Driver.vhd                                          --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 20/12/2023                                                         --
--                                                                               --
-- Comments : ACU_HDC1080_Driver test bench                                      --           
--                                                                               --
-- History  : Start up version 20/12/2023                                        --
-----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


Entity Tb_ACU_HDC1080_Driver is
End entity Tb_ACU_HDC1080_Driver;

Architecture beh of Tb_ACU_HDC1080_Driver is
--
-- Constants declaration
							   
--
-- Signals declaration
signal Clock   :std_logic:='0';
signal Reset   :std_logic:='1';
signal Enable  :std_logic:='0';
signal SCLK    :std_logic;

signal SDA_dir :std_logic;
signal N_ACK   :std_logic:='0';  -- 0=ACK
				 -- 1=NACK
signal SDA     : std_logic_vector(0 downto 0);
--
-- Component
Component ACU_HDC1080_Driver is
  Port (
         Clock                   : in  std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                               -- Asynchronous reset signal active high
         
         Enable                  : in  std_logic;                              -- HDC1080_Driver enable signal active high

         -- i2c pins
         SCLK                    : out std_logic;                              -- i2c clock signal
         SDA                     : inout std_logic_vector(0 downto 0);         -- i2c data signal
         SDA_dir                 : out std_logic;                              -- for debug only
	 
         -- outputs
         NewHDC1080_Dt           : out std_logic;

         HDC1080_Temp            : out std_logic_vector(15 downto 0);          -- Measured temperature
         HDC1080_Humidity        : out std_logic_vector(15 downto 0)           -- Measured humidity 
	 
  );

end component ACU_HDC1080_Driver;

begin

Clock  <= not Clock after 5 ns;

i_ACU_HDC1080_Driver: ACU_HDC1080_Driver
  Port map (
         Clock                   => Clock,
         Reset                   => Reset,
         
         Enable                  => Enable,

         -- i2c pins
         SCLK                    => SCLK,
         SDA                     => SDA,

         SDA_dir                 => SDA_dir,
	 
         -- outputs
         NewHDC1080_Dt           => open,

         HDC1080_Temp            => open,
         HDC1080_Humidity        => open 
	 
  );
  
SDA(0)  <= N_ACK when SDA_dir='0' else
          'Z';

end beh;
