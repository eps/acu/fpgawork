restart -f

run 1 us
# Somehow it doesn't compile the CycloneV ip so I have to force the signal
force -freeze sim:/tb_acu_hdc1080_driver/i_ACU_HDC1080_Driver/i_ACU_I2C/SDA_in 0 0

force -freeze sim:/tb_acu_hdc1080_driver/Reset 0 0
run 200 us

force -freeze sim:/tb_acu_hdc1080_driver/Enable 1 0
run 1 ms
run 200 us

force -freeze sim:/tb_acu_hdc1080_driver/Enable 0 0
run 200 us
force -freeze sim:/tb_acu_hdc1080_driver/Enable 1 0
run 4 ms
run 200 us

force -freeze sim:/tb_acu_hdc1080_driver/Enable 0 0
run 200 us
force -freeze sim:/tb_acu_hdc1080_driver/Enable 1 0
run 1 ms
run 200 us

force -freeze sim:/tb_acu_hdc1080_driver/Enable 0 0
run 200 us
force -freeze sim:/tb_acu_hdc1080_driver/Enable 1 0
run 2 ms
run 200 us



restart -f

run 1 us
# Somehow it doesn't compile the CycloneV ip so I have to force the signal
force -freeze sim:/tb_acu_hdc1080_driver/i_ACU_HDC1080_Driver/i_ACU_I2C/SDA_in 0 0

force -freeze sim:/tb_acu_hdc1080_driver/Reset 0 0
run 200 us

force -freeze sim:/tb_acu_hdc1080_driver/Enable 1 0
run 40 ms
force -freeze sim:/tb_acu_hdc1080_driver/Enable 0 0
run 8 ms

force -freeze sim:/tb_acu_hdc1080_driver/Enable 1 0
run 20 ms

force -freeze sim:/tb_acu_hdc1080_driver/Enable 0 0
run 8 ms

force -freeze sim:/tb_acu_hdc1080_driver/Enable 1 0
run 40 ms
