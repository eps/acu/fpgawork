###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work

#vmap altera_mf work
#vmap cyclonev work




pwd

vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/SimFolder/vhdl/cntFreeWheel.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_package_numericLib.vhd 
  
vcom -93 $path/ACU_csrFIFO_Driver.vhd


vcom -93 $path/Tb_ACU_csrFIFO_Driver.vhd
