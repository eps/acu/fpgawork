restart -f
run 1 us
#Release the reset
force -freeze sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver_RO/Reset 0 0
run 10 us

# Emulate something inside the FIFO in the RO instance
force -freeze sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver_RO/dtFrom_csr(0) 00000000000000000000000000000100 0
run 10 us
force -freeze sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver_RO/dtFrom_csr(0) 00000000000000000000000000000000 0
run 10 us

# Emulate FIFO full in the RO instance
force -freeze sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver_RO/dtFrom_csr(1)(0) 1 0
run 10 us
force -freeze sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver_RO/dtFrom_csr(1)(0) 0 0
run 10 us

# Emulate FIFO full
force -freeze sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver/dtFrom_csr(1)(0) 1 0
run 10 us
force -freeze sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver/dtFrom_csr(1)(0) 0 0
force -freeze sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver/dtFrom_csr(1)(1) 1 0
run 10 us
noforce sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver/dtFrom_csr(1)(1)
run 10 us

# Emulate something inside the FIFO 
force -freeze sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver/dtFrom_csr(0)(31) 1 0
run 10 us
noforce sim:/tb_acu_csrfifo_driver/i_ACU_csrFIFO_Driver/dtFrom_csr(0)
run 10 us

