------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_csrFIFO_Driver.vhd                                                                                  --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 10/01/2025                                                                                              --
--                                                                                                                    --
-- Comments : This module interfaces a memory mapped FIFO Control Status Register.                                    --
--            Visit https://www.intel.com/content/www/us/en/docs/programmable/683130/24-3/software-control.html       --
--            web site for more information concerning the register structure.                                        --
--            Via generic it is possible to configure if the driver has to limit its action on the read only          --
--            registers (fill level and status) (gRdOnly).                                                            --
--            The limit on the read only registers allows the driver to fetch those info with an higher rate          --
--            (110ns instead of 430ns).                                                                               --
--            The FIFO csr read action is triggered by the FIFO data read command input signal.                       --
--            After the FIFO csr interface action, based on the content of csr reg #0 and #1, a FIFO rd command       --
--            is generated. The FIFO rd command is high when there is something to read in the FIFO (csr_reg#0>0) or  --
--            when the FIFO is full (csr_reg#1(0)='1').                                                               --
--            A data valid pulse is generated as well resampling the FIFO rd command.                                 --
--            At the moment the highest FIFO read rate is 2MHz(500ns period), so this module is fast enough to        --
--            generate a FIFO data valid pulse independently of gRdOnly value.                                        --
--                   _  _  _  _  _  _  _  _  _  _  _  _  _  _  _  _  _  _  _  _  _  _  _                              --
--  Clock          _||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_                             --
--  Sched          X    0   X    1   X    2   X    3   X    4   X    5   X2 X3 X4 X5 X 0                              --
--  Sched-RdOnly   X    0   X    1   X    0   X    1   X    0   X    1   X    0   X    1                              --
--                                                                                                                    --
-- History  : Start up version 10/01/2025                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.ACU_package_numericLib.all;

Entity ACU_csrFIFO_Driver is
  Generic(
    --gBaseAddr          : std_logic_vector(31 downto 0):=x"c0000000";  -- csr base address
    gRdOnly            : std_logic:='0'                               -- Read only scheduler enable (Active high)       
                                                                  
  );
  Port(
    Reset              : in std_logic;                                -- Asynchronous reset active high
    Clock              : in std_logic;                                -- System clock
    dtFIFO_rd          : in std_logic;                                -- Data FIFO read command (its period has to be > 80/360ns)
    
    -- cfg
    ClrEvent           : in std_logic_vector(7 downto 0);             -- The event register is write 1 reset
    IntEnable          : in std_logic_vector(7 downto 0);             -- Interrupt enable register value to write
    A_FullThrs         : in std_logic_vector(31 downto 0);            -- Almost full threshold register value to write
    A_EmptyThrs        : in std_logic_vector(31 downto 0);            -- Almost empty threshold register value to write
    
    -- csr interface
    csrFIFOaddr        : out std_logic_vector(2 downto 0);            -- csr address
    csrFIFOrd          : out std_logic;                               -- csr read command
    csrFIFOrdDt        : in std_logic_vector(31 downto 0);            -- Read data from the addressed csr register
    csrFIFOwr          : out std_logic;                               -- csr write command
    csrFIFOwrDt        : out std_logic_vector(31 downto 0);           -- Write data to the addressed csr register
    
    -- csr sampled data
    New_csrData        : out std_logic;                               -- Pulse active high every scheduling end.
    csr_fill_level     : out std_logic_vector(31 downto 0);           -- number of used locations. It is 0 if the FIFO is full.
    csr_status         : out std_logic_vector(7 downto 0);            -- status info.
    csr_event          : out std_logic_vector(7 downto 0);            -- event info.
    csr_intEnable      : out std_logic_vector(7 downto 0);            -- interrupt enable.
    csr_aFullThrs      : out std_logic_vector(31 downto 0);           -- almost full threshold.
    csr_aEmptyThrs     : out std_logic_vector(31 downto 0);           -- almost empty threshold.
    
    -- FIFO rd cnm and data valid
    rdFIFOcmd          : out std_logic;                               -- FIFO read command
    rdDtValid          : out std_logic                                -- hps2FPGA read data valid pulse
  );
end Entity ACU_csrFIFO_Driver;

architecture beh of ACU_csrFIFO_Driver is

--
-- Constants declaration
constant TcCnt             : unsigned(1 downto 0) := "11";
constant TcSchedCntRdOnly  : unsigned(3 downto 0) :=x"1";   
constant TcSchedCntAll     : unsigned(3 downto 0) :=x"9";
constant schedAddr         : array3bits(9 downto 0):=("101","100","011","010",
                                                      "101","100","011","010","001","000");
--
-- Signals declaration
signal schedCnt            : unsigned(3 downto 0);     -- scheduler counter
signal TcSchedCnt          : unsigned(3 downto 0);     -- scheduer terminal counter 
signal cnt                 : unsigned(1 downto 0);     -- internal counter
signal dtFrom_csr          : array32bits(5 downto 0);  -- Data from csr array register

signal dtFIFO_rd_s0        : std_logic;                -- dtFIFO_rd sampled signal
signal dtFIFO_rd_s1        : std_logic;                -- dtFIFO_rd_s0 sampled signal
signal dtFIFO_rd_RE        : std_logic;                -- dtFIFO_rd rising edge
signal csrEnFlag           : std_logic;                -- It is high when dtFIFO_rd_RE=1 and goes low after all the csr registers are scheduled.
signal intRdDtValid        : std_logic;                -- Internal rdDtValid

begin

p_counters: process(Clock, Reset)
begin
  if (Reset='1') then
    schedCnt           <= (others=>'0'); 
    TcSchedCnt         <= (others=>'0');  
    cnt                <= (others=>'0');
    New_csrData        <= '0'; 
    dtFIFO_rd_s0       <= '0';
    dtFIFO_rd_s1       <= '0';
    dtFIFO_rd_RE       <= '0';
    csrEnFlag          <= '0';
    rdDtValid          <= '0';
    intRdDtValid       <= '0';
  elsif(Clock'event and Clock='1') then

    dtFIFO_rd_s0       <= dtFIFO_rd;
    dtFIFO_rd_s1       <= dtFIFO_rd_s0;
    dtFIFO_rd_RE       <= dtFIFO_rd_s0 and not dtFIFO_rd_s1;
    
    rdDtValid          <= intRdDtValid;

    if (gRdOnly='1') then
      -- Read only scheduling
      TcSchedCnt  <= TcSchedCntRdOnly;
    else
      TcSchedCnt  <= TcSchedCntAll;    
    end if;

    if (dtFIFO_rd_RE='1') then
      csrEnFlag          <= '1';
    end if; 
    
    New_csrData     <= '0';  
    intRdDtValid    <= '0';
    if (csrEnFlag='1') then   
      if (cnt < TcCnt) then
        cnt  <= cnt+1;
      else
        cnt   <= (others=>'0');
        if (schedCnt < TcSchedCnt) then
          schedCnt  <= schedCnt + 1;
        else
          schedCnt     <= (others=>'0'); 
          New_csrData  <= '1';
          csrEnFlag    <= '0';
          
          if (unsigned(dtFrom_csr(0)) > 0 or dtFrom_csr(1)(0) = '1') then -- there is something to read or the fifo is full
            intRdDtValid  <= '1';
          end if;
          
        end if;  
      end if;
    end if;
  
  end if;
end process p_counters;


p_csr:process(Clock, Reset)
begin
  if (Reset='1') then
    csrFIFOaddr  <= (others=>'0');
    csrFIFOrd    <= '0';
    csrFIFOwr    <= '0';
    csrFIFOwrDt  <= (others=>'0');
    dtFrom_csr   <= (others=>(others=>'0'));
    
  elsif(Clock'event and Clock='1') then
    -- Addr
    --csrFIFOaddr  <= gBaseAddr(31 downto 8) & schedAddr(to_integer(schedCnt));
    csrFIFOaddr  <= schedAddr(to_integer(schedCnt));    
    
    --rd
    csrFIFOrd    <= '0';
    if (schedCnt < x"6" and cnt < "10" and csrEnFlag='1') then
      csrFIFOrd    <= '1';
    end if;
    
    -- sample the data from csr
    if (schedCnt < x"6" and cnt="10") then
      dtFrom_csr(to_integer(schedCnt))  <= csrFIFOrdDt;
    end if;

    -- dt2wr
    case schedCnt is
      when x"6" =>
        csrFIFOwrDt  <= x"000000" & ClrEvent;
      when x"7" =>
        csrFIFOwrDt  <= x"000000" & IntEnable;      
      when x"8" =>
        csrFIFOwrDt  <= A_FullThrs;            
      when x"9" =>
        csrFIFOwrDt  <= A_EmptyThrs;
      when others =>
        csrFIFOwrDt  <= (others=>'0');
    end case;
    
    --wr
    csrFIFOwr    <= '0';
    if (schedCnt > x"5" and cnt < "01" and csrEnFlag='1') then
      csrFIFOwr    <= '1';
    end if;
    
  end if;
end process p_csr;

-- out
csr_fill_level  <= dtFrom_csr(0);
csr_status      <= dtFrom_csr(1)(7 downto 0);
csr_event       <= dtFrom_csr(2)(7 downto 0);
csr_intEnable   <= dtFrom_csr(3)(7 downto 0);
csr_aFullThrs   <= dtFrom_csr(4);
csr_aEmptyThrs  <= dtFrom_csr(5);

rdFIFOcmd  <= intRdDtValid;
end beh;
