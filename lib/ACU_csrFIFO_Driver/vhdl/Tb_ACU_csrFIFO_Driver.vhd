------------------------------------------------------------------------------------------------------------------------
-- File name: Tb_ACU_csrFIFO_Driver.vhd                                                                               --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 10/01/2025                                                                                              --
--                                                                                                                    --
-- Comments : ACU_csrFIFO_Driver test bench file                                                                      --
--                                                                                                                    --
-- History  : Start up version 10/01/2025                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


Entity Tb_ACU_csrFIFO_Driver is
end Entity Tb_ACU_csrFIFO_Driver;

architecture beh of Tb_ACU_csrFIFO_Driver is

--
-- Constant declaration
--
-- Signal declaration
signal Clock        : std_logic:='0';
signal Reset        : std_logic:='1';

signal ClrEvent     : std_logic_vector(7 downto 0):=x"AB";
signal IntEnable    : std_logic_vector(7 downto 0):=x"CD";
signal A_FullThrs   : std_logic_vector(31 downto 0):=x"ABCD1234";
signal A_EmptyThrs  : std_logic_vector(31 downto 0):=x"5678BEEF";

signal csrFIFOrdDt  : std_logic_vector(31 downto 0):=x"00000000";
signal RdFIFO2Msps  : std_logic;

--
-- Component declaration
Component ACU_csrFIFO_Driver is
  Generic(
    --gBaseAddr          : std_logic_vector(31 downto 0):=x"c0000000";  -- csr base address
    gRdOnly            : std_logic:='0'                               -- Read only scheduler enable (Active high)       
                                                                  
  );
  Port(
    Reset              : in std_logic;                                -- Asynchronous reset active high
    Clock              : in std_logic;                                -- System clock
    dtFIFO_rd          : in std_logic;                                -- Data FIFO read command (its period has to be > 80/360ns)   

    -- cfg
    ClrEvent           : in std_logic_vector(7 downto 0);             -- The event register is write 1 reset
    IntEnable          : in std_logic_vector(7 downto 0);             -- Interrupt enable register value to write
    A_FullThrs         : in std_logic_vector(31 downto 0);            -- Almost full threshold register value to write
    A_EmptyThrs        : in std_logic_vector(31 downto 0);            -- Almost empty threshold register value to write
    
    -- csr interface
    --csrFIFOaddr        : out std_logic_vector(31 downto 0);           -- csr address
    csrFIFOaddr        : out std_logic_vector(2 downto 0);           -- csr address    
    csrFIFOrd          : out std_logic;                               -- csr read command
    csrFIFOrdDt        : in std_logic_vector(31 downto 0);            -- Read data from the addressed csr register
    csrFIFOwr          : out std_logic;                               -- csr write command
    csrFIFOwrDt        : out std_logic_vector(31 downto 0);           -- Write data to the addressed csr register
    
    -- csr sampled data
    New_csrData        : out std_logic;                               -- Pulse active high every scheduling end.
    csr_fill_level     : out std_logic_vector(31 downto 0);           -- number of used locations. It is 0 if the FIFO is full.
    csr_status         : out std_logic_vector(7 downto 0);            -- status info.
    csr_event          : out std_logic_vector(7 downto 0);            -- event info.
    csr_intEnable      : out std_logic_vector(7 downto 0);            -- interrupt enable.
    csr_aFullThrs      : out std_logic_vector(31 downto 0);           -- almost full threshold.
    csr_aEmptyThrs     : out std_logic_vector(31 downto 0);           -- almost empty threshold.
    
    -- FIFO rd cnm and data valid
    rdFIFOcmd          : out std_logic;                               -- FIFO read command
    rdDtValid          : out std_logic                                -- hps2FPGA read data valid pulse
  );
end component ACU_csrFIFO_Driver;

Component cntFreeWheel is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       enable           : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntFreeWheel;

begin

Clock  <= not Clock after 5 ns;

i_500nsTimebase: cntFreeWheel 
  generic map(
       gCntOutWidth     => 8,
       gEnTrigValue     => 50
  )
  port map(
       clock            => Clock,
       reset            => Reset,
       enable           => RdFIFO2Msps,
       cntOut           => open    
    );



i_ACU_csrFIFO_Driver_RO: ACU_csrFIFO_Driver
  Generic map(
    --gBaseAddr          => x"c0000000",
    gRdOnly            => '1'                                     
  )
  Port map(
    Reset              => Reset,
    Clock              => Clock,
    dtFIFO_rd          => RdFIFO2Msps,
    
    -- cfg
    ClrEvent           => ClrEvent,
    IntEnable          => IntEnable,
    A_FullThrs         => A_FullThrs,
    A_EmptyThrs        => A_EmptyThrs,
    
    -- csr interface
    csrFIFOaddr        => open,
    csrFIFOrd          => open,
    csrFIFOrdDt        => csrFIFOrdDt,
    csrFIFOwr          => open,
    csrFIFOwrDt        => open,
    
    -- csr sampled data
    New_csrData        => open,
    csr_fill_level     => open,
    csr_status         => open,
    csr_event          => open,
    csr_intEnable      => open,
    csr_aFullThrs      => open,
    csr_aEmptyThrs     => open,
    
    -- FIFO rd cnm and data valid
    rdFIFOcmd          => open,
    rdDtValid          => open
  );
  
  
i_ACU_csrFIFO_Driver: ACU_csrFIFO_Driver
  Generic map(
    --gBaseAddr          => x"12345678",
    gRdOnly            => '0'                                     
  )
  Port map(
    Reset              => Reset,
    Clock              => Clock,
    dtFIFO_rd          => RdFIFO2Msps,
        
    -- cfg
    ClrEvent           => ClrEvent,
    IntEnable          => IntEnable,
    A_FullThrs         => A_FullThrs,
    A_EmptyThrs        => A_EmptyThrs,
    
    -- csr interface
    csrFIFOaddr        => open,
    csrFIFOrd          => open,
    csrFIFOrdDt        => csrFIFOrdDt,
    csrFIFOwr          => open,
    csrFIFOwrDt        => open,
    
    -- csr sampled data
    New_csrData        => open,
    csr_fill_level     => open,
    csr_status         => open,
    csr_event          => open,
    csr_intEnable      => open,
    csr_aFullThrs      => open,
    csr_aEmptyThrs     => open,
    
    -- FIFO rd cnm and data valid
    rdFIFOcmd          => open,
    rdDtValid          => open
  );
    
end beh;
