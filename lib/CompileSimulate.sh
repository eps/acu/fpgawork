#! /bin/bash

clear

echo "Compile and Simulate Script"

#echo mainDir
#echo enComp
#echo enSim


if [ $# -lt 5 ]; then

 echo "#######################################################"
 echo " Set the followings parameters:"
 echo "             1) Project name {ACU_Hysteresis_Comparator}"
 echo "             2) Enable compile active high {0,1}"
 echo "             3) Enable simulate active high {0,1}"
 echo "             4) Debug {=0}; Regression {=1} enable"
 echo "             5) Test case name {Tst_HystComp_0.tcl}"
 echo ""
 echo " NOTE!!: the project name has to be the same of the DUT"
 echo " and the test bench file ha to be named starting with "
 echo " a Tb_ string. i.e." 
 echo "    folder name : ACU_Hysteresis_Comparator"
 echo "    DUT name    : ACU_Hysteresis_Comparator.vhd"
 echo "    Tb name     : Tb_ACU_Hysteresis_Comparator.vhd"
 echo "#######################################################"

else

 mainDir="$1"
 enComp="$2"
 enSim="$3"
 endebug="$4"
 testCase="$5"
 
 echo $mainDir
 echo $enComp
 echo $enSim
 echo $endebug
 echo $testCase
 
 
 # Compile enable check  
 if [ $enComp == 1 ]; then

   echo ""
   echo "Compile enabled"
   echo ""
   echo "Creating sim directory"
   echo ""
   cd $mainDir
   SIMDIR="sim"
   if [ -d "$SIMDIR" ]; then
     cp -rf $SIMDIR $SIMDIR\_old
     rm -rf $SIMDIR/*
   else
     mkdir $SIMDIR
   fi
   cd $SIMDIR
   mkdir Results
   echo "Start Compile"
   echo ""
   echo "	Source Compile.tcl"
   source ../Compile.tcl
   echo ""
   echo "End Compile"
 fi
 echo ""
 # Simulate enable check
 if [ $enSim == 1 ]; then
   echo "Simulate enabled"
   TB_NAME="Tb_$mainDir"
   echo ""
   echo "Start Simulate"
   if [ $endebug == 0 ]; then
     echo "Debug Session"
     vsim $TB_NAME -t ps
   else
     echo "Regression Session"
     vsim -c -t ps -do "source ../TestCases/$testCase; run -all; quit -sim; exit" $TB_NAME
     cd ..
     SUBSTRING=`echo $testCase| cut -d'.' -f 1`
     echo "cp -rf $SIMDIR $SIMDIR"_"$SUBSTRING"
     NEWDIR=`echo $SIMDIR"_"$SUBSTRING`
     if [ -d "$NEWDIR" ]; then
       cp -rf $NEWDIR $NEWDIR\_old
       rm -rf $NEWDIR
     fi

     cp -rf $SIMDIR $NEWDIR
     #cp -rf $SIMDIR $SIMDIR"_"$SUBSTRING
     #cp -rf $SIMDIR $SIMDIR\_$SUBSTRING
  fi
   echo ""
   echo "End Simulate"
 fi

fi
