--------------------------------------------------------------------------------------------------
-- File name: ACU_PLL_PhShiftDriver.vhd                                                         --
--                                                                                              --
-- Author   : D.Rodomonti                                                                       --
-- Date     : 26/04/2024                                                                        --
--                                                                                              --
-- Comments : This module drives a PLL able to perform a clock phase shift.                     --
--            It interfaces the following PLL pins:                                             --
--              * updn       => it defines the phase shift direction.                           --
--              * cntsel     => it defines on which PLL output the phase shift is acting.       --
--              * phase_en   => pulse active high that enables a phase shift of Tvco/8.         --
--              * phase_done => feedback from the PLL.                                          --
--            The phase granularity is given by Tvco (Voltage Controlled Oscillator period)/8.  --
--            Tvco is also involved in the PLL output period definition with the following      --
--            formula:                                                                          --
--                   Tclkout=Cx*Tvco; Tvco=(N/M)*Tclkin                                         --
--            If we want to perform a phase shif from 0 to Tclkout with gNrBitPhSHift bit, the  --
--            formula to use is the following:                                                  --
--                 (2^gNrBitPhSHift)*phi=Tclkout                                                --
--                 (2^gNrBitPhSHift)*(Tvco/8)=Cx*Tvco                                           --
--                 Cx= 2^(gNrBitPhSHift-3)                                                      --
--            With Cx and the Tclkout assigned, it is possible to derive the N/M ratio and      --
--            configure the PLL IP.                                                             --
--            Please note that not all the N/M Cx values are allowed, so pay attention in the   --
--            PLL IP configuration about the error messages.                                    -- 
--            Please note thart the last significative gNrBitPhSHift bit of PWM_SetValue are    --
--            used to drive the phase shift action.                                             --
--            ex:                                                                               --
--               Tclkout = 10ns                                                                 --
--               gNrBitPhSHift = 5                                                              --
--               phi=Tvco/8; Tvco=(N*Tclkin)/M; Tclkout=Cx*Tvco                                 --
--               We want 2^5*phi=Tclkout so:                                                    --
--                 2^5*(Tvco/8)=Cx*Tvco  => Cx=4; M=4,N=1                                       --
--                 Tvco= 10/4=2,5ns                                                             --
--                 phi=2,5/8=0,3125ns                                                           --
--            The phase shift starts when a NewPWM_SetValue pulse is detected.                  --
--            Afterwards the number of phase shift is evaluated from PWM_SetValue and compared  --
--            with the old number of phase shift. The delta between them will define the new    --
--            number of shift to perform and the direction.                                     --
--            Considering 3 clock cycles for each phase shift and gNrBitPhSHift = 5, maximum    --
--            960 ns are necessary for the complete phase shift (worst case!!).                 --
--                                                                                              --
-- History  : Start up version 26/04/2024                                                       --
--------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_PLL_PhShiftDriver is
  Generic(
         gCntsel                 : std_logic_vector(4 downto 0):="00000";       -- PLL c0 output selected by default for the phase shifted clock. 
         gSetWidth               : integer range 1 to 32:=20;                   -- PWM threshold data width.
         gNrBitPhSHift           : integer range 1 to 32:=4                     -- Shift bit.
  );
  Port (
         Clock                   : in  std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                               -- Asynchronous reset signal active high
         
         Enable                  : in  std_logic;                               -- Driver enable signal active high
         
         --PLL interface
         updn                    : out std_logic;                               -- Shift direction 0=left
         cntsel                  : out std_logic_vector(4 downto 0);            -- It selects the PLL outputs to be phase shifted:
                                                                                --       11111 => All output Counters
                                                                                --       10010 => M Counter
                                                                                --       00000 => C0 Counter
                                                                                --       00001 => C1 Counter
                                                                                --       00010 => C2 Counter
                                                                                --       00011 => C3 Counter
                                                                                --       00100 => C4 Counter            
         phase_en                : out std_logic;                               -- It has to be high at least for two clk cycles and it triggers the single phase shift action.
         phase_done              : in  std_logic;                               -- feedback from PLL (async) 
         
         Alarm                   : out std_logic;                               -- Pulse active high when a phase done is not detected after a phase_en generation
         
         NewPWM_SetValue         : in  std_logic;                               -- It enables the phase shift evaluation.
         PWM_SetValue            : in  std_logic_vector(gSetWidth-1 downto 0)   -- PWM threshold.	 
  );
End entity ACU_PLL_PhShiftDriver;

Architecture beh of ACU_PLL_PhShiftDriver is
--
-- Constants declaration
--constant phase_en_h_period          : unsigned(3 downto 0):=to_unsigned(3,4); 	 -- phase_en semiperiod						   
constant phase_en_high          : unsigned(3 downto 0):=to_unsigned(2,4); 	 -- high phase_en semiperiod						   
constant phase_en_low           : unsigned(3 downto 0):=to_unsigned(1,4); 	 -- low phase_en semiperiod						   

--
-- Signals declaration
signal cntPhShift                   : unsigned(gNrBitPhSHift-1 downto 0);        -- phase_en counter
signal Tc_cntPhShift                : unsigned(gNrBitPhSHift-1 downto 0);        -- phase_en terminal counter value
signal absPWM_SetValue              : unsigned(gSetWidth-1 downto 0);            -- phase shift number
signal NewPWM_SetValue_s0           : std_logic;                                 -- NewPWM_SetValue sampled signal
signal NewPWM_SetValue_RE           : std_logic;                                 -- NewPWM_SetValue rising edge
signal NewPWM_SetValue_RE_s0        : std_logic;                                 -- NewPWM_SetValue_RE_s0 sampled signal
signal StartDriver                  : std_logic;                                 -- Start fsm when there is a new shift to perform
signal oldPWM_SetValue              : unsigned(gNrBitPhSHift-1 downto 0);        -- old phase shift number

signal phase_done_vx                : std_logic_vector(3 downto 0);              -- phase_done sampling vector
signal cnt_phase_en_h_period        : unsigned(3 downto 0);                      -- phase_en semiperiod counter

type fsmstate is (WaitForStart, SetPhEn, UnSetPhEn);
signal fsm_state                : fsmstate;

begin

cntsel                 <= gCntsel;

p_input: process(Clock, Reset)
begin
  if (Reset='1') then
    NewPWM_SetValue_s0     <= '0';
    NewPWM_SetValue_RE     <= '0';
    NewPWM_SetValue_RE_s0  <= '0';   
    StartDriver            <= '0'; 
    absPWM_SetValue        <= (others=>'0');
    
    updn                   <= '0';
    Tc_cntPhShift          <= (others=>'0');
    
    phase_done_vx          <= (others=>'0');
    oldPWM_SetValue        <= (others=>'0');
  elsif(Clock'event and Clock='1') then
    NewPWM_SetValue_s0     <= NewPWM_SetValue;
    NewPWM_SetValue_RE     <= NewPWM_SetValue and not NewPWM_SetValue_s0;
    NewPWM_SetValue_RE_s0  <= NewPWM_SetValue_RE;
    
    phase_done_vx          <= phase_done_vx(2 downto 0) & phase_done;
    
    StartDriver            <= '0';
    
    if (NewPWM_SetValue_RE = '1') then
      -- Sampling the new shift value
      absPWM_SetValue  <= unsigned(abs(signed(PWM_SetValue(gSetWidth-1 downto 0))));
    end if;
    
    if (NewPWM_SetValue_RE_s0 = '1') then
      -- Comparing the old shift value with the new one   
      if (oldPWM_SetValue > absPWM_SetValue(gNrBitPhSHift-1 downto 0)) then
        -- dir=left
        updn           <= '0';
        Tc_cntPhShift  <= oldPWM_SetValue - absPWM_SetValue(gNrBitPhSHift-1 downto 0);
        StartDriver    <= '1';
      elsif(oldPWM_SetValue < absPWM_SetValue(gNrBitPhSHift-1 downto 0)) then
        -- dir=right
        updn           <= '1';
        Tc_cntPhShift  <= absPWM_SetValue(gNrBitPhSHift-1 downto 0)-oldPWM_SetValue;
        StartDriver    <= '1';
      else
        updn           <= '0';    
        Tc_cntPhShift  <= (others=>'0');
        StartDriver    <= '0';
      end if;
      
      oldPWM_SetValue  <=absPWM_SetValue(gNrBitPhSHift-1 downto 0);
      
    end if;
   
  end if;
end process p_input;

--
-- FSM Process
pfsm: process (Reset,Clock)
begin
  if (Reset='1') then
    -- State evolution
    fsm_state              <= WaitForStart;
    
    -- Signals evolution
    cntPhShift             <= (others=>'0');
    cnt_phase_en_h_period  <= (others=>'0');
    Alarm                  <= '0';
    phase_en               <= '0';
    
  elsif (Clock'event and Clock='1') then

    
    case fsm_state is
    
      when WaitForStart =>
        cntPhShift             <= (others=>'0');
        cnt_phase_en_h_period  <= (others=>'0');
        Alarm                  <= '0';
        phase_en               <= '0';
        
        if (Enable='1' and StartDriver='1') then
          fsm_state              <= SetPhEn;
        end if;
        
      when SetPhEn  =>
        phase_en               <= '1';
        if (cnt_phase_en_h_period < phase_en_high-1) then
          cnt_phase_en_h_period  <= cnt_phase_en_h_period + 1;
        else
          cnt_phase_en_h_period  <= (others=>'0');
          fsm_state              <= UnSetPhEn;
        end if;
      
      when UnSetPhEn =>
        phase_en               <= '0';
        if (cnt_phase_en_h_period < phase_en_low-1) then
          cnt_phase_en_h_period  <= cnt_phase_en_h_period + 1;
        else
          cnt_phase_en_h_period  <= (others=>'0');
          if (phase_done_vx(1)= '0') then
            -- No phase done detected. Shift cancelled         
            Alarm                  <= '1';
            fsm_state              <= WaitForStart;
          else
            if (cntPhShift < Tc_cntPhShift-1) then
              -- More shift to perform
              cntPhShift  <= cntPhShift + 1;
              fsm_state   <= SetPhEn;
            else
              -- Shift completed 
              fsm_state   <= WaitForStart;
            end if;
          end if;
        end if;
		  
      when others =>    
        -- State evolution
        fsm_state     <= WaitForStart;
    
        -- Signals evolution
        cntPhShift             <= (others=>'0');
        cnt_phase_en_h_period  <= (others=>'0');
        Alarm                  <= '0';
        phase_en               <= '0';
       
    end case;
  
  end if;
end process pfsm;


end beh;
