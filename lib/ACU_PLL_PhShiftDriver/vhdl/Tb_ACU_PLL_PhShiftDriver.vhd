--------------------------------------------------------------------------------------------------
-- File name: Tb_ACU_PLL_PhShiftDriver.vhd                                                      --
--                                                                                              --
-- Author   : D.Rodomonti                                                                       --
-- Date     : 29/04/2024                                                                        --
--                                                                                              --
-- Comments : ACU_PLL_PhShiftDriver test bench.                                                 --
--                                                                                              --
-- History  : Start up version 29/04/2024                                                       --
--------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity Tb_ACU_PLL_PhShiftDriver is
End entity Tb_ACU_PLL_PhShiftDriver;
USE work.ACU_package_numericLib.all;

Architecture beh of Tb_ACU_PLL_PhShiftDriver is
--
-- Constants declaration
constant PhShValues      : array5bits(31 downto 0) := ("10000","00001","01000","00000","11010","10100","00100","10011",
                                                       "00111","00101","10110","11001","01100","01010","11111","00110",
                                                       "01110","11100","00011","10111","11101","10001","00010","11000",
                                                       "01101","11110","01001","01111","10010","11011","01011","10101");




constant PWM_Values      : array16bits(7 downto 0) := (x"0BB9",x"05DC",x"1194",x"02EE",
                                                       x"F447",x"FA24",x"EE6C",x"FD12");


--
-- Signals declaration
signal Clock             : std_logic:='0';
signal Reset             : std_logic:='1';
signal Enable            : std_logic:='0';
signal updn              : std_logic;                      
signal cntsel            : std_logic_vector(4 downto 0);               
signal phase_en          : std_logic;
signal phase_done        : std_logic;
signal NewPWM_SetValue   : std_logic; 
signal PWM_SetValue      : std_logic_vector(23 downto 0);  

signal Incr60us          : std_logic; 	 

signal cntOutPWM_Val     : std_logic_vector(7 downto 0);
signal cntOutPhShVal     : std_logic_vector(7 downto 0);
signal IncrPWM_Val       : std_logic; 	 

signal intPWM_SetValue   : std_logic_vector(23 downto 0);
signal Clock_Sh          : std_logic;

signal n_CounterEnable        :std_logic:='1';
                                                              
signal Usteuer                : signed(15 downto 0);
signal absUsteuer             : signed(15 downto 0);
signal stdUsteuer             : std_logic_vector(15 downto 0);
signal uUsteuer               : unsigned(15 downto 0);

signal Takt_time              : unsigned(15 downto 0):=x"1772";     -- Zeit fuer eine Periode x 10ns immer +2rechnen 6002 fuer 60us
signal Tot_zeit               : unsigned(7  downto 0):=x"C8";       -- Zeit zw. ein Trans. wird abgeschaltet der andered Zugeschaltet
signal time_korr              : unsigned(7  downto 0):=x"00";       -- Korrektur der Zeit fuer den pos. und neg. Aufteilung (Symetrierung der Zw.Kr. Sp) 
signal Korrektur_Vorzeichen   : std_logic:='0';                     -- = Vorzeichen fuer die Korrektur
signal HalbtaktRead           : std_logic:='0';

signal Sx_comb       : std_logic_vector(3 downto 0);
signal Sx            : std_logic_vector(3 downto 0);
signal Sx_ph         : std_logic_vector(3 downto 0);

--
-- Components declaration
Component ACU_PLL_PhShiftDriver is
  Generic(
         gCntsel                 : std_logic_vector(4 downto 0):="00000";       -- PLL c0 output selected by default for the phase shifted clock. 
         gSetWidth               : integer range 1 to 32:=20;                   -- PWM threshold data width.
         gNrBitPhSHift           : integer range 1 to 32:=4                     -- Shift bit.
  );
  Port (
         Clock                   : in  std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                               -- Asynchronous reset signal active high
         
         Enable                  : in  std_logic;                               -- Driver enable signal active high
         
         --PLL interface
         updn                    : out std_logic;                               -- Shift direction 0=left
         cntsel                  : out std_logic_vector(4 downto 0);            -- It selects the PLL outputs to be phase shifted:
                                                                                --       11111 => All output Counters
                                                                                --       10010 => M Counter
                                                                                --       00000 => C0 Counter
                                                                                --       00001 => C1 Counter
                                                                                --       00010 => C2 Counter
                                                                                --       00011 => C3 Counter
                                                                                --       00100 => C4 Counter           
         phase_en                : out std_logic;                               -- It has to be high at least for two clk cycles and it triggers the single phase shift action.
         phase_done              : in  std_logic;                               -- feedback from PLL (async) 
         
         Alarm                   : out std_logic;                               -- Pulse active high when a phase done is not detected after a phase_en generation
         
         NewPWM_SetValue         : in  std_logic;                               -- It enables the phase shift evaluation.
         PWM_SetValue            : in  std_logic_vector(gSetWidth-1 downto 0)   -- PWM threshold.	 
  );
End component ACU_PLL_PhShiftDriver;

Component ACU_PLL_100MHz_PhShift is
	port (
		refclk     : in  std_logic                    := '0';             --     refclk.clk
		rst        : in  std_logic                    := '0';             --      reset.reset
		outclk_0   : out std_logic;                                       --    outclk0.clk
		locked     : out std_logic;                                       --     locked.export
		phase_en   : in  std_logic                    := '0';             --   phase_en.phase_en
		scanclk    : in  std_logic                    := '0';             --    scanclk.scanclk
		updn       : in  std_logic                    := '0';             --       updn.updn
		cntsel     : in  std_logic_vector(4 downto 0) := (others => '0'); --     cntsel.cntsel
		phase_done : out std_logic                                        -- phase_done.phase_done
	);
end component ACU_PLL_100MHz_PhShift;

Component TiefHoch_3L is                                         -- Entity Declaration
   generic(
      takt                   : integer   := 100_000_000;
      g_TimeResonanz         : integer   := 2;                -- Achtung =2 ist min!! Verschiebung vom Taktbegin zum Resonanztriger
      g_TimeInputRead        : integer   := 2);               -- Achtung =2 ist min!! Verschiebung vom Taktbegin zum Daten einlesen

   port(
      clk, reset, Enable     : in  std_logic ;                -- Mit Enable wirden die Transistoren sofort abgeschaltet
      n_CounterEnable        : in  std_logic ;                -- =0 Saegezahn - Counter laeuft
                                                              -- Zeitsollwerte (Vorgabe des Spannungsmittelwertes)
      Usteuer                : in  unsigned(15 downto 0);     -- =1 Tiefsetzsteller voll ausgesteuert; =0 Hochsetzsteller voll ausgesteuert
      Takt_time              : in  unsigned(15 downto 0);     -- Zeit fuer eine Periode x 10ns immer +2rechnen 6002 fuer 60us
      Ust_immer_lesen        : in  std_logic;                 -- = '1' , Eingang wird staendig gelesen und in dem PWM-Takt sofort korrigiert
                                                              -- = '0' nur wenn der Takt beginnt wird Usteuer eingelesen
      Tot_zeit               : in  unsigned(7  downto 0);     -- Zeit zw. ein Trans. wird abgeschaltet der andered Zugeschaltet
      time_korr              : in  unsigned(7  downto 0);     -- Korrektur der Zeit fuer den pos. und neg. Aufteilung (Symetrierung der Zw.Kr. Sp) 
      Korrektur_Vorzeichen   : in  std_logic;                 -- = Vorzeichen fuer die Korrektur
      HalbtaktRead           : in  std_logic;                 -- =1 der Eingang wird auch bei dem Halben Takt gelesen =0 nur beim ganzen Takt
     
      S1,S2,S3,S4            : out std_logic ;                -- Signale fuer Transistoren
      TH_Data_Out_Valid      : out std_logic ;                -- PWM Output =1 ein Takt ist durchgelaufen
      Resonanz_Trigger       : out std_logic ;                -- Triger fuer Resonanzkonverter
      DataRead_Trigger       : out std_logic);                -- Eingangsdaten werden eingelesen
      
end component TiefHoch_3L;

Component cntFreeWheel is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       enable           : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntFreeWheel;

Component cntCmdInDriven is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       incrCmd          : in std_logic;
       enable           : out std_logic;
       cntOutValid      : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntCmdInDriven;

begin

Clock  <= not Clock after 5 ns;


i_cntFreeWheel_60u:cntFreeWheel
  generic map(
       gCntOutWidth     => 16,
       gEnTrigValue     => 6000
    )
  port map(
       clock            => Clock,
       reset            => Reset,
       enable           => Incr60us,
       cntOut           => open
    
    );

i_cntCmdInDrivenPhShValues: cntCmdInDriven
  generic map(
       gCntOutWidth     => 8,
       gEnTrigValue     => 32
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => Incr60us,
       enable           => IncrPWM_Val,
       cntOutValid      => NewPWM_SetValue,
       cntOut           => cntOutPhShVal
    
);

i_cntCmdInDrivenPWMValues: cntCmdInDriven
  generic map(
       gCntOutWidth     => 8,
       gEnTrigValue     => 8
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => IncrPWM_Val,
       enable           => open,
       cntOutValid      => open,
       cntOut           => cntOutPWM_Val
    
);


intPWM_SetValue <= PWM_Values(to_integer(unsigned(cntOutPWM_Val))) & "000" & PhShValues(to_integer(unsigned(cntOutPhShVal)));

i_ACU_PLL_PhShiftDriver: ACU_PLL_PhShiftDriver
  Generic map(
         gSetWidth        => 24,
         gNrBitPhSHift    => 5
  )
  Port map(
         Clock            => Clock,
         Reset            => Reset,
         Enable           => Enable,
         
         --PLL interface
         updn             => updn,
         cntsel           => cntsel,            
         phase_en         => phase_en,
         phase_done       => phase_done,
         
         Alarm            => open,
         
         NewPWM_SetValue  => NewPWM_SetValue,
         PWM_SetValue     => intPWM_SetValue	 
  );
  
i_ACU_PLL_100MHz_PhShift: ACU_PLL_100MHz_PhShift
	port map(
		refclk     => Clock,
		rst        => Reset,
		outclk_0   => Clock_Sh,
		locked     => open,
		phase_en   => phase_en,
		scanclk    => Clock,
		updn       => updn,
		cntsel     => cntsel,
		phase_done => phase_done
	);
	
-- 0° branch
Usteuer     <= signed(PWM_Values(to_integer(unsigned(cntOutPWM_Val))));
absUsteuer  <= ABS(Usteuer); 
stdUsteuer  <= std_logic_vector(absUsteuer);
uUsteuer    <= unsigned(stdUsteuer);
	
i_TiefHoch_3L_0: TiefHoch_3L

   port map(
      clk                    => Clock,
      reset                  => Reset,
      Enable                 => Enable,
      
      n_CounterEnable        => n_CounterEnable,

      Usteuer                => uUsteuer,
      Takt_time              => Takt_time,
      Ust_immer_lesen        => '0',
                                
      Tot_zeit               => Tot_zeit,
      time_korr              => time_korr, 
      Korrektur_Vorzeichen   => Korrektur_Vorzeichen,
      HalbtaktRead           => HalbtaktRead,
           
      S1                     => Sx(0),
      S2                     => Sx(1),
      S3                     => Sx(2),
      S4                     => Sx(3),
      TH_Data_Out_Valid      => open,
      Resonanz_Trigger       => open,
      DataRead_Trigger       => open
);

i_TiefHoch_3L_Sh: TiefHoch_3L

   port map(
      clk                    => Clock_Sh,
      reset                  => Reset,
      Enable                 => Enable,
      
      n_CounterEnable        => n_CounterEnable,

      Usteuer                => uUsteuer,
      Takt_time              => Takt_time,
      Ust_immer_lesen        => '0',
                                
      Tot_zeit               => Tot_zeit,
      time_korr              => time_korr, 
      Korrektur_Vorzeichen   => Korrektur_Vorzeichen,
      HalbtaktRead           => HalbtaktRead,
           
      S1                     => Sx_ph(0),
      S2                     => Sx_ph(1),
      S3                     => Sx_ph(2),
      S4                     => Sx_ph(3),
      TH_Data_Out_Valid      => open,
      Resonanz_Trigger       => open,
      DataRead_Trigger       => open
);

-- Comb Out
Sx_comb(0)  <= Sx(0) or Sx_ph(0);
Sx_comb(1)  <= Sx(1) or Sx_ph(1);
Sx_comb(2)  <= Sx(2) or Sx_ph(2);
Sx_comb(3)  <= Sx(3) or Sx_ph(3);

end beh;
