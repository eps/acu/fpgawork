###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work

vmap altera work
vmap altera_mf work
vmap cyclonev work
vmap altera_lnsim work
vmap work_lib work
vmap lpm work
vmap sgate work

pwd





    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/altera_syn_attributes.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/altera_standard_functions.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/alt_dspbuilder_package.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/altera_europa_support_lib.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/altera_primitives_components.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/altera_primitives.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/220pack.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/220model.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/sgate_pack.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/sgate.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/altera_mf_components.vhd  
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/altera_mf.vhd
    vlog -sv /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/mentor/altera_lnsim_for_vhdl.sv
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/altera_lnsim_components.vhd
    vlog     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/mentor/cyclonev_atoms_ncrypt.v 
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/cyclonev_atoms.vhd
    vcom     /home/drodomon/intelFPGA_lite/20.1/quartus/eda/sim_lib/cyclonev_components.vhd





vlog -reportprogress 300 -work work $path/ACU_PLL_100MHz_PhShift/ACU_PLL_100MHz_PhShift_0002.v
#vlog $path/ACU_PLL_100MHz_PhShift/ACU_PLL_100MHz_PhShift_0002.v
#vcom -93 $path/ACU_PLL_100MHz_PhShift/ACU_PLL_100MHz_PhShift_0002.v
vcom -93 $path/ACU_PLL_100MHz_PhShift.vhd

vcom -93 ../../../../SVN/Quartus_111/lib/ACU_package_numericLib.vhd 
vcom -93 ../../../../SVN/Quartus_111/lib/SimFolder/vhdl/cntFreeWheel.vhd 
vcom -93 ../../../../SVN/Quartus_111/lib/SimFolder/vhdl/cntCmdInDriven.vhd
vcom -93 ../../SFRS_FW/vhdl/TiefHoch_3L.vhd

vcom -93 $path/ACU_PLL_PhShiftDriver.vhd

vcom -93 $path/Tb_ACU_PLL_PhShiftDriver.vhd
