------------------------------------------------------------------------------------------------------
-- File name: Tb_ACU_HPS.vhd                                                                        --
--                                                                                                  --
-- Author   : D.Rodomonti                                                                           --
-- EMail    : d.rodomonti@gsi.de                                                                    --
-- Date     : 09/12/2022                                                                            --
--                                                                                                  --
-- Comments : This is the self generated test bench that combines the ACU_AvalonMMM_USI_Bridge with --
--            the ACU_HPS_QSys_Tb system design.                                                    --            
--                                                                                                  --
-- History  : V1.0 DR => Start up version 09/12/2022                                                --
------------------------------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--USE work.ACU_package_numericLib.all;

Entity Tb_ACU_HPS is 
End entity Tb_ACU_HPS;

architecture beh of Tb_ACU_HPS is
--
-- Constant declaration

--
-- Signal declaration
signal Reset                     : std_logic:='0';      -- Asynchronous reset active high
signal Resetn                    : std_logic;           -- Asynchronous reset active low
signal ClockSys                  : std_logic:='0';      -- System clock @100MHz
signal ClockHPS                  : std_logic:='1';      -- HPS clock @50MHz

signal mUSIC_Emul_FSPnr          : std_logic_vector (7 downto 0):=(others=>'0');
signal mUSIC_Emul_FSP_RdWrn      : std_logic:='1';
signal mUSIC_Emul_FSP_Dt2Wr      : std_logic_vector(2047 downto 0):=(others=>'0');
signal RemainingBytes            : std_logic_vector(11 downto 0);
signal mUSIC_Emul_FSP_Start      : std_logic:='0';

signal USI_itf_Address           : std_logic_vector(7 downto 0):=x"FF";
signal USI_itf_DataEn            : std_logic:='0';
signal USI_itf_RWn               : std_logic:='1';
signal USI_itf_Data              : std_logic_vector(7 downto 0);

signal RAM_Addr                  : std_logic_vector(9 downto 0);
signal RAM_WrCmd                 : std_logic;
signal RAM_WrData                : std_logic_vector(31 downto 0);
signal RAM_RdData                : std_logic_vector(31 downto 0);
signal FIFO_WrCmd                : std_logic;
signal FIFO_WrData               : std_logic_vector(31 downto 0);
signal FPGA_DtFtchdEn            : std_logic:='0';
signal FPGA_NewDtFtchd           : std_logic:='0';
signal FPGA_DataFetched          : std_logic_vector(63 downto 0):=(others=>'0');

signal FIFO_RdCmd                : std_logic;
signal FIFO_RdData               : std_logic_vector(31 downto 0);

signal acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_readdata                    : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_address                     : std_logic_vector(2 downto 0);  
signal acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_read                        : std_logic;                    
signal acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_writedata                   : std_logic_vector(31 downto 0);
signal acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_write                       : std_logic;                     

signal acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_readdata                     : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_address                      : std_logic_vector(2 downto 0);  
signal acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_read                         : std_logic;                     
signal acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_writedata                    : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_write                        : std_logic;                     

signal irq_mapper_receiver0_irq                                                : std_logic;                     
signal acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_irq_irq                       : std_logic_vector(0 downto 0);  

signal acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_readdata                      : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_address                       : std_logic_vector(5 downto 0);  
signal acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_read                          : std_logic;                     
signal acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_writedata                     : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_write                         : std_logic;                     

signal acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_readdata                   : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_waitrequest                : std_logic;                     
signal acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_address                    : std_logic_vector(0 downto 0);  
signal acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_read                       : std_logic;                     
signal acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_writedata                  : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_write                      : std_logic;                     

signal mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_chipselect      : std_logic;                     
signal mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_readdata        : std_logic_vector(31 downto 0); 
signal mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_waitrequest     : std_logic;                     
signal mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_address         : std_logic_vector(0 downto 0);  
signal mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_read            : std_logic;                     
signal mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_write           : std_logic;                     
signal mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_writedata       : std_logic_vector(31 downto 0); 
signal mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_read_ports_inv  : std_logic;                     
signal mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_write_ports_inv : std_logic;                     

signal rst_controller_reset_out_reset                                          : std_logic;                     
signal rst_controller_reset_out_reset_req                                      : std_logic;                     

signal acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_readdata                  : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_address                   : std_logic_vector(9 downto 0);  
signal acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_read                      : std_logic;                     
signal acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_byteenable                : std_logic_vector(3 downto 0);  
signal acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_writedata                 : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_write                     : std_logic;                     
signal acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_clken                     : std_logic;                     
signal mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_chipselect     : std_logic;                     
signal mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_readdata       : std_logic_vector(31 downto 0); 
signal mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_address        : std_logic_vector(9 downto 0);  
signal mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_byteenable     : std_logic_vector(3 downto 0);  
signal mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_write          : std_logic;                     
signal mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_writedata      : std_logic_vector(31 downto 0); 
signal mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_clken          : std_logic;                     

signal acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_readdata                     : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_address                      : std_logic_vector(1 downto 0);  
signal acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_read                         : std_logic;                     
signal acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_writedata                    : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_write                        : std_logic;                     
signal mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_chipselect        : std_logic;                     
signal mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_readdata          : std_logic_vector(31 downto 0); 
signal mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_address           : std_logic_vector(1 downto 0);  
signal mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_write             : std_logic;                     
signal mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_writedata         : std_logic_vector(31 downto 0); 

signal mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_write_ports_inv   : std_logic;                     

signal acu_hps_qsys_tb_inst_sysid_s2_hps_bfm_m0_readdata                       : std_logic_vector(31 downto 0); 
signal acu_hps_qsys_tb_inst_sysid_s2_hps_bfm_m0_address                        : std_logic_vector(0 downto 0);  

--
-- Component declaration

-- mUSIC_Emulator 
Component mUSIC_Emulator is
    Generic (
      gTimeBetween2DataEn  : integer range 256 downto 3 :=10;           -- It defines the DataEn generation period
      gMaxFSPDepth         : integer range 4096  downto 1 := 256        -- It is used to define the file chunk size transferred from
                                                                        -- USI to HPS (in Byte)
    );
    Port (
      Clock                : in std_logic;                              -- Clock signal (usually @100MHz)
      Reset                : in std_logic;                              -- Asynchronous reset signal active high
      
      -- Emulator driver signals
      FSPnr                : in  std_logic_vector (7 downto 0);         -- FSP number to select
      FSP_RdWrn            : in  std_logic;                             -- Read or write action flag
      FSP_Dt2Wr            : in  std_logic_vector((8*gMaxFSPDepth)-1 downto 0); -- Data to write in to the selected FSP
      RemainingBytes       : in  std_logic_vector (11 downto 0);        -- This counter reports the Data transmission between the USI 
      FSP_Start            : in  std_logic;                             -- Emulator start signal
      FSP_NewDtRead        : out std_logic;                             -- New FSP_DtRead pulse (active high)
      FSP_DtRead           : out std_logic_vector((8*gMaxFSPDepth)-1 downto 0); -- Data read from the selected FSP
      -- USI interface
      Address              : out std_logic_vector (7 downto 0);         -- Register address
      DataEn               : out std_logic;                             -- Data enable signal (aka ClockData)
      RWn                  : out std_logic;                             -- It reports if a register has to be read (1) or written (0)                       -- Data shift out command
      Data                 : inout std_logic_vector (7 downto 0)       -- Data bus to USI driver.
  
    );
  End component mUSIC_Emulator;

-- USI bridge
Component ACU_AvalonMMM_USI_Bridge is
    Generic (
      gNrFSPs              : integer range 127   downto 1 := 5;         -- Number of output FSPs(registers) emulated inside the bridge
      gMaxFSPDepth         : integer range 4096  downto 1 := 256        -- It is used to define the file chunk size transferred from
                                                                        -- USI to HPS (in Byte)
    );
    Port (
      Clock                : in std_logic;                              -- Clock signal (usually @100MHz)
      Reset                : in std_logic;                              -- Asynchronous reset signal active high
  
      -- USI interface
      Address              : in  std_logic_vector (7 downto 0);         -- Register address
      DataEn               : in  std_logic;                             -- Data enable signal (aka ClockData)
      RWn                  : in  std_logic;                             -- It reports if a register has to be read (1) or written (0)                       -- Data shift out command
      Data                 : inout std_logic_vector (7 downto 0);       -- Data bus to USI driver.
      RemainingBytes       : out std_logic_vector (11 downto 0);        -- This counter reports the Data transmission between the USI 
                                                                        -- driver and the FSP (register) instance.
      Available            : out std_logic_vector(gNrFSPs-1 downto 0);  -- It reports the FSP instance to the USI driver.                                                                         --ist
      ReadOnly             : out std_logic_vector(gNrFSPs-1 downto 0);  -- It is 1 when the selected FSP is a read only one. 
                                                                        -- NOTE:In this module there are no Read only FSPs(registers)! 
  
      -- RAM interface (AMM_M)
      RAM_Addr             : out std_logic_vector(9 downto 0);          -- RAM address
      RAM_WrCmd            : out std_logic;                             -- RAM write command (active high)
      RAM_WrData           : out std_logic_vector(31 downto 0);         -- RAM data to write
      RAM_RdData           : in  std_logic_vector(31 downto 0);         -- RAM read data. It has 2 clock cycles latency
  
      -- FIFO interface (AMM_M)
      FIFO_WrCmd           : out std_logic;                             -- Command to push in data
      FIFO_WrData          : out std_logic_vector(31 downto 0);         -- FIFO data to write
  
      -- Misc
      FPGA_DtFtchdEn       : in std_logic;                              -- Enable the FPGA_DataFetched data to be sent to the FIFO.
      FPGA_NewDtFtchd      : in std_logic;                              -- New FPGA_DataFetched available (pulse active high).
      FPGA_DataFetched     : in std_logic_vector(63 downto 0)           -- Data from FPGA to HPS (through FIFO)
  
  
    );
End component ACU_AvalonMMM_USI_Bridge;

-- Qsys design without HPS instance
Component ACU_HPS_QSys_Tb is
    port (
        clk_clk                         : in  std_logic                     := 'X';             -- clk
        fifo_mm_irq_fpga_irq            : out std_logic;                                        -- irq
        fifo_mm_reset_fpga_reset_n      : in  std_logic                     := 'X';             -- reset_n
        fifo_mm_s1_fpga_writedata       : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        fifo_mm_s1_fpga_write           : in  std_logic                     := 'X';             -- write
        fifo_mm_s2_hps_readdata         : out std_logic_vector(31 downto 0);                    -- readdata
        fifo_mm_s2_hps_read             : in  std_logic                     := 'X';             -- read
        fifo_mm_st_fpga_address         : in  std_logic_vector(2 downto 0)  := (others => 'X'); -- address
        fifo_mm_st_fpga_read            : in  std_logic                     := 'X';             -- read
        fifo_mm_st_fpga_writedata       : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        fifo_mm_st_fpga_write           : in  std_logic                     := 'X';             -- write
        fifo_mm_st_fpga_readdata        : out std_logic_vector(31 downto 0);                    -- readdata
        fifo_mm_st_hps_address          : in  std_logic_vector(2 downto 0)  := (others => 'X'); -- address
        fifo_mm_st_hps_read             : in  std_logic                     := 'X';             -- read
        fifo_mm_st_hps_writedata        : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        fifo_mm_st_hps_write            : in  std_logic                     := 'X';             -- write
        fifo_mm_st_hps_readdata         : out std_logic_vector(31 downto 0);                    -- readdata
        fifo_mm_sysclk_fpga_clk         : in  std_logic                     := 'X';             -- clk
        irq_lc_s2_hps_address           : in  std_logic_vector(5 downto 0)  := (others => 'X'); -- address
        irq_lc_s2_hps_writedata         : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        irq_lc_s2_hps_write             : in  std_logic                     := 'X';             -- write
        irq_lc_s2_hps_read              : in  std_logic                     := 'X';             -- read
        irq_lc_s2_hps_readdata          : out std_logic_vector(31 downto 0);                    -- readdata
        jtag_uart_s2_hps_chipselect     : in  std_logic                     := 'X';             -- chipselect
        jtag_uart_s2_hps_address        : in  std_logic                     := 'X';             -- address
        jtag_uart_s2_hps_read_n         : in  std_logic                     := 'X';             -- read_n
        jtag_uart_s2_hps_readdata       : out std_logic_vector(31 downto 0);                    -- readdata
        jtag_uart_s2_hps_write_n        : in  std_logic                     := 'X';             -- write_n
        jtag_uart_s2_hps_writedata      : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        jtag_uart_s2_hps_waitrequest    : out std_logic;                                        -- waitrequest
        onchip_ram_reset_fpga_reset     : in  std_logic                     := 'X';             -- reset
        onchip_ram_reset_fpga_reset_req : in  std_logic                     := 'X';             -- reset_req
        onchip_ram_s1_fpga_address      : in  std_logic_vector(9 downto 0)  := (others => 'X'); -- address
        onchip_ram_s1_fpga_clken        : in  std_logic                     := 'X';             -- clken
        onchip_ram_s1_fpga_chipselect   : in  std_logic                     := 'X';             -- chipselect
        onchip_ram_s1_fpga_write        : in  std_logic                     := 'X';             -- write
        onchip_ram_s1_fpga_readdata     : out std_logic_vector(31 downto 0);                    -- readdata
        onchip_ram_s1_fpga_writedata    : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        onchip_ram_s1_fpga_byteenable   : in  std_logic_vector(3 downto 0)  := (others => 'X'); -- byteenable
        onchip_ram_s2_hps_address       : in  std_logic_vector(9 downto 0)  := (others => 'X'); -- address
        onchip_ram_s2_hps_chipselect    : in  std_logic                     := 'X';             -- chipselect
        onchip_ram_s2_hps_clken         : in  std_logic                     := 'X';             -- clken
        onchip_ram_s2_hps_write         : in  std_logic                     := 'X';             -- write
        onchip_ram_s2_hps_readdata      : out std_logic_vector(31 downto 0);                    -- readdata
        onchip_ram_s2_hps_writedata     : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        onchip_ram_s2_hps_byteenable    : in  std_logic_vector(3 downto 0)  := (others => 'X'); -- byteenable
        onchip_ram_sysclk_fpga_clk      : in  std_logic                     := 'X';             -- clk
        pio_led_out_export              : out std_logic_vector(7 downto 0);                     -- export
        pio_led_s2_hps_address          : in  std_logic_vector(1 downto 0)  := (others => 'X'); -- address
        pio_led_s2_hps_write_n          : in  std_logic                     := 'X';             -- write_n
        pio_led_s2_hps_writedata        : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        pio_led_s2_hps_chipselect       : in  std_logic                     := 'X';             -- chipselect
        pio_led_s2_hps_readdata         : out std_logic_vector(31 downto 0);                    -- readdata
        reset_reset_n                   : in  std_logic                     := 'X';             -- reset_n
        sysid_s2_hps_readdata           : out std_logic_vector(31 downto 0);                    -- readdata
        sysid_s2_hps_address            : in  std_logic                     := 'X'              -- address
    );
end component ACU_HPS_QSys_Tb;

-- Qsys Auto generated BFM driver
component altera_avalon_interrupt_sink_vhdl is
    generic (
        ASSERT_HIGH_IRQ        : integer := 1;
        AV_IRQ_W               : integer := 1;
        ASYNCHRONOUS_INTERRUPT : integer := 0;
        VHDL_ID                : integer := 0
    );
    port (
        clk   : in std_logic                    := 'X';             -- clk
        reset : in std_logic                    := 'X';             -- reset
        irq   : in std_logic_vector(0 downto 0) := (others => 'X')  -- irq
    );
end component altera_avalon_interrupt_sink_vhdl;

component altera_irq_mapper is
    port (
        clk           : in  std_logic                    := 'X'; -- clk
        reset         : in  std_logic                    := 'X'; -- reset
        receiver0_irq : in  std_logic                    := 'X'; -- irq
        sender_irq    : out std_logic_vector(0 downto 0)         -- irq
    );
end component altera_irq_mapper;


component altera_mm_interconnect is
    port (
        ACU_HPS_QSys_Tb_inst_clk_bfm_clk_clk                                            : in  std_logic                     := 'X';             -- clk
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_clk_reset_reset_bridge_in_reset_reset : in  std_logic                     := 'X';             -- reset
        ACU_HPS_QSys_Tb_inst_reset_reset_bridge_in_reset_reset                          : in  std_logic                     := 'X';             -- reset
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_address                            : in  std_logic_vector(0 downto 0)  := (others => 'X'); -- address
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_waitrequest                        : out std_logic;                                        -- waitrequest
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_read                               : in  std_logic                     := 'X';             -- read
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_readdata                           : out std_logic_vector(31 downto 0);                    -- readdata
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_write                              : in  std_logic                     := 'X';             -- write
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_writedata                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_address                                   : out std_logic_vector(0 downto 0);                     -- address
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_write                                     : out std_logic;                                        -- write
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_read                                      : out std_logic;                                        -- read
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_readdata                                  : in  std_logic_vector(31 downto 0) := (others => 'X'); -- readdata
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_writedata                                 : out std_logic_vector(31 downto 0);                    -- writedata
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_waitrequest                               : in  std_logic                     := 'X';             -- waitrequest
        ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_chipselect                                : out std_logic                                         -- chipselect
    );
end component altera_mm_interconnect;

component altera_mm_interconnect_0003 is
    port (
        ACU_HPS_QSys_Tb_inst_clk_bfm_clk_clk                                             : in  std_logic                     := 'X';             -- clk
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_clk_reset_reset_bridge_in_reset_reset : in  std_logic                     := 'X';             -- reset
        ACU_HPS_QSys_Tb_inst_reset_reset_bridge_in_reset_reset                           : in  std_logic                     := 'X';             -- reset
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_address                            : in  std_logic_vector(9 downto 0)  := (others => 'X'); -- address
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_byteenable                         : in  std_logic_vector(3 downto 0)  := (others => 'X'); -- byteenable
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_read                               : in  std_logic                     := 'X';             -- read
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_readdata                           : out std_logic_vector(31 downto 0);                    -- readdata
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_write                              : in  std_logic                     := 'X';             -- write
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_writedata                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_clken                              : in  std_logic                     := 'X';             -- clken
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_address                                   : out std_logic_vector(9 downto 0);                     -- address
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_write                                     : out std_logic;                                        -- write
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_readdata                                  : in  std_logic_vector(31 downto 0) := (others => 'X'); -- readdata
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_writedata                                 : out std_logic_vector(31 downto 0);                    -- writedata
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_byteenable                                : out std_logic_vector(3 downto 0);                     -- byteenable
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_chipselect                                : out std_logic;                                        -- chipselect
        ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_clken                                     : out std_logic                                         -- clken
    );
end component altera_mm_interconnect_0003;

component altera_mm_interconnect_0004 is
    port (
        ACU_HPS_QSys_Tb_inst_clk_bfm_clk_clk                                          : in  std_logic                     := 'X';             -- clk
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_clk_reset_reset_bridge_in_reset_reset : in  std_logic                     := 'X';             -- reset
        ACU_HPS_QSys_Tb_inst_reset_reset_bridge_in_reset_reset                        : in  std_logic                     := 'X';             -- reset
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0_address                            : in  std_logic_vector(1 downto 0)  := (others => 'X'); -- address
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0_read                               : in  std_logic                     := 'X';             -- read
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0_readdata                           : out std_logic_vector(31 downto 0);                    -- readdata
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0_write                              : in  std_logic                     := 'X';             -- write
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0_writedata                          : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_address                                   : out std_logic_vector(1 downto 0);                     -- address
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_write                                     : out std_logic;                                        -- write
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_readdata                                  : in  std_logic_vector(31 downto 0) := (others => 'X'); -- readdata
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_writedata                                 : out std_logic_vector(31 downto 0);                    -- writedata
        ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_chipselect                                : out std_logic                                         -- chipselect
    );
end component altera_mm_interconnect_0004;

component altera_reset_controller is
    generic (
        NUM_RESET_INPUTS          : integer := 6;
        OUTPUT_RESET_SYNC_EDGES   : string  := "deassert";
        SYNC_DEPTH                : integer := 2;
        RESET_REQUEST_PRESENT     : integer := 0;
        RESET_REQ_WAIT_TIME       : integer := 1;
        MIN_RST_ASSERTION_TIME    : integer := 3;
        RESET_REQ_EARLY_DSRT_TIME : integer := 1;
        USE_RESET_REQUEST_IN0     : integer := 0;
        USE_RESET_REQUEST_IN1     : integer := 0;
        USE_RESET_REQUEST_IN2     : integer := 0;
        USE_RESET_REQUEST_IN3     : integer := 0;
        USE_RESET_REQUEST_IN4     : integer := 0;
        USE_RESET_REQUEST_IN5     : integer := 0;
        USE_RESET_REQUEST_IN6     : integer := 0;
        USE_RESET_REQUEST_IN7     : integer := 0;
        USE_RESET_REQUEST_IN8     : integer := 0;
        USE_RESET_REQUEST_IN9     : integer := 0;
        USE_RESET_REQUEST_IN10    : integer := 0;
        USE_RESET_REQUEST_IN11    : integer := 0;
        USE_RESET_REQUEST_IN12    : integer := 0;
        USE_RESET_REQUEST_IN13    : integer := 0;
        USE_RESET_REQUEST_IN14    : integer := 0;
        USE_RESET_REQUEST_IN15    : integer := 0;
        ADAPT_RESET_REQUEST       : integer := 0
    );
    port (
        reset_in0      : in  std_logic := 'X'; -- reset
        clk            : in  std_logic := 'X'; -- clk
        reset_out      : out std_logic;        -- reset
        reset_req      : out std_logic;        -- reset_req
        reset_req_in0  : in  std_logic := 'X'; -- reset_req
        reset_in1      : in  std_logic := 'X'; -- reset
        reset_req_in1  : in  std_logic := 'X'; -- reset_req
        reset_in2      : in  std_logic := 'X'; -- reset
        reset_req_in2  : in  std_logic := 'X'; -- reset_req
        reset_in3      : in  std_logic := 'X'; -- reset
        reset_req_in3  : in  std_logic := 'X'; -- reset_req
        reset_in4      : in  std_logic := 'X'; -- reset
        reset_req_in4  : in  std_logic := 'X'; -- reset_req
        reset_in5      : in  std_logic := 'X'; -- reset
        reset_req_in5  : in  std_logic := 'X'; -- reset_req
        reset_in6      : in  std_logic := 'X'; -- reset
        reset_req_in6  : in  std_logic := 'X'; -- reset_req
        reset_in7      : in  std_logic := 'X'; -- reset
        reset_req_in7  : in  std_logic := 'X'; -- reset_req
        reset_in8      : in  std_logic := 'X'; -- reset
        reset_req_in8  : in  std_logic := 'X'; -- reset_req
        reset_in9      : in  std_logic := 'X'; -- reset
        reset_req_in9  : in  std_logic := 'X'; -- reset_req
        reset_in10     : in  std_logic := 'X'; -- reset
        reset_req_in10 : in  std_logic := 'X'; -- reset_req
        reset_in11     : in  std_logic := 'X'; -- reset
        reset_req_in11 : in  std_logic := 'X'; -- reset_req
        reset_in12     : in  std_logic := 'X'; -- reset
        reset_req_in12 : in  std_logic := 'X'; -- reset_req
        reset_in13     : in  std_logic := 'X'; -- reset
        reset_req_in13 : in  std_logic := 'X'; -- reset_req
        reset_in14     : in  std_logic := 'X'; -- reset
        reset_req_in14 : in  std_logic := 'X'; -- reset_req
        reset_in15     : in  std_logic := 'X'; -- reset
        reset_req_in15 : in  std_logic := 'X'  -- reset_req
    );
end component altera_reset_controller;

component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm is
    generic (
        AV_ADDRESS_W               : integer := 32;
        AV_SYMBOL_W                : integer := 8;
        AV_NUMSYMBOLS              : integer := 4;
        AV_BURSTCOUNT_W            : integer := 3;
        AV_READRESPONSE_W          : integer := 8;
        AV_WRITERESPONSE_W         : integer := 8;
        USE_READ                   : integer := 1;
        USE_WRITE                  : integer := 1;
        USE_ADDRESS                : integer := 1;
        USE_BYTE_ENABLE            : integer := 1;
        USE_BURSTCOUNT             : integer := 1;
        USE_READ_DATA              : integer := 1;
        USE_READ_DATA_VALID        : integer := 1;
        USE_WRITE_DATA             : integer := 1;
        USE_BEGIN_TRANSFER         : integer := 0;
        USE_BEGIN_BURST_TRANSFER   : integer := 0;
        USE_WAIT_REQUEST           : integer := 1;
        USE_TRANSACTIONID          : integer := 0;
        USE_WRITERESPONSE          : integer := 0;
        USE_READRESPONSE           : integer := 0;
        USE_CLKEN                  : integer := 0;
        AV_CONSTANT_BURST_BEHAVIOR : integer := 1;
        AV_BURST_LINEWRAP          : integer := 1;
        AV_BURST_BNDR_ONLY         : integer := 1;
        AV_MAX_PENDING_READS       : integer := 0;
        AV_MAX_PENDING_WRITES      : integer := 0;
        AV_FIX_READ_LATENCY        : integer := 1;
        AV_READ_WAIT_TIME          : integer := 1;
        AV_WRITE_WAIT_TIME         : integer := 0;
        REGISTER_WAITREQUEST       : integer := 0;
        AV_REGISTERINCOMINGSIGNALS : integer := 0;
        VHDL_ID                    : integer := 0
    );
    port (
        clk                    : in  std_logic                     := 'X';             --       clk.clk
        reset                  : in  std_logic                     := 'X';             -- clk_reset.reset
        avm_readdata           : in  std_logic_vector(31 downto 0) := (others => 'X'); --        m0.readdata
        avm_read               : out std_logic;                                        --          .read
        avm_address            : out std_logic_vector(0 downto 0);
        avm_arbiterlock        : out std_logic;
        avm_beginbursttransfer : out std_logic;
        avm_begintransfer      : out std_logic;
        avm_burstcount         : out std_logic_vector(0 downto 0);
        avm_byteenable         : out std_logic_vector(3 downto 0);
        avm_clken              : out std_logic;
        avm_debugaccess        : out std_logic;
        avm_lock               : out std_logic;
        avm_readdatavalid      : in  std_logic                     := 'X';
        avm_readid             : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_readresponse       : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_response           : in  std_logic_vector(1 downto 0)  := (others => 'X');
        avm_transactionid      : out std_logic_vector(7 downto 0);
        avm_waitrequest        : in  std_logic                     := 'X';
        avm_write              : out std_logic;
        avm_writedata          : out std_logic_vector(31 downto 0);
        avm_writeid            : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_writeresponse      : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_writeresponsevalid : in  std_logic                     := 'X'
    );
end component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm;

component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm is
    generic (
        AV_ADDRESS_W               : integer := 32;
        AV_SYMBOL_W                : integer := 8;
        AV_NUMSYMBOLS              : integer := 4;
        AV_BURSTCOUNT_W            : integer := 3;
        AV_READRESPONSE_W          : integer := 8;
        AV_WRITERESPONSE_W         : integer := 8;
        USE_READ                   : integer := 1;
        USE_WRITE                  : integer := 1;
        USE_ADDRESS                : integer := 1;
        USE_BYTE_ENABLE            : integer := 1;
        USE_BURSTCOUNT             : integer := 1;
        USE_READ_DATA              : integer := 1;
        USE_READ_DATA_VALID        : integer := 1;
        USE_WRITE_DATA             : integer := 1;
        USE_BEGIN_TRANSFER         : integer := 0;
        USE_BEGIN_BURST_TRANSFER   : integer := 0;
        USE_WAIT_REQUEST           : integer := 1;
        USE_TRANSACTIONID          : integer := 0;
        USE_WRITERESPONSE          : integer := 0;
        USE_READRESPONSE           : integer := 0;
        USE_CLKEN                  : integer := 0;
        AV_CONSTANT_BURST_BEHAVIOR : integer := 1;
        AV_BURST_LINEWRAP          : integer := 1;
        AV_BURST_BNDR_ONLY         : integer := 1;
        AV_MAX_PENDING_READS       : integer := 0;
        AV_MAX_PENDING_WRITES      : integer := 0;
        AV_FIX_READ_LATENCY        : integer := 1;
        AV_READ_WAIT_TIME          : integer := 1;
        AV_WRITE_WAIT_TIME         : integer := 0;
        REGISTER_WAITREQUEST       : integer := 0;
        AV_REGISTERINCOMINGSIGNALS : integer := 0;
        VHDL_ID                    : integer := 0
    );
    port (
        clk                    : in  std_logic                     := 'X';             --       clk.clk
        reset                  : in  std_logic                     := 'X';             -- clk_reset.reset
        avm_address            : out std_logic_vector(2 downto 0);                     --        m0.address
        avm_readdata           : in  std_logic_vector(31 downto 0) := (others => 'X'); --          .readdata
        avm_writedata          : out std_logic_vector(31 downto 0);                    --          .writedata
        avm_write              : out std_logic;                                        --          .write
        avm_read               : out std_logic;                                        --          .read
        avm_arbiterlock        : out std_logic;
        avm_beginbursttransfer : out std_logic;
        avm_begintransfer      : out std_logic;
        avm_burstcount         : out std_logic_vector(0 downto 0);
        avm_byteenable         : out std_logic_vector(3 downto 0);
        avm_clken              : out std_logic;
        avm_debugaccess        : out std_logic;
        avm_lock               : out std_logic;
        avm_readdatavalid      : in  std_logic                     := 'X';
        avm_readid             : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_readresponse       : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_response           : in  std_logic_vector(1 downto 0)  := (others => 'X');
        avm_transactionid      : out std_logic_vector(7 downto 0);
        avm_waitrequest        : in  std_logic                     := 'X';
        avm_writeid            : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_writeresponse      : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_writeresponsevalid : in  std_logic                     := 'X'
    );
end component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm;

component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm is
    generic (
        AV_ADDRESS_W               : integer := 32;
        AV_SYMBOL_W                : integer := 8;
        AV_NUMSYMBOLS              : integer := 4;
        AV_BURSTCOUNT_W            : integer := 3;
        AV_READRESPONSE_W          : integer := 8;
        AV_WRITERESPONSE_W         : integer := 8;
        USE_READ                   : integer := 1;
        USE_WRITE                  : integer := 1;
        USE_ADDRESS                : integer := 1;
        USE_BYTE_ENABLE            : integer := 1;
        USE_BURSTCOUNT             : integer := 1;
        USE_READ_DATA              : integer := 1;
        USE_READ_DATA_VALID        : integer := 1;
        USE_WRITE_DATA             : integer := 1;
        USE_BEGIN_TRANSFER         : integer := 0;
        USE_BEGIN_BURST_TRANSFER   : integer := 0;
        USE_WAIT_REQUEST           : integer := 1;
        USE_TRANSACTIONID          : integer := 0;
        USE_WRITERESPONSE          : integer := 0;
        USE_READRESPONSE           : integer := 0;
        USE_CLKEN                  : integer := 0;
        AV_CONSTANT_BURST_BEHAVIOR : integer := 1;
        AV_BURST_LINEWRAP          : integer := 1;
        AV_BURST_BNDR_ONLY         : integer := 1;
        AV_MAX_PENDING_READS       : integer := 0;
        AV_MAX_PENDING_WRITES      : integer := 0;
        AV_FIX_READ_LATENCY        : integer := 1;
        AV_READ_WAIT_TIME          : integer := 1;
        AV_WRITE_WAIT_TIME         : integer := 0;
        REGISTER_WAITREQUEST       : integer := 0;
        AV_REGISTERINCOMINGSIGNALS : integer := 0;
        VHDL_ID                    : integer := 0
    );
    port (
        clk                    : in  std_logic                     := 'X';             --       clk.clk
        reset                  : in  std_logic                     := 'X';             -- clk_reset.reset
        avm_address            : out std_logic_vector(5 downto 0);                     --        m0.address
        avm_readdata           : in  std_logic_vector(31 downto 0) := (others => 'X'); --          .readdata
        avm_writedata          : out std_logic_vector(31 downto 0);                    --          .writedata
        avm_write              : out std_logic;                                        --          .write
        avm_read               : out std_logic;                                        --          .read
        avm_arbiterlock        : out std_logic;
        avm_beginbursttransfer : out std_logic;
        avm_begintransfer      : out std_logic;
        avm_burstcount         : out std_logic_vector(0 downto 0);
        avm_byteenable         : out std_logic_vector(3 downto 0);
        avm_clken              : out std_logic;
        avm_debugaccess        : out std_logic;
        avm_lock               : out std_logic;
        avm_readdatavalid      : in  std_logic                     := 'X';
        avm_readid             : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_readresponse       : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_response           : in  std_logic_vector(1 downto 0)  := (others => 'X');
        avm_transactionid      : out std_logic_vector(7 downto 0);
        avm_waitrequest        : in  std_logic                     := 'X';
        avm_writeid            : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_writeresponse      : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_writeresponsevalid : in  std_logic                     := 'X'
    );
end component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm;

component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm is
    generic (
        AV_ADDRESS_W               : integer := 32;
        AV_SYMBOL_W                : integer := 8;
        AV_NUMSYMBOLS              : integer := 4;
        AV_BURSTCOUNT_W            : integer := 3;
        AV_READRESPONSE_W          : integer := 8;
        AV_WRITERESPONSE_W         : integer := 8;
        USE_READ                   : integer := 1;
        USE_WRITE                  : integer := 1;
        USE_ADDRESS                : integer := 1;
        USE_BYTE_ENABLE            : integer := 1;
        USE_BURSTCOUNT             : integer := 1;
        USE_READ_DATA              : integer := 1;
        USE_READ_DATA_VALID        : integer := 1;
        USE_WRITE_DATA             : integer := 1;
        USE_BEGIN_TRANSFER         : integer := 0;
        USE_BEGIN_BURST_TRANSFER   : integer := 0;
        USE_WAIT_REQUEST           : integer := 1;
        USE_TRANSACTIONID          : integer := 0;
        USE_WRITERESPONSE          : integer := 0;
        USE_READRESPONSE           : integer := 0;
        USE_CLKEN                  : integer := 0;
        AV_CONSTANT_BURST_BEHAVIOR : integer := 1;
        AV_BURST_LINEWRAP          : integer := 1;
        AV_BURST_BNDR_ONLY         : integer := 1;
        AV_MAX_PENDING_READS       : integer := 0;
        AV_MAX_PENDING_WRITES      : integer := 0;
        AV_FIX_READ_LATENCY        : integer := 1;
        AV_READ_WAIT_TIME          : integer := 1;
        AV_WRITE_WAIT_TIME         : integer := 0;
        REGISTER_WAITREQUEST       : integer := 0;
        AV_REGISTERINCOMINGSIGNALS : integer := 0;
        VHDL_ID                    : integer := 0
    );
    port (
        clk                    : in  std_logic                     := 'X';             --       clk.clk
        reset                  : in  std_logic                     := 'X';             -- clk_reset.reset
        avm_address            : out std_logic_vector(0 downto 0);                     --        m0.address
        avm_readdata           : in  std_logic_vector(31 downto 0) := (others => 'X'); --          .readdata
        avm_writedata          : out std_logic_vector(31 downto 0);                    --          .writedata
        avm_waitrequest        : in  std_logic                     := 'X';             --          .waitrequest
        avm_write              : out std_logic;                                        --          .write
        avm_read               : out std_logic;                                        --          .read
        avm_arbiterlock        : out std_logic;
        avm_beginbursttransfer : out std_logic;
        avm_begintransfer      : out std_logic;
        avm_burstcount         : out std_logic_vector(0 downto 0);
        avm_byteenable         : out std_logic_vector(3 downto 0);
        avm_clken              : out std_logic;
        avm_debugaccess        : out std_logic;
        avm_lock               : out std_logic;
        avm_readdatavalid      : in  std_logic                     := 'X';
        avm_readid             : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_readresponse       : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_response           : in  std_logic_vector(1 downto 0)  := (others => 'X');
        avm_transactionid      : out std_logic_vector(7 downto 0);
        avm_writeid            : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_writeresponse      : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_writeresponsevalid : in  std_logic                     := 'X'
    );
end component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm;

component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_bfm is
    generic (
        AV_ADDRESS_W               : integer := 32;
        AV_SYMBOL_W                : integer := 8;
        AV_NUMSYMBOLS              : integer := 4;
        AV_BURSTCOUNT_W            : integer := 3;
        AV_READRESPONSE_W          : integer := 8;
        AV_WRITERESPONSE_W         : integer := 8;
        USE_READ                   : integer := 1;
        USE_WRITE                  : integer := 1;
        USE_ADDRESS                : integer := 1;
        USE_BYTE_ENABLE            : integer := 1;
        USE_BURSTCOUNT             : integer := 1;
        USE_READ_DATA              : integer := 1;
        USE_READ_DATA_VALID        : integer := 1;
        USE_WRITE_DATA             : integer := 1;
        USE_BEGIN_TRANSFER         : integer := 0;
        USE_BEGIN_BURST_TRANSFER   : integer := 0;
        USE_WAIT_REQUEST           : integer := 1;
        USE_TRANSACTIONID          : integer := 0;
        USE_WRITERESPONSE          : integer := 0;
        USE_READRESPONSE           : integer := 0;
        USE_CLKEN                  : integer := 0;
        AV_CONSTANT_BURST_BEHAVIOR : integer := 1;
        AV_BURST_LINEWRAP          : integer := 1;
        AV_BURST_BNDR_ONLY         : integer := 1;
        AV_MAX_PENDING_READS       : integer := 0;
        AV_MAX_PENDING_WRITES      : integer := 0;
        AV_FIX_READ_LATENCY        : integer := 1;
        AV_READ_WAIT_TIME          : integer := 1;
        AV_WRITE_WAIT_TIME         : integer := 0;
        REGISTER_WAITREQUEST       : integer := 0;
        AV_REGISTERINCOMINGSIGNALS : integer := 0;
        VHDL_ID                    : integer := 0
    );
    port (
        clk                    : in  std_logic                     := 'X';             --       clk.clk
        reset                  : in  std_logic                     := 'X';             -- clk_reset.reset
        avm_address            : out std_logic_vector(9 downto 0);                     --        m0.address
        avm_readdata           : in  std_logic_vector(31 downto 0) := (others => 'X'); --          .readdata
        avm_writedata          : out std_logic_vector(31 downto 0);                    --          .writedata
        avm_write              : out std_logic;                                        --          .write
        avm_read               : out std_logic;                                        --          .read
        avm_byteenable         : out std_logic_vector(3 downto 0);                     --          .byteenable
        avm_clken              : out std_logic;                                        --          .clken
        avm_arbiterlock        : out std_logic;
        avm_beginbursttransfer : out std_logic;
        avm_begintransfer      : out std_logic;
        avm_burstcount         : out std_logic_vector(0 downto 0);
        avm_debugaccess        : out std_logic;
        avm_lock               : out std_logic;
        avm_readdatavalid      : in  std_logic                     := 'X';
        avm_readid             : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_readresponse       : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_response           : in  std_logic_vector(1 downto 0)  := (others => 'X');
        avm_transactionid      : out std_logic_vector(7 downto 0);
        avm_waitrequest        : in  std_logic                     := 'X';
        avm_writeid            : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_writeresponse      : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_writeresponsevalid : in  std_logic                     := 'X'
    );
end component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_bfm;

component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm is
    generic (
        AV_ADDRESS_W               : integer := 32;
        AV_SYMBOL_W                : integer := 8;
        AV_NUMSYMBOLS              : integer := 4;
        AV_BURSTCOUNT_W            : integer := 3;
        AV_READRESPONSE_W          : integer := 8;
        AV_WRITERESPONSE_W         : integer := 8;
        USE_READ                   : integer := 1;
        USE_WRITE                  : integer := 1;
        USE_ADDRESS                : integer := 1;
        USE_BYTE_ENABLE            : integer := 1;
        USE_BURSTCOUNT             : integer := 1;
        USE_READ_DATA              : integer := 1;
        USE_READ_DATA_VALID        : integer := 1;
        USE_WRITE_DATA             : integer := 1;
        USE_BEGIN_TRANSFER         : integer := 0;
        USE_BEGIN_BURST_TRANSFER   : integer := 0;
        USE_WAIT_REQUEST           : integer := 1;
        USE_TRANSACTIONID          : integer := 0;
        USE_WRITERESPONSE          : integer := 0;
        USE_READRESPONSE           : integer := 0;
        USE_CLKEN                  : integer := 0;
        AV_CONSTANT_BURST_BEHAVIOR : integer := 1;
        AV_BURST_LINEWRAP          : integer := 1;
        AV_BURST_BNDR_ONLY         : integer := 1;
        AV_MAX_PENDING_READS       : integer := 0;
        AV_MAX_PENDING_WRITES      : integer := 0;
        AV_FIX_READ_LATENCY        : integer := 1;
        AV_READ_WAIT_TIME          : integer := 1;
        AV_WRITE_WAIT_TIME         : integer := 0;
        REGISTER_WAITREQUEST       : integer := 0;
        AV_REGISTERINCOMINGSIGNALS : integer := 0;
        VHDL_ID                    : integer := 0
    );
    port (
        clk                    : in  std_logic                     := 'X';             --       clk.clk
        reset                  : in  std_logic                     := 'X';             -- clk_reset.reset
        avm_address            : out std_logic_vector(1 downto 0);                     --        m0.address
        avm_readdata           : in  std_logic_vector(31 downto 0) := (others => 'X'); --          .readdata
        avm_writedata          : out std_logic_vector(31 downto 0);                    --          .writedata
        avm_write              : out std_logic;                                        --          .write
        avm_read               : out std_logic;                                        --          .read
        avm_arbiterlock        : out std_logic;
        avm_beginbursttransfer : out std_logic;
        avm_begintransfer      : out std_logic;
        avm_burstcount         : out std_logic_vector(0 downto 0);
        avm_byteenable         : out std_logic_vector(3 downto 0);
        avm_clken              : out std_logic;
        avm_debugaccess        : out std_logic;
        avm_lock               : out std_logic;
        avm_readdatavalid      : in  std_logic                     := 'X';
        avm_readid             : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_readresponse       : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_response           : in  std_logic_vector(1 downto 0)  := (others => 'X');
        avm_transactionid      : out std_logic_vector(7 downto 0);
        avm_waitrequest        : in  std_logic                     := 'X';
        avm_writeid            : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_writeresponse      : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_writeresponsevalid : in  std_logic                     := 'X'
    );
end component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm;

component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_sysid_s2_hps_bfm is
    generic (
        AV_ADDRESS_W               : integer := 32;
        AV_SYMBOL_W                : integer := 8;
        AV_NUMSYMBOLS              : integer := 4;
        AV_BURSTCOUNT_W            : integer := 3;
        AV_READRESPONSE_W          : integer := 8;
        AV_WRITERESPONSE_W         : integer := 8;
        USE_READ                   : integer := 1;
        USE_WRITE                  : integer := 1;
        USE_ADDRESS                : integer := 1;
        USE_BYTE_ENABLE            : integer := 1;
        USE_BURSTCOUNT             : integer := 1;
        USE_READ_DATA              : integer := 1;
        USE_READ_DATA_VALID        : integer := 1;
        USE_WRITE_DATA             : integer := 1;
        USE_BEGIN_TRANSFER         : integer := 0;
        USE_BEGIN_BURST_TRANSFER   : integer := 0;
        USE_WAIT_REQUEST           : integer := 1;
        USE_TRANSACTIONID          : integer := 0;
        USE_WRITERESPONSE          : integer := 0;
        USE_READRESPONSE           : integer := 0;
        USE_CLKEN                  : integer := 0;
        AV_CONSTANT_BURST_BEHAVIOR : integer := 1;
        AV_BURST_LINEWRAP          : integer := 1;
        AV_BURST_BNDR_ONLY         : integer := 1;
        AV_MAX_PENDING_READS       : integer := 0;
        AV_MAX_PENDING_WRITES      : integer := 0;
        AV_FIX_READ_LATENCY        : integer := 1;
        AV_READ_WAIT_TIME          : integer := 1;
        AV_WRITE_WAIT_TIME         : integer := 0;
        REGISTER_WAITREQUEST       : integer := 0;
        AV_REGISTERINCOMINGSIGNALS : integer := 0;
        VHDL_ID                    : integer := 0
    );
    port (
        clk                    : in  std_logic                     := 'X';             --       clk.clk
        reset                  : in  std_logic                     := 'X';             -- clk_reset.reset
        avm_address            : out std_logic_vector(0 downto 0);                     --        m0.address
        avm_readdata           : in  std_logic_vector(31 downto 0) := (others => 'X'); --          .readdata
        avm_arbiterlock        : out std_logic;
        avm_beginbursttransfer : out std_logic;
        avm_begintransfer      : out std_logic;
        avm_burstcount         : out std_logic_vector(0 downto 0);
        avm_byteenable         : out std_logic_vector(3 downto 0);
        avm_clken              : out std_logic;
        avm_debugaccess        : out std_logic;
        avm_lock               : out std_logic;
        avm_read               : out std_logic;
        avm_readdatavalid      : in  std_logic                     := 'X';
        avm_readid             : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_readresponse       : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_response           : in  std_logic_vector(1 downto 0)  := (others => 'X');
        avm_transactionid      : out std_logic_vector(7 downto 0);
        avm_waitrequest        : in  std_logic                     := 'X';
        avm_write              : out std_logic;
        avm_writedata          : out std_logic_vector(31 downto 0);
        avm_writeid            : in  std_logic_vector(7 downto 0)  := (others => 'X');
        avm_writeresponse      : in  std_logic_vector(0 downto 0)  := (others => 'X');
        avm_writeresponsevalid : in  std_logic                     := 'X'
    );
end component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_sysid_s2_hps_bfm;

begin

Resetn  <= not(Reset); 

ClockSys  <= not(ClockSys) after 5 ns;
ClockHPS  <= not(ClockHPS) after 10 ns;

i_mUSIC_Emulator: mUSIC_Emulator
  Port map(
    Clock                => ClockSys,
    Reset                => Reset,
    
    -- Emulator driver signals
    FSPnr                => mUSIC_Emul_FSPnr,
    FSP_RdWrn            => mUSIC_Emul_FSP_RdWrn,
    FSP_Dt2Wr            => mUSIC_Emul_FSP_Dt2Wr,
    RemainingBytes       => RemainingBytes,
    FSP_Start            => mUSIC_Emul_FSP_Start,
    FSP_NewDtRead        => open,
    FSP_DtRead           => open,
    -- USI interface
    Address              => USI_itf_Address,
    DataEn               => USI_itf_DataEn,
    RWn                  => USI_itf_RWn,
    Data                 => USI_itf_Data
  );

  i_ACU_AvalonMMM_USI_Bridge: ACU_AvalonMMM_USI_Bridge 

  Port map(
    Clock                => ClockSys,
    Reset                => Reset,

    -- USI interface
    Address              => USI_itf_Address,
    DataEn               => USI_itf_DataEn,
    RWn                  => USI_itf_RWn,
    Data                 => USI_itf_Data,
    RemainingBytes       => RemainingBytes,
    Available            => open,
    ReadOnly             => open,

    -- RAM interface (AMM_M)
    RAM_Addr             => RAM_Addr,
    RAM_WrCmd            => RAM_WrCmd,
    RAM_WrData           => RAM_WrData,
    RAM_RdData           => RAM_RdData,

    -- FIFO interface (AMM_M)
    FIFO_WrCmd           => FIFO_WrCmd,
    FIFO_WrData          => FIFO_WrData,

    -- Misc
    FPGA_DtFtchdEn       => FPGA_DtFtchdEn,
    FPGA_NewDtFtchd      => FPGA_NewDtFtchd,
    FPGA_DataFetched     => FPGA_DataFetched


  );
 
  i_ACU_HPS_QSys_Tb: component ACU_HPS_QSys_Tb
    port map (
        clk_clk                         => ClockHPS,
        fifo_mm_reset_fpga_reset_n      => Resetn,    

        fifo_mm_irq_fpga_irq            => irq_mapper_receiver0_irq,                                                --       fifo_mm_irq_fpga.irq      
        fifo_mm_s1_fpga_writedata       => FIFO_WrData, 
        fifo_mm_s1_fpga_write           => FIFO_WrCmd,    

        fifo_mm_s2_hps_readdata         => FIFO_RdData,   
        fifo_mm_s2_hps_read             => FIFO_RdCmd,     
                            --                       
        fifo_mm_st_fpga_address         => acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_address,                     --        fifo_mm_st_fpga.address
        fifo_mm_st_fpga_read            => acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_read,                        --                       .read
        fifo_mm_st_fpga_writedata       => acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_writedata,                   --                       .writedata
        fifo_mm_st_fpga_write           => acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_write,                       --                       .write
        fifo_mm_st_fpga_readdata        => acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_readdata,                    --                       .readdata

        fifo_mm_st_hps_address          => acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_address,                      --         fifo_mm_st_hps.address
        fifo_mm_st_hps_read             => acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_read,                         --                       .read
        fifo_mm_st_hps_writedata        => acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_writedata,                    --                       .writedata
        fifo_mm_st_hps_write            => acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_write,                        --                       .write
        fifo_mm_st_hps_readdata         => acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_readdata, 

        fifo_mm_sysclk_fpga_clk         => ClockSys,

        irq_lc_s2_hps_address           => acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_address,                       --          irq_lc_s2_hps.address
        irq_lc_s2_hps_writedata         => acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_writedata,                     --                       .writedata
        irq_lc_s2_hps_write             => acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_write,                         --                       .write
        irq_lc_s2_hps_read              => acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_read,                          --                       .read
        irq_lc_s2_hps_readdata          => acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_readdata,                      --                       .readdata

        jtag_uart_s2_hps_chipselect     => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_chipselect,      --       jtag_uart_s2_hps.chipselect
        jtag_uart_s2_hps_address        => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_address(0),      --                       .address
        jtag_uart_s2_hps_read_n         => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_read_ports_inv,  --                       .read_n
        jtag_uart_s2_hps_readdata       => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_readdata,        --                       .readdata
        jtag_uart_s2_hps_write_n        => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_write_ports_inv, --                       .write_n
        jtag_uart_s2_hps_writedata      => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_writedata,       --                       .writedata
        jtag_uart_s2_hps_waitrequest    => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_waitrequest,     --                       .waitrequest

        onchip_ram_reset_fpga_reset     => rst_controller_reset_out_reset,                                          --  onchip_ram_reset_fpga.reset
        onchip_ram_reset_fpga_reset_req => rst_controller_reset_out_reset_req,                                      --                       .reset_req
        onchip_ram_s1_fpga_address      => RAM_Addr,      
        onchip_ram_s1_fpga_clken        => '1',        
        onchip_ram_s1_fpga_chipselect   => '1',    
        onchip_ram_s1_fpga_write        => RAM_WrCmd,
        onchip_ram_s1_fpga_readdata     => RAM_RdData,
        onchip_ram_s1_fpga_writedata    => RAM_WrData,
        onchip_ram_s1_fpga_byteenable   => (others=>'1'), 

        onchip_ram_s2_hps_address       => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_address,        --      onchip_ram_s2_hps.address
        onchip_ram_s2_hps_chipselect    => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_chipselect,     --                       .chipselect
        onchip_ram_s2_hps_clken         => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_clken,          --                       .clken
        onchip_ram_s2_hps_write         => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_write,          --                       .write
        onchip_ram_s2_hps_readdata      => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_readdata,       --                       .readdata
        onchip_ram_s2_hps_writedata     => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_writedata,      --                       .writedata
        onchip_ram_s2_hps_byteenable    => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_byteenable,     --                       .byteenable
        onchip_ram_sysclk_fpga_clk      => ClockSys,

        pio_led_out_export              => open,     --            pio_led_out.export
        pio_led_s2_hps_address          => mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_address,           --         pio_led_s2_hps.address
        pio_led_s2_hps_write_n          => mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_write_ports_inv,   --                       .write_n
        pio_led_s2_hps_writedata        => mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_writedata,         --                       .writedata
        pio_led_s2_hps_chipselect       => mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_chipselect,        --                       .chipselect
        pio_led_s2_hps_readdata         => mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_readdata,          --                       .readdata
        
        reset_reset_n                   => Resetn,                              --                  reset.reset_n
        
        sysid_s2_hps_readdata           => acu_hps_qsys_tb_inst_sysid_s2_hps_bfm_m0_readdata,                       --           sysid_s2_hps.readdata
        sysid_s2_hps_address            => acu_hps_qsys_tb_inst_sysid_s2_hps_bfm_m0_address(0)                      --                       .address
    );

-- FIFO S2 master
acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm : component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm
    generic map (
        AV_ADDRESS_W               => 1,
        AV_SYMBOL_W                => 8,
        AV_NUMSYMBOLS              => 4,
        AV_BURSTCOUNT_W            => 1,
        AV_READRESPONSE_W          => 1,
        AV_WRITERESPONSE_W         => 1,
        USE_READ                   => 1,
        USE_WRITE                  => 0,
        USE_ADDRESS                => 0,
        USE_BYTE_ENABLE            => 0,
        USE_BURSTCOUNT             => 0,
        USE_READ_DATA              => 1,
        USE_READ_DATA_VALID        => 0,
        USE_WRITE_DATA             => 0,
        USE_BEGIN_TRANSFER         => 0,
        USE_BEGIN_BURST_TRANSFER   => 0,
        USE_WAIT_REQUEST           => 0,
        USE_TRANSACTIONID          => 0,
        USE_WRITERESPONSE          => 0,
        USE_READRESPONSE           => 0,
        USE_CLKEN                  => 0,
        AV_CONSTANT_BURST_BEHAVIOR => 1,
        AV_BURST_LINEWRAP          => 0,
        AV_BURST_BNDR_ONLY         => 0,
        AV_MAX_PENDING_READS       => 0,
        AV_MAX_PENDING_WRITES      => 0,
        AV_FIX_READ_LATENCY        => 1,
        AV_READ_WAIT_TIME          => 0,
        AV_WRITE_WAIT_TIME         => 0,
        REGISTER_WAITREQUEST       => 0,
        AV_REGISTERINCOMINGSIGNALS => 0,
        VHDL_ID                    => 1
    )
    port map (
        clk                    => ClockHPS,     
        reset                  => Reset, 
        avm_readdata           => FIFO_RdData,
        avm_read               => FIFO_RdCmd, 
        avm_address            => open,                                                 -- (terminated)
        avm_burstcount         => open,                                                 -- (terminated)
        avm_writedata          => open,                                                 -- (terminated)
        avm_begintransfer      => open,                                                 -- (terminated)
        avm_beginbursttransfer => open,                                                 -- (terminated)
        avm_waitrequest        => '0',                                                  -- (terminated)
        avm_write              => open,                                                 -- (terminated)
        avm_byteenable         => open,                                                 -- (terminated)
        avm_readdatavalid      => '0',                                                  -- (terminated)
        avm_arbiterlock        => open,                                                 -- (terminated)
        avm_lock               => open,                                                 -- (terminated)
        avm_debugaccess        => open,                                                 -- (terminated)
        avm_transactionid      => open,                                                 -- (terminated)
        avm_readid             => "00000000",                                           -- (terminated)
        avm_writeid            => "00000000",                                           -- (terminated)
        avm_clken              => open,                                                 -- (terminated)
        avm_response           => "00",                                                 -- (terminated)
        avm_writeresponsevalid => '0',                                                  -- (terminated)
        avm_readresponse       => "0",                                                  -- (terminated)
        avm_writeresponse      => "0"                                                   -- (terminated)
    );

    -- FIFO status master FPGA side
    acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm : component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm
		generic map (
			AV_ADDRESS_W               => 3,
			AV_SYMBOL_W                => 8,
			AV_NUMSYMBOLS              => 4,
			AV_BURSTCOUNT_W            => 1,
			AV_READRESPONSE_W          => 1,
			AV_WRITERESPONSE_W         => 1,
			USE_READ                   => 1,
			USE_WRITE                  => 1,
			USE_ADDRESS                => 1,
			USE_BYTE_ENABLE            => 0,
			USE_BURSTCOUNT             => 0,
			USE_READ_DATA              => 1,
			USE_READ_DATA_VALID        => 0,
			USE_WRITE_DATA             => 1,
			USE_BEGIN_TRANSFER         => 0,
			USE_BEGIN_BURST_TRANSFER   => 0,
			USE_WAIT_REQUEST           => 0,
			USE_TRANSACTIONID          => 0,
			USE_WRITERESPONSE          => 0,
			USE_READRESPONSE           => 0,
			USE_CLKEN                  => 0,
			AV_CONSTANT_BURST_BEHAVIOR => 1,
			AV_BURST_LINEWRAP          => 0,
			AV_BURST_BNDR_ONLY         => 0,
			AV_MAX_PENDING_READS       => 0,
			AV_MAX_PENDING_WRITES      => 0,
			AV_FIX_READ_LATENCY        => 0,
			AV_READ_WAIT_TIME          => 1,
			AV_WRITE_WAIT_TIME         => 0,
			REGISTER_WAITREQUEST       => 0,
			AV_REGISTERINCOMINGSIGNALS => 0,
			VHDL_ID                    => 2
		)
		port map (
			clk                    => ClockSys,
			reset                  => Reset,
			avm_address            => acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_address,               --        m0.address
			avm_readdata           => acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_readdata,              --          .readdata
			avm_writedata          => acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_writedata,             --          .writedata
			avm_write              => acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_write,                 --          .write
			avm_read               => acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_m0_read,                  --          .read
			avm_burstcount         => open,                                                              -- (terminated)
			avm_begintransfer      => open,                                                              -- (terminated)
			avm_beginbursttransfer => open,                                                              -- (terminated)
			avm_waitrequest        => '0',                                                               -- (terminated)
			avm_byteenable         => open,                                                              -- (terminated)
			avm_readdatavalid      => '0',                                                               -- (terminated)
			avm_arbiterlock        => open,                                                              -- (terminated)
			avm_lock               => open,                                                              -- (terminated)
			avm_debugaccess        => open,                                                              -- (terminated)
			avm_transactionid      => open,                                                              -- (terminated)
			avm_readid             => "00000000",                                                        -- (terminated)
			avm_writeid            => "00000000",                                                        -- (terminated)
			avm_clken              => open,                                                              -- (terminated)
			avm_response           => "00",                                                              -- (terminated)
			avm_writeresponsevalid => '0',                                                               -- (terminated)
			avm_readresponse       => "0",                                                               -- (terminated)
			avm_writeresponse      => "0"                                                                -- (terminated)
		);
    
    -- FIFO status master FPGA side
	acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm : component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm
		generic map (
			AV_ADDRESS_W               => 3,
			AV_SYMBOL_W                => 8,
			AV_NUMSYMBOLS              => 4,
			AV_BURSTCOUNT_W            => 1,
			AV_READRESPONSE_W          => 1,
			AV_WRITERESPONSE_W         => 1,
			USE_READ                   => 1,
			USE_WRITE                  => 1,
			USE_ADDRESS                => 1,
			USE_BYTE_ENABLE            => 0,
			USE_BURSTCOUNT             => 0,
			USE_READ_DATA              => 1,
			USE_READ_DATA_VALID        => 0,
			USE_WRITE_DATA             => 1,
			USE_BEGIN_TRANSFER         => 0,
			USE_BEGIN_BURST_TRANSFER   => 0,
			USE_WAIT_REQUEST           => 0,
			USE_TRANSACTIONID          => 0,
			USE_WRITERESPONSE          => 0,
			USE_READRESPONSE           => 0,
			USE_CLKEN                  => 0,
			AV_CONSTANT_BURST_BEHAVIOR => 1,
			AV_BURST_LINEWRAP          => 0,
			AV_BURST_BNDR_ONLY         => 0,
			AV_MAX_PENDING_READS       => 0,
			AV_MAX_PENDING_WRITES      => 0,
			AV_FIX_READ_LATENCY        => 0,
			AV_READ_WAIT_TIME          => 1,
			AV_WRITE_WAIT_TIME         => 0,
			REGISTER_WAITREQUEST       => 0,
			AV_REGISTERINCOMINGSIGNALS => 0,
			VHDL_ID                    => 3
		)
		port map (
			clk                    => ClockHPS, 
			reset                  => Reset, 
			avm_address            => acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_address,   --        m0.address
			avm_readdata           => acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_readdata,  --          .readdata
			avm_writedata          => acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_writedata, --          .writedata
			avm_write              => acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_write,     --          .write
			avm_read               => acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm_m0_read,      --          .read
			avm_burstcount         => open,                                                 -- (terminated)
			avm_begintransfer      => open,                                                 -- (terminated)
			avm_beginbursttransfer => open,                                                 -- (terminated)
			avm_waitrequest        => '0',                                                  -- (terminated)
			avm_byteenable         => open,                                                 -- (terminated)
			avm_readdatavalid      => '0',                                                  -- (terminated)
			avm_arbiterlock        => open,                                                 -- (terminated)
			avm_lock               => open,                                                 -- (terminated)
			avm_debugaccess        => open,                                                 -- (terminated)
			avm_transactionid      => open,                                                 -- (terminated)
			avm_readid             => "00000000",                                           -- (terminated)
			avm_writeid            => "00000000",                                           -- (terminated)
			avm_clken              => open,                                                 -- (terminated)
			avm_response           => "00",                                                 -- (terminated)
			avm_writeresponsevalid => '0',                                                  -- (terminated)
			avm_readresponse       => "0",                                                  -- (terminated)
			avm_writeresponse      => "0"                                                   -- (terminated)
		);

    -- FIFO interrupt manager FPGA side (sort of interconnect)
    irq_mapper : component altera_irq_mapper
		port map (
			clk           => ClockSys,              --       clk.clk
			reset         => Reset, -- clk_reset.reset
			receiver0_irq => irq_mapper_receiver0_irq,                                          -- receiver0.irq
			sender_irq    => acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_irq_irq                  --    sender.irq
		);
    
    -- FIFO interrupt sink FPGA side
    acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm : component altera_avalon_interrupt_sink_vhdl
		generic map (
			ASSERT_HIGH_IRQ        => 1,
			AV_IRQ_W               => 1,
			ASYNCHRONOUS_INTERRUPT => 0,
			VHDL_ID                => 0
		)
		port map (
			clk   => ClockSys,              --       clock_reset.clk
			reset => Reset, -- clock_reset_reset.reset
			irq   => acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_irq_irq                  --               irq.irq
		);

    -- Interrupt latency counter master
    acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm : component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm
		generic map (
			AV_ADDRESS_W               => 6,
			AV_SYMBOL_W                => 8,
			AV_NUMSYMBOLS              => 4,
			AV_BURSTCOUNT_W            => 1,
			AV_READRESPONSE_W          => 1,
			AV_WRITERESPONSE_W         => 1,
			USE_READ                   => 1,
			USE_WRITE                  => 1,
			USE_ADDRESS                => 1,
			USE_BYTE_ENABLE            => 0,
			USE_BURSTCOUNT             => 0,
			USE_READ_DATA              => 1,
			USE_READ_DATA_VALID        => 0,
			USE_WRITE_DATA             => 1,
			USE_BEGIN_TRANSFER         => 0,
			USE_BEGIN_BURST_TRANSFER   => 0,
			USE_WAIT_REQUEST           => 0,
			USE_TRANSACTIONID          => 0,
			USE_WRITERESPONSE          => 0,
			USE_READRESPONSE           => 0,
			USE_CLKEN                  => 0,
			AV_CONSTANT_BURST_BEHAVIOR => 1,
			AV_BURST_LINEWRAP          => 0,
			AV_BURST_BNDR_ONLY         => 0,
			AV_MAX_PENDING_READS       => 0,
			AV_MAX_PENDING_WRITES      => 0,
			AV_FIX_READ_LATENCY        => 1,
			AV_READ_WAIT_TIME          => 0,
			AV_WRITE_WAIT_TIME         => 0,
			REGISTER_WAITREQUEST       => 0,
			AV_REGISTERINCOMINGSIGNALS => 0,
			VHDL_ID                    => 4
		)
		port map (
			clk                    => ClockHPS,                 --       clk.clk
			reset                  => Reset, -- clk_reset.reset
			avm_address            => acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_address,    --        m0.address
			avm_readdata           => acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_readdata,   --          .readdata
			avm_writedata          => acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_writedata,  --          .writedata
			avm_write              => acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_write,      --          .write
			avm_read               => acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm_m0_read,       --          .read
			avm_burstcount         => open,                                                 -- (terminated)
			avm_begintransfer      => open,                                                 -- (terminated)
			avm_beginbursttransfer => open,                                                 -- (terminated)
			avm_waitrequest        => '0',                                                  -- (terminated)
			avm_byteenable         => open,                                                 -- (terminated)
			avm_readdatavalid      => '0',                                                  -- (terminated)
			avm_arbiterlock        => open,                                                 -- (terminated)
			avm_lock               => open,                                                 -- (terminated)
			avm_debugaccess        => open,                                                 -- (terminated)
			avm_transactionid      => open,                                                 -- (terminated)
			avm_readid             => "00000000",                                           -- (terminated)
			avm_writeid            => "00000000",                                           -- (terminated)
			avm_clken              => open,                                                 -- (terminated)
			avm_response           => "00",                                                 -- (terminated)
			avm_writeresponsevalid => '0',                                                  -- (terminated)
			avm_readresponse       => "0",                                                  -- (terminated)
			avm_writeresponse      => "0"                                                   -- (terminated)
		);

    -- JTAG UART Interconnect
    mm_interconnect_5 : component altera_mm_interconnect
        port map (
          ACU_HPS_QSys_Tb_inst_clk_bfm_clk_clk                                            => ClockHPS,                                --                                          ACU_HPS_QSys_Tb_inst_clk_bfm_clk.clk
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_clk_reset_reset_bridge_in_reset_reset => Reset,      
                    ACU_HPS_QSys_Tb_inst_reset_reset_bridge_in_reset_reset                          => Reset,                --                          ACU_HPS_QSys_Tb_inst_reset_reset_bridge_in_reset.reset
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_address                            => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_address,                --                              ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0.address
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_waitrequest                        => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_waitrequest,            --                                                                          .waitrequest
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_read                               => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_read,                   --                                                                          .read
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_readdata                           => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_readdata,               --                                                                          .readdata
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_write                              => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_write,                  --                                                                          .write
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_bfm_m0_writedata                          => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_writedata,              --                                                                          .writedata
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_address                                   => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_address,     --                                     ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps.address
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_write                                     => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_write,       --                                                                          .write
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_read                                      => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_read,        --                                                                          .read
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_readdata                                  => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_readdata,    --                                                                          .readdata
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_writedata                                 => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_writedata,   --                                                                          .writedata
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_waitrequest                               => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_waitrequest, --                                                                          .waitrequest
          ACU_HPS_QSys_Tb_inst_jtag_uart_s2_hps_chipselect                                => mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_chipselect   --                                                                          .chipselect
    );
    mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_read_ports_inv <= not mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_read;

	mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_write_ports_inv <= not mm_interconnect_5_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_write;


    -- JTAG UART master
    acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm : component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm
		generic map (
			AV_ADDRESS_W               => 3,
			AV_SYMBOL_W                => 8,
			AV_NUMSYMBOLS              => 4,
			AV_BURSTCOUNT_W            => 1,
			AV_READRESPONSE_W          => 1,
			AV_WRITERESPONSE_W         => 1,
			USE_READ                   => 1,
			USE_WRITE                  => 1,
			USE_ADDRESS                => 1,
			USE_BYTE_ENABLE            => 0,
			USE_BURSTCOUNT             => 0,
			USE_READ_DATA              => 1,
			USE_READ_DATA_VALID        => 0,
			USE_WRITE_DATA             => 1,
			USE_BEGIN_TRANSFER         => 0,
			USE_BEGIN_BURST_TRANSFER   => 0,
			USE_WAIT_REQUEST           => 1,
			USE_TRANSACTIONID          => 0,
			USE_WRITERESPONSE          => 0,
			USE_READRESPONSE           => 0,
			USE_CLKEN                  => 0,
			AV_CONSTANT_BURST_BEHAVIOR => 1,
			AV_BURST_LINEWRAP          => 0,
			AV_BURST_BNDR_ONLY         => 0,
			AV_MAX_PENDING_READS       => 0,
			AV_MAX_PENDING_WRITES      => 0,
			AV_FIX_READ_LATENCY        => 0,
			AV_READ_WAIT_TIME          => 1,
			AV_WRITE_WAIT_TIME         => 0,
			REGISTER_WAITREQUEST       => 0,
			AV_REGISTERINCOMINGSIGNALS => 0,
			VHDL_ID                    => 5
		)
		port map (
			clk                    => ClockHPS,
			reset                  => Reset,
			avm_address            => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_address,     --        m0.address
			avm_readdata           => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_readdata,    --          .readdata
			avm_writedata          => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_writedata,   --          .writedata
			avm_waitrequest        => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_waitrequest, --          .waitrequest
			avm_write              => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_write,       --          .write
			avm_read               => acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm_m0_read,        --          .read
			avm_burstcount         => open,                                                     -- (terminated)
			avm_begintransfer      => open,                                                     -- (terminated)
			avm_beginbursttransfer => open,                                                     -- (terminated)
			avm_byteenable         => open,                                                     -- (terminated)
			avm_readdatavalid      => '0',                                                      -- (terminated)
			avm_arbiterlock        => open,                                                     -- (terminated)
			avm_lock               => open,                                                     -- (terminated)
			avm_debugaccess        => open,                                                     -- (terminated)
			avm_transactionid      => open,                                                     -- (terminated)
			avm_readid             => "00000000",                                               -- (terminated)
			avm_writeid            => "00000000",                                               -- (terminated)
			avm_clken              => open,                                                     -- (terminated)
			avm_response           => "00",                                                     -- (terminated)
			avm_writeresponsevalid => '0',                                                      -- (terminated)
			avm_readresponse       => "0",                                                      -- (terminated)
			avm_writeresponse      => "0"                                                       -- (terminated)
		);
    
    -- On chip RAM interconnect
    mm_interconnect_7 : component altera_mm_interconnect_0003
		port map (
			ACU_HPS_QSys_Tb_inst_clk_bfm_clk_clk                                             => ClockHPS,                                --                                           ACU_HPS_QSys_Tb_inst_clk_bfm_clk.clk
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_clk_reset_reset_bridge_in_reset_reset => Reset,                -- ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_clk_reset_reset_bridge_in_reset.reset
			ACU_HPS_QSys_Tb_inst_reset_reset_bridge_in_reset_reset                           => Reset,                --                           ACU_HPS_QSys_Tb_inst_reset_reset_bridge_in_reset.reset
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_address                            => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_address,               --                              ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0.address
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_byteenable                         => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_byteenable,            --                                                                           .byteenable
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_read                               => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_read,                  --                                                                           .read
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_readdata                           => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_readdata,              --                                                                           .readdata
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_write                              => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_write,                 --                                                                           .write
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_writedata                          => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_writedata,             --                                                                           .writedata
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_bfm_m0_clken                              => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_clken,                 --                                                                           .clken
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_address                                   => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_address,    --                                     ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps.address
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_write                                     => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_write,      --                                                                           .write
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_readdata                                  => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_readdata,   --                                                                           .readdata
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_writedata                                 => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_writedata,  --                                                                           .writedata
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_byteenable                                => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_byteenable, --                                                                           .byteenable
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_chipselect                                => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_chipselect, --                                                                           .chipselect
			ACU_HPS_QSys_Tb_inst_onchip_ram_s2_hps_clken                                     => mm_interconnect_7_acu_hps_qsys_tb_inst_onchip_ram_s2_hps_clken       --                                                                           .clken
		);
        
    -- On chip RAM master (hps side)
    acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm : component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_bfm
		generic map (
			AV_ADDRESS_W               => 10,
			AV_SYMBOL_W                => 8,
			AV_NUMSYMBOLS              => 4,
			AV_BURSTCOUNT_W            => 1,
			AV_READRESPONSE_W          => 1,
			AV_WRITERESPONSE_W         => 1,
			USE_READ                   => 1,
			USE_WRITE                  => 1,
			USE_ADDRESS                => 1,
			USE_BYTE_ENABLE            => 1,
			USE_BURSTCOUNT             => 0,
			USE_READ_DATA              => 1,
			USE_READ_DATA_VALID        => 0,
			USE_WRITE_DATA             => 1,
			USE_BEGIN_TRANSFER         => 0,
			USE_BEGIN_BURST_TRANSFER   => 0,
			USE_WAIT_REQUEST           => 0,
			USE_TRANSACTIONID          => 0,
			USE_WRITERESPONSE          => 0,
			USE_READRESPONSE           => 0,
			USE_CLKEN                  => 1,
			AV_CONSTANT_BURST_BEHAVIOR => 1,
			AV_BURST_LINEWRAP          => 0,
			AV_BURST_BNDR_ONLY         => 0,
			AV_MAX_PENDING_READS       => 0,
			AV_MAX_PENDING_WRITES      => 0,
			AV_FIX_READ_LATENCY        => 1,
			AV_READ_WAIT_TIME          => 0,
			AV_WRITE_WAIT_TIME         => 0,
			REGISTER_WAITREQUEST       => 0,
			AV_REGISTERINCOMINGSIGNALS => 0,
			VHDL_ID                    => 7
		)
		port map (
			clk                    => ClockHPS,                     --       clk.clk
			reset                  => Reset,     -- clk_reset.reset
			avm_address            => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_address,    --        m0.address
			avm_readdata           => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_readdata,   --          .readdata
			avm_writedata          => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_writedata,  --          .writedata
			avm_write              => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_write,      --          .write
			avm_read               => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_read,       --          .read
			avm_byteenable         => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_byteenable, --          .byteenable
			avm_clken              => acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm_m0_clken,      --          .clken
			avm_burstcount         => open,                                                     -- (terminated)
			avm_begintransfer      => open,                                                     -- (terminated)
			avm_beginbursttransfer => open,                                                     -- (terminated)
			avm_waitrequest        => '0',                                                      -- (terminated)
			avm_readdatavalid      => '0',                                                      -- (terminated)
			avm_arbiterlock        => open,                                                     -- (terminated)
			avm_lock               => open,                                                     -- (terminated)
			avm_debugaccess        => open,                                                     -- (terminated)
			avm_transactionid      => open,                                                     -- (terminated)
			avm_readid             => "00000000",                                               -- (terminated)
			avm_writeid            => "00000000",                                               -- (terminated)
			avm_response           => "00",                                                     -- (terminated)
			avm_writeresponsevalid => '0',                                                      -- (terminated)
			avm_readresponse       => "0",                                                      -- (terminated)
			avm_writeresponse      => "0"                                                       -- (terminated)
		);

    -- Reset manager
    rst_controller : component altera_reset_controller
        generic map (
            NUM_RESET_INPUTS          => 1,
            OUTPUT_RESET_SYNC_EDGES   => "deassert",
            SYNC_DEPTH                => 2,
        RESET_REQUEST_PRESENT     => 1,
        RESET_REQ_WAIT_TIME       => 1,
        MIN_RST_ASSERTION_TIME    => 3,
        RESET_REQ_EARLY_DSRT_TIME => 1,
        USE_RESET_REQUEST_IN0     => 0,
        USE_RESET_REQUEST_IN1     => 0,
        USE_RESET_REQUEST_IN2     => 0,
        USE_RESET_REQUEST_IN3     => 0,
        USE_RESET_REQUEST_IN4     => 0,
        USE_RESET_REQUEST_IN5     => 0,
        USE_RESET_REQUEST_IN6     => 0,
        USE_RESET_REQUEST_IN7     => 0,
        USE_RESET_REQUEST_IN8     => 0,
        USE_RESET_REQUEST_IN9     => 0,
        USE_RESET_REQUEST_IN10    => 0,
        USE_RESET_REQUEST_IN11    => 0,
        USE_RESET_REQUEST_IN12    => 0,
        USE_RESET_REQUEST_IN13    => 0,
        USE_RESET_REQUEST_IN14    => 0,
        USE_RESET_REQUEST_IN15    => 0,
        ADAPT_RESET_REQUEST       => 0
    )
    port map (
        reset_in0      => Reset, -- reset_in0.reset
        clk            => ClockSys,    --       clk.clk
        reset_out      => rst_controller_reset_out_reset,                             -- reset_out.reset
        reset_req      => rst_controller_reset_out_reset_req,                         --          .reset_req
        reset_req_in0  => '0',                                                        -- (terminated)
        reset_in1      => '0',                                                        -- (terminated)
        reset_req_in1  => '0',                                                        -- (terminated)
        reset_in2      => '0',                                                        -- (terminated)
        reset_req_in2  => '0',                                                        -- (terminated)
        reset_in3      => '0',                                                        -- (terminated)
        reset_req_in3  => '0',                                                        -- (terminated)
        reset_in4      => '0',                                                        -- (terminated)
        reset_req_in4  => '0',                                                        -- (terminated)
        reset_in5      => '0',                                                        -- (terminated)
        reset_req_in5  => '0',                                                        -- (terminated)
        reset_in6      => '0',                                                        -- (terminated)
        reset_req_in6  => '0',                                                        -- (terminated)
        reset_in7      => '0',                                                        -- (terminated)
        reset_req_in7  => '0',                                                        -- (terminated)
        reset_in8      => '0',                                                        -- (terminated)
        reset_req_in8  => '0',                                                        -- (terminated)
        reset_in9      => '0',                                                        -- (terminated)
        reset_req_in9  => '0',                                                        -- (terminated)
        reset_in10     => '0',                                                        -- (terminated)
        reset_req_in10 => '0',                                                        -- (terminated)
        reset_in11     => '0',                                                        -- (terminated)
        reset_req_in11 => '0',                                                        -- (terminated)
        reset_in12     => '0',                                                        -- (terminated)
        reset_req_in12 => '0',                                                        -- (terminated)
        reset_in13     => '0',                                                        -- (terminated)
        reset_req_in13 => '0',                                                        -- (terminated)
        reset_in14     => '0',                                                        -- (terminated)
        reset_req_in14 => '0',                                                        -- (terminated)
        reset_in15     => '0',                                                        -- (terminated)
        reset_req_in15 => '0'                                                         -- (terminated)
    );

-- LED PIO Interconnect
	mm_interconnect_8 : component altera_mm_interconnect_0004
		port map (
			ACU_HPS_QSys_Tb_inst_clk_bfm_clk_clk                                          => ClockHPS,                             --                                        ACU_HPS_QSys_Tb_inst_clk_bfm_clk.clk
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_clk_reset_reset_bridge_in_reset_reset => Reset,             -- ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_clk_reset_reset_bridge_in_reset.reset
			ACU_HPS_QSys_Tb_inst_reset_reset_bridge_in_reset_reset                        => Reset,             --                        ACU_HPS_QSys_Tb_inst_reset_reset_bridge_in_reset.reset
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0_address                            => acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_address,               --                              ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0.address
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0_read                               => acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_read,                  --                                                                        .read
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0_readdata                           => acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_readdata,              --                                                                        .readdata
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0_write                              => acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_write,                 --                                                                        .write
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_bfm_m0_writedata                          => acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_writedata,             --                                                                        .writedata
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_address                                   => mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_address,    --                                     ACU_HPS_QSys_Tb_inst_pio_led_s2_hps.address
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_write                                     => mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_write,      --                                                                        .write
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_readdata                                  => mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_readdata,   --                                                                        .readdata
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_writedata                                 => mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_writedata,  --                                                                        .writedata
			ACU_HPS_QSys_Tb_inst_pio_led_s2_hps_chipselect                                => mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_chipselect  --                                                                        .chipselect
		);
    
    mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_write_ports_inv <= not mm_interconnect_8_acu_hps_qsys_tb_inst_pio_led_s2_hps_write;

-- LED PIO master
acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm : component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm
    generic map (
        AV_ADDRESS_W               => 3,
        AV_SYMBOL_W                => 8,
        AV_NUMSYMBOLS              => 4,
        AV_BURSTCOUNT_W            => 1,
        AV_READRESPONSE_W          => 1,
        AV_WRITERESPONSE_W         => 1,
        USE_READ                   => 1,
        USE_WRITE                  => 1,
        USE_ADDRESS                => 1,
        USE_BYTE_ENABLE            => 0,
        USE_BURSTCOUNT             => 0,
        USE_READ_DATA              => 1,
        USE_READ_DATA_VALID        => 0,
        USE_WRITE_DATA             => 1,
        USE_BEGIN_TRANSFER         => 0,
        USE_BEGIN_BURST_TRANSFER   => 0,
        USE_WAIT_REQUEST           => 0,
        USE_TRANSACTIONID          => 0,
        USE_WRITERESPONSE          => 0,
        USE_READRESPONSE           => 0,
        USE_CLKEN                  => 0,
        AV_CONSTANT_BURST_BEHAVIOR => 1,
        AV_BURST_LINEWRAP          => 0,
        AV_BURST_BNDR_ONLY         => 0,
        AV_MAX_PENDING_READS       => 0,
        AV_MAX_PENDING_WRITES      => 0,
        AV_FIX_READ_LATENCY        => 0,
        AV_READ_WAIT_TIME          => 1,
        AV_WRITE_WAIT_TIME         => 0,
        REGISTER_WAITREQUEST       => 0,
        AV_REGISTERINCOMINGSIGNALS => 0,
        VHDL_ID                    => 8
    )
    port map (
        clk                    => ClockHPS,                 --       clk.clk
        reset                  => Reset, -- clk_reset.reset
        avm_address            => acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_address,   --        m0.address
        avm_readdata           => acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_readdata,  --          .readdata
        avm_writedata          => acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_writedata, --          .writedata
        avm_write              => acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_write,     --          .write
        avm_read               => acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm_m0_read,      --          .read
        avm_burstcount         => open,                                                 -- (terminated)
        avm_begintransfer      => open,                                                 -- (terminated)
        avm_beginbursttransfer => open,                                                 -- (terminated)
        avm_waitrequest        => '0',                                                  -- (terminated)
        avm_byteenable         => open,                                                 -- (terminated)
        avm_readdatavalid      => '0',                                                  -- (terminated)
        avm_arbiterlock        => open,                                                 -- (terminated)
        avm_lock               => open,                                                 -- (terminated)
        avm_debugaccess        => open,                                                 -- (terminated)
        avm_transactionid      => open,                                                 -- (terminated)
        avm_readid             => "00000000",                                           -- (terminated)
        avm_writeid            => "00000000",                                           -- (terminated)
        avm_clken              => open,                                                 -- (terminated)
        avm_response           => "00",                                                 -- (terminated)
        avm_writeresponsevalid => '0',                                                  -- (terminated)
        avm_readresponse       => "0",                                                  -- (terminated)
        avm_writeresponse      => "0"                                                   -- (terminated)
    );
-- System ID master
acu_hps_qsys_tb_inst_sysid_s2_hps_bfm : component acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_sysid_s2_hps_bfm
    generic map (
        AV_ADDRESS_W               => 3,
        AV_SYMBOL_W                => 8,
        AV_NUMSYMBOLS              => 4,
        AV_BURSTCOUNT_W            => 1,
        AV_READRESPONSE_W          => 1,
        AV_WRITERESPONSE_W         => 1,
        USE_READ                   => 1,
        USE_WRITE                  => 1,
        USE_ADDRESS                => 1,
        USE_BYTE_ENABLE            => 0,
        USE_BURSTCOUNT             => 0,
        USE_READ_DATA              => 1,
        USE_READ_DATA_VALID        => 0,
        USE_WRITE_DATA             => 1,
        USE_BEGIN_TRANSFER         => 0,
        USE_BEGIN_BURST_TRANSFER   => 0,
        USE_WAIT_REQUEST           => 0,
        USE_TRANSACTIONID          => 0,
        USE_WRITERESPONSE          => 0,
        USE_READRESPONSE           => 0,
        USE_CLKEN                  => 0,
        AV_CONSTANT_BURST_BEHAVIOR => 1,
        AV_BURST_LINEWRAP          => 0,
        AV_BURST_BNDR_ONLY         => 0,
        AV_MAX_PENDING_READS       => 0,
        AV_MAX_PENDING_WRITES      => 0,
        AV_FIX_READ_LATENCY        => 0,
        AV_READ_WAIT_TIME          => 1,
        AV_WRITE_WAIT_TIME         => 0,
        REGISTER_WAITREQUEST       => 0,
        AV_REGISTERINCOMINGSIGNALS => 0,
        VHDL_ID                    => 9
    )
    port map (
        clk                    => ClockHPS,                 --       clk.clk
        reset                  => Reset, -- clk_reset.reset
        avm_address            => acu_hps_qsys_tb_inst_sysid_s2_hps_bfm_m0_address,     --        m0.address
        avm_readdata           => acu_hps_qsys_tb_inst_sysid_s2_hps_bfm_m0_readdata,    --          .readdata
        avm_burstcount         => open,                                                 -- (terminated)
        avm_writedata          => open,                                                 -- (terminated)
        avm_begintransfer      => open,                                                 -- (terminated)
        avm_beginbursttransfer => open,                                                 -- (terminated)
        avm_waitrequest        => '0',                                                  -- (terminated)
        avm_write              => open,                                                 -- (terminated)
        avm_read               => open,                                                 -- (terminated)
        avm_byteenable         => open,                                                 -- (terminated)
        avm_readdatavalid      => '0',                                                  -- (terminated)
        avm_arbiterlock        => open,                                                 -- (terminated)
        avm_lock               => open,                                                 -- (terminated)
        avm_debugaccess        => open,                                                 -- (terminated)
        avm_transactionid      => open,                                                 -- (terminated)
        avm_readid             => "00000000",                                           -- (terminated)
        avm_writeid            => "00000000",                                           -- (terminated)
        avm_clken              => open,                                                 -- (terminated)
        avm_response           => "00",                                                 -- (terminated)
        avm_writeresponsevalid => '0',                                                  -- (terminated)
        avm_readresponse       => "0",                                                  -- (terminated)
        avm_writeresponse      => "0"                                                   -- (terminated)
    );
end architecture beh;
