	ACU_HPS_QSys_Tb_tb u0 (
		.acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_address       (<connected-to-acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_address>),       //         acu_hps_qsys_tb_inst_onchip_ram_s1_fpga.address
		.acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_clken         (<connected-to-acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_clken>),         //                                                .clken
		.acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_chipselect    (<connected-to-acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_chipselect>),    //                                                .chipselect
		.acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_write         (<connected-to-acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_write>),         //                                                .write
		.acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_readdata      (<connected-to-acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_readdata>),      //                                                .readdata
		.acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_writedata     (<connected-to-acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_writedata>),     //                                                .writedata
		.acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_byteenable    (<connected-to-acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_byteenable>),    //                                                .byteenable
		.acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_writedata        (<connected-to-acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_writedata>),        //            acu_hps_qsys_tb_inst_fifo_mm_s1_fpga.writedata
		.acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_write            (<connected-to-acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_write>),            //                                                .write
		.acu_hps_qsys_tb_inst_onchip_ram_sysclk_fpga_clk       (<connected-to-acu_hps_qsys_tb_inst_onchip_ram_sysclk_fpga_clk>),       //     acu_hps_qsys_tb_inst_onchip_ram_sysclk_fpga.clk
		.acu_hps_qsys_tb_inst_fifo_mm_sysclk_fpga_clk          (<connected-to-acu_hps_qsys_tb_inst_fifo_mm_sysclk_fpga_clk>),          //        acu_hps_qsys_tb_inst_fifo_mm_sysclk_fpga.clk
		.acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_clock_clk   (<connected-to-acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_clock_clk>),   // acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_clock.clk
		.acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_clk_clk      (<connected-to-acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_clk_clk>),      //    acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_clk.clk
		.acu_hps_qsys_tb_inst_fifo_mm_reset_fpga_reset_n       (<connected-to-acu_hps_qsys_tb_inst_fifo_mm_reset_fpga_reset_n>),       //         acu_hps_qsys_tb_inst_fifo_mm_reset_fpga.reset_n
		.acu_hps_qsys_tb_inst_onchip_ram_reset_fpga_reset      (<connected-to-acu_hps_qsys_tb_inst_onchip_ram_reset_fpga_reset>),      //      acu_hps_qsys_tb_inst_onchip_ram_reset_fpga.reset
		.acu_hps_qsys_tb_inst_onchip_ram_reset_fpga_reset_req  (<connected-to-acu_hps_qsys_tb_inst_onchip_ram_reset_fpga_reset_req>),  //                                                .reset_req
		.acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_reset_reset (<connected-to-acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_reset_reset>), // acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_reset.reset
		.acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_reset_reset  (<connected-to-acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_reset_reset>)   //  acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_reset.reset
	);

