// Copyright (C) 2020 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 20.1std.1
// ALTERA_TIMESTAMP:Thu Nov 12 05:58:26 PST 2020
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
cQZwNwoPn8HFKtY3IVJL1KpHn5G6jU0gvHzIOxptyCpFdmiTt8gx9JIxrmUeXb4T
ta6N5RSA5ed26ZSRLmx7LDRbprLg7bcQ60qwym4ykvdpf2pO7J8TcBQNcp4q03BY
c7sJU4I6bn7kFlTURBRihXLTzxJoc+pmtw/jEiGHMik=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2336)
y5nU1FlNiX2N5/jENTMBl4Fc71U0RsmhiNJGBcg22b6HEczVkaH+oOY7j4UVHWUm
7bNz7qO3Beicx9arh9ngSoAylIYFT9Ahp5cZ8mVyvpKQA63Mm9fUINpSNi7oD/3k
PHqNGBvHD21JD3FprwhKWWJiDVcFCxMka62jb0Bf2EDfPIxQ5i3OdaJ6mLkvS0jn
sCxwPa+Q1LMpkWtr6vFSSYufB2b/+RQ0YmIUhUWseZ/pVDXIj4UCUtj0W7CuOufZ
sZBLFvI1qWi+H5s3BaKO0o0ccFlS6hvrIWSosFbtlQfrBZsi71GuQu1jAIBAb1Xz
f9itJKsZzKyH/LyPvVfTGvg2netcCRt0agAvgCtM9j4pdppi7V/ClPdUaq7iZA2O
rhJgp8gAABJ/wujqbKqLwXYvhYcG43NagOTk6cRsezXForTgpSdOPgMYgunu9l8O
se4SLYtAf8LZOWznCrI8tbRMD3Owe2W+rPyrUu3HrWrS0y+z0HzXMXVn8wCxhGIx
7p7xzeTGLfi79+tvrnMjwEnz+1HhHL3npZhEb6krFAyGdoAd2IbVAAjOWcV8CuEk
M0kp35wDTYzAgq6rzKm9T+VbM0I3m3xNQaYuG0U5h1r7DInACICZhXLR47sCM7G+
FLIj2cg9Msgb/DI487QA5i8DYEoImgM/7HZsm8Y5Q8pD7RgykIFqSHvLMyIhSsd2
bp9sdDEagntjZgcWVSmgcbA9UTCiUApKzqkWuvjKF1K3DTORg/L1ROblyz4lVrBL
Lwip5OXddFu6XrbsmFBkfi5uaAbEX68d/glp6E/pMSaDsJlCvaFUfJ7d/X8va3Wu
75JaWRDtR/fy86ibhH7goE+kDFijUKGDYZcpwb8eoTnSMsqBaGn2ixmJgvOI8p2t
76B7dW5eWwYKY1iHKNimknXSSM9+jo74N2lJvhH1jeF9zG1oNSPFhWsFVgwDqDOL
Ap6jtXCpaUwYF4BP/f1RznsnYU93gutNPyjv+oT3GpXVRW32HdQIzviZ2BObU4GF
4n/F1mHe1jlewewGn0umMr1acrMsrVbJ8Tb+tRugE8S+FvGxjs6p8bNqRbdUDbDj
rrm/y4PmuEbiRbi3HfxOvWQzGCmvM2dig4tPG19pvvuhn4KlGnCn81s5wha4jUCZ
mXy4CXS4/GUXI5e1/1H83hiSHDsz2hCV+IeaPRBYZTkZdut/YDCh6hbH4VXnqKkt
0Jx1+XnqUNCjEtDx8n9Ixvxl/GODyDBLkzHBDDndvpGYiYtRO2W1xI6sIz3fUAsC
Gr6VfFHBmDM7gWifSQrhUW5yZMaF6JNyo44l0QPy1P4FyhvmTJaN4JRPui9GVlkg
Rg5CeaMNYcZFN+HwluOY2YTIWIpW4qP+BaorTxaMQ+/G6BQ5MRj57rsDdho2J2zj
QDCxl999mM1KnFg7/4Rv/I2Dn182wXl0lYZ9II42G1HKynaS4ltF7n1+3rykaJk8
BmIEFvtb0ojTq16TiK1wyScKb6U3I4Qnv1E65ohr4f6voflmdgtduaAgsse1QdiC
ffg4hwwSCUYrRxWCqKfF1uLsf3mGZila/p9PoRQzFYTktyJLDi9DjCywCMZ0KUAB
wF1HrlP139/qi1dXjGzG8thv8stw/8MNNk2O3BoL9bghygJGcopv9iR2hqLVykFI
uq9v97fWTOwmePvZGaJlTTIrn+UumT9FaoVbIWByAohx3wjt2uVuohbsJbqL86B9
jjYWX1SJ9A7PIHohdTuEuJqWLFxQ5kUPkH9p0Hy4WPfaIYmGS1YdVk14SWwX31DS
Btgiwu3w8DT//NITfzmSS1aRFYCjUIXpfsQ8njZvMKW4OJfKFt28kPt2L25SHVTi
vbyDMeqDkzzMImk7ahyHcd7vppSlOuE4J6V+OsUqz0zSkd1glF0RU56snzA5JiNG
VpJpKFnC79UIEfR7flRERyKL3iHE1NAXSVWqpSK+9FeunL3118pEj1ykEoTspK3t
33csVMp6xP6onSXh/pUqRN/jX4MQL+jdgINIt+G0SAV/kAINBcsle8BhcU6+giCD
N6/JCn3FS14UYtC3HF6JuDbyl3k5dXb5vOGjmLtOPOZHKS2pnSpVhT6LSEFn04gR
7MJz/8TiZ/eKvRjQXBZQltFMNujZFe1/FlVQiAHXXcGijhsE2KG2JZ0glrMDZvjI
Sdo6JQRqyzWv7rZvhouGd/F1zhLl9+77xFfXuDqTUoztSiohdq0TC9DVWMulbtHc
SM6DyNwmdzM0zU2ipDmdRLxhMhSywpY4z3/pof+CDR1+5NMW/HItcyX41I6s31jn
JfN8vQRzeC9KygaVQasx4hqYO9aMkxERTFb8iGbSw9JqFDn0lIbVh3LaCXOlJ8qf
X7vdBbRmdfvtEs7pVgvn84gPMDsqTWZrtinP+TNvGT1INE3cd+C6PZnQjNaczXj3
KJoVKlwE0mTGjk4RNRA6qMtG33QFILpFyS1ryUzosZkcOibUj1peFQolYAxUpBcI
O9CrC3Ns0o80uunQNq9Mp+aACyhWM2LkSaQM2ADMKvGmB/xsbVtpM2V6FSvswVpC
amMbhy3xmFwMrjkD9xnb+2WZb8ec/AncbUpkK0jyPFle80WuJcmtzVPfPBFu8zF9
RVLkd8QiHYFvSgnkij1MzR6BNQGJN8IVrWmMhMJj6N/MqHl+wyJaE5CD0uID22go
pMjnkmIVe9NIxJ6BMuWoc5GiYOSof+5phTDaJp28TN5YWpLcnzzwjwqoiLfcojNA
cSFX9Mwhb4+6h453sqZN1bUChhhhdNH6GmGv4aNp11NOBiEcv2VlqcGjCA4NZjbW
R/+2yrHxTWLAsyODT+fzAQviQodbIAny7ONel6WLRAmzm9Bjd3c0GrANggTZ+MpW
6yXnOEmKXN7XQMt9w5lq/yn+gDCQQhXBVEYt/OxVuxHDq4C5iNElSrrWRSQP/ov/
UvdtoC4aasRGCFghSKOYq+NZZPOQsq/TWEpuw+YWyGfanC0I82EOW3VQkBhDVLFJ
2vWYGU2MNBxxGinDEmZf5XQrG6XoGesLWY6Pv4AgvUzLsa3m1a+7rgWdgZKJks0k
UJVxS1XiTOp7ve0qY5HsL55PVu2NKU32Idefb+qI2Y0=
`pragma protect end_protected
