restart -f


run 1 us
force -freeze sim:/acu_hps_qsys_tb_tb/Reset 0 0

run 1 us


##############################
# Read SysID
##############################
puts "Read SysID"
force -deposit sim:/acu_hps_qsys_tb_tb/RdSysID_Flag 1 0
run 200 ns

##############################
# Read PIO LED registers (0x0 even if the span in the address map is 0x10)
##############################
puts "Read PIO LED"
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 0 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdPIO_LED_Flag 1 0
run 200 ns


##############################
# Write PIO LED registers (0x0 even if the span in the address map is 0x10)
##############################
puts "Write PIO LED"
set PIO_LED_DtWr [list 101 88 70 51]

for {set i 0} {$i < 4} {incr i} {
    puts "Addr $i"
    set SelDt2Wr [lindex $PIO_LED_DtWr $i]
    puts "Dt2Wr $SelDt2Wr"
    force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 0 0
    force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM $SelDt2Wr 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdPIO_LED_Flag 1 0
    force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
    run 500 ns
}


##############################
# Read RAM registers HPS side 
##############################
set RAMAddr [list 0 1 2 3 32 64 128 256 512 1023]
puts "Read RAM registers HPS side"
for {set i 0} {$i < 10} {incr i} {
    set selAddr [lindex $RAMAddr $i]
    puts "Addr $selAddr"
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $selAddr 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdRAMhps_Flag 1 0
    run 200 ns
}


##############################
# Write RAM registers HPS side 
##############################
puts "Write RAM registers HPS side"
set RAmDt2Wr [list 70 101 88 51 855638016 1476395008 1174405120 1694498816 654311424 570425344]
for {set i 0} {$i < 10} {incr i} {
    set selAddr [lindex $RAMAddr $i]
    set selDt2Wr [lindex $RAmDt2Wr $i]
    puts "Addr $selAddr"
    force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM $selAddr 0
    force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM $selDt2Wr 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdRAMhps_Flag 1 0
    force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
    run 500 ns
}

##############################
# Read RAM registers HPS side 
##############################
puts "Read RAM registers HPS side"
for {set i 0} {$i < 10} {incr i} {
    set selAddr [lindex $RAMAddr $i]
    puts "Addr $selAddr"
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $selAddr 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdRAMhps_Flag 1 0
    run 200 ns
}

##############################
# Read RAM registers FPGA side 
##############################

# Read FSP050

force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 00110010 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 1 0

run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
run 1 us

# Read FSP051

force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 00110011 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 1 0

run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
run 1us


# Read FSP052

force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 00110100 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 1 0

run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
run 1us

# Read FSP053

force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 00110101 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 1 0

run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
run 1us

##############################
# Write RAM registers FPGA side 
##############################

# Write FSP050 4B 0xDEADBEEF
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 00110010 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 0 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Dt2Wr 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011011110101011011011111011101111 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
run 1us

# Write FSP051 4B 0xCAFE4DAD
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 00110011 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 0 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Dt2Wr 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011001010111111100100110110101101 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
run 1us

# Write FSP052 4B 0x1234ABCD
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 00110100 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 0 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Dt2Wr 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010010001101001010101111001101 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
run 1us

# Write FSP053 4B 0xF1CA4658
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 00110101 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 0 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Dt2Wr 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011110001110010100100011001011000 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
run 1us

##############################
# Read RAM registers HPS side 
##############################
puts "Read RAM registers HPS side"
for {set i 0} {$i < 10} {incr i} {
    set selAddr [lindex $RAMAddr $i]
    puts "Addr $selAddr"
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $selAddr 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdRAMhps_Flag 1 0
    run 200 ns
}

##############################
# Read JTAG UART registers  0x0= data; 0x1= control
##############################
puts "Read RAM registers HPS side"
for {set i 0} {$i < 2} {incr i} {
    set selAddr [lindex $RAMAddr $i]
    puts "Addr $selAddr"
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $selAddr 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdJTAG_Flag 1 0

    run 200 ns
}

##############################
# Set JTAG UART Rd interrupt enable  
##############################
puts "Set the read interrupt enable"

force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 1 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdJTAG_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns

##############################
# Read JTAG UART registers  0x0= data; 0x1= control
##############################
puts "Read RAM registers HPS side"
for {set i 0} {$i < 2} {incr i} {
    set selAddr [lindex $RAMAddr $i]
    puts "Addr $selAddr"
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $selAddr 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdJTAG_Flag 1 0

    run 200 ns
}

##############################
# Set JTAG UART Wr interrupt enable  
##############################
puts "Set the read interrupt enable"

force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 1 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 3 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdJTAG_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns

##############################
# Read JTAG UART registers  0x0= data; 0x1= control
##############################
puts "Read RAM registers HPS side"
for {set i 0} {$i < 2} {incr i} {
    set selAddr [lindex $RAMAddr $i]
    puts "Addr $selAddr"
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $selAddr 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdJTAG_Flag 1 0

    run 200 ns
}

##############################
# Write JTAG UART Wr FIFO data  
# Note: this test case maybe needs the HW JTAG interface in order to be performed:
#       I am able to read the registers and to set the rd/wr interrupt enable bits (@ 0x1 address), 
#       but as soon as I write the register 0 with 8 bit for the write FIFO, these are ignored.
#       I don't see neither the number of words in the write FIFO changing (reading the reg 0x1)
#       Another think to take into consideration is that as soon as I enable the interrupt for the write,
#       the pending interrupt for the "write FIFO almost empty" is set to one. This gives me the feeling
#       that the ip is not under reset. 
##############################
for {set j 0} {$j < 64} {incr j} {
  puts "Write data $j to the Write FIFO"
  force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 0 0
  force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM $j 0
  force -deposit sim:/acu_hps_qsys_tb_tb/RdJTAG_Flag 1 0
  force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
  run 500 ns
  
  ##############################
  # Read JTAG UART registers  0x0= data; 0x1= control
  ##############################
  puts "Read RAM registers HPS side"
  for {set i 0} {$i < 2} {incr i} {
    set selAddr [lindex $RAMAddr $i]
    puts "Addr $selAddr"
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $selAddr 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdJTAG_Flag 1 0

    run 200 ns
  }

}
##############################
# Read IRQ Latency Counter registers
##############################
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 32 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdIRQ_LC_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns


set IRQ_LCAddr [list 0 1 32 33 34 35 36]
for {set i 0} {$i < 7} {incr i} {
    set selAddr [lindex $IRQ_LCAddr $i]
    puts "Addr $selAddr"
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $selAddr 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdIRQ_LC_Flag 1 0
    run 200 ns
}

##############################
# Read FIFO status registers HPS SIDE
# 0x0 => fill level
# 0x1 => status: |underflow|overflow|almost empty|almost full|empty|full|
# 0x2 => event: |underflow|overflow|almost empty|almost full|empty|full|
# 0x3 => interrupt enable:|underflow|overflow|almost empty|almost full|empty|full|
# 0x4 => almost full threshold (by default= fifo size-4)
# 0x5 => almost empty threshold (by default= 1)
##############################
for {set i 0} {$i < 6} {incr i} {
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $i 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
    run 200 ns
}
# Clear Event register
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 2 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 63 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns

# Set almost full threshold to 32
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 4 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 32 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns


# Set almost empty threshold to 4
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 5 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 4 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns


# Enable all interrupts
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 3 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 63 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns


##############################
# Read FIFO status registers HPS SIDE
##############################
for {set i 0} {$i < 6} {incr i} {
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $i 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
    run 200 ns
}

# Clear Event register
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 2 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 63 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns

##############################
# Read FIFO data HPS SIDE
##############################
force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM 0 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFO_hps_Flag 1 0
run 200 ns

##############################
# Read FIFO status registers HPS SIDE
##############################
for {set i 0} {$i < 6} {incr i} {
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $i 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
    run 200 ns
}

# Clear Event register
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 2 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 63 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns

##############################
# Read FIFO status registers FPGA SIDE
# 0x0 => fill level
# 0x1 => status: |underflow|overflow|almost empty|almost full|empty|full|
# 0x2 => event: |underflow|overflow|almost empty|almost full|empty|full|
# 0x3 => interrupt enable:|underflow|overflow|almost empty|almost full|empty|full|
# 0x4 => almost full threshold (by default= fifo size-4)
# 0x5 => almost empty threshold (by default= 1)
##############################
for {set i 0} {$i < 6} {incr i} {
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $i 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_fpga_Flag 1 0
    run 200 ns
}

# Clear Event register
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 2 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 63 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_fpga_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns

# Set almost full threshold to 32
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 4 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 32 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_fpga_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns


# Set almost empty threshold to 4
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 5 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 4 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_fpga_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns


# Enable all interrupts
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 3 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 63 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_fpga_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns

##############################
# Read FIFO status registers FPGA SIDE
##############################
for {set i 0} {$i < 6} {incr i} {
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $i 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_fpga_Flag 1 0
    run 200 ns
}

# Clear Event register
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 2 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 63 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_fpga_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns

##############################
# Write FIFO data FPGA SIDE
##############################
# Fetch data from USI == Write FSP100
# Write FSP100 256B 0xCAFE...1234
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 01100100 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 0 0
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Dt2Wr 11001010111111100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001001000110100 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
run 100 ns
force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
run 30 us

# Fetch data from FPGA sources
proc dec2bin {dec} {
    binary scan [binary format I $dec] B* bin
    return $bin
}

force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_DtFtchdEn 1 0
run 100 ns
# 64b counter (0 to 63) incremented every 2us used to emulate FPGA_DataFetched (maybe a vhdl module will be easier)

for {set i 0} {$i < 16} {incr i} {
  set dt2Wr [concat [dec2bin [expr $i+1]][dec2bin $i]]
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_DataFetched $dt2Wr 0
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_NewDtFtchd 1 0
  run 10 ns
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_NewDtFtchd 0 0  
  run 1900 ns
}

for {set i 16} {$i < 32} {incr i} {
  set dt2Wr [concat [dec2bin [expr $i+1]][dec2bin $i]]
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_DataFetched $dt2Wr 0
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_NewDtFtchd 1 0
  run 10 ns
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_NewDtFtchd 0 0  
  run 1900 ns
}

for {set i 32} {$i < 48} {incr i} {
  set dt2Wr [concat [dec2bin [expr $i+1]][dec2bin $i]]
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_DataFetched $dt2Wr 0
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_NewDtFtchd 1 0
  run 10 ns
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_NewDtFtchd 0 0  
  run 1900 ns
}

for {set i 48} {$i < 64} {incr i} {
  set dt2Wr [concat [dec2bin [expr $i+1]][dec2bin $i]]
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_DataFetched $dt2Wr 0
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_NewDtFtchd 1 0
  run 10 ns
  force -freeze sim:/acu_hps_qsys_tb_tb/FPGA_NewDtFtchd 0 0  
  run 1900 ns
}

##############################
# Read FIFO status registers FPGA SIDE
##############################
for {set i 0} {$i < 6} {incr i} {
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM $i 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_fpga_Flag 1 0
    run 200 ns
}

# Clear Event register
force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 2 0
force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 63 0
force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_fpga_Flag 1 0
force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
run 500 ns

##############################
# Read FIFO data HPS SIDE
##############################
for {set i 0} {$i < 8} {incr i} {
    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM 0 0
    force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFO_hps_Flag 1 0
    run 200 ns
}

###########################################
# FW Update emulation
#	RAM location description:
#		0x0 USI2HPS command location => FPGA2HPS data transfer commands and configurations:
#               [31..28] File type (rbf,txt,...)
#               [27..26] Unused
#               [25]     Ready to send data from FPGA 2 HPS (through FIFO)????? maybe not necessary ????
#               [24]     Generate a file
#               [23..0]  File size
#              0x1 HPS2USI Status info location => FPGA2HPS data transfer status:
#               [31..3]  Unused
#               [2]      File saved
#               [1]      Data received
#               [0]      Ready to receive data
#              0x2 USI2HPS command location => FPGA reconfiguration command:
#               [31..2]  Unused
#               [1]      Reconfigure FPGA with Factory image (Debug only!!)
#               [0]      Reconfigure FPGA
#              0x3 HPS2USI Status info location => FPGA reconfiguration status:
#               [31..4]  Unused
#               [3..0]   FPGA configuration status
#              0x4 => USI2HPS Data transfer => It is equivalent to a "SerialFSPOut". The file chunk size is defined by gMaxFSPDepth. 
###########################################
	# HPS polls RAM address 0 and 2


	for {set i 0} {$i < 4} {incr i} {
    		force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM 0 0
    		force -deposit sim:/acu_hps_qsys_tb_tb/RdRAMhps_Flag 1 0
    		run 200 ns
    		force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM 2 0
    		force -deposit sim:/acu_hps_qsys_tb_tb/RdRAMhps_Flag 1 0
    		run 200 ns
	}

	# USI writes that a rbf file of 512 B has to be generated

	# Write FSP050 4B 0xDEADBEEF
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 00110010 0
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 0 0
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Dt2Wr 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000000001000000000 0
	run 100 ns
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
	run 100 ns
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
	run 1us

	# HPS stops the polling as soon as 0x0 bit 24 is detected as 1
    	force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM 2 0
    	force -deposit sim:/acu_hps_qsys_tb_tb/RdRAMhps_Flag 1 0
    	run 200 ns
	
    	force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM 0 0
    	force -deposit sim:/acu_hps_qsys_tb_tb/RdRAMhps_Flag 1 0
    	run 200 ns

	# The HPS checks the FIFO status: if it is not empty, it will read the FIFO till it is and after that sets the register 0x1 bit 0	
	# Emulating the not empty condition with 8 symbols still to read
	##############################
        # Read FIFO status registers HPS SIDE
        ##############################
        
	force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM 1 0
        force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
        run 200 ns
	
	for {set i 0} {$i < 8} {incr i} {
	    force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM 0 0
	    force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFO_hps_Flag 1 0
	    run 200 ns
	}	
	
	force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM 1 0
        force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag 1 0
        run 200 ns

        force -freeze sim:/acu_hps_qsys_tb_tb/AddrBFM 1 0
        force -freeze sim:/acu_hps_qsys_tb_tb/WrDataBFM 1 0
        force -deposit sim:/acu_hps_qsys_tb_tb/RdRAMhps_Flag 1 0
        force -deposit sim:/acu_hps_qsys_tb_tb/EnWr 1 0
        run 500 ns
        
        # The USI polls in the meantime the register 0x1 and as soon as detects a one in the bit 0, it starts to send the rbf file through register 0x4
        # Read FSP051

	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 00110011 0
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 1 0

	run 100 ns
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
	run 100 ns
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
	run 1us

	##############################
	# Write FIFO data FPGA SIDE
	##############################
	# Fetch data from USI == Write FSP100
	# Write FSP100 256B 0xCAFE...1234
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSPnr 01100100 0
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_RdWrn 0 0
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Dt2Wr 11001010111111100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001001000110100 0
	run 100 ns
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 1 0
	run 100 ns
	force -freeze sim:/acu_hps_qsys_tb_tb/mUSIC_Emul_FSP_Start 0 0
	#run 30 us
    	##############################
	# Read FIFO data HPS SIDE
	##############################
	for {set i 0} {$i < 236} {incr i} {	
		force -freeze  sim:/acu_hps_qsys_tb_tb/AddrBFM 0 0
		force -deposit sim:/acu_hps_qsys_tb_tb/RdFIFO_hps_Flag 1 0
		run 118 ns
	}
	
	
	
