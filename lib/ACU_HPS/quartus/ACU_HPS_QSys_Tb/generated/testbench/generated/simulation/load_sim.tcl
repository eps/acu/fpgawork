set QSYS_SIMDIR .
source ./mentor/msim_setup.tcl

dev_com

com

elab

if { [file exists "wave.do"] == 1 } {
	do wave.do
	run 1 us
	force -freeze sim:/acu_hps_qsys_tb_tb/Reset 0 0

        run 1 us
}

