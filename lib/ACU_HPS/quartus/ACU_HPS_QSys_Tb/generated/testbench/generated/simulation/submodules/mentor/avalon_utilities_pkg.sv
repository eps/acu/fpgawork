// Copyright (C) 2020 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 20.1std.1
// ALTERA_TIMESTAMP:Thu Nov 12 05:58:26 PST 2020
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
EO3xn4p1t2YPZhH5T/DXo6GDvp9Tttg9yciQheULPw3vG9Sd+6sRhCXuwHpq9lPR
3BxvOVMwA8GAoKaQydZbxb5FXNWDVK9YgqCtv/98JslGSJ/IX1LK6AqLoS/wq9mj
T9dXF4KQ/fPp18caA9m0t9ZIIAf7Dq83RYSfP09BUZc=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2848)
Gev2lOWF8wp2jd65J/2cYFpkfaminRKbCpX+Lebq99CYKYYhUepnZzNmwxfC53jR
H5hiYO2n5poin0CJL9SSRc6XsDruLsdrvSbO7iYIQRxecwg9UyVHhasA9Vp47cka
/eOzggVtDHzsKrOymvm8V495bWhp5J4UWnTrdHWI7MUcH2n+gT2p6GJDsDmWPRts
W3/ki69Z6tGNztm/5oO6Sz9BL+ND5/6DFIMM8tYHzN6sShzq04z6CZ0yutg6I6vD
8nLIihHwRPzJ4JlrUz5ZzdciTqakA0f3vyYpvmgCXEZ+jQglNfTplDkUq9cbaYid
qyMy/sgu2yvWEa4VaVx5zl8TUzhD+sy1V9XxJH++19nxcIJ3OZNfUpFWk05gxRJ/
qsKSlU5SYg9VB2J4gPf5c1dO5cWE9BD9YkpKerSg/SeaL4zb93WcDmsjGbYOHPFk
/Zq0c1Vko0rmXj58u+svtWKeZZPcol1IhYWibPMHPfnXPWcd+b5lY0hPkeNjwEzX
OFb47j/3tJt9pfdkv5AEDi2VVG0UVkH9nS/RMCfdpzyHSCn4o+5r9Qm4mvYi/ofT
htCbY6CMZWcB39P75GvwdgH/ovLl0fyMpptdrzyd0rgTeUHjEIdo0znzfy5HrPXl
Hpou8Ldi6N1ql47zdNThhP95n+zCKA6+WwJP4SDV4zWXp5lblHk3YepBQReVxXvv
dVTIL4R4fTNKSMuC4YaoXZT5/d8GB8AgvOlZs3uWks5/qEciTxm9wEWi3ZMjgsFy
2QzucZ3n4KiyEAF8NwbNRIu0UW26F2zZpUcoKNyy8hbFkK7Lkv84hVgsFTttovIj
CuKFq60gx61JnZZ/TTZ0ltgIPcFOV9yAzEDeDr5DOkLlGp2rqYfPIyzRoqYMhtxW
R/xubew2bQVZtcPFHP+S0clSCbkrRrsf8jZRuLZ9QvXnpSV7FeJa/9I09e2b5J/c
wNZx+Jw/XFOg2vYT3mEqsNLECJwD9+5pOxvCAsONYd9AdufWYJo7jE4bHAAHuVCE
66ouygkOpDn0Y6RxlGqDIiATYynXEi9yTabknO1mb6X38HlHsjwh+DcZxHm3NjfM
Y1vOGj06ostGgH9XboNwnLV8NDhk9rdHehir1dExnvMIID+zji1KvvyGu/62mqCy
NT1MsvI1mremsd2Ulnatr7yxicdYiSqUQfkuHR77GQvMGdV9/JREKQ3aa3q3jeLj
U15aKHpU8jLWvjjtAlseD2f1lJ5DaNyHP6paVkhU2i910JqszwJFdTyp2OkZDqY+
r2mlP/trXMC/kO5PcPOD1e9gQZI9UiAof6tnJmQLgJWcTA9bA//Lo/ao8v4dwL/u
c23jqTovECCHGodBvPNE724eCapsBaIA915ivMwKLSh8NJHRfq/Fk4qfs/VtZ9eE
rngwt3vk9Rr+EZ+os+xUJyOMgx5wqnkYJp/nvtEDTUmK+w/73T/sQUP5K31E0/XZ
2YQ6jFmtnA/TKZOl8cVh3u/Y3pa1/P+WVGA90AxWZH5L1uRGFWitoR19DzGGx6Oa
nIwonz4pbutImNixm74l61yDvYcxQOMsKqJ818HyxgvbP7LJzJui6OpqDJDg8rSa
Tl1edkXqgNCtXLvlwGfQkFfkMTQYCfS7+Qn+QnUE/gYGv0ZN5GyfsOQjLu4taQ20
YW/4kTmMVWtnvMbSWQm9gO7UKA1w6dVh72u+hhXkpbGea616JD+WjklhQG4rx4Uv
oWCKk2j+fACKzc5a40TJ1nxUBU/jKqifnqqrbYb6UoxYymeHbc3OZEutuQEJ9U7K
zXQiYmFfOe/6yYvs6xjtw41UHjA40P96kJBk8UJJlQCxvIei3aNiUN8QILcBx6MB
GxnN8XeMjati1D1jy58iCuArQmVQIEDUkF8UtNP04rOQl0nvAwdE+8MzNpkgJo0Q
7ZFqbTJEabvwImFpYe7ngQyJNr8X33JM5NYsOEimXerUDdhNd1oYmxDA3KXUKboN
mcKEVXPDsT5AIoEitap1LisDpcbyjeuvKeuBKdCweyi9kb3wXM6vmTa76u9F0btD
fE4MRmZuG8uwqcJ+IefLzUk9uSD78ooSkvDMfcQ7tehnha8kRsXhoSMLh5PoJb9t
/8OJ1X4thGwmutm0akEOft4NK4R2+BEhOa70DtDPvazRvtqBolu8pFoCr3yOW6Vr
mgSaehScnSSbeintG94upxBpesZmBjz7cu/rUyGCV8j3x6wUZKCqaK+9fdLtrjCg
S8cw70nYrh/0UwCq2gfuY6ZtvCDa9yKFSXDhI1MuSpLR0dz9rf+oalNVIAoQPCIp
t3mlMq6JmEKo47sbr+kd0/n4vcLulPPr++Z2zz1XhBXY90OmuSXxfXhnk7LSvnVL
V5OR1IkZEbC97PC33elmIhpx4PZuWN+DphpOHs9C/+VLLYkfoUG3IdfltG8Q5rqd
d8mVRy4wP7oa81J5hu3aRymCWWN06MT/Ru+jP0hCVkZvpkvWh2Gtnt7qBEA4Hznq
kEj/ECgbB82DDjBhoOxx5yJgKMBvwkc2i81xXDw98f0M4o3u4SZip/lqkNoSYetG
QpBAwpccywI+yK8P5ftAyNDYYdSvbEIcEWgeCjrLDpmp0eGRFrQikyWL8It9ptj/
uJiswbh/oPjUyoRQE9PMKGuNoYCnvtExpwTeyywEh4HZyJVgEn08PLrPij8SLrgR
6qTLjAFwYBQs1uHSI1jclx/0UfVXA+NmdyGwRKD7HtPIPyJEqcZtROf9ja3JlSFT
bTJx8oDsl3AbF7aa8T3mamrBNbvLxLQrwZvtJobRhpMWqpQ/onmt/YGlqEc9elEp
bn6WXjp12PkxsBs9gcm7ZsQB20Q0mVVNXW+p1YXHuvu36xI2pB0w9UlagnHo9rER
fzzNHT6uTUtyoy70gSuuE9BpQNSwZ/uGUtu9b1ky93Ie0Wch4hzdGpEBtfdkkPyO
CIHdRAM5XK54w0ZdyE5lZdcoMgxZnL0WOsGSBbP/jeq1DKDq2tb06+5AY9L11e5u
7//WLw7eTT7teRIq5C3fpt0nZv71lEmgsbXsYywI7rpjW5TFzF21Pe/pCv5Ec0C4
6uw29WBYTqT3H5XrB4Oe9raIb5hGtitluy65SdZ8HIMredzv1VxeooLtfAKjJAOK
/IcFp4jzUWinAS4OEGsjvhU1JNaRwo9o2YdX9BRQoh9EJSiS5SXkDXukmI1tbGfo
1e62mESGbcmgbkxl/wPkaeAwKfXZPucBENdV1yrLxS8PrlQmexQ/BaFrl2I7G4RP
rzWA5KUHpPkL/XHPFhFFVorbq2j7ztLVHX12xkHFdQZpANU9YR7bB+XVNlmhZfc4
CqV3fg7MtSoNNStGrT8hxTGwqHsnCFnIDTm29KZnktEd+C/LuBUGxYx231xaCL1+
XEyfJ0i8dRrs6u88RLDPXkSRdkYzGawrJbubgcrB3yO5V3gf/hrsL6oZkWzY5SBf
fMQOweaos9fFlo9V2NCyzxti+0+U0BBQiCBgJA8U7kdklLNawQyYAUu8gTM0FsP7
jE76sG4lCVVY3CJJoMXaoYUjgadDyD8BCWqD85Ej1We0QyxB2h4+FkMhj0b1dS1+
0SfxDvaLLoUv4MOh/JAhfEaHzzyHdxTzd/46o1RTDoihYYH4FcAL/K/zyr4/z+Je
usVHWReElW2xkp9HzvWbVnb/bMwWeEa1ZwZcXfKwFuO7NwzUEiFWxZAYFok46kBj
tIi77/RmZ1JKpI3x8tbU0oIZgH72wjwRwUCwa6D/oLsJ8cfNomifytO/yJcTSQd5
CCCIIzStAkPhbRPe80Oi6A==
`pragma protect end_protected
