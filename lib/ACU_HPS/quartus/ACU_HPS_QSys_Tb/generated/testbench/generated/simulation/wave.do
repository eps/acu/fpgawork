onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Tb
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/RdSysID_Flag
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/RdPIO_LED_Flag
add wave -noupdate /acu_hps_qsys_tb_tb/RdRAMhps_Flag
add wave -noupdate /acu_hps_qsys_tb_tb/RdJTAG_Flag
add wave -noupdate /acu_hps_qsys_tb_tb/RdIRQ_LC_Flag
add wave -noupdate /acu_hps_qsys_tb_tb/RdFIFOst_hps_Flag
add wave -noupdate /acu_hps_qsys_tb_tb/RdFIFOst_fpga_Flag
add wave -noupdate /acu_hps_qsys_tb_tb/RdirqFIFO_fpga_Flag
add wave -noupdate /acu_hps_qsys_tb_tb/RdFIFO_hps_Flag
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/EnWr
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/AddrBFM
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/WrDataBFM
add wave -noupdate -divider mUSIC_Emul
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/Clock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/Reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSPnr
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_RdWrn
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_Dt2Wr
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/RemainingBytes
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_Start
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_NewDtRead
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_DtRead
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/Address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/DataEn
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/RWn
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/Data
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_Start_s0
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_Start_s1
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_Start_RE
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_Start_RE_s0
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_Start_RE_s1
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/FSP_Start_RE_s2
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/tmpCntByte
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/cntTimeBetween2DataEn
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_mUSIC_Emulator/tmpDt2Wr
add wave -noupdate -divider {USI bridge}
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/Clock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/Reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/Address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/DataEn
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/RWn
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/Data
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/RemainingBytes
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/Available
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/ReadOnly
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/RAM_Addr
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/RAM_WrCmd
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/RAM_WrData
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/RAM_RdData
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/FIFO_WrCmd
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/FIFO_WrData
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/FPGA_DtFtchdEn
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/FPGA_NewDtFtchd
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/FPGA_DataFetched
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/Address_s0
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/Address_s1
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/RWn_s0
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/cntGeneric
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/Tc_cntGeneric
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/dt2WrRAM
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/addrRAM
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/DataEn_s0
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/DataEn_s1
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/DataEn_RE
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/foundFlag
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/RAM_DtOut
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/Avail
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/tmpReg
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/FPGA_NewDtFtchd_s0
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/SplitFlag
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/i_ACU_AvalonMMM_USI_Bridge/fsm_state
add wave -noupdate -divider PlatformDesign
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/clk_clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_irq_fpga_irq
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_reset_fpga_reset_n
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_s1_fpga_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_s1_fpga_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_s2_hps_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_s2_hps_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_st_fpga_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_st_fpga_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_st_fpga_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_st_fpga_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_st_fpga_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_st_hps_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_st_hps_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_st_hps_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_st_hps_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_st_hps_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/fifo_mm_sysclk_fpga_clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/irq_lc_s2_hps_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/irq_lc_s2_hps_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/irq_lc_s2_hps_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/irq_lc_s2_hps_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/irq_lc_s2_hps_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/jtag_uart_s2_hps_chipselect
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/jtag_uart_s2_hps_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/jtag_uart_s2_hps_read_n
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/jtag_uart_s2_hps_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/jtag_uart_s2_hps_write_n
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/jtag_uart_s2_hps_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/jtag_uart_s2_hps_waitrequest
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_reset_fpga_reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_reset_fpga_reset_req
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s1_fpga_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s1_fpga_clken
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s1_fpga_chipselect
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s1_fpga_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s1_fpga_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s1_fpga_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s1_fpga_byteenable
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s2_hps_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s2_hps_chipselect
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s2_hps_clken
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s2_hps_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s2_hps_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s2_hps_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_s2_hps_byteenable
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/onchip_ram_sysclk_fpga_clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/pio_led_out_export
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/pio_led_s2_hps_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/pio_led_s2_hps_write_n
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/pio_led_s2_hps_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/pio_led_s2_hps_chipselect
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/pio_led_s2_hps_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/reset_reset_n
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/sysid_s2_hps_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/sysid_s2_hps_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/irq_mapper_receiver0_irq
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/irq_mapper_receiver1_irq
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/interrupt_latency_counter_0_irq_irq
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/rst_controller_reset_out_reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/rst_controller_reset_out_reset_req
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/reset_reset_n_ports_inv
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst/rst_controller_reset_out_reset_ports_inv
add wave -noupdate -divider {FIFO irq FPGA driver}
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm/clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm/reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm/irq
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm/req
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm/ack
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm/data_out0
add wave -noupdate -divider {FIFO HPS driver}
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_arbiterlock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_beginbursttransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_begintransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_burstcount
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_byteenable
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_clken
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_debugaccess
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_lock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_readdatavalid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_readid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_readresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_response
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_transactionid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_waitrequest
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_writeid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_writeresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm/avm_writeresponsevalid
add wave -noupdate -divider {FIFO Status FPGA Driver}
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_arbiterlock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_beginbursttransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_begintransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_burstcount
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_byteenable
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_clken
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_debugaccess
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_lock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_readdatavalid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_readid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_readresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_response
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_transactionid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_waitrequest
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_writeid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_writeresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm/avm_writeresponsevalid
add wave -noupdate -divider {FIFO Status HPS driver}
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_arbiterlock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_beginbursttransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_begintransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_burstcount
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_byteenable
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_clken
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_debugaccess
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_lock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_readdatavalid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_readid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_readresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_response
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_transactionid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_waitrequest
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_writeid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_writeresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_fifo_mm_st_hps_bfm/avm_writeresponsevalid
add wave -noupdate -divider {irq LC driver}
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_arbiterlock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_beginbursttransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_begintransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_burstcount
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_byteenable
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_clken
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_debugaccess
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_lock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_readdatavalid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_readid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_readresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_response
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_transactionid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_waitrequest
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_writeid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_writeresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm/avm_writeresponsevalid
add wave -noupdate -divider {JTAG UART Driver}
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_waitrequest
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_arbiterlock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_beginbursttransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_begintransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_burstcount
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_byteenable
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_clken
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_debugaccess
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_lock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_readdatavalid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_readid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_readresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_response
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_transactionid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_writeid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_writeresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm/avm_writeresponsevalid
add wave -noupdate -divider {RAM HPS driver}
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_byteenable
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_clken
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_arbiterlock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_beginbursttransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_begintransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_burstcount
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_debugaccess
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_lock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_readdatavalid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_readid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_readresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_response
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_transactionid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_waitrequest
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_writeid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_writeresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_onchip_ram_s2_hps_bfm/avm_writeresponsevalid
add wave -noupdate -divider {LED Driver}
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_waitrequest
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_arbiterlock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_beginbursttransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_begintransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_burstcount
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_byteenable
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_clken
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_debugaccess
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_lock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_readdatavalid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_readid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_readresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_response
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_transactionid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_writeid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_writeresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm/avm_writeresponsevalid
add wave -noupdate -divider {SysID driver}
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/clk
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/reset
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_address
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_readdata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_waitrequest
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_read
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_arbiterlock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_beginbursttransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_begintransfer
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_burstcount
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_byteenable
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_clken
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_debugaccess
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_lock
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_readdatavalid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_readid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_readresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_response
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_transactionid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_write
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_writedata
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_writeid
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_writeresponse
add wave -noupdate -radix hexadecimal /acu_hps_qsys_tb_tb/acu_hps_qsys_tb_inst_sysid_s2_hps_bfm/avm_writeresponsevalid
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0} {{Cursor 2} {4070000 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1999050 ps} {2000050 ps}
