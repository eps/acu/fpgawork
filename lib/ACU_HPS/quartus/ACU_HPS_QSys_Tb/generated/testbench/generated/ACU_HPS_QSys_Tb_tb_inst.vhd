	component ACU_HPS_QSys_Tb_tb is
		port (
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_address       : in  std_logic_vector(9 downto 0)  := (others => 'X'); -- address
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_clken         : in  std_logic                     := 'X';             -- clken
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_chipselect    : in  std_logic                     := 'X';             -- chipselect
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_write         : in  std_logic                     := 'X';             -- write
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_readdata      : out std_logic_vector(31 downto 0);                    -- readdata
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_writedata     : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_byteenable    : in  std_logic_vector(3 downto 0)  := (others => 'X'); -- byteenable
			acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_writedata        : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_write            : in  std_logic                     := 'X';             -- write
			acu_hps_qsys_tb_inst_onchip_ram_sysclk_fpga_clk       : in  std_logic                     := 'X';             -- clk
			acu_hps_qsys_tb_inst_fifo_mm_sysclk_fpga_clk          : in  std_logic                     := 'X';             -- clk
			acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_clock_clk   : in  std_logic                     := 'X';             -- clk
			acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_clk_clk      : in  std_logic                     := 'X';             -- clk
			acu_hps_qsys_tb_inst_fifo_mm_reset_fpga_reset_n       : in  std_logic                     := 'X';             -- reset_n
			acu_hps_qsys_tb_inst_onchip_ram_reset_fpga_reset      : in  std_logic                     := 'X';             -- reset
			acu_hps_qsys_tb_inst_onchip_ram_reset_fpga_reset_req  : in  std_logic                     := 'X';             -- reset_req
			acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_reset_reset : in  std_logic                     := 'X';             -- reset
			acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_reset_reset  : in  std_logic                     := 'X'              -- reset
		);
	end component ACU_HPS_QSys_Tb_tb;

	u0 : component ACU_HPS_QSys_Tb_tb
		port map (
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_address       => CONNECTED_TO_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_address,       --         acu_hps_qsys_tb_inst_onchip_ram_s1_fpga.address
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_clken         => CONNECTED_TO_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_clken,         --                                                .clken
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_chipselect    => CONNECTED_TO_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_chipselect,    --                                                .chipselect
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_write         => CONNECTED_TO_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_write,         --                                                .write
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_readdata      => CONNECTED_TO_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_readdata,      --                                                .readdata
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_writedata     => CONNECTED_TO_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_writedata,     --                                                .writedata
			acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_byteenable    => CONNECTED_TO_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_byteenable,    --                                                .byteenable
			acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_writedata        => CONNECTED_TO_acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_writedata,        --            acu_hps_qsys_tb_inst_fifo_mm_s1_fpga.writedata
			acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_write            => CONNECTED_TO_acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_write,            --                                                .write
			acu_hps_qsys_tb_inst_onchip_ram_sysclk_fpga_clk       => CONNECTED_TO_acu_hps_qsys_tb_inst_onchip_ram_sysclk_fpga_clk,       --     acu_hps_qsys_tb_inst_onchip_ram_sysclk_fpga.clk
			acu_hps_qsys_tb_inst_fifo_mm_sysclk_fpga_clk          => CONNECTED_TO_acu_hps_qsys_tb_inst_fifo_mm_sysclk_fpga_clk,          --        acu_hps_qsys_tb_inst_fifo_mm_sysclk_fpga.clk
			acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_clock_clk   => CONNECTED_TO_acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_clock_clk,   -- acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_clock.clk
			acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_clk_clk      => CONNECTED_TO_acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_clk_clk,      --    acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_clk.clk
			acu_hps_qsys_tb_inst_fifo_mm_reset_fpga_reset_n       => CONNECTED_TO_acu_hps_qsys_tb_inst_fifo_mm_reset_fpga_reset_n,       --         acu_hps_qsys_tb_inst_fifo_mm_reset_fpga.reset_n
			acu_hps_qsys_tb_inst_onchip_ram_reset_fpga_reset      => CONNECTED_TO_acu_hps_qsys_tb_inst_onchip_ram_reset_fpga_reset,      --      acu_hps_qsys_tb_inst_onchip_ram_reset_fpga.reset
			acu_hps_qsys_tb_inst_onchip_ram_reset_fpga_reset_req  => CONNECTED_TO_acu_hps_qsys_tb_inst_onchip_ram_reset_fpga_reset_req,  --                                                .reset_req
			acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_reset_reset => CONNECTED_TO_acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_reset_reset, -- acu_hps_qsys_tb_inst_fifo_mm_irq_fpga_bfm_reset.reset
			acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_reset_reset  => CONNECTED_TO_acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_reset_reset   --  acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm_reset.reset
		);

