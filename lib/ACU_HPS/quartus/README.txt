There are two Qsys folders:
	1) ACU_HPS_QSys
	2) ACU_HPS_QSys_Tb
	
The first includes the complete project, so it is the one going into synthesis.
The second is the exact copy of the first one without the hps instance.
It is necessary to remove the hps instance in order to perform functional simulation without any Questa Verification IP license.
