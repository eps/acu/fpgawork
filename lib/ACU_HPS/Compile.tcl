###########################################
# Compile.tcl file#
# It contains all design file to compile. #
###########################################
echo "set path"
path="../vhdl"

QSYS_TBDIR="../quartus/ACU_HPS_QSys_Tb/generated/testbench/ACU_HPS_QSys_Tb_tb/simulation"
QSYS_SIMDIR="../quartus/ACU_HPS_QSys_Tb/generated/simulation"
QUARTUS_INSTALL_DIR="/home/drodomon/intelFPGA_lite/20.1/quartus/"

echo "define and map work lib"
vlib work
vmap work work


pwd
echo "Set QSYS_SIMDIR"

echo $QSYS_SIMDIR
vcom -93 /home/drodomon/intelFPGA_lite/20.1/ip/altera/sopc_builder_ip/verification/altera_avalon_mm_master_bfm_vhdl/altera_avalon_mm_master_bfm_vhdl_pkg.vhd
vcom -93 /home/drodomon/intelFPGA_lite/20.1/ip/altera/sopc_builder_ip/verification/altera_avalon_mm_master_bfm_vhdl/altera_avalon_mm_master_bfm_vhdl.vhd

vcom -93 /home/drodomon/intelFPGA_lite/20.1/ip/altera/sopc_builder_ip/verification/altera_avalon_interrupt_sink_vhdl/altera_avalon_interrupt_sink_vhdl_pkg.vhd
vcom -93 /home/drodomon/intelFPGA_lite/20.1/ip/altera/sopc_builder_ip/verification/altera_avalon_interrupt_sink_vhdl/altera_avalon_interrupt_sink_vhdl.vhd

vlog -sv /home/drodomon/intelFPGA_lite/20.1/ip/altera/sopc_builder_ip/verification/altera_avalon_interrupt_sink/mentor/altera_avalon_interrupt_sink_vhdl_wrapper.sv
vlog -sv /home/drodomon/intelFPGA_lite/20.1/ip/altera/sopc_builder_ip/verification/altera_avalon_interrupt_sink/altera_avalon_interrupt_sink_vhdl_wrapper.sv
#vlog -sv /home/drodomon/intelFPGA_lite/20.1/ip/altera/sopc_builder_ip/verification/altera_avalon_interrupt_sink/mentor/altera_avalon_mm_master_bfm_vhdl_wrapper.sv
#vlog -sv /home/drodomon/intelFPGA_lite/20.1/ip/altera/sopc_builder_ip/verification/altera_avalon_mm_master_bfm/altera_avalon_mm_master_bfm_vhdl_wrapper.sv

vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.v"    
vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/220model.v"                        
vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.v"                         
vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.v"                 
vlog -sv "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim.sv"      
vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/cyclonev_atoms_ncrypt.v"         
vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/cyclonev_hmi_atoms_ncrypt.v"         
vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_atoms.v"                           
vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/cyclonev_hssi_atoms_ncrypt.v"    
vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_hssi_atoms.v"                  
vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/cyclonev_pcie_hip_atoms_ncrypt.v"
vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_pcie_hip_atoms.v"              

vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_syn_attributes.vhd"    
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_standard_functions.vhd"
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/alt_dspbuilder_package.vhd"   
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_europa_support_lib.vhd"
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives_components.vhd"   
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.vhd"  
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/220pack.vhd"     
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/220model.vhd"    
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate_pack.vhd"
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.vhd"     
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf_components.vhd"    
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.vhd"   

vlog -sv "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/altera_lnsim_for_vhdl.sv"

vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim_components.vhd"     

vlog "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/cyclonev_atoms_ncrypt.v"            

vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_atoms.vhd"    
vcom -93 "$QUARTUS_INSTALL_DIR/eda/sim_lib/cyclonev_components.vhd"     


vlog "$QSYS_SIMDIR/submodules/altera_reset_controller.v"                
vlog "$QSYS_SIMDIR/submodules/altera_reset_synchronizer.v"                
vlog -sv "$QSYS_SIMDIR/submodules/ACU_HPS_QSys_Tb_irq_mapper.sv"                   
vlog "$QSYS_SIMDIR/submodules/ACU_HPS_QSys_Tb_sysid_qsys_0.v"              


vcom -93 "$QSYS_TBDIR/acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_fifo_mm_s1_fpga_bfm.vhd"
vcom -93 "$QSYS_TBDIR/acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_irq_lc_s2_hps_bfm.vhd"
vcom -93 "$QSYS_TBDIR/acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_pio_led_s2_hps_bfm.vhd"
vcom -93 "$QSYS_TBDIR/acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_fifo_mm_s2_hps_bfm.vhd"
vcom -93 "$QSYS_TBDIR/acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_jtag_uart_s2_hps_bfm.vhd"
vcom -93 "$QSYS_TBDIR/acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_sysid_s2_hps_bfm.vhd"
vcom -93 "$QSYS_TBDIR/acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_fifo_mm_st_fpga_bfm.vhd"
vcom -93 "$QSYS_TBDIR/acu_hps_qsys_tb_tb_acu_hps_qsys_tb_inst_onchip_ram_s1_fpga_bfm.vhd"
#  ACU_HPS_QSys_Tb_tb.vhd

vcom -93 "$QSYS_SIMDIR/submodules/ACU_HPS_QSys_Tb_pio_LED.vhd"    
vcom -93 "$QSYS_SIMDIR/submodules/ACU_HPS_QSys_Tb_onchip_RAM.vhd" 
vcom -93 "$QSYS_SIMDIR/submodules/ACU_HPS_QSys_Tb_jtag_uart.vhd"  
vlog "$QSYS_SIMDIR/submodules/interrupt_latency_counter.v"    
vlog "$QSYS_SIMDIR/submodules/irq_detector.v"                 
vlog "$QSYS_SIMDIR/submodules/state_machine_counter.v"        

vcom -93 "$QSYS_SIMDIR/submodules/ACU_HPS_QSys_Tb_fifo_MM.vhd"    
vcom -93 "$QSYS_SIMDIR/ACU_HPS_QSys_Tb.vhd"  

vcom -93 ../../ACU_package_numericLib.vhd
vcom -93 ../../SimFolder/vhdl/mUSIC_Emulator.vhd




vcom -93 ../../ACU_AvalonMMM_USI_Bridge/vhdl/ACU_AvalonMMM_USI_Bridge.vhd

vcom -93 $path/Tb_ACU_HPS.vhd


