###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work

#vmap altera_mf work
#vmap cyclonev work




pwd
#vcom -93 ../../../../SVN/Quartus_111/lib/ACU_package_numericLib.vhd 
#vcom -93 ../../../../SVN/Quartus_111/lib/ACU_SPI/vhdl/ACU_Generic_4lines_SPI_Master.vhd

#vcom -93 ../../SFRS_FW/vhdl/ADC_Takt.vhd
#vcom -93 ../../SFRS_FW/vhdl/adc4_7606_6Ch_Serial.vhd 

vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/SimFolder/vhdl/cntFreeWheel.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/SimFolder/vhdl/cntCmdInDriven.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/SimFolder/vhdl/up_dw_cntCmdInDriven.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_InputFilter/vhdl/ram64wxb_CV.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_InputFilter/vhdl/ACU_InputFilter.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_SPI/vhdl/ACU_Generic_4lines_SPI_Master.vhd

#vcom -93 $path/ACU_ADC_AD7606_Driver.vhd
vcom -93 $path/TiefHoch_3L.vhd
vcom -93 $path/SFRS_ButtonsMngr.vhd
vcom -93 ../../ACU_AD5791_DAC_Driver/vhdl/ACU_AD5791_DAC_Driver.vhd
vcom -93 $path/Tb_SFRS_FW.vhd
