LIBRARY ieee;
USE ieee.std_logic_1164.all;
--USE ieee.std_logic_arith.all;
USE ieee.std_logic_signed.all;
use ieee.numeric_std.all;

--Versionskontrolle
--==================
--V0.1   born
--V1.0   Wechsel von generic'integer auf generic'real und auf Quartus V9.0 (13.01.10 - DS)
--V1.1   Dokumentiert und Ports neu benamt (22.04.10 - DS)
--V2.0   Vollstaendiger Umbau des Moduls, neuer Algorithmus zur Mittelwertsbildung
--       'RemoveOutliers' noch nicht aktiv  (27.04.10 - DS)
--V2.1   Richtiges Schieberegister anstelle indiziertes Array eingebaut. 'sState_MovingAverage' gestrafft durch
--       den Wegfall von 2 States. Fehlerhafte VZ-Biterweiterung im State 'SUM' korrigiert. War (15) ist (17).
--       (21.07.10 - DS)
--V2.2   NewValuePulse Korrigiert (19.06.2012 - J.Bley)
--V2.2a  Kleine Aenderungen in den Kommentaren (12.05.1 - DS)

-- Bedarf
-- ========
-- 542 Logikelemente
-- 434 Register

--             ********************************* Wichtige Information für AD7634 *************************************************
--             *                                                                                                                 *
--             * AD7634 ist eine 18 bit,670 KSPS, Differential Programmable Input Pulse SAR ADC.                                 *
--             * 5V Internal  Reference: Drift 3 ppm/Celcius (Temp. Output)                                                      *
--             * The internal reference of the ADC provides an excelllent peformance and can be used in almost appliatications.  *
--             * However the Linearity performance is guarenteed only with an external reference.                                *
--             * For the new Design of this PCB, The external reference (2.5V)is used.                                           *
--             *                                                                                                                 *
--             *******************************************************************************************************************
-- Funktion:
-- =============
-- Am Port 'DataInput' wird ein neuer Wert angelegt und mit einem 1 Takt Puls am Port 'ReadDataPulse'
-- mitgeteilt.
-- Der neue Wert kommt in ein Schieberegister (hier indiziertes Array) an die aktuelle
-- Stelle des Arrayzeigers.
-- Das Array wird allerdings nicht mit n-Werten gefuellt (n steht hierbei fuer die Anzahl von Mittelungen)
-- sondern mit n+2 Werten. D.h. soll eine 16-fach Mittelung durchgefuehrt werden, werden dafuer insg.
-- 18 Werte erfasst. Dies wegen der Funktion 'RemoveOutliers'.
-- Ist der Port 'RemoveOutliers' = '0' werden die ersten n-Werte des Array aufaddiert und anschliessend die Summe
-- durch n/4 geteilt. D.h. bei einer 16-fach Mittelung wird die Summe durch 4 geteilt. Dadurch verringert sich
-- die Bitwertigkeit. z.B. bei einer Wertigkeit von 80uV/Bit auf 20uV/Bit. (Siehe Beispiel zur Wertigkeit)
-- Ist der Port 'RemoveOutliers' = '1' werden vor der Summenbildung der groesste und kleinste Wert aus dem Array indiziert.
-- Bei der anschliessenden Summenbildung werden alle n+2 Werte des Array aufaddiert, dabei allerdings die beiden
-- indizierten Werte ausgespart. Also wird die Summe ebenfalls auf n Werten gebildet, denen aber urspruenglich n+2
-- zugrunde liegen. Anschliessend wird diese Summe wie zuvor beschrieben weiter verarbeitet.

-- Beispiel zur Wertigkeit
-- =======================
-- Annahme der ADC liefert 18 Bit und jedes Bit besitzt die Wertigkeit 80uV/Bit.
-- Durch eine 16fach Mittelung werden aus den 18 Bit 22 Bit.
-- Nun gibt es 2 Moeglichkeiten der Division des Wertes um die Wertigkeiten zu erhalten.
-- 1. Die Summe der Einzelwerte durch die Anzahl der Einzelwerte -> Summe/16 => wieder 18 Bit
--    mit einer Wertigkeit von 80uV/Bit
-- 2. Die Summe durch 4 und die Wertigkeit durch 4 -> Summe /4 => es bleiben 20 Bit
--    mit einer Wertigkeit von 20uV/Bit
-- 3. Keinerlei Division der Summe, es bleiben 22 Bit, aber deren Wertigkeit/16 =>
--    jedes Bit besitzt nun eine Wertigkeit von 5uV/Bit

--===================================================================================================================================================

ENTITY ADC_MovingAverage is
   generic
   (
      gADC_MovingAverageVersion      : real := 2.2
   );

   port
   (
      Clock          : in  std_logic ;
      Reset          : in  std_logic ;
      DataInput      : in  std_logic_vector(17 downto 0);      -- neuer ADC Wert (AD7634 - 18 bit)
      ReadDataPulse  : in  std_logic ;                         -- wenn '1' neuen ADC Wert einlesen und in Array schreiben
      RemoveOutliers : in  std_logic ;                         -- wenn '1' kleinsten und groesssten Wert aus Mittelung entfernen
      AverageMode    : in  std_logic_vector(3 downto 0);       -- Mittelwertmode (Anzahl der Mittelungen)

      Average        : out std_logic_vector(19 downto 0);      -- Mittelwert
      NewValuePulse  : out std_logic                           -- L->H Puls wenn neuer Wert an 'Average' ansteht
   );
end ADC_MovingAverage;

architecture RTL of ADC_MovingAverage is

signal   sAverage             : std_logic_vector(19 downto 0);
signal   sSum                 : std_logic_vector(21 downto 0);

constant cArrayDepth          : integer := 17;                 -- Maximale Anzahl von Arrayeintraegen (max. Mittelung + 2)

type     tArray  is array(cArrayDepth downto 0) of std_logic_vector(17 downto 0);
signal   sArray : tArray;

signal   sNewValuePulse       : std_logic;

signal   sMaxValue            : std_logic_vector(17 downto 0);             -- Max. Wert wenn 'RemoveOutliers' aktive (nur Debug Info!)
signal   sMinValue            : std_logic_vector(17 downto 0);             -- Min. Wert wenn 'RemoveOutliers' aktive (nur Debug Info!)
signal   sMaxIndex            : integer range cArrayDepth downto 0 := 0;   -- Index des Max. Wertes, wenn 'RemoveOutliers' aktiv
signal   sMinIndex            : integer range cArrayDepth downto 0 := 0;   -- Index des Min. Wertes, wenn 'RemoveOutliers' aktiv
signal   sMinMaxMarked        : std_logic;                                 -- Wenn '1' wurdeen max. und min. Werte indiziert

signal   sIndex               : integer range cArrayDepth downto 0 := 0;   -- Index fuer 'RemoveOutliers' und 'Summenbildung'
signal   sArrayIndexMax       : integer range cArrayDepth downto 0 := 0;   -- letzter Wert im Array der gefuelt werden darf, abh. von 'AverageMode'

signal   sAverageMode         : std_logic_vector(3 downto 0);
signal   sRemoveOutliers      : std_logic;

type     tState_MovingAverage is (
                                    WAIT_FOR_READ_DATA_PULSE,
                                    REMOVE_OUTLIERS,
                                    SUM,
                                    DIVIDE
                                 );

signal   sState_MovingAverage  : tState_MovingAverage;

type     tState_Adder is ( ADD, INC_INDEX );
signal   sState_Adder  : tState_Adder;

type     tState_FindOutliers is (COMPARE,INC_INDEX);
signal   sState_FindOutliers  : tState_FindOutliers;



begin

--===================================================================================================================================================

   pADC_MovingAverage : process (Clock, Reset)

   begin

      if (Reset = '1') then

         for i in 0 to cArrayDepth loop                                      -- Array loeschen
            sArray(i)      <= (others =>'0');
         end loop;

         sAverage                   <= (others =>'0');
         sSum                       <= (others =>'0');
         sIndex                     <= 0 ;
         sNewValuePulse             <= '0';
         sMaxValue                  <= (others =>'0');
         sMinValue                  <= (others =>'0');
         sMaxIndex                  <= 0;
         sMinIndex                  <= 0;
         sMinMaxMarked              <= '0';
         sRemoveOutliers            <= '0';
         sAverageMode               <= (others =>'0');
         sState_MovingAverage       <= WAIT_FOR_READ_DATA_PULSE;
         sState_Adder               <= ADD;
         sState_FindOutliers        <= COMPARE;

      else

         if (Clock'event and Clock = '1') then

            sRemoveOutliers   <= RemoveOutliers;
            sAverageMode      <= AverageMode;

            --sAverageMode = 0 = Mittelung AUS
            --sAverageMode = 1 = Mittelung 2x
            --sAverageMode = 2 = Mittelung 4x
            --sAverageMode = 3 = Mittelung 8x
            --sAverageMode = 4 = Mittelung 16x

            case (sState_MovingAverage) is

               -- warten das 'ReadDataPulse' = '1' wird und den neuen an 'DataInput' anliegenden
               -- Wert in an die aktuelle Stelle im Array(n) lesen.
               -- Alle signale fuer die ggf. Ermittelung von Max. und Min. Wert ruecksetzen

             --============================================================================
               when WAIT_FOR_READ_DATA_PULSE =>
             --============================================================================

                  sNewValuePulse    <= '0';

                  if (ReadDataPulse = '0') then

                     for i in 0 to cArrayDepth loop                        -- Array halten
                        sArray(i)      <= sArray(i);
                     end loop;

                  else

                     sSum                    <= (others =>'0');            -- diverse Siganle initialisieren
                     sIndex                  <= 0;
                     sMaxValue               <= DataInput;
                     sMinValue               <= DataInput;
                     sMaxIndex               <= 0;
                     sMinIndex               <= 0;
                     sMinMaxMarked           <= '0';

                     sArray(0)   <= DataInput ;                              -- neuen Wert ins Schieberegister

                     for i in 1 to cArrayDepth  loop                       -- Schieberegister-Daten schieben
                        sArray(i)<= sArray(i-1);
                     end loop;

                     case (sAverageMode) is                                -- max. Anzahl n von Array(n) Werten festlegen, abh. von 'AverageMode'
                        when X"4"   => sArrayIndexMax <= cArrayDepth;                  -- 16 + 2 (0..17) Werte
                        when X"3"   => sArrayIndexMax <= ((cArrayDepth - 1 )/2) + 1;   -- 8 + 2 (0..9) Werte
                        when X"2"   => sArrayIndexMax <=  ((cArrayDepth - 1 )/4) + 1;  -- 4 + 2 (0..5) Werte
                        when X"1"   => sArrayIndexMax <=  ((cArrayDepth - 1 )/8) + 1;  -- 2 + 2 (0..3) Werte
                        when X"0"   => sArrayIndexMax <=  0;                           -- keine Mittelung
                        when others => null;
                     end case;

                     if ( (sRemoveOutliers = '1') AND (sAverageMode /= X"0") ) then   -- entscheiden ob max., min. Werte aus Array bei Summierung entfernt werden sollen oder nicht
                        sState_MovingAverage   <= REMOVE_OUTLIERS;
                     else
                        sState_MovingAverage   <= SUM;
                     end if;

                  end if;

               -- sollen die min. und max. Ausreisser entfernt werden wird dieser State aufgerufen
               -- dabei werden alle zur Mittelung herangezogene Werte (bei n-facher Mittelung -> n+2 Werte)
               -- vergliechen und der groesste und kleinste markiert. Bei der
               -- anschliessenden Summierung werden diese beiden Werte nicht aufaddiert

             --============================================================================
               when REMOVE_OUTLIERS =>
             --============================================================================

                  case (sState_FindOutliers) is

                     ---------------------------------------------------------------------
                     when COMPARE =>

                        if (sArray(sIndex) > sMaxValue) then
                           sMaxValue   <= sArray(sIndex);          -- groessten Wert sichern
                           sMaxIndex   <= sIndex;
                        end if;

                        if (sArray(sIndex) < sMinValue) then
                           sMinValue   <= sArray(sIndex);          -- kleinsten Wert sichern
                           sMinIndex   <= sIndex;
                        end if;

                        sState_FindOutliers <=INC_INDEX;

                     ---------------------------------------------------------------------
                     when INC_INDEX =>

                        if (sIndex < sArrayIndexMax) then         -- noch nicht alle n+2 Werte verglichen
                           sIndex               <= sIndex + 1;
                           sMinMaxMarked        <= '0';
                           sState_MovingAverage <= REMOVE_OUTLIERS;
                        else
                           sIndex               <= 0;
                           sMinMaxMarked        <= '1';
                           sState_MovingAverage <= SUM;
                        end if;

                        sState_FindOutliers  <= COMPARE;

                     when others =>

                        sState_FindOutliers  <= COMPARE;

                  end case;

               -- jetzt werden alle Werte aufaddiert
               -- ist 'sRemoveOutliers' = '1' werden die Werte von 'sMinIndex' und 'sMaxIndex' dabei ausgespart
               -- ist 'sRemoveOutliers' = '0' werdn nur n Werte aufaddiert (bei z.B. 16-facher Mittelung
               -- werden nicht die 18 im Array befindlichen Werte, sondern nur die ersten 16 Werte addiert)

             --============================================================================
               when  SUM =>
             --============================================================================

                  case (sState_Adder) is

                     ---------------------------------------------------------------------
                     when ADD =>

                        if ( (sMinMaxMarked = '1') AND ((sIndex = sMinIndex) OR (sIndex = sMaxIndex)) ) then   -- diesen Wert nicht addieren
                           sState_Adder   <= INC_INDEX;
                        else
                           sSum           <= sSum + (sArray(sIndex)(17)&sArray(sIndex)(17)&sArray(sIndex)(17)&sArray(sIndex)(17)&sArray(sIndex));
                           sState_Adder   <= INC_INDEX;
                        end if;

                     ---------------------------------------------------------------------
                     when INC_INDEX =>

                        if (sMinMaxMarked = '1') then          -- RemoveOutliers ist aktiv, d.h. Index laeuft von 0 bis n + 2
                           if (sIndex < sArrayIndexMax) then
                              sIndex               <= sIndex + 1;
                              sState_MovingAverage <= SUM;
                           else
                              sIndex               <= 0;
                              sState_MovingAverage <= DIVIDE;
                           end if;
                        else                                   -- RemoveOutliers ist NICHT aktiv, d.h. Index laeuft von 0 bis n
                           if (sIndex < sArrayIndexMax - 2) then
                              sIndex               <= sIndex + 1;
                              sState_MovingAverage <= SUM;
                           else
                              sIndex               <= 0;
                              sState_MovingAverage <= DIVIDE;
                           end if;
                        end if;

                        sState_Adder   <= ADD;

                     ---------------------------------------------------------------------
                     when others =>

                        sState_Adder   <= ADD;

                  end case;

               --============================================================================
               when DIVIDE =>                                  -- Division der Summe, Bitwertigkeiten festlegen
               --============================================================================
                  case (sAverageMode) is
                     when X"4"   => sAverage <= sSum(21 downto 2);
                     when X"3"   => sAverage <= sSum(20 downto 1);
                     when X"2"   => sAverage <= sSum(19 downto 0);
                     when X"1"   => sAverage <= sSum(18 downto 0) & "0";
                     when X"0"   => sAverage <= sSum(17 downto 0) & "0" & "0";
                     when others => null;
                  end case;

                  sNewValuePulse       <= '1';                    -- NewValuePulse
                  sState_MovingAverage <= WAIT_FOR_READ_DATA_PULSE;

               when others =>
                  sState_MovingAverage <= WAIT_FOR_READ_DATA_PULSE;

            end case;   --case (sState_MovingAverage) is

         end if;  --if (Clock'event and Clock = '1') then

      end if;  --if (Reset = '1') then

   end process pADC_MovingAverage;

   Average        <= sAverage ;
   NewValuePulse  <= sNewValuePulse;

end architecture RTL;
