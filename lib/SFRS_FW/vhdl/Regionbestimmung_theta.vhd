-- Inhalt:          Bestimmung des Regions und Sektors (1..6) von Uab
--                  (zusaetzlich werden bestimmt: Zone (1..6) und Sektor (1..6) von Uab
--                 Im Unterschied zur vorangegangenen Berechnungsmethode wird hier Betrag und Winkeltheta als
--                 Eingangsgroessen benutzt. Das fuert zur einer immer symetrischer Aufteilung des Regions
------------------------------------------------------------------------------
-- Author:           Wiest Alexander
-- Letzte Aenderung: 10.05.202020
--============================================================================
LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY Regionbestimmung_theta IS
   GENERIC(
      --Initialisierungswert 50 MHz:
      takt           	: integer  := 100_000_000  );

   PORT(
      -- Eingaenge:
      clk, reset,
      Enable  				: in std_logic;
	   -- Raumzeigerkomponente
      Urzmax         	: in unsigned (15 downto 0); -- maximaler Wert des Raumzeigers
      Uab_Betrag        : in unsigned (15 downto 0); -- Betrag von alpha - beta Spannung der PWM
      Theta             : in unsigned (17 downto 0); -- Winkel Theta 0..360°

      --Ausgaenge:
      Zone,
      Region, Sektor    : out unsigned (2 downto 0);
      ThetaSektor  		: out unsigned (15 downto 0);
      Data_Out_ValidReg : out std_logic);

end Regionbestimmung_theta ;

--============================================================================
ARCHITECTURE behavioral OF Regionbestimmung_theta IS
   CONSTANT C_300deg 	    : unsigned (17 downto 0):= to_unsigned(218453,18); 	-- 300°
   CONSTANT C_240deg 	    : unsigned (17 downto 0):= to_unsigned(174762,18); 	-- 240°
   CONSTANT C_180deg  		 : unsigned (17 downto 0):= to_unsigned(131072,18); 	-- 180°
   CONSTANT C_120deg 		 : unsigned (17 downto 0):= to_unsigned(87381, 18); 	-- 120°
   CONSTANT C_90deg    		 : unsigned (17 downto 0):= to_unsigned(65535, 18);	-- 90°
   CONSTANT C_60deg 		    : unsigned (17 downto 0):= to_unsigned(43690, 18); 	-- 60°
   CONSTANT C_30deg 		    : unsigned (17 downto 0):= to_unsigned(21845, 18); 	-- 30°

   CONSTANT C_k1      	    : unsigned (15 downto 0):= to_unsigned(56756,16);    --cos(30deg)= Wurzel(3)/2 *65535 = 56756
   --CONSTANT C_k1      	    : unsigned (15 downto 0):= to_unsigned(65535,16);    -- Wurzel(3)/2 *65535 = 56756
   
   SIGNAL s_Theta           : unsigned (17 downto 0):=(others=>'0'); 				-- Winkel Theta 0..360°
   SIGNAL s_ThetaSektor     : unsigned (17 downto 0):=(others=>'0'); 				-- Winkel Theta gebrochen auf 60° Wird dadurch erreicht, in dem man Theta durch 4 Teilt
   SIGNAL s_Bereich         : integer range 1 to 4  := 1;            				-- Aufteilung in vier Bereiche
   SIGNAL s_BereichAlt      : integer range 1 to 4  := 1;

	SIGNAL s_Uab_betrag      : unsigned (15 downto 0):=(others=>'0');
	SIGNAL s_BETA_Urz        : unsigned (15 downto 0):=(others=>'0');
	SIGNAL s_ALPHA_Urz       : unsigned (15 downto 0):=(others=>'0');


	SIGNAL s_FUNKTION1vor    : unsigned (15 downto 0):=(others=>'0');
	SIGNAL s_FUNKTION1       : unsigned (15 downto 0):=(others=>'0');
	SIGNAL s_FUNKTION2vor    : unsigned (15 downto 0):=(others=>'0');
	SIGNAL s_FUNKTION2       : unsigned (15 downto 0):=(others=>'0');

   SIGNAL s_Mult1           : unsigned (15 downto 0):=(others=>'0');
   SIGNAL s_Mult2    	    : unsigned (15 downto 0):=(others=>'0');
   SIGNAL s_Mult32          : unsigned (31 downto 0):=(others=>'0');

   -- Maximalwert des Raumzeigers
	SIGNAL s_Urzmax   	    : unsigned (15 downto 0):=(others=>'0'); --Urzmax
	SIGNAL s_Urzmax_c_k1     : unsigned (15 downto 0):=(others=>'0');
	SIGNAL s_Urzmax2   	    : unsigned (15 downto 0):=(others=>'0'); --Urzmax/2
   SIGNAL s_Urzmax2_c_k1    : unsigned (15 downto 0):=(others=>'0');

   --Ausgaenge-------------------------------------------------
   SIGNAL s_Zone      	 	 : unsigned(2 downto  0):=(others=>'0');
   SIGNAL s_Zone_alt   	 	 : unsigned(2 downto  0):=(others=>'0');
   SIGNAL S_Region 	    	 : unsigned(2 downto  0):=(others=>'0');
   SIGNAL s_Sektor 	    	 : unsigned(2 downto  0):=(others=>'0');

	SIGNAL s_RegionAlt    	 : unsigned(2 downto  0):="001";
	SIGNAL s_SektorAlt    	 : unsigned(2 downto  0):="001";

   SIGNAL s_SekZon_wechsel  : std_logic            :='1';
   SIGNAL s_Bereich_count 	 : unsigned(15 downto 0):=(others=>'0');

   TYPE s_STATE_SECTOR IS( EINGANG_SPEICHERN,
									SEKTOR_BESTIMMUNG,
									ZONEN_BESTIMMUNG,
                           SINUSBERECHNUNG_VORBEREITEN, SINUS_BERECHNEN,
                           MULT1_DURCHFUEHREN, MULT1_SPEICHERN,
                           COSINUS_BERECHNEN,
                           MULT2_DURCHFUEHREN, MULT2_SPEICHERN,
                           MULT3_VORBEREITEN, MULT3_DURCHFUEHREN, MULT3_SPEICHERN,
                           FUNKTIONEN_BERECHNEN,
                           MULT4_VORBEREITEN, MULT4_DURCHFUEHREN, MULT4_SPEICHERN,
                           MULT5_VORBEREITEN, MULT5_DURCHFUEHREN, MULT5_SPEICHERN,
                           BEREICH_BESTIMMEN,
                           REGION_BERECHNEN,
                           DATEN_RAUSGEBEN);
   SIGNAL s_STATE_HAUPT:  s_STATE_SECTOR := EINGANG_SPEICHERN;


   --Sinusergebnis-------------------------------------------
   SIGNAL s_sinus_theta 		: unsigned (15 downto 0) :=(others=>'0');
   SIGNAL s_Startsinus			: std_logic              :='0';
   SIGNAL s_sin_finish			: std_logic              :='0';
   SIGNAL s_sinus_ergebniss   : unsigned (15 downto 0) :=(others=>'0');

BEGIN--===============================================================================

Sektor_Region_Prozess: process(reset, clk, Enable, Uab_betrag, Theta, Urzmax)--Statemachine mit asynchronem Reset
BEGIN

   if (reset = '1') then

      s_Theta          	<=(others=>'0');
      s_ThetaSektor 		<=(others=>'0');
      s_Mult1	    		<=(others=>'0');
      s_Mult2       		<=(others=>'0');
      s_Mult32	    		<=(others=>'0');
      s_Urzmax2     		<=(others=>'0');
		s_Zone         	<=(others=>'0');
      s_Zone_alt     	<=(others=>'0');
      s_Sektor    		<=(others=>'0');
      s_Region    		<=(others=>'0');
      s_RegionAlt       <=(others=>'0');
      s_SekZon_wechsel  <= '1';
      s_Bereich_count   <=(others=>'0');
      s_BereichAlt      <= 1;
      Data_Out_ValidReg <= '0';              -- Ausgang noch nicht bereit
      s_STATE_HAUPT 		<= EINGANG_SPEICHERN;

   else
			if rising_edge(clk)  then
            CASE s_STATE_HAUPT is
	----------------------------------------------------------------------------
	when EINGANG_SPEICHERN =>     --1. Uebernimmt die Daten am Eingang
	------------------------------------------------------------------------------
		Data_Out_ValidReg    <= '0';

		if (Enable =  '1' )  then          -- Warte auf Start
			s_Uab_betrag  		<= Uab_betrag;
			s_Theta           <= Theta;    -- vorher Theta(17 DOWNTO 2) & "00" 360deg/4 ?
         --s_Urzmax          <= unsigned('0' &       std_logic_vector(Urzmax(14 downto 0)));
			--s_Urzmax2         <= unsigned('0' & '0' & std_logic_vector(Urzmax(14 downto 1)));
			
         s_Urzmax          <= unsigned(      std_logic_vector(Urzmax));
			s_Urzmax2         <= unsigned('0' & std_logic_vector(Urzmax(14 downto 0)));
         
         s_STATE_HAUPT     <= SEKTOR_BESTIMMUNG;
		else
			Null;
		end if;

	--------------------------------------------------------------------------------
	when SEKTOR_BESTIMMUNG =>  -- Sektor bestimmung des Raumzeigers durch Thetaposition
	------------------------------------------------------------------------------
		if    (s_Theta <  C_60deg )   then 	s_Sektor <= "001";  s_ThetaSektor <=s_Theta;              -- 60°  Vorraussetzung fuer diese Berechnung, dass der Winkel 100% stimmt
		elsif (s_Theta <  C_120deg)   then  s_Sektor <= "010";  s_ThetaSektor <=s_Theta - C_60deg;    -- 120°
		elsif (s_Theta <  C_180deg)   then  s_Sektor <= "011";  s_ThetaSektor <=s_Theta - C_120deg;   -- 180°
      elsif (s_Theta <  C_240deg)   then  s_Sektor <= "100";  s_ThetaSektor <=s_Theta - C_180deg;	 -- 240°
		elsif (s_Theta <  C_300deg)   then  s_Sektor <= "101";  s_ThetaSektor <=s_Theta - C_240deg;	 -- 300°
		elsif (s_Theta >= C_300deg)   then 	s_Sektor <= "110";  s_ThetaSektor <=s_Theta - C_300deg;   -- 360°
		else
			NULL;
		end if;
		s_STATE_HAUPT  <= ZONEN_BESTIMMUNG;

	--------------------------------------------------------------------------
	when ZONEN_BESTIMMUNG =>  -- 2. Zonen des Raumzeigers bestimmen
	--------------------------------------------------------------------------
		if    (s_Theta < C_30deg OR s_Theta >= C_300deg + C_30deg) then  s_Zone <= "001";   -- 330° .. 30°  --1
      elsif (s_Theta < C_90deg)                                  then  s_Zone <= "010"; 	-- 30° .. 90°   --2
      elsif (s_Theta < C_180deg - C_30deg)                       then  s_Zone <= "011"; 	-- usw.
		elsif (s_Theta < C_240deg - C_30deg)                       then  s_Zone <= "100";   --
		elsif (s_Theta < C_300deg - C_30deg)                       then  s_Zone <= "101"; 	--
		elsif (s_Theta < C_300deg + C_30deg)                       then  s_Zone <= "110";   --
		else
			NULL;
		end if;
		s_STATE_HAUPT  <= SINUSBERECHNUNG_VORBEREITEN;

   -----------------------------------------------------------------------------
   when SINUSBERECHNUNG_VORBEREITEN =>   -- 3 Berechnung der Sinus Funktion
	-----------------------------------------------------------------------------
      s_Startsinus     	<= '1';
      s_sinus_theta		<= s_ThetaSektor(15 downto 0);
		s_STATE_HAUPT    	<= SINUS_BERECHNEN;

	-----------------------------------------------------------------------------
   when SINUS_BERECHNEN =>   -- 4 Berechnung der Sinus Funktionen
	-----------------------------------------------------------------------------
		s_Startsinus 		<= '0';
		if (s_sin_finish = '1') then
         s_Mult1 		   <= s_sinus_ergebniss;  --Bestimmung von alpha Komponente unterbrochen auf 60°
         s_Mult2        <= s_Uab_betrag;
         s_STATE_HAUPT	<= MULT1_DURCHFUEHREN;
		end if;

   -----------------------------------------------------------------------------
   when MULT1_DURCHFUEHREN =>
   -----------------------------------------------------------------------------
   	s_Mult32 		<= s_Mult1 * s_Mult2;
      s_STATE_HAUPT	<= MULT1_SPEICHERN;

   -----------------------------------------------------------------------------
   when MULT1_SPEICHERN => -- Cosinus Berechnung
   -----------------------------------------------------------------------------
      s_BETA_Urz   	<= s_Mult32 (31 downto 16);

      s_Startsinus   <= '1';
      s_sinus_theta  <= C_90deg(15 downto 0) - s_ThetaSektor(15 downto 0); -- cos(x) = sin (90 - x) fuer den Bereich 0..90
      s_STATE_HAUPT	<= COSINUS_BERECHNEN;

   -----------------------------------------------------------------------------
   when COSINUS_BERECHNEN => -- Bestimmung von Beta komponente gebrochen auf 60°
   -----------------------------------------------------------------------------
      s_Startsinus 		<= '0';
      if (s_sin_finish = '1') then
         s_Mult1 		   <= s_sinus_ergebniss;
         s_STATE_HAUPT	<= MULT2_DURCHFUEHREN;
      end if;

   -----------------------------------------------------------------------------
    when MULT2_DURCHFUEHREN =>
   -----------------------------------------------------------------------------
      s_Mult32 		<= s_Mult1 * s_Mult2;
      s_STATE_HAUPT	<= MULT2_SPEICHERN;

   -----------------------------------------------------------------------------
    when MULT2_SPEICHERN =>
   -----------------------------------------------------------------------------
      s_ALPHA_Urz   	<= s_Mult32 (31 downto 16);
      s_STATE_HAUPT	<= MULT3_VORBEREITEN;

   -------------------------------------------------------------------------
    when MULT3_VORBEREITEN => --Berechnung der Funktion zum Vergleich
   --------------------------------------------------------------------------
      s_Mult1        <= s_Urzmax ;
      s_Mult2        <= C_k1;          --Wurzel(3)/2
      s_STATE_HAUPT	<= MULT3_DURCHFUEHREN;

   -----------------------------------------------------------------------------
    when MULT3_DURCHFUEHREN =>
   -----------------------------------------------------------------------------
      s_Mult32 		<= s_Mult1 * s_Mult2;
      s_STATE_HAUPT	<= MULT3_SPEICHERN;

   -----------------------------------------------------------------------------
    when MULT3_SPEICHERN =>
   -----------------------------------------------------------------------------
      s_Urzmax_c_k1  <= s_Mult32 (31 downto 16);
      s_STATE_HAUPT	<= FUNKTIONEN_BERECHNEN;

	--------------------------------------------------------------------------
   when FUNKTIONEN_BERECHNEN =>
	--------------------------------------------------------------------------
      s_Urzmax2_c_k1 <= unsigned('0' & std_logic_vector(s_Urzmax_c_k1(15 downto 1)) ); -- entspricht s_Urzmax/2

      s_FUNKTION1vor <= s_Urzmax2   - s_ALPHA_Urz;
      s_FUNKTION2vor <= s_ALPHA_Urz - s_Urzmax2;
      s_STATE_HAUPT	<= MULT4_VORBEREITEN;

  -------------------------------------------------------------------------
   when MULT4_VORBEREITEN =>
  --------------------------------------------------------------------------
     s_Mult1        <= s_FUNKTION1vor ;
     s_Mult2        <= C_k1;          --Wurzel(3)/2
     s_STATE_HAUPT  <= MULT4_DURCHFUEHREN;

  -----------------------------------------------------------------------------
   when MULT4_DURCHFUEHREN =>
  -----------------------------------------------------------------------------
     s_Mult32 		 <= s_Mult1 * s_Mult2;
     s_STATE_HAUPT <= MULT4_SPEICHERN;

  -----------------------------------------------------------------------------
   when MULT4_SPEICHERN =>
  -----------------------------------------------------------------------------
     s_FUNKTION1     <= s_Mult32 (30 downto 15);
     s_STATE_HAUPT	<= MULT5_VORBEREITEN;

  -------------------------------------------------------------------------
   when MULT5_VORBEREITEN =>
  --------------------------------------------------------------------------
     s_Mult1        <= s_FUNKTION2vor ;
     s_STATE_HAUPT  <= MULT5_DURCHFUEHREN;

  -----------------------------------------------------------------------------
   when MULT5_DURCHFUEHREN =>
  -----------------------------------------------------------------------------
     s_Mult32 		 <= s_Mult1 * s_Mult2;
     s_STATE_HAUPT <= MULT5_SPEICHERN;

  -----------------------------------------------------------------------------
   when MULT5_SPEICHERN =>
  -----------------------------------------------------------------------------
     s_FUNKTION2     <= s_Mult32 (30 downto 15);
     s_STATE_HAUPT	<= BEREICH_BESTIMMEN;

	-----------------------------------------------------------------------------
	when BEREICH_BESTIMMEN => --Bereich bestimmen
	-----------------------------------------------------------------------------
      IF     s_Urzmax2_c_k1  >=  s_Uab_betrag                                  AND s_Bereich_count < "011"   then  s_Bereich <= 1;
      ELSIF (s_Urzmax2_c_k1  <   s_Uab_betrag AND s_Uab_betrag < s_Urzmax2     AND s_Bereich_count < "011")  then  s_Bereich <= 2;
      elsif (s_Urzmax2       <=  s_Uab_betrag AND s_Uab_betrag < s_Urzmax_c_k1 AND s_Bereich_count < "011")  then  s_Bereich <= 3;
      elsif (s_Urzmax_c_k1   <=  s_Uab_betrag                                  AND s_Bereich_count < "011")  then  s_Bereich <= 4;
      else
         NULL;
      end if;

   s_STATE_HAUPT	<= REGION_BERECHNEN;

	-------------------------------------------------------------------------
	when REGION_BERECHNEN =>
	--------------------------------------------------------------------------
		CASE s_Bereich IS
			WHEN 1 =>
         IF ((s_Zone="001" AND s_Sektor = "001") OR (s_Zone="010" AND s_Sektor = "010")   OR
             (s_Zone="011" AND s_Sektor = "011") OR (s_Zone="100" AND s_Sektor = "100")   OR
             (s_Zone="101" AND s_Sektor = "101") OR (s_Zone="110" AND s_Sektor = "110" )) then
             s_Region     <= "001";
         elsif
            ((s_Zone="001" AND s_Sektor = "110") OR (s_Zone="010" AND s_Sektor = "001")  OR
             (s_Zone="011" AND s_Sektor = "010") OR (s_Zone="100" AND s_Sektor = "011")  OR
             (s_Zone="101" AND s_Sektor = "100") OR (s_Zone="110" AND s_Sektor = "101")) then
             s_Region     <= "010";
         else
            NULL;
         end if;

         WHEN 2 =>
            IF ((s_Zone="001" AND s_Sektor = "001") OR (s_Zone="010" AND s_Sektor = "010")   OR
                (s_Zone="011" AND s_Sektor = "011") OR (s_Zone="100" AND s_Sektor = "100")   OR
                (s_Zone="101" AND s_Sektor = "101") OR (s_Zone="110" AND s_Sektor = "110" )) then
                IF (s_FUNKTION1 > s_BETA_Urz AND s_RegionAlt /= "100") then
                  s_Region     <= "001";
                else
                  s_Region     <= "100";
               END IF;

            elsif
               ((s_Zone="001" AND s_Sektor = "110") OR (s_Zone="010" AND s_Sektor = "001")  OR
                (s_Zone="011" AND s_Sektor = "010") OR (s_Zone="100" AND s_Sektor = "011")  OR
                (s_Zone="101" AND s_Sektor = "100") OR (s_Zone="110" AND s_Sektor = "101")) then
                IF (s_FUNKTION1 < s_BETA_Urz AND s_RegionAlt /= "010") then
                  s_Region     <= "101";
                else
                  s_Region     <= "010";
               end if;
            else
               NULL;
            end if;

			WHEN 3 =>
         IF ((s_Zone="001" AND s_Sektor = "001") OR (s_Zone="010" AND s_Sektor = "010")   OR
             (s_Zone="011" AND s_Sektor = "011") OR (s_Zone="100" AND s_Sektor = "100")   OR
             (s_Zone="101" AND s_Sektor = "101") OR (s_Zone="110" AND s_Sektor = "110" )) then
             IF (s_FUNKTION2 > s_BETA_Urz AND s_RegionAlt /= "100") then
               s_Region     <= "011";
             else
               s_Region     <= "100";
            END IF;

         elsif
            ((s_Zone="001" AND s_Sektor = "110") OR (s_Zone="010" AND s_Sektor = "001")   OR
             (s_Zone="011" AND s_Sektor = "010") OR (s_Zone="100" AND s_Sektor = "011")   OR
             (s_Zone="101" AND s_Sektor = "100") OR (s_Zone="110" AND s_Sektor = "101"))  then
             IF (s_Urzmax2_c_k1 > s_BETA_Urz AND s_RegionAlt /= "110") then
               s_Region     <= "101";
             else
               s_Region     <= "110";
             end if;
         else
            NULL;
         end if;

         WHEN 4 =>
            IF ((s_Zone="001" AND s_Sektor = "001") OR (s_Zone="010" AND s_Sektor = "010")   OR
                (s_Zone="011" AND s_Sektor = "011") OR (s_Zone="100" AND s_Sektor = "100")   OR
                (s_Zone="101" AND s_Sektor = "101") OR (s_Zone="110" AND s_Sektor = "110" )) then
                s_Region     <= "011";
            elsif
               ((s_Zone="001" AND s_Sektor = "110") OR (s_Zone="010" AND s_Sektor = "001")   OR
                (s_Zone="011" AND s_Sektor = "010") OR (s_Zone="100" AND s_Sektor = "011")   OR
                (s_Zone="101" AND s_Sektor = "100") OR (s_Zone="110" AND s_Sektor = "101"))  then
                s_Region     <= "110";
            else
               NULL;
            end if;

			end CASE;


         if s_SekZon_wechsel ='1'                THEN
            s_Bereich_count <= (others=>'0');
         elsif s_Bereich /= s_BereichAlt         THEN
               s_Bereich_count <= s_Bereich_count + 1;
         else
            NULL;
         end if;

s_STATE_HAUPT   		   <= DATEN_RAUSGEBEN;


-----------------------------------------------------------------------------
 when DATEN_RAUSGEBEN =>      --8.
-----------------------------------------------------------------------------
IF (s_SektorAlt /= s_Sektor OR  s_Zone_alt /= s_Zone) then -- Zone oder Sektor haben sich geaendert
   s_SekZon_wechsel  <= '1';                                     -- dann darf sich region auch wechseln
else
   s_SekZon_wechsel  <= '0';                                     -- Region hat sich einmal inerhalb von 30° gewechselt
end if;

   Region 		      <= s_Region;
	s_RegionAlt       <= s_Region;
   Zone           	<= s_Zone;
   s_Zone_alt        <= s_Zone;
   Sektor         	<= s_Sektor;
   s_SektorAlt    	<= s_Sektor;
   s_BereichAlt      <= s_Bereich;
   ThetaSektor	   	<= s_ThetaSektor(15 DOWNTO 0);
   Data_Out_ValidReg <= '1';
   s_STATE_HAUPT     <= EINGANG_SPEICHERN;

----------------------------------------------------------------------------
 when others =>
----------------------------------------------------------------------------
   s_STATE_HAUPT   <= EINGANG_SPEICHERN;

   end case;
   end if;
 end if;
end process Sektor_Region_Prozess ;




sinusa: process (s_sinus_theta, s_Startsinus, clk, reset) --berechnte sinus (0..90) Eingangsvariable 16bit UNSIGNED
   --Konstanten-----------------------------------------------
   CONSTANT c_pi2   :   UNSIGNED (15 downto 0):= TO_UNSIGNED(65535,16);	   --pi/2 = 90 ist voller Aussteuerbereich
   CONSTANT c_pi51  :   UNSIGNED (15 downto 0):= TO_UNSIGNED(37515,16);	   --51,52 = (2^16-1) * 51,52/90
   CONSTANT c_a1    :   UNSIGNED (16 downto 0):= TO_UNSIGNED(102944,17);	--2^17-1 *1,570796/2 ~>2^16-1 * 1.570796
   CONSTANT c_a2    :   UNSIGNED (15 downto 0):= TO_UNSIGNED(42334,16);	   --2^16-1 *0,645964
   CONSTANT c_a3    :   UNSIGNED (15 downto 0):= TO_UNSIGNED(5223,16);	   --2^16-1 *0,079693
   CONSTANT c_a4    :   UNSIGNED (15 downto 0):= TO_UNSIGNED(307,16);		--2^16-1 *4,681754*10^-3
	CONSTANT c_b1	  :  	UNSIGNED (16 downto 0):= TO_UNSIGNED(80852,17);	   --2^17-1 *1,233701/2 = 80852 ~>2^16-1 * 1.233701
   CONSTANT c_b2	  :  	UNSIGNED (15 downto 0):= TO_UNSIGNED(16624,16);    --2^16-1 *0,25367
   CONSTANT c_b3    :  	UNSIGNED (15 downto 0):= TO_UNSIGNED(1367,16);	   --2^16-1 *0,020863

   --Fuer Multiplikation
   VARIABLE v_Mult1 :   unsigned (31 downto 0):=(others=>'0');
   VARIABLE v_Mult2 :   unsigned (31 downto 0):=(others=>'0');
   VARIABLE v_Mult3 :   unsigned (31 downto 0):=(others=>'0');
   VARIABLE v_Mult21:   unsigned (31 downto 0):=(others=>'0');
   VARIABLE v_Mult22:   unsigned (31 downto 0):=(others=>'0');
   VARIABLE v_Mult23:   unsigned (31 downto 0):=(others=>'0');
   VARIABLE v_Mult24:   unsigned (32 downto 0):=(others=>'0');
   VARIABLE v_Mult31:   unsigned (31 downto 0):=(others=>'0');
   VARIABLE v_Mult32:   unsigned (31 downto 0):=(others=>'0');
   VARIABLE v_Mult33:   unsigned (31 downto 0):=(others=>'0');

   ALIAS		a_Mult1 :	UNSIGNED (15 downto 0) IS v_Mult1 (31 downto 16);
	ALIAS		a_Mult2 :	UNSIGNED (15 downto 0) IS v_Mult2 (31 downto 16);
	ALIAS		a_Mult3 :	UNSIGNED (15 downto 0) IS v_Mult3 (31 downto 16);
	ALIAS		a_Mult21:	UNSIGNED (15 downto 0) IS v_Mult21(31 downto 16);
	ALIAS		a_Mult22:	UNSIGNED (15 downto 0) IS v_Mult22(31 downto 16);
	ALIAS		a_Mult23:	UNSIGNED (15 downto 0) IS v_Mult23(31 downto 16);
	ALIAS		a_Mult24:	UNSIGNED (15 downto 0) IS v_Mult24(31 downto 16);
	ALIAS		a_Mult31:	UNSIGNED (15 downto 0) IS v_Mult31(31 downto 16);
	ALIAS		a_Mult32:	UNSIGNED (15 downto 0) IS v_Mult32(31 downto 16);
	ALIAS		a_Mult33:	UNSIGNED (15 downto 0) IS v_Mult33(31 downto 16);

   VARIABLE v_usig1_mult_a_16    : UNSIGNED (15 downto 0):=(others=>'0');
   VARIABLE v_usig1_mult_b_16    : UNSIGNED (15 downto 0):=(others=>'0');
   VARIABLE v_usig2_mult_a_16    : UNSIGNED (15 downto 0):=(others=>'0');
   VARIABLE v_usig2_mult_b_16    : UNSIGNED (15 downto 0):=(others=>'0');
   VARIABLE v_usig3_mult_a_17    : UNSIGNED (16 DOWNTO 0):=(others=>'0');
   VARIABLE v_usig3_mult_b_16    : UNSIGNED (15 DOWNTO 0):=(others=>'0');
   VARIABLE v_usig5_mult_a_16    : UNSIGNED (15 downto 0):=(others=>'0');
   VARIABLE v_usig5_mult_b_16    : UNSIGNED (15 downto 0):=(others=>'0');
   VARIABLE v_usig1_Mult_32  		: UNSIGNED (31 downto 0):=(others=>'0');
   VARIABLE v_usig2_Mult_32  		: UNSIGNED (31 downto 0):=(others=>'0');
   VARIABLE v_usig3_Mult_33  		: UNSIGNED (32 downto 0):=(others=>'0');
   VARIABLE v_usig5_Mult_32  		: UNSIGNED (31 downto 0):=(others=>'0');

   --Winkel----------
   VARIABLE v_winkel    			: unsigned (15 downto 0):=(others=>'0');
   VARIABLE v_winkeltyp 			: std_logic             :='0';

   --Endergebnis-----
   VARIABLE v_Summe		         : unsigned (18 downto 0):=(others=>'0');

   --Zustaende---------
   TYPE T_STATE_SINUS  IS(	WINKELAUSWAHL,
                           MULTIPLIKATION1VOR, MULTIPLIKATION1BER, MULTIPLIKATION1SAV,
                           MULTIPLIKATION2VOR, MULTIPLIKATION2BER, MULTIPLIKATION2SAV,
                           MULTIPLIKATION3VOR, MULTIPLIKATION3BER, MULTIPLIKATION3SAV,
                           ADDITION,
                           AUSGABE);
   VARIABLE v_STATE_SINUS:	T_STATE_SINUS := WINKELAUSWAHL;

 BEGIN
      if (reset = '1') then
			v_Mult1       	   :=(others=>'0');
			v_Mult2       	   :=(others=>'0');
			v_Mult3       	   :=(others=>'0');
			v_Mult21      	   :=(others=>'0');
			v_Mult22      	   :=(others=>'0');
			v_Mult23      	   :=(others=>'0');
			v_Mult24      	   :=(others=>'0');
			v_Mult31      	   :=(others=>'0');
			v_Mult32      	   :=(others=>'0');
			v_Mult33      	   :=(others=>'0');

         v_usig1_mult_a_16 :=(others=>'0');
         v_usig1_mult_b_16 :=(others=>'0');
         v_usig2_mult_a_16 :=(others=>'0');
         v_usig2_mult_b_16 :=(others=>'0');
         v_usig3_mult_a_17 :=(others=>'0');
         v_usig3_mult_b_16 :=(others=>'0');
         v_usig5_mult_a_16 :=(others=>'0');
         v_usig5_mult_b_16 :=(others=>'0');
         v_usig1_Mult_32   :=(others=>'0');
         v_usig2_Mult_32  	:=(others=>'0');
         v_usig3_Mult_33  	:=(others=>'0');
         v_usig5_Mult_32  	:=(others=>'0');

			v_winkel      	   :=(others=>'0');
			v_winkeltyp   	   :='0';
			v_Summe       	   :=(others=>'0');
			v_STATE_SINUS 	   := WINKELAUSWAHL;

      else
         if rising_edge(clk)   THEN
            CASE v_STATE_SINUS IS

	------------------------------------------------------------------
   when WINKELAUSWAHL =>   --1.Takt
   ------------------------------------------------------------------
		s_sin_finish		<= '0';

		if  (s_Startsinus ='1') then
			If (s_sinus_theta < c_pi51)   then  --51,52 Schnittpunkt, -> kleinsten Fehler
				v_winkel    := s_sinus_theta;
				v_winkeltyp := '0';
			else
				v_winkel    := c_pi2 - s_sinus_theta;
				v_winkeltyp := '1';
			end If;

			v_STATE_SINUS	:= MULTIPLIKATION1VOR;
		end if;

   ------------------------------------------------------------------
   when MULTIPLIKATION1VOR =>  --2.Takt
   ------------------------------------------------------------------
		v_usig1_mult_a_16 := v_winkel;
		v_usig1_mult_b_16 := v_winkel;
		v_usig2_mult_a_16 := c_a2;
		v_usig2_mult_b_16 := v_winkel;
		v_usig5_mult_a_16 := c_a4;
		v_usig5_mult_b_16 := v_winkel;
	   v_STATE_SINUS 	   := MULTIPLIKATION1BER;

  ------------------------------------------------------------------
	when MULTIPLIKATION1BER =>  --3.Takt
  ------------------------------------------------------------------
		v_usig1_Mult_32 := v_usig1_mult_a_16 * v_usig1_mult_b_16;
		v_usig2_Mult_32 := v_usig2_mult_a_16 * v_usig2_mult_b_16;
		v_usig5_Mult_32 := v_usig5_mult_a_16 * v_usig5_mult_b_16;
		v_STATE_SINUS   := MULTIPLIKATION1SAV;

  ------------------------------------------------------------------
	when MULTIPLIKATION1SAV =>  --4.Takt
  ------------------------------------------------------------------
		v_Mult1        := v_usig1_Mult_32;
		v_Mult2        := v_usig2_Mult_32;
		v_Mult3        := v_usig5_Mult_32;
		v_STATE_SINUS 	:= MULTIPLIKATION2VOR;

  ------------------------------------------------------------------
	when MULTIPLIKATION2VOR =>  --5.Takt
  ------------------------------------------------------------------
	   CASE v_winkeltyp IS
			when '0' =>
				v_usig1_mult_a_16 := a_Mult1;
				v_usig1_mult_b_16 := a_Mult3;
				v_usig2_mult_a_16 := c_a3;
				v_usig2_mult_b_16 := v_winkel;
				v_usig3_mult_a_17 := c_a1;
				v_usig3_mult_b_16 := v_winkel;
			when others =>
				v_usig1_mult_a_16 := c_b3;
				v_usig1_mult_b_16 := a_Mult1;
				v_usig3_mult_a_17 := c_b1;
				v_usig3_mult_b_16 := a_Mult1;
		END CASE;

		v_usig5_mult_a_16       := a_Mult1;
		v_usig5_mult_b_16       := a_Mult1;
		v_STATE_SINUS 	         := MULTIPLIKATION2BER;

  ------------------------------------------------------------------
	when MULTIPLIKATION2BER =>  --6.Takt
  ------------------------------------------------------------------
		v_usig1_Mult_32 := v_usig1_mult_a_16 * v_usig1_mult_b_16;
		v_usig2_Mult_32 := v_usig2_mult_a_16 * v_usig2_mult_b_16; --Value only vald if s_winkeltyp='0'
		v_usig3_Mult_33 := v_usig3_mult_a_17 * v_usig3_mult_b_16;
		v_usig5_Mult_32 := v_usig5_mult_a_16 * v_usig5_mult_b_16;
		v_STATE_SINUS   := MULTIPLIKATION2SAV;

  ------------------------------------------------------------------
	when MULTIPLIKATION2SAV =>  --7.Takt
  ------------------------------------------------------------------
		v_Mult22      := v_usig1_Mult_32;
		v_Mult23      := v_usig2_Mult_32;
		v_Mult24      := v_usig3_Mult_33;
		v_Mult21      := v_usig5_Mult_32;
		v_STATE_SINUS := MULTIPLIKATION3VOR;

  ------------------------------------------------------------------
	when MULTIPLIKATION3VOR =>  --8.Takt
  ------------------------------------------------------------------
		CASE v_winkeltyp IS
			when '0'    =>
				v_usig1_mult_a_16 := a_Mult21;
				v_usig1_mult_b_16 := a_Mult23;
				v_usig2_mult_a_16 := a_Mult1;
				v_usig2_mult_b_16 := a_Mult2;
			when others =>
				v_usig1_mult_a_16 := c_b2;
				v_usig1_mult_b_16 := a_Mult21;
		END CASE;

		v_usig5_mult_a_16       := a_Mult21;
		v_usig5_mult_b_16       := a_Mult22;
      v_STATE_SINUS 	         := MULTIPLIKATION3BER;

  ------------------------------------------------------------------
	when MULTIPLIKATION3BER =>  --9.Takt
  ------------------------------------------------------------------
		v_usig1_Mult_32 := v_usig1_mult_a_16 * v_usig1_mult_b_16;
		v_usig2_Mult_32 := v_usig2_mult_a_16 * v_usig2_mult_b_16;
		v_usig5_Mult_32 := v_usig5_mult_a_16 * v_usig5_mult_b_16;
      v_STATE_SINUS   := MULTIPLIKATION3SAV;

  ------------------------------------------------------------------
	when MULTIPLIKATION3SAV =>  --10.Takt
  ------------------------------------------------------------------
		v_Mult32       := v_usig1_Mult_32;
		v_Mult33       := v_usig2_Mult_32;
		v_Mult31       := v_usig5_Mult_32;
      v_STATE_SINUS 	:= ADDITION;

   ------------------------------------------------------------------
   when ADDITION =>    --11. Takt, bede Reihen aufaddieren
   ------------------------------------------------------------------
      CASE v_winkeltyp IS
         when '0'    =>
            v_Summe := resize(a_Mult24,19) - resize(a_Mult33,19) + resize(a_Mult32,19) - resize(a_Mult31,19);
         when others =>
            v_Summe := resize(c_pi2,19)    - resize(a_Mult24,19) + resize(a_Mult32,19) - resize(a_Mult31,19);
      END CASE;
      v_STATE_SINUS 	:= AUSGABE;

   ------------------------------------------------------------------
   when AUSGABE=>    --12. Ergebniss ausgeben
   ------------------------------------------------------------------
      if (v_Summe >  TO_UNSIGNED (65535,16)) then
         s_sinus_ergebniss  <= TO_UNSIGNED (65535,16);
		else
			s_sinus_ergebniss  <= v_Summe(15 downto 0); --Vorzeichen ist immer positiv
		end if;

		s_sin_finish	<= '1';
      v_STATE_SINUS 	:= WINKELAUSWAHL;

   ----------------------------------------------------------------------------
   when others => --Wird theoretisch nie erreicht
   ----------------------------------------------------------------------------
      v_STATE_SINUS	:= WINKELAUSWAHL;

   END CASE;
   END IF;  --clk
   END IF;  --reset
END process sinusa;

end ARCHITECTURE behavioral;
