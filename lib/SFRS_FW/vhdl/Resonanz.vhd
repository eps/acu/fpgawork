--  Filename: Resonanz.vhd (VHDL File)
--  Datum   : 27.06.2020
--            22.01.2021 Aenderung von IGBT_Sek_Takt_ON und switch_ON auf unsigned mit mehreren Optionen
--  Authors : A.Wiest

--------------------------------------------------------------------------------------------------------------
-- Version Kontrolle :
--  born                A.Wiest          27.06.2020
--  Aenderungen
--------------------------------------------------------------------------------------------------------------

--  Beschreibung:
--  PWM Muster fuer den Resonanzuebertrager
--  mit Ansteuerung fuer die Hilfsschalter (die Hilfsschalter werden umgeschaltet wenn die Steuerspannung (nicht der Sollstrom) ihre Polaritaet wechselt)

---===========================================================================================================
library ieee;                                       -- Library Declaration
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Resonanz is               
   generic(
      takt                   : integer   := 100_000_000);

   port(
      --System Signals
      clk, reset, Enable     : in  std_logic ;

      -- Zeitkonstanten                                        -- primaerseite:
      toff                   : in  unsigned(15 downto 0);      -- 0000    0000 Primarseitige Transistoren sind aus, die Sek. Seite ist aber bereits eingeschaltet und steht fuer die richtige Polaritaet
      t1_gap                 : in  unsigned(15 downto 0);      -- 0001 o. 0010 (erst ein Transistor d. Prim.seite wird eingeschaltet), hier die Uebergangszeit
      t2_gap                 : in  unsigned(15 downto 0);      -- 1001 o. 0110 prim; Hilfstransistor (f.Rueckspeisung) d. Sek Seite noch nicht eingeschaltet
      ton                    : in  unsigned(15 downto 0);      -- 1001 o. 0110 prim; Sek Polarität + Hilfstransistor -  Zeit, in der die Energie (Sinuskupe) Uebertragen o. Zurueckgesp. wird (Takte x 10ns)  S1+S4 o. S2+S3
      t3_gap                 : in  unsigned(15 downto 0);      -- 1001 o. 0110 prim; Sek Polarität  ohne Hilfstransistor 
      t4_gap                 : in  unsigned(15 downto 0);      -- 0001 o. 0010 oder  
                                                               -- 1000 o. 0100 (einen Transistor)
      
      tPolar_Umschaltung     : in  unsigned(15 downto 0);      -- Zeit fuer die Umschaltung der Polaritaet (pos->neg oder neg->pos) der Ausgangsspannung
      
      tSymetrie              : in  unsigned(15 downto 0);      -- Symetrierung (Gewichtung erster oder zweiter Stromkuppe) -> wird momentan nicht verwendet
      Symetrievorzeichen     : in  std_logic;                  -- =1 erste Stromkuppe wird mehr gewichtet(wird laenger)
      BenutzeExtClk          : in  std_logic;                  -- =1 Ext Clock wird benutzt -> dadurch wird ein Durchlauf stattfinden (bein Enable=1)
      ExtClk                 : in  std_logic;                  -- =1 die Sinushalbwelle wird ausgeloest

      -- PWM - Steuerung
      Ust                    : in  signed  (15 downto 0);      -- Steuerspannung um die Polarität der Ausgangsspannung und Einschaltgrenzen fuer die Schalter zu bestimmen
      Ust_switch_ON          : in  unsigned(15 downto 0);      -- Grenze der Steuerspannung, bei der die Hilfschalter abgeschalet werden
      
      Ist_sign               : in  std_logic;                  -- Steuerstrom Vorzeichen Neg = '1' um die Speisung (1. & 3. Quadrant) oder Ruckspeisung (2. & 4. Quadrant)
      Switch_ON              : in  unsigned (1 downto 0);      -- = "00" , Hilfsschütze sind abgeschaltet
                                                               -- = "01" , Hilfsschütze sind über die Schwellen aktiv (egal ob Speisung oder Rueckspeisung )      
                                                               -- = "10" , Hilfsschütze sind aktiv über die Schwelle & wenn nicht zurückgespeist wird
                                                               -- = "11" , noch nicht belegt
      
      IGBT_Sek_Takt_ON       : in  unsigned (1 downto 0);      -- = "00" Sekundaerseite wird nicht getaktet
                                                               -- = "01" Sekundaerseite wird immer getaktet
                                                               -- = "10" Sekundaerseite wird nur beim Ruckspeisung getaktet
                                                               -- = "11" Sekundaerseite wird angefangen von dem kleinenBand & beim Ruckspeisung getaktet
      
      -- Outputs
      S1p,S2p,S3p,S4p        : out std_logic ;                 -- Signale fuer Transistoren der Primaerseite
      S1s,S2s,S3s,S4s        : out std_logic ;                 -- Signale fuer Transistoren der Sekundaerseite
      S1x,S2x,S3x,S4x        : out std_logic ;                 -- Signale fuer Hilfsschuetze (sind parallel zu den jeweiligen IGBTs der Sekundaerseite)
      PWM_Data_Out_Valid     : out std_logic ;                 -- PWM Output Valid Signal
      PeriodTime             : out unsigned(15 downto 0);      -- Zeit fuer eine Periode
      PolarUmschaltung       : out std_logic );                -- findet die Polaritaet-Umschaltung statt, so wird dieser Signal = '1', sonst ='0'         
end Resonanz;

--==============================================================================================================================

architecture behavioral of Resonanz is -- Architecture Declaration

   signal s_ton                  : unsigned(15 downto 0)        := (others => '0');      
   signal s_ton_korr             : unsigned(15 downto 0)        := (others => '0');      
   signal s_t1_gap               : unsigned(15 downto 0)        := (others => '0');       
   signal s_t2_gap               : unsigned(15 downto 0)        := (others => '0');      
   signal s_t3_gap               : unsigned(15 downto 0)        := (others => '0');      
   signal s_t4_gap               : unsigned(15 downto 0)        := (others => '0');      
   signal s_toff                 : unsigned(15 downto 0)        := (others => '0');      
   signal s_PeriodTime           : unsigned(15 downto 0)        := (others => '0');      

   signal s_Ust_abs              : unsigned(15 downto 0)        := (others => '0');      

   signal s_tSymetrie            : unsigned(15 downto 0)        := (others => '0');        
   signal s_Symetrievorzeichen   : std_logic                    := '0';
   signal s_Polarity             : std_logic                    := '0';                   
   signal s_Polarity_vorher      : std_logic                    := '0';
   signal s_rueckspeisung        : std_logic                    := '0';             -- wird dazu verwendet, damit man weisst wann der Transistor der Sek.Seite Takten muss -> besser er taktet die ganze Zeit
   signal s_PolarUmschaltung     : std_logic                    := '0';
   
   signal s_cnt1                 : unsigned(15 downto 0)        := (others => '0'); -- 1. Counter
   signal s_cnt2                 : unsigned(15 downto 0)        := (others => '0'); -- 2. Counter
   signal s_cnt3                 : unsigned(15 downto 0)        := (others => '0'); -- 3. Counter
   signal s_cnt4                 : unsigned(15 downto 0)        := (others => '0'); -- 4. Counter
   signal s_cnt5                 : unsigned(15 downto 0)        := (others => '0'); -- 5. Counter

   signal s_wechseltakt          : unsigned(0  downto 0)        := (others => '0'); -- Zustand des Wechseltaktes

   signal s_prim                 : std_logic_vector (3 downto 0):= (others => '0'); -- Zustand der Schalter auf der Primaerseite (s4p,s3p,s2p,s1p)
   signal s_sec                  : std_logic_vector (3 downto 0):= (others => '0'); -- Zustand der Schalter auf der Sekundaerseite (s4s,s3s,s2s,s1s)
   signal s_Auxiliary            : std_logic_vector (3 downto 0):= (others => '0'); -- Zustand der Hilfs-Schalter auf der Sekundaerseite (s4x,s3x,s2x,s1x)

   type STATE_PWM is (  EINGANG_SPEICHERN,
                        POLARITAET_AND_TOFF,
                        HILFSSCHALTER,
                        RESONANZ_UST_POS,
                        RESONANZ_UST_NEG);

   signal s_pwm_state : STATE_PWM:= EINGANG_SPEICHERN;



begin   --=========================================================================================================================
   Resonanz : process(clk, reset, Enable, BenutzeExtClk, ExtClk)             -- PWM Process

   begin
      if (reset = '1') then
         s_ton                  <= (others => '0');   -- gespeicherten Zeiten
         s_ton_korr             <= (others => '0');
         s_t1_gap               <= (others => '0');
         s_t2_gap               <= (others => '0');
         s_t3_gap               <= (others => '0');
         s_t4_gap               <= (others => '0');
         s_toff                 <= (others => '0');
         
         s_PeriodTime           <= (others => '0');
         PeriodTime             <= (others => '0');
         
         s_tSymetrie            <= (others => '0');
         s_Symetrievorzeichen   <= '0';
         s_prim                 <= (others => '0');
         s_sec                  <= (others => '0');
         s_Auxiliary            <= (others => '0');

         s_Polarity             <= '0';              -- 0 = positive polaritaet am Ausgang
         s_Polarity_vorher      <= '0';
         s_rueckspeisung        <= '0'; 
         s_PolarUmschaltung     <= '0';
         s_USt_abs              <= (others => '0');

         s_cnt1                 <= (others => '0');  -- alle Zaehler  = 0
         s_cnt2                 <= (others => '0');
         s_cnt3                 <= (others => '0');
         s_cnt4                 <= (others => '0');
         s_cnt5                 <= (others => '0');
                                                     -- Alle IGBTs und Schalter sind ausgeschaltet
         S1p                    <= '0';              -- Primaerseite
         S2p                    <= '0';
         S3p                    <= '0';
         S4p                    <= '0';

         S1s                    <= '0';              -- Sekundaerseite
         S2s                    <= '0';
         S3s                    <= '0';
         S4s                    <= '0';

         S1x                    <= '0';              -- Hilfsschalter
         S2x                    <= '0';
         S3x                    <= '0';
         S4x                    <= '0';

         s_wechseltakt          <= "0";              -- Zustand des Wechseltaktes(0 -erster Durchlauf, 1 der zweite Durchlauf)
         PWM_Data_Out_Valid     <= '0';
         PolarUmschaltung       <= '0';

      else
         if rising_edge(clk) then
           if (Enable = '1') then    
            S1p                 <= s_prim(3);       -- Schalter auf der Primaerseite
            S2p                 <= s_prim(2);       -- Lauft weiter bis Takt zu Ende ist
            S3p                 <= s_prim(1);
            S4p                 <= s_prim(0);
           
           else
            S1p                 <= '0';             -- Schalter auf der Primaerseite werden bei Enable='0' abgeschaltet
            S2p                 <= '0';        
            S3p                 <= '0';
            S4p                 <= '0';
           end if;
                                                    -- Sekundaerseite reagiert nicht auf Enable (bleibt in dem vorangegangenem Zustand)
            S1s                 <= s_sec(3);        -- Schalter auf der Sekundaerseite
            S2s                 <= s_sec(2);        -- Die Sek.seite laeuft weiter unabhhaengig von dem Enable
            S3s                 <= s_sec(1);
            S4s                 <= s_sec(0);

            S1x                 <= s_Auxiliary(3);  -- Hilfsschalter parallel zur sekundaerschaltern
            S2x                 <= s_Auxiliary(2);  -- lauft weiter bis Takt zu ende ist
            S3x                 <= s_Auxiliary(1);
            S4x                 <= s_Auxiliary(0);
            
            if s_PolarUmschaltung = '0' then
               s_PeriodTime        <= t1_gap + t2_gap + ton + t3_gap + t4_gap + toff + 4; -- Halbe Sinuswelle (+4 wegen den Uebergaengen in die Zustaende)
               PeriodTime          <= s_PeriodTime (13 downto 0) & "00" +4;               -- ganze Periode +2 wegen dem Tiefsetzst. u. +2 wenn im Eingang_SPEICHERN noch zwei Takte gewartet wird
            end if;
         
         case s_pwm_state is
         ---------------------------------------------------------------------------------
         when EINGANG_SPEICHERN => --1. Takt
         ---------------------------------------------------------------------------------
            s_cnt1                  <= (others => '0'); 
            s_cnt2                  <= (others => '0'); 
            s_cnt3                  <= (others => '0'); 
            s_cnt4                  <= (others => '0'); 
            s_cnt5                  <= (others => '0');
            PWM_Data_Out_Valid      <= '0';

            if (Enable = '1') then                                                                             -- PWM ist freigegeben
               s_ton                <= ton;                                                                    -- vorgegebene Zeiten speichern
               s_t1_gap             <= t1_gap;
               s_t2_gap             <= t2_gap;
               s_t3_gap             <= t3_gap;
               s_t4_gap             <= t4_gap;
               s_toff               <= toff;
               
               
               s_tSymetrie          <= tSymetrie;
               s_Symetrievorzeichen <= Symetrievorzeichen;
               s_Polarity           <= NOT(Ust(15));                                                            -- = '1' , Ausgangsspannung positiv (NOT eingefuehrt)
                                                                                                                -- Steuerspannung, die von dem Stromregler kommt
               if ABS(Ust) < 32765 then s_Ust_abs <= unsigned(std_logic_vector(ABS(Ust)));
               else                     s_Ust_abs <= to_unsigned(32766,16) ;
               end if;
               
               if ((Ust <= 0 AND Ist_sign = '0') OR (Ust > 0 AND Ist_sign = '1')) then s_rueckspeisung <= '0'; -- Zeichen fuer Ust getauscht
               else                                                                    s_rueckspeisung <= '1';
               end if;


               if BenutzeExtClk ='0' OR (BenutzeExtClk = '1' AND ExtClk ='1') then 
                  s_pwm_state       <= HILFSSCHALTER;
               end if;

            else
               s_prim               <= "0000";   --werden eigentlich sofort asynchron abgeschaltet, somit hier nicht notwendig, nur zur Uebersicht
               --s_sec                <= "0000"; -- muss die sek. Seite wirklich abgeschaltet werden?
               s_Auxiliary          <= "0000"; -- muessen die schalter wirklich abgeschaltet werden?
            end if;

         ---------------------------------------------------------------------------------
         when HILFSSCHALTER =>    --2.  Takt
         ---------------------------------------------------------------------------------                -- bei Stromrichtungsaenderung bleibt der Schalter drin, nur wenn die Spannung sich aendert  
            if    (Switch_ON = "00") then                                                                 -- muss man die Schalter umdrehen, sehr Wahrscheinlich werden dabei Stoerungen entstehen
               s_Auxiliary  <= "0000";                                                                    -- die Hilfsschuetze sind ausgeschaltet 
            
            elsif (Switch_ON = "01" AND (s_Ust_abs <= Ust_switch_ON)) then                                -- die Hilfsschuetze  ausschalten, da die Steuerspannung kleiner als die Grenze ist 
               s_Auxiliary  <= "0000"; 
            
            elsif (Switch_ON = "01" AND (s_Ust_abs > Ust_switch_ON))      then                            -- die Hilfsschuetze sind uber die Schwellen aktiv (egal ob Speisung oder Rueckspeisung )
               if   (s_Polarity = '0' AND s_sec(3)='1' AND s_sec(0) ='1') then                            -- Die Hilfsschalter werden nur dan dazu geschaltet, wenn die IGBTs bereits an sind
                  s_Auxiliary  <= "1001";                                                                 -- damit soll Signalfehler ausgeschlossen werden
               elsif(s_Polarity = '1' AND s_sec(2)='1' AND s_sec(1) ='1') then
                  s_Auxiliary  <= "0110";
               end if;
            
            elsif (Switch_ON = "10" AND s_rueckspeisung = '1') then                                       -- beim Rueckspeisen sind für "10" die Schalter abgeschaltet
               s_Auxiliary  <= "0000";
            
            elsif (Switch_ON = "10" AND (s_Ust_abs <= Ust_switch_ON) AND s_rueckspeisung = '0') then      -- Rueckspeisung nicht aktiv, aber die Spannungsschwelle noch nicht erreicht -> Schalter aus
               s_Auxiliary  <= "0000";
 
            elsif (Switch_ON = "10" AND (s_Ust_abs > Ust_switch_ON) AND s_rueckspeisung = '0') then      -- die Hilfsschuetze sind uber die Schwellen aktiv nur bei Speisung 
               if   (s_Polarity = '0' AND s_sec(3)='1' AND s_sec(0) ='1') then                           -- Die Hilfsschalter werden nur dan dazu geschaltet, wenn die IGBTs bereits an sind
                  s_Auxiliary  <= "1001";                                                                -- damit soll Signalfehler ausgeschlossen werden
               elsif(s_Polarity = '1' AND s_sec(2)='1' AND s_sec(1) ='1') then
                  s_Auxiliary  <= "0110";
               end if;
               
            else
               NULL;                                                                                     -- Zustand "11" noch nicht definiert
            end if;
            s_pwm_state       <= POLARITAET_AND_TOFF;
                                                                                                         -- vielleicht sollte man sicherheitshalber, falls die Polaritetumschaltung kommt und die Schalter noch an sind
                                                                                                         -- warum auch immer, dass diese abgeschaltet werden noch bevor man umschaltung der IGBTs ausfuehrt
                                                                                                         
         ---------------------------------------------------------------------------------               -- beider Polaritaetumschaltung geht man davon aus, dass die Hilfsschalter bereits abgeschaltet sind da 
         when POLARITAET_AND_TOFF =>    --3.  Takt                                                       -- die Steuerspannung unter einem Wert ist unn somit sind diese abgeschaltet
         ---------------------------------------------------------------------------------
            if    (s_Polarity_vorher = '1' AND s_Polarity = '0' AND s_cnt1 < tPolar_Umschaltung) then    -- Vorher war die Polaritaet negativ "0110"
               s_PolarUmschaltung   <= '1';                                                              -- Die PWM des Buck/Boost bleibt in diesen Momemt stehen
               PolarUmschaltung     <= '1';                                                              -- Anzeige Polaritaetumschaltung der sekundaerseite findet statt 
               s_cnt1               <= s_cnt1 + 1;                                                       -- und abwarten
            
            elsif (s_Polarity_vorher = '1' AND s_Polarity = '0' AND s_cnt2 < tPolar_Umschaltung) then
               s_sec                <= "0000";                                                           -- erst jetzt die Sekundaerseite ausschalten
               s_cnt2               <= s_cnt2 + 1;                                                       -- und abwarten
            
            elsif (s_Polarity_vorher = '1' AND s_Polarity = '0' AND s_cnt3 < tPolar_Umschaltung) then    -- IGBTs auf d. sek. Seite umpolen
               s_sec                <= "1001";                                                           -- und abwarten
               s_cnt3               <= s_cnt3 + 1; 

            elsif (s_Polarity_vorher = '1' AND s_Polarity = '0' AND s_cnt3 = tPolar_Umschaltung) then    -- Polartaetsumschaltung wurde durchgefuehrt
               s_Polarity_vorher    <= '0';                                                              -- Wert fuer Polaritaet ist jetzt geaendert
               s_wechseltakt        <= "0";                                                              
               s_cnt1               <=(others => '0');
               s_cnt2               <=(others => '0');
               s_cnt3               <=(others => '0');
               s_PolarUmschaltung   <= '0';                                                              -- Umschaltung der Polaritaet ist jetzt abgeschlossen
               PolarUmschaltung     <= '0';
               s_pwm_state          <= RESONANZ_UST_POS;
             
                                                                                                         --------------------------------------------      
            elsif (s_Polarity_vorher = '0' AND s_Polarity = '1' AND s_cnt1 < tPolar_Umschaltung) then    -- Vorher war die Polaritaet positiv
               s_PolarUmschaltung   <= '1';                                                              -- Umschaltung der Polaritaet ist gestartet
               PolarUmschaltung     <= '1';
               s_cnt1               <= s_cnt1 + 1;                                                       -- und abwarten

            elsif (s_Polarity_vorher = '0' AND s_Polarity = '1' AND s_cnt1 < tPolar_Umschaltung) then    
               s_sec                <= "0000";                                                           -- Sekundaerseite ausschalten
               s_cnt2               <= s_cnt2 + 1;                                                       -- und abwarten
            
            elsif (s_Polarity_vorher = '0' AND s_Polarity = '1' AND s_cnt3 < tPolar_Umschaltung) then    -- IGBTs auf d. sek. Seite umpolen
               s_sec                <= "0110";                                                           -- und abwarten
               s_cnt3               <= s_cnt3 + 1; 
            
            elsif (s_Polarity_vorher = '0' AND s_Polarity = '1' AND s_cnt1 = tPolar_Umschaltung) then    
               s_Polarity_vorher    <= '1';                                                              -- Wert fuer Polaritaet ist jetzt geaendert
               s_wechseltakt        <= "0";
               s_cnt1               <=(others => '0');
               s_cnt2               <=(others => '0');
               s_cnt3               <=(others => '0');
               s_PolarUmschaltung   <= '0';                                                              -- Umschaltung der Polaritaet ist abgeschlossen
               PolarUmschaltung     <= '0';
               s_pwm_state          <= RESONANZ_UST_NEG;
                                                                                                         
            
                                                                                                         --------------------------------------------  
            elsif (s_Polarity_vorher = '0' AND s_Polarity = '0' AND s_cnt1 < s_toff) then                -- kein Polaritaetswechsel erforderlich 
               s_sec                <= "1001";                                                           -- Sek.seite bleibt, Prim. Seite ist aber dabei ausgeschaltet
               s_cnt1               <= s_cnt1 + 1;
            elsif (s_Polarity_vorher = '0' AND s_Polarity = '0' AND s_cnt1 = s_toff) then
               s_Polarity_vorher    <= '0';                                                              -- nur als Info, Zuweisung eigentlich nicht erforderlich
               s_cnt1               <=(others => '0');
               s_pwm_state          <= RESONANZ_UST_POS;
                                                                                                        
            
            elsif (s_Polarity_vorher = '1' AND s_Polarity = '1' AND s_cnt1 < s_toff) then
               s_sec                <= "0110";                                                          -- Sek.seite bleibt, Prim. Seite ist aber dabei ausgeschaltet
               s_cnt1               <= s_cnt1 + 1;
            else                                                                                        -- s_Polarity_vorher = '1' AND s_Polarity = '1' s_cnt1 = s_toff)
               s_Polarity_vorher    <= '1';                                                             -- nur als Info, Zuweisung eigentlich nicht erforderlich
               s_cnt1               <=(others => '0');
               s_pwm_state          <= RESONANZ_UST_NEG;
            end if;

         ---------------------------------------------------------------------------------
         when RESONANZ_UST_POS =>                  --4.  Takt
         ---------------------------------------------------------------------------------
            if s_cnt1    < s_t1_gap     then                                                                 -- 2. Schritt (nach dem toff) S4 oder S3 wird eingeschaltet
               s_cnt1    <= s_cnt1 + 1;                                                                      -- Vorbereitung zur Energieuebertragung
               if (s_wechseltakt = "0") then      s_prim <= "0001";
               else                               s_prim <= "0100";  end if;                                 -- vorher "0010"

            elsif s_cnt2 < s_t2_gap     then                                                                 -- 3. Schritt S1+S4 oder S2+S3 werden eingeschaltet -> Energieuebertragung
               s_cnt2   <= s_cnt2 + 1;                                                                       -- Diodenunterstuetzung auf der Sekundaerseite ist noch ausgeschaltet                    
               if s_wechseltakt = "0"   then      s_prim <= "1001";
                  if (s_Symetrievorzeichen = '0') then s_ton_korr <= s_ton + s_tSymetrie;                    -- dieser Zustand soll laenger sein
                  else                                 s_ton_korr <= s_ton - s_tSymetrie; end if;            -- man geht davon aus, dass ton>>tSymetrie , somit gibt es kein neg. Wert          
               
               else                             
                                                  s_prim <= "0110";  
                  if (s_Symetrievorzeichen = '0') then s_ton_korr <= s_ton - s_tSymetrie;                    -- dieser Zustand soll kuerzer sein
                  else                                 s_ton_korr <= s_ton + s_tSymetrie; end if; 
               end if;
                                                                                                             -- 4. Schritt Diodenunterstuetzung (dadurch Rueckspeisung moeglich) sek. Seite
            elsif (s_cnt3 < s_ton_korr) then   
               s_cnt3   <= s_cnt3 + 1;                                                                       -- Die Energie wird weiterhin Uebertragen

               if (IGBT_Sek_Takt_ON = "00") then  s_sec <= "1001";                                           -- Sekundaerseite wird nicht getaktet                                                  
               elsif ((IGBT_Sek_Takt_ON = "01" )                         OR                                  -- Sekundaerseite wird immer getaktet
                      (IGBT_Sek_Takt_ON = "10" AND  s_rueckspeisung ='1')OR                                  -- Sekundaerseite wird nur beim Rueckspeisen getaktet
                      (IGBT_Sek_Takt_ON = "11" AND (s_rueckspeisung ='1' OR s_Ust_abs < Ust_switch_ON)))then -- Sekundaerseite wird beim Rueckspeisen getaktet in den Grenzen um Null 
                  if (s_wechseltakt = "0")  then  s_sec <= "1011";                                           -- Zusaetzlich zur der leitende Diode wird Transistor zugeschaltet (fuer die Rueckspeisung)
                  else                            s_sec <= "1101"; end if;                                   -- ich denke, dass die Sekundaerseite immer Takten muss, da man nicht sicher sein kann  
                                                                                                             -- wann tatsächlich die Rueckspeisung beginnt. Man musste dann auch den Strom betrachten - und wenn
               else NULL;                                                                                    -- der Strom zu klein ist - dann auch dn Transistor Takten Was gewinnt man dadurch?
               end if;                                                                                       -- besser immer takten und fertig 

            elsif (s_cnt4 < s_t3_gap)   then                                                                 -- Der Takt - Transistor (Diodenunterstuetzung/Ruecksp. der SekS. wird wieder ausgeschaltet
               s_cnt4   <= s_cnt4 + 1;                                                                       -- dabei ist die Sinuswelle bereits zu Ende
                                                  s_sec <= "1001";                                           -- Beide Transistoren der Primaerseite sind noch eingeschaltet 

            elsif (s_cnt5 < s_t4_gap)   then                                                                 -- Einer der Transistoren der Primaerseite wird abgeschaltet
               s_cnt5   <= s_cnt5 + 1;                                                                       -- (je nach wechseltakt)
               if s_wechseltakt = "0" then        s_prim <= "1000";
               else                               s_prim <= "0010";  end if;                                 -- Vorher "0100"

            else                                                                                             -- Es kann sein, dass diese Reihnfolge beim Rueckspeisen nicht optimal ist und das man diese aendern muss
                                                  s_prim <= "0000";
               s_cnt1               <=(others => '0');                                                       -- Alle Zaehler auf Null setzen
               s_cnt2               <=(others => '0');
               s_cnt3               <=(others => '0');
               s_cnt4               <=(others => '0');
               s_cnt5               <=(others => '0');
                           
               s_wechseltakt        <= s_wechseltakt + 1;
               pwm_Data_Out_Valid   <='1';
               s_pwm_state          <= EINGANG_SPEICHERN;

            end if;

        ---------------------------------------------------------------------------------
      when RESONANZ_UST_NEG =>                  --4.  Takt 
      ---------------------------------------------------------------------------------
         if (s_cnt1      < s_t1_gap)     then                                                            -- 10  2. Schritt S4 oder S3 wird eingeschaltet
               s_cnt1    <= s_cnt1 + 1;
               if s_wechseltakt = "0"    then   s_prim <= "0001";
               else                             s_prim <= "0100";  end if;                               -- Vorher "0010"

         elsif (s_cnt2 < s_t2_gap)       then                                                             -- 70  3. Schritt S1+S4  oder S2+S3 werden eingeschaltet
            s_cnt2   <= s_cnt2 + 1;
            if (s_wechseltakt = "0")     then    
                                                s_prim <= "1001";
               if (s_Symetrievorzeichen = '0') then s_ton_korr <= s_ton + s_tSymetrie;                    -- dieser Zustand soll laenger sein
               else                                 s_ton_korr <= s_ton - s_tSymetrie; end if;            -- es kann sein, dass bei neg. Steuerspannung hier der Vorzeichen gedreht werden muss
            else                            
                                                s_prim <= "0110";
               if (s_Symetrievorzeichen = '0') then s_ton_korr <= s_ton - s_tSymetrie;                    -- dieser Zustand soll kuerzer sein
               else                                 s_ton_korr <= s_ton + s_tSymetrie; end if; 
            end if;
             
         elsif (s_cnt3 < s_ton_korr )    then                                                             -- 1750 4. Schritt
            s_cnt3   <= s_cnt3 + 1;
            if (IGBT_Sek_Takt_ON = "00") then  s_sec <= "0110";                                           -- Sekundaerseite wird nicht getaktet                                                          
            elsif ((IGBT_Sek_Takt_ON = "01" )                         OR                                  -- Sekundaerseite wird immer getaktet
                   (IGBT_Sek_Takt_ON = "10" AND s_rueckspeisung ='1') OR                                  -- Sekundaerseite wird nur beim Rueckspeisen getaktet
                   (IGBT_Sek_Takt_ON = "11" AND (s_rueckspeisung ='1' OR s_Ust_abs < Ust_switch_ON)))then -- Sekundaerseite wird beim Rueckspeisen getaktet in den Grenzen um Null 

               if (s_wechseltakt = "0")  then  s_sec <= "0111";                                           -- Zusaetzlich zur der leitende Diode wird Transistor zugeschaltet (fuer die Rueckspeisung)
               else                            s_sec <= "1110"; end if;
            
            else NULL;  
            end if;

         elsif s_cnt4 < s_t3_gap    then                                                                  -- Takt - Transistor der Sek Seite wird wieder ausgeschaltet
            s_cnt4   <= s_cnt4 + 1;
                                               s_sec <= "0110";

         elsif s_cnt5 < s_t4_gap     then
            s_cnt5   <= s_cnt5 + 1;
            if (s_wechseltakt = "0") then      s_prim <= "1000";
            else                               s_prim <= "0010"; end if;                                  -- Vorher "0100"

         else
                                               s_prim <= "0000";
            s_cnt1               <=(others => '0');                                                       -- Alle Zaehler auf Null setzen
            s_cnt2               <=(others => '0');
            s_cnt3               <=(others => '0');
            s_cnt4               <=(others => '0');
            s_cnt5               <=(others => '0');

            s_wechseltakt        <= s_wechseltakt + 1;
            pwm_Data_Out_Valid   <='1';
            s_pwm_state          <= EINGANG_SPEICHERN;

         end if;
      ---------------------------------------------------------------------------------
         when others =>
      ---------------------------------------------------------------------------------
            s_pwm_state <= EINGANG_SPEICHERN;

            end case;
         end if;                     --clk
      end if;                        --reset

   end process Resonanz;

end architecture behavioral;
