-- Beschreibung:
-- Diese VHDL Datei berechnet Umkerfunktion der Transistorkennlinie

-- Der Block implementiert folgende Gleichung:
-- Ausgang = b0(k) + (y(k+1)-y(k))*(Iph-IStuetz(k)*m(k)
-- m entspricht der Steigung

-- Eing. Groessen:
-- reset Berechnung neu starten, alles auf Null setzen
-- Enable symbolisiert Gueltigkeit der Eingangsdaten

-- Iph      SIGNED 16 Bit  130A^=2^15
-- yPunkte  SIGNED 16 Bit
-- Ausg. Groessen:
-- Uout_ph SIGNED 16Bit:  Signed nicht noetig, aber macht die Verbindung zum Vorsteuerungsblock einfacher, da dort alle Variablen 16Bit Signed sind.
--========================================================================

LIBRARY ieee;
USE      ieee.std_logic_1164.ALL;
USE      ieee.numeric_std.ALL;

ENTITY IGBT_Saturation_PWM IS
   GENERIC (
      takt                : INTEGER     := 100_000_000 );                  -- Initialisierung 100 MHz

   PORT (
      clk, reset, Enable  : IN  STD_LOGIC;                                 -- System-Eingaenge
      Iph1                : IN  SIGNED    (15 DOWNTO 0):= (OTHERS => '0');
      Iph2                : IN  SIGNED    (15 DOWNTO 0):= (OTHERS => '0');
      Iph3                : IN  SIGNED    (15 DOWNTO 0):= (OTHERS => '0');

      Faktor              : IN  SIGNED    (15 DOWNTO 0):= (OTHERS => '0'); -- Multiplikationsfaktor
      Vorzeichen          : IN  STD_LOGIC;

      Uout_ph1_TT         : out signed(15 downto 0)    := (others => '0'); -- Generated Transistor Voltage
      Uout_ph2_TT         : out signed(15 downto 0)    := (others => '0');
      Uout_ph3_TT         : out signed(15 downto 0)    := (others => '0');

      Uout_ph1_DD         : out signed(15 downto 0)    := (others => '0'); -- Generated Transistor Voltage
      Uout_ph2_DD         : out signed(15 downto 0)    := (others => '0');
      Uout_ph3_DD         : out signed(15 downto 0)    := (others => '0');

      Uout_ph1_DT         : out signed(15 downto 0)    := (others => '0'); -- Generated Transistor Voltage
      Uout_ph2_DT         : out signed(15 downto 0)    := (others => '0');
      Uout_ph3_DT         : out signed(15 downto 0)    := (others => '0');

      Iph1_sign           : out std_logic              := '0';
      Iph2_sign           : out std_logic              := '0';
      Iph3_sign           : out std_logic              := '0';

      Data_Out_Valid      : OUT STD_LOGIC              := '0'   );
   END IGBT_Saturation_PWM;

   ARCHITECTURE behavioral OF IGBT_Saturation_PWM IS
      TYPE yStuetzstellen IS ARRAY(0 TO 45) OF UNSIGNED(14 DOWNTO 0);     -- Vorsteuerung
      TYPE xStuetzstellen IS ARRAY(0 TO 45) OF SIGNED  (15 DOWNTO 0);     -- Sollwert

      CONSTANT yPunkteTransistor   : yStuetzstellen := (                            -- Punkte werden ausgemessen
         to_unsigned ( 0, 15),
         to_unsigned ( 32, 15),
         to_unsigned ( 38, 15),
         to_unsigned ( 42, 15),
         to_unsigned ( 45, 15),
         to_unsigned ( 47, 15),
         to_unsigned ( 49, 15),
         to_unsigned ( 51, 15),
         to_unsigned ( 53, 15),
         to_unsigned ( 58, 15),
         to_unsigned ( 62, 15),
         to_unsigned ( 65, 15),
         to_unsigned ( 68, 15),
         to_unsigned ( 71, 15),
         to_unsigned ( 73, 15),
         to_unsigned ( 85, 15),
         to_unsigned ( 93, 15),
         to_unsigned ( 98, 15),
         to_unsigned ( 103, 15),
         to_unsigned ( 107, 15),
         to_unsigned ( 110, 15),
         to_unsigned ( 113, 15),
         to_unsigned ( 115, 15),
         to_unsigned ( 118, 15),
         to_unsigned ( 120, 15),
         to_unsigned ( 121, 15),
         to_unsigned ( 123, 15),
         to_unsigned ( 125, 15),
         to_unsigned ( 126, 15),
         to_unsigned ( 128, 15),
         to_unsigned ( 129, 15),
         to_unsigned ( 131, 15),
         to_unsigned ( 133, 15),
         to_unsigned ( 135, 15),
         to_unsigned ( 137, 15),
         to_unsigned ( 140, 15),
         to_unsigned ( 142, 15),
         to_unsigned ( 143, 15),
         to_unsigned ( 147, 15),
         to_unsigned ( 151, 15),
         to_unsigned ( 155, 15),
         to_unsigned ( 158, 15),
         to_unsigned ( 162, 15),
         to_unsigned ( 166, 15),
         to_unsigned ( 170, 15),
         to_unsigned ( 174, 15));

      CONSTANT yPunkteDiode   : yStuetzstellen := (                            -- Punkte werden ausgemessen
         to_unsigned ( 0, 15),
         to_unsigned ( 39, 15),
         to_unsigned ( 46, 15),
         to_unsigned ( 51, 15),
         to_unsigned ( 55, 15),
         to_unsigned ( 58, 15),
         to_unsigned ( 60, 15),
         to_unsigned ( 63, 15),
         to_unsigned ( 65, 15),
         to_unsigned ( 71, 15),
         to_unsigned ( 76, 15),
         to_unsigned ( 80, 15),
         to_unsigned ( 84, 15),
         to_unsigned ( 87, 15),
         to_unsigned ( 89, 15),
         to_unsigned ( 104, 15),
         to_unsigned ( 113, 15),
         to_unsigned ( 120, 15),
         to_unsigned ( 125, 15),
         to_unsigned ( 129, 15),
         to_unsigned ( 133, 15),
         to_unsigned ( 136, 15),
         to_unsigned ( 139, 15),
         to_unsigned ( 141, 15),
         to_unsigned ( 143, 15),
         to_unsigned ( 146, 15),
         to_unsigned ( 147, 15),
         to_unsigned ( 149, 15),
         to_unsigned ( 151, 15),
         to_unsigned ( 152, 15),
         to_unsigned ( 154, 15),
         to_unsigned ( 155, 15),
         to_unsigned ( 158, 15),
         to_unsigned ( 160, 15),
         to_unsigned ( 162, 15),
         to_unsigned ( 164, 15),
         to_unsigned ( 166, 15),
         to_unsigned ( 168, 15),
         to_unsigned ( 172, 15),
         to_unsigned ( 176, 15),
         to_unsigned ( 180, 15),
         to_unsigned ( 184, 15),
         to_unsigned ( 188, 15),
         to_unsigned ( 192, 15),
         to_unsigned ( 196, 15),
         to_unsigned ( 201, 15));

      CONSTANT xPunkte   : xStuetzstellen := (
         to_signed ( 0,    16),
         to_signed ( 16,   16),
         to_signed ( 32,   16),
         to_signed ( 48,   16),
         to_signed ( 64,   16),
         to_signed ( 80,   16),
         to_signed ( 96,   16),
         to_signed ( 112,  16),

         to_signed ( 128,  16),
         to_signed ( 192,  16),
         to_signed ( 256,  16),
         to_signed ( 320,  16),
         to_signed ( 384,  16),
         to_signed ( 448,  16),

         to_signed ( 512,  16),
         to_signed ( 1024, 16),
         to_signed ( 1536, 16),
         to_signed ( 2048, 16),
         to_signed ( 2560, 16),
         to_signed ( 3072, 16),
         to_signed ( 3584, 16),
         to_signed ( 4096, 16),
         to_signed ( 4608, 16),
         to_signed ( 5120, 16),
         to_signed ( 5632, 16),
         to_signed ( 6144, 16),
         to_signed ( 6656, 16),
         to_signed ( 7168, 16),
         to_signed ( 7680, 16),
         to_signed ( 8192, 16),
         to_signed ( 8704, 16),
         to_signed ( 9216, 16),

         to_signed ( 10240,16),
         to_signed ( 11264,16),
         to_signed ( 12288,16),
         to_signed ( 13312,16),
         to_signed ( 14336,16),
         to_signed ( 15360,16),

         to_signed ( 17408,16),
         to_signed ( 19456,16),
         to_signed ( 21504,16),
         to_signed ( 23552,16),
         to_signed ( 25600,16),
         to_signed ( 27648,16),
         to_signed ( 29696,16),
         to_signed ( 31744,16));


      SIGNAL s_absIph        : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_absIph1       : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_absIph2       : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_absIph3       : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');

      ------------------------------------------------------------------------
      SIGNAL s_Uout_ph1_T    : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uout_ph1_TT   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uout_ph1_DD   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uout_ph1_DT   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      
      SIGNAL s_Uout_ph2_T    : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uout_ph2_TT   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uout_ph2_DD   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uout_ph2_DT   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      
      SIGNAL s_Uout_ph3_T    : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uout_ph3_TT   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uout_ph3_DD   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uout_ph3_DT   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      
     
      SIGNAL s_yPunkte       : yStuetzstellen       := yPunkteTransistor;
      SIGNAL s_Summe         : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');

      SIGNAL s_b0            : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_deltaY        : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL sIsoll_diff     : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');

      SIGNAL s_Mult1         : SIGNED(31 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Mult_Input1   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Mult_Input2   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Ergebnis      : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      
      SIGNAL s_SignPh1       : STD_LOGIC            := '0';
      SIGNAL s_SignPh2       : STD_LOGIC            := '0';
      SIGNAL s_SignPh3       : STD_LOGIC            := '0';

      SIGNAL s_kBereich      : INTEGER RANGE 0 TO 43:= 0;

      TYPE T_STATE_BERECHNUNG IS(EINGANG_SPEICHERN, --Zustaende---------
                                 KENNLINIE_AUSWAEHLEN,
                                 BEREICH_BESTIMMEN,
                                 GERADE_BESTIMMEN,
                                 MULT1_VORB, MULT1,
                                 ADDITION,
                                 MULT2_VORB,MULT2,
                                 ENDERGEBNIS_BERECHNEN,
                                 DATA_VORBEREITEN,
                                 DATA_AUSGEBEN);
      SIGNAL s_STATE : T_STATE_BERECHNUNG := EINGANG_SPEICHERN;

      TYPE Phase_typ is (phase1_T, phase1_D,
                         phase2_T, phase2_D,
                         phase3_T, phase3_D);  --Model state machines
      SIGNAL s_phase : Phase_typ:= phase1_T ;

   BEGIN
      berechnung : PROCESS (clk, reset, Enable)
      BEGIN

         IF (reset = '1') THEN
            s_phase         <= phase1_T;
            Uout_ph1_TT     <= (OTHERS => '0');
            Uout_ph2_TT     <= (OTHERS => '0');
            Uout_ph3_TT     <= (OTHERS => '0');

            Uout_ph1_DD     <= (OTHERS => '0');
            Uout_ph2_DD     <= (OTHERS => '0');
            Uout_ph3_DD     <= (OTHERS => '0');

            Uout_ph1_DT     <= (OTHERS => '0');
            Uout_ph2_DT     <= (OTHERS => '0');
            Uout_ph3_DT     <= (OTHERS => '0');

            s_yPunkte       <= yPunkteTransistor;
            Data_Out_Valid  <= '0';
            s_STATE         <= EINGANG_SPEICHERN;

         ELSE
            IF rising_edge(clk) THEN   -- ggf. Outputs zuweisen
               CASE s_STATE IS
                  ------------------------------------------------------------------
                  WHEN EINGANG_SPEICHERN => --1.Takt
                  ------------------------------------------------------------------
                     IF (Enable = '1') THEN
                        IF Iph1(Iph1'LEFT) = '1' then s_SignPh1   <='1';
                        ELSE                          s_SignPh1   <='0';
                        END IF;

                        IF       Iph1 < -32766 then   s_absIph1 <= to_signed(32767,16);
                        ELSIF    Iph1 >  32766 then   s_absIph1 <= to_signed(32767,16);
                        ELSE                          s_absIph1 <= ABS(Iph1);
                        END IF;

                        ---------------------------------------------------------
                        IF Iph2(Iph2'LEFT) = '1' then s_SignPh2   <='1';
                        ELSE                          s_SignPh2   <='0';
                        END IF;

                        IF      Iph2 < -32766 then    s_absIph2 <= to_signed(32767,16);
                        ELSIF   Iph1 >  32766 then    s_absIph2 <= to_signed(32767,16);
                        ELSE                          s_absIph2 <= ABS(Iph2);
                        END IF;

                        ---------------------------------------------------------
                        IF Iph3(Iph3'LEFT) = '1' then s_SignPh3   <='1';
                        ELSE                          s_SignPh3   <='0';
                        END IF;

                        IF       Iph3 < -32766 then   s_absIph3 <= to_signed(32767,16);
                        ELSIF    Iph3 >  32766 then   s_absIph3 <= to_signed(32767,16);
                        ELSE                          s_absIph3 <= ABS(Iph3);
                        END IF;

                        s_STATE <= KENNLINIE_AUSWAEHLEN;
                     END IF;
                     Data_Out_Valid <= '0';

                  ------------------------------------------------------------------
                  WHEN KENNLINIE_AUSWAEHLEN => --2.Takt
                  ------------------------------------------------------------------
                    CASE s_phase IS
                     when phase1_T | phase2_T | phase3_T => s_yPunkte <= yPunkteTransistor;
                     when phase1_D | phase2_D | phase3_D => s_yPunkte <= yPunkteDiode;
                    END CASE;

                    CASE s_phase IS
                     when phase1_T | phase1_D  => s_absIph  <= s_absIph1;
                     when phase2_T | phase2_D  => s_absIph  <= s_absIph2;
                     when phase3_T | phase3_D  => s_absIph  <= s_absIph3;
                    END CASE;
                    s_STATE <= BEREICH_BESTIMMEN;

                  ------------------------------------------------------------------
                  WHEN BEREICH_BESTIMMEN => --3.Takt
                  ------------------------------------------------------------------
                     -- Bestimmt den Bereich in Abhaengigkeit der x-Achsen Stützpunkte
                     IF       s_absIph > xPunkte(43) THEN   s_kBereich <= 43;
                     ELSIF    s_absIph > xPunkte(42) THEN   s_kBereich <= 42;
                     ELSIF    s_absIph > xPunkte(41) THEN   s_kBereich <= 41;
                     ELSIF    s_absIph > xPunkte(40) THEN   s_kBereich <= 40;
                     ELSIF    s_absIph > xPunkte(39) THEN   s_kBereich <= 39;
                     ELSIF    s_absIph > xPunkte(38) THEN   s_kBereich <= 38;

                     ELSIF    s_absIph > xPunkte(37) THEN   s_kBereich <= 37;
                     ELSIF    s_absIph > xPunkte(36) THEN   s_kBereich <= 36;
                     ELSIF    s_absIph > xPunkte(35) THEN   s_kBereich <= 35;
                     ELSIF    s_absIph > xPunkte(34) THEN   s_kBereich <= 34;
                     ELSIF    s_absIph > xPunkte(33) THEN   s_kBereich <= 33;
                     ELSIF    s_absIph > xPunkte(32) THEN   s_kBereich <= 32;

                     ELSIF    s_absIph > xPunkte(31) THEN   s_kBereich <= 31;
                     ELSIF    s_absIph > xPunkte(30) THEN   s_kBereich <= 30;
                     ELSIF    s_absIph > xPunkte(29) THEN   s_kBereich <= 29;
                     ELSIF    s_absIph > xPunkte(28) THEN   s_kBereich <= 28;
                     ELSIF    s_absIph > xPunkte(27) THEN   s_kBereich <= 27;
                     ELSIF    s_absIph > xPunkte(26) THEN   s_kBereich <= 26;
                     ELSIF    s_absIph > xPunkte(25) THEN   s_kBereich <= 25;
                     ELSIF    s_absIph > xPunkte(24) THEN   s_kBereich <= 24;
                     ELSIF    s_absIph > xPunkte(23) THEN   s_kBereich <= 23;
                     ELSIF    s_absIph > xPunkte(22) THEN   s_kBereich <= 22;
                     ELSIF    s_absIph > xPunkte(21) THEN   s_kBereich <= 21;
                     ELSIF    s_absIph > xPunkte(20) THEN   s_kBereich <= 20;
                     ELSIF    s_absIph > xPunkte(19) THEN   s_kBereich <= 19;
                     ELSIF    s_absIph > xPunkte(18) THEN   s_kBereich <= 18;
                     ELSIF    s_absIph > xPunkte(17) THEN   s_kBereich <= 17;
                     ELSIF    s_absIph > xPunkte(16) THEN   s_kBereich <= 16;
                     ELSIF    s_absIph > xPunkte(15) THEN   s_kBereich <= 15;
                     ELSIF    s_absIph > xPunkte(14) THEN   s_kBereich <= 14;

                     ELSIF    s_absIph > xPunkte(13) THEN   s_kBereich <= 13;
                     ELSIF    s_absIph > xPunkte(12) THEN   s_kBereich <= 12;
                     ELSIF    s_absIph > xPunkte(11) THEN   s_kBereich <= 11;
                     ELSIF    s_absIph > xPunkte(10) THEN   s_kBereich <= 10;
                     ELSIF    s_absIph > xPunkte(9)  THEN   s_kBereich <= 9 ;
                     ELSIF    s_absIph > xPunkte(8)  THEN   s_kBereich <= 8 ;

                     ELSIF    s_absIph > xPunkte(7)  THEN   s_kBereich <= 7 ;
                     ELSIF    s_absIph > xPunkte(6)  THEN   s_kBereich <= 6 ;
                     ELSIF    s_absIph > xPunkte(5)  THEN   s_kBereich <= 5 ;
                     ELSIF    s_absIph > xPunkte(4)  THEN   s_kBereich <= 4 ;
                     ELSIF    s_absIph > xPunkte(3)  THEN   s_kBereich <= 3 ;
                     ELSIF    s_absIph > xPunkte(2)  THEN   s_kBereich <= 2 ;
                     ELSIF    s_absIph > xPunkte(1)  THEN   s_kBereich <= 1 ;
                     ELSE                                   s_kBereich <= 0 ;
                     END IF;
                     s_STATE <= GERADE_BESTIMMEN;

                  ------------------------------------------------------------------
                  WHEN GERADE_BESTIMMEN => --3.Takt
                  ------------------------------------------------------------------                  -- Y-Werte fuer den Transistorumkehrfunktion
                     s_b0           <= signed('0' & std_LOGIC_VECTOR(s_yPunkte(s_kBereich)));           -- Ermittlung des Wertes b0 aus dem aktuellen Bereich (Wert der unteren Stütze)
                     s_deltaY       <= resize(signed('0' & std_LOGIC_VECTOR(s_yPunkte(s_kBereich + 1)))
                                      - signed('0' & std_LOGIC_VECTOR(s_yPunkte(s_kBereich))),16);      -- Differenz der y-Achse zwischen den beiden Stützstellen (y2-y1)
                     sIsoll_diff    <= s_absIph - xPunkte(s_kBereich);                                  -- Differenz des Stromes zur Stuetzstelle
                     s_STATE        <= MULT1_VORB;

                  ------------------------------------------------------------------
                   WHEN MULT1_VORB => --3.Takt
                  ------------------------------------------------------------------
                     s_Mult_Input1  <= s_deltaY;                         -- Differenz des Stromes zur Stuetzstelle
                     s_Mult_Input2  <= sIsoll_diff;
                     s_STATE        <= MULT1;

                  ------------------------------------------------------------------
                  WHEN MULT1 => --4.Takt
                  ------------------------------------------------------------------
                     s_Mult1        <= s_Mult_Input1 * s_Mult_Input2;    -- 16Bit *16 Bit
                     s_STATE        <= ADDITION;

                  ------------------------------------------------------------------
                  WHEN ADDITION => --5.Takt
                  ------------------------------------------------------------------
                     IF    (s_kBereich > 36) THEN   s_Summe <= s_b0 + s_Mult1(31-1-4  downto 15-4 ); -- 10Bit *16 Bit
                     ELSIF (s_kBereich > 30) THEN   s_Summe <= s_b0 + s_Mult1(31-1-5  downto 15-5 ); -- 10Bit *16 Bit
                     ELSIF (s_kBereich > 13) THEN   s_Summe <= s_b0 + s_Mult1(31-1-6  downto 15-6 ); -- 10Bit *16 Bit
                     ELSIF (s_kBereich > 7 ) THEN   s_Summe <= s_b0 + s_Mult1(31-1-9  downto 15-9 ); -- 10Bit *16 Bit
                     ELSE                           s_Summe <= s_b0 + s_Mult1(31-1-11 downto 15-11); -- 10Bit *16 Bit
                     END IF;
                     s_STATE        <= MULT2_VORB;

                  ------------------------------------------------------------------
                   WHEN MULT2_VORB => --4.Takt
                  ------------------------------------------------------------------
                     s_Mult_Input1  <= Faktor;
                     s_Mult_Input2  <= s_Summe;
                     s_STATE        <= MULT2;

                  ------------------------------------------------------------------
                   WHEN MULT2 => --4.Takt
                  ------------------------------------------------------------------
                     s_Mult1        <= s_Mult_Input1 * s_Mult_Input2; -- 10Bit *16 Bit
                     s_STATE        <= ENDERGEBNIS_BERECHNEN;

                  ------------------------------------------------------------------
                   WHEN ENDERGEBNIS_BERECHNEN => --4.Takt
                  ------------------------------------------------------------------
                     s_Ergebnis     <= s_Mult1(30 downto 15);
                     s_STATE        <= DATA_VORBEREITEN;

                  ------------------------------------------------------------------
                   WHEN DATA_VORBEREITEN => --4.Takt
                  ------------------------------------------------------------------
                     IF (s_phase = phase1_T) then
                        s_Uout_ph1_T   <= s_Ergebnis;                        --  UCE
                        s_Uout_ph1_TT  <= s_Ergebnis(15 downto 1) & '0';     --  UCE* 2
                        s_phase        <= phase1_D;
                        s_STATE        <= KENNLINIE_AUSWAEHLEN ;

                     ELSIF (s_phase = phase1_D) then
                        s_Uout_ph1_DD  <= s_Ergebnis(15 downto 1) & '0';     --  Diode* 2
                        s_Uout_ph1_DT  <= s_Uout_ph1_T + s_Ergebnis;         --  D + T
                        s_phase        <= phase2_T;
                        s_STATE        <= KENNLINIE_AUSWAEHLEN ;

                     ELSIF (s_phase = phase2_T) then
                        s_Uout_ph2_T   <= s_Ergebnis;                        
                        s_Uout_ph2_TT  <= s_Ergebnis(15 downto 1) & '0';    
                        s_phase        <= phase2_D;
                        s_STATE        <= KENNLINIE_AUSWAEHLEN ;

                     ELSIF (s_phase = phase2_D) then
                        s_Uout_ph2_DD  <= s_Ergebnis(15 downto 1) & '0';     
                        s_Uout_ph2_DT  <= s_Uout_ph2_T + s_Ergebnis;
                        s_phase        <= phase3_T;
                        s_STATE        <= KENNLINIE_AUSWAEHLEN ;

                     ELSIF (s_phase = phase3_T) then
                        s_Uout_ph3_T   <= s_Ergebnis;                       
                        s_Uout_ph3_TT  <= s_Ergebnis(15 downto 1) & '0';    
                        s_phase        <= phase3_D;
                        s_STATE        <= KENNLINIE_AUSWAEHLEN ;

                     ELSIF (s_phase = phase3_D) then
                        s_Uout_ph3_DD  <= s_Ergebnis(15 downto 1) & '0';    
                        s_Uout_ph3_DT  <= s_Uout_ph3_T + s_Ergebnis;
                        s_phase        <= phase1_T;
                        s_STATE        <= DATA_AUSGEBEN ;
                     END IF;

                  ---------------------------------------------------------------------------------
                  when DATA_AUSGEBEN =>     --7. Takt
                  ---------------------------------------------------------------------------------
                  if Vorzeichen = '0' then
                     Uout_ph1_TT    <= s_Uout_ph1_TT;
                     Uout_ph1_DD    <= s_Uout_ph1_DD;
                     Uout_ph1_DT    <= s_Uout_ph1_DT;

                     Uout_ph2_TT    <= s_Uout_ph2_TT;
                     Uout_ph2_DD    <= s_Uout_ph2_DD;
                     Uout_ph2_DT    <= s_Uout_ph2_DT;

                     Uout_ph3_TT    <= s_Uout_ph3_TT;
                     Uout_ph3_DD    <= s_Uout_ph3_DD;
                     Uout_ph3_DT    <= s_Uout_ph3_DT;
                  else
                     Uout_ph1_TT    <= NOT(s_Uout_ph1_TT) + 1;
                     Uout_ph1_DD    <= NOT(s_Uout_ph1_DD) + 1;
                     Uout_ph1_DT    <= NOT(s_Uout_ph1_DT) + 1;

                     Uout_ph2_TT    <= NOT(s_Uout_ph2_TT) + 1;
                     Uout_ph2_DD    <= NOT(s_Uout_ph2_DD) + 1;
                     Uout_ph2_DT    <= NOT(s_Uout_ph2_DT) + 1;

                     Uout_ph3_TT    <= NOT(s_Uout_ph3_TT) + 1;
                     Uout_ph3_DD    <= NOT(s_Uout_ph3_DD) + 1;
                     Uout_ph3_DT    <= NOT(s_Uout_ph3_DT) + 1;
                  end if;
 
                     Iph1_sign      <= NOT(s_SignPh1);
                     Iph2_sign      <= NOT(s_SignPh2);
                     Iph3_sign      <= NOT(s_SignPh3);

                     Data_Out_Valid <= '1';
                     s_STATE        <= EINGANG_SPEICHERN ;

                  ---------------------------------------------------------------------------------
                  WHEN OTHERS =>
                  ----------------------------------------------------------------------------------
                     s_STATE <= EINGANG_SPEICHERN;

               END CASE;
            END IF;
         END IF;
      END PROCESS berechnung;

END ARCHITECTURE behavioral;
