-----------------------------------------------------------------------------------
-- File name: dr_i2c.vhd                                                         --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 04/05/2018                                                         --
--                                                                               --
-- Comments : generic 2 wires i2c                                                -- 
--                                                                               --
-- History  : Start up version 04/05/2018                                        --
-----------------------------------------------------------------------------------
--            Added IncomingStartRd signal: The read action is usually made of 2 --
--            steps                                                              --
--                * Write the address to read                                    --
--                * Send the read command                                        --
--            In case the write address is also triggering a measuring action,   --
--            it could be necessary to wait till the read action is complete     --
--            before accessing the register.                                     --
--            With IncomingStartRd it is known when the start read protocol is   --
--            going to be generated and acting on StartEn signal, it is possible --
--            to freeze the dr_i2c for a time configurable on the wrap module.   --
--            The IncomingStartRd signal can be ignored on the wrap module if it --
--            is not usefull. DR 01/02/2021                                      --
-----------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity dr_i2c is
generic  
   (
      gCtrlByte                  : std_logic_vector(7 downto 0):= x"A0";       -- it defines the i2c chip  
      gSCLKPeriodIn_ns           : integer range 20 to 10000:=500              -- it defines the SCLK period. 
                                                                               -- min=20 ns; max 100 us; 50% duty cycle with Clock=100MHz
									       
         );
  Port (
         Clock                   : in std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in std_logic;                               -- Asynchronous reset signal active high
         RdWr_l                  : in std_logic;                               -- command type 1= RD; 0= WR.
         Addr_Dt_2_WR            : in std_logic_vector(7 downto 0);            -- Start address/Data to write
         StartEn                 : in std_logic;                               -- Start enable command
									       
         DataWordToProcess       : in std_logic_vector(8 downto 0);            -- it defines the number of data words to RD/WR each i2c cycle:
                                                                               --     RD => min=4; max=259
                                                                               --     WR => min 3; max=10
									       
         RdData                  : out std_logic_vector(7 downto 0);           -- Read data
         NewRdData               : out std_logic;                              -- New read data pulse.
         SDA_dir                 : out std_logic;                              -- for debug only
         Dt2WrReq                : out std_logic;                              -- in Wr mode, it asks for new data to write.
         EndProtocol             : out std_logic;                              -- it is a pulse active high when the stop command is sent
	 
         IncomingStartRd         : out std_logic;                              -- It is a pulse active high when a read protocol is going to start
	 
         -- i2c pins
         SCLK                    : out std_logic;                              -- i2c clock signal
         SDA                     : inout std_logic                             -- bidirectional data line
  );
End entity dr_i2c;

Architecture beh of dr_i2c is
--
-- Constants declaration
constant SCLKPeriod             : unsigned(15 downto 0):=to_unsigned(gSCLKPeriodIn_ns,16);
constant SCLK_H_Period          : unsigned(15 downto 0):=to_unsigned(gSCLKPeriodIn_ns/2,16);
constant SCLK_Q_Period          : unsigned(15 downto 0):=to_unsigned(gSCLKPeriodIn_ns/4,16);
constant SCLK_3Q_Period         : unsigned(15 downto 0):=to_unsigned((gSCLKPeriodIn_ns/4)*3,16);


--
-- Signals declaration
signal SDA_out                  : std_logic;                               -- SDA line with master controller
signal SDA_direction            : std_logic;                               -- 0=> wr; 1=rd

signal intDataReg               : std_logic_vector(7 downto 0);            -- internal data register

signal RdWr_l_s0                : std_logic;				                     -- RdWr_l sampled signal
signal Addr_Dt_2_WR_s0          : std_logic_vector(7 downto 0);            -- Addr_Dt_2_WR sampled signal
signal StartEn_s0               : std_logic;                               -- StartEn sampled signal

signal cnt_SCLK                 : unsigned(15 downto 0);                   -- SCLK period counter

signal cnt_Byte                 : unsigned(2 downto 0);                    -- Byte (8 bit) counter

signal cntWord                  : unsigned(8 downto 0);                    -- words to read/write counter

type fsmstate is (SetStart, ManageByte, ManageACK, SetNACK, SetStop, NACK_Alarm);
signal fsm_state                : fsmstate;

signal NACK_Alrm                : std_logic;                               -- it is high when a NACK is received
signal DataWordToProcess_s0     : unsigned(8 downto 0);                    -- DataWordToProcess sampled signal
signal cnt_StStp                : unsigned(15 downto 0);                   -- it counts the start and stop status lenght
signal maskedRdWr_l             : std_logic;                               -- in order to perform a sequential rd action, a wr action has to be performed before.
                                                                           -- for the first 2 "ManageByte loops" a write action is forced and after that,
                                                                           -- RdWr_l_s0 will take back the control
signal stopOutFlag              : std_logic;                               -- it is high when the stop condition is generated

begin


--
-- Inputs sampling process
p_InSamp: process (Reset,Clock)
begin
  if (Reset='1') then
    RdWr_l_s0             <= '0';
    Addr_Dt_2_WR_s0       <= (others=>'0');
    StartEn_s0            <= '0';
    DataWordToProcess_s0  <= (others=>'0');
  elsif (Clock'event and Clock='1') then
    RdWr_l_s0             <= RdWr_l;
    Addr_Dt_2_WR_s0       <= Addr_Dt_2_WR;
    StartEn_s0            <= StartEn;
    DataWordToProcess_s0  <= unsigned(DataWordToProcess);
  end if;
end process;


--
-- SCLK process
p_SCLKgen: process (Reset,Clock)
begin
  if (Reset='1') then
    cnt_SCLK    <= (others=>'0');
    SCLK        <= '1';
    
  elsif (Clock'event and Clock='1') then
  
    if (StartEn_s0='1') then
    
      if (cnt_SCLK < SCLKPeriod-1) then
        cnt_SCLK  <= cnt_SCLK + 1;
      else
        cnt_SCLK  <= (others=>'0');
      end if;
      
--      if (cnt_SCLK < SCLK_H_Period-1) then
      if (cnt_SCLK < SCLK_H_Period-1 or stopOutFlag = '1') then
        SCLK      <= '1';
      else
        SCLK      <= '0';
      end if;
    else
      cnt_SCLK    <= (others=>'0');
      SCLK        <= '1';      
    end if;
    
  end if;
end process;

--
-- FSM Process
pfsm: process (Reset,Clock)
begin
  if (Reset='1') then
    -- State evolution
    fsm_state <= SetStart;
    
    -- Signals evolution
    SDA_direction    <= '0';
    SDA_out          <= '1';
    cnt_Byte         <= (others=>'1');
    intDataReg       <= (others=>'0');
    RdData           <= (others=>'0');
    NewRdData        <= '0';
    cntWord          <= (others=>'0');
    NACK_Alrm        <= '0';
    Dt2WrReq         <= '0';
    cnt_StStp        <= (others=>'0');
    EndProtocol      <= '0';
    maskedRdWr_l     <= '0';
    
    stopOutFlag      <= '0';
    IncomingStartRd  <= '0';
    
  elsif (Clock'event and Clock='1') then
  
    case fsm_state is
  
      when SetStart =>
        -- Signals evolution
        EndProtocol  <= '0';
        cnt_Byte     <= (others=>'1');
        NACK_Alrm    <= '0';
        Dt2WrReq     <= '0';
	
        -- Check this!
        SDA_out      <= '1';
        stopOutFlag  <= '0';
	
	if (cntWord = "000000010" and cnt_SCLK = SCLKPeriod-6 and RdWr_l_s0='1') then
	  IncomingStartRd  <= '1';
	else
	  IncomingStartRd  <= '0';
	end if;
	
        --
        -- Start stop counter evolution
        if (StartEn_s0='1') then
          if (cnt_StStp < SCLKPeriod-1 ) then
            cnt_StStp  <= cnt_StStp + 1;
          else
            cnt_StStp  <= (others=>'0');
          end if;
        else
	       cnt_StStp  <= (others=>'0');
        end if;
	
        --
        -- Start condition out
--        if (cntWord = 0) then
          -- common start condition at the beginning of the protocol
          if (cnt_StStp < SCLK_Q_Period - 1) then
            SDA_out  <= '1';
          else
            SDA_out  <= '0';
          end if;
--        else                                           -- to manage properly a start condition after receiving back an ACK
--	       -- start condition for sequential read protocol
--          if (cnt_StStp < SCLK_H_Period - 1) then
--            SDA_out  <= '1';
--          else
--            SDA_out  <= '0';
--          end if;	
--        end if;
	
        --
        -- Check point for state/signals evolution
        if (cnt_SCLK = SCLK_3Q_Period-1) then
          -- State evolution
          fsm_state   <= ManageByte;
	  
          -- Signals evolution
          if (cntWord = 0) then
            intDataReg  <= gCtrlByte(7 downto 1) & '0';  -- wr action
          else
            intDataReg  <= gCtrlByte(7 downto 1) & '1';  -- rd action
          end if;
        end if;

      when ManageByte =>
	
        -- Signals evolution
        NewRdData      <= '0';
        Dt2WrReq       <= '0';
        cnt_StStp      <= (others=>'0');
	
        --
        -- SDA inout pin managing
        if (maskedRdWr_l='1') then
          -- SDA = IN => rd action
          if (cnt_SCLK = SCLK_Q_Period-1) then
            -- SDA is sampled in the middle of SCLK high semiperiod
            intDataReg(to_integer(cnt_Byte)) <= SDA;
          end if;
	  
          -- SIPO register
          if (cnt_SCLK = SCLK_3Q_Period-1 and cnt_Byte = 0) then
            RdData     <= intDataReg;
            NewRdData  <= '1';
          end if;  
        
        else
          -- SDA = OUT => wr action
          SDA_out  <= intDataReg(to_integer(cnt_Byte));
        end if;
	
        --
        -- Byte counter: it counts 8 SCLK pulses (1 Byte sent or received).
        if (cnt_SCLK = SCLK_3Q_Period-1) then
          cnt_Byte  <= cnt_Byte - 1;
        end if;
        
        --
        -- Check point for state/signals evolution
        if (cnt_SCLK = SCLK_3Q_Period-1 and cnt_Byte = 0) then
          -- State evolution
          if (maskedRdWr_l='1' and cntWord = DataWordToProcess_s0-1 ) then -- Read action complete
            fsm_state      <= SetNACK;
          else
            fsm_state      <= ManageACK;
          end if;
	  
          -- Signals evolution
          if (maskedRdWr_l='1' ) then -- Read action 
            SDA_direction  <= '0';
          else
            SDA_direction  <= '1';
          end if;
          
        end if;

      when ManageACK =>
        
        -- Signals evolution
        NewRdData      <= '0';
        Dt2WrReq       <= '0';
        SDA_out        <= '0'; 
        
        --
        -- word counter evolution: it counts the ammount of words(8bits) in each protocol
        if (cnt_SCLK = SCLK_3Q_Period-1) then
          if (cntWord < DataWordToProcess_s0-1) then
            cntWord  <= cntWord + 1;    
          end if;
        end if;

	
        if (maskedRdWr_l='1' ) then -- Read action 
	  
          if (cnt_SCLK = SCLK_3Q_Period-1) then
            -- State evolution
            fsm_state  <= ManageByte;
            -- Signals evolution
            SDA_direction  <= '1';
          end if;
	  
        else                     -- Write action
          --
          -- Check ACK
          if (cnt_SCLK = SCLK_Q_Period-1) then
            if (SDA = '0') then
              NACK_Alrm  <= '0';
            else
              NACK_Alrm  <= '1';  
            end if;
          end if;
	  
          --
          -- Generate a Dt2WrReq pulse to inform the driver module that a new data to wr has to be provided
          if (cnt_SCLK = SCLK_Q_Period-1) then
            Dt2WrReq  <= '1';
          else
            Dt2WrReq  <= '0';  
          end if;
	  
          --
          -- Check point for state/signals evolution
          if (cnt_SCLK = SCLK_3Q_Period-1) then
            -- Signals evolution
            SDA_direction  <= '0';
            intDataReg     <= Addr_Dt_2_WR_s0;
	  
            if (NACK_Alrm = '0') then                                                    -- ACK received
	    
              if (cntWord = 2 and cnt_Byte = 7 and RdWr_l_s0='1') then                   -- RdWr_l_s0 mask release
                maskedRdWr_l   <= RdWr_l_s0;
                SDA_direction  <= '1';
              end if;

              if (cntWord = 1 and cnt_Byte = 7 and RdWr_l_s0='1') then                   -- Sequential read enable check
                fsm_state     <= SetStart;
              else
	      
                if (cntWord < DataWordToProcess_s0-1) then                               
                  fsm_state  <= ManageByte;      
                else                                                                     -- All words written
                  fsm_state  <= SetStop;
                end if;
	      
              end if;
            else                                                                         -- NACK received
              fsm_state  <= NACK_Alarm;
            end if;
          end if;

	     end if;

      when SetNACK =>
	
        -- Signals evolution
	     SDA_out   <= '1'; 
        cntWord   <= (others => '0');
        Dt2WrReq  <= '0';
        NewRdData <= '0';
	
        --
        -- Check point for state/signals evolution  
        if (cnt_SCLK = SCLK_3Q_Period-1) then
          -- State evolution
          fsm_state  <= SetStop;
          -- Signals evolution
          SDA_direction  <= '0';
        end if;

      when SetStop =>
        -- Signals evolution
        cntWord       <= (others => '0');
        Dt2WrReq      <= '0';
        maskedRdWr_l  <= '0';
        cnt_Byte      <= (others=>'1');

        --
        -- Start stop counter evolution	
        if (StartEn_s0='1') then
          if (cnt_StStp < SCLKPeriod-1 ) then
            cnt_StStp  <= cnt_StStp + 1;
          else
            cnt_StStp  <= (others=>'0');
          end if;
        else
          cnt_StStp  <= (others=>'0');
        end if;
	
        --
        -- Stop condition out
        if (cnt_StStp < SCLK_H_Period-1) then
          SDA_out        <= '0';
        else
          SDA_out        <= '1';
          stopOutFlag    <= '1';
        end if;
	
        --
        -- Check point for state/signals evolution  
        if (cnt_SCLK = SCLK_3Q_Period-1 ) then
          -- State evolution
          fsm_state   <= SetStart;	  
        end if;

        if (cnt_StStp = SCLKPeriod-4 ) then  
          -- Signals evolution
          EndProtocol <= '1';
        end if;

      when NACK_Alarm =>
        -- State evolution
        fsm_state   <= SetStart;
        -- Signals evolution
        NACK_Alrm  <= '1';
        Dt2WrReq   <= '0';
	  
      when others => 
        -- State evolution
        fsm_state <= SetStart;
    
        -- Signals evolution
        SDA_direction  <= '0';
        SDA_out        <= '1';
        cnt_Byte       <= (others=>'1');
        intDataReg     <= (others=>'0');
        RdData         <= (others=>'0');
        NewRdData      <= '0';
        cntWord        <= (others=>'0');
        NACK_Alrm      <= '0';
        Dt2WrReq       <= '0';
        cnt_StStp      <= (others=>'0');
        EndProtocol    <= '0';
        maskedRdWr_l   <= '0';
	
        stopOutFlag    <= '0';
    end case;
  
  end if;
end process;

--
-- SDA three states manager
SDA <= SDA_out when (SDA_direction='0') else 'Z';

SDA_dir <= SDA_direction;

end beh;
