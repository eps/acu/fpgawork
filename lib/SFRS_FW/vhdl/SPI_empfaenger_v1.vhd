--  Filename: SPI_empfaenger.vhd (VHDL File)
--  Authors : A.Wiest

--  26.10.2021, Created
--  empfaengt die Daten ueber SPI
---============================================================================================================================================================
LIBRARY ieee;                                      -- Library Declaration
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;
USE     ieee.math_real.ALL;

ENTITY SPI_empfaenger IS    
                                                   -- Entity Declaration
   GENERIC (        
      takt             : integer  := 100_000_000;  -- 100 MHz
      SPI_takt         : integer  := 4;            -- Anzahl der clocks fuer ein Zustand (high or low) von dem SPI ckl
      SPI_bit_count    : integer  := 56 );         -- Anzahl der bits, die uebertragen werden        
 

   PORT (
      clk, reset       : IN  std_logic;            -- Control Signals    
      SPI_CLK_IN       : IN  std_logic;    
      SPI_DI           : IN  std_logic;
      SPI_CSn          : OUT std_logic;
      SPI_DI_OK        : OUT std_logic;
--		New_Data_Output  : OUT std_logic;
      DATA_Output      : OUT std_logic_vector (SPI_bit_count - 1 downto 0));    -- Ausgabe der Empfangenen Daten
        
END SPI_empfaenger;
--=================================================================================================================================================================

ARCHITECTURE behavioral OF SPI_empfaenger IS                                             
   SIGNAL s_SPI_clk                : std_logic  := '0';  
   SIGNAL s_sync_SPI_CLK1          : std_logic  := '0';  
   SIGNAL s_sync_SPI_CLK2          : std_logic  := '0'; 
   SIGNAL s_sync_SPI_CLK3          : std_logic  := '0'; 
   
   SIGNAL s_SPI_clk_counter        : integer     range (SPI_bit_count + 8)*SPI_takt*2 downto 0    := 0;
   SIGNAL s_SPI_clk_bit_count      : integer     range  SPI_bit_count + 8             downto 0    := 0;
   
   SIGNAL s_SPI_CSn                : std_logic  := '0';  
   
   SIGNAL s_SPI_DI                 : std_logic  := '0'; 
   SIGNAL s_sync_SPI_DI1           : std_logic  := '0';   
   SIGNAL s_Parity_in_spi          : std_logic  := '0';  
   SIGNAL s_SPI_DI_ok              : std_logic  := '0'; 

   SIGNAL s_DATA_Empfangen         : std_logic_vector (SPI_bit_count + 8 downto 0)   := (others=>'0'); -- 1 Startbit + 64 Bit Daten + 1 Parity Bit + 1Stop Bit  +5bit pause
   SIGNAL s_SPI_input_counter      : integer    range  SPI_bit_count + 8 downto 0    := 0;

--   signal New_Data_Output_s0       : std_logic;
--   signal New_Data_Output_s1       : std_logic;
--   signal New_Data_Output_RE       : std_logic;
	
BEGIN


 
 SPI_CLK_Synchronization : process (clk, reset, SPI_CLK_IN) --Synchronization of SPI_CKL signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_SPI_CLK1  <= '0';                           -- Data at the fireset clock 
         s_sync_SPI_CLK2  <= '0';
         s_sync_SPI_CLK3  <= '0';
         s_SPI_clk        <= '0';                           -- Data at the second clock  
      else  
         s_sync_SPI_CLK1  <= SPI_CLK_IN;                    -- 1. Clock
         s_sync_SPI_CLK2  <= s_sync_SPI_CLK1;               -- 2. Clock 
         s_sync_SPI_CLK3  <= s_sync_SPI_CLK2; 
         s_SPI_clk        <= s_sync_SPI_CLK3; 

      end if; 
   end if;  
  end process  SPI_CLK_Synchronization;
  
  
  SPI_DI_Synchronization : process (clk, reset, SPI_DI)     --Synchronization of SPI_DI signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_SPI_DI1   <= '0';                           -- Data at the fireset clock 
         s_SPI_DI         <= '0';                           -- Data at the second clock  
      else  
         s_sync_SPI_DI1   <= SPI_DI; 
         s_SPI_DI         <= s_sync_SPI_DI1;                -- 1. Clock
      
      end if; 
   end if;  
  end process  SPI_DI_Synchronization;


--============================================================================
SPI_CSn_process :   process (clk, reset)         -- CS erzeugen
BEGIN

   if (reset = '1') then                                    -- Clock Synchronization
      s_SPI_CSn            <= '1';
      SPI_CSn              <= '1'; 
      s_SPI_clk_counter    <=  0;
     
   elsif (rising_edge (clk) AND clk = '1')                                             then            
      
      if    (s_SPI_clk_counter = SPI_takt +1 AND s_SPI_clk = '1' AND  s_SPI_CSn = '0') then 
         s_SPI_CSn         <= '1';
         SPI_CSn           <= '1'; 
         s_SPI_clk_counter <=  s_SPI_clk_counter + 1;        
      
      elsif    (s_SPI_clk_counter > SPI_takt*4 +1 AND s_SPI_clk = '1' AND s_SPI_CSn = '1') then 
         s_SPI_CSn         <= '0';
         SPI_CSn           <= '0';
         s_SPI_clk_counter <=  0;   
         
      elsif (s_SPI_clk = '0' AND s_SPI_clk_counter > 0)                                 then 
         s_SPI_CSn         <= '0'; 
         SPI_CSn           <= '0';
         s_SPI_clk_counter <=  0;  
          
      elsif (s_SPI_clk = '1') then                                                           
         s_SPI_clk_counter <= s_SPI_clk_counter + 1;
         
      else   
         Null;
      end if; 
  end if;
end process SPI_CSn_process; 

--============================================================================
SPI_DI_process :   process (s_SPI_clk, s_SPI_CSn, reset) -- Daten ueber SPI Empfangen
                                                         -- SPI_DI bits declaration
                                                         -- s_Data_in_reg_spi (0)        = '1' start Bit  
                                                         -- s_Data_in_reg_spi (1 to 64)  = '1' Data bits
                                                         -- s_Data_in_reg_spi (65)       = '1' parity Bit
                                                         -- s_Data_in_reg_spi (66)       = '1' stop Bit
BEGIN
   if (reset = '1' OR s_SPI_CSn = '1' ) then             -- Clock Synchronization
      s_SPI_input_counter  <= 0;                         -- Counter of input-bits Bits
      s_Parity_in_spi      <= '0';
      s_DATA_Empfangen     <= (others =>'0');
	
--      New_Data_Output_s0   <= '0';
--      New_Data_Output_s1   <= '0';
--      New_Data_Output_RE   <= '0';
		
--   	--New_Data_Output      <= '0';
     
   elsif (rising_edge (s_SPI_clk) AND s_SPI_clk = '1' AND s_SPI_CSn ='0') then             -- Writting the input data of SPI

--      	New_Data_Output_s0      <= '0';
--         New_Data_Output_s1   <= New_Data_Output_s0;
--         New_Data_Output_RE   <= New_Data_Output_s0 and not New_Data_Output_s1;

	
         if   (s_SPI_input_counter = 0)   then                                               -- read Pause
            s_DATA_Empfangen                      <= (others =>'0');
            s_DATA_Empfangen(0)                   <= s_SPI_DI;
            s_SPI_input_counter                   <= s_SPI_input_counter + 1; 
     
         elsif(s_SPI_input_counter = 1 ) then                                           -- reade Startbit
            s_DATA_Empfangen(1)                   <= s_SPI_DI;
            s_SPI_input_counter                   <= s_SPI_input_counter + 1; 
      
      
         elsif (s_SPI_input_counter > 1 AND s_SPI_input_counter < SPI_bit_count + 2) then  -- write the Input data    
            s_DATA_Empfangen(s_SPI_input_counter) <= s_SPI_DI;    
            s_SPI_input_counter                   <= s_SPI_input_counter + 1;     
                                                                                           -- Build of parity Bit
            if    (s_SPI_DI = '1' AND s_Parity_in_spi ='0') then  s_Parity_in_spi <= '1';  -- ungerade Anzahl der Bits   
            elsif (s_SPI_DI = '0' AND s_Parity_in_spi ='0') then  s_Parity_in_spi <= '0';  -- gerade Anzahl der Bits   
            elsif (s_SPI_DI = '0' AND s_Parity_in_spi ='1') then  s_Parity_in_spi <= '1'; 
            elsif (s_SPI_DI = '1' AND s_Parity_in_spi ='1') then  s_Parity_in_spi <= '0';   
            end if;
                 
         elsif(s_SPI_input_counter = SPI_bit_count + 2) then                                                              
            s_DATA_Empfangen(s_SPI_input_counter) <= s_SPI_DI;                             -- message of Parity error    
            s_SPI_input_counter                   <= s_SPI_input_counter + 1;    
            
            if (s_SPI_DI = s_Parity_in_spi) then  s_SPI_DI_ok <= '1';                      -- Empfangener und berechneter Parity bit sind identisch               
            else                                  s_SPI_DI_ok <= '0';   
            end if;  
            
         elsif(s_SPI_input_counter = SPI_bit_count + 3) then        
            s_DATA_Empfangen(s_SPI_input_counter) <= s_SPI_DI;                             -- Stop Bit      
            s_SPI_input_counter                   <= s_SPI_input_counter + 1;    
         
         else  
            s_SPI_input_counter   <=  0;
            s_Parity_in_spi       <= '0';
            SPI_DI_OK             <= s_SPI_DI_ok;
            
            if (s_SPI_DI_ok = '1' )then 
               DATA_Output  <= s_DATA_Empfangen(SPI_bit_count+1 downto 2); -- Daten werden am Ausgang geschrieben
					
--					New_Data_Output_s0      <= '1';
            else 
               Null;                                                       -- keine Aenderung an den Daten, Wert bleibt erhalten
            end if;
            
         end if;
      
   end if; 

end process SPI_DI_process; 

--New_Data_Output  <= New_Data_Output_RE;

END behavioral;  
