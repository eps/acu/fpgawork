--  Filename: Addierer16_mit_Begrenzung.vhd
--
--  Beschrebung:
--      Summe = alpha + beta
--
--  Eing. Groessen:
--      alpha           alpha Komponente 16bit signed
--      beta            beta Komponente 16bit signed
--
--      clk             clock
--      Enable          symbolisiert Gültigkeit der Eingangsdaten
--      reset           Berechnung neue starten, alle Varablen auf Null setzen
--
--  Ausg. Groessen:
--      Summe     16bit signed
--      Data_Out_Valid_abc  '1' wenn die Berechnung fertig ist
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity Addierer16_mit_Begrenzung is
  generic(
     takt : integer := 100_000_000);

  port(
    --System-Eingaenge:
    clk, reset, Enable : in std_logic;

    --Eingaenge:
    alpha, beta        : in signed (15 downto 0);

    --Ausgaenge
    Summe              : out signed(15 downto 0);
    Data_Out_Valid     : out std_logic);

end Addierer16_mit_Begrenzung;

--============================================================================
architecture behavioral of Addierer16_mit_Begrenzung is
   attribute multstyle               : string;
   attribute multstyle of behavioral : architecture is "dsp";  -- Nutzt bevorzugt embedded multiplier

   signal s_alpha : signed (15 downto 0) := (others => '0');
   signal s_beta  : signed (15 downto 0) := (others => '0');
   signal s_Summe : signed (16 downto 0) := (others => '0');  --16 + 16  =17 bit

   type STATE_ADDIRER is(EINGANG_SPEICHERN, ADDITION, DATENRAUSGEBEN);
   signal s_STATE : STATE_ADDIRER := EINGANG_SPEICHERN;


begin

   hauptprozess : process (clk, reset, Enable)
   begin
      if (reset = '1') then
         s_alpha        <= (others => '0');
         s_beta         <= (others => '0');
         s_Summe        <= (others => '0');
         Summe          <= (others => '0');
         s_STATE        <= EINGANG_SPEICHERN;
         Data_Out_Valid <= '0';

   elsif rising_edge(clk) then
      case s_STATE is
   
   ----------------------------------------------------------------------------------
   when EINGANG_SPEICHERN =>       --1.Takt
   ----------------------------------------------------------------------------------
      Data_Out_Valid <= '0';
      if (Enable = '1') then
         s_alpha     <= alpha;
         s_beta      <= beta;
         s_STATE     <= ADDITION;
      else
         s_STATE     <= Null;
      end if;

   ----------------------------------------------------------------------------------
   when ADDITION =>
   ----------------------------------------------------------------------------------
      s_Summe  <= resize(s_alpha,17) + resize(s_beta,17);  
      s_STATE  <= DATENRAUSGEBEN;

   ----------------------------------------------------------------------------------
   when DATENRAUSGEBEN =>
   ----------------------------------------------------------------------------------
      if    s_Summe > to_signed(32767,  16) then Summe <= to_signed(32767, 16);
      elsif s_Summe < to_signed(-32767, 16) then Summe <= to_signed(-32767, 16);
      else                                       Summe <= s_Summe(15 downto 0);
      end if;
      s_STATE        <= EINGANG_SPEICHERN;
      Data_Out_Valid <= '1';
   
   ---------------------------------------------------------------------
   when others =>
   ---------------------------------------------------------------------
      s_STATE <= EINGANG_SPEICHERN;

   end case;
   end if;  --reset
   end process hauptprozess;

end architecture behavioral;
