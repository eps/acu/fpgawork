-- Aufgabe dieses Blocks ist es, die Signale von GUI bei einer Aenderung erst nach 0,5sek auszugeben

LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY GUI_uebergabe IS
   GENERIC(           
      takt               : integer  := 100_000_000) ;           -- Initialisierung 100 MHz
    
   PORT(
      clk, reset, enable : in  std_logic                    ;   -- System-Eingänge:
      Input1             : in  std_logic_vector(23 downto 0);
      Input2             : in  std_logic_vector(23 downto 0);
      Input3             : in  std_logic_vector(15 downto 0);
      Input4             : in  std_logic_vector(15 downto 0);
      Input5             : in  std_logic_vector(15 downto 0);
      Input6             : in  std_logic_vector(15 downto 0);
      Input7             : in  std_logic_vector(15 downto 0);
      Input8             : in  std_logic_vector(15 downto 0);

      Output1            : out std_logic_vector(23 downto 0);
      Output2            : out std_logic_vector(23 downto 0);
      Output3            : out std_logic_vector(15 downto 0);
      Output4            : out std_logic_vector(15 downto 0);
      Output5            : out std_logic_vector(15 downto 0);
      Output6            : out std_logic_vector(15 downto 0);
      Output7            : out std_logic_vector(15 downto 0);
      Output8            : out std_logic_vector(15 downto 0);
      
      Data_Out_Valid     : out std_logic                   );

END GUI_uebergabe;

--============================================================================
ARCHITECTURE behavioral OF GUI_uebergabe IS

signal s_Input1     : std_logic_vector (23 downto 0) := (others => '0');
signal s_Input2     : std_logic_vector (23 downto 0) := (others => '0');
signal s_Input3     : std_logic_vector (15 downto 0) := (others => '0');
signal s_Input4     : std_logic_vector (15 downto 0) := (others => '0');
signal s_Input5     : std_logic_vector (15 downto 0) := (others => '0');
signal s_Input6     : std_logic_vector (15 downto 0) := (others => '0');
signal s_Input7     : std_logic_vector (15 downto 0) := (others => '0');
signal s_Input8     : std_logic_vector (15 downto 0) := (others => '0');

signal s_Output1    : std_logic_vector (23 downto 0) := (others => '0');
signal s_Output2    : std_logic_vector (23 downto 0) := (others => '0');
signal s_Output3    : std_logic_vector (15 downto 0) := (others => '0');
signal s_Output4    : std_logic_vector (15 downto 0) := (others => '0');
signal s_Output5    : std_logic_vector (15 downto 0) := (others => '0');
signal s_Output6    : std_logic_vector (15 downto 0) := (others => '0');
signal s_Output7    : std_logic_vector (15 downto 0) := (others => '0');
signal s_Output8    : std_logic_vector (15 downto 0) := (others => '0');

Constant counter_max: unsigned (31 downto 0) := to_unsigned (800_000_00,32); -- 0,8Sek
--Constant counter_max: unsigned (31 downto 0) := to_unsigned (50,32);     -- nur fuer Simulation

SIGNAL   s_Counter  : unsigned (31 downto 0) :=(others => '0')            ; --Max 2^32= 42_949_672_96

TYPE T_STATE IS(EINGANG_SPEICHERN,VERGLEICHEN,AUSGEBEN);
SIGNAL s_STATE: T_STATE := EINGANG_SPEICHERN;

begin

GUI_uebergabe_p: process(reset, clk, enable) 
  BEGIN
  
   if reset = '1' OR enable = '0' then
      s_Input1  <= (others => '0'); 
      s_Input2  <= (others => '0'); 
      s_Input3  <= (others => '0'); 
      s_Input4  <= (others => '0'); 
      s_Input5  <= (others => '0'); 
      s_Input6  <= (others => '0'); 
      s_Input7  <= (others => '0'); 
      s_Input8  <= (others => '0'); 

      s_Output1 <= (others => '0'); 
      s_Output2 <= (others => '0'); 
      s_Output3 <= (others => '0'); 
      s_Output4 <= (others => '0');
      s_Output5 <= (others => '0'); 
      s_Output6 <= (others => '0');
      s_Output7 <= (others => '0'); 
      s_Output8 <= (others => '0');
      
      Output1   <= (others => '0'); 
      Output2   <= (others => '0'); 
      Output3   <= (others => '0'); 
      Output4   <= (others => '0');
      Output5   <= (others => '0'); 
      Output6   <= (others => '0');
      Output7   <= (others => '0'); 
      Output8   <= (others => '0');
      
      s_STATE   <= EINGANG_SPEICHERN; 
   
   elsif rising_edge(clk)  then
      case s_STATE is

      ----------------------------------------------------------------------------------
      when EINGANG_SPEICHERN =>       --1.Takt
      ----------------------------------------------------------------------------------
         if (enable = '1') then
            s_Input1        <= Input1;
            s_Input2        <= Input2;
            s_Input3        <= Input3;
            s_Input4        <= Input4;
            s_Input5        <= Input5;
            s_Input6        <= Input6;
            s_Input7        <= Input7;
            s_Input8        <= Input8;
            
            Data_Out_Valid  <= '0';
            s_STATE         <= VERGLEICHEN;
         end if;
         Data_Out_Valid     <= '0';

      ----------------------------------------------------------------------------------
      when VERGLEICHEN =>       --2.Takt
      ----------------------------------------------------------------------------------
         if(s_Input1 /= s_Output1  OR
            s_Input2 /= s_Output2  OR 
            s_Input3 /= s_Output3  OR
            s_Input4 /= s_Output4  OR
            s_Input5 /= s_Output5  OR
            s_Input6 /= s_Output6  OR
            s_Input7 /= s_Output7  OR
            s_Input8 /= s_Output8  ) then
            if s_Counter < counter_max then
               s_Counter <= s_Counter + 1;
            else
               s_Output1 <= Input1;
               s_Output2 <= Input2;
               s_Output3 <= Input3;
               s_Output4 <= Input4;
               s_Output5 <= Input5;
               s_Output6 <= Input6;
               s_Output7 <= Input7;
               s_Output8 <= Input8;
               
               s_Counter <= (others => '0'); 
               s_STATE   <= AUSGEBEN;
            end if; 
         else
            s_STATE      <= AUSGEBEN;
         end if;

      ----------------------------------------------------------------------------------
      when AUSGEBEN =>       --2.Takt
      ----------------------------------------------------------------------------------
         Output1         <= s_Output1;
         Output2         <= s_Output2;
         Output3         <= s_Output3;
         Output4         <= s_Output4;
         Output5         <= s_Output5;
         Output6         <= s_Output6;
         Output7         <= s_Output7;
         Output8         <= s_Output8;
         
         Data_Out_Valid  <= '1';
         s_STATE         <= EINGANG_SPEICHERN; 

   ---------------------------------------------------------------------
   when others =>
   ---------------------------------------------------------------------
      s_STATE <= EINGANG_SPEICHERN;

   end case;
   end if;
END process GUI_uebergabe_p;
END architecture behavioral;
