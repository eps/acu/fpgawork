--  Filename: Dac_DigMux.vhd
--  Date    : 04.02.2021 born
--  Author  : A.Wiest
--  Version : 1.0

--  Description:

-- Umschaltung digitaler Eingaenge
-- 

--============================================================================
 
LIBRARY ieee;                                          --Library Declaration
USE  ieee.std_logic_1164.all; 
USE  ieee.numeric_std.all;

------------------------------------------------------------------------------


entity Dac_DigMux is

   generic (
		clk_in_hz   : integer  := 100_000_000);        -- 100Mhz system clock
	
   port ( 
		clk, reset  : in std_logic;
		COMMAND     : in std_logic_vector (3 downto 0);
		
		MUX0_IN     : in  std_logic_vector(7 downto 0);   
		MUX1_IN     : in  std_logic_vector(7 downto 0);   
		MUX2_IN     : in  std_logic_vector(7 downto 0);   
		MUX3_IN     : in  std_logic_vector(7 downto 0); 
      
      MUX4_IN     : in  std_logic_vector(7 downto 0);   
		MUX5_IN     : in  std_logic_vector(7 downto 0);   
		MUX6_IN     : in  std_logic_vector(7 downto 0);   
		MUX7_IN     : in  std_logic_vector(7 downto 0);
 
      MUX8_IN     : in  std_logic_vector(7 downto 0);   
		MUX9_IN     : in  std_logic_vector(7 downto 0);   
		MUX10_IN    : in  std_logic_vector(7 downto 0);   
		MUX11_IN    : in  std_logic_vector(7 downto 0); 
      
      MUX12_IN    : in  std_logic_vector(7 downto 0);   
		MUX13_IN    : in  std_logic_vector(7 downto 0);   
		MUX14_IN    : in  std_logic_vector(7 downto 0);   
		MUX15_IN    : in  std_logic_vector(7 downto 0);     
	
		MUX1_OUT    : out std_logic;
		MUX2_OUT    : out std_logic;
		MUX3_OUT    : out std_logic;
		MUX4_OUT    : out std_logic;
		
      MUX5_OUT    : out std_logic;
		MUX6_OUT    : out std_logic;
		MUX7_OUT    : out std_logic;
		MUX8_OUT    : out std_logic );
	
end Dac_DigMux;


architecture RTL of Dac_DigMux is

BEGIN

process_DAC_DigMux :  process (clk, reset)
	
BEGIN

if (reset ='1') then
	MUX1_OUT  <= '0';
	MUX2_OUT  <= '0';  
	MUX3_OUT  <= '0';  
	MUX4_OUT  <= '0';  
	
   MUX5_OUT  <= '0';
	MUX6_OUT  <= '0';  
	MUX7_OUT  <= '0';  
	MUX8_OUT  <= '0';
	
elsif (rising_edge(clk) and clk = '1') then
	case COMMAND(3 downto 0) is
		when "0000" => MUX1_OUT <= MUX0_IN(7);  MUX2_OUT <= MUX0_IN(6);  MUX3_OUT <= MUX0_IN(5);  MUX4_OUT <= MUX0_IN(4);  MUX5_OUT <= MUX0_IN(3);  MUX6_OUT <= MUX0_IN(2);  MUX7_OUT <= MUX0_IN(1);  MUX8_OUT <= MUX0_IN(0);      
      when "0001" => MUX1_OUT <= MUX1_IN(7);  MUX2_OUT <= MUX1_IN(6);  MUX3_OUT <= MUX1_IN(5);  MUX4_OUT <= MUX1_IN(4);  MUX5_OUT <= MUX1_IN(3);  MUX6_OUT <= MUX1_IN(2);  MUX7_OUT <= MUX1_IN(1);  MUX8_OUT <= MUX1_IN(0);      
      when "0010" => MUX1_OUT <= MUX2_IN(7);  MUX2_OUT <= MUX2_IN(6);  MUX3_OUT <= MUX2_IN(5);  MUX4_OUT <= MUX2_IN(4);  MUX5_OUT <= MUX2_IN(3);  MUX6_OUT <= MUX2_IN(2);  MUX7_OUT <= MUX2_IN(1);  MUX8_OUT <= MUX2_IN(0);     
      when "0011" => MUX1_OUT <= MUX3_IN(7);  MUX2_OUT <= MUX3_IN(6);  MUX3_OUT <= MUX3_IN(5);  MUX4_OUT <= MUX3_IN(4);  MUX5_OUT <= MUX3_IN(3);  MUX6_OUT <= MUX3_IN(2);  MUX7_OUT <= MUX3_IN(1);  MUX8_OUT <= MUX3_IN(0);   
      
      when "0100" => MUX1_OUT <= MUX4_IN(7);  MUX2_OUT <= MUX4_IN(6);  MUX3_OUT <= MUX4_IN(5);  MUX4_OUT <= MUX4_IN(4);  MUX5_OUT <= MUX4_IN(3);  MUX6_OUT <= MUX4_IN(2);  MUX7_OUT <= MUX4_IN(1);  MUX8_OUT <= MUX4_IN(0);      
      when "0101" => MUX1_OUT <= MUX5_IN(7);  MUX2_OUT <= MUX5_IN(6);  MUX3_OUT <= MUX5_IN(5);  MUX4_OUT <= MUX5_IN(4);  MUX5_OUT <= MUX5_IN(3);  MUX6_OUT <= MUX5_IN(2);  MUX7_OUT <= MUX5_IN(1);  MUX8_OUT <= MUX5_IN(0);      
      when "0110" => MUX1_OUT <= MUX6_IN(7);  MUX2_OUT <= MUX6_IN(6);  MUX3_OUT <= MUX6_IN(5);  MUX4_OUT <= MUX6_IN(4);  MUX5_OUT <= MUX6_IN(3);  MUX6_OUT <= MUX6_IN(2);  MUX7_OUT <= MUX6_IN(1);  MUX8_OUT <= MUX6_IN(0);     
      when "0111" => MUX1_OUT <= MUX7_IN(7);  MUX2_OUT <= MUX7_IN(6);  MUX3_OUT <= MUX7_IN(5);  MUX4_OUT <= MUX7_IN(4);  MUX5_OUT <= MUX7_IN(3);  MUX6_OUT <= MUX7_IN(2);  MUX7_OUT <= MUX7_IN(1);  MUX8_OUT <= MUX7_IN(0);   
      
      when "1000" => MUX1_OUT <= MUX8_IN(7);  MUX2_OUT <= MUX8_IN(6);  MUX3_OUT <= MUX8_IN(5);  MUX4_OUT <= MUX8_IN(4);  MUX5_OUT <= MUX8_IN(3);  MUX6_OUT <= MUX8_IN(2);  MUX7_OUT <= MUX8_IN(1);  MUX8_OUT <= MUX8_IN(0);      
      when "1001" => MUX1_OUT <= MUX9_IN(7);  MUX2_OUT <= MUX9_IN(6);  MUX3_OUT <= MUX9_IN(5);  MUX4_OUT <= MUX9_IN(4);  MUX5_OUT <= MUX9_IN(3);  MUX6_OUT <= MUX9_IN(2);  MUX7_OUT <= MUX9_IN(1);  MUX8_OUT <= MUX9_IN(0);      
      when "1010" => MUX1_OUT <= MUX10_IN(7); MUX2_OUT <= MUX10_IN(6); MUX3_OUT <= MUX10_IN(5); MUX4_OUT <= MUX10_IN(4); MUX5_OUT <= MUX10_IN(3); MUX6_OUT <= MUX10_IN(2); MUX7_OUT <= MUX10_IN(1); MUX8_OUT <= MUX10_IN(0);     
      when "1011" => MUX1_OUT <= MUX11_IN(7); MUX2_OUT <= MUX11_IN(6); MUX3_OUT <= MUX11_IN(5); MUX4_OUT <= MUX11_IN(4); MUX5_OUT <= MUX11_IN(3); MUX6_OUT <= MUX11_IN(2); MUX7_OUT <= MUX11_IN(1); MUX8_OUT <= MUX11_IN(0);   
	
      when "1100" => MUX1_OUT <= MUX12_IN(7); MUX2_OUT <= MUX12_IN(6); MUX3_OUT <= MUX12_IN(5); MUX4_OUT <= MUX12_IN(4); MUX5_OUT <= MUX12_IN(3); MUX6_OUT <= MUX12_IN(2); MUX7_OUT <= MUX12_IN(1); MUX8_OUT <= MUX12_IN(0);      
      when "1101" => MUX1_OUT <= MUX13_IN(7); MUX2_OUT <= MUX13_IN(6); MUX3_OUT <= MUX13_IN(5); MUX4_OUT <= MUX13_IN(4); MUX5_OUT <= MUX13_IN(3); MUX6_OUT <= MUX13_IN(2); MUX7_OUT <= MUX13_IN(1); MUX8_OUT <= MUX13_IN(0);      
      when "1110" => MUX1_OUT <= MUX14_IN(7); MUX2_OUT <= MUX14_IN(6); MUX3_OUT <= MUX14_IN(5); MUX4_OUT <= MUX14_IN(4); MUX5_OUT <= MUX14_IN(3); MUX6_OUT <= MUX14_IN(2); MUX7_OUT <= MUX14_IN(1); MUX8_OUT <= MUX14_IN(0);     
      when "1111" => MUX1_OUT <= MUX15_IN(7); MUX2_OUT <= MUX15_IN(6); MUX3_OUT <= MUX15_IN(5); MUX4_OUT <= MUX15_IN(4); MUX5_OUT <= MUX15_IN(3); MUX6_OUT <= MUX15_IN(2); MUX7_OUT <= MUX15_IN(1); MUX8_OUT <= MUX15_IN(0);   
	
   
   end case;
	
end if;
end process  process_DAC_DigMux;
	
end RTL;







