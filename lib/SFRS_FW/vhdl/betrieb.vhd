-- Autor : Alexander Wiest
-- Bassiert auf Ueberlegungen aus Zusammenarbeit mit J.Maier
-- V0.1   09.02.2021


LIBRARY ieee;
USE     ieee.std_logic_1164.ALL;
USE     ieee.numeric_std.ALL;

ENTITY betrieb IS
   GENERIC(
      gSM_CLKFrequency                 : INTEGER           := 100_000_000;
      gInternTimer_loop                : INTEGER           := 10;  -- 100000 *10ns= 1ms -> fuer simulation 10

      --Tripline Prozess
      gTimer_Test                      : INTEGER           := 15;  -- 300  (0,3 sec) Zeit fuer einzelne Tests zw. Aktion u. Messung

      --Ein / Ausschalte-Prozess
      gDeChargeSwitchOFFTime           : INTEGER           := 9;   -- 500 Zeit um den Entladeschutz auszuschalten
      gChargingTime                    : INTEGER           := 6;   -- 15000 Zeit um den Zw.Kreis aufzuladen
      gActivRectConstMod               : INTEGER           := 10;  -- 5000 nach 5sek, nach dem Ladeschutz geschaltet hat, wird die aktive Gleichrichtung aktiviert
      gMainSwitchONTime                : INTEGER           := 7;   -- 500 Zeit bei der Hauptschutz und Ladeschutz zusammengeschaltet sind
      gChargeSwitchOFFTime             : INTEGER           := 10;  -- 400 Zeit bei der Hauptschutz AN ist und Ladeschutz abgeschaltet wird
      gMainSwitchOFFTime               : INTEGER           := 5;   -- 400 Zeit bei der Hauptschutz abgeschaltet wird
      gDeChargeTime                    : INTEGER           := 8;   -- 8000 Entladezeit fuer den Zwischenkreis

      --QP Prozess
      gQP_BEFOR_SwitchTime             : INTEGER           := 6;   -- Erste Abwartezeit damit beide NG nicht gleichzeitig schalten
      gQP_SwitchTime                   : INTEGER           := 4 ); -- Zeit vor und nach dem Einschalten der QP /KS Schuetze


   PORT (
      -- Basis-Eingänge
      clk, reset, enable, QP_switch_enable         : IN STD_LOGIC;-- mit Enable koennen nachfolgene FPGA Module gesperrt werden-> " Reglersperre"
                                                                  -- mit QP_switch_enable werden QP Switch+KS+Thy deaktiviert
                                                                  -- enable Signal die die Sperre für die nachgeschaltete Module
      -- Commands
      MFU_Command                                  : IN unsigned (3 downto 0); -- Signal nach dem Decoder
                                                                  -- 0 nichts machen (nach jedem Komando, abgesehen von der Reglersperre, springt auf null)
                                                                  -- 1 Einschalten , Puls 100ms
                                                                  -- 2 Ausschalten , Puls 100ms
                                                                  -- 3 Reset       , Puls 100ms
                                                                  -- 4 Reglersperre, steht dann dauerhaft an, kommt vom CS Intern nicht möglich?


      --Error Inputs IGBTs--------------------------------------
      L3_Modul_W_err                              : IN STD_LOGIC; -- 1=OK (kein Schmitttriger) IGBT Module fuer 3L-Konverter
      L3_Modul_V_err                              : IN STD_LOGIC; -- 1=OK (kein Schmitttriger)
      L3_Modul_U_err                              : IN STD_LOGIC; -- 1=OK (kein Schmitttriger)

      TSS_V13orV14_err                            : IN STD_LOGIC; -- 1=OK IGBT Module fuer Tief-/Hoch Setzsteller
      TSS_V15orV16_err                            : IN STD_LOGIC;

      HB_V17orV18_err                             : IN STD_LOGIC; -- 1=OK IGBT Module fuer Resonanzkonvert Pimaerseite H-Bruecke
      HB_V19orV20_err                             : IN STD_LOGIC; -- 1=OK IGBT Module fuer Resonanzkonvert Pimaerseite H-Bruecke
      Sek_V21orV22_err                            : IN STD_LOGIC; -- 1=OK IGBT Module fuer Resonanzkonvert Sekundaerseite
      Sek_V23orV24_err                            : IN STD_LOGIC; -- 1=OK IGBT Module fuer Resonanzkonvert Sekundaerseite

      --Error von den Schuetzen---------------------------------
      PrimSeiteSchutz_err                         : IN STD_LOGIC; -- 1=OK Relaiskarte Schuetze der Prim. Seite -Haupt-,Lade-Entlade
      QpSchutz_err                                : IN STD_LOGIC; -- 1=OK Relaiskarte QP Schuetze - QP_Pos, FL_Thyristor, Kurzschliesser
      Qp2_err                                     : IN STD_LOGIC; -- 1=OK Relaiskarte QP Schuetze - QP_neg (redundant aufgebaut, deswegen extra Ausgang)
      Umschalter_err                              : IN STD_LOGIC; -- 1=OK Umschalter parallel zu den IGBTs auf der Sek. Seite

      --ADC-Eingaenge-------------------------------------------
      PhVoltage_ADC1_13_err                       : IN STD_LOGIC; -- 1=OK Phasenspannungerfassung nach dem Hauptschutz - Uph1,Uph2,Uph3
      PhCurrent_ADC1_46_err                       : IN STD_LOGIC; -- 1=OK Phasen-Strom-erfassung nach dem Hauptschutz  - Iph1, Iph2, Iph3
      DCLinkTSS_V_ADC2_13_err                     : IN STD_LOGIC; -- 1=OK Dc Sp. Erfassung Uzwpos, Uzwneg, U BuckBoost
      DC_OutVolt_ADC2_46_err                      : IN STD_LOGIC; -- 1=OK Ausgangsspannung pos, neg, gesamt
      RdampVolt_ADC3_13_err                       : IN STD_LOGIC; -- 1=OK Spannung auf den Dumpwiderstand1 und Dampwiderstand2
      DC_Current1_ADC3_46_err                     : IN STD_LOGIC; -- 1=OK BuckBoost Strom und Resonanzstrom der Primaerseite der H-Bruecke
      DC_Current2_ADC4_13_err                     : IN STD_LOGIC; -- 1=OK Strom in dem FreilaufThyristor und Strom im Ausgangs
      Reserve_ADC4_46_err                         : IN STD_LOGIC; -- 1=OK Reserve

      --InterlockBoards-----------------------------------------
      Interlock1_Goup1_err                        : IN STD_LOGIC; -- 1=OK 3L-Input Shoke, 55°C Watter Cooling Plate -> Feed Energy back into the grid
      Interlock1_Goup2_err                        : IN STD_LOGIC; -- 1=OK Door Backsite oppen, MainFuse, 400VMonitoring and FI for Electronic, -> Abschaltung
      Interlock1_Goup3_err                        : IN STD_LOGIC; -- 1=OK 3LShoke 125°C, LoadRes. 120°C, Disch.Res. 120°C, Wat.Coll.Plate 65°C -> Abschaltung
      Interlock1_Goup4_err                        : IN STD_LOGIC; -- 1=OK ILoad.Res >20A, 24V El. Fuse, 24V IGBT Driver Fuse,Fast Shutdown -> Abschaltung

      Interlock2_Goup1_err                        : IN STD_LOGIC; -- 1=OK BBShoke 110°C, Res.Shoke o. Transformer 110°C, Outp.Shoke 110°C -> Feed Energy back
      Interlock2_Goup2_err                        : IN STD_LOGIC; -- 1=OK BBShoke 125°C, Res.Shoke o. Transformer 125°C, UPS OK, RFilter 120°C -> Abschaltung
      Interlock2_Goup3_err                        : IN STD_LOGIC; -- 1=OK OutputShoke 125°C, UPS Current, DCCT 24V,Rdamp 120°C-> Abschaltung
      Interlock2_Goup4_err                        : IN STD_LOGIC; -- 1=OK Cryo Temp Mag, MPS, PPS, Fast ShutdownSwitch -> Abschaltung

      Tripline_1_in                               : IN STD_LOGIC; -- Tripline getrennt = '0'
      Tripline_2_in                               : IN STD_LOGIC;
      Tripline_3_in                               : IN STD_LOGIC;

      --Einstellungen-------------------------------------------
      Reset_mode                                  : IN STD_LOGIC; --='0' nur der erster Fehler wird gespeichert bis reset kommt; ='1' alle Fehler
      Tripline_mode                               : IN STD_LOGIC; --='1' Tripline wirden nach dem Reset immer testiert, =0 nur beim ersten Reset

      --Outputs-------------------------------------------------
      ErrorBand                                   : OUT unsigned (127 DOWNTO 0);
      resetAllCards                               : OUT STD_LOGIC;
      FPGA_Tripline_out                           : OUT STD_LOGIC_VECTOR (2 downto 0); -- Tripline =1 -> Tripline durchgeschaltet
      switch_out                                  : OUT STD_LOGIC_VECTOR (2 downto 0); -- (Entlade, Lade, Haupt)
      QP_switch_out                               : OUT STD_LOGIC_VECTOR (3 downto 0); -- (Th, KS, QP2_neg, QP1_pos)
      Ready_for_operate                           : OUT STD_LOGIC;                     -- =1 Nach dem Einschalten, wenn auch akt. Gleichrichtung laeuft
      QP_Ready_for_operate                        : OUT STD_LOGIC;                     -- QP Schalter in der Stellung für Betrieb

      BuckBost_enable                             : OUT STD_LOGIC;                     -- FPGA-Bloecke werden erst dann zu aktivieren, wenn alles gut ist
      Resonanz_enable                             : OUT STD_LOGIC;                     -- Resonanzblock wird ab/angeschaltet
      Level3_PWM_enable                           : OUT STD_LOGIC;                     -- im Fehlerfall =0

      AktivRectPWMON                              : OUT STD_LOGIC;                     -- =1 PWM der akt. Gleichrichtung wird eingeschaltet
      ActivRectConstMod                           : OUT STD_LOGIC;                     -- =1 aktive Einspeisung ist mit Konstanter Modulation aktiviert
      AktivRectControllON                         : OUT STD_LOGIC;                     -- =1 Regler fuer die aktive gleichrichtung aktivieren
      UebergangUPDown                             : OUT STD_LOGIC;                     -- 0->1 Vorsteuerung wird umgeschaltet von Generator auf messung
      Umin_monitoringON                           : OUT STD_LOGIC;                     -- ='0' die Überwachung der Zw.Kr. Sp  auf Unterspannung ist aktiviert =1 deakt
      Uzw_ist                                     : IN signed (15 downto 0);           -- istwert der Zwischenkreisspannung
      Uzw_soll                                    : IN signed (15 downto 0));          -- Sollwert der Zwischenkreisspannung
END ENTITY betrieb;

ARCHITECTURE behav OF betrieb IS

   SIGNAL s_ErrorBand                : unsigned(127 DOWNTO 0) :=(others => '0');         -- Alle Fehler sind in einem Vektor gespeichert
   SIGNAL s_first_reset_redy         : STD_LOGIC                      := '0';            -- nach dem ersten Reset =1

   -- Signale fuer den ersten internen Counter
   SIGNAL s_WaitCounterMax1          : unsigned(17 DOWNTO 0)          :=(others => '0'); -- Anzahl der Ausseren Schleifen
   SIGNAL s_WaitCounter1             : unsigned(17 DOWNTO 0)          :=(others => '0'); -- Incriment aussere Schleife
   SIGNAL s_Timer_intern1            : unsigned(17 DOWNTO 0)          :=(others => '0'); -- Innere Schleife
   SIGNAL s_startCounter1            : STD_LOGIC                      := '0';            -- Starte Counter
   SIGNAL s_counter_ready1           : STD_LOGIC                      := '0';            -- Counter ist fertig

   -- Signale fuer den zweiten internen Counter
   SIGNAL s_WaitCounterMax2          : unsigned(17 DOWNTO 0)          :=(others => '0'); -- Anzahl der Ausseren Schleifen
   SIGNAL s_WaitCounter2             : unsigned(17 DOWNTO 0)          :=(others => '0'); -- Incriment aussere Schleife
   SIGNAL s_Timer_intern2            : unsigned(17 DOWNTO 0)          :=(others => '0'); -- Innere Schleife
   SIGNAL s_startCounter2            : STD_LOGIC                      := '0';            -- Starte Counter
   SIGNAL s_counter_ready2           : STD_LOGIC                      := '0';            -- Counter ist fertig
   SIGNAL s_resetTimer2              : STD_LOGIC                      := '0';            -- Timer2 wird intern aus dem ON_OFF_Prozess resetet

   -- Signale fuer den 3. internen Counter
   SIGNAL s_WaitCounterMax3          : unsigned(17 DOWNTO 0)          :=(others => '0'); -- Anzahl der Ausseren Schleifen
   SIGNAL s_WaitCounter3             : unsigned(17 DOWNTO 0)          :=(others => '0'); -- Incriment aussere Schleife
   SIGNAL s_Timer_intern3            : unsigned(17 DOWNTO 0)          :=(others => '0'); -- Innere Schleife
   SIGNAL s_startCounter3            : STD_LOGIC                      := '0';            -- Starte Counter
   SIGNAL s_counter_ready3           : STD_LOGIC                      := '0';            -- Counter ist fertig
   SIGNAL s_resetTimer3              : STD_LOGIC                      := '0';            -- Timer3 wird intern aus dem QP Prozess resetet


   TYPE STATE_COUNTER IS(WARTEN_AUF_START, ZAELEN, AUSGABE);
   SIGNAL s_STATE_COUNTER1: STATE_COUNTER  := WARTEN_AUF_START;
   SIGNAL s_STATE_COUNTER2: STATE_COUNTER  := WARTEN_AUF_START;
   SIGNAL s_STATE_COUNTER3: STATE_COUNTER  := WARTEN_AUF_START;

   --Ausgaenge
   SIGNAL s_PrimSwitchProz_enable  : STD_LOGIC                      := '0';            -- Schalter Prozess abbrechen und zum Ausschalten gehen
   SIGNAL s_BuckBost_enable        : STD_LOGIC                      := '0';            -- Softwarebloecke aktivieren,
   SIGNAL s_Resonanz_enable        : STD_LOGIC                      := '0';            -- Softwarebloecke aktivieren, hier beispielhaft Resonanzblock
   SIGNAL s_Level3_PWM_enable      : STD_LOGIC                      := '0';


   --Uebergabe zwischen den Prozessen
   SIGNAL s_QP_SwitchProz_Enable   : STD_LOGIC                      := '0';      -- Freigabe von dem Fehlerprozess in den QP_Switch_Prozess =1 Freigabe erteilt
   SIGNAL s_Ready_for_operate      : STD_LOGIC                      := '0';      -- Hauptschutz ist eingeschaltet, Betriebsbereit

   -- Fehlermeldungen von den IGBTtrieber werden synchronisiert
   SIGNAL s_L3_Modul_W_err         : STD_LOGIC:='0'; SIGNAL s_sync_L3_Modul_W_err     : STD_LOGIC:='0'; --3L IGBTS
   SIGNAL s_L3_Modul_V_err         : STD_LOGIC:='0'; SIGNAL s_sync_L3_Modul_V_err     : STD_LOGIC:='0';
   SIGNAL s_L3_Modul_U_err         : STD_LOGIC:='0'; SIGNAL s_sync_L3_Modul_U_err     : STD_LOGIC:='0';

   SIGNAL s_TSS_V13orV14_err       : STD_LOGIC:='0'; SIGNAL s_sync_TSS_V13orV14_err   : STD_LOGIC:='0'; -- IGBT Module fuer Tief-/Hoch Setzsteller
   SIGNAL s_TSS_V15orV16_err       : STD_LOGIC:='0'; SIGNAL s_sync_TSS_V15orV16_err   : STD_LOGIC:='0'; -- IGBT Module fuer Tief-/Hoch Setzsteller

   SIGNAL s_HB_V17orV18_err        : STD_LOGIC:='0'; SIGNAL s_sync_HB_V17orV18_err    : STD_LOGIC:='0'; -- IGBT Module f. Resonanzkonvert Pimaerseite H-Bruecke
   SIGNAL s_HB_V19orV20_err        : STD_LOGIC:='0'; SIGNAL s_sync_HB_V19orV20_err    : STD_LOGIC:='0'; -- IGBT Module f. Resonanzkonvert Pimaerseite H-Bruecke
   SIGNAL s_Sek_V21orV22_err       : STD_LOGIC:='0'; SIGNAL s_sync_Sek_V21orV22_err   : STD_LOGIC:='0'; -- IGBT Module fuer Resonanzkonvert Sekundaerseite
   SIGNAL s_Sek_V23orV24_err       : STD_LOGIC:='0'; SIGNAL s_sync_Sek_V23orV24_err   : STD_LOGIC:='0'; -- IGBT Module fuer Resonanzkonvert Sekundaerseite

   --Error von den Schuetzen-----------------------------------------
   SIGNAL s_PrimSeiteSchutz_err    : STD_LOGIC:='0'; SIGNAL s_sync_PrimSeiteSchutz_err: STD_LOGIC:='0'; -- Relaiskarte Lade, Entlade, Hauptschutz
   SIGNAL s_QpSchutz_err           : STD_LOGIC:='0'; SIGNAL s_sync_QpSchutz_err       : STD_LOGIC:='0'; -- QP_Pos, FL_Thyristor, Kurzschliesser
   SIGNAL s_Qp2_err                : STD_LOGIC:='0'; SIGNAL s_sync_Qp2_err            : STD_LOGIC:='0'; -- QP_neg (redundant, deswegen extra Ausgang)
   SIGNAL s_Umschalter_err         : STD_LOGIC:='0'; SIGNAL s_sync_Umschalter_err     : STD_LOGIC:='0'; -- Umschalter parallel zu den IGBTs auf der Sek. Seite

   --ADC-Eingaenge-----------------------------------------
   SIGNAL s_PhVoltage_ADC1_13_err  : STD_LOGIC:='0'; SIGNAL s_sync_PhVoltage_ADC1_13_err   : STD_LOGIC:='0'; -- UPhasenspannungerfassung Uph1,Uph2,Uph3
   SIGNAL s_PhCurrent_ADC1_46_err  : STD_LOGIC:='0'; SIGNAL s_sync_PhCurrent_ADC1_46_err   : STD_LOGIC:='0'; -- Ph. Strom-erfassung nach Hauptschutz- Iph1,2,3
   SIGNAL s_DCLinkTSS_V_ADC2_13_err: STD_LOGIC:='0'; SIGNAL s_sync_DCLinkTSS_V_ADC2_13_err : STD_LOGIC:='0'; -- Dc Sp. Erfassung Uzwpos, Uzwneg, U BuckBoost
   SIGNAL s_DC_OutVolt_ADC2_46_err : STD_LOGIC:='0'; SIGNAL s_sync_DC_OutVolt_ADC2_46_err  : STD_LOGIC:='0'; -- Dc Sp.  Ausgangsspannung pos, neg, gesamt
   SIGNAL s_RdampVolt_ADC3_13_err  : STD_LOGIC:='0'; SIGNAL s_sync_RdampVolt_ADC3_13_err   : STD_LOGIC:='0'; -- Dc Sp. Spannung auf den Dumpwiderstand1 und 2
   SIGNAL s_DC_Current1_ADC3_46_err: STD_LOGIC:='0'; SIGNAL s_sync_DC_Current1_ADC3_46_err : STD_LOGIC:='0'; -- BuckBoost u. Resonanzstrom der H-Bruecke
   SIGNAL s_DC_Current2_ADC4_13_err: STD_LOGIC:='0'; SIGNAL s_sync_DC_Current2_ADC4_13_err : STD_LOGIC:='0'; -- Strom in dem FreilaufThyristor im Ausgangs
   SIGNAL s_Reserve_ADC4_46_err    : STD_LOGIC:='0'; SIGNAL s_sync_Reserve_ADC4_46_err     : STD_LOGIC:='0'; -- Reserve

   -- Fehlermeldungen der Interlockkarten
   SIGNAL s_Interlock1_Goup1_err   : STD_LOGIC:='0'; SIGNAL s_sync_Interlock1_Goup1_err  : STD_LOGIC:='0'; -- 3LShoke, 55°C W.Cool.Plate -> Feed Energy back
   SIGNAL s_Interlock1_Goup2_err   : STD_LOGIC:='0'; SIGNAL s_sync_Interlock1_Goup2_err  : STD_LOGIC:='0'; -- Door, MainFuse,400V Electr. a. FI-> Abschaltung
   SIGNAL s_Interlock1_Goup3_err   : STD_LOGIC:='0'; SIGNAL s_sync_Interlock1_Goup3_err  : STD_LOGIC:='0'; -- 3LShoke 125°C,Load-,Disch.Res.120°C,-> Abschalt.
   SIGNAL s_Interlock1_Goup4_err   : STD_LOGIC:='0'; SIGNAL s_sync_Interlock1_Goup4_err  : STD_LOGIC:='0'; -- Load.Res 20A, 24V El.Fuse,24VIGBTFuse,Fast Shut.

   SIGNAL s_Interlock2_Goup1_err   : STD_LOGIC:='0'; SIGNAL s_sync_Interlock2_Goup1_err  : STD_LOGIC:='0'; --BB_Shoke110°C,Res.Shoke+Trafo110°C,Out.Sh. 110°C -> Feed Energy back.
   SIGNAL s_Interlock2_Goup2_err   : STD_LOGIC:='0'; SIGNAL s_sync_Interlock2_Goup2_err  : STD_LOGIC:='0'; --BBShoke125°C,Res.Shoke+Trafo125°C,UPS_OK,RFilter 120°C -> Abschaltung
   SIGNAL s_Interlock2_Goup3_err   : STD_LOGIC:='0'; SIGNAL s_sync_Interlock2_Goup3_err  : STD_LOGIC:='0'; --OutShoke 125°C,I_UPS, DCCT 24V,Rdamp 120°C-> Absch.
   SIGNAL s_Interlock2_Goup4_err   : STD_LOGIC:='0'; SIGNAL s_sync_Interlock2_Goup4_err  : STD_LOGIC:='0'; --Cryo Temp Mag,MPS,PPS, Fast Shutd. -> Abschaltung

   SIGNAL s_Tripline_1_in          : STD_LOGIC:='0'; SIGNAL s_sync_Tripline_1_in         : STD_LOGIC:='0';
   SIGNAL s_Tripline_2_in          : STD_LOGIC:='0'; SIGNAL s_sync_Tripline_2_in         : STD_LOGIC:='0';
   SIGNAL s_Tripline_3_in          : STD_LOGIC:='0'; SIGNAL s_sync_Tripline_3_in         : STD_LOGIC:='0';

   SIGNAL s_FPGA_Tripline_out      : STD_LOGIC_VECTOR (2 downto 0);

   TYPE STATE_TRIPLINE IS       (FIRST_TIMER_AFTER_POWERUP,
                                 FIRST_READ_ERROR_AFTER_POWERUP,
                                 FIRST_RESET_AFTER_POWERUP, RESET_TIME, TIME_AFTER_FIRST_RESET,
                                 AFTER_FIRST_RESET_READ_BACK,WAIT_AFTER_READ_BACK,
                                 FPGA_TRIPLINE_TEST,
                                 SECOND_RESET_TIME,WAIT_AFTER_SECOND_RESET,
                                 WRITE_ERROR,
                                 TRIPLINE_IGBT_LOCK_OUTPUTS,
                                 WAIT_FOR_RESET_SIGNAL );
   SIGNAL s_STATE_TRIPLINE            : STATE_TRIPLINE         := FIRST_TIMER_AFTER_POWERUP;

   TYPE STATE_ON_OFF_Prozedur IS(WAIT_FOR_ON_COMMAND,
                                 WAIT_FOR_DISCHARGING_SWITCH,
                                 TIME_FOR_CHARGE,
                                 WAIT_FOR_ACT_RECT_CONSTMOD_ON,
                                 WAIT_FOR_MAIN_SWITCH_ON,
                                 WAIT_FOR_CHARGE_SWITCH_OFF,
                                 WAIT_FOR_ACT_RECT_CONTROL_ON ,
                                 WAIT_FOR_ACT_RECT_SWITCH_FEEDFORWARD,
                                 WAIT_FOR_OFF_COMMAND,
                                 COUNTER_RESET_OFF,
                                 WAIT_AFTER_MAIN_SWITCH_OFF,
                                 DECHARGING,
                                 TIME_FOR_DECHARGE );
   SIGNAL s_STATE_ON_OFF_Prozedur     : STATE_ON_OFF_Prozedur := WAIT_FOR_ON_COMMAND;


    TYPE STATE_QP_Prozedur IS   (WAIT_FOR_QP_ENABLE,
                                 KS_ON,
                                 THYRISTOR_OFF,
                                 WAIT_FOR_QP1_ON,
                                 WAIT_FOR_QP2_ON,
                                 KS_OFF,KS_OFF_WAIT,
                                 QP_CIRCUIT_READY,
                                 QP_OFF,
                                 WAIT_SWITCH_OFF );
   SIGNAL s_STATE_QP_Prozedur        : STATE_QP_Prozedur      := WAIT_FOR_QP_ENABLE;

BEGIN

-----------Synchronisation-----------------
Inputs_Synchronization_p : process (clk, reset)
  BEGIN
      if (reset = '1') then
         --Error Inputs IGBTs-----------------------------------------
         s_sync_L3_Modul_W_err          <= '0';              s_L3_Modul_W_err          <= '0'; -- IGBT Module fuer 3L-Konverter
         s_sync_L3_Modul_V_err          <= '0';              s_L3_Modul_V_err          <= '0';
         s_sync_L3_Modul_U_err          <= '0';              s_L3_Modul_U_err          <= '0';

         s_sync_TSS_V13orV14_err        <= '0';              s_TSS_V13orV14_err        <= '0'; -- IGBT Module fuer Tief-/Hoch Setzsteller
         s_sync_TSS_V15orV16_err        <= '0';              s_TSS_V15orV16_err        <= '0';

         s_sync_HB_V17orV18_err         <= '0';              s_HB_V17orV18_err         <= '0'; -- IGBT Module fuer Resonanzkonvert Pimaerseite H-Bruecke
         s_sync_HB_V19orV20_err         <= '0';              s_HB_V19orV20_err         <= '0';

         s_sync_Sek_V21orV22_err        <= '0';              s_Sek_V21orV22_err        <= '0'; -- IGBT Module fuer Resonanzkonvert Sekundaerseite
         s_sync_Sek_V23orV24_err        <= '0';              s_Sek_V23orV24_err        <= '0';

         --Error von den Schuetzen-----------------------------------------
         s_sync_PrimSeiteSchutz_err     <= '0';              s_PrimSeiteSchutz_err     <= '0'; -- Relaiskarte Schuetze der Prim. Seite -Haupt-,Lade-Entlade
         s_sync_QpSchutz_err            <= '0';              s_QpSchutz_err            <= '0'; -- Relaiskarte QP Schuetze - QP_Pos, FL_Thyristor, Kurzschliesser
         s_sync_Qp2_err                 <= '0';              s_Qp2_err                 <= '0'; -- Relaiskarte QP Schuetze - QP_neg (redundant, extra Ausgang)
         s_sync_Umschalter_err          <= '0';              s_Umschalter_err          <= '0'; -- Umschalter parallel zu den IGBTs auf der Sek. Seite

         --ADC-Eingaenge-----------------------------------------
         s_sync_PhVoltage_ADC1_13_err   <= '0';              s_PhVoltage_ADC1_13_err   <= '0'; -- Uph1,Uph2,Uph3
         s_sync_PhCurrent_ADC1_46_err   <= '0';              s_PhCurrent_ADC1_46_err   <= '0'; -- Iph1, Iph2, Iph3
         s_sync_DCLinkTSS_V_ADC2_13_err <= '0';              s_DCLinkTSS_V_ADC2_13_err <= '0'; -- Dc Sp. Erfassung Uzwpos, Uzwneg, U BuckBoost
         s_sync_DC_OutVolt_ADC2_46_err  <= '0';              s_DC_OutVolt_ADC2_46_err  <= '0'; -- Ausgangsspannung pos, neg, gesamt
         s_sync_RdampVolt_ADC3_13_err   <= '0';              s_RdampVolt_ADC3_13_err   <= '0'; -- Spannung auf den Dumpwiderstand1 und Dampwiderstand2
         s_sync_DC_Current1_ADC3_46_err <= '0';              s_DC_Current1_ADC3_46_err <= '0'; -- BuckBoost Strom und Resonanzstrom der Primaers. der H-Bruecke
         s_sync_DC_Current2_ADC4_13_err <= '0';              s_DC_Current2_ADC4_13_err <= '0'; -- Strom in dem FreilaufThyristor und Strom im Ausgangs
         s_sync_Reserve_ADC4_46_err     <= '0';              s_Reserve_ADC4_46_err     <= '0'; -- 1=OK Reserve

         --InterlockBoards-----------------------------------------
         s_sync_Interlock1_Goup1_err    <= '0';              s_Interlock1_Goup1_err    <= '0'; -- 3L-Input Shoke, 55°C Watter Cooling Plate -> Feed back
         s_sync_Interlock1_Goup2_err    <= '0';              s_Interlock1_Goup2_err    <= '0'; -- Door, MainFuse, 400Vand FI for Electronic, -> Abschaltung
         s_sync_Interlock1_Goup3_err    <= '0';              s_Interlock1_Goup3_err    <= '0'; -- 3LShoke 125°C, LoadRes. 120°C, Disch.Res. 120°C,C.Plate 65°C
         s_sync_Interlock1_Goup4_err    <= '0';              s_Interlock1_Goup4_err    <= '0'; -- ILoad.Res >20A, 24V El. Fuse, 24V IGBT Driver Fuse,Fast Shut.

         s_sync_Interlock2_Goup1_err    <= '0';              s_Interlock2_Goup1_err    <= '0'; -- BBShoke110°,Res.Shoke+Trafo110°, Outp.Shoke 110°C Feed Back
         s_sync_Interlock2_Goup2_err    <= '0';              s_Interlock2_Goup2_err    <= '0'; -- BBShoke 125°,Res.Shoke+Trafo125°C, UPS OK,RFilter120° -> Absch
         s_sync_Interlock2_Goup3_err    <= '0';              s_Interlock2_Goup3_err    <= '0'; -- OutShoke 125°C,I_UPS, DCCT 24V,Rdamp 120°C-> Absch.
         s_sync_Interlock2_Goup4_err    <= '0';              s_Interlock2_Goup4_err    <= '0'; -- Cryo Temp Mag, MPS, PPS, Fast ShutdownSwitch -> Abschaltung

         s_sync_Tripline_1_in           <= '0';               s_Tripline_1_in          <= '0';
         s_sync_Tripline_2_in           <= '0';               s_Tripline_2_in          <= '0';
         s_sync_Tripline_3_in           <= '0';               s_Tripline_3_in          <= '0';

      elsif (rising_edge (clk)) then
         --Error Inputs IGBTs-----------------------------------------
         s_sync_L3_Modul_W_err          <= L3_Modul_W_err;              s_L3_Modul_W_err          <= s_sync_L3_Modul_W_err;   -- IGBT Module fuer 3L-Konverter
         s_sync_L3_Modul_V_err          <= L3_Modul_V_err;              s_L3_Modul_V_err          <= s_sync_L3_Modul_V_err;
         s_sync_L3_Modul_U_err          <= L3_Modul_U_err;              s_L3_Modul_U_err          <= s_sync_L3_Modul_U_err;
         s_sync_TSS_V13orV14_err        <= TSS_V13orV14_err;            s_TSS_V13orV14_err        <= s_sync_TSS_V13orV14_err; -- IGBTs Tief-/Hoch Setzsteller
         s_sync_TSS_V15orV16_err        <= TSS_V15orV16_err;            s_TSS_V15orV16_err        <= s_sync_TSS_V15orV16_err;
         s_sync_HB_V17orV18_err         <= HB_V17orV18_err;             s_HB_V17orV18_err         <= s_sync_HB_V17orV18_err;  -- Resonanzkonvert H-Bruecke
         s_sync_HB_V19orV20_err         <= HB_V19orV20_err;             s_HB_V19orV20_err         <= s_sync_HB_V19orV20_err;
         s_sync_Sek_V21orV22_err        <= Sek_V21orV22_err;            s_Sek_V21orV22_err        <= s_sync_Sek_V21orV22_err; -- Resonanzkonvert Sekundaerseite
         s_sync_Sek_V23orV24_err        <= Sek_V23orV24_err;            s_Sek_V23orV24_err        <= s_sync_Sek_V23orV24_err;

         --Error von den Schuetzen-----------------------------------------
         s_sync_PrimSeiteSchutz_err     <= PrimSeiteSchutz_err;         s_PrimSeiteSchutz_err     <= s_sync_PrimSeiteSchutz_err; -- Relaiskarte Prim.
         s_sync_QpSchutz_err            <= QpSchutz_err;                s_QpSchutz_err            <= s_sync_QpSchutz_err;        -- Relaiskarte Sek
         s_sync_Qp2_err                 <= Qp2_err;                     s_Qp2_err                 <= s_sync_Qp2_err;             -- Relaiskarte QP_neg
         s_sync_Umschalter_err          <= Umschalter_err;              s_Umschalter_err          <= s_sync_Umschalter_err;      -- Umschalter

         --ADC-Eingaenge-----------------------------------------
         s_sync_PhVoltage_ADC1_13_err   <= PhVoltage_ADC1_13_err;       s_PhVoltage_ADC1_13_err   <= s_sync_PhVoltage_ADC1_13_err;   -- Uph1,Uph2,Uph3
         s_sync_PhCurrent_ADC1_46_err   <= PhCurrent_ADC1_46_err;       s_PhCurrent_ADC1_46_err   <= s_sync_PhCurrent_ADC1_46_err;   -- Iph1, Iph2, Iph3
         s_sync_DCLinkTSS_V_ADC2_13_err <= DCLinkTSS_V_ADC2_13_err;     s_DCLinkTSS_V_ADC2_13_err <= s_sync_DCLinkTSS_V_ADC2_13_err; -- Uzwpos,neg, U BuckBoost
         s_sync_DC_OutVolt_ADC2_46_err  <= DC_OutVolt_ADC2_46_err ;     s_DC_OutVolt_ADC2_46_err  <= s_sync_DC_OutVolt_ADC2_46_err ; -- Uout pos, neg, gesamt
         s_sync_RdampVolt_ADC3_13_err   <= RdampVolt_ADC3_13_err;       s_RdampVolt_ADC3_13_err   <= s_sync_RdampVolt_ADC3_13_err;   -- Urdamp1,2
         s_sync_DC_Current1_ADC3_46_err <= DC_Current1_ADC3_46_err;     s_DC_Current1_ADC3_46_err <= s_sync_DC_Current1_ADC3_46_err; -- I_BuckBoost Iresonanz
         s_sync_DC_Current2_ADC4_13_err <= DC_Current2_ADC4_13_err;     s_DC_Current2_ADC4_13_err <= s_sync_DC_Current2_ADC4_13_err; -- I Th, Iout
         s_sync_Reserve_ADC4_46_err     <= Reserve_ADC4_46_err;         s_Reserve_ADC4_46_err     <= s_sync_Reserve_ADC4_46_err;     -- Reserve

         --InterlockBoards-----------------------------------------
         s_sync_Interlock1_Goup1_err    <= Interlock1_Goup1_err;        s_Interlock1_Goup1_err    <= s_sync_Interlock1_Goup1_err;
         s_sync_Interlock1_Goup2_err    <= Interlock1_Goup2_err;        s_Interlock1_Goup2_err    <= s_sync_Interlock1_Goup2_err;
         s_sync_Interlock1_Goup3_err    <= Interlock1_Goup3_err;        s_Interlock1_Goup3_err    <= s_sync_Interlock1_Goup3_err;
         s_sync_Interlock1_Goup4_err    <= Interlock1_Goup4_err;        s_Interlock1_Goup4_err    <= s_sync_Interlock1_Goup4_err;

         s_sync_Interlock2_Goup1_err    <= Interlock2_Goup1_err;        s_Interlock2_Goup1_err    <= s_sync_Interlock2_Goup1_err;
         s_sync_Interlock2_Goup2_err    <= Interlock2_Goup2_err;        s_Interlock2_Goup2_err    <= s_sync_Interlock2_Goup2_err;
         s_sync_Interlock2_Goup3_err    <= Interlock2_Goup3_err;        s_Interlock2_Goup3_err    <= s_sync_Interlock2_Goup3_err;
         s_sync_Interlock2_Goup4_err    <= Interlock2_Goup4_err;        s_Interlock2_Goup4_err    <= s_sync_Interlock2_Goup4_err;

         --Tripline-----------------------------------------------
         s_sync_Tripline_1_in           <= Tripline_1_in;               s_Tripline_1_in           <= s_sync_Tripline_1_in;
         s_sync_Tripline_2_in           <= Tripline_2_in;               s_Tripline_2_in           <= s_sync_Tripline_2_in;
         s_sync_Tripline_3_in           <= Tripline_3_in;               s_Tripline_3_in           <= s_sync_Tripline_3_in;
      end if;
  end process  Inputs_Synchronization_p;

-----------Reglersperre-----------------
Controler_Lock_p : process (clk, reset, enable)
   BEGIN
      if (reset = '1') then
         Resonanz_enable      <= '0';
         BuckBost_enable      <= '0';
         Level3_PWM_enable    <= '0';

      elsif (rising_edge (clk)) then
         if (MFU_Command = to_unsigned(4,4) OR enable ='0') then -- Reglersperre
            Resonanz_enable   <= '0';                            -- FPGA Module werden gesperrt
            BuckBost_enable   <= '0';
            Level3_PWM_enable <= '0';
         else
            Resonanz_enable   <= s_Resonanz_enable;
            BuckBost_enable   <= s_BuckBost_enable;
            Level3_PWM_enable <= s_Level3_PWM_enable;
         end if;
      end if;
  end process  Controler_Lock_p;

---1ms Timer-----------------------------------------------------------------------------------
Timer1_ms_p : process (clk, reset, s_startCounter1)                 -- s_WaitCounterMax Angabe in ms
   BEGIN
      IF (reset = '1') then
         s_WaitCounter1    <=(others => '0');
         s_Timer_intern1   <=(others => '0');
         s_counter_ready1  <='0';
         s_STATE_COUNTER1  <= WARTEN_AUF_START;

      ELSIF (rising_edge(clk)) THEN
         CASE(s_STATE_COUNTER1) IS
            ------------------------------------------------------------------
            WHEN WARTEN_AUF_START =>
            ------------------------------------------------------------------
               s_counter_ready1    <='0';
               if (s_startCounter1  = '1') then
                  s_STATE_COUNTER1 <= ZAELEN;
               end if;

            ------------------------------------------------------------------
            WHEN ZAELEN =>
            ------------------------------------------------------------------
               if (s_WaitCounter1 < to_unsigned(gTimer_Test,18))  then                          -- Aeussere Schleife
                  if(s_Timer_intern1 < to_unsigned(gInternTimer_loop - 4, 17)) then   -- eine Schleife entspicht 1ms = 100000
                     s_Timer_intern1 <= s_Timer_intern1 + 1;                          -- Inere Schleife
                  else
                     s_Timer_intern1 <=(others => '0');
                     s_WaitCounter1  <= s_WaitCounter1 +1;
                  end if;
               else
                  s_WaitCounter1     <=(others => '0');
                  s_STATE_COUNTER1   <= AUSGABE;
               end if;
            ------------------------------------------------------------------
            WHEN AUSGABE =>
            ------------------------------------------------------------------
               s_counter_ready1  <='1';
               s_STATE_COUNTER1  <= WARTEN_AUF_START;

         END CASE;
      END IF;
  end process  Timer1_ms_p;


---1ms zweiter Timer---------------------------------------------------------------------------
Timer2_ms_p : process (clk, reset, s_resetTimer2, s_startCounter2)  -- s_WaitCounterMax Angabe in ms
   BEGIN
      IF (reset = '1' OR s_resetTimer2 = '1') then
         s_WaitCounter2    <=(others => '0');
         s_Timer_intern2   <=(others => '0');
         s_counter_ready2  <='0';
         s_STATE_COUNTER2  <= WARTEN_AUF_START;

      ELSIF (rising_edge(clk)) THEN
         CASE(s_STATE_COUNTER2) IS
            ------------------------------------------------------------------
            WHEN WARTEN_AUF_START =>
            ------------------------------------------------------------------
               s_counter_ready2    <='0';
               if (s_startCounter2 = '1') then
                  s_STATE_COUNTER2 <= ZAELEN;
               end if;
            ------------------------------------------------------------------
            WHEN ZAELEN =>
            ------------------------------------------------------------------
               if (s_WaitCounter2 < s_WaitCounterMax2)  then                         -- Aeussere Schleife
                  if(s_Timer_intern2 < to_unsigned(gInternTimer_loop - 4, 17)) then  -- eine Schleife entspicht 1ms = 100000
                     s_Timer_intern2 <= s_Timer_intern2 + 1;                         -- Inere Schleife
                  else
                     s_Timer_intern2 <=(others => '0');
                     s_WaitCounter2  <= s_WaitCounter2 +1;
                  end if;
               else
                  s_WaitCounter2     <=(others => '0');
                  s_STATE_COUNTER2   <= AUSGABE;
               end if;
            ------------------------------------------------------------------
            WHEN AUSGABE =>
            ------------------------------------------------------------------
               s_counter_ready2  <='1';
               s_STATE_COUNTER2  <= WARTEN_AUF_START;
         END CASE;
      END IF;
  end process  Timer2_ms_p;

  ---1ms 3. Timer------------------------------------------------------------------------------
Timer3_ms_p : process (clk, reset, s_resetTimer3, s_startCounter3)  -- s_WaitCounterMax Angabe in ms
   BEGIN
      IF (reset = '1' OR s_resetTimer3 ='1') then
         s_WaitCounter3    <=(others => '0');
         s_Timer_intern3   <=(others => '0');
         s_counter_ready3  <='0';
         s_STATE_COUNTER3  <= WARTEN_AUF_START;

      ELSIF (rising_edge(clk)) THEN
         CASE(s_STATE_COUNTER3) IS
            ------------------------------------------------------------------
            WHEN WARTEN_AUF_START =>
            ------------------------------------------------------------------
               s_counter_ready3    <='0';
               if (s_startCounter3 = '1') then
                  s_STATE_COUNTER3 <= ZAELEN;
               end if;
            ------------------------------------------------------------------
            WHEN ZAELEN =>
            ------------------------------------------------------------------
               if (s_WaitCounter3 < s_WaitCounterMax3)  then                         -- Aeussere Schleife
                  if(s_Timer_intern3 < to_unsigned(gInternTimer_loop - 4, 17)) then  -- eine Schleife entspicht 1ms = 100000
                     s_Timer_intern3 <= s_Timer_intern3 + 1;                         -- Inere Schleife
                  else
                     s_Timer_intern3 <=(others => '0');
                     s_WaitCounter3  <= s_WaitCounter3 +1;
                  end if;
               else
                  s_WaitCounter3     <=(others => '0');
                  s_STATE_COUNTER3   <= AUSGABE;
               end if;
            ------------------------------------------------------------------
            WHEN AUSGABE =>
            ------------------------------------------------------------------
               s_counter_ready3  <='1';
               s_STATE_COUNTER3  <= WARTEN_AUF_START;
         END CASE;
      END IF;
  end process  Timer3_ms_p;

--Error_Prozess-----------------------------------------------------------
Error_p : PROCESS (clk, reset)
   BEGIN
      IF (reset = '1') THEN
         s_ErrorBand             <=(others => '0');  -- Alle Fehler loeschen
         ErrorBand               <=(others => '0');  -- Alle Fehler loeschen
         s_FPGA_Tripline_out     <=(others => '0');  -- drei Triplines sind getrennt
         s_first_reset_redy      <= '0';             -- nach dem ersten Reset =1
         s_PrimSwitchProz_enable <= '0';             -- Prozess fuer die Schalterabarbeitung ist nicht freigegeben
         resetAllCards           <= '0';             -- kein Reset der Karten, die Karten werden erst spaeter resetet
         s_STATE_TRIPLINE        <= FIRST_TIMER_AFTER_POWERUP;

      ELSIF (rising_edge(clk)) THEN
         ErrorBand         <= s_ErrorBand;
         FPGA_Tripline_out <= s_FPGA_Tripline_out;

         CASE(s_STATE_TRIPLINE) IS
            ------------------------------------------------------------------
            WHEN FIRST_TIMER_AFTER_POWERUP =>      -- nach dem Einschalten 1sec abwarten
            ------------------------------------------------------------------
               if (MFU_Command = to_unsigned(3,4)) then     -- Warte auf Reset-Kommando von der MFU
                                                            -- sonst etwas schwirig zum Kotrolieren, da .sof und nicht .pof Datei beim Debuging
                                                            -- das ist dann Startsignal zum Start der Kontrolle
                  s_FPGA_Tripline_out <=(others => '0');    -- drei Triplines sind getrennt
                  s_startCounter1     <= '1';               -- Starte 1.Counter Prozess
                  s_STATE_TRIPLINE    <= FIRST_READ_ERROR_AFTER_POWERUP;
               else
                  s_startCounter1     <= '0';
               end if;

            ------------------------------------------------------------------
            WHEN FIRST_READ_ERROR_AFTER_POWERUP => -- alle Karten muessen nach dem Einschalten Fehler liefern(insg.29 Karten +3Triplines)
            ------------------------------------------------------------------
               s_startCounter1   <= '0';           -- Counter kann zweites Mal nicht starten
               IF (s_counter_ready1  ='1') THEN    -- Zeit ist abgelaufen, beim ersten Einschalten muessen alle Karten ='0' (Fehler) liefern
                  if (s_L3_Modul_W_err           = '1')   then s_ErrorBand(0)  <= '1'; end if; --Error Inputs IGBTs------------------------------------
                  if (s_L3_Modul_V_err           = '1')   then s_ErrorBand(1)  <= '1'; end if;
                  if (s_L3_Modul_U_err           = '1')   then s_ErrorBand(2)  <= '1'; end if;
                  if (s_TSS_V13orV14_err         = '1')   then s_ErrorBand(3)  <= '1'; end if;
                  if (s_TSS_V15orV16_err         = '1')   then s_ErrorBand(4)  <= '1'; end if;
                  if (s_HB_V17orV18_err          = '1')   then s_ErrorBand(5)  <= '1'; end if;
                  if (s_HB_V19orV20_err          = '1')   then s_ErrorBand(6)  <= '1'; end if;
                  if (s_Sek_V21orV22_err         = '1')   then s_ErrorBand(7)  <= '1'; end if;
                  if (s_Sek_V23orV24_err         = '1')   then s_ErrorBand(8)  <= '1'; end if;

                  if (s_PrimSeiteSchutz_err      = '1')   then s_ErrorBand(9)  <= '1'; end if; --Error von den Schuetzen-------------------------------
                  if (s_QpSchutz_err             = '1')   then s_ErrorBand(10) <= '1'; end if;
                  if (s_Qp2_err                  = '1')   then s_ErrorBand(11) <= '1'; end if;
                  if (s_Umschalter_err           = '1')   then s_ErrorBand(12) <= '1'; end if;

                  if (s_PhVoltage_ADC1_13_err    = '1')   then s_ErrorBand(13) <= '1'; end if; --ADC-Eingaenge-----------------------------------------
                  if (s_PhCurrent_ADC1_46_err    = '1')   then s_ErrorBand(14) <= '1'; end if;
                  if (s_DCLinkTSS_V_ADC2_13_err  = '1')   then s_ErrorBand(15) <= '1'; end if;
                  if (s_DC_OutVolt_ADC2_46_err   = '1')   then s_ErrorBand(16) <= '1'; end if;
                  if (s_RdampVolt_ADC3_13_err    = '1')   then s_ErrorBand(17) <= '1'; end if;
                  if (s_DC_Current1_ADC3_46_err  = '1')   then s_ErrorBand(18) <= '1'; end if;
                  if (s_DC_Current2_ADC4_13_err  = '1')   then s_ErrorBand(19) <= '1'; end if;

                  --if (s_Reserve_ADC4_46_err      = '1')   then s_ErrorBand(20) <= '1'; end if;
                  if (s_DC_Current2_ADC4_13_err  = '1')   then s_ErrorBand(20) <= '1'; end if;

                  if (s_Interlock1_Goup1_err     = '1')   then s_ErrorBand(21) <= '1'; end if; --InterlockBoards---------------------------------------
                  if (s_Interlock1_Goup2_err     = '1')   then s_ErrorBand(22) <= '1'; end if;
                  if (s_Interlock1_Goup3_err     = '1')   then s_ErrorBand(23) <= '1'; end if;
                  if (s_Interlock1_Goup4_err     = '1')   then s_ErrorBand(24) <= '1'; end if;

                  if (s_Interlock2_Goup1_err     = '1')   then s_ErrorBand(25) <= '1'; end if;
                  if (s_Interlock2_Goup2_err     = '1')   then s_ErrorBand(26) <= '1'; end if;
                  if (s_Interlock2_Goup3_err     = '1')   then s_ErrorBand(27) <= '1'; end if;
                  if (s_Interlock2_Goup4_err     = '1')   then s_ErrorBand(18) <= '1'; end if;

                  if (s_Tripline_1_in            = '1')   then s_ErrorBand(29) <= '1'; end if; -- Triplines--- =0 beim Einschalten
                  if (s_Tripline_2_in            = '1')   then s_ErrorBand(30) <= '1'; end if;
                  if (s_Tripline_3_in            = '1')   then s_ErrorBand(31) <= '1'; end if;

                  s_STATE_TRIPLINE       <= FIRST_RESET_AFTER_POWERUP;
               END IF;

            ------------------------------------------------------------------
            WHEN FIRST_RESET_AFTER_POWERUP =>
            ------------------------------------------------------------------
               if (MFU_Command = to_unsigned(3,4) OR s_first_reset_redy = '0') then -- Warte auf Resetkommando von der MFU
                  resetAllCards          <= '1';                                    -- Alle Karten werden resetet
                  s_FPGA_Tripline_out    <=(others => '1');                         -- Der FPGA schliesst alle drei Triplines
                  s_startCounter1        <= '1';                                    -- Starte 1.Counter Prozess
                  s_STATE_TRIPLINE       <= RESET_TIME ;
               end if;

            -----------------------------------------------------------------
            WHEN RESET_TIME =>
            ------------------------------------------------------------------
               if (s_counter_ready1  ='1') THEN
                  resetAllCards          <= '0';
                  s_startCounter1        <= '1';                                 -- Starte Counter Prozess
                  s_STATE_TRIPLINE       <= TIME_AFTER_FIRST_RESET ;
               else
                  s_startCounter1        <= '0';
               end if;

            -----------------------------------------------------------------
            WHEN TIME_AFTER_FIRST_RESET =>
            ------------------------------------------------------------------
               if (s_counter_ready1  ='1') THEN
                 s_startCounter1        <= '1';                                 -- Starte Counter Prozess
                 s_STATE_TRIPLINE       <= AFTER_FIRST_RESET_READ_BACK;
               else
                  s_startCounter1       <= '0';
               end if;

            ------------------------------------------------------------------
            WHEN AFTER_FIRST_RESET_READ_BACK => -- wieder 29Karten + 3 Triplines werden geprueft
            ------------------------------------------------------------------
               IF (s_counter_ready1  ='1')                THEN                                 -- nach dem Reset muessen alle Karten ='1' (kein Fehler) liefern
                  if (s_L3_Modul_W_err           = '0')   then s_ErrorBand(32) <= '1'; end if; -- Error Inputs IGBTs-----------------------------------
                  if (s_L3_Modul_V_err           = '0')   then s_ErrorBand(33) <= '1'; end if;
                  if (s_L3_Modul_U_err           = '0')   then s_ErrorBand(34) <= '1'; end if;
                  if (s_TSS_V13orV14_err         = '0')   then s_ErrorBand(35) <= '1'; end if;
                  if (s_TSS_V15orV16_err         = '0')   then s_ErrorBand(36) <= '1'; end if;
                  if (s_HB_V17orV18_err          = '0')   then s_ErrorBand(37) <= '1'; end if;
                  if (s_HB_V19orV20_err          = '0')   then s_ErrorBand(38) <= '1'; end if;
                  if (s_Sek_V21orV22_err         = '0')   then s_ErrorBand(39) <= '1'; end if;
                  if (s_Sek_V23orV24_err         = '0')   then s_ErrorBand(40) <= '1'; end if;

                  if (s_PrimSeiteSchutz_err      = '0')   then s_ErrorBand(41) <= '1'; end if; -- Error von den Schuetzen------------------------------
                  if (s_QpSchutz_err             = '0')   then s_ErrorBand(42) <= '1'; end if;
                  if (s_Qp2_err                  = '0')   then s_ErrorBand(43) <= '1'; end if;
                  if (s_Umschalter_err           = '0')   then s_ErrorBand(44) <= '1'; end if;

                  if (s_PhVoltage_ADC1_13_err    = '0')   then s_ErrorBand(45) <= '1'; end if; -- ADC-Eingaenge-----------------------------------------
                  if (s_PhCurrent_ADC1_46_err    = '0')   then s_ErrorBand(46) <= '1'; end if;
                  if (s_DCLinkTSS_V_ADC2_13_err  = '0')   then s_ErrorBand(47) <= '1'; end if;
                  if (s_DC_OutVolt_ADC2_46_err   = '0')   then s_ErrorBand(48) <= '1'; end if;
                  if (s_RdampVolt_ADC3_13_err    = '0')   then s_ErrorBand(49) <= '1'; end if;
                  if (s_DC_Current1_ADC3_46_err  = '0')   then s_ErrorBand(50) <= '1'; end if;
                  if (s_DC_Current2_ADC4_13_err  = '0')   then s_ErrorBand(51) <= '1'; end if;

                  --if (s_Reserve_ADC4_46_err      = '0')   then s_ErrorBand(52) <= '1'; end if;
                  if (s_DC_Current2_ADC4_13_err  = '0')   then s_ErrorBand(52) <= '1'; end if;

                  if (s_Interlock1_Goup1_err     = '0')   then s_ErrorBand(53) <= '1'; end if; --InterlockBoards----------------------------------------
                  if (s_Interlock1_Goup2_err     = '0')   then s_ErrorBand(54) <= '1'; end if;
                  if (s_Interlock1_Goup3_err     = '0')   then s_ErrorBand(55) <= '1'; end if;
                  if (s_Interlock1_Goup4_err     = '0')   then s_ErrorBand(56) <= '1'; end if;

                  if (s_Interlock2_Goup1_err     = '0')   then s_ErrorBand(57) <= '1'; end if;
                  if (s_Interlock2_Goup2_err     = '0')   then s_ErrorBand(58) <= '1'; end if;
                  if (s_Interlock2_Goup3_err     = '0')   then s_ErrorBand(59) <= '1'; end if;
                  if (s_Interlock2_Goup4_err     = '0')   then s_ErrorBand(60) <= '1'; end if;

                  if (s_Tripline_1_in            = '0')   then s_ErrorBand(61) <= '1'; end if; -- Triplinen muessen durchgesch. sein
                  if (s_Tripline_2_in            = '0')   then s_ErrorBand(62) <= '1'; end if;
                  if (s_Tripline_3_in            = '0')   then s_ErrorBand(63) <= '1'; end if;

                  s_startCounter1        <= '1';
                  s_STATE_TRIPLINE       <= WAIT_AFTER_READ_BACK;
               ELSE
                  s_startCounter1        <= '0';
               END IF;

            ------------------------------------------------------------------
            WHEN WAIT_AFTER_READ_BACK => --
            ------------------------------------------------------------------
                IF (s_counter_ready1  ='1') THEN
                  resetAllCards           <= '0';
                  s_first_reset_redy      <= '1';

                  if (Tripline_mode = '1' ) then                                             -- Triplins werden beim Reset immer testiert
                     s_startCounter1      <= '1';                                            -- jetzt trennt der FPGA drei Triplinen
                     s_FPGA_Tripline_out  <= (others => '0');
                     s_STATE_TRIPLINE     <=  FPGA_TRIPLINE_TEST;                            -- TriplineTest durchfuehren

                  elsif (Tripline_mode     = '0' AND s_first_reset_redy = '0') then          -- Triplins werden nur beim ersten Reset testiert
                     s_startCounter1      <= '1';                                            -- Starte Counter Prozess
                     s_FPGA_Tripline_out  <= (others => '0');
                     s_STATE_TRIPLINE     <= FPGA_TRIPLINE_TEST;
                  else
                     s_startCounter1      <= '0';
                     s_FPGA_Tripline_out  <= (others => '1');
                     s_STATE_TRIPLINE     <= WRITE_ERROR;                                    -- Ueberspringe Triplinetest, lese fehler zurueck
                  end if;
               ELSE
                  s_startCounter1         <= '0';
               END IF;

            ------------------------------------------------------------------
            WHEN FPGA_TRIPLINE_TEST =>
            ------------------------------------------------------------------
               IF (s_counter_ready1   = '1') THEN
                  if (s_Tripline_1_in = '1') then s_ErrorBand (64)  <= '1'; end if; -- Triplinen muessen jetzt getrennt sein sonst Fehler
                  if (s_Tripline_2_in = '1') then s_ErrorBand (65)  <= '1'; end if;
                  if (s_Tripline_3_in = '1') then s_ErrorBand (66)  <= '1'; end if;

                  resetAllCards       <= '1';                                        -- Alle Karten werden kurz resetet, (wichtig aber nur RelaisKarte)
                  s_FPGA_Tripline_out <=(others => '1');                             -- Der FPGA schliesst alle drei Triplines
                  s_startCounter1     <= '1';                                        -- Starte Counter Prozess
                  s_STATE_TRIPLINE    <= SECOND_RESET_TIME ;
               ELSE
                  s_startCounter1     <= '0';
               END IF;

            ------------------------------------------------------------------
            WHEN SECOND_RESET_TIME =>
            ------------------------------------------------------------------
               IF (s_counter_ready1    = '1') THEN
                  resetAllCards       <= '0';
                  s_startCounter1     <= '1';                                -- Starte Counter Prozess
                  s_STATE_TRIPLINE    <= WAIT_AFTER_SECOND_RESET;
               ELSE
                  s_startCounter1     <= '0';
               end if;

            ------------------------------------------------------------------
            WHEN WAIT_AFTER_SECOND_RESET =>
            ------------------------------------------------------------------
               s_startCounter1        <= '0';
               IF (s_counter_ready1    = '1') THEN
                  resetAllCards       <= '0';
                  s_STATE_TRIPLINE    <= WRITE_ERROR ;
               end if;

            ------------===============================================------------------------------------------------------
            WHEN WRITE_ERROR => --Pruefe im Betrieb die Fehler
            ------------------------------------------------------------------
            if (s_L3_Modul_W_err           = '0')   then s_ErrorBand(67)  <= '1'; end if; --Error Inputs IGBTs-----------------------------------------
            if (s_L3_Modul_V_err           = '0')   then s_ErrorBand(68)  <= '1'; end if;
            if (s_L3_Modul_U_err           = '0')   then s_ErrorBand(69)  <= '1'; end if;

            if (s_TSS_V13orV14_err         = '0')   then s_ErrorBand(70)  <= '1'; end if;
            if (s_TSS_V15orV16_err         = '0')   then s_ErrorBand(71)  <= '1'; end if;

            if (s_HB_V17orV18_err          = '0')   then s_ErrorBand(72)  <= '1'; end if;
            if (s_HB_V19orV20_err          = '0')   then s_ErrorBand(73)  <= '1'; end if;
            if (s_Sek_V21orV22_err         = '0')   then s_ErrorBand(74)  <= '1'; end if;
            if (s_Sek_V23orV24_err         = '0')   then s_ErrorBand(75)  <= '1'; end if;

            if (s_PrimSeiteSchutz_err      = '0')   then s_ErrorBand(76)  <= '1'; end if; --Error von den Schuetzen-------------------------------------
            if (s_QpSchutz_err             = '0')   then s_ErrorBand(77)  <= '1'; end if;
            if (s_Qp2_err                  = '0')   then s_ErrorBand(78)  <= '1'; end if;
            if (s_Umschalter_err           = '0')   then s_ErrorBand(79)  <= '1'; end if;

            if (s_PhVoltage_ADC1_13_err    = '0')   then s_ErrorBand(80)  <= '1'; end if; --ADC-Eingaenge-----------------------------------------
            if (s_PhCurrent_ADC1_46_err    = '0')   then s_ErrorBand(81)  <= '1'; end if;
            if (s_DCLinkTSS_V_ADC2_13_err  = '0')   then s_ErrorBand(82)  <= '1'; end if;
            if (s_DC_OutVolt_ADC2_46_err   = '0')   then s_ErrorBand(83)  <= '1'; end if;
            if (s_RdampVolt_ADC3_13_err    = '0')   then s_ErrorBand(84)  <= '1'; end if;
            if (s_DC_Current1_ADC3_46_err  = '0')   then s_ErrorBand(85)  <= '1'; end if;
            if (s_DC_Current2_ADC4_13_err  = '0')   then s_ErrorBand(86)  <= '1'; end if;

            --if (s_Reserve_ADC4_46_err      = '0')   then s_ErrorBand(87)  <= '1'; end if;
            if (s_DC_Current2_ADC4_13_err  = '0')   then s_ErrorBand(87)  <= '1'; end if;

            if (s_Interlock1_Goup1_err     = '0')   then s_ErrorBand(88)  <= '1'; end if; --InterlockBoards-----------------------------------------
            if (s_Interlock1_Goup2_err     = '0')   then s_ErrorBand(89)  <= '1'; end if;
            if (s_Interlock1_Goup3_err     = '0')   then s_ErrorBand(90)  <= '1'; end if;
            if (s_Interlock1_Goup4_err     = '0')   then s_ErrorBand(91)  <= '1'; end if;

            if (s_Interlock2_Goup1_err     = '0')   then s_ErrorBand(92)  <= '1'; end if;
            if (s_Interlock2_Goup2_err     = '0')   then s_ErrorBand(93)  <= '1'; end if;
            if (s_Interlock2_Goup3_err     = '0')   then s_ErrorBand(94)  <= '1'; end if;
            if (s_Interlock2_Goup4_err     = '0')   then s_ErrorBand(95)  <= '1'; end if;

            if (s_Tripline_1_in = '0' AND s_FPGA_Tripline_out (0) = '1') then s_ErrorBand(96)  <= '1'; end if; -- Triplinen muessen durchgesch. sein
            if (s_Tripline_2_in = '0' AND s_FPGA_Tripline_out (1) = '1') then s_ErrorBand(97)  <= '1'; end if;
            if (s_Tripline_3_in = '0' AND s_FPGA_Tripline_out (2) = '1') then s_ErrorBand(98)  <= '1'; end if;


            s_STATE_TRIPLINE        <= TRIPLINE_IGBT_LOCK_OUTPUTS;

            ------------------------------------------------------------------
            WHEN TRIPLINE_IGBT_LOCK_OUTPUTS => --Tripline und IGBT_Lock setzen
            ------------------------------------------------------------------
            -- Erst warte auf Resetkommando (wird nur einmal durchgefuehrt):
               -- nach dem ersten Resetkommando sind die Triplinen von dem FPGA offen
               -- s_ErrorBand (28..0)  nach dem Einschalten muessen alle Karten Fehler(Input=0) liefern
               -- s_ErrorBand (31..29) nach dem Einschalten muessen die Triplines getrennt sein =0

               -- Warte auf Resetkommando oder 1.Durchlauf: Triplinen werden geschlossen u. 1.Resetsignal fuer alle Karten wird erzeugt und dann zurueckgelesen
               -- s_ErrorBand (60..32) nach dem Reset und Tripline =1  muessen die Karten kein Fehler(Input=1) liefern
               -- s_ErrorBand (63..61) nach dem Reset und Tripline =1  muessen die Triplines =1 sein

         --->Schleife:   FPGA_TRIPLINE_TEST:
               --Tripline-Mode1 o. 1.Durchlauf : s_ErrorBand (66..64) alle drei Tripline werden jetzt getrennt =0 -> dann wird zuruckgelesen, die Triplinen =0
                 -- danach 2. Reset fuer die Karten wird erzeugt. Der zweite Reset ist wichtig für die Relaiskarten, da diese nach dem Triplins getrennt wurden
                 -- in diesem Zustand ohne Resetsignal auch verbleibt
               --Tripline-Mode 0 geht direkt ohne Triplinetest (also Tripline werden nicht getrennt) und ohne Resetsignal  in den Schritt "WRITE_ERROR"


         --->Schleife: WRITE_ERROR:
               -- s_ErrorBand (95..67) Fehlermeldungen werden zuruckgelesen, duerfen keine Fehler anstehen =1

         --->Schleife: WAIT_FOR_RESET_SIGNAL
         --Resetmode = '0' verbleibe in diesem Schritt und warte auf Resetkommando, dann springe in FIRST_RESET_AFTER_POWERUP (d.h. die Fehler werden nur einmalig geschrieben bis Reset kommt)
         --> Resetmode = '1' springe in WRITE_ERROR (d.h. Fehler werden permanent geschrieben) Wenn das Resetkommando kommt wird auch in de FIRST_RESET_AFTER_POWERUP gesprungen

               -- Fehlepruefung nach dem Start
            --   if((UNSIGNED(s_ErrorBand (29 downto 0))  > 0) OR -- beim Start Karten haben sich nicht korrekt gemeldet
            --    (UNSIGNED(s_ErrorBand (31 downto 29)) > 0) OR -- beim Start Karten haben nicht Tripline=0 geliefert
                  
               if ((UNSIGNED(s_ErrorBand (60 downto 32)) > 0) OR -- nach dem 1.Reset muessen die Karten =1 liefern, Triplinen werden jetzt geschlossen
                  (UNSIGNED(s_ErrorBand (63 downto 61)) > 0) OR -- nach dem 1.Reset muessen die Triplinen =1 sein,Triplines werden jetzt getrennt
                  (UNSIGNED(s_ErrorBand (66 downto 64)) > 0) OR -- nach der Triplinetrennung musessen Triplins auch getrennt sein, jetzt kommt der zweiter Resetsignal
                  (UNSIGNED(s_ErrorBand (98 downto 67)) > 0))   -- Fehlerueberwachung im Laufendem Betrieb
               then
                  s_FPGA_Tripline_out (0) <= '0'; -- 1. Tripline wird unterbrochen
                  s_FPGA_Tripline_out (1) <= '0';
                  s_FPGA_Tripline_out (2) <= '0';

                  s_BuckBost_enable       <= '0'; -- FPGA Module werden angehalten
                  s_Resonanz_enable       <= '0'; -- Die Sekundaerseitige Transistoren werdenausgeschaltet, aber der Takt wird trotzdem zu ende Laufen
                  s_level3_PWM_enable     <= '0';

                  s_PrimSwitchProz_enable <= '0'; -- Schuetze der Pimaerseite (Entlade, Lade, Haupt lassen sich nicht einschalten)
                  s_QP_SwitchProz_Enable  <= '0'; -- Schuetze der Sekundaerseite (QP1,2, KS) lassen sich nicht einschalten, Thyristor bleibt AN

               else
                  s_FPGA_Tripline_out (0) <= '1';
                  s_FPGA_Tripline_out (1) <= '1';
                  s_FPGA_Tripline_out (2) <= '1';

                  s_BuckBost_enable       <= '1'; -- FPGA Module werden angehalten
                  s_Resonanz_enable       <= '1'; -- Die Sekundaerseitige Transistoren werdenausgeschaltet, aber der Takt wird trotzdem zu ende Laufen
                  s_level3_PWM_enable     <= '1';

                  s_PrimSwitchProz_enable <= '1';
                  s_QP_SwitchProz_Enable  <= '1';
               end if;

               s_STATE_TRIPLINE    <= WAIT_FOR_RESET_SIGNAL;

            -----------------------------------------------------------------
            WHEN WAIT_FOR_RESET_SIGNAL =>
            -----------------------------------------------------------------
               if  (MFU_Command = to_unsigned(3,4)) then                             -- Reset Kommando
                  --s_ErrorBand              <=(others => '0');                     -- alle Fehler auf Null (=1 liegt einen Fehler vor)
                  s_ErrorBand(127 downto 32) <=(others => '0');                      -- die Startfehler kann man nicht mit reset loeschen

                  s_STATE_TRIPLINE <= FIRST_RESET_AFTER_POWERUP;                     -- springt zum Reset und nicht zum ersten Schritt

               else                                                                  -- kein Resetsignal liegt vor
                  if (Reset_mode = '0' ) then                                        -- es wird nur der erste Fehler gespeicher
                     s_STATE_TRIPLINE    <= WAIT_FOR_RESET_SIGNAL;
                  else                                                               -- es werden alle Fehler gespeichert bis reset kommt
                     s_STATE_TRIPLINE    <= WRITE_ERROR;
                  end if;
               end if;

            ------------===============================================------------------------------------------------------
            WHEN OTHERS =>
            ------------------------------------------------------------------
               s_STATE_TRIPLINE    <= WAIT_FOR_RESET_SIGNAL;                        -- SPECIAL ERROR , aus irgendeinem Grund die gueltigen Zustände verlassen
         END CASE;

      END IF;
   END PROCESS Error_p;


ON_OFF_Prozedur_p : PROCESS (clk, reset)
   BEGIN
      IF (reset = '1') THEN
         switch_out                  <= "000"; -- (Entlade, Lade, Haupt) , also Entladeschutz ist aktiviert da NC, Lade und Haupt sind aus
         s_resetTimer2               <= '0';
         s_startCounter2             <= '0';
         s_WaitCounterMax2           <= (others => '0');

         ActivRectConstMod           <= '0';   -- =1 aktive Gleichrichtung wird mit einer konstanten Modulation "gesteuert" gestartet
         AktivRectPWMON              <= '0';   -- =1 PWM der aktiven Gleichrichtung aktiviert
         AktivRectControllON         <= '0';   -- =1 Regler fuer die aktive gleichrichtung aktivieren
         UebergangUPDown             <= '0';   -- Vorsteuerung 0->1 vom Generator auf vorsteuerung
         Umin_monitoringON           <= '1';   -- Die Ueberwachung der Unterspannung am Zwischenkreis ist deaktiviert
         Ready_for_operate           <= '0';   -- Hauptsch. ON, Lade OFF, die aktive Gleichrichtung laeuft
         s_Ready_for_operate         <= '0';
         s_STATE_ON_OFF_Prozedur     <= WAIT_FOR_ON_COMMAND;

      ELSIF (rising_edge(clk)) THEN
         Ready_for_operate           <= s_Ready_for_operate;

         CASE(s_STATE_ON_OFF_Prozedur) IS
            ------------------------------------------------------------------
            WHEN WAIT_FOR_ON_COMMAND =>  --Warte auf's Einschalten
            ------------------------------------------------------------------
               if (MFU_Command = to_unsigned(1,4) AND s_PrimSwitchProz_enable = '1' AND enable = '1')  then
                  switch_out                  <= "100";                                   -- Einschalten + keine Fehler stehen an   + Enable
                  s_startCounter2             <= '1';                                     -- Entlade Schutz AN -> Kontakte dadurch geoeffnet
                  s_WaitCounterMax2           <= to_unsigned(gDeChargeSwitchOFFTime,18);  -- und gehe weiter zur Einschalteprozedur
                  s_STATE_ON_OFF_Prozedur     <= WAIT_FOR_DISCHARGING_SWITCH ;
               end if;

            ------------------------------------------------------------------
            WHEN WAIT_FOR_DISCHARGING_SWITCH =>  --Warte bis Entladeuschutz aus ist
            ------------------------------------------------------------------
               if  (MFU_Command = to_unsigned(2,4) OR s_PrimSwitchProz_enable = '0')  then --Sofort ausschalten
                  s_resetTimer2               <= '1';
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gDeChargeSwitchOFFTime,18);
                  s_STATE_ON_OFF_Prozedur     <= COUNTER_RESET_OFF;           -- Ausschaltprozedur durchfueren

               elsif (s_counter_ready2   = '1') THEN                          -- Entladeschutz ist jetzt aus
                  switch_out                  <= "110";                       -- Ladeschutz einschalten
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gChargingTime,18);
                  s_STATE_ON_OFF_Prozedur     <= TIME_FOR_CHARGE;
               else
                  s_startCounter2             <= '0';
               end if;

            ------------------------------------------------------------------
            WHEN TIME_FOR_CHARGE =>
            ------------------------------------------------------------------
               if  (MFU_Command = to_unsigned(2,4) OR s_PrimSwitchProz_enable = '0')  then --Sofort ausschalten
                  s_resetTimer2               <= '1';
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gDeChargeSwitchOFFTime,18);
                  s_STATE_ON_OFF_Prozedur     <= COUNTER_RESET_OFF;

               elsif (s_counter_ready2   = '1') THEN        -- Ladeschutz ist jetzt eingeschaltet (Zw.Kreis mit Ladewiderstaenden aufgeladen)
                  Umin_monitoringON           <= '0';       -- Die Überwachung der Unterspannung am Zwischenkreis ist jetzt (auf DC-Sensing Karte) aktiviert
                  AktivRectPWMON              <= '1';       -- PWM der Act. Gleichrichtung ist ON
                  ActivRectConstMod           <= '1';       -- Modulation der Akt. Gleichrichtung auf constante Modulation gesteuert
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gActivRectConstMod,18);
                  s_STATE_ON_OFF_Prozedur     <= WAIT_FOR_ACT_RECT_CONSTMOD_ON;
               else
                  s_startCounter2             <= '0';
               end if;

            ------------------------------------------------------------------
            WHEN WAIT_FOR_ACT_RECT_CONSTMOD_ON =>
            ------------------------------------------------------------------
               if  (MFU_Command = to_unsigned(2,4) OR s_PrimSwitchProz_enable = '0')  then --Sofort ausschalten
                  s_resetTimer2               <= '1';
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gDeChargeSwitchOFFTime,18);
                  s_STATE_ON_OFF_Prozedur     <= COUNTER_RESET_OFF;

               elsif (s_counter_ready2   = '1') THEN      -- Aktive Gleichrichtung wurde aktiviert
                  AktivRectPWMON              <= '0';     -- PWM der Act. Gleichrichtung ist OFF
                  ActivRectConstMod           <= '0';     -- Modulation der Akt. Gleichrichtung auf geregelten Betrieb
                  switch_out                  <= "111";   -- Ladeschutz bleibt noch kurze Zeit an + Hauptschutz einschalten
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gMainSwitchONTime,18);
                  s_STATE_ON_OFF_Prozedur     <= WAIT_FOR_MAIN_SWITCH_ON;
               else
                  if (Uzw_ist >= Uzw_soll) then
                     AktivRectPWMON           <= '0';
                     ActivRectConstMod        <= '0';     -- Modulation der Akt. Gleichrichtung auf geregelten Betrieb
                  else
                     AktivRectPWMON           <= '1';     -- PWM ON
                     ActivRectConstMod        <= '1';     -- Modulation der Akt. Gleichrichtung auf gesteuerten Betrieb
                  end if;
                  s_startCounter2             <= '0';
               end if;

            ------------------------------------------------------------------
            WHEN WAIT_FOR_MAIN_SWITCH_ON =>
            ------------------------------------------------------------------
               if  (MFU_Command = to_unsigned(2,4) OR s_PrimSwitchProz_enable = '0')  then --Sofort ausschalten
                  s_resetTimer2               <= '1';
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gMainSwitchOFFTime,18);
                  s_STATE_ON_OFF_Prozedur     <= COUNTER_RESET_OFF;

               elsif (s_counter_ready2   = '1') THEN      -- Hauptschutz ist jetzt eingeschaltet
                  switch_out                  <= "101";   -- Ladeschutz ausschalten, Hauptschutz bleibt an
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gChargeSwitchOFFTime,18);
                  s_STATE_ON_OFF_Prozedur     <= WAIT_FOR_CHARGE_SWITCH_OFF;
               else
                  s_startCounter2             <= '0';
               end if;

            ------------------------------------------------------------------
            WHEN WAIT_FOR_CHARGE_SWITCH_OFF =>
            ------------------------------------------------------------------
               if  (MFU_Command = to_unsigned(2,4) OR s_PrimSwitchProz_enable = '0')  then --Sofort ausschalten
                  s_resetTimer2               <= '1';
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gMainSwitchOFFTime,18);
                  s_STATE_ON_OFF_Prozedur     <= COUNTER_RESET_OFF;

               elsif (s_counter_ready2   = '1') THEN      -- Hauptschutz ist jetzt eingeschaltet, Ladeschutz ausgeschaltet
                  AktivRectPWMON              <= '1';     -- PWM der Act. Gleichrichtung ist ON
                  AktivRectControllON         <= '1';     -- die Regelung der aktiven Gleichrichtung wird eingeschaltet
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gActivRectConstMod,18);
                  s_STATE_ON_OFF_Prozedur     <= WAIT_FOR_ACT_RECT_CONTROL_ON;
               else
                  s_startCounter2             <= '0';
               end if;

            ------------------------------------------------------------------
            WHEN WAIT_FOR_ACT_RECT_CONTROL_ON =>
            ------------------------------------------------------------------
               if  (MFU_Command = to_unsigned(2,4) OR s_PrimSwitchProz_enable = '0')  then --Sofort ausschalten
                  s_resetTimer2               <= '1';
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gMainSwitchOFFTime,18);
                  s_STATE_ON_OFF_Prozedur     <= COUNTER_RESET_OFF;

               elsif (s_counter_ready2   = '1') THEN      -- die Regelung der aktiven Gleichrichtung ist jetzt eingeschaltet vorst. auf Gen.
                  UebergangUPDown             <= '1';     -- Umschalte Vorsteuerung auf Messung
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gActivRectConstMod,18);
                  s_STATE_ON_OFF_Prozedur     <= WAIT_FOR_ACT_RECT_SWITCH_FEEDFORWARD;
               else
                  s_startCounter2             <= '0';
               end if;

            ------------------------------------------------------------------
            WHEN WAIT_FOR_ACT_RECT_SWITCH_FEEDFORWARD =>
            ------------------------------------------------------------------
               if  (MFU_Command = to_unsigned(2,4) OR s_PrimSwitchProz_enable = '0')  then --Sofort ausschalten
                  s_resetTimer2               <= '1';
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gMainSwitchOFFTime,18);
                  s_STATE_ON_OFF_Prozedur     <= COUNTER_RESET_OFF;

               elsif (s_counter_ready2   = '1') THEN      -- Umschaltung der Vorsteuerung auf Messung ist stattgefunden
                  s_Ready_for_operate         <= '1';
                  s_STATE_ON_OFF_Prozedur     <= WAIT_FOR_OFF_COMMAND;
               else
                  s_startCounter2             <= '0';
               end if;

            ------------------------------------------------------------------
            WHEN WAIT_FOR_OFF_COMMAND =>
            ------------------------------------------------------------------
               if (MFU_Command = to_unsigned(2,4) OR s_PrimSwitchProz_enable = '0')  then -- Ausschaltprozedur einleiten
                  s_Ready_for_operate         <= '0';
                  s_resetTimer2               <= '0';
                  s_startCounter2             <= '1';
                  s_WaitCounterMax2           <= to_unsigned(gMainSwitchOFFTime,18);
                  s_STATE_ON_OFF_Prozedur     <= COUNTER_RESET_OFF;
               else
                  Null;
               end if;

            ------------------------------------------------------------------
            WHEN COUNTER_RESET_OFF => 
            ------------------------------------------------------------------
               switch_out                  <= "100"; -- Lade Schutz AN-> Kontakte AUS + (Haupt- und Entladeschutz) AUS
               
               Umin_monitoringON           <= '1';   -- Die Überwachung der Unterspannung am Zwischenkreis ist jetzt (auf DC-Sensing Karte)  deaktiviert
               
               AktivRectPWMON              <= '0';   -- PWM der Akt. Gleichrichtung ist OFF
               AktivRectControllON         <= '0';   -- Regelung der aktiven Gleichrichtung abschalten 
               UebergangUPDown             <= '0';   -- Umschalte Vorsteuerung auf Gen. Betrieb um das naechstes Einschalten moeglich zu machen
               
               s_startCounter2             <= '1';
               s_resetTimer2               <= '0';
               s_STATE_ON_OFF_Prozedur     <= WAIT_AFTER_MAIN_SWITCH_OFF;

            ------------------------------------------------------------------
            WHEN WAIT_AFTER_MAIN_SWITCH_OFF =>  --Ausschaltprozedur
            ------------------------------------------------------------------
               s_startCounter2             <= '0';

               IF (s_counter_ready2   = '1') THEN      -- Hauptschutz ist jetzt eingeschaltet, Ladeschutz ausgeschaltet
                  s_STATE_ON_OFF_Prozedur  <= DECHARGING;
               end if;

            ------------------------------------------------------------------
            WHEN DECHARGING =>
            ------------------------------------------------------------------
               switch_out                  <= "000"; --Entladeschutz ausschalten kontakte sind jetzt geschlossen
               s_startCounter2             <= '1';
               s_WaitCounterMax2           <= to_unsigned(gDeChargeTime,18);
               s_STATE_ON_OFF_Prozedur     <= TIME_FOR_DECHARGE;

            ------------------------------------------------------------------
            WHEN TIME_FOR_DECHARGE =>
            ------------------------------------------------------------------
               s_startCounter2         <= '0';
               if (s_counter_ready2   = '1') THEN
                  s_STATE_ON_OFF_Prozedur     <= WAIT_FOR_ON_COMMAND; --auf das Einschalten Warten, Entladeschutz bleibt dabei ganze Zeit AN
               end if;

            ------------------------------------------------------------------
            WHEN OTHERS =>
            ------------------------------------------------------------------
               s_STATE_ON_OFF_Prozedur <= WAIT_FOR_OFF_COMMAND; -- SPECIAL ERROR , aus irgendeinem Grund die gueltigen Zustaende verlassen
         END CASE;
      END IF;
   END PROCESS ON_OFF_Prozedur_p;



   QP_Switch_p : PROCESS (clk, reset, s_QP_SwitchProz_Enable)
      BEGIN
         IF (reset = '1') THEN
            QP_switch_out               <= "0000";   -- Thyristor ist eingeschaltet, da NC, KS, QP2, QP1 Schuetze sind ausgeschaltet,
            s_resetTimer3               <= '0';
            s_startCounter3             <= '0';
            s_WaitCounterMax3           <= (others => '0');
            QP_Ready_for_operate        <= '0';
            s_STATE_QP_Prozedur         <= WAIT_FOR_QP_ENABLE;

         ELSIF (rising_edge(clk)) THEN
            CASE(s_STATE_QP_Prozedur) IS
               ------------------------------------------------------------------
               WHEN WAIT_FOR_QP_ENABLE =>  -- die erste drei Schritte nur weil KS nicht NC ist
               ------------------------------------------------------------------
                  if (QP_switch_enable = '1' )  then                                   -- QP_enable (wird sich noch ergeben) Ausserhalb des Blocks
                     QP_switch_out           <= "0000";                                -- Tyristor ON
                     s_startCounter3         <= '1';
                     s_WaitCounterMax3       <= to_unsigned(gQP_BEFOR_SwitchTime,18);  -- Damit die KS Schuetz von beiden NG nicht gleichzeitig einschalten
                     s_STATE_QP_Prozedur     <= KS_ON;                                 -- diese Zeit fuer beide NG unterschiedlich (z.B 1sec Versatz)
                  end if;

               ------------------------------------------------------------------
               WHEN KS_ON =>  --KS wird eingeschaltet
               ------------------------------------------------------------------
                  IF (s_counter_ready3        = '1' ) THEN     -- Counter ist abgelaufen
                     QP_switch_out           <= "0100";        -- Thyristor ON + KS ON
                     s_startCounter3         <= '1';
                     s_WaitCounterMax3       <= to_unsigned(gQP_SwitchTime,18);
                     s_STATE_QP_Prozedur     <= THYRISTOR_OFF;
                  else
                     s_startCounter3         <= '0';
                  end if;

               ------------------------------------------------------------------
               WHEN THYRISTOR_OFF =>  --Thyristor wird ausgeschaltet
               ------------------------------------------------------------------
                  IF (s_counter_ready3        = '1') THEN      -- Counter abgelaufen
                     QP_switch_out           <= "1100";        -- Thyristor ist ausgeschaltet KS bleibt ON  - das ist die Ruheposition des Sekundärkreises
                     s_STATE_QP_Prozedur     <= WAIT_FOR_QP1_ON;
                  else
                     s_startCounter3         <= '0';
                  end if;

               ------------------------------------------------------------------
               WHEN WAIT_FOR_QP1_ON =>  -- QP1 einschalten, wenn NG bereit ist (Eingeschaltet
               ------------------------------------------------------------------
                  if(s_QP_SwitchProz_Enable = '0' OR QP_Switch_Enable = '0') THEN  -- Fehler auf QP Tripline  oder von Aussen,verbleibe hier
                     Null;                                     -- Verbleibe hier, so lange auf QP Tripline  Fehler anstehet

                  elsif(s_Ready_for_operate   = '1') THEN      -- Netzgeraet ist betriebsbereit(Hauptschutz ist AN)
                     QP_switch_out           <= "1110";        -- TH OFF (KS+QP2) ON
                     s_startCounter3         <= '1';
                     s_WaitCounterMax3       <= to_unsigned(gQP_SwitchTime,18);
                     s_STATE_QP_Prozedur     <= WAIT_FOR_QP2_ON;
                  end if;

               ------------------------------------------------------------------
               WHEN WAIT_FOR_QP2_ON =>  -- KS + QP2 ist eingeschaltet QP1 einschalten
               ------------------------------------------------------------------
                  if(s_QP_SwitchProz_Enable = '0') THEN        -- Fehler auf QP Tripline
                     s_STATE_QP_Prozedur     <= QP_OFF;        -- Ausschaltprozedur einleiten

                  elsif(s_counter_ready3      = '1') THEN
                     QP_switch_out           <= "1111";        -- TH OFF (KS+ QP2 +QP1) ON
                     s_startCounter3         <= '1';
                     s_WaitCounterMax3       <= to_unsigned(gQP_SwitchTime,18);
                     s_STATE_QP_Prozedur     <= KS_OFF;
                  else
                     s_startCounter3         <= '0';
                  end if;

               ------------------------------------------------------------------
               WHEN KS_OFF => -- Kurzschliesser ausschalten
               ------------------------------------------------------------------
                  if(s_QP_SwitchProz_Enable = '0') THEN        -- Fehler auf QP Tripline
                     s_STATE_QP_Prozedur  <= QP_OFF;           -- Ausschaltprozedur einleiten

                  elsif (s_counter_ready3  = '1' ) THEN        -- KS ist jetzt ausgeschaltet
                     QP_switch_out        <= "1011";           -- TH OFF + KS OFF+ QP2 ON + QP1 ON
                     s_startCounter3      <= '1';
                     s_WaitCounterMax3    <= to_unsigned(gQP_SwitchTime,18);
                     s_STATE_QP_Prozedur  <= KS_OFF_WAIT;
                  else
                     s_startCounter3      <= '0';
                  end if;

               ------------------------------------------------------------------
               WHEN KS_OFF_WAIT  =>  -- Warte auf Abschaltung (z.B. beim Fehler)
               ------------------------------------------------------------------
                  if(s_QP_SwitchProz_Enable = '0' ) THEN       -- Fehler auf QP Tripline
                     s_resetTimer3        <= '1';              -- Timer wird sofort resetet, egal wo er vorhin war (z.B. aus einem noch nicht abgelaufndem Zustand)
                     s_STATE_QP_Prozedur  <= QP_OFF;           -- Ausschaltprozedur einleiten

                  elsif (s_counter_ready3  = '1' ) THEN
                     QP_Ready_for_operate <= '1';
                     s_STATE_QP_Prozedur  <= QP_CIRCUIT_READY;
                  else
                     QP_Ready_for_operate <= '0';              -- QP Kreis ist betriebsbereit
                     s_startCounter3      <= '0';
                  end if;

               ------------------------------------------------------------------
               WHEN QP_CIRCUIT_READY =>  -- Warte auf Abschaltung (z.B. beim Fehler)
               ------------------------------------------------------------------
                  if(s_QP_SwitchProz_Enable = '0' OR QP_Switch_Enable = '0') THEN    -- Fehler auf QP Tripline oder Enable extern =0
                     s_resetTimer3        <= '1';              -- Timer wird sofort resetet, egal wo er vorhin war (z.B. aus einem noch nicht abgelaufndem Zustand)
                     s_STATE_QP_Prozedur  <= QP_OFF;           -- Ausschaltprozedur einleiten
                  end if;

               ------------------------------------------------------------------
               WHEN QP_OFF =>
               ------------------------------------------------------------------
                  s_resetTimer3          <= '0';
                  QP_Ready_for_operate   <= '0';               -- QP Kreis nicht mehr betriebsbereit
                  QP_switch_out          <= "0100";            -- TH ON + KS ON + (QP2 + QP1) OFF
                  s_startCounter3        <= '1';
                  s_WaitCounterMax3      <= to_unsigned(gQP_SwitchTime,18);
                  s_STATE_QP_Prozedur    <= WAIT_SWITCH_OFF;

               ------------------------------------------------------------------
               WHEN WAIT_SWITCH_OFF =>
               ------------------------------------------------------------------
                  IF (s_counter_ready3    = '1') THEN          -- Counter abgelaufen
                     QP_switch_out       <= "1100";            -- TH OFF + KS ON + (QP2 + QP1) OFF
                     s_startCounter3     <= '1';
                     s_WaitCounterMax3   <= to_unsigned(gQP_SwitchTime,18);
                     s_STATE_QP_Prozedur <= THYRISTOR_OFF;
                  else
                     s_startCounter3     <= '0';
                  end if;

               ------------------------------------------------------------------
               WHEN OTHERS =>
               ------------------------------------------------------------------
                  s_STATE_QP_Prozedur <= WAIT_FOR_QP_ENABLE; -- SPECIAL ERROR , aus irgendeinem Grund die gueltigen Zustände verlassen
            END CASE;
         END IF;
      END PROCESS QP_Switch_p;

END ARCHITECTURE behav;
