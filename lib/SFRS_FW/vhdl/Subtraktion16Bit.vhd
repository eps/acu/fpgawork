-- Date: 15.02.2023
-- Author: A.Wiest, 

LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY Subtraktion16Bit IS
GENERIC(
		takt   						: integer               		:= 100_000_000 );      -- 100 MHz

PORT(	clk,reset         		: IN  STD_LOGIC:= '0';
	Input1,Input2					: IN  SIGNED(15 DOWNTO 0);  	
   In1minusIn2						: OUT SIGNED(15 DOWNTO 0));
END ENTITY Subtraktion16Bit;


ARCHITECTURE behavioral OF Subtraktion16Bit IS
  SIGNAL s_Input1     : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_Input2     : signed   (15 downto 0):=(others=>'0');

BEGIN

main : PROCESS (clk,reset)
BEGIN

if  reset = '1' then                                     -- Resetting all the required signals
	In1minusIn2 <= (others=>'0');

else
	if rising_edge(clk)   THEN
	   
      if    Input1 >  32766 then s_input1 <= to_signed( 32767,16);
      elsif Input1 < -32766 then s_input1 <= to_signed(-32767,16);
      else                       s_input1 <= Input1;
      end if;

      if    Input2 >  32766 then s_input2 <= to_signed( 32767,16);
      elsif Input2 < -32766 then s_input2 <= to_signed(-32767,16);
      else                       s_input2 <= Input2;
      end if;

      In1minusIn2 <= s_Input1 - s_Input2;

	END IF;  --clk

END IF;  --reset
END PROCESS main;


END ARCHITECTURE behavioral;
