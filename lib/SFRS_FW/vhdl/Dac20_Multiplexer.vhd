--  Filename: Dac20_Multiplexer.vhd
--  Date    : 14.09.2022 born
--  Author  : A. Wiest
--  Version : 1.0

-- Description:
-- This Bloack is used to select the suitable input for the DAC as a input. Its 2 * 1 multiplexer.
-- Mostly used for the Altera Quartus II implimentation purpose.

--============================================================================
 
LIBRARY ieee;                                          --Library Declaration
USE  ieee.std_logic_1164.all; 
USE  ieee.numeric_std.all;
------------------------------------------------------------------------------

entity Dac20_Multiplexer is
   port ( 
		clk, reset  : in std_logic;
		COMMAND     : in std_logic_vector (1 downto 0);
		
		MUX1_IN     : in  std_logic_vector(19 downto 0);   
		MUX2_IN     : in  std_logic_vector(23 downto 0);     
		MUX_OUT     : out std_logic_vector(23 downto 0));	
end Dac20_Multiplexer;


architecture RTL of Dac20_Multiplexer is

BEGIN

process_DAC20_MUX :  process (clk, reset)
BEGIN

   if (reset ='1') then
	   MUX_OUT   <= (others=>'0');

   elsif (rising_edge(clk) and clk = '1') then
	   case COMMAND(1 downto 0) is
		   when "00"   => 	MUX_OUT	<= MUX1_IN & "0000";      -- Input 1 selected         
         when "01"   => 	MUX_OUT	<= MUX2_IN;               -- Input 2 selected       
         when others => 	MUX_OUT	<= MUX1_IN & "0000";      -- Input 1 selected       
	   end case;	
   end if;
   
end process  process_DAC20_MUX ;
	
end RTL;







