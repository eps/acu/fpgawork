--  Filename: TiefHoch_3L.vhd (VHDL File)
--  Datum   : 09.06.2020
--  Authors : A.Wiest
--
--------------------------------------------------------------------------------------------------------------
-- Version Kontrolle :
-- born A.Wiest              09.06.2020
-- Counter als extra Prozess 17.03.2022
--------------------------------------------------------------------------------------------------------------
--  Beschreibung:
--  PWM Muster fuer einen unipoleren Tief- und Hochsetzsteller mit 3L Technologie

---===========================================================================================================
library ieee;                                                  -- Library Declaration
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;

entity TiefHoch_3L is                                         -- Entity Declaration
   generic(
      takt                   : integer   := 100_000_000;
      g_TimeResonanz         : integer   := 2;                -- Achtung =2 ist min!! Verschiebung vom Taktbegin zum Resonanztriger
      g_TimeInputRead        : integer   := 2);               -- Achtung =2 ist min!! Verschiebung vom Taktbegin zum Daten einlesen

   port(
      clk, reset, Enable     : in  std_logic ;                -- Mit Enable wirden die Transistoren sofort abgeschaltet
      n_CounterEnable        : in  std_logic ;                -- =0 Saegezahn - Counter laeuft
                                                              -- Zeitsollwerte (Vorgabe des Spannungsmittelwertes)
      Usteuer                : in  unsigned(15 downto 0);     -- =1 Tiefsetzsteller voll ausgesteuert; =0 Hochsetzsteller voll ausgesteuert
      Takt_time              : in  unsigned(15 downto 0);     -- Zeit fuer eine Periode x 10ns immer +2rechnen 6002 fuer 60us
      Ust_immer_lesen        : in  std_logic;                 -- = '1' , Eingang wird staendig gelesen und in dem PWM-Takt sofort korrigiert
                                                              -- = '0' nur wenn der Takt beginnt wird Usteuer eingelesen
      Tot_zeit               : in  unsigned(7  downto 0);     -- Zeit zw. ein Trans. wird abgeschaltet der andered Zugeschaltet
      time_korr              : in  unsigned(7  downto 0);     -- Korrektur der Zeit fuer den pos. und neg. Aufteilung (Symetrierung der Zw.Kr. Sp) 
      Korrektur_Vorzeichen   : in  std_logic;                 -- = Vorzeichen fuer die Korrektur
      HalbtaktRead           : in  std_logic;                 -- =1 der Eingang wird auch bei dem Halben Takt gelesen =0 nur beim ganzen Takt
     
      S1,S2,S3,S4            : out std_logic ;                -- Signale fuer Transistoren
      TH_Data_Out_Valid      : out std_logic ;                -- PWM Output =1 ein Takt ist durchgelaufen
      Resonanz_Trigger       : out std_logic ;                -- Triger fuer Resonanzkonverter
      DataRead_Trigger       : out std_logic);                -- Eingangsdaten werden eingelesen
end TiefHoch_3L;

--==============================================================================================================================
architecture behavioral of TiefHoch_3L is -- Architecture Declaration
   
   signal s_Takt_time        : unsigned(15 downto 0) := (others => '0');       -- gespeicherte Zeit fuer Ttakt
   signal s_Takt_time4       : unsigned(15 downto 0) := (others => '0');       -- gespeicherte Zeit fuer Ttakt/4
   signal s_Tot_zeit         : unsigned(7  downto 0) := (others => '0');       -- gespeicherte Zeit fuer Totzeit
   
   signal s_tein_eing_sp     : unsigned(15 downto 0) := (others => '0');       -- gespeicherte Zeit fuer Usteuer
   signal s_tein1            : unsigned(15 downto 0) := (others => '0');       -- gespeicherte Zeit fuer Usteuer
   signal s_tein2            : unsigned(15 downto 0) := (others => '0');
   
   signal s_tein1up          : unsigned(14 downto 0) := (others => '0');
   signal s_tein1down        : unsigned(14 downto 0) := (others => '0');
   
   signal s_tein2up          : unsigned(14 downto 0) := (others => '0');
   signal s_tein2down        : unsigned(14 downto 0) := (others => '0');
   
   signal s_time_korr_sp     : unsigned(7  downto 0) := (others => '0');       -- gespeicherte Zeit fuer Usteuer

   signal s_cnt1             : unsigned(15 downto 0) := (others => '0');       -- 1. Counter
   signal s_cnt2             : unsigned(15 downto 0) := (others => '0');       -- 2. Counter

   signal s_steigung_counter : std_logic             :='0';
   signal s_DataReady        : std_logic             :='0';
   signal s_ersterDurchlauf  : unsigned(1 downto 0)  := (others => '0');
   
   type STATE_ZEIT is ( EINGANG_SPEICHERN,
                        TEIN_BESTIMMEN,
                        TEIN_TEILEN);

   signal s_Zeitbestimmung_state : STATE_ZEIT := EINGANG_SPEICHERN;

begin   --=========================================================================================================================
   
  p_Zeitbestimmung : process(clk, reset, Enable) 
   begin
      if (reset = '1' OR Enable = '0') then           -- Resetting all the required signals
         s_tein1                <= (others => '0');   -- gespeicherte Zeit fuer Usteuer und taus
         s_tein2                <= (others => '0');
         s_tein_eing_sp         <= (others => '0');
         s_time_korr_sp         <= (others => '0'); 
         s_Takt_time            <= (others => '0'); 
         s_Tot_zeit             <= (others => '0'); 
         
         s_tein1up              <= (others => '0');  
         s_tein1down            <= (others => '0');    
         s_tein2up              <= (others => '0');  
         s_tein2down            <= (others => '0');     
               
         DataRead_Trigger       <= '0';
         s_DataReady            <= '0';
      
      else
         if rising_edge(clk) then

            case s_Zeitbestimmung_state is
            ---------------------------------------------------------------------------------
            when EINGANG_SPEICHERN => --1. Takt
            ---------------------------------------------------------------------------------
               if (Enable = '1') then
                  if (s_cnt1 = g_TimeInputRead AND s_steigung_counter ='0') OR 
                     (s_cnt2 = g_TimeInputRead AND s_steigung_counter ='1' AND HalbtaktRead ='1')    then 
       
                     s_Takt_time             <= Takt_time;
                     s_Takt_time4            <= "00" & Takt_time(15 downto 2);    
                     s_tein_eing_sp          <= Usteuer  ;
                     s_time_korr_sp          <= time_korr;
                     s_Tot_zeit              <= Tot_zeit ;
                     DataRead_Trigger        <= '1'      ;
                     s_Zeitbestimmung_state  <= TEIN_BESTIMMEN;
                  end if;
               end if;
                  
            ---------------------------------------------------------------------------------
            when TEIN_BESTIMMEN =>    --2.  Takt
            ---------------------------------------------------------------------------------
               if (Ust_immer_lesen = '1') then
                  if (Korrektur_Vorzeichen = '0')                       then  -- positive Korrektur
                     if    Usteuer  > s_Takt_time                       then s_tein1 <= s_Takt_time - time_korr         ;
                     elsif Usteuer  > time_korr                         then s_tein1 <= Usteuer     - time_korr         ;
                     else                                                    s_tein1 <= (others => '0')                 ; end if;
                        
                     if Usteuer  + time_korr > s_Takt_time              then s_tein2 <= s_Takt_time                     ;
                     else                                                    s_tein2 <= Usteuer     + time_korr         ; end if;
    
                  else                                                        -- negative Korrektur
                     if    Usteuer  > s_Takt_time                       then s_tein2 <= s_Takt_time - time_korr         ;
                     elsif Usteuer  > time_korr                         then s_tein2 <= Usteuer     - time_korr         ;
                     else                                                    s_tein2 <= (others => '0')                 ; end if;
                     
                     if Usteuer + time_korr > s_Takt_time               then s_tein1 <= s_Takt_time                     ;
                     else                                                    s_tein1 <= Usteuer     + time_korr         ; end if;
                  end if;
                  
               else     -- mit dem Dataread zwei mal pro Takt einlesen
                  if (Korrektur_Vorzeichen = '0')                       then -- positive Korrektur
                     if    s_tein_eing_sp  > s_Takt_time                then s_tein1 <= s_Takt_time    - s_time_korr_sp ;
                     elsif s_tein_eing_sp  > s_time_korr_sp             then s_tein1 <= s_tein_eing_sp - s_time_korr_sp ;
                     else                                                    s_tein1 <= (others => '0')                 ; end if;
                        
                     if s_tein_eing_sp + s_time_korr_sp > s_Takt_time   then s_tein2 <= s_Takt_time;
                     else                                                    s_tein2 <= s_tein_eing_sp + s_time_korr_sp ; end if;
    
                  else                                                       -- negative Korrektur
                     if    s_tein_eing_sp  > s_Takt_time                then s_tein2 <= s_Takt_time    - s_time_korr_sp ;
                     elsif s_tein_eing_sp  > s_time_korr_sp             then s_tein2 <= s_tein_eing_sp - s_time_korr_sp ;
                     else                                                    s_tein2 <= (others => '0')                 ; end if;
                     
                     if s_tein_eing_sp + s_time_korr_sp > s_Takt_time   then s_tein1 <= s_Takt_time;
                     else                                                    s_tein1 <= s_tein_eing_sp + s_time_korr_sp ; end if;
                  end if;
               end if;
               
               DataRead_Trigger       <= '0';
               s_Zeitbestimmung_state <= TEIN_TEILEN;
               
            ---------------------------------------------------------------------------------
            when TEIN_TEILEN =>    --2.  Takt
            ---------------------------------------------------------------------------------
                                          s_tein1up   <= s_tein1(15 downto 1)   ;
            
               if   s_tein1(0) = '0' then s_tein1down <= s_tein1(15 downto 1)   ;
               else                       s_tein1down <= s_tein1(15 downto 1) +1; end if;
            
            
                                          s_tein2up   <= s_tein2(15 downto 1)   ;
            
               if   s_tein2(0) = '0' then s_tein2down <= s_tein2(15 downto 1)   ;
               else                       s_tein2down <= s_tein2(15 downto 1) +1; end if;
               
               s_DataReady            <= '1';
               s_Zeitbestimmung_state <= EINGANG_SPEICHERN;
            
            ---------------------------------------------------------------------------------
            when others =>
            ---------------------------------------------------------------------------------
               s_Zeitbestimmung_state <= EINGANG_SPEICHERN;

            end case;   
         end if;                 --clk
      end if;                    --reset
  end process p_Zeitbestimmung;


  p_Counter : process(clk, reset, n_CounterEnable, Enable, Takt_time) 
   begin
      if (reset = '1' OR Enable = '0' OR Takt_time < 5000) then                               --Takt darf nicht groesser als 20kHz sein
         s_cnt1                 <= (others => '0');                                           -- alle Zaehler  = 0
         s_cnt2                 <= (others => '0');  
         s_steigung_counter     <= '0'            ;
         TH_Data_Out_Valid      <= '0'            ;
         s_ersterDurchlauf      <= (others => '0');      
         
      else
         if rising_edge(clk) AND n_CounterEnable = '0' then
            if s_cnt1 = 0 AND s_steigung_counter = '0' then  
               s_cnt1              <= s_cnt1 + 1;                                            -- Zaehler fuer halben Takt
               s_cnt2              <= '0' & Takt_time(15 downto 1) ;
               
            elsif s_cnt1 < '0' & Takt_time(15 downto 1)  AND s_steigung_counter = '0' then   -- ein Takt weniger wegen Speichern
               s_cnt1              <= s_cnt1 + 1;                                            -- Zaehler fuer halben Takt
               
               if s_cnt2 - 1 < 65000 then                                                    -- Schutz vor dem Ueberlauf
                  s_cnt2           <= s_cnt2 - 1;
               end if;
               
               TH_Data_Out_Valid   <= '0';
            
            elsif s_cnt1 = '0' & Takt_time(15 downto 1)  AND s_steigung_counter = '0' then
               
               if s_cnt1 - 1 < 65000 then                                                    -- Schutz vor dem Ueberlauf
                  s_cnt1           <= s_cnt1 - 1;
               end if;

               s_cnt2              <= s_cnt2 + 1;
               s_steigung_counter  <= '1';                                                   -- jetzt laeuft der Zaeler nach unten           
               TH_Data_Out_Valid   <= '1'; 
               
            elsif s_cnt1 > 1            AND s_steigung_counter = '1' then
               if s_cnt1 - 1 < 65000 then                                                    -- Schutz vor dem Ueberlauf
                  s_cnt1           <= s_cnt1 - 1;
               end if;
               
               s_cnt2              <= s_cnt2 + 1;
               TH_Data_Out_Valid   <= '0';
            
            else
               if s_ersterDurchlauf < 2 then
               		s_ersterDurchlauf   <= s_ersterDurchlauf +1;
               end if;
               
               s_steigung_counter  <= '0';
               s_cnt1              <= s_cnt1 + 1;
               s_cnt2              <= '0' & Takt_time(15 downto 1) - 1; 

            end if;
         end if; 
      end if;                                                                             -- clk
  end process p_Counter;


  p_DataValid : process(clk, reset)          
   begin
      if (reset = '1') then                                                               -- Resetting all the required signals
         Resonanz_Trigger      <= '0'; 
      else
         if rising_edge(clk) then
            if    s_cnt1 = g_TimeResonanz                 AND s_steigung_counter ='0' then Resonanz_Trigger <= '1';
            elsif s_cnt1 = g_TimeResonanz +  s_Takt_time4 AND s_steigung_counter ='0' then Resonanz_Trigger <= '1';
            
            elsif s_cnt2 = g_TimeResonanz                 AND s_steigung_counter ='1' then Resonanz_Trigger <= '1'; 
            elsif s_cnt2 = g_TimeResonanz +  s_Takt_time4 AND s_steigung_counter ='1' then Resonanz_Trigger <= '1';
            else                                                                           Resonanz_Trigger <= '0';
            end if;
         end if;
      end if; 
  end process p_DataValid;


Compare_prozess: process(clk, reset, Enable, s_DataReady,s_ersterDurchlauf) 
begin
   if (reset = '1' OR Enable ='0' OR s_DataReady = '0' OR s_ersterDurchlauf < 2) then   
      S1 <= '0' ; 
      S2 <= '0' ; 
      S3 <= '0' ;  
      S4 <= '0' ;
   
   else
      if rising_edge(clk) then
         if ((s_tein1up  +1 <  s_cnt1 + s_Tot_zeit) AND  s_steigung_counter = '1') OR
            ((s_tein1down   <  s_cnt1 + s_Tot_zeit) AND  s_steigung_counter = '0')    then S1 <= '0'; else  S1 <= '1'; end if; 
           
         if s_tein1up =0 AND s_tein1down =0                                           then S2 <= '1'; 
         elsif ((s_tein1up  +1 >= s_cnt1)           AND  s_steigung_counter = '1') OR
            ((s_tein1down   >= s_cnt1)              AND  s_steigung_counter = '0')    then S2 <= '0'; 
         else                                                                              S2 <= '1'; 
         end if; 
         
         if s_tein2up =0 AND s_tein2down =0                                           then S3 <= '1'; 
         elsif ((s_tein2up  +1 >= s_cnt2)           AND  s_steigung_counter = '1') OR
            ((s_tein2down   >= s_cnt2)              AND  s_steigung_counter = '0')    then S3 <= '0'; 
         else                                                                              S3 <= '1'; 
         end if; 
            

         if ((s_tein2up  +1 < s_cnt2 + s_Tot_zeit)  AND  s_steigung_counter = '1') OR
            ((s_tein2down   < s_cnt2 + s_Tot_zeit)  AND  s_steigung_counter = '0')    then S4 <= '0'; else  S4 <= '1'; end if; 
      end if; 
   end if;
end process Compare_prozess;


end architecture behavioral;