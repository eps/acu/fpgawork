--  Filename: Sig4_1_Mux.vhd
--  Date    : 19.05.2021 born
--  Author  : Wiest 
--  Version : 1.0

--  Description:

--This Bloack is used to select the suitable input. Its 4 * 1 multiplexer.
-- Mostly used for the Altera Quartus II implimentation purpose.

--============================================================================
LIBRARY ieee;                                          --Library Declaration
USE  ieee.std_logic_1164.all; 
USE  ieee.numeric_std.all;

------------------------------------------------------------------------------
entity Sig4_1_Mux is

   generic (
		clk_in_hz   : integer  := 100_000_000);        -- 100Mhz system clock
	
   port ( 
		clk, reset  : in std_logic;
		COMMAND     : in std_logic_vector (1 downto 0);
		
		MUX_IN_0    : in  std_logic_vector(15 downto 0);   
		MUX_IN_1    : in  std_logic_vector(15 downto 0);   
		MUX_IN_2    : in  std_logic_vector(15 downto 0);   
		MUX_IN_3    : in  std_logic_vector(15 downto 0);   
	
		MUX_OUT    : out std_logic_vector(15 downto 0));
	
end Sig4_1_Mux;


architecture RTL of Sig4_1_Mux is

BEGIN

process_Sig4_1_Mux :  process (clk, reset)
BEGIN

if (reset ='1') then	
   MUX_OUT  <= (others=>'0');

elsif (rising_edge(clk) and clk = '1') then
	case COMMAND(1 downto 0) is
		when "00" => MUX_OUT	<= MUX_IN_0;      --Input 1 selected      
      when "01" => MUX_OUT	<= MUX_IN_1;       
      when "10" => MUX_OUT	<= MUX_IN_2;      
      when "11" => MUX_OUT	<= MUX_IN_3;      
	end case;
end if;
end process  process_Sig4_1_Mux ;
	
end RTL;







