--		Aufgabe dieses Blocks ist es, die Signalkette zu dem IGBT zu testen,
--		sodass die Zuordnung des IGBTs und Signals manuell geprueft werden.

LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY IGBT_Test IS

   GENERIC(           
      --Initialisierung 100 MHz
      takt        				: integer  := 100_000_000  ); 
    
   PORT(
      --System-Eingänge:
      clk, reset, enable	: in std_logic ;
        
      --Eingänge:
      SchalterPosition     : in std_logic_vector (4 downto 0); -- "00000" 0  alle Schalter ausgeschaltet
																					-- "01101" 13 alle Schalter eingeschaltet
                                                               -- "01111" 15 Ausgaengen mit Eingaengen verbunden 
																					
                                                               -- "00001" 1 Schalter ist eingeschaltet
  																					-- "00010" 2 Schalter ist eingeschaltet
																					-- "00011" 3 Schalter ist eingeschaltet ...		
      IGBT1_in       	   : in std_logic;
 		IGBT2_in       	   : in std_logic;
		IGBT3_in				   : in std_logic;	
		IGBT4_in	            : in std_logic;

		IGBT5_in       	   : in std_logic;
 		IGBT6_in       	   : in std_logic;
		IGBT7_in				   : in std_logic;	
		IGBT8_in	            : in std_logic;	

		IGBT9_in       	   : in std_logic;
 		IGBT10_in       	   : in std_logic;
		IGBT11_in				: in std_logic;	
		IGBT12_in	         : in std_logic;

      IGBT1_out          	: out std_logic := '0'; 
      IGBT2_out          	: out std_logic := '0'; 
      IGBT3_out          	: out std_logic := '0'; 
      IGBT4_out          	: out std_logic := '0'; 
        
      IGBT5_out          	: out std_logic := '0'; 
      IGBT6_out          	: out std_logic := '0'; 
		IGBT7_out          	: out std_logic := '0';  	
      IGBT8_out          	: out std_logic := '0'; 

		IGBT9_out          	: out std_logic := '0'; 
		IGBT10_out          	: out std_logic := '0'; 	
      IGBT11_out          	: out std_logic := '0'; 
		IGBT12_out          	: out std_logic := '0' );

END IGBT_Test ;

--============================================================================
ARCHITECTURE behavioral OF IGBT_Test IS

signal IGBT_Schalter_vector : std_logic_vector (11 downto 0) := (others => '0');
begin

IGBT_Test_p: process(reset, clk, enable, SchalterPosition) 
  BEGIN
  
   if reset = '1' OR enable = '0' then
		IGBT_Schalter_vector <= (others => '0');  
   
   elsif rising_edge(clk)  then
	   case SchalterPosition is
         when "00000" =>  IGBT_Schalter_vector <= "000000000000";  --0  Position Alles aus
         when "00001" =>  IGBT_Schalter_vector <= "000000000001";  --1  HB_V17 prim
		   when "00010" =>  IGBT_Schalter_vector <= "000000000010";  --2  HB_V18 prim
		   when "00011" =>  IGBT_Schalter_vector <= "000000000100";  --3  HB_V19 prim
         when "00100" =>  IGBT_Schalter_vector <= "000000001000";  --4  HB_V20 prim
         when "00101" =>  IGBT_Schalter_vector <= "000000010000";  --5  HB_V21 sek
         when "00110" =>  IGBT_Schalter_vector <= "000000100000";  --6  HB_V22 sek
         when "00111" =>  IGBT_Schalter_vector <= "000001000000";  --7  HB_V23 sek
         when "01000" =>  IGBT_Schalter_vector <= "000010000000";  --8  HB_V24 sek
         when "01001" =>  IGBT_Schalter_vector <= "000100000000";  --9  Umschalter 1_3
         when "01010" =>  IGBT_Schalter_vector <= "001000000000";  --10 Umschalter 2_4
         when "01011" =>  IGBT_Schalter_vector <= "010000000000";  --11
         when "01100" =>  IGBT_Schalter_vector <= "100000000000";  --12
         when "01101" =>  IGBT_Schalter_vector <= "111111111111";  --13 Alles eingeschaltet
                                                                   --14 nicht belegt
         when others =>  IGBT_Schalter_vector <= "000000000000";	
      end case;
	end if;
END process IGBT_Test_p;


IGBT_out_p: process(reset, clk, enable, IGBT_Schalter_vector, SchalterPosition) 
  BEGIN
      if reset = '1' OR enable = '0' then
		   IGBT1_out <= '0'; IGBT5_out <= '0'; IGBT9_out  <= '0'; 
         IGBT2_out <= '0'; IGBT6_out <= '0'; IGBT10_out <= '0'; 
         IGBT3_out <= '0'; IGBT7_out <= '0'; IGBT11_out <= '0'; 
         IGBT4_out <= '0'; IGBT8_out <= '0'; IGBT12_out <= '0';	
     
      elsif rising_edge(clk) then
         if   SchalterPosition = "01111" then  --Schalterposition 15 (Default) -> Eingang wird an den Ausgang weitergereicht
            IGBT1_out  <= IGBT1_in;  IGBT5_out  <= IGBT5_in;  IGBT9_out  <= IGBT9_in ;
            IGBT2_out  <= IGBT2_in;  IGBT6_out  <= IGBT6_in;  IGBT10_out <= IGBT10_in;
            IGBT3_out  <= IGBT3_in;  IGBT7_out  <= IGBT7_in;  IGBT11_out <= IGBT11_in;
            IGBT4_out  <= IGBT4_in;  IGBT8_out  <= IGBT8_in;  IGBT12_out <= IGBT12_in;
         
         elsif SchalterPosition = "10000" then --Schalterposition 16
            IGBT1_out  <= IGBT1_in;  -- Primaerseite wird weitergereicht  
            IGBT2_out  <= IGBT2_in;   
            IGBT3_out  <= IGBT3_in;    
            IGBT4_out  <= IGBT4_in;    
            
            IGBT5_out  <= '0';       -- Sekundaerseite ist abgeschaltet
            IGBT6_out  <= '0'; 
            IGBT7_out  <= '0';
            IGBT8_out  <= '0';
         
            IGBT9_out  <= '0';      -- Schalter sind abgeschaltet 
            IGBT10_out <= '0';
            IGBT11_out <= '0';
            IGBT12_out <= '0'; 
       
         elsif SchalterPosition = "10001" then --Schalterposition 17
            IGBT1_out  <= IGBT1_in;  -- Primaerseite wird weitergereicht  
            IGBT2_out  <= IGBT2_in;   
            IGBT3_out  <= IGBT3_in;    
            IGBT4_out  <= IGBT4_in;    
            
            IGBT5_out  <= '0';       -- Sekundaerseite ist abgeschaltet
            IGBT6_out  <= '0'; 
            IGBT7_out  <= '0';
            IGBT8_out  <= '0';
         
            IGBT9_out  <= '1';      -- Umschalter 1_3 sind eingeschaltet
            IGBT10_out <= '0';      -- Umschalter 2_4 sind  ausgeschaltet
            IGBT11_out <= '0';
            IGBT12_out <= '0'; 
       
         elsif SchalterPosition = "10010" then --Schalterposition 18
            IGBT1_out  <= IGBT1_in;  -- Primaerseite wird weitergereicht  
            IGBT2_out  <= IGBT2_in;   
            IGBT3_out  <= IGBT3_in;    
            IGBT4_out  <= IGBT4_in;    
            
            IGBT5_out  <= '0';       -- Sekundaerseite ist abgeschaltet
            IGBT6_out  <= '0'; 
            IGBT7_out  <= '0';
            IGBT8_out  <= '0';
         
            IGBT9_out  <= '0';      -- Umschalter 1_3 sind ausgeschaltet
            IGBT10_out <= '1';      -- Umschalter 2_4 sind eingeschaltet
            IGBT11_out <= '0';
            IGBT12_out <= '0'; 
       
         else
            IGBT1_out  <= IGBT_Schalter_vector(0);
            IGBT2_out  <= IGBT_Schalter_vector(1);
            IGBT3_out  <= IGBT_Schalter_vector(2);
            IGBT4_out  <= IGBT_Schalter_vector(3);
        
            IGBT5_out  <= IGBT_Schalter_vector(4);
            IGBT6_out  <= IGBT_Schalter_vector(5);
            IGBT7_out  <= IGBT_Schalter_vector(6); 
            IGBT8_out  <= IGBT_Schalter_vector(7);

            IGBT9_out  <= IGBT_Schalter_vector(8);
            IGBT10_out <= IGBT_Schalter_vector(9);
            IGBT11_out <= IGBT_Schalter_vector(10);
            IGBT12_out <= IGBT_Schalter_vector(11);
         end if;
      end if;
END process IGBT_out_p;

END architecture behavioral;
