------------------------------------------------------------------------------------------------------------------------
-- File name: Tb_MAX31865_Driver.vhd                                                                                  --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 24/04/2019                                                                                              --
--                                                                                                                    --
-- Comments : MAX31865_Driver test bench.                                                                             --
--                                                                                                                    --
-- History  : Start up version 24/04/2019                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ACU_package.all;

Entity Tb_MAX31865_Driver is
end Entity Tb_MAX31865_Driver;

architecture beh of Tb_MAX31865_Driver is

--
-- Constants declaration
constant TC_cntData              : unsigned(3 downto 0):=x"D";

--
-- Signals declaration
signal Clock                     : std_logic:= '0';
signal Reset                     : std_logic:= '1';
signal MaxThrs                   : std_logic_vector(15 downto 0);                         
signal MinThrs                   : std_logic_vector(15 downto 0);                         
signal SCLK                      : std_logic;
signal SCS_n                     : std_logic;
signal SDt_in                    : std_logic;
signal SDt_out                   : std_logic;

signal DtFromS                   : array16bits(12 downto 0);                                 -- Data Array from slave to master
signal DtFromM                   : array16bits(12 downto 0);                                 -- Data Array from master to slave

signal NewDt_FromMaster          : std_logic;                                                -- One clock cycle pulse active high when there is a new 
signal Dt_FromMaster             : std_logic_vector(15 downto 0);                            -- Data received.

signal cntData                   : unsigned(3 downto 0);                                     -- array index
signal Dt_2Send                  : std_logic_vector(15 downto 0);                            -- Data to send.
--
-- Components declaration
Component MAX31865_Driver is
  Generic (
         gSCLK_High              : unsigned(7 downto 0):= to_unsigned(20,8);                 -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0):= to_unsigned(20,8);                 -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0):= to_unsigned(80,8);                 -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
         gCntOutWidth            : integer := 24;                                            -- Time base counter width
         gEnTrigValue            : integer := 2200000                                        -- Time base terminal counter(22ms=2200000*10ns)

  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high

         MaxThrs                 : in std_logic_vector(15 downto 0);                         -- Temperature max threshold
         MinThrs                 : in std_logic_vector(15 downto 0);                         -- Temperature min threshold

         NewDtOut                : out std_logic;                                            -- It is one clock pulse high when all the outputs valid are
         ReadMaxThrs             : out std_logic_vector(15 downto 0);                        -- Temperature max threshold in use
         ReadMinThrs             : out std_logic_vector(15 downto 0);                        -- Temperature min threshold in use
         ReadCfgReg              : out std_logic_vector(7 downto 0);                         -- Configuration register status
         ReadFaultReg            : out std_logic_vector(7 downto 0);                         -- Fault register status
         ReadTemperature         : out std_logic_vector(15 downto 0);                        -- Temperature measured value
         TemperatureInterlock    : out std_logic;                                            -- It is high when the measured temperature is higher or lower the
                                                                                             -- max/min threshold or when there is an over/uder voltage on the 
                                                                                             -- suppy voltage.
	 
         SCLK                    : out std_logic;                                            -- SPI clock signal
         SCS_n                   : out std_logic;                                            -- Active low SPI Chip Select signal
         SDt_in                  : in  std_logic;                                            -- Serial SPI incoming data 
         SDt_out                 : out std_logic                                             -- Serial SPI outgoing data 
  );
end component MAX31865_Driver;


Component GenericSPI_SlaveWrap is
  Generic (
	 gDataWidth              : integer range 0 to 248                                    -- Data to receive width.
  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high

         SCLK                    : in  std_logic;                                            -- SPI clock signal
         SCS_n                   : in  std_logic;                                            -- Active low SPI Chip Select signal
         SDt_in                  : in  std_logic;                                            -- Serial SPI incoming data 
         SDt_out                 : out std_logic;                                            -- Serial SPI outgoing data 

         Dt_2Send                : in  std_logic_vector(gDataWidth-1 downto 0);              -- Data to send.
                                                                                             --  is different from the locally calculated one.

         NewDt_FromMaster        : out std_logic;                                            -- One clock cycle pulse active high when there is a new 
	                                                                                     -- Dt_FromMaster 
         Dt_FromMaster           : out std_logic_vector(gDataWidth-1 downto 0)               -- Data received.
	 
  );
end component GenericSPI_SlaveWrap;


begin

Clock       <= not Clock after 5 ns;                                                              -- 100 MHz system clock 
MaxThrs     <= x"ABCD";
MinThrs     <= x"0123";

DtFromS(0)  <= x"3210";
DtFromS(1)  <= x"7654";
DtFromS(2)  <= x"BA98";
DtFromS(3)  <= x"FEDC";
DtFromS(4)  <= x"1111";
DtFromS(5)  <= x"2222";
DtFromS(6)  <= x"3333";
DtFromS(7)  <= x"4444";
DtFromS(8)  <= x"5555";
DtFromS(9)  <= x"6666";
DtFromS(10) <= x"7777";
DtFromS(11) <= x"8888";
DtFromS(12) <= x"9999";

-- data counter process
p_dtCnt:process(Clock,Reset)
begin
  if (Reset='1') then
    cntData  <= (others=>'0');
    DtFromM  <= (others=>(others=>'0'));
  elsif (Clock'event and Clock='1') then
    if (NewDt_FromMaster='1') then
      DtFromM(to_integer(cntData)) <= Dt_FromMaster;
      if (cntData < Tc_cntData-1) then
        cntData  <= cntData + 1;
      else
        cntData  <= (others=>'0');
      end if;
    end if;
  end if;
end process p_dtCnt;

--
-- master driver instance
i_MAX31865_Driver: MAX31865_Driver 
  Generic map(
         gEnTrigValue            => 11800                                          -- 2200000 is the real value to use, but for debugging purposes
	                                                                           -- 11800 is more than enough.
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,

         MaxThrs                 => MaxThrs,
         MinThrs                 => MinThrs,

         NewDtOut                => open,
         ReadMaxThrs             => open,
         ReadMinThrs             => open,
         ReadCfgReg              => open,
         ReadFaultReg            => open,
         ReadTemperature         => open,
         TemperatureInterlock    => open,
	 
         SCLK                    => SCLK,   
         SCS_n                   => SCS_n,  
         SDt_in                  => SDt_in, 
         SDt_out                 => SDt_out
  );
  
--
-- Slave instance
Dt_2Send  <= DtFromS(to_integer(cntData));

i_GenericSPI_SlaveWrap: GenericSPI_SlaveWrap 
  Generic map(
	 gDataWidth              => 16
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,

         SCLK                    => SCLK,  
         SCS_n                   => SCS_n, 
         SDt_in                  => SDt_out,
         SDt_out                 => SDt_in,

         Dt_2Send                => Dt_2Send,

         NewDt_FromMaster        => NewDt_FromMaster,
         Dt_FromMaster           => Dt_FromMaster
	 
  );

end beh;
