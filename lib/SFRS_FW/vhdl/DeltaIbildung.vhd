-- Filename: DeltaIbildung.vhd (VHDL File)
-- Datum   : 15.09.2022  
-- Authors : A.wiest
-- bildet Delta I aus dem Sollwert mit einer vorgegebener Zeit zwischen den Messpunkten
-- Weiterhin wir auch ULmagnet=L(I) * di/dt berechnet
-- Ergebnis x Multiplikator x2 .. x16 
---========================================================================================================================================================

LIBRARY ieee;                                                             -- Library Declaration
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

                                                                          -- Entity Declaration
ENTITY DeltaIbildung IS   

 GENERIC( 
      takt              : integer  := 100_000_000; --Initalphalisierung 50 MHz
      Delta_max         : integer  := 1000 );      -- Maximal erlaubter Delta-Wert im Strom

   PORT(
      --System Signals
      clk, reset, Enable    : in std_logic             := '0';

      --Inputs
      Isoll                 : in signed   (19 downto 0):=(others=>'0'); -- Set value of the current
      Llast                 : in signed   (15 downto 0):=(others=>'0'); -- Magnetinduktivitat (Integerwert 1..0)
      Faktor                : in signed   (15 downto 0):=(others=>'0'); -- Wert fuer die Multiplikation mit Delta I (0..1)
       
      -- Verschiebung der Multiplikation 0= Keine Verschiebung, 1= x2, 2=x4, 3=x8
      Verschiebung          : in unsigned (2  downto 0)  :=(others=>'0'); 
      Trigger               : in std_logic               := '0';
     
      -- Outputs
      DeltaI_Fak            : out signed(15 downto 0)    :=(others=>'0'); -- DeltaI x Multiplikator x 128
      DeltaI                : out signed(15 downto 0)    :=(others=>'0'); -- DeltaI 
      U_Llast               : out signed(15 downto 0)    :=(others=>'0'); -- Induktive Spannung UL=L(I) *di/dt 
      Data_Out_Valid        : out std_logic              := '0'        ); -- Output Valid Signal 

END DeltaIbildung ;

--==============================================================================================================================

ARCHITECTURE behavioral OF DeltaIbildung IS                             -- Architecture Declaration  

SIGNAL s_Isoll_aktuell     : signed  (19 downto 0):=(others=>'0');      -- Vorheriger Wert des Eingangsstromes
SIGNAL s_Isoll_vorher      : signed  (19 downto 0):=(others=>'0');      -- Vorheriger Wert des Eingangsstromes
SIGNAL s_DeltaI            : signed  (19 downto 0):=(others=>'0');      -- Vorheriger Wert des Eingangsstromes

SIGNAL s_DeltaI_Fak        : signed  (15 downto 0):=(others=>'0');      -- deltaI x Faktor

SIGNAL s_mult_a            : signed  (15 downto 0):=(others=>'0'); 
SIGNAL s_mult_b            : signed  (15 downto 0):=(others=>'0');
SIGNAL s_mult32            : signed  (31 downto 0):=(others=>'0'); 

TYPE STATE_DELTA_I IS (EINGANG_SPEICHERN, 
                       DELTA_KUERZEN,
                       MULTIPLIKATION1_VORBEREITEN,
                       MULTIPLIKATION1,
                       MULT_VERSCHIEBEN,
                       MULTIPLIKATION2_VORBEREITEN,
                       MULTIPLIKATION2,
                       DATA_VALID );
SIGNAL s_DeltaI_STATE : STATE_DELTA_I := EINGANG_SPEICHERN;


BEGIN --=========================================================================================================================
p_DeltaIbildung : Process (clk, reset, Enable, Trigger)  
   ALIAS		a_DeltaI :	SIGNED (15 downto 0) IS s_DeltaI (15 downto 0);  

 BEGIN 
   if  reset = '1' then                           -- Resetting all the required signals 
      s_Isoll_aktuell      <=(others=>'0');       -- Actual Value of the set current     
      s_Isoll_vorher       <=(others=>'0');       -- Vorheriger Wert des Eingangsstromes   
      s_mult32             <=(others=>'0');       -- Multiplikationsergebnis
      s_DeltaI             <=(others=>'0'); 
      s_DeltaI_Fak         <=(others=>'0');
      s_DeltaI_STATE       <= EINGANG_SPEICHERN; 
        
   else
      if rising_edge(clk)   THEN
        CASE s_DeltaI_STATE  IS

   ---------------------------------------------------------------------------------
   when EINGANG_SPEICHERN =>   --1.Takt
   ---------------------------------------------------------------------------------   
      If (Enable= '1' AND Trigger ='1' ) then                                          
         s_Isoll_aktuell  <= Isoll         ;
         s_DeltaI_STATE   <= DELTA_KUERZEN ; 
         
         if     Isoll - s_Isoll_vorher >  Delta_max then s_DeltaI <= to_signed( Delta_max,20) ; 
         elsif  Isoll - s_Isoll_vorher < -Delta_max then s_DeltaI <= to_signed(-Delta_max,20) ;
         else                                            s_DeltaI <= Isoll - s_Isoll_vorher   ; -- Bildung des Delta I
         end if;
      
      elsif (Enable = '0') then 
         s_Isoll_vorher <=(others=>'0'); -- Muss zur 0 gesetzt werden, damit nach dem Enable=1 kein Sprung kommt und warte weiter auf Triggersignal
      
      else
         Null;
      end If   ;

      Data_Out_Valid      <= '0';
      
      
   ---------------------------------------------------------------------------------
   when DELTA_KUERZEN =>   --2.Takt
   --------------------------------------------------------------------------------- 
      if     s_DeltaI >  32766 then s_DeltaI <= to_signed( 32766,20); --kann nicht groesser sein als 16Bit
      elsif  s_DeltaI < -32766 then s_DeltaI <= to_signed(-32766,20);
      else  NULL;                                                     --also kleiner als 32766      
      end if;
      s_DeltaI_STATE   <= MULTIPLIKATION1_VORBEREITEN; 
      
   ---------------------------------------------------------------------------------
   when MULTIPLIKATION1_VORBEREITEN => --3.Takt
   ---------------------------------------------------------------------------------
      s_mult_a         <= a_DeltaI       ;  --nur 16BIt werden verwendet    
      s_mult_b         <= Faktor         ; 
      s_DeltaI_STATE   <= MULTIPLIKATION1; 
 
   ---------------------------------------------------------------------------------
   when MULTIPLIKATION1 => --4.Takt
   ---------------------------------------------------------------------------------
      s_mult32         <= s_mult_a * s_mult_b;      -- di x Faktor 
      s_DeltaI_STATE   <= MULT_VERSCHIEBEN   ;
      
   ---------------------------------------------------------------------------------
   when MULT_VERSCHIEBEN => --5.Takt
   ---------------------------------------------------------------------------------
      Case Verschiebung IS
         when "000" =>                                 s_DeltaI_Fak <= s_mult32(30 downto 15) ;  -- keine Verschiebung
         when "001" =>   
            if    s_mult32(31 downto 14) >  32766 then s_DeltaI_Fak <= to_signed( 32766,16)   ;
            elsif s_mult32(31 downto 14) < -32766 then s_DeltaI_Fak <= to_signed(-32766,16)   ;
            else                                       s_DeltaI_Fak <= s_mult32(29 downto 14) ;  -- x2
            end if;
            
         when "010" =>   
            if    s_mult32(31 downto 13) >  32766 then s_DeltaI_Fak <= to_signed( 32766,16)   ;
            elsif s_mult32(31 downto 13) < -32766 then s_DeltaI_Fak <= to_signed(-32766,16)   ;
            else                                       s_DeltaI_Fak <= s_mult32(28 downto 13) ;  -- x4
            end if;

         when "011" => 
            if    s_mult32(31 downto 12) >  32766 then s_DeltaI_Fak <= to_signed( 32766,16)   ;
            elsif s_mult32(31 downto 12) < -32766 then s_DeltaI_Fak <= to_signed(-32766,16)   ;
            else                                       s_DeltaI_Fak <= s_mult32(27 downto 12) ;  -- x8
            end if;
            
         when "100" => 
            if    s_mult32(31 downto 11) >  32766 then s_DeltaI_Fak <= to_signed( 32766,16)   ;
            elsif s_mult32(31 downto 11) < -32766 then s_DeltaI_Fak <= to_signed(-32766,16)   ;
            else                                       s_DeltaI_Fak <= s_mult32(26 downto 11) ;  -- x16
            end if;   
         when others =>                                s_DeltaI_Fak <= s_mult32(30 downto 15) ;
        end Case;
    s_DeltaI_STATE   <= MULTIPLIKATION2_VORBEREITEN;
      
      
   ---------------------------------------------------------------------------------
   when MULTIPLIKATION2_VORBEREITEN => --6.Takt
   ---------------------------------------------------------------------------------
      s_mult_a         <= s_DeltaI_Fak   ;      -- di x Faktor
      s_mult_b         <= Llast          ; 
      s_DeltaI_STATE   <= MULTIPLIKATION2; 
      
   ---------------------------------------------------------------------------------
   when MULTIPLIKATION2 => --7.Takt
   ---------------------------------------------------------------------------------
      s_mult32         <= s_mult_a * s_mult_b;      -- di x Faktor 
      s_DeltaI_STATE   <= DATA_VALID         ;
      
   ---------------------------------------------------------------------------------
   when DATA_VALID => --8.Takt
   ---------------------------------------------------------------------------------
      s_Isoll_vorher  <= s_Isoll_aktuell       ; 
      DeltaI          <= a_DeltaI              ;
      DeltaI_Fak      <= s_DeltaI_Fak          ;
      U_Llast         <= s_mult32(30 downto 15);
      Data_Out_Valid  <= '1'                   ;     
      s_DeltaI_STATE  <= EINGANG_SPEICHERN     ; 
  
   END CASE;
    
   END IF;  --clk
   END IF;  --reset
   END PROCESS p_DeltaIbildung;

END ARCHITECTURE behavioral; 
