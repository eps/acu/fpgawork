LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.std_logic_signed.all;
USE     ieee.numeric_std.all;
USE     ieee.math_real.all;

--===================================================================================================================================================
ENTITY ADC_MovingAverage_16Bitx64 is                                                               -- Mittelung x64 ->1117 LE alte  Logic,  541 neu logic
   generic (                                                                                
      xFachMittelung           : integer := 16                    ;                                -- Wieviel-Fach Mittelung 1=keine Mittelung, sonst 2,4,8,16,32,64,128,256
      Bitbreite                : integer := 16)                   ;
   
   port (                                                                                          -- alle andere Werte nicht zulaessig
      Clock                    : in  std_logic                    ;
      Reset                    : in  std_logic                    ;
      DataInput                : in  signed(Bitbreite-1 downto 0) ;                                -- neuer ADC Wert 
      ReadDataPulse            : in  std_logic                    ;                                -- wenn '1' neuen ADC Wert einlesen und in Array schreiben

      Average                  : out signed(Bitbreite-1 downto 0) ;                                -- Achtung Fenster - Mittelwert, kein echter Mittelwert
      NewValuePulse            : out std_logic                   );                                -- L->H Puls wenn neuer Wert an 'Average' ansteht
end ADC_MovingAverage_16Bitx64 ;


architecture RTL of ADC_MovingAverage_16Bitx64 is
constant cArrayDepth           : integer   := integer(log2(real(xFachMittelung)));                 -- z.B. log2(2) =1

signal   sSum                  : signed    (Bitbreite + cArrayDepth -1 downto 0):= (others =>'0'); -- Summe der Werte 
signal   sIndex                : unsigned  (            cArrayDepth -1 downto 0):= (others =>'0'); -- Index des Spechers
signal   sIndexSumme           : integer   range        xFachMittelung downto 0 := 0             ; -- Index fuer die Summenbildung

type     tArray  is array(xFachMittelung -1 downto 0) of signed(15 downto 0);
signal   sArray                : tArray;

type     tState_MovingAverage is (WAIT_FOR_READ_DATA_PULSE , SUM, DIVIDE);
signal   sState_MovingAverage  : tState_MovingAverage;

begin
--===================================================================================================================================================
   pADC_MovingAverage : process (Clock, Reset)
   begin
      if (Reset = '1') then
         for i in 0 to xFachMittelung -1 loop sArray(i) <= (others =>'0'); end loop; -- Array loeschen

         sSum                       <= (others =>'0');
         sIndex                     <= (others =>'0');
         Average                    <= (others =>'0');
         sIndexSumme                <= 0 ;
         NewValuePulse              <='0';
         sState_MovingAverage       <= WAIT_FOR_READ_DATA_PULSE ; 

      else
         if (Clock'event and Clock = '1') then
            case (sState_MovingAverage) is
               ------------------------------------------------------------------------------
               when WAIT_FOR_READ_DATA_PULSE =>
               ------------------------------------------------------------------------------
                  NewValuePulse                 <= '0'           ; 
                  
                  if (ReadDataPulse = '1') then                                         -- warten das 'ReadDataPulse' = '1' wird            
                     sIndex                     <= sIndex +1     ;
                     sArray(to_integer(sIndex)) <= DataInput     ;                      -- neuer Wert ins Schieberegister
                     sState_MovingAverage       <= SUM           ;
                  end if;

               ------------------------------------------------------------------------------
               when  SUM =>
               ------------------------------------------------------------------------------
                  IF sIndexSumme < xFachMittelung then    
                     sSum                 <= sSum + resize(sArray(sIndexSumme),Bitbreite + cArrayDepth);
                     sIndexSumme          <= sIndexSumme +1;
                  ELSE
                     sIndexSumme          <= 0;
                     sState_MovingAverage <= DIVIDE;
                  END IF;

               ------------------------------------------------------------------------------
               when DIVIDE =>         -- Division der Summe
               ------------------------------------------------------------------------------ 
                  Average                 <= sSum(Bitbreite + cArrayDepth -1 downto cArrayDepth)  ;
                  sSum                    <= (others =>'0')                                       ;
                  NewValuePulse           <= '1'                                                  ; 
                  sState_MovingAverage    <= WAIT_FOR_READ_DATA_PULSE                             ;

            end case;   -- case (sState_MovingAverage) is
         end if;        -- if (Clock'event and Clock = '1') then
      end if;           -- if (Reset = '1') then

   end process pADC_MovingAverage;

end architecture RTL;