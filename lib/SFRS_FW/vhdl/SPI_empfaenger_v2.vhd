--  Filename: SPI_empfaenger.vhd (VHDL File)
--  Authors : A.Wiest

--  26.10.2021, Created
--  empfaengt die Daten ueber SPI
---============================================================================================================================================================
LIBRARY ieee;                                      -- Library Declaration
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;
USE     ieee.math_real.ALL;

ENTITY SPI_empfaenger IS    
                                                   -- Entity Declaration
   GENERIC (        
      takt             : integer  := 100_000_000;  -- 100 MHz
      SPI_takt         : integer  := 3;            -- Anzahl der clocks fuer ein Zustand (high or low) von dem SPI ckl
      SPI_Empf_Versch  : integer  := 1;            -- Anzahl der clocks fuer die Verschiebung des Empfanges (um sicher zu gehen)
      SPI_bit_count    : integer  := 16);          -- Anzahl der bits, die uebertragen werden        

   PORT (
      clk, reset       : IN  std_logic;            -- Control Signals    
      SPI_CLK_IN       : IN  std_logic;    
      SPI_DI           : IN  std_logic;
      SPI_SCn          : OUT std_logic;
      SPI_DI_OK        : OUT std_logic;
      DATA_Output      : OUT std_logic_vector (SPI_bit_count - 1 downto 0));    -- Ausgabe der Empfangenen Daten
        
END SPI_empfaenger;
--=================================================================================================================================================================

ARCHITECTURE behavioral OF SPI_empfaenger IS                                             
   SIGNAL s_SPI_clk                : std_logic  := '0';  
   SIGNAL s_SPI_clkr               : std_logic  := '0';  
   SIGNAL s_SPI_clkr_vorher        : std_logic  := '0';  
   
   SIGNAL s_sync_SPI_CLK1          : std_logic  := '0';  
   SIGNAL s_clk_counter            : integer     range SPI_takt downto 0    := 0;
   
   SIGNAL s_SPI_DI                 : std_logic  := '0'; 
   SIGNAL s_SPI_DIr                : std_logic  := '0'; 
   SIGNAL s_SPI_DIr_vorher         : std_logic  := '0'; 
   
   SIGNAL s_sync_SPI_DI1           : std_logic  := '0';  
   SIGNAL s_Parity_in_spi          : std_logic  := '0';  
   SIGNAL s_SPI_parity_ok          : std_logic  := '0'; 

   SIGNAL s_DATA_Empfangen         : std_logic_vector (SPI_bit_count + 3 downto 0)   := (others=>'0'); -- 1 Startbit + 64 Bit Daten + 1 Parity Bit + 1Stop Bit  +5bit pause
   SIGNAL s_SPI_bit_counter        : integer    range  SPI_bit_count + 3 downto 0    := 0;

   TYPE STATE_SPI_DI IS(WARTE_AUF_DATA, EMPFANG_SCHIEBEN, DATENEMPFANGEN); --Zustaende fuer Data Out SPI
   SIGNAL s_SPI_DI_STATE :  STATE_SPI_DI := WARTE_AUF_DATA;
BEGIN


 
 SPI_CLK_Synchronization : process (clk, reset) --Synchronization of SPI_CKL signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_SPI_CLK1  <= '0';                           -- Data at the fireset clock 
         s_SPI_clkr       <= '0';                           -- Data at the second clock  
      else  
         s_sync_SPI_CLK1  <= SPI_CLK_IN;                    -- 1. Clock
         s_SPI_clkr       <= s_sync_SPI_CLK1;               -- 2. Clock
      end if; 
   end if;  
  end process  SPI_CLK_Synchronization;
  
  
  SPI_DI_Synchronization : process (clk, reset)     --Synchronization of SPI_DI signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_SPI_DI1   <= '0';                           -- Data at the fireset clock 
         s_SPI_DIr        <= '0';                           -- Data at the second clock  
      else  
         s_sync_SPI_DI1   <= SPI_DI;                        -- 1. Clock
         s_SPI_DIr        <=  s_sync_SPI_DI1;               -- 2. Clock
      end if; 
   end if;  
  end process  SPI_DI_Synchronization;


  SPI_DI_Pruefung : process (clk, reset)     --Synchronization of SPI_DI signal
  BEGIN
   if (rising_edge (clk)) then

      if    (s_SPI_DIr = '0' AND s_SPI_DIr_vorher = '0' ) then s_SPI_DI <= '0'; s_SPI_DIr_vorher <= s_SPI_DIr;-- Daten OK
      elsif (s_SPI_DIr = '0' AND s_SPI_DIr_vorher = '0' ) then s_SPI_DI <= '0'; s_SPI_DIr_vorher <= s_SPI_DIr;-- Daten OK
      elsif (s_SPI_DIr = '1' AND s_SPI_DIr_vorher = '1' ) then s_SPI_DI <= '1'; s_SPI_DIr_vorher <= s_SPI_DIr;-- Daten OK
      elsif (s_SPI_DIr = '1' AND s_SPI_DIr_vorher = '1' ) then s_SPI_DI <= '1'; s_SPI_DIr_vorher <= s_SPI_DIr;-- Daten OK
      else
         --(s_SPI_DIr = '0' AND s_SPI_DIr_vorher = '1' AND s_SPI_DIr_vorher = '0'; -- ein Wischer, daten bleiben
         --(s_SPI_DIr = '1' AND s_SPI_DIr_vorher = '0' AND s_SPI_DIr_vorher = '1'; -- ein Wischer, daten bleiben
         --(s_SPI_DIr = '0' AND s_SPI_DIr_vorher = '1' AND s_SPI_DIr_vorher = '1'; --kann nicht bestimmt werden
         --(s_SPI_DIr = '1' AND s_SPI_DIr_vorher = '0' AND s_SPI_DIr_vorher = '0'; --kann nicht bestimmt werden
         s_SPI_DIr_vorher <= s_SPI_DIr;
      end if; 
   end if;  
  end process  SPI_DI_Pruefung;

  
  SPI_CLK_Pruefung : process (clk, reset)     --Synchronization of SPI_DI signal
  BEGIN
   if (rising_edge (clk)) then

      if    (s_SPI_clkr = '0' AND s_SPI_clkr_vorher = '0' ) then s_SPI_clk <= '0'; s_SPI_clkr_vorher <= s_SPI_clkr;-- Clock OK
      elsif (s_SPI_clkr = '0' AND s_SPI_clkr_vorher = '0' ) then s_SPI_clk <= '0'; s_SPI_clkr_vorher <= s_SPI_clkr;-- Clock OK
      elsif (s_SPI_clkr = '1' AND s_SPI_clkr_vorher = '1' ) then s_SPI_clk <= '1'; s_SPI_clkr_vorher <= s_SPI_clkr;-- Clock OK
      elsif (s_SPI_clkr = '1' AND s_SPI_clkr_vorher = '1' ) then s_SPI_clk <= '1'; s_SPI_clkr_vorher <= s_SPI_clkr;-- Clock OK
      else
         --(s_SPI_clkr = '0' AND s_SPI_clkr_vorher = '1' AND s_SPI_clkr_vorher = '0'; -- ein Wischer, daten bleiben
         --(s_SPI_clkr = '1' AND s_SPI_clkr_vorher = '0' AND s_SPI_clkr_vorher = '1'; -- ein Wischer, daten bleiben
         --(s_SPI_clkr = '0' AND s_SPI_clkr_vorher = '1' AND s_SPI_clkr_vorher = '1'; -- kann nicht bestimmt werden
         --(s_SPI_clkr = '1' AND s_SPI_clkr_vorher = '0' AND s_SPI_clkr_vorher = '0'; -- kann nicht bestimmt werden
         s_SPI_clkr_vorher <= s_SPI_clkr;
      end if; 
   end if;  
  end process  SPI_CLK_Pruefung;


--============================================================================
SPI_DI_process :   process (clk, reset)                  -- Daten ueber SPI Empfangen
                                                         -- SPI_DI bits declaration
                                                         -- s_Data_in_reg_spi (0)        = '1' start Bit  
                                                         -- s_Data_in_reg_spi (1 to 64)  = '1' Data bits
                                                         -- s_Data_in_reg_spi (65)       = '1' parity Bit
                                                         -- s_Data_in_reg_spi (66)       = '1' stop Bit
BEGIN
   if (reset = '1') then                                 -- Clock Synchronization
      s_SPI_bit_counter    <= 0;                         -- Counter of input-bits Bits
      s_Parity_in_spi      <= '0';
      s_clk_counter        <= 0;
      s_DATA_Empfangen     <= (others =>'0');
      s_SPI_DI_STATE       <= WARTE_AUF_DATA;
     
   elsif (rising_edge (clk) AND clk = '1') then             -- Writting the input data of SPI
      CASE  s_SPI_DI_STATE  IS
      
      ----------------------------------------------------------------------------------
      when WARTE_AUF_DATA => --1. Takt
      ----------------------------------------------------------------------------------
         s_DATA_Empfangen  <= (others =>'0');
         if s_SPI_clk = '1' then 
            s_SPI_DI_STATE <= EMPFANG_SCHIEBEN;
         end if;
      
      ----------------------------------------------------------------------------------
      when EMPFANG_SCHIEBEN => --1. Takt
      ----------------------------------------------------------------------------------  
         if (s_clk_counter < SPI_Empf_Versch -1) then 
            s_clk_counter  <= s_clk_counter + 1;
         else
            s_SPI_DI_STATE <= DATENEMPFANGEN;
            s_clk_counter  <= 0;
         end if;
         
      ---------------------------------------------------------------------------------
      when DATENEMPFANGEN    => --2. Takt
      ----------------------------------------------------------------------------------
         if (s_clk_counter < SPI_takt - 1 ) then
            s_clk_counter  <= s_clk_counter + 1;

         elsif (s_SPI_bit_counter = 0) then                                                -- reade the Startbit 
            s_DATA_Empfangen(0)                   <= s_SPI_DI;
            s_SPI_bit_counter                     <= s_SPI_bit_counter + 1;
            s_clk_counter                         <= 0; 
      
         elsif (s_SPI_bit_counter > 0 AND s_SPI_bit_counter < SPI_bit_count + 1) then      -- write the Input data    
            if    (s_SPI_DI = '1' AND s_Parity_in_spi ='0') then  s_Parity_in_spi <= '1';  -- ungerade Anzahl der Bits   
            elsif (s_SPI_DI = '0' AND s_Parity_in_spi ='0') then  s_Parity_in_spi <= '0';  -- gerade Anzahl der Bits   
            elsif (s_SPI_DI = '0' AND s_Parity_in_spi ='1') then  s_Parity_in_spi <= '1'; 
            elsif (s_SPI_DI = '1' AND s_Parity_in_spi ='1') then  s_Parity_in_spi <= '0';   
            end if;
            
            s_DATA_Empfangen(s_SPI_bit_counter)   <= s_SPI_DI;    
            s_SPI_bit_counter                     <= s_SPI_bit_counter + 1;     
            s_clk_counter                         <= 0;
                 
         elsif(s_SPI_bit_counter = SPI_bit_count + 1) then                                                              
            s_DATA_Empfangen(s_SPI_bit_counter)   <= s_SPI_DI;                    -- message of Parity error    
            s_SPI_bit_counter                     <= s_SPI_bit_counter + 1;    
             s_clk_counter                        <= 0;
             
            if (s_SPI_DI = s_Parity_in_spi) then  s_SPI_parity_ok <= '1';         -- Empfangener und berechneter Parity bit sind identisch               
            else                                  s_SPI_parity_ok <= '0';   
            end if;  
           
            
         elsif(s_SPI_bit_counter = SPI_bit_count + 2) then        
            s_DATA_Empfangen(s_SPI_bit_counter)   <= s_SPI_DI;                   -- Stop Bit      
            s_SPI_bit_counter                     <= s_SPI_bit_counter + 1;    
            s_clk_counter                         <= 0;
         
         else  
            s_SPI_bit_counter     <=  0;
            s_Parity_in_spi       <= '0';
            SPI_DI_OK             <= s_SPI_parity_ok;
            s_clk_counter         <= 0;
            s_SPI_DI_STATE        <= WARTE_AUF_DATA;
             
            --if (s_DATA_Empfangen(0)='1' AND s_SPI_parity_ok ='1' AND s_DATA_Empfangen(SPI_bit_count + 2)='1')then 
               DATA_Output  <= s_DATA_Empfangen(SPI_bit_count downto 1); -- Daten werden am Ausgang geschrieben
           -- else 
           --    Null;                                                     -- keine Aenderung an den Daten, Wert bleibt erhalten
           -- end if;
            
         end if;
      -----------------------------------------------------------------
      when others =>  
      -----------------------------------------------------------------
         s_SPI_DI_STATE  <= WARTE_AUF_DATA;
      end case;
   
   end if; 

end process SPI_DI_process; 

END behavioral;  
