-- Filename: ta_tb_tc.vhd
-- Erstellt A.Wiest, letzte Aenderung 10.09.2018
--
-- Inhalt:   Bestimmung der Zeiten ta, tb, tc abhängig von Teta und Region der Uab
--
--           Eing. Groessen:
--           	RegionUab des Raumzeigers (1..6),
--					Theta 0..90°, zur Info:
--					ThetaSektorUab 0..60° (Winkel des Raumzeigers ist auf 60° gebrochen), da jeder Sektor bis 60° geht
--              Achtung, 60° entsprechen 2^16-1 * 60°/90°= 43690,  da sinus-Berechnung von 0..90° geht und 16Bit hat
--
--					 T2s - Periodendauer von 2 Takten (Berechnung ist dadurch einfacher)
--              T2s=('0','0'..'1') entspricht einer Taktlänge des FPGA
--              Mit 2^16-1 läst sich eine max. Taktzeit von 0,65ms/2 ->
--              und minimale Frequenz von fmin=3,05kHz realisieren
--              m= Urz/ Ed*cos(30°) - Modulation (0..1,1547)
--              1 entspricht voller Aussteuerung
--
--            Ausg. Groessenen:
--              Zeiten: ta, tb, tc = Ts-(ta+tb)
--              Data_Out_Valid_tabc ='1' wenn die Daten berechnet sind
--------------------------------------------------------------------------------

LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY ta_tb_tc IS

   GENERIC(            												--Initialisierung 100 MHz
      takt						: integer  := 100_000_000  );

   PORT(
      clk,reset,
      Enable					: in std_logic ;						--System-Eingänge:

      Modulation		   	: in unsigned    (15 downto 0); 	-- 0..1
      RegionUab			   : in unsigned    (2  downto 0); 	-- 1..6
		SektorUab				: in unsigned 	  (2  downto 0);  -- 1..6
		ThetaSektorUab			: in unsigned    (15 downto 0); 	-- 0..60°
      Tschalt					: in unsigned    (15 downto 0); 	-- Zeit für eine Periode (Anzahl der Takte x 10ns)

		--Ausgänge (nur positive Werte)
      ta, tb, tc						: out unsigned   (15 downto  0);
		Tsum								: out unsigned	  (15 downto  0);
		SektorTabc, RegionTabc	   : out unsigned   (2 downto  0) ;
      Data_Out_Valid_tabc 			: out std_logic := '0'        );

END ta_tb_tc ;

--============================================================================
ARCHITECTURE behavioral OF ta_tb_tc IS

--Winkelkonstanten-----------------------------------------------
CONSTANT C_pi2 		: unsigned (15 downto 0):= to_unsigned(65535,16); 		-- pi/2 = 90° ist voller Aussteuerbereich
CONSTANT C_pi3 		: unsigned (15 downto 0):= to_unsigned(43690,16); 		-- pi/3= (pi/2)*2/3= (2^16-1)*2/3 = 60°
CONSTANT C_pi6 		: unsigned (15 downto 0):= to_unsigned(21845,16); 		-- pi/6 (pi/2)*1/3= (2^16-1)*1/3 = 30°

CONSTANT C_5_6 		: unsigned (17 downto 0):= to_unsigned(218453,18); 	-- 5/6* (2^18-1) = 300°
CONSTANT C_4_6 		: unsigned (17 downto 0):= to_unsigned(174762,18); 	-- 4/6* (2^18-1) = 240°
CONSTANT C_pi  		: unsigned (17 downto 0):= to_unsigned(131072,18); 	-- 3/6* (2^18-1) = 180°
CONSTANT C_2_6 		: unsigned (17 downto 0):= to_unsigned(87381,18); 		-- 2/6* (2^18-1) = 120°

-- Sinusberechnung
SIGNAL s_Mult1			: unsigned (31 downto 0):=(others=>'0');
SIGNAL s_Mult2			: unsigned (31 downto 0):=(others=>'0');
SIGNAL s_Mult3			: unsigned (31 downto 0):=(others=>'0');
SIGNAL s_Mult21		: unsigned (31 downto 0):=(others=>'0');
SIGNAL s_Mult22		: unsigned (31 downto 0):=(others=>'0');
SIGNAL s_Mult23		: unsigned (31 downto 0):=(others=>'0');
SIGNAL s_Mult24		: unsigned (32 downto 0):=(others=>'0');
SIGNAL s_Mult31		: unsigned (31 downto 0):=(others=>'0');
SIGNAL s_Mult32		: unsigned (31 downto 0):=(others=>'0');
SIGNAL s_Mult33		: unsigned (31 downto 0):=(others=>'0');
SIGNAL s_Mult34		: unsigned (31 downto 0):=(others=>'0');

SIGNAL s_winkel		: unsigned (15 downto 0):=(others=>'0'); 	-- gespeicherte Winkelgroessegebrochen auf 60°
SIGNAL s_sinus_theta : unsigned (15 downto 0):=(others=>'0');	-- Winkel für die Sinusberechnung

SIGNAL s_winkeltyp 	: std_logic := '0';								-- ='0' <51,56°
SIGNAL s_Startsinus  : std_logic := '0';								-- Startkomando fuer die Sinusberechnung
SIGNAL s_sin_finish 	: std_logic := '0';								-- Sinusberechnung ist zu Ende
SIGNAL s_Summe 		: unsigned (18 downto 0):=(others=>'0'); 	-- Sinusergebniss
SIGNAL s_Mult4			: unsigned (31 downto 0):=(others=>'0');	-- m*2*Ts

SIGNAL s_Summe_m		: unsigned (31 downto 0):=(others=>'0');	-- m*2*Ts * sinus_ergebniss

   --Zustaende---------
   TYPE T_STATE_SINUS  IS(	WINKELAUSWAHL,
									MULTIPLIKATION1VOR,
									MULTIPLIKATION1BER,
									MULTIPLIKATION1SAV,
									MULTIPLIKATION2VOR,
									MULTIPLIKATION2BER,
									MULTIPLIKATION2SAV,
									MULTIPLIKATION3VOR,
									MULTIPLIKATION3BER,
									MULTIPLIKATION3SAV,
									ADDITION,
									VORZEICHEN,
									MULTIPLIKATION_M);
   SIGNAL s_STATE_SINUS:	T_STATE_SINUS := WINKELAUSWAHL;


--Gespeicherte Eingänge:---------------------------------
   SIGNAL s_Ts_sp            	: unsigned (15 downto 0):=(others=>'0'); 	-- Taktzeit
   SIGNAL s_Ts2_sp            : unsigned (15 downto 0):=(others=>'0'); 	-- Taktzeit * 2  (vereinfacht die Berechnung)

   SIGNAL s_theta_strich_pos	: unsigned (17 downto 0):=(others=>'0'); 	-- gespeicherter Winkel Theta, gebrochen auf 60°
   SIGNAL s_ThetaSektorUab		: unsigned (17 downto 0):=(others=>'0'); 	-- gespeicherter Winkel Theta, gebrochen auf 60°

   SIGNAL s_Modulation_sp    	: unsigned (15 downto 0):=(others=>'0');
   SIGNAL s_RegionUab_sp      : unsigned (2  downto 0):=(others=>'0');
   SIGNAL s_Sektor_sp        	: unsigned (2  downto 0):=(others=>'0');

--Winkelgroessen-------------------------------------------
   SIGNAL s_Winkela				: unsigned (17 downto 0):=(others=>'0');
   SIGNAL s_Winkelb				: unsigned (17 downto 0):=(others=>'0');
   SIGNAL s_sinus_vorzeichen	:  std_logic := '1';

--Reduzieren der Multiplier--------------
	SIGNAL usig1_mult_a_16  : UNSIGNED (15 downto 0):=(others=>'0');
	SIGNAL usig1_mult_b_16  : UNSIGNED (15 downto 0):=(others=>'0');

	SIGNAL usig2_mult_a_16  : UNSIGNED (15 downto 0):=(others=>'0');
	SIGNAL usig2_mult_b_16  : UNSIGNED (15 downto 0):=(others=>'0');

	SIGNAL usig3_mult_a_17  : UNSIGNED (16 DOWNTO 0):=(others=>'0');
	SIGNAL usig3_mult_b_16  : UNSIGNED (15 DOWNTO 0):=(others=>'0');

	SIGNAL sig4_mult_a_17   : SIGNED (16 DOWNTO 0):=(others=>'0');
	SIGNAL sig4_mult_b_16   : SIGNED (15 DOWNTO 0):=(others=>'0');

	SIGNAL usig5_mult_a_16  : UNSIGNED (15 downto 0):=(others=>'0');
	SIGNAL usig5_mult_b_16  : UNSIGNED (15 downto 0):=(others=>'0');

	SIGNAL usig1_Mult_32  	: UNSIGNED (31 downto 0):=(others=>'0');
	SIGNAL usig2_Mult_32  	: UNSIGNED (31 downto 0):=(others=>'0');
	SIGNAL usig3_Mult_33  	: UNSIGNED (32 downto 0):=(others=>'0');
	SIGNAL sig4_Mult_33  	: SIGNED (32 downto 0):=(others=>'0');
	SIGNAL usig5_Mult_32  	: UNSIGNED (31 downto 0):=(others=>'0');


--Berechnete Ausgänge-----------------------------------
SIGNAL s_Ta       			: unsigned (15 downto 0):=(others=>'0');
SIGNAL s_Tb       			: unsigned (15 downto 0):=(others=>'0');
SIGNAL s_Tc       			: unsigned (15 downto 0):=(others=>'0');


--Sinusergebnis-------------------------------------------
SIGNAL s_Ergebnis1			: unsigned (15 downto 0):=(others=>'0');
SIGNAL s_Ergebnis2			: unsigned (15 downto 0):=(others=>'0');
SIGNAL s_sinus_ergebniss	: signed   (16 downto 0):=(others=>'0');

--Hauptprozess-------------------------------------------
TYPE s_STATE_haupt_prozess IS  (	EINGANG_SPEICHERN,
											WINKELBRECHEN,
											WINKELBESTIMMEN,
											SINUS1_VORBEREITEN,
											SINUSBERECHNEN_1,
											SINUS2_VORBEREITEN,
											SINUSBERECHNEN_2,
											ENDERGEBNISS_BERECHNEN,
											Tab_KORRIGIEREN,
											Tc_BERECHNEN,
											DATEN_RAUSGEBEN);

SIGNAL s_STATE_haupt : s_STATE_haupt_prozess := EINGANG_SPEICHERN;


BEGIN --===========================================================================
p_sinus:process (s_theta_strich_pos, s_Startsinus, clk, reset) --berechnte sinus (0..90) Eingangsvaralphable 16bit UNSIGNED

   --Konstanten-----------------------------------------------
   CONSTANT c_pi2   :   UNSIGNED (15 downto 0):= TO_UNSIGNED(65535,16);	--pi/2 = 90 ist voller Aussteuerbereich
   CONSTANT c_pi51  :   UNSIGNED (15 downto 0):= TO_UNSIGNED(37515,16);	--51,52 = (2^16-1) * 51,52/90

   CONSTANT c_a1    :   UNSIGNED (16 downto 0):= TO_UNSIGNED(102944,17);	--2^17-1 *1,570796/2 ~>2^16-1 * 1.570796
   CONSTANT c_a2    :   UNSIGNED (15 downto 0):= TO_UNSIGNED(42334,16);	--2^16-1 *0,645964
   CONSTANT c_a3    :   UNSIGNED (15 downto 0):= TO_UNSIGNED(5223,16);	--2^16-1 *0,079693
   CONSTANT c_a4    :   UNSIGNED (15 downto 0):= TO_UNSIGNED(307,16);		--2^16-1 *4,681754*10^-3

	CONSTANT c_b1	  :  	UNSIGNED (16 downto 0):= TO_UNSIGNED(80852,17);	--2^17-1 *1,233701/2 = 80852 ~>2^16-1 * 1.233701
   CONSTANT c_b2	  :  	UNSIGNED (15 downto 0):= TO_UNSIGNED(16624,16); --2^16-1 *0,25367
   CONSTANT c_b3    :  	UNSIGNED (15 downto 0):= TO_UNSIGNED(1367,16);	--2^16-1 *0,020863

	ALIAS		a_Mult1 :	UNSIGNED (15 downto 0) IS s_Mult1 (31 downto 16);
	ALIAS		a_Mult2 :	UNSIGNED (15 downto 0) IS s_Mult2 (31 downto 16);
	ALIAS		a_Mult3 :	UNSIGNED (15 downto 0) IS s_Mult3 (31 downto 16);
	ALIAS		a_Mult21:	UNSIGNED (15 downto 0) IS s_Mult21(31 downto 16);
	ALIAS		a_Mult22:	UNSIGNED (15 downto 0) IS s_Mult22(31 downto 16);
	ALIAS		a_Mult23:	UNSIGNED (15 downto 0) IS s_Mult23(31 downto 16);
	ALIAS		a_Mult24:	UNSIGNED (15 downto 0) IS s_Mult24(31 downto 16);
	ALIAS		a_Mult31:	UNSIGNED (15 downto 0) IS s_Mult31(31 downto 16);
	ALIAS		a_Mult32:	UNSIGNED (15 downto 0) IS s_Mult32(31 downto 16);
	ALIAS		a_Mult33:	UNSIGNED (15 downto 0) IS s_Mult33(31 downto 16);

 BEGIN
      if (reset = '1') then
			s_Mult1       	<=(others=>'0');
			s_Mult2       	<=(others=>'0');
			s_Mult3       	<=(others=>'0');
			s_Mult21      	<=(others=>'0');
			s_Mult22      	<=(others=>'0');
			s_Mult23      	<=(others=>'0');
			s_Mult24      	<=(others=>'0');
			s_Mult31      	<=(others=>'0');
			s_Mult32      	<=(others=>'0');
			s_Mult33      	<=(others=>'0');

			s_winkel      	<=(others=>'0');
			s_winkeltyp   	<='0';
			s_Summe       	<=(others=>'0');

			s_STATE_SINUS 	<= WINKELAUSWAHL;

      else
         if rising_edge(clk)   THEN

            CASE s_STATE_SINUS IS

	------------------------------------------------------------------
   when WINKELAUSWAHL =>   --1.Takt
   ------------------------------------------------------------------
		s_sin_finish		<= '0';

		if  (s_Startsinus ='1') then
			If (s_sinus_theta < c_pi51)   then  --51,52 Schnittpunkt, -> kleinsten Fehler
				s_winkel    <= s_sinus_theta;
				s_winkeltyp <= '0';
			else
				s_winkel    <= c_pi2 - s_sinus_theta;
				s_winkeltyp <= '1';
			end If;

			s_STATE_SINUS	<= MULTIPLIKATION1VOR;
		end if;

   ------------------------------------------------------------------
   when MULTIPLIKATION1VOR =>  --2.Takt
   ------------------------------------------------------------------
		usig1_mult_a_16 <= s_winkel;
		usig1_mult_b_16 <= s_winkel;

		usig2_mult_a_16 <= c_a2;
		usig2_mult_b_16 <= s_winkel;

		usig5_mult_a_16 <= c_a4;
		usig5_mult_b_16 <= s_winkel;

      s_STATE_SINUS 	 <= MULTIPLIKATION1BER;

  ------------------------------------------------------------------
	when MULTIPLIKATION1BER =>  --3.Takt
  ------------------------------------------------------------------
		usig1_Mult_32  <= usig1_mult_a_16*usig1_mult_b_16;
		usig2_Mult_32  <= usig2_mult_a_16*usig2_mult_b_16;
		usig5_Mult_32  <= usig5_mult_a_16*usig5_mult_b_16;
		s_STATE_SINUS 	<= MULTIPLIKATION1SAV;

  ------------------------------------------------------------------
	when MULTIPLIKATION1SAV =>  --4.Takt
  ------------------------------------------------------------------
		s_Mult1        <= usig1_Mult_32;
		s_Mult2        <= usig2_Mult_32;
		s_Mult3        <= usig5_Mult_32;
		s_STATE_SINUS 	<= MULTIPLIKATION2VOR;

  ------------------------------------------------------------------
	when MULTIPLIKATION2VOR =>  --5.Takt
  ------------------------------------------------------------------
	   CASE s_winkeltyp IS
			when '0' =>
				usig1_mult_a_16 <= a_Mult1;
				usig1_mult_b_16 <= a_Mult3;
				usig2_mult_a_16 <= c_a3;
				usig2_mult_b_16 <= s_winkel;
				usig3_mult_a_17 <= c_a1;
				usig3_mult_b_16 <= s_winkel;

         when others =>
				usig1_mult_a_16 <= c_b3;
				usig1_mult_b_16 <= a_Mult1;
				usig3_mult_a_17 <= c_b1;
				usig3_mult_b_16 <= a_Mult1;
		END CASE;

		usig5_mult_a_16   <= a_Mult1;
		usig5_mult_b_16   <= a_Mult1;
		s_STATE_SINUS 		<= MULTIPLIKATION2BER;

  ------------------------------------------------------------------
	when MULTIPLIKATION2BER =>  --6.Takt
  ------------------------------------------------------------------
		usig1_Mult_32  <= usig1_mult_a_16*usig1_mult_b_16;
		usig2_Mult_32  <= usig2_mult_a_16*usig2_mult_b_16; --Value only vald if s_winkeltyp='0'
		usig3_Mult_33  <= usig3_mult_a_17*usig3_mult_b_16;
		usig5_Mult_32  <= usig5_mult_a_16*usig5_mult_b_16;
		s_STATE_SINUS 	<= MULTIPLIKATION2SAV;

  ------------------------------------------------------------------
	when MULTIPLIKATION2SAV =>  --7.Takt
  ------------------------------------------------------------------
		s_Mult22       <= usig1_Mult_32;
		s_Mult23       <= usig2_Mult_32;
		s_Mult24       <= usig3_Mult_33;
		s_Mult21       <= usig5_Mult_32;
		s_STATE_SINUS 	<= MULTIPLIKATION3VOR;

  ------------------------------------------------------------------
	when MULTIPLIKATION3VOR =>  --8.Takt
  ------------------------------------------------------------------
		CASE s_winkeltyp IS
			when '0'    =>
				usig1_mult_a_16 <= a_Mult21;
				usig1_mult_b_16 <= a_Mult23;
				usig2_mult_a_16 <= a_Mult1;
				usig2_mult_b_16 <= a_Mult2;
         when others =>
				usig1_mult_a_16 <= c_b2;
				usig1_mult_b_16 <= a_Mult21;
		END CASE;

		usig5_mult_a_16   <= a_Mult21;
		usig5_mult_b_16   <= a_Mult22;
      s_STATE_SINUS     <= MULTIPLIKATION3BER;

  ------------------------------------------------------------------
	when MULTIPLIKATION3BER =>  --9.Takt
  ------------------------------------------------------------------
		usig1_Mult_32  <= usig1_mult_a_16*usig1_mult_b_16;
		usig2_Mult_32  <= usig2_mult_a_16*usig2_mult_b_16;
		usig5_Mult_32  <= usig5_mult_a_16*usig5_mult_b_16;
      s_STATE_SINUS 	<= MULTIPLIKATION3SAV;

  ------------------------------------------------------------------
	when MULTIPLIKATION3SAV =>  --10.Takt
  ------------------------------------------------------------------
		s_Mult32       <= usig1_Mult_32;
		s_Mult33       <= usig2_Mult_32;
		s_Mult31       <= usig5_Mult_32;
      s_STATE_SINUS 	<= ADDITION;

   ------------------------------------------------------------------
   when ADDITION =>    --11. Takt, bede Reihen aufaddieren
   ------------------------------------------------------------------
      CASE s_winkeltyp IS
         when '0'    =>
            s_Summe <= resize(a_Mult24,19) - resize(a_Mult33,19)
							+ resize(a_Mult32,19) - resize(a_Mult31,19);
         when others =>
            s_Summe <= resize(c_pi2,19)    - resize(a_Mult24,19)
							+ resize(a_Mult32,19) - resize(a_Mult31,19);
      END CASE;
      s_STATE_SINUS 	<= VORZEICHEN;

   ------------------------------------------------------------------
   when VORZEICHEN =>    --12. Takt,  Vorzeichen aufsetzen
   ------------------------------------------------------------------
      if s_Summe > TO_UNSIGNED(65535,16) then
			s_Summe <= TO_UNSIGNED(65535,19);
		else

		end if;

      s_STATE_SINUS 	<= MULTIPLIKATION_M;

	------------------------------------------------------------------
   when MULTIPLIKATION_M =>    --7.  Begrenzung, falls erforderlich
   ------------------------------------------------------------------
      s_Summe_m 		<= s_Summe(15 downto 0) * s_Mult4(31 downto 16);
		s_sin_finish	<= '1';
      s_STATE_SINUS 	<= WINKELAUSWAHL;
   ----------------------------------------------------------------------------
   when others => --Wird theoretisch nie erreicht
   ----------------------------------------------------------------------------
      s_STATE_SINUS	<= WINKELAUSWAHL;

   END CASE;
   END IF;  --clk
   END IF;  --reset

END process p_sinus;

------------------------------------------------------------------------------------
p_haupt_prozess: process(reset, clk, Enable) --Hauptprozess
------------------------------------------------------------------------------------
begin
   if reset = '1' then
      Data_Out_Valid_tabc  <= '0';   					-- Ausgang nicht bereit
		SektorTabc				<= "001";
		RegionTabc				<= "001";
		s_ThetaSektorUab		<= (others=>'0');			-- 0..60
		s_Ts_sp					<= (others=>'0');			-- Periodendauer einer Periode, gespeichert
      s_Ts2_sp					<= (others=>'0');			-- Periodendauer zwei Perioden, gespeichert
		s_Mult4					<= (others=>'0');
		s_STATE_haupt			<= EINGANG_SPEICHERN;

   elsif rising_edge(clk)  then
      case s_STATE_haupt is

  -----------------------------------------------------------------------------
  when EINGANG_SPEICHERN =>     --1. Uebernimmt die Daten am Eingang
  -----------------------------------------------------------------------------
   if (Enable = '1') then                           	-- Eingänge speichern:
      s_ThetaSektorUab  <= "00" & ThetaSektorUab;
      s_Modulation_sp   <= Modulation     ;
      s_RegionUab_sp    <= RegionUab      ;
		s_Sektor_sp			<= SektorUab		;
      s_Ts_sp				<= Tschalt;
		s_Ts2_sp				<= Tschalt(14 downto 0) & '0'; -- 2*Tschalt
		s_STATE_haupt		<= WINKELBRECHEN;
	end if;

   Data_Out_Valid_tabc	<= '0';								-- Ausgang nicht bereit

   -----------------------------------------------------------------------------
   when WINKELBRECHEN =>
   ---------------------------------------------------------------------------
	   s_Mult4				<= s_Ts2_sp * s_Modulation_sp;   -- =2*Ts*m
	   s_STATE_haupt		<= WINKELBESTIMMEN;

   -----------------------------------------------------------------------------
   when WINKELBESTIMMEN =>
   ----------------------------------------------------------------------------
      CASE s_RegionUab_sp IS
         when "001"|"010"  => 								--kleiner 60°
            s_Winkela <= C_pi3 - s_ThetaSektorUab;		--60° - s_ThetaSektorUab
   
            if (s_ThetaSektorUab < C_pi6)                    then
            s_Winkelb <= C_pi3 + s_ThetaSektorUab;           else
            s_Winkelb <= C_pi2 - (s_ThetaSektorUab - C_pi6); end if;
   
         when "011"       =>
            s_Winkelb <= s_ThetaSektorUab;
   
            if (s_ThetaSektorUab < C_pi6)                    then
            s_Winkela <= C_pi3 + s_ThetaSektorUab;           else
            s_Winkela <= C_pi2 - (s_ThetaSektorUab - C_pi6); end if;
   
         when "100"| "101" =>
            s_Winkela <= s_ThetaSektorUab;
   
            if (s_ThetaSektorUab < C_pi6)                    then
            s_Winkelb <= C_pi3 + s_ThetaSektorUab;           else
            s_Winkelb <= C_pi2 - (s_ThetaSektorUab - C_pi6); end if;
   
         when "110"        =>
            s_Winkela <= s_ThetaSektorUab;
            s_Winkelb <= C_pi3 - s_ThetaSektorUab;
   
         when others       =>
            s_Winkela <= (others=>'0');
            s_Winkelb <= (others=>'0');
      END CASE;

	s_STATE_haupt   <= SINUS1_VORBEREITEN;

   -----------------------------------------------------------------------------
   when SINUS1_VORBEREITEN =>   -- Sinusprozess starten
   -----------------------------------------------------------------------------
	   s_Startsinus 		<= '1';
	   s_sinus_theta		<= s_Winkela(15 downto 0); --Achtung 90° entsprechen 2^16=65536, da 16Bit unsigned
	   s_STATE_haupt     <= SINUSBERECHNEN_1;

   -----------------------------------------------------------------------------
   when SINUSBERECHNEN_1 =>   -- Sinusprozess starten
   -----------------------------------------------------------------------------
	   s_Startsinus 		<= '0';

      if (s_sin_finish = '1') then
         s_Ergebnis1 	<= s_Summe_m(31 downto 16);
         s_STATE_HAUPT	<= SINUS2_VORBEREITEN;
      end if;

   ----------------------------------------------------------------------------
   when SINUS2_VORBEREITEN =>   -- Sinusprozess starten
   -----------------------------------------------------------------------------
      s_Startsinus 		<= '1';
      s_sinus_theta		<= s_Winkelb(15 downto 0); --Achtung 90° entsprechen 2^16=65536, da 16Bit unsigned
      s_STATE_haupt     <= SINUSBERECHNEN_2;

   -----------------------------------------------------------------------------
   when SINUSBERECHNEN_2 =>   -- Sinusprozess starten
   -----------------------------------------------------------------------------
      s_Startsinus 		<= '0';
   
      if (s_sin_finish = '1') then
         s_Ergebnis2 	<= s_Summe_m(31 downto 16);
         s_STATE_HAUPT	<= ENDERGEBNISS_BERECHNEN;
      end if;

   ---------------------------------------------------------------------------
   when ENDERGEBNISS_BERECHNEN =>	--  Berechne: ta = xa0 + xa1*sin(xa2)
                                    --  Berechne: tb = xb0 + xb1*sin(xb2)
                                    --  Berechne: tc = Ts - ta - tb
   ---------------------------------------------------------------------------
      CASE s_RegionUab_sp IS
         when "001"|"010" =>							-- RegionUab 1 oder 2
            s_Ta <= s_Ergebnis1;						-- 2*Ts*Modulation*sin(xa)
            s_Tb <= s_Ts_sp - s_Ergebnis2;		-- Ts - 2*Ts*Modulation*sin(xb)
   
         when "011"  =>									-- RegionUab 3
            s_Ta <= s_Ts2_sp -  s_Ergebnis1;		-- 2*Ts - 2*Ts*Modulation*sin(xa)
            s_Tb <= s_Ergebnis2;						-- 2*Ts*Modulation*sin(xb)
   
         when "100"|"101" =>							-- RegionUab 4 oder 5
            s_Ta <= s_Ts_sp - s_Ergebnis1;		-- Ts - 2*Ts*Modulation*sin(xa)
            s_Tb <= s_Ergebnis2 - s_Ts_sp;		-- -Ts + 2*Ts*Modulation*sin(xb)
   
         when "110"  =>									-- RegionUab 6
            s_Ta <= s_Ergebnis1 - s_Ts_sp;		-- -Ts + 2*Ts*Modulation*sin(xa)
            s_Tb <= s_Ergebnis2;						-- 2*Ts*Modulation*sin(xb)
   
         when others => --7 gibt es nicht
            s_Ta <= (others=>'0');
            s_Tb <= (others=>'0');
            s_Tc <= (others=>'0');
      END CASE;

   s_STATE_haupt <= Tab_KORRIGIEREN;

   -----------------------------------------------------------------------------
   when Tab_KORRIGIEREN => -- tc= Ts - ta - tb
   -------------------------------------------------------------------------
      IF s_Ta > s_Ts_sp + to_unsigned(10,16) THEN --Ueberlauf, total falsche Berechnung
         s_Ta <=(others=>'0');    
      elsif s_Ta > s_Ts_sp then
         s_Ta <= s_Ts_sp;
      --elsif s_Ta < to_unsigned(10,16) then
      --   s_Ta <= to_unsigned(9,16); -- damit kein Null entsteht, sonnst DAC macht ein Spike, kann man nicht unterscheiden, DAC oder Berechnung
      end if;
      
      IF s_Tb > s_Ts_sp + to_unsigned(10,16) THEN
         s_Tb <=(others=>'0'); 
      
      elsif s_Tb > s_Ts_sp then
         s_Tb <= s_Ts_sp;
      --elsif s_Tb < to_unsigned(10,16) then
      --   s_Tb <= to_unsigned(9,16); 
      end if;
      
      s_STATE_haupt 	<= Tc_BERECHNEN;
  
  -----------------------------------------------------------------------------
  when Tc_BERECHNEN => -- tc= Ts -ta -tb
  -------------------------------------------------------------------------
		if (s_Ta + s_Tb) >= s_Ts_sp then s_Tc <= (others=>'0');
		else			                     s_Tc <= s_Ts_sp - s_Ta - s_Tb;
		end if;

	   s_STATE_haupt 	<= DATEN_RAUSGEBEN;

  -----------------------------------------------------------------------------
  when DATEN_RAUSGEBEN => -- alle Daten werden gleichzeitig rausgegeben
  -----------------------------------------------------------------------------
		ta                 	 <= s_Ta;
		tb                 	 <= s_Tb;
		tc                 	 <= s_Tc;
		TSum						 <= s_Ta + s_Tb + s_Tc;
		SektorTabc				 <= s_Sektor_sp;
		RegionTabc				 <= s_RegionUab_sp;

		Data_Out_Valid_tabc 	 <= '1';    -- Daten bereit
		s_STATE_haupt       	 <= EINGANG_SPEICHERN;

  ----------------------------------------------------------------------------
  when others =>          -- Absicherung für FPGA
  ----------------------------------------------------------------------------
		s_STATE_haupt      <= EINGANG_SPEICHERN;

   end case;

   end if;
END process p_haupt_prozess ;

END ARCHITECTURE behavioral;
