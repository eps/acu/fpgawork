--* Version : 1.0 Born (14.1.2019) - JM)
--  Version : 2.0 Born (02.10.2019) -AW)


-- Beschreibung:
-- Dieses VHDL-Modul berechnet den Sinus mit einem Taylorpolynom um 0° und um 90° mit einem Switch bei ca. 51,52°. Da der Sinus in jedem Quadranten symmetrisch ist,
-- lässt sich der Bereich von 0-90 auf 0-360 strecken, durch x-Achsenverschiebung des Inputs und ggf. Negierung des Ausgangs.
--
-- Eing. Groessen:
-- clk bspw. 100MHz system clock
-- reset Berechnung neue starten, alles auf Null setzen
-- Enable symbolisiert Gueltigkeit der Eingangsdaten
-- theta 18 Bit Signed Input entspricht 0-360°

-- Ausg. Groessen:
-- Sine 16bit signed -> -1...1
-- Data_Out_Valid wenn die Daten berechnet sind = '1'

--========================================================================

LIBRARY 	ieee;
USE 		ieee.std_logic_1164.ALL;
USE 		ieee.numeric_std.ALL;

ENTITY SinGen6 IS
	GENERIC 	(
		takt 			   : INTEGER 	 := 100_000_000; 									-- Initialisierung 100 MHz
		g_ticksProInc  : integer    := 8 ;												-- 8 entsp. ungefaehr 50 Hz(47,68 Hz)
		g_theta_corr   : UNSIGNED (15 downto 0) := to_unsigned(104/8,16));   -- Um so viele Takte wird die Ausgabe fuer Theta nacheilend verschoben

	PORT(
		--System-Eingaenge:
		clk, reset,	Enable 		: IN STD_LOGIC;

		--Amplitudenfaktor, Achtung 32767 ist Fullscale!!!! sonst gibt es überlauf!!
		amplitude_G1 				: IN UNSIGNED (15 DOWNTO 0):= (OTHERS => '0'); -- Amplitude des Ausgangs-Gruppe1 zwischen 0 und 1 -> 2^16-1 ^= 1 (maximal)
		amplitude_G2 				: IN UNSIGNED (15 DOWNTO 0):= (OTHERS => '0'); -- Amplitude des Ausgangs-Gruppe2 zwischen 0 und 1 -> 2^16-1 ^= 1 (maximal)

		--Phasenverschiebung
		ph_shift1_2 				: IN UNSIGNED (17 DOWNTO 0):= (OTHERS => '0'); -- Phasenverschiebung zwische Gruppe1 und Gruppe2
																									  -- um diesen winkel eilt das zweite System nach


		--Winkel
		theta  						: IN  UNSIGNED (17 DOWNTO 0):= (OTHERS => '0'); -- Generator rechnet nur 0-90 mit 16 Bit -> 18 Bit für alle 4 Quadranten
		theta_out_corr				: OUT UNSIGNED (17 DOWNTO 0):= (OTHERS => '0'); -- Winkel wird ausgegeben, versehen mit Korrekturfaktor für die Berechnung
		theta_extern            : IN  STD_LOGIC             := '1';	            -- =1 theta wird vom Extern vorgegebend =0 theta wird intern berechnet

		--Ausgaenge Gruppe1
		Sine_ph1           		: OUT SIGNED (15 DOWNTO 0) := (OTHERS => '0'); -- Ausgang steht für -1 bis 1
		Sine_ph2           		: OUT SIGNED (15 DOWNTO 0) := (OTHERS => '0'); -- Ausgang steht für -1 bis 1
		Sine_ph3           		: OUT SIGNED (15 DOWNTO 0) := (OTHERS => '0'); -- Ausgang steht für -1 bis 1

		--Ausgaenge Gruppe2
		Sine_ph_a           		: OUT SIGNED (15 DOWNTO 0) := (OTHERS => '0'); -- Ausgang steht für -1 bis 1
		Sine_ph_b          		: OUT SIGNED (15 DOWNTO 0) := (OTHERS => '0'); -- Ausgang steht für -1 bis 1
		Sine_ph_c          		: OUT SIGNED (15 DOWNTO 0) := (OTHERS => '0'); -- Ausgang steht für -1 bis 1

		Data_Out_Valid 			: OUT STD_LOGIC := '0' );

	END SinGen6;

	----Architektur 1--------------------------------------------------------------
	ARCHITECTURE behavioral OF SinGen6 IS

		--SINUS-prozess
		SIGNAL s_Sine   	: signed   (15 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Theta  	: UNSIGNED (17 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Theta_in : UNSIGNED (17 DOWNTO 0) := (OTHERS => '0');

		--Sinusergebnis-------------------------------------------
		--Fuer Multiplikation
		SIGNAL s_Mult1  : UNSIGNED (31 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Mult2  : UNSIGNED (31 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Mult3  : UNSIGNED (31 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Mult21 : UNSIGNED (31 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Mult22 : UNSIGNED (31 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Mult23 : UNSIGNED (31 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Mult24 : UNSIGNED (32 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Mult31 : UNSIGNED (31 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Mult32 : UNSIGNED (31 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Mult33 : UNSIGNED (31 DOWNTO 0) := (OTHERS => '0');

		--Reduzieren der Multiplier--------------

		--Signale
		SIGNAL usig1_mult_a_16    	: UNSIGNED (15 downto 0):=(others=>'0');
		SIGNAL usig1_mult_b_16    	: UNSIGNED (15 downto 0):=(others=>'0');

		SIGNAL usig2_mult_a_16    	: UNSIGNED (15 downto 0):=(others=>'0');
		SIGNAL usig2_mult_b_16    	: UNSIGNED (15 downto 0):=(others=>'0');

		SIGNAL usig3_mult_a_17    	: UNSIGNED (16 DOWNTO 0):=(others=>'0');
		SIGNAL usig3_mult_b_16    	: UNSIGNED (15 DOWNTO 0):=(others=>'0');

		SIGNAL sig4_mult_a_17    	: SIGNED   (16 DOWNTO 0):=(others=>'0');
		SIGNAL sig4_mult_b_16    	: SIGNED   (15 DOWNTO 0):=(others=>'0');

		SIGNAL usig5_mult_a_16    	: UNSIGNED (15 downto 0):=(others=>'0');
		SIGNAL usig5_mult_b_16    	: UNSIGNED (15 downto 0):=(others=>'0');


		SIGNAL usig1_Mult_32  		: UNSIGNED (31 downto 0):=(others=>'0');
		SIGNAL usig2_Mult_32  		: UNSIGNED (31 downto 0):=(others=>'0');
		SIGNAL usig3_Mult_33  		: UNSIGNED (32 downto 0):=(others=>'0');
		SIGNAL sig4_Mult_33  		: SIGNED   (32 downto 0):=(others=>'0');
		SIGNAL usig5_Mult_32  		: UNSIGNED (31 downto 0):=(others=>'0');

		--Winkelgroessen-------------------------------------------
		SIGNAL s_sinus_Theta      	: UNSIGNED (17 DOWNTO 0) := (OTHERS => '0');

		--Winkel----------
		SIGNAL s_winkel           	: unsigned (15 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_winkeltyp        	: std_logic := '0';
		SIGNAL s_quadrant         	: INTEGER RANGE 3 DOWNTO 0 := 0;

		-- Quadranten in mathematischer Reihenfolge aber eins runter also anstatt 1-4 -> 0-3:
		-- 1 | 0
		-- -----
		-- 2 | 3


		--Sinus Endergebnis-----
		SIGNAL s_Summe            : UNSIGNED (18 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_Ampl				  : UNSIGNED (31 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_sinus_ergebniss  : signed   (15 DOWNTO 0) := (OTHERS => '0');
		SIGNAL s_sin_finish		  : std_logic 					:= '0';

		--Zustaende fuer die Sinusberechnung---------
		TYPE T_STATE_SINUS IS(QUADRANTAUSWAHL,
		WINKELAUSWAHL,
		MULTIPLIKATION1VOR,	MULTIPLIKATION1BER,	MULTIPLIKATION1SAV,
		MULTIPLIKATION2VOR,	MULTIPLIKATION2BER,	MULTIPLIKATION2SAV,
		MULTIPLIKATION3VOR,	MULTIPLIKATION3BER,	MULTIPLIKATION3SAV,
		ADDITION,
		AMPLITUDE_BERECHNEN,
		VORZEICHEN,
		QUADRANTENAUFBAU);
		SIGNAL s_STATE_SINUS : T_STATE_SINUS := QUADRANTAUSWAHL;

		--Zustaende fuer das Hauptprozess---------
		TYPE T_STATE IS(	EINGANG_SPEICHERN,
								BERECHNUNG_VORBEREITEN,
								BERECHNUNG_STARTEN,
								ENDERGEBNISS_AUSGEBEN);
		SIGNAL s_STATE_HAUPT: T_STATE := EINGANG_SPEICHERN;

		SIGNAL s_count       : INTEGER RANGE 1 to 6   := 1;
		SIGNAL s_start_sin   : std_logic 				 := '0';
		SIGNAL s_amplitude   : unsigned(15 DOWNTO 0)  := (OTHERS => '0');
		SIGNAL s_ph_shift1_2 : unsigned(17 DOWNTO 0)  := (OTHERS => '0');

		SIGNAL s_Sine_ph1  : signed(15 DOWNTO 0) 	  := (OTHERS => '0');
		SIGNAL s_Sine_ph2  : signed(15 DOWNTO 0) 	  := (OTHERS => '0');
		SIGNAL s_Sine_ph3  : signed(15 DOWNTO 0) 	  := (OTHERS => '0');
		SIGNAL s_Sine_ph_a : signed(15 DOWNTO 0) 	  := (OTHERS => '0');
		SIGNAL s_Sine_ph_b : signed(15 DOWNTO 0) 	  := (OTHERS => '0');
		SIGNAL s_Sine_ph_c : signed(15 DOWNTO 0) 	  := (OTHERS => '0');

		--Interner Generator
		SIGNAL s_sawTooth 		: unsigned(17 DOWNTO 0)  := (OTHERS => '0');
		SIGNAL s_counter_winkel : unsigned(7 DOWNTO 0)   := (OTHERS => '0');


		BEGIN

			--===========================================================================
			sinus : PROCESS (s_Theta, clk, reset, s_amplitude, s_start_sin) --berechnte sinus (0..90) Eingangsvariable 16bit unsigned

				--Konstanten----------------------------------------------
				CONSTANT c_2pi  : unsigned (17 DOWNTO 0) := to_unsigned(262143, 18); --2*pi
				CONSTANT c_3pi4 : unsigned (17 DOWNTO 0) := to_unsigned(196608, 18); --3*pi/4 = 270
				CONSTANT c_pi   : unsigned (17 DOWNTO 0) := to_unsigned(131072, 18); --pi = 180
				CONSTANT c_pi2  : unsigned (15 DOWNTO 0) := to_unsigned(65535, 16); 	--pi/2 = 90 ist voller Aussteuerbereich der Berechnung
				CONSTANT c_pi51 : unsigned (15 DOWNTO 0) := to_unsigned(37515, 16); 	--51,52 = (2^16-1) * 51,52/90

				CONSTANT c_a1   : unsigned (16 DOWNTO 0) := to_unsigned(102944, 17); --2^17-1 *1,570796/2
				CONSTANT c_a2   : unsigned (15 DOWNTO 0) := to_unsigned(42334, 16); 	--2^16-1 *0,645964
				CONSTANT c_a3   : unsigned (15 DOWNTO 0) := to_unsigned(5223, 16); 	--2^16-1 *0,079693
				CONSTANT c_a4   : unsigned (15 DOWNTO 0) := to_unsigned(307, 16); 	--2^16-1 *4,681754*10^-3

				CONSTANT c_b1   : unsigned (16 DOWNTO 0) := to_unsigned(80852, 17); 	--2^17-1 *1,233701/2 = 80852
				CONSTANT c_b2   : unsigned (15 DOWNTO 0) := to_unsigned(16624, 16); 	--2^16-1 *0,25367
				CONSTANT c_b3   : unsigned (15 DOWNTO 0) := to_unsigned(1367, 16); 	--2^16-1 *0,020863

				ALIAS a_Mult1   : unsigned (15 DOWNTO 0) IS s_Mult1 (31 DOWNTO 16);
				ALIAS a_Mult2   : unsigned (15 DOWNTO 0) IS s_Mult2 (31 DOWNTO 16);
				ALIAS a_Mult3   : unsigned (15 DOWNTO 0) IS s_Mult3 (31 DOWNTO 16);
				ALIAS a_Mult21  : unsigned (15 DOWNTO 0) IS s_Mult21(31 DOWNTO 16);
				ALIAS a_Mult22  : unsigned (15 DOWNTO 0) IS s_Mult22(31 DOWNTO 16);
				ALIAS a_Mult23  : unsigned (15 DOWNTO 0) IS s_Mult23(31 DOWNTO 16);
				ALIAS a_Mult24  : unsigned (15 DOWNTO 0) IS s_Mult24(31 DOWNTO 16);
				ALIAS a_Mult31  : unsigned (15 DOWNTO 0) IS s_Mult31(31 DOWNTO 16);
				ALIAS a_Mult32  : unsigned (15 DOWNTO 0) IS s_Mult32(31 DOWNTO 16);
				ALIAS a_Mult33  : unsigned (15 DOWNTO 0) IS s_Mult33(31 DOWNTO 16);

			BEGIN
				IF (reset = '1') THEN
					s_Mult1        <= (OTHERS => '0');
					s_Mult2        <= (OTHERS => '0');
					s_Mult3        <= (OTHERS => '0');
					s_Mult21       <= (OTHERS => '0');
					s_Mult22       <= (OTHERS => '0');
					s_Mult23       <= (OTHERS => '0');
					s_Mult24       <= (OTHERS => '0');
					s_Mult31       <= (OTHERS => '0');
					s_Mult32       <= (OTHERS => '0');
					s_Mult33       <= (OTHERS => '0');

					s_winkel       <= (OTHERS => '0');
					s_winkeltyp    <= '0';
					s_Summe        <= (OTHERS => '0');
					s_sin_finish   <= '0';
					s_STATE_SINUS  <= WINKELAUSWAHL;

				ELSE
					IF rising_edge(clk) THEN

						CASE s_STATE_SINUS IS
							------------------------------------------------------------------
							WHEN QUADRANTAUSWAHL => --1.Takt
							------------------------------------------------------------------
								s_sin_finish <= '0';

								IF (s_start_sin = '1') THEN
									IF s_Theta    <= c_pi2 THEN
										s_quadrant    <= 0;
										s_sinus_Theta <= s_Theta;

									ELSIF s_Theta <= c_pi THEN
										s_quadrant    <= 1;
										s_sinus_Theta <= c_pi - s_Theta;

									ELSIF s_Theta <= c_3pi4 THEN
										s_quadrant    <= 2;
										s_sinus_Theta <= s_Theta - c_pi;

									ELSE
										s_quadrant    <= 3;
										s_sinus_Theta <= c_2pi - s_Theta;
									END IF;

								s_STATE_SINUS <= WINKELAUSWAHL;

								ELSE
									NULL;
								END IF;

							------------------------------------------------------------------
							WHEN WINKELAUSWAHL => --1.Takt
							------------------------------------------------------------------
								IF (s_sinus_Theta < c_pi51) THEN --51,52 Schnittpunkt, -> kleinsten Fehler
									s_winkel     <= s_sinus_Theta(15 DOWNTO 0);
									s_winkeltyp  <= '0';
								ELSE
									s_winkel     <= c_pi2 - s_sinus_Theta(15 DOWNTO 0);
									s_winkeltyp  <= '1';
								END IF;
								s_STATE_SINUS   <= MULTIPLIKATION1VOR;

							------------------------------------------------------------------
							when MULTIPLIKATION1VOR =>  --2.Takt
							------------------------------------------------------------------
								usig1_mult_a_16 <= s_winkel;
								usig1_mult_b_16 <= s_winkel;

								usig2_mult_a_16 <= c_a2;
								usig2_mult_b_16 <= s_winkel;

								usig5_mult_a_16 <= c_a4;
								usig5_mult_b_16 <= s_winkel;

								s_STATE_SINUS 	 <= MULTIPLIKATION1BER;

						   ------------------------------------------------------------------
							when MULTIPLIKATION1BER =>  --3.Takt
						   ------------------------------------------------------------------
								usig1_Mult_32 	 <= usig1_mult_a_16*usig1_mult_b_16;
								usig2_Mult_32 	 <= usig2_mult_a_16*usig2_mult_b_16;
								usig5_Mult_32 	 <= usig5_mult_a_16*usig5_mult_b_16;
								s_STATE_SINUS 	 <= MULTIPLIKATION1SAV;

						   ------------------------------------------------------------------
							when MULTIPLIKATION1SAV =>  --4.Takt
						   ------------------------------------------------------------------
								s_Mult1 			 <= usig1_Mult_32;
								s_Mult2 			 <= usig2_Mult_32;
								s_Mult3 			 <= usig5_Mult_32;
								s_STATE_SINUS 	 <= MULTIPLIKATION2VOR;

						   ------------------------------------------------------------------
							when MULTIPLIKATION2VOR =>  --5.Takt
						   ------------------------------------------------------------------
								CASE s_winkeltyp IS

									when '0' =>
										usig1_mult_a_16<= a_Mult1;
										usig1_mult_b_16<= a_Mult3;

										usig2_mult_a_16<= c_a3;
										usig2_mult_b_16<= s_winkel;

										usig3_mult_a_17<= c_a1;
										usig3_mult_b_16<= s_winkel;

									when others =>
										usig1_mult_a_16<= c_b3;
										usig1_mult_b_16<= a_Mult1;

										usig3_mult_a_17<= c_b1;
										usig3_mult_b_16<= a_Mult1;
								END CASE;

								usig5_mult_a_16 		<= a_Mult1;
								usig5_mult_b_16 		<= a_Mult1;
								s_STATE_SINUS 			<= MULTIPLIKATION2BER;

						   ------------------------------------------------------------------
							when MULTIPLIKATION2BER =>  --6.Takt
						   ------------------------------------------------------------------
								usig1_Mult_32 			<= usig1_mult_a_16*usig1_mult_b_16;
								usig2_Mult_32 			<= usig2_mult_a_16*usig2_mult_b_16; --Value only valid if s_winkeltyp='0'
								usig3_Mult_33 			<= usig3_mult_a_17*usig3_mult_b_16;
								usig5_Mult_32 			<= usig5_mult_a_16*usig5_mult_b_16;
								s_STATE_SINUS 			<= MULTIPLIKATION2SAV;

						   ------------------------------------------------------------------
							when MULTIPLIKATION2SAV =>  --7.Takt
						   ------------------------------------------------------------------
								s_Mult22 				<= usig1_Mult_32;
								s_Mult23 				<= usig2_Mult_32;
								s_Mult24 				<= usig3_Mult_33;
								s_Mult21 				<= usig5_Mult_32;
								s_STATE_SINUS 			<= MULTIPLIKATION3VOR;

						   ------------------------------------------------------------------
							when MULTIPLIKATION3VOR =>  --8.Takt
						   ------------------------------------------------------------------
								CASE s_winkeltyp IS
									when '0'    =>
										usig1_mult_a_16 <= a_Mult21;
										usig1_mult_b_16 <= a_Mult23;

										usig2_mult_a_16 <= a_Mult1;
										usig2_mult_b_16 <= a_Mult2;

									when others =>
										usig1_mult_a_16 <= c_b2;
										usig1_mult_b_16 <= a_Mult21;

								END CASE;

								usig5_mult_a_16 	<= a_Mult21;
								usig5_mult_b_16 	<= a_Mult22;
								s_STATE_SINUS 		<= MULTIPLIKATION3BER;

						   ------------------------------------------------------------------
							when MULTIPLIKATION3BER =>  --9.Takt
						   ------------------------------------------------------------------
								usig1_Mult_32 		<= usig1_mult_a_16*usig1_mult_b_16;
								usig2_Mult_32 		<= usig2_mult_a_16*usig2_mult_b_16;
								usig5_Mult_32 		<= usig5_mult_a_16*usig5_mult_b_16;
								s_STATE_SINUS 		<= MULTIPLIKATION3SAV;

						  ------------------------------------------------------------------
							when MULTIPLIKATION3SAV =>  --10.Takt
						  ------------------------------------------------------------------
								s_Mult32 			<= usig1_Mult_32;
								s_Mult33 			<= usig2_Mult_32;
								s_Mult31 			<= usig5_Mult_32;
								s_STATE_SINUS 		<= ADDITION;

							------------------------------------------------------------------
							WHEN ADDITION => --5. beide Reihen aufaddieren
							------------------------------------------------------------------
								CASE s_winkeltyp IS
									WHEN '0' =>
										s_Summe <= resize(a_Mult24, 19) - resize(a_Mult33, 19) + resize(a_Mult32, 19) - resize(a_Mult31, 19);
									WHEN OTHERS =>
										s_Summe <= resize(c_pi2, 19) - resize(a_Mult24, 19)	 + resize(a_Mult32, 19) - resize(a_Mult31, 19);
								END CASE;
								s_STATE_SINUS <= AMPLITUDE_BERECHNEN;

							------------------------------------------------------------------
							WHEN AMPLITUDE_BERECHNEN => --6. Vorzeichen aufsetzen
							------------------------------------------------------------------
								s_Ampl 					<= s_Summe(15 DOWNTO 0) * s_amplitude;
								s_STATE_SINUS 			<= VORZEICHEN;

							------------------------------------------------------------------
							WHEN VORZEICHEN => --6. Vorzeichen aufsetzen
							------------------------------------------------------------------
								s_sinus_ergebniss 	<= signed(std_logic_vector(s_Ampl(31 DOWNTO 16)));
								s_STATE_SINUS			<= QUADRANTENAUFBAU;

							-----------------------------------WINKELBESTIMMEN-----------------------------------------
							WHEN QUADRANTENAUFBAU =>
							----------------------------------------------------------------------------
								CASE s_quadrant IS
									WHEN 0 => s_Sine 	<=   s_sinus_ergebniss;
									WHEN 1 => s_Sine 	<=   s_sinus_ergebniss;
									WHEN 2 => s_Sine 	<= - s_sinus_ergebniss;
									WHEN 3 => s_Sine 	<= - s_sinus_ergebniss;
								END CASE;
								s_sin_finish 			<= '1';
								s_STATE_SINUS  		<= QUADRANTAUSWAHL;

							---------------------------------------------------------------------------------
							WHEN OTHERS =>
							----------------------------------------------------------------------------------
								s_STATE_SINUS <= QUADRANTAUSWAHL;
						END CASE;
					END IF; --clk
				END IF; --reset
			END PROCESS sinus;


------------------------------------------------------------------------------------
p_hauptprozess: process(reset, clk, Enable) --Hauptprozess
------------------------------------------------------------------------------------

	CONSTANT c_120_18bit 	: unsigned (17 downto 0):= to_unsigned(87381,18);	--(2^18-1) *1/3  entspticht 120 deg
	CONSTANT c_240_18bit 	: unsigned (17 downto 0):= to_unsigned(174762,18);	--(2^18-1) *2/3  entspticht 240 deg
	CONSTANT c_pi2			 	: unsigned (17 downto 0):= to_unsigned(65536,18);	--(2^18-1) */2   entspticht 90 deg

begin
   if (reset = '1') then
      Sine_ph1			<=(others=>'0');
      Sine_ph2   	 	<=(others=>'0');
		Sine_ph3			<=(others=>'0');
		Sine_ph_a		<=(others=>'0');
		Sine_ph_b		<=(others=>'0');
		Sine_ph_c		<=(others=>'0');

		s_Sine_ph1	   <=(others=>'0');
      s_Sine_ph2   	<=(others=>'0');
		s_Sine_ph3	   <=(others=>'0');

		s_Sine_ph_a		<=(others=>'0');
		s_Sine_ph_b		<=(others=>'0');
		s_Sine_ph_c		<=(others=>'0');

		s_amplitude		<=(others=>'0');
		s_ph_shift1_2	<=(others=>'0');
		s_Theta     	<=(others=>'0');
		s_count			<= 1;
		s_start_sin		<= '0';

		Data_Out_Valid <= '0';   -- Ausgang noch nicht bereit
      s_STATE_HAUPT  <= EINGANG_SPEICHERN;

   elsif rising_edge(clk)  then
      case S_STATE_HAUPT is

   -----------------------------------------------------------------------------
   when EINGANG_SPEICHERN =>     --1. uebernimmt die Daten am Eingang
   -----------------------------------------------------------------------------
      Data_Out_Valid    <= '0';

      if (Enable = '1' ) then
			if theta_extern= '1' then
				s_Theta_in <= theta + c_pi2; -- + pi/2 damit startet als cos- Funktion
			elsE
				s_Theta_in <= s_sawTooth + c_pi2;
			end if;

			s_ph_shift1_2 <= ph_shift1_2;
			S_STATE_HAUPT <= BERECHNUNG_VORBEREITEN;
		else
			NULL;
      end if;

	-----------------------------------------------------------------------------
   when BERECHNUNG_VORBEREITEN =>   -- 4 Berechnung der Sinus- bzw. cos- Funktionen
	-----------------------------------------------------------------------------
		CASE s_count IS
			when 1 =>
				s_Theta  		<= s_Theta_in;  --	Vorbereiten
				s_amplitude 	<= amplitude_G1;--amplitude_G1;

			when 2 =>
				s_Theta  		<= s_Theta_in - c_120_18bit;
				s_amplitude 	<= amplitude_G1;--amplitude_G1;

			when 3 =>
				s_Theta  		<= s_Theta_in - c_240_18bit;
				s_amplitude 	<= amplitude_G1;--amplitude_G1;

			when 4 =>
				s_Theta  		<= s_Theta_in - s_ph_shift1_2;
				s_amplitude 	<= amplitude_G2;

			when 5 =>
				s_Theta  		<= s_Theta_in - c_120_18bit - s_ph_shift1_2;
				s_amplitude 	<= amplitude_G2;

			when 6 =>
				s_Theta  		<= s_Theta_in - c_240_18bit - s_ph_shift1_2;
				s_amplitude 	<= amplitude_G2;
		END CASE;

		s_start_sin    <= '1';
		s_STATE_HAUPT  <= BERECHNUNG_STARTEN;

   -----------------------------------------------------------------------------
   when BERECHNUNG_STARTEN =>  --2.
   -----------------------------------------------------------------------------
		s_start_sin      <= '0';

		if (s_sin_finish = '1') then
			CASE s_count IS
				when 1 =>
					s_Sine_ph1  	<= s_Sine;
					s_count	 	 	<= 2 ;
					S_STATE_HAUPT  <= BERECHNUNG_VORBEREITEN;

				when 2 =>
					s_Sine_ph2  	<= s_Sine;
					s_count	 	 	<= 3 ;
					S_STATE_HAUPT  <= BERECHNUNG_VORBEREITEN;

				when 3 =>
					s_Sine_ph3  	<= s_Sine;
					s_count	 	 	<= 4 ;
					S_STATE_HAUPT  <= BERECHNUNG_VORBEREITEN;

				when 4 =>
					s_Sine_ph_a  	<= s_Sine;
					s_count	 	 	<= 5;
					S_STATE_HAUPT  <= BERECHNUNG_VORBEREITEN;

				when 5 =>
					s_Sine_ph_b 	<= s_Sine;
					s_count	 	 	<= 6 ;
					S_STATE_HAUPT  <= BERECHNUNG_VORBEREITEN;

				when 6 =>
					s_Sine_ph_c  	<= s_Sine;
					s_count	 	 	<= 1 ;
					S_STATE_HAUPT  <= ENDERGEBNISS_AUSGEBEN;
			END CASE;

		else
			NULL;
		end if;

	---------------------------------------------------------------------------
   when ENDERGEBNISS_AUSGEBEN => --11
	---------------------------------------------------------------------------
		Sine_ph1 		<= s_Sine_ph1;
		Sine_ph2 		<= s_Sine_ph2;
		Sine_ph3 		<= s_Sine_ph3;

		Sine_ph_a 		<= s_Sine_ph_a;
		Sine_ph_b 		<= s_Sine_ph_b;
		Sine_ph_c 		<= s_Sine_ph_c;

		Data_Out_Valid <= '1';
      S_STATE_HAUPT  <= EINGANG_SPEICHERN;

	----------------------------------------------------------------------------
   when others =>          -- Absicherung fuer FPGA
	----------------------------------------------------------------------------
      s_STATE_HAUPT  <= EINGANG_SPEICHERN;

   end case;
   end if;
END process p_hauptprozess ;


------------------------------------------------------------------------------------
p_Winkelgenerator: PROCESS(reset, clk, Enable) --Hauptprozess
BEGIN
   IF(reset = '1') THEN -- Zurücksetzen des Zaehlers
		s_sawTooth 				<= to_unsigned(0,18);
		s_counter_winkel  	<= to_unsigned(0,8);

	ELSIF rising_edge(clk)  THEN
		s_counter_winkel 		<= s_counter_winkel + 1;   -- Incrementiere den Counter
		
      if theta_extern = '0' then --Interner Funktionsgenerator
         theta_out_corr		   <= s_sawTooth - g_theta_corr;
      else                       -- Externer Funktionsgenerator
         theta_out_corr		   <= s_Theta_in - s_ph_shift1_2; --somit wird es winkel fuer das zweite System ausgegeben
      end if;
         
      


		IF (s_counter_winkel >= g_ticksProInc - 1) THEN -- Wenn der Counter einen gewissen Wert erreicht hat, wird der Ausgang incrementiert
			s_sawTooth 			<= s_sawTooth + 1;
			s_counter_winkel  <= to_unsigned(0,8);  		-- Zurücksetzen des Counters
		END IF;

	END IF;
END process p_Winkelgenerator ;



END ARCHITECTURE behavioral;
