--  Filename: Betrag.vhd
--
--  Beschrebung:
--  Ausgang= |Eingang|
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Betrag is
  port( reset     : in  std_logic;
        Eingang   : in  signed (15 downto 0);
        Ausgang   : out signed (15 downto 0));
end Betrag;

--============================================================================
architecture behavioral of Betrag is
  
begin
   hauptprozess : process (reset, Eingang)
   begin
      if (reset = '1') then
         Ausgang        <= (others => '0');
      else
        If ABS(Eingang) > 32767 then Ausgang <= to_signed(32767,16);
        else                         Ausgang <= ABS(Eingang)       ; end if;
     
      end if;  --reset
   end process hauptprozess;

end architecture behavioral;
