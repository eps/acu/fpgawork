------------------------------------------------------------------------------------------------------------------------
-- File name: Generic_4lines_SPI_Driver.vhd                                                                           --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 03/04/2019                                                                                              --
--                                                                                                                    --
-- Comments : This module is a generic 4 lines SPI protocol driver. It is based on Generic_SPI_Driver module.         --
--            The difference is the number of lines involved for the protocol: in this module they are 4, in the      --
--            "original" one they are 3.                                                                              --
--            It generates a SCLK pulses sequence based on nrSCLK_pulses signal input. It is used a signal and not a  --
--            generic to set the number of SCLK output pulses because in this way it is possible to use the same SPI  --
--            driver instance for more than one device.                                                               --
--            Of course a wrap logic is necessary to distinguish between the chip select signals (CS_n) and the two   --
--            or more devices to drive have to be compatible from the SCLK frequency point of view.                   --
--            It works both in read and in write mode at the same time.                                               --
--            SDt_in signal is sampled on the SCLK falling edge.                                                      --
--                                                                                                                    --
-- History  : Start up version 03/04/2019                                                                             --
------------------------------------------------------------------------------------------------------------------------
--            * Extended gSCLK_High and gSCLK_Low to 8 bits                                                           --
--            * Added gCSn2SCLK_SetupTime generic(8 bits)                                                             --
------------------------------------------------------------------------------------------------------------------------
--        __    __    __    __    __    __    __    __      __    __    __    __    __    __	__    __    __    __  --
-- clk __|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |_.._|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|    --
--              ______                                                                    _____	                      --
-- NewVal _____|     |___________________________________..______________________________|     |_____________________ --
--                                                                                                                    --
-- Dt_In       X             12345							 X	       67890          --
--        __________________                                                        _________________                 --
-- SCS_n                   |____________________________.._________________________|     	     |_______________ --
--                                _____       _____         _____       _____            		    _____     --
-- SCLK                     _____|     |_____|     |____.._|     |_____|     |_____________________________|	 |___ --
--                                                                                       		    	      --
--                                                                                                                    --
-- SDt_out                       X   DBgL-1  X   DBgL-2 .. X    DB1    X    DB0          		   X   DBgL-1 --
--                                                                                                                    --
-- SDt_in                           X   DBgL-1  X   DBgL-2 .. X    DB1    X    DB0          		   X   DBgL-1 --
--                                                                                       		              --
-- Data2Read                           X   DBgL-1  X   DBgL-2 .. X    DB1    X    DB0          		   X   DBgL-1 --
--                                                                                        _____	                      --
-- NewDt_Received  ______________________________________..______________________________|     |_____________________ --
--                                                                                                                    --
-- Dt_Received      X             abcde							 X	       12345          --
--                                                                                       		              --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity Generic_4lines_SPI_Driver is
  Generic (
         gSCLK_High              : unsigned(7 downto 0);                                     -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0);                                     -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0);                                     -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
	 gDataWidth              : integer range 0 to 256                                    -- Data to send width. It has to be higher or equal to 
	                                                                                     -- nrSCLK_pulses signal value.
  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high
         NewDt_In                : in  std_logic;                                            -- One clock cycle pulse active high when there is a new Dt_In
	                                                                                     -- or when a read action has to start. 
         Dt_In                   : in std_logic_vector(gDataWidth-1 downto 0);               -- Data to send.

         nrSCLK_pulses           : in std_logic_vector (7 downto 0);                         -- Number of SCLK pulses for each NewVal received

         NewDt_Received          : out std_logic;                                            -- One clock cycle pulse active high when there is a new Dt_Received 
         Dt_Received             : out std_logic_vector(gDataWidth-1 downto 0);              -- Data to send.
	 
         SCLK                    : out std_logic;                                            -- SPI clock signal
         SCS_n                   : out std_logic;                                            -- Active low SPI Chip Select signal
         SDt_in                  : in  std_logic;                                            -- Serial SPI incoming data 
         SDt_out                 : out std_logic                                             -- Serial SPI outgoing data 
  );
end Entity Generic_4lines_SPI_Driver;

architecture beh of Generic_4lines_SPI_Driver is

--
-- Constants declaration

--
-- Signals declaration
signal Data2Write               : std_logic_vector(gDataWidth-1 downto 0);                   -- Data to write
signal Dt_In_s0                 : std_logic_vector(gDataWidth-1 downto 0);                   -- Dt_In sampled signal
signal NewData_s0               : std_logic;                                                 -- NewData sampled signal
signal NewData_s1               : std_logic;                                                 -- NewData_s0 sampled signal
signal NewData_RE               : std_logic;                                                 -- NewData rising edge signal
signal NewData_RE_s0            : std_logic;                                                 -- NewData_RE sampled signal
signal SDt_in_s0                : std_logic;                                                 -- SDt_in sampled signal

signal Data2Read                : std_logic_vector(gDataWidth-1 downto 0);                   -- Read Data from the driven device

signal cntDt                    : unsigned(7 downto 0);                                      -- shift up to gDataWidth-1 bits counter
signal TCcntDt                  : unsigned(7 downto 0);                                      -- cntDt terminal counter. It is related to nrSCLK_pulses value.

signal cntSCLK_HalfPeriod       : unsigned(7 downto 0);                                      -- this is the counter used to manage the SCLK half period length.
signal SampleDtFromSlave        : std_logic;                                                 -- It is the pulse used to sample SDt_in on SCLK falling edge.

type fsmState is (WaitForSt, SetSyncn, SetSCLK, UnSetSCLK, UnSetSyncn);
signal fsm_state                : fsmState;

begin

p_dtSamp:process(Clock,Reset)
begin
  if (Reset='1') then

    NewData_s0     <= '0';
    NewData_s1     <= '0';
    NewData_RE     <= '0';
    NewData_RE_s0  <= '0';
    
    TCcntDt        <= (others=>'0');
    Dt_In_s0       <= (others=>'0');
    SDt_in_s0      <= '0';
  elsif (Clock'event and Clock='1') then

    NewData_s0     <= NewDt_In;
    NewData_s1     <= NewData_s0;
    NewData_RE     <= NewData_s0 and not NewData_s1;
    NewData_RE_s0  <= NewData_RE;

    if (NewData_RE='1') then
      --Data2Write   <= Dt_In;
      -- It is like that to compensate the first shift action
      Dt_In_s0     <= Dt_In(0) & Dt_In(gDataWidth-1 downto 1);
      TCcntDt      <= unsigned(nrSCLK_pulses);
    end if;

    SDt_in_s0      <= SDt_in;
    
  end if;
end process p_dtSamp;


p_FSM:process(Clock,Reset)
begin
  if (Reset='1') then
    -- State evolution
    fsm_state           <= WaitForSt;

    -- Signals evolution
    cntDt               <= (others=>'0');
    Data2Write          <= (others=>'0');
    SCS_n               <= '1';
    SCLK                <= '0';
    --Data2Read           <= (others=>'0');
    NewDt_Received      <= '0';
    Dt_Received         <= (others=>'0');
    cntSCLK_HalfPeriod  <= (others=>'0');
    
    SampleDtFromSlave   <= '0';    
  elsif(Clock'event and Clock='1') then
    
    case fsm_state is
      when WaitForSt =>

        -- State evolution
        if (NewData_RE_s0='1') then
          fsm_state  <= SetSyncn;
        end if;

        -- Signals evolution
        cntDt   <= (others=>'0');

        SCLK               <= '0';
        SCS_n              <= '1';
	NewDt_Received     <= '0';
      when SetSyncn  =>

        -- State evolution
        if (cntSCLK_HalfPeriod < gCSn2SCLK_SetupTime-1) then
	  cntSCLK_HalfPeriod <= cntSCLK_HalfPeriod + 1;
	else
	  cntSCLK_HalfPeriod <= (others => '0');
          fsm_state   <= SetSCLK;
	end if;

        --fsm_state   <= SetSCLK;

        -- Signals evolution
        SCS_n       <= '0';
        Data2Write  <= Dt_In_s0;
      when SetSCLK   =>

        -- State evolution
	if (cntSCLK_HalfPeriod < gSCLK_High-1) then
	  cntSCLK_HalfPeriod <= cntSCLK_HalfPeriod + 1;
	else
	  cntSCLK_HalfPeriod <= (others => '0');
          fsm_state   <= UnSetSCLK;
	end if;
	  

        -- Signals evolution
        SCLK               <= '1';
        SampleDtFromSlave  <= '0';
	
	if (cntSCLK_HalfPeriod < 1) then
          Data2Write  <= Data2Write(gDataWidth-2 downto 0) & Data2Write(gDataWidth-1);
	end if;
	
      when UnSetSCLK =>

        -- State evolution
	if (cntSCLK_HalfPeriod < gSCLK_Low-1) then
          cntSCLK_HalfPeriod <= cntSCLK_HalfPeriod + 1;
	else
          cntSCLK_HalfPeriod <= (others => '0');
	  
--          if (cntDt < TCcntDt-1) then
          if (cntDt < TCcntDt) then
            fsm_state  <= SetSCLK;
            cntDt      <= cntDt + 1;
          else
            fsm_state  <= UnSetSyncn;
            cntDt  <= (others=>'0');
          end if;
	end if;

        -- Signals evolution
        SCLK          <= '0';
	if (cntSCLK_HalfPeriod < 1) then
--	  Data2Read(0)  <= SDt_in_s0;
--	  Data2Read(gDataWidth-1 downto 1)  <= Data2Read(gDataWidth-2 downto 0);
          SampleDtFromSlave  <= '1';
	else
          SampleDtFromSlave  <= '0';
	end if;
	
      when UnSetSyncn  =>

        -- State evolution
        fsm_state  <= WaitForSt;
	
        -- Signals evolution
        SCS_n              <= '1';
        SampleDtFromSlave  <= '0';
	--
	-- It can be used also to report the data transfer end in Wr mode 
	NewDt_Received     <= '1';
	
	Dt_Received        <= Data2Read;
	
      when others =>

        -- State evolution
        fsm_state  <= WaitForSt;

        -- Signals evolution
        cntDt               <= (others=>'0');
        Data2Write          <= (others=>'0');
        SCS_n               <= '1';
        SCLK                <= '0';
--        Data2Read           <= (others=>'0');
        NewDt_Received      <= '0';
        Dt_Received         <= (others=>'0');
        cntSCLK_HalfPeriod  <= (others=>'0');
        SampleDtFromSlave   <= '0';

    end case;

  end if;

end process p_FSM;

-- data from slave
p_dtFromSlave: process(Clock,Reset)
begin
  if (Reset='1') then
    Data2Read           <= (others=>'0');    
  elsif(Clock'event and Clock='1') then
    if (SampleDtFromSlave = '1') then  
      Data2Read(0)  <= SDt_in_s0;
      Data2Read(gDataWidth-1 downto 1)  <= Data2Read(gDataWidth-2 downto 0);
    end if;
  end if;
end process;
   
--
-- Output
SDt_out  <= Data2Write(gDataWidth-1);

end beh;
