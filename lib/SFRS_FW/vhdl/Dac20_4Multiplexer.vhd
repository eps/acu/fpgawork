--  Filename: Dac20_Multiplexer.vhd
--  Date    : 14.09.2022 born
--  Author  : A. Wiest
--  Version : 1.0

-- Description:
-- This Bloack is used to select the suitable input for the DAC as a input. Its 2 * 1 multiplexer.
-- Mostly used for the Altera Quartus II implimentation purpose.

--============================================================================ 
LIBRARY ieee;                                          --Library Declaration
USE  ieee.std_logic_1164.all; 
USE  ieee.numeric_std.all;
------------------------------------------------------------------------------

entity Dac20_4Multiplexer is
   port ( 
		clk, reset  : in std_logic;
		COMMAND     : in std_logic_vector (1 downto 0);
		
		MUX1_IN     : in  signed(19 downto 0);   
		MUX2_IN     : in  signed(19 downto 0); 
      MUX3_IN     : in  signed(19 downto 0); 
      MUX4_IN     : in  signed(15 downto 0); 
      
		MUX_OUT     : out signed(19 downto 0));	
end Dac20_4Multiplexer;


architecture RTL of Dac20_4Multiplexer is

BEGIN

process_DAC20_4MUX :  process (clk, reset)
BEGIN

   if (reset ='1') then
	   MUX_OUT   <= (others=>'0');

   elsif (rising_edge(clk) and clk = '1') then
	   case COMMAND(1 downto 0) is
		   when "00"   => 	MUX_OUT	<= MUX1_IN;      --Input 1 selected         
         when "01"   => 	MUX_OUT	<= MUX2_IN;      --Input 2 selected 
         when "10"   => 	MUX_OUT	<= MUX3_IN;      --Input 3 selected 
         when "11"   => 	
            if MUX4_IN(15) ='0' then MUX_OUT	<=         MUX4_IN  & "0000";        --pos
            else                     MUX_OUT	<= NOT(ABS(MUX4_IN) & "0000") + 1 ;  --neg 
            end if; 
	   end case;	
   end if;
   
end process  process_DAC20_4MUX ;
	
end RTL;







