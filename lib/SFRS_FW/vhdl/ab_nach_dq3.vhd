--  Filename: ab_nach_dq33.vhd, A.Wiest geaendert am 05.2020
-- Gesamte Berechnung dauert 1,35 us  /3 = 0,45us pro Durchlauf
-- Fuer 4 Durchlaufe 1,8us
--  Beschrebetaung:
--      Transformation des Koordinatensystems alpha_betha -> dq
--      d= alpha*cos(theta)+ beta*sin(theta)
--      q= -alpha*sin(theta)+ beta*cos(theta)
--      Transformation des Koordinatensystems dq -> alpha_betha
--      alpha= d*cos(theta) - q*sin(theta)
--      beta = d*sin(theta)   q*cos(theta)
--      insgesamt 4 Systeme werden umgewandelt

--  Eing. Groessen:
--      theta1,theta2,theta3     0..360 18bit unsigned
--      alpha1,alpha2,alpha3     alpha Kompnente 16bit signed
--      beta1,beta2,beta3        beta  Kompnente 16bit signed
--      Enable                   Gueltigkeit der Eingangsdaten
--      reset                    Berechnung neue starten
--
--  Ausg. Groessen:
--      d1,d2,d3               Komponente der dq System, 16bit signed
--      q1,q2,q3               Komponente der dq System, 16bit signed
--      Data_Out_Valid_dq     '1' wenn die Daten berechnet sind
--------------------------------------------------------------------------------


LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY ab_nach_dq3 IS
   GENERIC(
      takt         : integer  := 100_000_000  ); --Initalphalisierung 100 MHz

   PORT(
      --System-Eingaenge:
      clk, reset,
      Enable                  : in std_logic ;

      --Eingaenge:
      alpha1, beta1           : in signed       (15 downto 0);
      theta1                  : in unsigned     (17 downto 0); --Genauigkeit in jedem Quadrant 16 Bit

      alpha2, beta2           : in signed       (15 downto 0);
      theta2                  : in unsigned     (17 downto 0); --Genauigkeit in jedem Quadrant 16 Bit

      alpha3, beta3           : in signed       (15 downto 0);
      theta3                  : in unsigned     (17 downto 0); --Genauigkeit in jedem Quadrant 16 Bit
      
      d4, q4                  : in signed       (15 downto 0);
      theta4                  : in unsigned     (17 downto 0); --Genauigkeit in jedem Quadrant 16 Bit
        
      
      --Ausgaenge (nur positive Werte)
      d1,q1,d2, q2,d3,q3      : out signed      (15 downto 0):=(others=>'0');
      alpha4,beta4            : out signed      (15 downto 0):=(others=>'0');
      Data_Out_Valid_dq       : out std_logic:= '0');
END ab_nach_dq3 ;

--============================================================================
ARCHITECTURE behavioral OF ab_nach_dq3 IS
--attrbetaute multstyle : string;
--attrbetaute multstyle of behavioral : architecture is "logic"; -- > 9bit multiplier werden nicht genutzt, sondern nur logic
--Hauptprozess
   --Gespeicherte Eingaenge-----------------------------------
  SIGNAL s_alpha      : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_beta       : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_theta      : unsigned (18 downto 0):=(others=>'0');

  SIGNAL s_alpha1     : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_beta1      : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_theta1     : unsigned (17 downto 0):=(others=>'0');
  SIGNAL s_alpha2     : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_beta2      : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_theta2     : unsigned (17 downto 0):=(others=>'0');
  SIGNAL s_alpha3     : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_beta3      : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_theta3     : unsigned (17 downto 0):=(others=>'0');
  
  SIGNAL s_d4         : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_q4         : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_theta4     : unsigned (17 downto 0):=(others=>'0');
  
  SIGNAL s_durchlauf  : unsigned (1  downto 0):=(others=>'0');
  
  --gespeicherte Ausgaenge
  SIGNAL s_d1         : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_q1         : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_d2         : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_q2         : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_d3         : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_q3         : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_alpha4     : signed   (15 downto 0):=(others=>'0');
  SIGNAL s_beta4      : signed   (15 downto 0):=(others=>'0');
  

  --Fuer Multiplikation--------------------------------------------
  SIGNAL s_Multa_33   : signed (32 downto 0):=(others=>'0');
  SIGNAL s_Multb_33   : signed (32 downto 0):=(others=>'0');
  SIGNAL sa_Mult1h    : signed (15 downto 0):=(others=>'0');
  SIGNAL sa_Mult2h    : signed (15 downto 0):=(others=>'0');
  SIGNAL sa_Mult3h    : signed (15 downto 0):=(others=>'0');
  SIGNAL sa_Mult4h    : signed (15 downto 0):=(others=>'0');
  SIGNAL s_Summe_d    : signed (15 downto 0):=(others=>'0');
  SIGNAL s_summe_q    : signed (15 downto 0):=(others=>'0');

   --Winkelgroessen-------------------------------------------
   SIGNAL s_theta_strich_sin       :  unsigned (18 downto 0):=(others=>'0');   --0..90
   SIGNAL s_theta_strich_cos       :  unsigned (18 downto 0):=(others=>'0');   --0..90
   SIGNAL s_sinus_theta            :  unsigned (15 downto 0):=(others=>'0');
   SIGNAL s_theta_strich_sin_pos   :  std_logic := '1';                      --Vorzeichen
   SIGNAL s_theta_strich_cos_pos   :  std_logic := '1';
   SIGNAL s_sinus_vorzeichen       :  std_logic := '1';

   --Startkomando fuer die Sinusberechnung-------------------------------------
   SIGNAL s_Startsinus : std_logic := '0';

  TYPE T_STATE IS(
                     EINGANG_SPEICHERN,     --State Maschine
                     EINGANG_AUSWAEHLEN,
                     WINKELBESTIMMEN,
                     SINUSBERECHNUNG_VORBEREITEN,
                     SINUSBERECHNEN,
                     HAUPTMULTIPLIKATION1VOR,
                     HAUPTMULTIPLIKATION1BER,
                     HAUPTMULTIPLIKATION1SAV,
                     HAUPTMULTIPLIKATION2VOR,
                     HAUPTMULTIPLIKATION2BER,
                     HAUPTMULTIPLIKATION2SAV,
                     COSINUSBERECHNUNG_VORBEREITEN ,
                     COSINUSBERECHNEN,
                     HAUPTMULTIPLIKATION3VOR,
                     HAUPTMULTIPLIKATION3BER,
                     HAUPTMULTIPLIKATION3SAV,
                     HAUPTMULTIPLIKATION4VOR,
                     HAUPTMULTIPLIKATION4BER,
                     HAUPTMULTIPLIKATION4SAV,
                     SUMME_BERECHNEN,
                     ENDERGEBNISS_BERECHNEN,
                     DATEN_RAUSGEBEN);

   SIGNAL s_STATE_HAUPT: T_STATE := EINGANG_SPEICHERN;

--Sinusergebnis-------------------------------------------
   --Fuer Multiplikation
  SIGNAL s_Mult1  :   unsigned (31 downto 0):=(others=>'0');
  SIGNAL s_Mult2  :   unsigned (31 downto 0):=(others=>'0');
  SIGNAL s_Mult3  :   unsigned (31 downto 0):=(others=>'0');
  SIGNAL s_Mult21 :   unsigned (31 downto 0):=(others=>'0');
  SIGNAL s_Mult22 :   unsigned (31 downto 0):=(others=>'0');
  SIGNAL s_Mult23 :   unsigned (31 downto 0):=(others=>'0');
  SIGNAL s_Mult24 :   unsigned (32 downto 0):=(others=>'0');
  SIGNAL s_Mult31 :   unsigned (31 downto 0):=(others=>'0');
  SIGNAL s_Mult32 :   unsigned (31 downto 0):=(others=>'0');
  SIGNAL s_Mult33 :   unsigned (31 downto 0):=(others=>'0');

  --Winkel----------
  SIGNAL s_winkel               : unsigned (15 downto 0):=(others=>'0');
  SIGNAL s_winkeltyp            : std_logic             :='0';
  SIGNAL s_theta_strich_pos     : std_logic             :='0';
  SIGNAL s_sin_finish           : std_logic             :='0';

   --Endergebnis-----
  SIGNAL s_Summe                : unsigned (18 downto 0):=(others=>'0');
  SIGNAL s_sinus_ergebniss      : signed   (16 downto 0):=(others=>'0');

   --Zustaende---------
   TYPE T_STATE_SINUS  IS(
                           WINKELAUSWAHL,
                           MULTIPLIKATION1VOR,
                           MULTIPLIKATION1BER,
                           MULTIPLIKATION1SAV,
                           MULTIPLIKATION2VOR,
                           MULTIPLIKATION2BER,
                           MULTIPLIKATION2SAV,
                           MULTIPLIKATION3VOR,
                           MULTIPLIKATION3BER,
                           MULTIPLIKATION3SAV,
                           ADDITION,
                           VORZEICHEN);
   SIGNAL s_STATE_SINUS:   T_STATE_SINUS := WINKELAUSWAHL;

   --Reduzieren der Multiplier--------------
   --Signale
   SIGNAL usig1_mult_a_16   : unsigned (15 downto 0):=(others=>'0');
   SIGNAL usig1_mult_b_16   : unsigned (15 downto 0):=(others=>'0');

   SIGNAL usig2_mult_a_16   : unsigned (15 downto 0):=(others=>'0');
   SIGNAL usig2_mult_b_16   : unsigned (15 downto 0):=(others=>'0');

   SIGNAL usig3_mult_a_17   : unsigned (16 DOWNTO 0):=(others=>'0');
   SIGNAL usig3_mult_b_16   : unsigned (15 DOWNTO 0):=(others=>'0');

   SIGNAL sig4_mult_a_17    : signed   (16 DOWNTO 0):=(others=>'0');
   SIGNAL sig4_mult_b_16    : signed   (15 DOWNTO 0):=(others=>'0');

   SIGNAL usig5_mult_a_16   : unsigned (15 downto 0):=(others=>'0');
   SIGNAL usig5_mult_b_16   : unsigned (15 downto 0):=(others=>'0');


   SIGNAL usig1_Mult_32     : unsigned (31 downto 0):=(others=>'0');
   SIGNAL usig2_Mult_32     : unsigned (31 downto 0):=(others=>'0');
   SIGNAL usig3_Mult_33     : unsigned (32 downto 0):=(others=>'0');
   SIGNAL sig4_Mult_33      : signed   (32 downto 0):=(others=>'0');
   SIGNAL usig5_Mult_32     : unsigned (31 downto 0):=(others=>'0');

BEGIN --===========================================================================

sinusa: process (s_theta_strich_pos, s_Startsinus, clk, reset) --berechnte sinus (0..90) Eingangsvaralphable 16bit unsigned
  --Konstanten-----------------------------------------------
  CONSTANT c_pi2   : unsigned (15 downto 0):= to_unsigned(65535,16);    --pi/2 = 90 ist voller Aussteuerbereich
  CONSTANT c_pi51  : unsigned (15 downto 0):= to_unsigned(37515,16);    --51,52 = (2^16-1) * 51,52/90

  CONSTANT c_a1    : unsigned (16 downto 0):= to_unsigned(102944,17);   --2^17-1 *1,570796/2 ~>2^16-1 * 1.570796
  CONSTANT c_a2    : unsigned (15 downto 0):= to_unsigned(42334,16);    --2^16-1 *0,645964
  CONSTANT c_a3    : unsigned (15 downto 0):= to_unsigned(5223,16);     --2^16-1 *0,079693
  CONSTANT c_a4    : unsigned (15 downto 0):= to_unsigned(307,16);      --2^16-1 *4,681754*10^-3

  CONSTANT c_b1    : unsigned (16 downto 0):= to_unsigned(80852,17);    --2^17-1 *1,233701/2 = 80852 ~>2^16-1 * 1.233701
  CONSTANT c_b2    : unsigned (15 downto 0):= to_unsigned(16624,16);    --2^16-1 *0,25367
  CONSTANT c_b3    : unsigned (15 downto 0):= to_unsigned(1367,16);     --2^16-1 *0,020863

  ALIAS    a_Mult1 :   unsigned (15 downto 0) IS s_Mult1 (31 downto 16);
  ALIAS    a_Mult2 :   unsigned (15 downto 0) IS s_Mult2 (31 downto 16);
  ALIAS    a_Mult3 :   unsigned (15 downto 0) IS s_Mult3 (31 downto 16);
  ALIAS    a_Mult21:   unsigned (15 downto 0) IS s_Mult21(31 downto 16);
  ALIAS    a_Mult22:   unsigned (15 downto 0) IS s_Mult22(31 downto 16);
  ALIAS    a_Mult23:   unsigned (15 downto 0) IS s_Mult23(31 downto 16);
  ALIAS    a_Mult24:   unsigned (15 downto 0) IS s_Mult24(31 downto 16);
  ALIAS    a_Mult31:   unsigned (15 downto 0) IS s_Mult31(31 downto 16);
  ALIAS    a_Mult32:   unsigned (15 downto 0) IS s_Mult32(31 downto 16);
  ALIAS    a_Mult33:   unsigned (15 downto 0) IS s_Mult33(31 downto 16);

 BEGIN
      if (reset = '1') then
         s_Mult1          <=(others=>'0');
         s_Mult2          <=(others=>'0');
         s_Mult3          <=(others=>'0');
         s_Mult21         <=(others=>'0');
         s_Mult22         <=(others=>'0');
         s_Mult23         <=(others=>'0');
         s_Mult24         <=(others=>'0');
         s_Mult31         <=(others=>'0');
         s_Mult32         <=(others=>'0');
         s_Mult33         <=(others=>'0');

         s_winkel         <=(others=>'0');
         s_winkeltyp      <='0';
         s_Summe          <=(others=>'0');

         s_STATE_SINUS  <= WINKELAUSWAHL;

      else
         if rising_edge(clk)   THEN
            CASE s_STATE_SINUS IS

   ------------------------------------------------------------------
   when WINKELAUSWAHL =>   --1.Takt
   ------------------------------------------------------------------
      s_sin_finish      <= '0';

      if  (s_Startsinus ='1') then
         If (s_sinus_theta < c_pi51)   then  --51,52 Schnittpunkt, -> kleinsten Fehler
            s_winkel    <= s_sinus_theta;
            s_winkeltyp <= '0';
         else
            s_winkel    <= c_pi2 - s_sinus_theta;
            s_winkeltyp <= '1';
         end If;

         s_STATE_SINUS   <= MULTIPLIKATION1VOR;
      end if;

   ------------------------------------------------------------------
   when MULTIPLIKATION1VOR =>  --2.Takt
   ------------------------------------------------------------------
      usig1_mult_a_16 <= s_winkel;
      usig1_mult_b_16 <= s_winkel;

      usig2_mult_a_16 <= c_a2;
      usig2_mult_b_16 <= s_winkel;

      usig5_mult_a_16 <= c_a4;
      usig5_mult_b_16 <= s_winkel;

      s_STATE_SINUS   <= MULTIPLIKATION1BER;

  ------------------------------------------------------------------
   when MULTIPLIKATION1BER =>  --3.Takt
  ------------------------------------------------------------------
      usig1_Mult_32  <= usig1_mult_a_16*usig1_mult_b_16;
      usig2_Mult_32  <= usig2_mult_a_16*usig2_mult_b_16;
      usig5_Mult_32  <= usig5_mult_a_16*usig5_mult_b_16;
      s_STATE_SINUS  <= MULTIPLIKATION1SAV;

  ------------------------------------------------------------------
   when MULTIPLIKATION1SAV =>  --4.Takt
  ------------------------------------------------------------------
      s_Mult1        <= usig1_Mult_32;
      s_Mult2        <= usig2_Mult_32;
      s_Mult3        <= usig5_Mult_32;
      s_STATE_SINUS  <= MULTIPLIKATION2VOR;

  ------------------------------------------------------------------
   when MULTIPLIKATION2VOR =>  --5.Takt
  ------------------------------------------------------------------
      CASE s_winkeltyp IS
         when '0' =>
            usig1_mult_a_16 <= a_Mult1;
            usig1_mult_b_16 <= a_Mult3;
            usig2_mult_a_16 <= c_a3;
            usig2_mult_b_16 <= s_winkel;
            usig3_mult_a_17 <= c_a1;
            usig3_mult_b_16 <= s_winkel;

         when others =>
            usig1_mult_a_16 <= c_b3;
            usig1_mult_b_16 <= a_Mult1;
            usig3_mult_a_17 <= c_b1;
            usig3_mult_b_16 <= a_Mult1;
      END CASE;

      usig5_mult_a_16     <= a_Mult1;
      usig5_mult_b_16     <= a_Mult1;
      s_STATE_SINUS       <= MULTIPLIKATION2BER;

   ------------------------------------------------------------------
   when MULTIPLIKATION2BER =>  --6.Takt
   ------------------------------------------------------------------
      usig1_Mult_32  <= usig1_mult_a_16*usig1_mult_b_16;
      usig2_Mult_32  <= usig2_mult_a_16*usig2_mult_b_16; --Value only vald if s_winkeltyp='0'
      usig3_Mult_33  <= usig3_mult_a_17*usig3_mult_b_16;
      usig5_Mult_32  <= usig5_mult_a_16*usig5_mult_b_16;
      s_STATE_SINUS  <= MULTIPLIKATION2SAV;

   ------------------------------------------------------------------
   when MULTIPLIKATION2SAV =>  --7.Takt
   ------------------------------------------------------------------
      s_Mult22       <= usig1_Mult_32;
      s_Mult23       <= usig2_Mult_32;
      s_Mult24       <= usig3_Mult_33;
      s_Mult21       <= usig5_Mult_32;
      s_STATE_SINUS  <= MULTIPLIKATION3VOR;

   ------------------------------------------------------------------
   when MULTIPLIKATION3VOR =>  --8.Takt
   ------------------------------------------------------------------
      CASE s_winkeltyp IS
         when '0'    =>
            usig1_mult_a_16 <= a_Mult21;
            usig1_mult_b_16 <= a_Mult23;
            usig2_mult_a_16 <= a_Mult1;
            usig2_mult_b_16 <= a_Mult2;
         when others =>
            usig1_mult_a_16 <= c_b2;
            usig1_mult_b_16 <= a_Mult21;
      END CASE;

      usig5_mult_a_16     <= a_Mult21;
      usig5_mult_b_16     <= a_Mult22;
      s_STATE_SINUS       <= MULTIPLIKATION3BER;

  ------------------------------------------------------------------
   when MULTIPLIKATION3BER =>  --9.Takt
  ------------------------------------------------------------------
      usig1_Mult_32  <= usig1_mult_a_16*usig1_mult_b_16;
      usig2_Mult_32  <= usig2_mult_a_16*usig2_mult_b_16;
      usig5_Mult_32  <= usig5_mult_a_16*usig5_mult_b_16;
      s_STATE_SINUS  <= MULTIPLIKATION3SAV;

  ------------------------------------------------------------------
   when MULTIPLIKATION3SAV =>  --10.Takt
  ------------------------------------------------------------------
      s_Mult32       <= usig1_Mult_32;
      s_Mult33       <= usig2_Mult_32;
      s_Mult31       <= usig5_Mult_32;
      s_STATE_SINUS  <= ADDITION;

   ------------------------------------------------------------------
   when ADDITION =>    --11. Takt, bede Reihen aufaddieren
   ------------------------------------------------------------------
      CASE s_winkeltyp IS
         when '0'    =>
            s_Summe <= resize(a_Mult24,19) - resize(a_Mult33,19)
                     + resize(a_Mult32,19) - resize(a_Mult31,19);
         when others =>
            s_Summe <= resize(c_pi2,19)    - resize(a_Mult24,19)
                     + resize(a_Mult32,19) - resize(a_Mult31,19);
      END CASE;
      s_STATE_SINUS    <= VORZEICHEN;

   ------------------------------------------------------------------
   when VORZEICHEN =>    --12. Takt,  Vorzeichen aufsetzen
   ------------------------------------------------------------------
      if s_Summe > to_unsigned(65535,16) then 
         s_Summe <= to_unsigned(65535,19);
      else
         if (s_sinus_vorzeichen = '1') then
            s_sinus_ergebniss    <= signed(std_logic_vector('0' & s_Summe(15 downto 0)));
         else
            s_sinus_ergebniss    <= signed(NOT(std_logic_vector('0' & s_Summe(15 downto 0)))) + 1;
         end if;
      end if;

      s_sin_finish   <= '1';
      s_STATE_SINUS  <= WINKELAUSWAHL;

   ----------------------------------------------------------------------------
   when others => --Wird theoretisch nie erreicht
   ----------------------------------------------------------------------------
      s_STATE_SINUS   <= WINKELAUSWAHL;

   END CASE;
   END IF;  --clk
   END IF;  --reset
END process sinusa;


------------------------------------------------------------------------------------
p_hauptprozess: process(reset, clk, Enable) --Hauptprozess
------------------------------------------------------------------------------------
  CONSTANT c_90_18bit    : unsigned (17 downto 0):= to_unsigned(65535,18);   --(2^18-1) *1/4 = 65535 entspticht pi/2
  CONSTANT c_180_18bit   : unsigned (17 downto 0):= to_unsigned(131071,18);   --(2^18-1) *1/2 = 131071.5 entspticht pi
  CONSTANT c_270_18bit   : unsigned (17 downto 0):= to_unsigned(196607,18);   --(2^18-1) *3/4 = 131071.5 entspticht 2pi * 3/4

begin
   if (reset = '1') then
      s_alpha1                <=(others=>'0'); --Eingaenge
      s_beta1                 <=(others=>'0');
      s_theta1                <=(others=>'0');
      
      s_alpha2                <=(others=>'0');
      s_beta2                 <=(others=>'0');
      s_theta2                <=(others=>'0');
     
      s_alpha3                <=(others=>'0');
      s_beta3                 <=(others=>'0');
      s_theta3                <=(others=>'0');
      
      s_d4                    <=(others=>'0');
      s_q4                    <=(others=>'0');
      s_theta4                <=(others=>'0');
      
      s_d1                    <=(others=>'0'); --Ausgaenge
      s_q1                    <=(others=>'0'); 
      s_d2                    <=(others=>'0'); 
      s_q2                    <=(others=>'0'); 
      s_d3                    <=(others=>'0'); 
      s_q3                    <=(others=>'0'); 
      s_alpha4                <=(others=>'0'); 
      s_beta4                 <=(others=>'0'); 

      s_durchlauf             <=(others=>'0');

      sa_Mult1h               <=(others=>'0');
      sa_Mult2h               <=(others=>'0');
      sa_Mult3h               <=(others=>'0');
      sa_Mult4h               <=(others=>'0');

      s_theta_strich_sin      <=(others=>'0');     --0..90
      s_theta_strich_cos      <=(others=>'0');     --0..90
      s_sinus_theta           <=(others=>'0');     --0..90
      s_theta_strich_sin_pos  <= '1';              --Vorzeichen
      s_theta_strich_cos_pos  <= '1';
      s_sinus_vorzeichen      <= '1';

      s_Summe_d             <=(others=>'0');--0..90
      s_Summe_q             <=(others=>'0');--0..90
      d1                    <=(others=>'0');-- d-Ausgang
      d2                    <=(others=>'0');-- d-Ausgang
      d3                    <=(others=>'0');-- d-Ausgang
      q1                    <=(others=>'0');-- q-Ausgang
      q2                    <=(others=>'0');-- q-Ausgang
      q3                    <=(others=>'0');-- q-Ausgang
      
      alpha4                <=(others=>'0');-- alpha-Ausgang  
      beta4                 <=(others=>'0');-- beta-Ausgang  

      s_Startsinus          <= '0';
      Data_Out_Valid_dq     <= '0';   -- Ausgang noch nicht bereit
      s_STATE_HAUPT         <= EINGANG_SPEICHERN;

   elsif rising_edge(clk)  then
      case S_STATE_HAUPT is

   -----------------------------------------------------------------------------
   when EINGANG_SPEICHERN =>  --1. uebernimmt die Daten am Eingang
   -----------------------------------------------------------------------------
      Data_Out_Valid_dq     <= '0';

      if (Enable = '1' ) then
         s_alpha1      <= alpha1;   -- alpha Komponente
         s_beta1       <= beta1;    -- beta  Komponente
         s_theta1      <= theta1;   -- 0..360
         s_alpha2      <= alpha2;   -- alpha Komponente
         s_beta2       <= beta2;    -- beta  Komponente
         s_theta2      <= theta2;   -- 0..360
         s_alpha3      <= alpha3;   -- alpha Komponente
         s_beta3       <= beta3;    -- beta  Komponente
         s_theta3      <= theta3;   -- 0..360
         
         s_d4          <= d4;       -- d Komponente als Eingang
         s_q4          <= q4;       -- q Komponente als Eingang
         s_theta4      <= theta4;   -- 0..360

         S_STATE_HAUPT <= EINGANG_AUSWAEHLEN;
      end if;

   -----------------------------------------------------------------------------
   when EINGANG_AUSWAEHLEN =>  --1. uebernimmt die Daten am Eingang
   -----------------------------------------------------------------------------
      if    s_durchlauf = "00" then
         s_alpha  <= s_alpha1;       -- alpha Komponente
         s_beta   <= s_beta1;        -- beta  Komponente
         s_theta  <= '0' & s_theta1; -- 0..360
      elsif s_durchlauf = "01" then
         s_alpha  <= s_alpha2;       -- alpha Komponente
         s_beta   <= s_beta2;        -- beta  Komponente
         s_theta  <= '0' & s_theta2; -- 0..360
      elsif s_durchlauf = "10" then
         s_alpha  <= s_alpha3;       -- alpha Komponente
         s_beta   <= s_beta3;        -- beta  Komponente
         s_theta  <= '0' & s_theta3; -- 0..360
      else
         s_alpha  <= s_d4;           -- d Komponente
         s_beta   <= s_q4;           -- q Komponente
         s_theta  <= '0' & s_theta4; -- 0..360
      end if;

      S_STATE_HAUPT<= WINKELBESTIMMEN;

  -----------------------------------------------------------------------------
  when WINKELBESTIMMEN =>  --2. Bestimmen des Quadranten
  -----------------------------------------------------------------------------
      if (s_theta <= C_90_18bit)    THEN --0..90
         s_theta_strich_sin         <= s_theta;
         s_theta_strich_cos         <= C_90_18bit - s_theta;     -- cos(x) = sin (90 - x) fuer den Bereich 0.90
         s_theta_strich_sin_pos     <= '1';                      --Vorzeichen fuer Sinusfunktion   positiv
         s_theta_strich_cos_pos     <= '1';                      --Vorzeichen fuer Cosinusfunktion positiv

      elsif (s_theta <= C_180_18bit) THEN --90 ..180
         s_theta_strich_sin         <= C_90_18bit -(s_theta - C_90_18bit-1);
         s_theta_strich_cos         <= s_theta - C_90_18bit - 1;
         s_theta_strich_sin_pos     <= '1';                      --Vorzeichen fuer Sinusfunktion   positiv
         s_theta_strich_cos_pos     <= '0';                      --Vorzeichen fuer Cosinusfunktion negativ

      elsif (s_theta <= C_270_18bit) THEN --180 ..270
         s_theta_strich_sin         <= s_theta - C_180_18bit - 1;
         s_theta_strich_cos         <= C_90_18bit - (s_theta - C_180_18bit-1);
         s_theta_strich_sin_pos     <= '0';                      --Vorzeichen fuer Sinusfunktion negativ
         s_theta_strich_cos_pos     <= '0';                      --Vorzeichen fuer Cosinusfunktion negativ

      else
         s_theta_strich_sin         <= C_90_18bit-(s_theta - C_270_18bit - 1 );
         s_theta_strich_cos         <= s_theta - C_270_18bit - 1;
         s_theta_strich_sin_pos     <= '0';                      --Vorzeichen fuer Sinusfunktion negativ
         s_theta_strich_cos_pos     <= '1';                      --Vorzeichen fuer Cosinusfunktion positiv
      end if;

      s_STATE_HAUPT    <= SINUSBERECHNUNG_VORBEREITEN;

   ------------------------------------------------------------------------------------------
   when SINUSBERECHNUNG_VORBEREITEN =>   -- 3 Speichern der Werte und Sinus Berechnung starten
   ------------------------------------------------------------------------------------------
      s_sinus_theta          <= s_theta_strich_sin(15 downto 0);
      s_sinus_vorzeichen     <= s_theta_strich_sin_pos;
      s_Startsinus           <= '1';
      s_STATE_HAUPT          <= SINUSBERECHNEN;

   -----------------------------------------------------------------------------
   when SINUSBERECHNEN =>   -- 4 Warten auf Berechnung im sinusa-Prozess
   -----------------------------------------------------------------------------
      s_Startsinus       <= '0';
      if (s_sin_finish = '1') then
         s_STATE_HAUPT   <= HAUPTMULTIPLIKATION1VOR;
      end if;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION1VOR => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sig4_mult_a_17   <= s_sinus_ergebniss;
      sig4_mult_b_16   <= s_alpha;
      s_STATE_HAUPT    <= HAUPTMULTIPLIKATION1BER;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION1BER => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sig4_Mult_33    <= sig4_mult_a_17*sig4_mult_b_16;
      s_STATE_HAUPT   <= HAUPTMULTIPLIKATION1SAV;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION1SAV => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sa_Mult1h       <= sig4_Mult_33(32 DOWNTO 17);
      s_STATE_HAUPT   <= HAUPTMULTIPLIKATION2VOR;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION2VOR => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sig4_mult_a_17 <= s_sinus_ergebniss;
      sig4_mult_b_16 <= s_beta;
      s_STATE_HAUPT    <= HAUPTMULTIPLIKATION2BER;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION2BER => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sig4_Mult_33    <= sig4_mult_a_17*sig4_mult_b_16;
      s_STATE_HAUPT   <= HAUPTMULTIPLIKATION2SAV;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION2SAV => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sa_Mult2h       <= sig4_Mult_33(32 DOWNTO 17);
      s_STATE_HAUPT   <= COSINUSBERECHNUNG_VORBEREITEN;

   ------------------------------------------------------------------------------------------------
   when COSINUSBERECHNUNG_VORBEREITEN => -- 6. Speichern der Werte und Cosinus Berechnung starten
   ------------------------------------------------------------------------------------------------
      s_sinus_theta        <= s_theta_strich_cos(15 downto 0);
      s_sinus_vorzeichen   <= s_theta_strich_cos_pos;
      s_Startsinus         <= '1';
      s_STATE_HAUPT        <= COSINUSBERECHNEN;

   -----------------------------------------------------------------------------
   when COSINUSBERECHNEN =>     -- 7. Warten auf Berechnung im sinusa-Prozess
   -----------------------------------------------------------------------------
      s_Startsinus       <= '0';
      if (s_sin_finish = '1') then
         s_STATE_HAUPT   <= HAUPTMULTIPLIKATION3VOR;
      end if;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION3VOR => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sig4_mult_a_17 <= s_sinus_ergebniss;
      sig4_mult_b_16 <= s_alpha;
      s_STATE_HAUPT  <= HAUPTMULTIPLIKATION3BER;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION3BER => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sig4_Mult_33   <= sig4_mult_a_17*sig4_mult_b_16;
      s_STATE_HAUPT  <= HAUPTMULTIPLIKATION3SAV;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION3SAV => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sa_Mult3h      <= sig4_Mult_33(32 DOWNTO 17);
      s_STATE_HAUPT  <= HAUPTMULTIPLIKATION4VOR;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION4VOR => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sig4_mult_a_17 <= s_sinus_ergebniss;
      sig4_mult_b_16 <= s_beta;
      s_STATE_HAUPT  <= HAUPTMULTIPLIKATION4BER;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION4BER => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sig4_Mult_33  <= sig4_mult_a_17*sig4_mult_b_16;
      s_STATE_HAUPT <= HAUPTMULTIPLIKATION4SAV;

   ---------------------------------------------------------------------------
   when HAUPTMULTIPLIKATION4SAV => --5 Berechnung der Summanden
   ---------------------------------------------------------------------------
      sa_Mult4h     <= sig4_Mult_33(32 DOWNTO 17);
      s_STATE_HAUPT <= SUMME_BERECHNEN;

   ---------------------------------------------------------------------------
   when SUMME_BERECHNEN => --10.
   ---------------------------------------------------------------------------
      if (s_durchlauf = "00" OR s_durchlauf = "01" OR s_durchlauf = "10") then
         s_Summe_d      <=   sa_Mult3h + sa_Mult2h;
         s_Summe_q      <= - sa_Mult1h + sa_Mult4h;
      else
         s_Summe_d      <=   sa_Mult3h - sa_Mult2h;
         s_Summe_q      <=   sa_Mult1h + sa_Mult4h;
      end if;
      
      s_STATE_HAUPT     <=   ENDERGEBNISS_BERECHNEN;

   ---------------------------------------------------------------------------
   when ENDERGEBNISS_BERECHNEN => --11.
   ---------------------------------------------------------------------------
      --d                <= s_Summe_d; --Bei dieser Variante ist der Maximalwert 32767/2
      --q                <= s_Summe_q;

      if (ABS(s_Summe_d) < 16383)    then --Ergebniss ohne Ueberlauf
         IF s_Summe_d(15) = '0'      then
            if    s_durchlauf = "00" then s_d1     <= signed(std_logic_vector(s_Summe_d(14 downto 0) & '0'));
            elsif s_durchlauf = "01" then s_d2     <= signed(std_logic_vector(s_Summe_d(14 downto 0) & '0'));
            elsif s_durchlauf = "10" then s_d3     <= signed(std_logic_vector(s_Summe_d(14 downto 0) & '0')); 
            else                          s_alpha4 <= signed(std_logic_vector(s_Summe_d(14 downto 0) & '0')); end if;
         ELSE
            if    s_durchlauf = "00" then s_d1     <= signed(std_logic_vector(s_Summe_d(14 downto 0) & '1'));
            elsif s_durchlauf = "01" then s_d2     <= signed(std_logic_vector(s_Summe_d(14 downto 0) & '1'));
            elsif s_durchlauf = "10" then s_d3     <= signed(std_logic_vector(s_Summe_d(14 downto 0) & '1'));
            else                          s_alpha4 <= signed(std_logic_vector(s_Summe_d(14 downto 0) & '1')); end if;
         END IF;

      else       --Ergebniss mit Ueberlauf
         IF s_Summe_d(15) = '0'      then
            if    s_durchlauf = "00" then s_d1     <= to_signed(32767,16);
            elsif s_durchlauf = "01" then s_d2     <= to_signed(32767,16);
            elsif s_durchlauf = "10" then s_d3     <= to_signed(32767,16);
            else                          s_alpha4 <= to_signed(32767,16);  end if;
         ELSE
            if    s_durchlauf = "00" then s_d1     <= to_signed(-32767,16);
            elsif s_durchlauf = "01" then s_d2     <= to_signed(-32767,16);
            elsif s_durchlauf = "10" then s_d3     <= to_signed(-32767,16);
            else                          s_alpha4 <= to_signed(-32767,16); end if;
         END IF;
      end if;


      if (ABS(s_Summe_q) < 16383)    then --Ergebniss ohne Ueberlauf
         IF s_Summe_q(15) = '0'      then
            if    s_durchlauf = "00" then s_q1    <= signed(std_logic_vector(s_Summe_q(14 downto 0) & '0'));
            elsif s_durchlauf = "01" then s_q2    <= signed(std_logic_vector(s_Summe_q(14 downto 0) & '0'));
            elsif s_durchlauf = "10" then s_q3    <= signed(std_logic_vector(s_Summe_q(14 downto 0) & '0'));
            else                          s_beta4 <= signed(std_logic_vector(s_Summe_q(14 downto 0) & '0')); end if;
         ELSE
            if    s_durchlauf = "00" then s_q1    <= signed(std_logic_vector(s_Summe_q(14 downto 0) & '1'));
            elsif s_durchlauf = "01" then s_q2    <= signed(std_logic_vector(s_Summe_q(14 downto 0) & '1'));
            elsif s_durchlauf = "10" then s_q3    <= signed(std_logic_vector(s_Summe_q(14 downto 0) & '1'));
            else                          s_beta4 <= signed(std_logic_vector(s_Summe_q(14 downto 0) & '1')); end if;
         END IF;
      
      else       --Ergebniss mit Ueberlauf
         IF s_Summe_q(15) = '0'      then
            if    s_durchlauf = "00" then s_q1    <= to_signed(32767,16);
            elsif s_durchlauf = "01" then s_q2    <= to_signed(32767,16);
            elsif s_durchlauf = "10" then s_q3    <= to_signed(32767,16);
            else                          s_beta4 <= to_signed(32767,16);  end if;
         ELSE
            if    s_durchlauf = "00" then s_q1    <= to_signed(-32767,16);
            elsif s_durchlauf = "01" then s_q2    <= to_signed(-32767,16);
            elsif s_durchlauf = "10" then s_q3    <= to_signed(-32767,16);
            else                          s_beta4 <= to_signed(-32767,16); end if;
         END IF;
      end if;
      
       S_STATE_HAUPT     <= DATEN_RAUSGEBEN;
       
    ---------------------------------------------------------------------------
   when DATEN_RAUSGEBEN => --12.
   ---------------------------------------------------------------------------  
     
      if (s_durchlauf = "00" OR s_durchlauf = "01" OR s_durchlauf = "10") then
         S_STATE_HAUPT     <= EINGANG_AUSWAEHLEN;
      else
         S_STATE_HAUPT     <= EINGANG_SPEICHERN;
         Data_Out_Valid_dq <= '1';
         d1                <= s_d1;
         d2                <= s_d2;
         d3                <= s_d3;
         
         q1                <= s_q1;
         q2                <= s_q2;
         q3                <= s_q3;
         
         alpha4            <= s_alpha4;
         beta4             <= s_beta4;
      end if;
      
      s_durchlauf          <= s_durchlauf + 1;

      
   ----------------------------------------------------------------------------
  when others =>          -- Absicherung fuer FPGA
   ----------------------------------------------------------------------------
      s_STATE_HAUPT       <= EINGANG_SPEICHERN;

  end case;

  end if;
END process p_hauptprozess ;

END ARCHITECTURE behavioral;
