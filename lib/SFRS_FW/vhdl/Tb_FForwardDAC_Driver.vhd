-----------------------------------------------------------------------------------------------------------------------------
-- File name: Tb_FForwardDAC_Driver.vhd                                                                                    --
--                                                                                                                         --
-- Author   : D.Rodomonti                                                                                                  --
-- Date     : 21/01/2016                                                                                                   --
--                                                                                                                         --
-- Comments : FForwardDAC_Driver test bench                                                                                --
--                                                                                                                         --
-- History  : Start up version 25/01/2016                                                                                  --
-----------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

USE work.ACU_package.all;

Entity Tb_FForwardDAC_Driver is
End entity Tb_FForwardDAC_Driver;

Architecture beh of Tb_FForwardDAC_Driver is
constant TCcnt6us              : unsigned(9 downto 0):= to_unsigned(599,10); -- 600*10ns=6us

signal Clock                   : std_logic:='0';                        -- Clock signal @100MHz
signal Reset                   : std_logic;                        -- Asynchronous reset signal active high
signal NewDataChannel          : std_logic;                        -- New Data to sample enable/pulse. Active high.
signal cnt6us                  : unsigned(9 downto 0);

signal DataChannel_0           : std_logic_vector(15 downto 0);   -- Digital data to convert channel 0.
signal DataChannel_1           : std_logic_vector(15 downto 0);   -- Digital data to convert channel 1.
signal DataChannel_2           : std_logic_vector(15 downto 0);   -- Digital data to convert channel 2.
signal DataChannel_3           : std_logic_vector(15 downto 0);   -- Digital data to convert channel 3.

signal DataChannel_x           : array16bits(3 downto 0);
--
-- Component declaration
component  FForwardDAC_Driver is
 generic  
   (
      gFForwardDAC_DriverVersion      : real := 1.0
   );
  Port (
         Clock                   : in std_logic;                        -- Clock signal @100MHz
         Reset                   : in std_logic;                        -- Asynchronous reset signal active high
         NewDataChannel          : in std_logic;                        -- New Data to sample enable/pulse. Active high.
                                                                        -- Note!! NewSDI signal frequency is up to 6 us (as the USI max speed!)
         DataChannel_0           : in  std_logic_vector(15 downto 0);   -- Digital data to convert channel 0.
         DataChannel_1           : in  std_logic_vector(15 downto 0);   -- Digital data to convert channel 1.
         DataChannel_2           : in  std_logic_vector(15 downto 0);   -- Digital data to convert channel 2.
	 DataChannel_3           : in  std_logic_vector(15 downto 0);   -- Digital data to convert channel 3.
         
	 SDI_0                   : out  std_logic;                      -- Serial Data In to DACnr 0.
	 SDI_1                   : out  std_logic;                      -- Serial Data In to DACnr 1.
	 SDI_2                   : out  std_logic;                      -- Serial Data In to DACnr 2.
	 SDI_3                   : out  std_logic;                      -- Serial Data In to DACnr 3.
	 LDAC                    : out  std_logic;                      -- Load data into DAC Internal Register. Active on the rising edge.
	 SCLK                    : out  std_logic                       -- DAC input clock signal @12,5MHz
  );
end component;

component ACU_Timebase is
  Generic(
    gACU_TimebaseVersion      : real := 1.0;
    gTerminalCounter          : unsigned (15 downto 0):= to_unsigned(599,16) -- 600*10ns=6us
  );
  Port(
    Clock                         : in std_logic;                             -- Clock signal @100MHz
    Reset                         : in std_logic;                             -- Asynchronous reset signal active high
    TimeBAsePulse                 : out std_logic                             -- it is high one clk cycle when cntTimer=gTerminalCounter
  );
End component ACU_Timebase;

begin

Reset          <= '1';
Clock          <= not Clock after 5 ns;


p_6us_cnt:process(Clock,Reset)
begin
  if (Reset='1') then
    cnt6us           <=(others=>'0');
    NewDataChannel   <= '0';
    DataChannel_x(0) <= "1010101010101010";
    DataChannel_x(1) <= "0101010101010101";
    DataChannel_x(2) <= "0000111100001111";
    DataChannel_x(3) <= "1111000011110000";
  elsif(Clock'event and Clock='1') then
    if (cnt6us < TCcnt6us-1) then
      cnt6us         <= cnt6us+1;
      NewDataChannel <= '0';
    else
      cnt6us         <=(others=>'0');
      NewDataChannel <= '1';
      DataChannel_x  <= DataChannel_x(0)&DataChannel_x(3 downto 1);
    end if;
  end if;
end process;

DataChannel_0  <= DataChannel_x(0);
DataChannel_1  <= DataChannel_x(1);
DataChannel_2  <= DataChannel_x(2);
DataChannel_3  <= DataChannel_x(3);

i_FForwardDAC_Driver:  FForwardDAC_Driver

  Port Map(
         Clock           => Clock,
         Reset           => Reset,
         NewDataChannel  => NewDataChannel,
         DataChannel_0   => DataChannel_0,
         DataChannel_1   => DataChannel_1,
         DataChannel_2   => DataChannel_2,
	 DataChannel_3   => DataChannel_3,
	 SDI_0           => open,
	 SDI_1           => open,
	 SDI_2           => open,
	 SDI_3           => open,
	 LDAC            => open,
	 SCLK            => open
  );

i_ACU_Timebase: ACU_Timebase 
  Port Map(
    Clock             => Clock,
    Reset             => Reset,
    TimeBAsePulse     => open
  );

end beh;
