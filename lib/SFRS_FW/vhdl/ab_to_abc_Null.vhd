--  Filename: ab_to_abc.vhd
--
--  Beschrebung:
--      Transformation alpha beta -> abc 
--      Iu =        Ialpha +                      Igamma
--      Iv = -1/2 * Ialpha +  sqrt(3)/2 * Ibeta + Igamma
--      Iw = -1/2 * Ialpha -  sqrt(3)/2 * Ibeta + Igamma
--


--  Eing. Groessen:
--      alpha        alpha Komponente 16bit signed
--      beta         beta Komponente 16bit signed
--      NullComp     Null Komponente

--      clk          clock
--      Enable       symbolisiert Gültigkeit der Eingangsdaten
--      reset        Berechnung neue starten,
--
--  Ausg. Groessen:
--      Iu           Komponente des abc Systems, 16bit signed
--      Iv           Komponente des abc Systems, 16bit signed
--      Iw           Komponente des abc Systems, 16bit signed
--
--      Data_Out_Valid_abc  '1' wenn die Berechnung fertig ist
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity ab_to_abc_Null is
   generic ( takt : integer := 100_000_000);

   port(
      clk, reset, Enable    : in  std_logic;            -- System-Eingaenge:
      alpha, beta, NullComp : in  signed (15 downto 0); -- Eingaenge:
      Iu, Iv, Iw            : out signed (15 downto 0); -- Ausgaenge
      Data_Out_Valid_abc    : out std_logic);

end ab_to_abc_Null;

--============================================================================
architecture behavioral of ab_to_abc_Null is
   attribute multstyle               : string;
   attribute multstyle of behavioral : architecture is "dsp";           -- Nutzt bevorzugt embedded multiplier

   constant C1       : signed (15 downto 0) := to_signed (28377 , 16);  -- sqrt(3)/2   = 0,866 =  (2^15-1)* 0,866  = 28376
  
   signal s_NullComp : signed (15 downto 0) := (others => '0');
   signal s_alpha2   : signed (15 downto 0) := (others => '0');
   signal s_Mult32   : signed (31 downto 0) := (others => '0');
  
   type STATE_ab_to_abc_Null is(EINGANG_SPEICHERN, MULTIPLIKATION1, ADDITION);
   signal s_STATE : STATE_ab_to_abc_Null := EINGANG_SPEICHERN ;

begin
   hauptprozess     : process (clk, reset, Enable) 
   ALIAS sa_Mult32  : signed (15 DOWNTO 0) IS s_Mult32 (30 DOWNTO 15);
   
   begin 
      if (reset = '1') then
         Iu                  <= (others => '0')   ;
         Iv                  <= (others => '0')   ;
		   Iw                  <= (others => '0')   ;
         s_STATE             <= EINGANG_SPEICHERN ;
         Data_Out_Valid_abc  <= '0'               ;

      elsif rising_edge (clk) then
         CASE s_STATE is
            ----------------------------------------------------------------------------------
            when EINGANG_SPEICHERN =>       --1.Takt
            ----------------------------------------------------------------------------------
               Data_Out_Valid_abc <= '0';

               if (Enable = '1') then   
                  if     alpha < 0 then s_alpha2 <= '1' & alpha (15 downto 1)    ;
                  else                  s_alpha2 <= '0' & alpha (15 downto 1)    ; end if;

                  if    alpha + NullComp >  32766 then Iu <= to_signed( 32767,16);
                  elsif alpha + NullComp < -32766 then Iu <= to_signed(-32767,16);
                  else                                 Iu <= alpha + NullComp    ; end if;  

                  s_NullComp    <= NullComp        ;
                  s_STATE       <= MULTIPLIKATION1 ;
               end if;

            ----------------------------------------------------------------------------------
            when MULTIPLIKATION1  =>
            ----------------------------------------------------------------------------------
               s_Mult32 <= C1 * beta;  -- sqrt(3)/2 * beta
               s_STATE  <= ADDITION ;

            ---------------------------------------------------------------------------------
            when ADDITION =>                -- beide Reihen aufaddieren
            ---------------------------------------------------------------------------------
               if     resize(s_NullComp,17) + s_Mult32(31 downto 15) - resize(s_alpha2,17) >  32766 then  Iv <= to_signed( 32767,16);  
               elsif  resize(s_NullComp,17) + s_Mult32(31 downto 15) - resize(s_alpha2,17) < -32766 then  Iv <= to_signed(-32767,16);   
               else    Iv <= s_NullComp + sa_Mult32 - s_alpha2 ; end if;


               if     (resize(s_NullComp,17) - s_Mult32(31 downto 15) - resize(s_alpha2,17)) > to_signed(32766,17)  then  Iw <= to_signed( 32767,16);  
               elsif  (resize(s_NullComp,17) - s_Mult32(31 downto 15) - resize(s_alpha2,17)) < to_signed(-32766,17) then  Iw <= to_signed(-32767,16);   
               else        Iw <= s_NullComp - sa_Mult32 - s_alpha2 ; end if;

               s_STATE             <= EINGANG_SPEICHERN ;
               Data_Out_Valid_abc  <= '1'               ;

            end case;

      end if;  --reset
   end process hauptprozess;
end architecture behavioral;
