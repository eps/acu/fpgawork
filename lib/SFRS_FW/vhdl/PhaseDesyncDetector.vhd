--  Filename: PhaseDesyncDetector.vhd, J.M. geaendert am 29.4.2019
-- 
--  Beschreibung:
-- 	Dieser Block prüft, ob die Regelung von theta  mit PLL korrekt funktioniert, da sich bei einer Regelung von Iq auf Null zwei stabile
-- 	Zustände einstellen können (+- Id). Falls dies der Fall ist, wird der Regler Integrator zurückgesetzt.
--  Eing. Groessen:  
--		clk					Takteingang
--		Enable				Gueltigkeit der Eingangsdaten              
--		id						d Komponente 16bit signed
--		iq						Komponente 16bit signed
--		reset					Berechnung neu starten, alle Signale zuruecksetzen
--
--  Ausg. Groessen: 
--		resetPLL  			reset Ausgang für die PLL Regelung
--------------------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY PhaseDesyncDetector IS
   GENERIC( 
	takt        			: integer  := 100_000_000  ); --Initialisierung 50 MHz
    
   PORT(
      --System-Eingaenge: 
      clk, reset, 
      Enable   			: in std_logic ;

      --Eingaenge:
		id_signBit			: in std_logic;
      iq          	   : in signed(15 downto 0);
		--Ausgänge
		resetPLL				: out std_logic:='0');

END PhaseDesyncDetector ;

--============================================================================
ARCHITECTURE behavioral OF PhaseDesyncDetector IS

-- Reset_Sync
CONSTANT Iq_Upper :signed(15 DOWNTO 0):=to_signed(2000,16);
CONSTANT Iq_Lower :signed(15 DOWNTO 0):=to_signed(-2000,16);

BEGIN --===========================================================================

Reset_Sync: process (clk,enable) 

 BEGIN
	IF(rising_edge(clk)) THEN
		IF enable='1' THEN
			IF iq < Iq_Upper AND iq > Iq_Lower THEN   -- Bereich, da Iq trotz Regelung nie genau 0 ist.
				IF id_signBit = '1' THEN               -- Wenn id negativ, dann ist die Regelung um 180° verschoben
					resetPLL<='1';                      -- Zurücksetzen der Regelung auf 0 ->sollte wieder in Sync sein mit dem Netz
				ELSE
					resetPLL<='0';
				END IF;
			END IF;
		END IF;
	END IF;
END process Reset_Sync;

END ARCHITECTURE behavioral;