-- Filename: 3LevelConverterEmulator.vhd
-- Erstellt J.Mair, letzte Aenderung 32.4.2019
--
-- Inhalt:   Emulation des 3 Level Umrichters 
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY ThreeLevelConverterEmulator IS

   GENERIC(            												
      takt						: integer  	:= 100_000_000;   --Initialisierung 100 MHz
		maxCount 				: integer 	:=	100);   
PORT(
      clk,reset,      
      Enable					: IN STD_LOGIC;						
		
		S1u,S2u,S3u,S4u		: IN STD_LOGIC;
		S1v,S2v,S3v,S4v		: IN STD_LOGIC;
		S1w,S2w,S3w,S4w		: IN STD_LOGIC;

		UConverterVoltage		: OUT SIGNED(15 DOWNTO 0):=to_signed(0,16);-- 500V = 2^15
      VConverterVoltage		: OUT SIGNED(15 DOWNTO 0):=to_signed(0,16);-- 500V = 2^15
		WConverterVoltage		: OUT SIGNED(15 DOWNTO 0):=to_signed(0,16);-- 500V = 2^15
		
		UConverterAmperage	: OUT SIGNED(15 DOWNTO 0):=to_signed(0,16);-- ??
      VConverterAmperage	: OUT SIGNED(15 DOWNTO 0):=to_signed(0,16);-- ??
		WConverterAmperage	: OUT SIGNED(15 DOWNTO 0):=to_signed(0,16);-- ??
		
		U_VConverterAmperage : OUT SIGNED(15 DOWNTO 0):=to_signed(0,16);-- ??
		
		Data_Out_Valid_ConvEmulator 	: OUT STD_LOGIC := '0'
      );

END ThreeLevelConverterEmulator ;

--============================================================================
ARCHITECTURE behavioral OF ThreeLevelConverterEmulator IS

SIGNAL counter : UNSIGNED(9 DOWNTO 0):= to_unsigned(0,10);


SIGNAL s_UConverterVoltage		: SIGNED(31 DOWNTO 0):=to_signed(0,32);-- 500V = 2^31
SIGNAL s_VConverterVoltage		: SIGNED(31 DOWNTO 0):=to_signed(0,32);-- 500V = 2^31
SIGNAL s_WConverterVoltage		: SIGNED(31 DOWNTO 0):=to_signed(0,32);-- 500V = 2^31


SIGNAL s_UConverterAmperage	: SIGNED(31 DOWNTO 0):=to_signed(0,32);-- 500V = 2^31
SIGNAL s_VConverterAmperage	: SIGNED(31 DOWNTO 0):=to_signed(0,32);-- 500V = 2^31
SIGNAL s_WConverterAmperage	: SIGNED(31 DOWNTO 0):=to_signed(0,32);-- 500V = 2^31

BEGIN --===========================================================================



	UConverterVoltage		<= s_UConverterVoltage (31 DOWNTO 16);
	VConverterVoltage		<= s_VConverterVoltage (31 DOWNTO 16);
	WConverterVoltage		<= s_WConverterVoltage (31 DOWNTO 16);
	
	UConverterAmperage	<= s_UConverterAmperage(31 DOWNTO 16);
	VConverterAmperage	<= s_VConverterAmperage(31 DOWNTO 16);
	WConverterAmperage	<= s_WConverterAmperage(31 DOWNTO 16);
	
	U_VConverterAmperage <= s_UConverterAmperage(31 DOWNTO 16)-s_VConverterAmperage(31 DOWNTO 16);
	
	
p_main: PROCESS (clk,enable,S1u,S2u,S3u,S4u,S1v,S2v,S3v,S4v,S1w,S2w,S3w,S4w,reset) 
BEGIN
	IF(reset='1') THEN
		s_UConverterVoltage<=to_signed(0,32); -- 0V
		s_VConverterVoltage<=to_signed(0,32); -- 0V
		s_WConverterVoltage<=to_signed(0,32); -- 0V
	
   ELSIF (rising_edge(clk)) THEN
		IF enable='1' THEN
			s_UConverterVoltage<=to_signed(0,32); -- 0V
			s_VConverterVoltage<=to_signed(0,32); -- 0V
			s_WConverterVoltage<=to_signed(0,32); -- 0V
			
			IF(S1u='1' AND S2u='1') THEN	s_UConverterVoltage <=to_signed(1395864371,32);  END IF; -- 325V	
			IF(S2u='1' AND S3u='1') THEN	s_UConverterVoltage <=to_signed(0,32);     		 END IF; -- 0V
			IF(S3u='1' AND S4u='1') THEN	s_UConverterVoltage <=to_signed(-1395864371,32); END IF; -- -325V

			IF(S1v='1' AND S2v='1') THEN	s_VConverterVoltage <=to_signed(1395864371,32);  END IF; -- 325V
			IF(S2v='1' AND S3v='1') THEN	s_VConverterVoltage <=to_signed(0,32); 			 END IF; -- 0V
			IF(S3v='1' AND S4v='1') THEN	s_VConverterVoltage <=to_signed(-1395864371,32); END IF; -- -325V

			IF(S1w='1' AND S2w='1') THEN  s_WConverterVoltage <=to_signed(1395864371,32);  END IF; -- 325V
			IF(S2w='1' AND S3w='1') THEN	s_WConverterVoltage <=to_signed(0,32); 			 END IF; -- 0V
			IF(S3w='1' AND S4w='1') THEN	s_WConverterVoltage <=to_signed(-1395864371,32); END IF; -- -325V	
		END IF;
	END IF;
END PROCESS p_main;

p_int: PROCESS (enable,clk,reset) 
BEGIN
	IF(reset='1') THEN
		s_UConverterAmperage<=to_signed(0,32); -- 0V
		s_VConverterAmperage<=to_signed(0,32); -- 0V
		s_WConverterAmperage<=to_signed(0,32); -- 0V
	
   ELSIF rising_edge(clk) THEN
		IF (enable='1') THEN
			counter <= counter + 1;
         
			IF counter >= maxCount THEN -- alle 1us
				counter <= to_unsigned(0,10);
            
				s_UConverterAmperage<= s_UConverterAmperage + s_UConverterVoltage(31 DOWNTO 12); -- Warum Skalierung so off? erwartet DOWNTO 20->weil 1/L~= 2000 (2^11)
				s_VConverterAmperage<= s_VConverterAmperage + s_VConverterVoltage(31 DOWNTO 12); -- Nicht richtig skaliert, nur um Verlauf zu testen
				s_WConverterAmperage<= s_WConverterAmperage + s_WConverterVoltage(31 DOWNTO 12); -- Für richtige Skalierung:/2^20 * 1/L * VoltToAmpScaling
			END IF;
         
		END IF;
	END IF;
END PROCESS p_int;
END ARCHITECTURE behavioral;