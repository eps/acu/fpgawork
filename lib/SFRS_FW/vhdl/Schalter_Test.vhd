--      Aufgabe dieses Blocks ist es, die Signalkette zu den Schaltern zu testen,
--      sodass die Zuordnung des Schalters und Signals manuell geprueft werden kann

LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY Schalter_Test IS

   GENERIC(           
      --Initialisierung 100 MHz
      takt                    : integer  := 100_000_000  ); 
    
   PORT(
      --System-Eingänge:
      clk, reset, enable   : in std_logic ;
        
      --Eingänge:
      SchalterPosition     : in std_logic_vector (3 downto 0); -- "0000" 0  alle Schalter ausgeschaltet
                                                               -- "1101" 13 alle Schalter eingeschaltet
                                                               -- "1111" 15 Ausgaengen mit Eingaengen verbunden 
                                                               
                                                               -- "0001" 1 Schalter ist eingeschaltet
                                                               -- "0010" 2 Schalter ist eingeschaltet
                                                               -- "0011" 3 Schalter ist eingeschaltet ...      
      Schalter1_in         : in std_logic;
      Schalter2_in         : in std_logic;
      Schalter3_in         : in std_logic;   
      Schalter4_in         : in std_logic;

      Schalter5_in         : in std_logic;
      Schalter6_in         : in std_logic;
      Schalter7_in         : in std_logic;   
      Schalter8_in         : in std_logic;   

      
      Schalter1_out        : out std_logic := '0'; 
      Schalter2_out        : out std_logic := '0'; 
      Schalter3_out        : out std_logic := '0'; 
      Schalter4_out        : out std_logic := '0'; 
        
      Schalter5_out        : out std_logic := '0'; 
      Schalter6_out        : out std_logic := '0'; 
      Schalter7_out        : out std_logic := '0';     
      Schalter8_out        : out std_logic := '0');

END Schalter_Test ;

--============================================================================
ARCHITECTURE behavioral OF Schalter_Test IS

signal Schalter_vector : std_logic_vector (7 downto 0) := (others => '0');
begin

Schalter_Test_p: process(reset, clk, enable, SchalterPosition) 
  BEGIN
  
   if reset = '1' OR enable = '0' then
      Schalter_vector <= (others => '0');  
   
   elsif rising_edge(clk)  then
      case SchalterPosition is
         when "0000" =>  Schalter_vector <= "00000000";   -- Alles ausgeschaltet
         --Einzelne Schalter
         when "0001" =>  Schalter_vector <= "00000001";  -- 1 Entladeschutz An
         when "0010" =>  Schalter_vector <= "00000010";  -- 2 Ladeschutz An  
         when "0011" =>  Schalter_vector <= "00000100";  -- 3 Hauptschutz An
         when "0100" =>  Schalter_vector <= "00001000";  -- 4 Thyristor An
         when "0101" =>  Schalter_vector <= "00010000";  -- 5 Kurzschliesser An
         when "0110" =>  Schalter_vector <= "00100000";  -- 6 QP Schutz (pos) An
         when "0111" =>  Schalter_vector <= "01000000";  -- 7 QP Schutz (neg) An
         
         -- Gruppen
         when "1000" =>  Schalter_vector <= "01100000";  -- 8 QP Schutz (neg)  + QP Schutz (pos) An
         when "1001" =>  Schalter_vector <= "01100101";  -- 9 QP Schutz (neg)+(pos)  + Hauptschutz An + Entladeschutz Aus
         when "1010" =>  Schalter_vector <= "00000101";  -- 10 Entladeschutz Aus + Hauptschutz An
         when "1011" =>  Schalter_vector <= "00000011";  -- 11 Entladeschutz Aus + Ladeschutz AN 
         
         -- nicht belegt
         when others =>  Schalter_vector <= "00000000";   
      end case;
   end if;
END process Schalter_Test_p;


Schalter_out_p: process(reset, clk, enable, Schalter_vector, SchalterPosition) 
  BEGIN
      if reset = '1' OR enable = '0' then
         Schalter1_out <= '0'; Schalter5_out <= '0';
         Schalter2_out <= '0'; Schalter6_out <= '0';
         Schalter3_out <= '0'; Schalter7_out <= '0';
         Schalter4_out <= '0'; Schalter8_out <= '0';
     
      elsif rising_edge(clk) then
         if SchalterPosition = "1111" then    --15
            Schalter1_out  <= Schalter1_in;
            Schalter2_out  <= Schalter2_in;
            Schalter3_out  <= Schalter3_in;
            Schalter4_out  <= Schalter4_in;
        
            Schalter5_out  <= Schalter5_in;
            Schalter6_out  <= Schalter6_in;
            Schalter7_out  <= Schalter7_in;
            Schalter8_out  <= Schalter8_in;
         
         else
            Schalter1_out  <= Schalter_vector(0);
            Schalter2_out  <= Schalter_vector(1);
            Schalter3_out  <= Schalter_vector(2);
            Schalter4_out  <= Schalter_vector(3);
        
            Schalter5_out  <= Schalter_vector(4);
            Schalter6_out  <= Schalter_vector(5);
            Schalter7_out  <= Schalter_vector(6);
            Schalter8_out  <= Schalter_vector(7);
         end if;

      end if;
END process Schalter_out_p;

END architecture behavioral;
