-- Filename: adc4_7606_6Ch_Serial.vhd (VHDL FILE)
-- Date :    28.04.2016 (born) ; geaendert  15.10.2017 A.Wiest
-- Author:   M.Dudhat

-- SFRS Projekt
-- SERIAL READ OPERATION AD7606-6 (6-channels) 16-bits,6 Channels, Power supply : +5V

--Power-Down Mode Selection :
--Power-Down Mode    STBY       RANGE
--   Standby          0           1
--   Shutdown         0           0

--ADC LSB : 
--             +FS    MIDSCALE    –FS    LSB
--±10V RANGE   +10V    0V         –10V   305μV
--±5V RANGE    +5V     0V         –5V    152μV   

--Interface Mode Selection:
--PAR/SER/BYTE SEL      DB15        Interface Mode
--       0               0          Parallel interface mode
--       1               0          Serial interface mode	<- Das wird benutzt
--       1               1          Parallel byte interface                                                           

--SERIAL INTERFACE (PAR/SER/BYTE SEL = 1) PROCESS :

-- To read data back from the AD7606 over the serial interface, the PAR/SER/BYTE SEL pin must be tied high. 
-- The CS and SCLK signals are used to transfer data from the AD7606. 
-- The AD7606/ AD7606-6/AD7606-4 have two serial data output pins, adc1_DOUTA and adc1_DOUTB. 
-- Data can be read back from the AD7606/AD76706-6/AD7606-4 using one or both of these DOUT lines.

-- There is no change to the data read process when using two separate CONVST x signals.
-- The disadvantage of using just one DOUT line is that the throughput rate is reduced if reading occurs after conversion.
-- The unused DOUT line should be left unconnected in serial mode

-- All channels are sampled simul-taneously when both CONVST pins (CONVST A, CONVST B) are tied together. There is no change to the data read process when using two separate CONVST x signals
-- The CONVST A and CONVST B pins must be tied/driven together when oversampling is turned on.

-- For the AD7606-6,conversion results from Channel V1 to Channel V3 fireset appear on adc1_DOUTA, and conversion results from Channel V4 to Channel V6 fireset appear on adc1_DOUTB.

LIBRARY ieee;                                    					--Libarary Declaration
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.math_real.ALL;

--==========================================================================================================================================
entity adc4_7606_6Ch_Serial is                        					-- Entity Declaration

GENERIC (
   clk_in_hz     		: integer  := 100_000_000;    				-- 100MHz system clock  
	c_treset	   		: integer  := 5;             				   -- RESET high pulse with (treset, Anzahl der Takte
	c_conv_wait     	: integer  := 5;             				   -- Minimum delay between Reset low to convst high     (t7)
	c_busy_read_wait  : integer  := 600;             				-- Zeit bevor man die Busy Leitung zurückliesst
	c_tslck_2			: integer  := 3;             			   	-- Zeit für den high oder lowzustand des slck -Signals
	c_t4					: integer  := 3;             			   	-- Zeit t4 zw. busy=0 and cs =0
	c_t23					: integer  := 3;             			   	-- Zeit t23 zw. alle bits sind geschriebend cs =1
	c_bit_count			: integer  := 47);             			   -- Anzahl aller ADC bits 6*16=96/2 -1
   
PORT (
   clk           		: in  std_logic;                        	-- Clock Signal für FPGA Entity
   reset          	: in  std_logic;                        	-- reset Signal für FPGA Entity
   Enable	      	: in  std_logic;                        	-- Startet den Konvertierungsablauf
   	
   -- Ausgänge der ausgewerteten Kanäle  
   adc1_channel_1    : out std_logic_vector (15 downto 0):= (others => '0');   
   adc1_channel_2    : out std_logic_vector (15 downto 0):= (others => '0');
   adc1_channel_3    : out std_logic_vector (15 downto 0):= (others => '0');
   adc1_channel_4    : out std_logic_vector (15 downto 0):= (others => '0');
	adc1_channel_5    : out std_logic_vector (15 downto 0):= (others => '0');
	adc1_channel_6    : out std_logic_vector (15 downto 0):= (others => '0');
	
	adc2_channel_1    : out std_logic_vector (15 downto 0):= (others => '0');   
   adc2_channel_2    : out std_logic_vector (15 downto 0):= (others => '0');
   adc2_channel_3    : out std_logic_vector (15 downto 0):= (others => '0');
   adc2_channel_4    : out std_logic_vector (15 downto 0):= (others => '0');
	adc2_channel_5    : out std_logic_vector (15 downto 0):= (others => '0');
	adc2_channel_6    : out std_logic_vector (15 downto 0):= (others => '0');
	
	adc3_channel_1    : out std_logic_vector (15 downto 0):= (others => '0');   
   adc3_channel_2    : out std_logic_vector (15 downto 0):= (others => '0');
   adc3_channel_3    : out std_logic_vector (15 downto 0):= (others => '0');
   adc3_channel_4    : out std_logic_vector (15 downto 0):= (others => '0');
	adc3_channel_5    : out std_logic_vector (15 downto 0):= (others => '0');
	adc3_channel_6    : out std_logic_vector (15 downto 0):= (others => '0');
	
	adc4_channel_1    : out std_logic_vector (15 downto 0):= (others => '0');   
   adc4_channel_2    : out std_logic_vector (15 downto 0):= (others => '0');
   adc4_channel_3    : out std_logic_vector (15 downto 0):= (others => '0');
   adc4_channel_4    : out std_logic_vector (15 downto 0):= (others => '0');
	adc4_channel_5    : out std_logic_vector (15 downto 0):= (others => '0');
	adc4_channel_6    : out std_logic_vector (15 downto 0):= (others => '0');

		
   --Anschlüse an ADC
   adc1_DOUTA        : in  std_logic;                        	-- serial data transfer V1,V2,V3
	adc1_DOUTB        : in  std_logic;                        	-- serial data transfer V4,V5,V6
	
   adc2_DOUTA        : in  std_logic;                        	-- serial data transfer V1,V2,V3
	adc2_DOUTB        : in  std_logic;                        	-- serial data transfer V4,V5,V6
	
   adc3_DOUTA        : in  std_logic;                        	-- serial data transfer V1,V2,V3
	adc3_DOUTB        : in  std_logic;                        	-- serial data transfer V4,V5,V6
	
   adc4_DOUTA        : in  std_logic;                        	-- serial data transfer V1,V2,V3
	adc4_DOUTB        : in  std_logic;                        	-- serial data transfer V4,V5,V6

   n_cs          		: out std_logic;                        	-- chip select enables tri state databus
   n_sclk        		: out std_logic;                        	-- fireset falling edge after busy clocks data out
   start_conv	 		: out std_logic;                        	-- fireset falling edge after busy clocks data out
   adc_reset     		: out std_logic;                        	-- Startet die ADC Konversion
   Data_Out_Valid_ADC: out std_logic);     			 				-- ='1' falls alles fertig 
  
END adc4_7606_6Ch_Serial;  


--=================================================================================================
ARCHITECTURE  behavioral OF adc4_7606_6Ch_Serial IS

SIGNAL timer_1	  				: integer range 32767 downto 0  := 0;   -- Zähler für die Zeiten
SIGNAL timer_2	  				: integer range 63 downto 0     := 0;   -- Zähler für die Zeiten
SIGNAL timer_3	  				: integer range 63 downto 0     := 0;   -- Zähler für die Zeiten
SIGNAL s_bit_cnt 	  			: integer range 47 downto 0     := 47;  -- 6 Chanels x 16Bits

SIGNAL s_channel_adc1_reg_1: std_logic_vector (47 downto 0):= (others => '0');  
SIGNAL s_channel_adc1_reg_2: std_logic_vector (47 downto 0):= (others => '0');  

SIGNAL s_channel_adc2_reg_1: std_logic_vector (47 downto 0):= (others => '0');  
SIGNAL s_channel_adc2_reg_2: std_logic_vector (47 downto 0):= (others => '0');

SIGNAL s_channel_adc3_reg_1: std_logic_vector (47 downto 0):= (others => '0');  
SIGNAL s_channel_adc3_reg_2: std_logic_vector (47 downto 0):= (others => '0');  

SIGNAL s_channel_adc4_reg_1: std_logic_vector (47 downto 0):= (others => '0');  
SIGNAL s_channel_adc4_reg_2: std_logic_vector (47 downto 0):= (others => '0');    


TYPE state_type IS (RESET_DELAY,        -- Eine Zeit für Riset wird abgewartet
						  IDLE,               -- ADC wird in Ruhezustand geschaltet
						  START_CONVERT,		 -- Konvertirungspuls erzeugen
						  WAIT_FOR_BUSY,      -- busy = '1' Wandlung laeuft; = '0' Wandlung fertig  
                    DATA_BIT_COUNT,     -- Alle bits Durchgehen
                    DATA_WRITE,         -- Einzelne bits schreiben
                    DATA_OUTPUT,			 -- alle Ausgänge werden gleichzeitig ausgegeben
						  DATA_OUTPUT_WAIT);  -- Ein Takt warten     
 
SIGNAL ADC_STATE           :  state_type:= RESET_DELAY;  -- Statemaschine Signal

-- Synchronisation Signals for ADC
SIGNAL s_sync_data_a 		: std_logic;
SIGNAL s_sync_data_b 		: std_logic;
SIGNAL s_sync_data_c 		: std_logic;
SIGNAL s_sync_data_d 		: std_logic;

SIGNAL s_sync_data_e 		: std_logic;
SIGNAL s_sync_data_f 		: std_logic;
SIGNAL s_sync_data_g 		: std_logic;
SIGNAL s_sync_data_h 		: std_logic;

SIGNAL s_adc1_DOUTA 			: std_logic;
SIGNAL s_adc1_DOUTB  		: std_logic;

SIGNAL s_adc2_DOUTA 			: std_logic;
SIGNAL s_adc2_DOUTB  		: std_logic;

SIGNAL s_adc3_DOUTA 			: std_logic;
SIGNAL s_adc3_DOUTB  		: std_logic;

SIGNAL s_adc4_DOUTA 			: std_logic;
SIGNAL s_adc4_DOUTB  		: std_logic;



 --=================================================================================================
BEGIN
  
 ADC_Output_Synchronization_A : process (clk, reset, s_adc1_DOUTA)  --Synchronization of adc1_DOUTA signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_data_a <= '0';  				-- Data at the fireset clock 
         s_adc1_DOUTA  <= '0';  				-- Data at the second clock 
      else  
         s_sync_data_a <= adc1_DOUTA;     -- 1. Clock
         s_adc1_DOUTA  <= s_sync_data_a;  -- 2. Clock
      end if; 
   end if;  
  end process  ADC_Output_Synchronization_A;

   
 ADC_Output_Synchronization_B : process (clk, reset, s_adc1_DOUTB) --Synchronization of adc1_DOUTB signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_data_b <= '0';  			   -- Data at the fireset clock 
         s_adc1_DOUTB  <= '0';  				-- Data at the second clock 
      else  
         s_sync_data_b <= adc1_DOUTB;     -- 1. Clock
         s_adc1_DOUTB  <= s_sync_data_b;  -- 2. Clock
      end if; 
   end if;  
  end process  ADC_Output_Synchronization_B;


 
 ADC_Output_Synchronization_C: process (clk, reset, s_adc2_DOUTA)  --Synchronization of adc2_DOUTA signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_data_c <= '0';  				-- Data at the fireset clock 
         s_adc2_DOUTA  <= '0';  				-- Data at the second clock 
      else  
         s_sync_data_c <= adc2_DOUTA;     -- 1. Clock
         s_adc2_DOUTA  <= s_sync_data_c;  -- 2. Clock
      end if; 
   end if;  
  end process  ADC_Output_Synchronization_C;

   
 ADC_Output_Synchronization_D : process (clk, reset, s_adc2_DOUTB) --Synchronization of adc2_DOUTB signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_data_d <= '0';  			    -- Data at the fireset clock 
         s_adc2_DOUTB  <= '0';  				 -- Data at the second clock 
      else  
         s_sync_data_d <= adc2_DOUTB;     -- 1. Clock
         s_adc2_DOUTB  <= s_sync_data_d;  -- 2. Clock
      end if; 
   end if;  
  end process  ADC_Output_Synchronization_D;


 ADC_Output_Synchronization_E: process (clk, reset, s_adc3_DOUTA)  --Synchronization of adc3_DOUTA signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_data_e <= '0';  				-- Data at the fireset clock 
         s_adc3_DOUTA  <= '0';  				-- Data at the second clock 
      else  
         s_sync_data_e <= adc3_DOUTA;     -- 1. Clock
         s_adc3_DOUTA  <= s_sync_data_e;  -- 2. Clock
      end if; 
   end if;  
  end process  ADC_Output_Synchronization_E;

   
 ADC_Output_Synchronization_F : process (clk, reset, s_adc3_DOUTB) --Synchronization of adc3_DOUTB signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_data_f <= '0';  			    -- Data at the fireset clock 
         s_adc3_DOUTB  <= '0';  				 -- Data at the second clock 
      else  
         s_sync_data_f <= adc3_DOUTB;     -- 1. Clock
         s_adc3_DOUTB  <= s_sync_data_f;  -- 2. Clock
      end if; 
   end if;  
  end process  ADC_Output_Synchronization_F;

ADC_Output_Synchronization_G: process (clk, reset, s_adc4_DOUTA)  --Synchronization of adc3_DOUTA signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_data_g <= '0';  				-- Data at the fireset clock 
         s_adc4_DOUTA  <= '0';  				-- Data at the second clock 
      else  
         s_sync_data_g <= adc4_DOUTA;     -- 1. Clock
         s_adc4_DOUTA  <= s_sync_data_g;  -- 2. Clock
      end if; 
   end if;  
  end process  ADC_Output_Synchronization_G;

   
 ADC_Output_Synchronization_H : process (clk, reset, s_adc4_DOUTB) --Synchronization of adc3_DOUTB signal
  BEGIN
   if (rising_edge (clk)) then
      if (reset = '1') then
         s_sync_data_h <= '0';  			    -- Data at the fireset clock 
         s_adc4_DOUTB  <= '0';  				 -- Data at the second clock 
      else  
         s_sync_data_h <= adc4_DOUTB;     -- 1. Clock
         s_adc4_DOUTB  <= s_sync_data_h;  -- 2. Clock
      end if; 
   end if;  
  end process  ADC_Output_Synchronization_H;


  
conv_cycle : process (clk, reset, Enable)  -- Conversion Cycle Process Definition
BEGIN 
  
   if (reset ='1') then
		
		timer_1 						<= 0;
		timer_2 						<= 0;
		timer_3 						<= 0;
		s_bit_cnt  					<= c_bit_count;
		
		n_cs       					<= '1';
      n_sclk     					<= '1';
      adc_reset  					<= '1';
      start_conv 					<= '1';
      s_channel_adc1_reg_1  	<= (others => '0');	s_channel_adc1_reg_2  	<= (others => '0');
		s_channel_adc2_reg_1  	<= (others => '0');  s_channel_adc2_reg_2  	<= (others => '0');
		s_channel_adc3_reg_1  	<= (others => '0');	s_channel_adc3_reg_2  	<= (others => '0');
		s_channel_adc4_reg_1  	<= (others => '0');  s_channel_adc4_reg_2  	<= (others => '0');


		ADC_STATE    				<= RESET_DELAY; 
		Data_Out_Valid_ADC		<= '0';
		
   elsif (rising_edge(clk) and clk = '1') then
      CASE ADC_STATE IS                 
	
	---------------------------------------------------------------------------------------------------
   when RESET_DELAY =>  --[1. Zustand]  "treset" =50ns
   ---------------------------------------------------------------------------------------------------
		if (timer_1 < c_treset) then                 
			timer_1    <= timer_1 + 1 ; 
		else
			timer_1    <= 0;
			adc_reset  <= '0';
			ADC_STATE  <= IDLE; 
	   end if;
	
	---------------------------------------------------------------------------------------------------
   when IDLE =>    --[2. Zustand] die Zeit  wird von Ausseren Beschaltung bestimmt
   ---------------------------------------------------------------------------------------------------
		if (Enable = '1') then
			ADC_STATE  <= START_CONVERT; 
		end if;
		
		Data_Out_Valid_ADC<= '0';
	
   ---------------------------------------------------------------------------------------------------
   when START_CONVERT  =>    --[2. Zustand] Starte Konvertierung Puls ----___---erzeugen
   ---------------------------------------------------------------------------------------------------
		if (timer_1 < c_conv_wait) then     --t7     
			start_conv  <= '1';
			timer_1   	<= timer_1 + 1;			
		
		elsif (timer_2 < c_conv_wait) then  --t7     
			timer_2   	<= timer_2 + 1;	
			start_conv  <= '0';
		
		elsif (timer_3 < c_conv_wait) then  --t7     
			timer_3   	<= timer_3 + 1;	
			start_conv  <= '1';	
		
		else
			timer_1 						<= 0;
			timer_2 						<= 0;
			timer_3 						<= 0;
			s_channel_adc1_reg_1 	<= (others => '0');	s_channel_adc1_reg_2 	<= (others => '0');
			s_channel_adc2_reg_1 	<= (others => '0');	s_channel_adc2_reg_2 	<= (others => '0');	
			s_channel_adc3_reg_1 	<= (others => '0');	s_channel_adc3_reg_2 	<= (others => '0');
			s_channel_adc4_reg_1 	<= (others => '0');	s_channel_adc4_reg_2 	<= (others => '0');

			ADC_STATE 		  			<= WAIT_FOR_BUSY;
		end if;
	
	---------------------------------------------------------------------------------------------------
    when WAIT_FOR_BUSY =>   --4 Wandlung abwarten
	---------------------------------------------------------------------------------------------------     
		if (timer_1 < c_busy_read_wait) then    -- warte für das Zurücklesen von busy		
			timer_1   	   <= timer_1 + 1;	    -- Signal "busy" wird nicht abgefragt
		else 
			timer_1 			<=  0;
			n_cs           <= '0';
	      n_sclk         <= '0';
			ADC_STATE   	<= DATA_WRITE;
	   end if;

	---------------------------------------------------------------------------------------------------
    when DATA_BIT_COUNT =>   --5. alle bits schreiben
	---------------------------------------------------------------------------------------------------
      if  (s_bit_cnt > 0)  then
         s_bit_cnt    	<= s_bit_cnt - 1; 	--Zähler für die Bits
         ADC_STATE    	<= DATA_WRITE;

      else 
			n_cs           <= '1';					-- Ist Fertig
			n_sclk         <= '1';
         s_bit_cnt      <= c_bit_count;
         ADC_STATE      <= DATA_OUTPUT;
      end if;
     
	---------------------------------------------------------------------------------------------------
    when DATA_WRITE =>       -- einzelne bits schreiben
	---------------------------------------------------------------------------------------------------
      -- Um die Daten auszulesen muss die n_RD Leitung jedes Mal einmal Low- und High- Zustand annehmen 
		if (timer_1 < c_tslck_2) then
         n_sclk   <= '0'; 
			timer_1  <= timer_1 + 1;

		elsif (timer_2 < c_tslck_2-2) then	--Hold rd_low for c_wait_rd_low (t21)
			n_sclk   <= '1';   				
			timer_2  <= timer_2 + 1;           
      
		else
			timer_1   <= 0;
			timer_2   <= 0; -- Die Daten sind synchronisiert, also sind zwei Takte zu spät, deswegen werden diese bei n_csk='1' geschrieben
			ADC_STATE <=  DATA_BIT_COUNT; 
			s_channel_adc1_reg_1 (s_bit_cnt)  <= s_adc1_DOUTA; -- Die Daten des ADC werden in den Register 
			s_channel_adc1_reg_2 (s_bit_cnt)  <= s_adc1_DOUTB; -- geschrieben
			
         s_channel_adc2_reg_1 (s_bit_cnt)  <= s_adc2_DOUTA; -- Die Daten des ADC werden in den Register 
			s_channel_adc2_reg_2 (s_bit_cnt)  <= s_adc2_DOUTB; -- geschrieben
			
			s_channel_adc3_reg_1 (s_bit_cnt)  <= s_adc3_DOUTA; -- Die Daten des ADC werden in den Register 
			s_channel_adc3_reg_2 (s_bit_cnt)  <= s_adc3_DOUTB; -- geschrieben
			
         s_channel_adc4_reg_1 (s_bit_cnt)  <= s_adc4_DOUTA; -- Die Daten des ADC werden in den Register 
			s_channel_adc4_reg_2 (s_bit_cnt)  <= s_adc4_DOUTB; -- geschrieben
			
		end if;
   
	---------------------------------------------------------------------------------------------------
    when DATA_OUTPUT =>        --[8 Zustand] 1Takt
	---------------------------------------------------------------------------------------------------
		adc1_channel_1    <= s_channel_adc1_reg_1(47 downto 32); -- adc1_DOUTA Kanäle 1,2,3 sind Ausgänge 
      adc1_channel_2    <= s_channel_adc1_reg_1(31 downto 16); 
      adc1_channel_3    <= s_channel_adc1_reg_1(15 downto 0);
		
      adc1_channel_4    <= s_channel_adc1_reg_2(47 downto 32); -- adc1_DOUTB  Kanäle 4,5,6 sind Ausgänge 
      adc1_channel_5    <= s_channel_adc1_reg_2(31 downto 16);             
      adc1_channel_6    <= s_channel_adc1_reg_2(15 downto 0);             
 
		
      adc2_channel_1    <= s_channel_adc2_reg_1(47 downto 32); -- adc2_DOUTA Kanäle 1,2,3 sind Ausgänge 
      adc2_channel_2    <= s_channel_adc2_reg_1(31 downto 16); 
      adc2_channel_3    <= s_channel_adc2_reg_1(15 downto 0);
		
      adc2_channel_4    <= s_channel_adc2_reg_2(47 downto 32); -- adc2_DOUTB  Kanäle 4,5,6 sind Ausgänge 
      adc2_channel_5    <= s_channel_adc2_reg_2(31 downto 16);             
      adc2_channel_6    <= s_channel_adc2_reg_2(15 downto 0); 

      
		adc3_channel_1    <= s_channel_adc3_reg_1(47 downto 32); -- adc3_DOUTA Kanäle 1,2,3 sind Ausgänge 
      adc3_channel_2    <= s_channel_adc3_reg_1(31 downto 16); 
      adc3_channel_3    <= s_channel_adc3_reg_1(15 downto 0);
		
      adc3_channel_4    <= s_channel_adc3_reg_2(47 downto 32); -- adc3_DOUTB  Kanäle 4,5,6 sind Ausgänge 
      adc3_channel_5    <= s_channel_adc3_reg_2(31 downto 16);             
      adc3_channel_6    <= s_channel_adc3_reg_2(15 downto 0); 
		
		
      adc4_channel_1    <= s_channel_adc4_reg_1(47 downto 32); -- adc4_DOUTA Kanäle 1,2,3 sind Ausgänge 
      adc4_channel_2    <= s_channel_adc4_reg_1(31 downto 16); 
      adc4_channel_3    <= s_channel_adc4_reg_1(15 downto 0);
		
      adc4_channel_4    <= s_channel_adc4_reg_2(47 downto 32); -- adc4_DOUTB  Kanäle 4,5,6 sind Ausgänge 
      adc4_channel_5    <= s_channel_adc4_reg_2(31 downto 16);             
      adc4_channel_6    <= s_channel_adc4_reg_2(15 downto 0); 

      Data_Out_Valid_ADC<= '1';
      ADC_STATE    		<= DATA_OUTPUT_WAIT;
		
	---------------------------------------------------------------------------------------------------
    when DATA_OUTPUT_WAIT =>        --[9 Zustand] 1Takt
	---------------------------------------------------------------------------------------------------
		ADC_STATE    		<= IDLE;		-- Es wird ein Takt gewartet, damit die Module die mit 50MHz takten
												-- auch diesen Signal als Enable benutzen koennten
    
   END CASE;
 END IF;
 END process;

END ARCHITECTURE behavioral;
