--  Beschreibung:
--  Entkopplung der Id und Iq Komponenten

-- Eing. Groesse: 
-- I_d, I_q - die d und q komponente des Netzstromes

--  Ausg. Groessen: 
--  U_d_entk = - I_q* omegaL Signed 16 Bit Enkoppelgroesse der d-Komponente
--  U_q_entk = I_d * omegaL Signed 16 Bit Enkoppelgroesse der q-Komponente


--========================================================================
LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY Entkopplung IS
   GENERIC   (takt  : integer   := 100_000_000 ); --Initialisierung 100 MHz    
   
   PORT      (
      clk, reset, 
      Enable         : IN STD_LOGIC ;                           -- System-Eingaenge: 
         
      I_d            : IN SIGNED (15 downto 0):=(others=>'0');  -- Eingaenge
      I_q            : IN SIGNED (15 downto 0):=(others=>'0');  -- 32767 entspricht der Zahl 1 als Ergebnis 
      omegaL         : IN SIGNED (15 downto 0):=(others=>'0');  -- 32767 entspricht der Zahl 1 als Ergebnis 

      U_d_entk       : OUT SIGNED(15 DOWNTO 0):=(others=>'0');  -- Ausgaenge
      U_q_entk       : OUT SIGNED(15 DOWNTO 0):=(others=>'0'); 
      
      Data_Out_Valid : OUT STD_LOGIC:='0' );

END Entkopplung ;


ARCHITECTURE behavioral OF Entkopplung IS
--attribute multstyle : string;
--attribute multstyle of behavioral : architecture is "logic"; -- > 9bit multiplier werden nicht genutzt, sondern nur logic    
   
   SIGNAL   s_Id         : SIGNED(15 DOWNTO 0) :=(others=>'0');
   SIGNAL   s_Iq         : SIGNED(15 DOWNTO 0) :=(others=>'0');
   SIGNAL   s_Ud_entk    : SIGNED(15 DOWNTO 0) :=(others=>'0');
   SIGNAL   s_Uq_entk    : SIGNED(15 DOWNTO 0) :=(others=>'0');
   
-- Berechnung der Multiplikation
   SIGNAL    s_Mult_32   : SIGNED (31 DOWNTO 0):=(others=>'0');
   SIGNAL    s_Mult1     : SIGNED (15 DOWNTO 0):=(others=>'0');
   SIGNAL    s_Mult2     : SIGNED (15 DOWNTO 0):=(others=>'0');
   
   --Zustaende---------
   TYPE T_STATE_BERECHNUNG IS(   EINGANG_SPEICHERN,
                                 MULT1_VORB, MULT1, MULT1_SP,
                                 MULT2, MULT2_SP,
                                 DATEN_AUSGEBEN   );
   SIGNAL s_STATE_BERECHNUNG:   T_STATE_BERECHNUNG := EINGANG_SPEICHERN;
   
   
   
BEGIN
        
berechnung: PROCESS (clk, reset, Enable) 
BEGIN

      if (reset = '1') then
         Data_Out_Valid       <= '0';
         s_STATE_BERECHNUNG   <= EINGANG_SPEICHERN;
         s_Id                 <= (others=>'0');
         s_Iq                 <= (others=>'0');
         s_Ud_entk            <= (others=>'0');
         s_Uq_entk            <= (others=>'0');
         
      else
         if rising_edge(clk)   THEN
            CASE s_STATE_BERECHNUNG IS
            
   ------------------------------------------------------------------ 
   when EINGANG_SPEICHERN =>  --1.Takt
   ------------------------------------------------------------------      
      Data_Out_Valid          <= '0';
      
      IF (Enable = '1') THEN  -- Erste Multiplikation vorbereiten
         s_Id                 <= I_d;
         s_Iq                 <= I_q;
         s_STATE_BERECHNUNG   <= MULT1_VORB;
      ELSE
         Null;
      END IF;
   
   ------------------------------------------------------------------ 
   when MULT1_VORB =>         --2.Takt
   ------------------------------------------------------------------         
      s_Mult1                 <= s_Id;
      s_Mult2                 <= omegaL;
      s_STATE_BERECHNUNG      <= MULT1;
   
   ------------------------------------------------------------------ 
   when MULT1 =>              --3.Takt
   ------------------------------------------------------------------      
      s_Mult_32               <= s_Mult1 * s_Mult2;         -- d-Komponente der Vorsteuerung
      s_STATE_BERECHNUNG      <= MULT1_SP;
   
   ------------------------------------------------------------------ 
   when MULT1_SP =>           --4.Takt
   ------------------------------------------------------------------
      s_Ud_entk               <= s_Mult_32(30 DOWNTO 15);   -- Ergebnis abspeichern
      s_Mult1                 <= s_Iq;                      -- Multiplikation vorbereiten
      s_STATE_BERECHNUNG      <= MULT2;
      
   ------------------------------------------------------------------ 
   when MULT2 =>              --5.Takt
   ------------------------------------------------------------------      
      s_Mult_32               <= s_Mult1 * s_Mult2;         -- q-Komponente der Vorsteuerung
      s_STATE_BERECHNUNG      <= MULT2_SP;
      
   ------------------------------------------------------------------ 
   when MULT2_SP =>           --6.Takt
   ------------------------------------------------------------------          
      s_Uq_entk               <= s_Mult_32(30 DOWNTO 15);   -- Ergebnis abspeichern
      s_STATE_BERECHNUNG      <= DATEN_AUSGEBEN;

   ------------------------------------------------------------------ 
   when DATEN_AUSGEBEN =>     --7.Takt
   ------------------------------------------------------------------      
      U_q_entk                <= NOT(s_Uq_entk) + 1;  -- d-Komponente der Vorsteuerung
      U_d_entk                <= s_Ud_entk;           -- q-Komponente der Vorsteuerung
      Data_Out_Valid          <= '1';
      s_STATE_BERECHNUNG      <= EINGANG_SPEICHERN;
   
   ---------------------------------------------------------------------------------
   when OTHERS=>   
   ----------------------------------------------------------------------------------
      s_STATE_BERECHNUNG      <=  EINGANG_SPEICHERN;   
      
END CASE;
END IF;
END IF;
END PROCESS berechnung;

END ARCHITECTURE behavioral;
