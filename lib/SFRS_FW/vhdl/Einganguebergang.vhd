--  Filename: EingangUebergang.vhd
--    8*10ns = 80ns
--  Beschrebung:
--      macht langsamen Uebergang von dem Eingang1 auf Eingang2 und umgekehrt
--      Ausgang = Eingang1*(1-t) + Eingang2 * t oder umgekehrt
--
--  Eing. Groessen:
--      Eingang1       16bit signed
--      Eingang2       16bit signed
--      UebergangUPDown = '1' -> Uebergang von Eing1. auf Eing. 2 ='0' dann andersrum
--      Schaltet man den Uebergang um, bevor dieser abgeschlossen ist, so ergibt das einen harten Übergang

--      clk            clock
--      Enable         symbolisiert Gültigkeit der Eingangsdaten
--      reset          Berechnung neue starten, alle Varablen auf Null setzen
--
--  Ausg. Groessen:
--      Ausgang        16bit signed
--      Data_Out_Valid  '1' wenn die Berechnung des Hauptprozesses Fertiig ist
--      counter_ready Uebergang abgeschlossen (ein kurzer Puls)
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity EingangUebergang is
   generic(
      takt             : integer   := 100_000_000;    -- Initalisierung 100 MHz
     gInternTimer_loop : integer   := 10);            -- 10 -> ca. 65 ms uebergang
                                                      -- Ein Durchlauf entspricht einer Berechnung

   port(
      clk, reset, Enable   : in std_logic;            -- System-Eingaenge

      UebergangUPDown      : in std_logic;            -- =1 Uebergang von Eing1 -> Eing2; =0 Ueberg. Eing2->Eing1
      Eingang1             : in signed (15 downto 0); -- Eingang beim dem gestartet wird
      Eingang2             : in signed (15 downto 0); -- Eingang in den uebergegangen wird

      Ausgang              : out signed(15 downto 0); -- Ausgaeng
      counter_ready        : out std_logic;           -- Uebergang ist passiert bei =1
      Data_Out_Valid       : out std_logic );

   end EingangUebergang;

--============================================================================
architecture behavioral of EingangUebergang is
  attribute multstyle               : string;
  attribute multstyle of behavioral : architecture is "dsp";    -- Nutzt bevorzugt embedded multiplier

  signal s_Eingang1                      : signed   (15 downto 0) := (others => '0');
  signal s_Eingang2                      : signed   (15 downto 0) := (others => '0');

  signal s_Mult32 : signed (31 downto 0) := (others => '0');
  signal s_Mult1  : signed (15 downto 0) := (others => '0');
  signal s_Mult2  : signed (15 downto 0) := (others => '0');
  signal sa_Mult1 : signed (15 downto 0) := (others => '0');
  signal sa_Mult2 : signed (15 downto 0) := (others => '0');

  signal s_Summe  : signed (16 downto 0) := (others => '0');  --16 + 16 = 17bit
  signal s_UebergangUPDown_vorher        : STD_LOGIC              := '0';            -- Vorheriger Zustand des Ubergangssignals

  type STATE_EingangUebergang is(
                           EINGANG_SPEICHERN,
                           MULT1VORBEREITEN, MULT1, MULT1_SPECHERN,
                           MULT2, MULT2_SPECHERN,
                           ADDITION, DATENLIMIT,
                           ZAELEN);
  signal s_STATE : STATE_EingangUebergang := EINGANG_SPEICHERN;

   -- Signale fuer den Counter
   SIGNAL s_Timer_intern             : unsigned(17 DOWNTO 0)        :=(others => '0'); -- Innere Schleife
   SIGNAL s_Counter_Up               : signed(15 DOWNTO 0)          :=(others => '0'); -- Counter, der nach oben zur 32767 zaehlt
   SIGNAL s_Counter_Down             : signed(15 DOWNTO 0)          :=(others => '0'); -- Counter, der nach unten zur 0 zaehlt
 
begin

  hauptprozess : process (clk, reset, Enable)
  begin
   if (reset = '1') then
      s_Eingang1                <= (others => '0');
      s_Eingang2                <= (others => '0');

      s_Mult32                  <= (others => '0');
      s_Mult1                   <= (others => '0');
      s_Mult2                   <= (others => '0');
      sa_Mult1                  <= (others => '0');
      sa_Mult2                  <= (others => '0');

      s_Summe                   <= (others => '0');
      
      s_Counter_Up              <= (others => '0');
      s_Counter_Down            <= to_signed(32767,16);
      s_Timer_intern            <= (others => '0');
      counter_ready             <= '0'; --Uebergang noch nicht statgefunden oder findet statt

      s_STATE                   <= EINGANG_SPEICHERN;
      Data_Out_Valid            <= '0';

   elsif rising_edge(clk) then
      case s_STATE is

   ----------------------------------------------------------------------------------
   when EINGANG_SPEICHERN =>       --1.Takt
   ----------------------------------------------------------------------------------
      if (Enable = '1') then
         s_Eingang1        <= Eingang1;
         s_Eingang2        <= Eingang2;
         s_STATE           <= MULT1VORBEREITEN;

         Data_Out_Valid    <= '0';
      end if;

   ----------------------------------------------------------------------------------
   when MULT1VORBEREITEN =>     --2.Takt
   ----------------------------------------------------------------------------------
      s_Mult1          <= s_Eingang1;
      s_Mult2          <= s_Counter_Down;
      s_STATE          <= MULT1;

----------------------------------------------------------------------------------
   when MULT1 =>                --3.Takt
   ----------------------------------------------------------------------------------
      s_Mult32      <= s_Mult1 * s_Mult2;
      s_STATE       <= MULT1_SPECHERN;

   ----------------------------------------------------------------------------------
   when MULT1_SPECHERN =>       --4.Takt
   ----------------------------------------------------------------------------------
      sa_Mult1      <= s_Mult32 (30 downto 15);
      s_Mult1       <= s_Eingang2;
      s_Mult2       <= s_Counter_Up;
      s_STATE       <= MULT2;

   ----------------------------------------------------------------------------------
   when MULT2 =>                --5.Takt
   ----------------------------------------------------------------------------------
      s_Mult32      <= s_Mult1 * s_Mult2;
      s_STATE       <= MULT2_SPECHERN;

   ----------------------------------------------------------------------------------
   when MULT2_SPECHERN =>       --6.Takt
   ----------------------------------------------------------------------------------
      sa_Mult2      <= s_Mult32 (30 downto 15);
      s_STATE       <= ADDITION;

   ---------------------------------------------------------------------------------
   when ADDITION =>             --7. Takt, beide Ergebnisse addieren
   ---------------------------------------------------------------------------------
      s_Summe       <= resize(sa_Mult1, 17) + resize(sa_Mult2, 17);
      s_STATE       <= DATENLIMIT;

   ---------------------------------------------------------------------------------
   when DATENLIMIT =>           --8.Takt
   ---------------------------------------------------------------------------------
      if    s_Summe > to_signed(32767, 16)  then  Ausgang <= to_signed(32767, 16);
      elsif s_Summe < to_signed(-32767, 16) then  Ausgang <= to_signed(-32767, 16);
      else                                        Ausgang <= s_Summe  (15 downto 0);
      end if;

      Data_Out_Valid       <= '1';
      S_STATE              <= ZAELEN;

   ------------------------------------------------------------------
   WHEN ZAELEN =>
   ------------------------------------------------------------------    -- Aeussere Schleife
      if(s_Timer_intern  < to_unsigned(gInternTimer_loop, 17)) then      -- eine Schleife 
         s_Timer_intern     <= s_Timer_intern + 1;                        -- Inere Schleife
      else
         s_Timer_intern     <=(others => '0');
         if    (UebergangUPDown ='1' AND s_Counter_Up < 32767) then
            s_Counter_Up    <= s_Counter_Up   + 1;
            s_Counter_Down  <= s_Counter_Down - 1;
            counter_ready   <= '0';
         elsif (UebergangUPDown ='0' AND s_Counter_Up >0)      then
            s_Counter_Up    <= s_Counter_Up   - 1;
            s_Counter_Down  <= s_Counter_Down + 1;
            counter_ready   <= '0';
         else 
            counter_ready   <= '1';             --uebergang ist passiert;
         end if;
      end if;
      S_STATE               <= EINGANG_SPEICHERN;

   ---------------------------------------------------------------------
   when others =>
   ---------------------------------------------------------------------
      s_STATE <= EINGANG_SPEICHERN;

   end case;
   end if;  --reset
   end process hauptprozess;

   end architecture behavioral;
