-- Filename:                ADC_Takt.vhd
-- Inhalt:                  gibt ein Takt zur abtastung für ein ADC und DAC
--
-- Author:                  Wiest Alexander 
-- Letzte Aenderung:        15.01.2019
--
--============================================================================
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;


ENTITY ADC_Takt IS
    
GENERIC (
   clk_input             : integer  := 100_000_000; -- 100MHz system clock  
   ckl_time_FIR          : integer  := 125;         -- 1,25us x 2= 2,5us -> 400kHz
   clk_time_BandPass_FIR : integer  := 111;         -- 
   clk_time_DAC          : integer  := 500;         -- 5us (als DAC Takt       -> kurzer Puls 10ns)
   ADC_schleifen         : integer  := 2;           -- 5*2= 10us als ADC Takt  -> kurzer Puls 10ns 
   Regler_schleifen      : integer  := 12);                                               
    
PORT(
   reset                 : IN std_logic;            -- System   
   clk                   : IN std_logic;
   Enable                : IN std_logic;            -- '0'Ausgang ist ='0'
   
   PWM_Takt_Enable       : IN std_logic; 
   PWM_Takt              : IN std_logic; 
                                                    --Ausgang
   clk_out_DAC,                                     -- DAC TAkt
   clk_out_ADC,                                     -- ADC Takt
   clk_out_Regler,
   clk_out_FIR,
   clk_BandPass_FIR      : OUT std_logic);
            
END ADC_Takt ;

--============================================================================
ARCHITECTURE behavioral OF ADC_Takt IS
  
TYPE   state_type_clk IS (IDLE, ZAEHLEN);
SIGNAL STATE            : state_type_clk := IDLE; 

SIGNAL s_zahler_DAC     : unsigned (15 downto 0)   := (others => '0');    
SIGNAL s_zahler_ADC     : unsigned (15 downto 0)   := (others => '0'); 
SIGNAL s_zahler_Regler  : unsigned (15 downto 0)   := (others => '0'); 

SIGNAL s_zahler_signal3 : unsigned (15 downto 0)   := (others => '0');
SIGNAL s_zahler_signal4 : unsigned (15 downto 0)   := (others => '0'); 
SIGNAL s_zahler_signal5 : unsigned (15 downto 0)   := (others => '0');
SIGNAL s_zahler_signal6 : unsigned (15 downto 0)   := (others => '0'); 


TYPE   t_reglerStates IS (IDLE, ZAEHLEN);
SIGNAL s_reglerState    : t_reglerStates := IDLE; 
SIGNAL s_ReglerTaktCnt  : UNSIGNED(15 DOWNTO 0);


BEGIN

ADC_Takt_p : PROCESS (clk, reset, Enable, PWM_Takt_Enable, PWM_Takt)
BEGIN
   IF reset = '1' then
      s_zahler_DAC      <= (others => '0');
      s_zahler_ADC      <= (others => '0');
      s_zahler_Regler   <= (others => '0');
      clk_out_DAC       <= '0';
      clk_out_ADC       <= '0';
      clk_out_Regler    <= '0';
      STATE             <= IDLE;   
    
   ELSIF (rising_edge(clk))THEN       
      CASE STATE IS                --State Machine Definition
   --------------------------------------------------------------------------------------------------
   when IDLE =>  
   --------------------------------------------------------------------------------------------------
      if   (Enable = '1' ) then
         STATE          <= ZAEHLEN;  
      else
         clk_out_DAC    <= '0';
         clk_out_ADC    <= '0';
         clk_out_Regler <= '0';
         STATE          <= IDLE;
      end if;
      
   --------------------------------------------------------------------------------------------------
   when ZAEHLEN =>  
   --------------------------------------------------------------------------------------------------
      if (PWM_Takt = '1' AND PWM_Takt_Enable = '1') then 
         s_zahler_DAC        <= to_unsigned(clk_time_DAC,16) ;       -- Zeit ist dann sofort abgelaufen
         s_zahler_ADC        <= (others => '0');
         s_zahler_Regler     <= (others => '0');
         
      elsif (s_zahler_DAC < clk_time_DAC - 1 ) then  -- Erster Zaehler (Ausserer Zaehler)
         s_zahler_DAC        <= s_zahler_DAC + 1;
         clk_out_DAC         <= '0';
         clk_out_ADC         <= '0';
         clk_out_Regler      <= '0';
      else
         if (s_zahler_ADC    < ADC_schleifen - 1) then
            s_zahler_ADC     <= s_zahler_ADC + 1;
         else
            s_zahler_ADC     <= (others => '0');
            clk_out_ADC      <= '1';
         end if;
         
         if (s_zahler_Regler < Regler_schleifen - 1) then
            s_zahler_Regler  <= s_zahler_Regler + 1;
         else
            s_zahler_Regler  <= (others => '0');
            clk_out_Regler   <= '1';
         end if;
         
         s_zahler_DAC        <= (others => '0');
         clk_out_DAC         <= '1';
         STATE               <= IDLE;
      end if;
   
   end case;   
   END IF;      
END PROCESS ADC_Takt_p;


p_FIR_Filter_Takt : PROCESS (clk, reset)
BEGIN
   IF reset = '1' then
      s_zahler_signal3     <= (others=>'0');
      clk_out_FIR          <= '0'; 
   
   ELSIF (rising_edge(clk))THEN
	   clk_out_FIR      <= '0';
		
		if (Enable = '1') then
        if    (s_zahler_signal3 < to_unsigned(2*ckl_time_FIR-1,16)  )  then
           s_zahler_signal3 <= s_zahler_signal3 + 1;      
        else
           s_zahler_signal3 <= (others => '0');
		     clk_out_FIR      <= '1';
        end if;
		end if;
      
   END IF; 
END PROCESS p_FIR_Filter_Takt;

p_BandPass_FIR_Takt : PROCESS (clk, reset, Enable)
BEGIN
   IF reset = '1' OR Enable = '0' then  
      s_zahler_signal5        <= (others=>'0');
      clk_BandPass_FIR        <= '0'; 
   
   ELSIF rising_edge(clk) THEN
      clk_BandPass_FIR     <= '0';
      if    (s_zahler_signal5 < to_unsigned(2*clk_time_BandPass_FIR-1,16) )  then
         s_zahler_signal5     <= s_zahler_signal5 + 1;
      else
         s_zahler_signal5     <= (others => '0');
         clk_BandPass_FIR     <= '1';
      end if;
      
   END IF; 
END PROCESS p_BandPass_FIR_Takt;

END behavioral  ;
