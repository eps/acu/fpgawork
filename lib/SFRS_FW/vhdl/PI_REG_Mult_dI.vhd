--  Filename: PI_REG_Mult_dI.vhd
--  A.Wiest 03.03.2022
--
--  Beschreibung:
--      Dieser Regler hat die P- und I-Anteilsowie Anti-Wind-UP
--
--  Eing. Groessen:
--      clk                bspw. 100MHz system clock
--      reset              Berechnung neue starten, alles auf Null setzten
--      Eingang            Eingang der (soll - ist) Abweichung vom Typ signed(16Bit)
--
--      Data_In_Valid      Steigende Flanke  Triggert einmalig  die Berechnung an
--
--  Ausg. Groessen:
--      Ausgang            Reglerausgang: 16bit signed
--      Data_Out_Valid_PI  wenn die Daten berechnet sind = '1'

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


--Entity Declaration======================================================
ENTITY PI_REG_Mult_dI IS
   GENERIC( 
      Max_Eingang_diff   : integer := 5000;
      Max_Ausgang        : integer := 1000;
      Min_Ausgang        : integer := 100;
      Kp_Cut             : integer := 0;
      Ki_Cut             : integer := 0 ); -- LSB-Bits werden abgeschnitten

   PORT(
      clk, reset, Enable : in std_logic  ; -- Start der Berechnung

      EingangSoll        : in  signed   (15 downto 0);  -- Diff =  EingangSoll - EingangIst [-1..1]
      EingangIst         : in  signed   (15 downto 0);

      Kp                 : in  signed   (15 downto 0);
      Ki                 : in  signed   (15 downto 0);

      Ausgang            : out signed   (15 downto 0) := (others => '0');
      Ausgang_abs        : out signed   (15 downto 0) := (others => '0');
      
      Regelfehler        : out signed   (15 downto 0) := (others => '0');
      Data_Out_Valid_PI  : out std_logic              := '0' );

END PI_REG_Mult_dI;

--===========================================================================
ARCHITECTURE behavioral OF PI_REG_Mult_dI IS

-- Speicherung des Integralergebnises
SIGNAL s_Integral_aktuell : signed (32 downto 0):=(others=>'0');     -- 33 bit  (32Bit + 32Bit)
SIGNAL s_Integral_vorhin  : signed (32 downto 0):=(others=>'0');     -- 33 bit


-- Multiplikation:
-- e(t)* Ki  nicht korrigierte I-Anteil bei der Multiplikation
-- e(t)* Kp Ergebnise der Multiplikation
SIGNAL s_EingangDiff     : signed(15 downto 0) := (others => '0'); -- 16 bit

SIGNAL s_EingangSoll     : signed(15 downto 0) := (others => '0'); -- 16 bit
SIGNAL s_EingangIst      : signed(15 downto 0) := (others => '0'); -- 16 bit


SIGNAL s_Mult_INPUT1     : signed(15 downto 0) := (others => '0'); -- 16 bit
SIGNAL s_Mult_INPUT2     : signed(15 downto 0) := (others => '0'); -- 16 bit
SIGNAL s_Mult32          : signed(31 downto 0) := (others => '0'); -- 32 bit

SIGNAL s_PAnteil         : signed(15 downto 0) := (others => '0'); -- 16 bit
SIGNAL s_IAnteil         : signed(31 downto 0) := (others => '0'); -- 32 bit

-- Summe beider Anteile
SIGNAL s_Summe           : signed  (15 downto 0) := (others => '0'); 

TYPE STATE_PID_CONTROLLER  IS(EINGANG_SPEICHERN, DIFF_BILDEN, MULT1_VORBEREITEN, MULTIPLIKATION1,
                              TERM1_SAVE,
                              TERM2_MULT, TERM2_SAVE,
                              INTEGRAL_BILDEN,
                              ADDITION,
                              DATENRAUSGEBEN);

SIGNAL s_STATE : STATE_PID_CONTROLLER := EINGANG_SPEICHERN;


BEGIN
hauptprozess: process (clk, reset) -- Main process starts
   BEGIN
      if reset = '1' then
         s_EingangDiff      <=(others=>'0');
         s_Mult_INPUT1      <=(others=>'0');
         s_Mult_INPUT2      <=(others=>'0');
         s_Mult32           <=(others=>'0');

         s_PAnteil          <=(others=>'0');          -- Multiplikationsergebnise auf Null setzen
         s_IAnteil          <=(others=>'0');
         s_Summe            <=(others=>'0');          -- Summe des Mult. ergebnises
         s_Integral_aktuell <=(others=>'0');          -- Integralergebnise auf Null setzen
         s_Integral_vorhin  <=(others=>'0');

         Ausgang            <=(others=>'0');
         Ausgang_abs        <=(others=>'0');
         Data_Out_Valid_PI  <= '0';                  -- Berechnung ist nicht abgeschlossen
         s_STATE            <= EINGANG_SPEICHERN;    -- naechster Schritt

      elsif (clk'event and clk='1') THEN
         CASE  s_STATE  IS

   ----------------------------------------------------------------------------
   when EINGANG_SPEICHERN => --1. Takt  Checking the entered inputvalues
   ----------------------------------------------------------------------------
      Data_Out_Valid_PI  <= '0';
      IF (Enable = '1') then
         IF    EingangSoll  >  32766 THEN s_EingangSoll <= to_signed ( 32767, 16);
         ELSIF EingangSoll  < -32766 THEN s_EingangSoll <= to_signed (-32767, 16);    
         ELSE                             s_EingangSoll <= EingangSoll ; 
         END IF;
         
         IF    EingangIst  >  32766 THEN  s_EingangIst  <= to_signed ( 32767, 16);
         ELSIF Eingangist  < -32766 THEN  s_EingangIst  <= to_signed (-32767, 16);    
         ELSE                             s_EingangIst  <= EingangIst ; 
         END IF;
         s_STATE         <= DIFF_BILDEN;
      END IF;
 
   ----------------------------------------------------------------------------
   when DIFF_BILDEN => --1. Takt  Checking the entered inputvalues
   ----------------------------------------------------------------------------
      IF    (s_EingangSoll - s_EingangIst) >  Max_Eingang_diff -1 THEN s_EingangDiff <= to_signed ( Max_Eingang_diff, 16);
      ELSIF (s_EingangSoll - s_EingangIst) < -Max_Eingang_diff +1 THEN s_EingangDiff <= to_signed (-Max_Eingang_diff, 16);        
      ELSE                                                             s_EingangDiff <= s_EingangSoll - s_EingangIst;
      END IF;
      s_STATE         <= MULT1_VORBEREITEN;

   -----------------------------------------------------------------------------
    when MULT1_VORBEREITEN =>  --3. Takt (P-Anteil)
   -----------------------------------------------------------------------------
      s_Mult_INPUT1 <= s_EingangDiff;
      s_Mult_INPUT2 <= Kp;   
      s_STATE       <= MULTIPLIKATION1;

   -----------------------------------------------------------------------------
    when MULTIPLIKATION1 =>  --3. Takt (P-Anteil)
   -----------------------------------------------------------------------------
      s_Mult32     <= s_Mult_INPUT1 * s_Mult_INPUT2;  --16bit * 16bit =32bit
      s_STATE      <= TERM1_SAVE;

   ---------------------------------------------------------------------------------
   when TERM1_SAVE =>   -- 4. Takt
   ---------------------------------------------------------------------------------
      if    s_Mult32(31 downto 15) >  32766 then s_PAnteil <= to_signed( 32767,16);
      elsif s_Mult32(31 downto 15) < -32766 then s_PAnteil <= to_signed(-32767,16); 
      else  
         if    Kp_Cut = 1 then s_PAnteil <= s_Mult32(29 downto 14);
         elsif Kp_Cut = 2 then s_PAnteil <= s_Mult32(28 downto 13); 
         elsif Kp_Cut = 3 then s_PAnteil <= s_Mult32(27 downto 12); 
         elsif Kp_Cut = 4 then s_PAnteil <= s_Mult32(26 downto 11); 
         elsif Kp_Cut = 5 then s_PAnteil <= s_Mult32(25 downto 10);
         elsif Kp_Cut = 6 then s_PAnteil <= s_Mult32(24 downto 9);
         else                  s_PAnteil <= s_Mult32(30 downto 15);  
         end if;  
      end if;
      
      if Ki_Cut = 1 then
         if s_EingangDiff(15) = '1' then s_Mult_INPUT1   <= "1"  & s_EingangDiff(15 downto 1);
         else                            s_Mult_INPUT1   <= "0"  & s_EingangDiff(15 downto 1); end if;
      elsif Ki_Cut = 2 then
         if s_EingangDiff(15) = '1' then s_Mult_INPUT1   <= "11" & s_EingangDiff(15 downto 2);
         else                            s_Mult_INPUT1   <= "00" & s_EingangDiff(15 downto 2); end if;
      elsif Ki_Cut = 3 then
         if s_EingangDiff(15) = '1' then s_Mult_INPUT1   <= "111" & s_EingangDiff(15 downto 3);
         else                            s_Mult_INPUT1   <= "000" & s_EingangDiff(15 downto 3); end if;
      else
                                         s_Mult_INPUT1   <= s_EingangDiff(15 downto 0);
      end if;
           
      s_Mult_INPUT2   <= Ki;
      s_STATE         <= TERM2_MULT;

   -----------------------------------------------------------------------------
   when TERM2_MULT =>  --5. Takt
   -----------------------------------------------------------------------------
      s_Mult32  <= s_Mult_INPUT1 * s_Mult_INPUT2;  --16bit * 16bit =32bit
      s_STATE   <= TERM2_SAVE;

   ---------------------------------------------------------------------------------
   when TERM2_SAVE =>   --6. Multiplikation Ueberlauf
   ---------------------------------------------------------------------------------
      s_IAnteil <= s_Mult32;    
      s_STATE   <= INTEGRAL_BILDEN;

   --------------------------------------------------------------------------------
   when INTEGRAL_BILDEN =>   --7 
   -----------------------------------------------------------------------------
      s_Integral_aktuell <= s_IAnteil + s_Integral_vorhin;
      s_STATE            <= ADDITION;

   -----------------------------------------------------------------------------
   when ADDITION =>        --8. Takt
   -----------------------------------------------------------------------------
      if s_PAnteil + s_Integral_aktuell(32 downto 16) > Max_Ausgang       then  
         s_Summe <= to_signed(Max_Ausgang,16);
         if s_IAnteil < 0 then  s_integral_vorhin <= s_Integral_aktuell;  end if; -- sonst bleibt integral vorhin unveraendert

      elsif s_PAnteil + s_Integral_aktuell(32 downto 16) < - Max_Ausgang    then  
         s_Summe <= to_signed(-Max_Ausgang,16);
         if s_IAnteil > 0 then  s_integral_vorhin <= s_Integral_aktuell;  end if; -- sonst bleibt integral vorhin unveraendert

      else   
         s_Summe            <= s_PAnteil + s_Integral_aktuell(31 downto 16);
         s_Integral_vorhin  <= s_Integral_aktuell;          -- Neuer Integrallwert wird gespeichert

      end if;
      s_STATE <= DATENRAUSGEBEN;

   -----------------------------------------------------------------------------
   when DATENRAUSGEBEN=>     --9.Takt
   -----------------------------------------------------------------------------
      Ausgang            <=     s_Summe;    -- 16bit
      Ausgang_abs        <= ABS(s_Summe);
      Regelfehler        <= s_EingangDiff;
      s_STATE            <= EINGANG_SPEICHERN;
      Data_Out_Valid_PI  <= '1';                         -- Daten sind jetzt ausgerechnet

   END CASE;

   END IF;  --reset

   END process hauptprozess;

END ARCHITECTURE behavioral;
