--  Filename: SPI_sender_DR.vhd (VHDL File)
--  Authors : A.Wiest

--  25.10.2021, Created
--  sendet daten ueber SPI
-- Added new data output singal (DR 30.01.2024)
---============================================================================================================================================================
LIBRARY ieee;                                       
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;
USE     ieee.math_real.ALL;


ENTITY SPI_sender_DR IS    
                                                   -- Entity Declaration
   GENERIC (        
      takt             : integer  := 100_000_000;  -- 100 MHz
      SPI_takt         : integer  := 4;            -- Anzahl der clocks fuer ein Zustand (high or low) von dem SPI ckl
      SPI_bit_count    : integer  := 56 );         -- Anzahl der bits, die uebertragen werden         
 
   PORT (
      clk, reset       : IN  std_logic ;                                                    -- Control Signals   
      
      New_DATA_TO_Transfer : IN  std_logic;	
      
      DATA_TO_Transfer : IN  std_logic_vector (SPI_bit_count-1 downto 0):= (others=>'0');   -- Daten zum senden
      SPI_CLK_OUT      : OUT std_logic ;
      SPI_DO           : OUT std_logic);                                                    -- Datenausgang
        
END SPI_sender_DR;
--=================================================================================================================================================================

ARCHITECTURE behavioral OF SPI_sender_DR IS                                               -- Architecture Declaration 
 
   SIGNAL s_DATA_TO_Transfer       : std_logic_vector (SPI_bit_count - 1 downto 0)   := (others=>'0');
   SIGNAL s_SPI_output_counter     : integer     range SPI_bit_count + 8 downto 0    := 0; -- 1 Startbit + ...Bit Daten + 1 Parity Bit + 1Stop Bit  +5bit pause

   SIGNAL s_clk_counter            : integer     range SPI_takt*10 downto 0    := 0;
   
   SIGNAL s_SPI_Write_start        : std_logic  := '0';  
   SIGNAL s_Parity_out_spi         : std_logic  := '0';   
   
   SIGNAL s_SPI_ckl                : std_logic  := '0';  
   SIGNAL s_SPI_clk_counter        : integer     range SPI_bit_count + 8 downto 0    := 0;
   SIGNAL s_SPI_bit_count          : integer     range SPI_bit_count + 8 downto 0    := 0;
   
   TYPE STATE_SPI_DO IS(EINGANG_SPEICHERN, DATENRAUSGEBEN); --Zustaende fuer Data Out SPI
   SIGNAL s_SPI_DO_STATE :  STATE_SPI_DO := EINGANG_SPEICHERN;
	
   Type fsm_state is (WaitDt, SendDt);
   signal fsm :fsm_state;
  
BEGIN


--============================================================================
SPI_out :   process (s_SPI_ckl, reset, s_SPI_Write_start)             -- Daten ueber SPI Rausgeben

--SPI OUTPUT bits declaration
--s_Data_out_reg_spi (0)       = '1' start Bit
--s_Data_out_reg_spi (1 to 64) = '1' Data bits
--s_Data_out_reg_spi (65)      = '1' parity Bit
--s_Data_out_reg_spi (66)      = '1' stop Bit

BEGIN
   if (reset = '1') then                           -- Clock Synchronization
      s_SPI_output_counter <=  0;                  -- Counter of outputs Bits
      s_Parity_out_spi     <= '0';
      s_clk_counter        <=  0;
      
      SPI_DO               <= '0';
      s_DATA_TO_Transfer   <= (others =>'0');
      s_SPI_DO_STATE       <= EINGANG_SPEICHERN;
     
   elsif (rising_edge (s_SPI_ckl) AND s_SPI_ckl = '1') then            -- Writting the output data of SPI
      CASE  s_SPI_DO_STATE  IS
   
      ----------------------------------------------------------------------------------
      when EINGANG_SPEICHERN => --1. Takt
      ----------------------------------------------------------------------------------
         if  s_SPI_Write_start  = '1' then
            s_DATA_TO_Transfer      <= DATA_TO_Transfer; -- Daten zum Uebertragen
            s_SPI_DO_STATE          <= DATENRAUSGEBEN;
         end if;

      ----------------------------------------------------------------------------------
      when DATENRAUSGEBEN    => --2. Takt
      ----------------------------------------------------------------------------------
         if   (s_SPI_bit_count = 2) then
            SPI_DO                <= '1';                                                       -- write the Startbit
      
         elsif (s_SPI_bit_count > 2 AND s_SPI_bit_count < SPI_bit_count + 3) then                                           -- write the Output data    
            SPI_DO                <= s_DATA_TO_Transfer(s_SPI_bit_count - 3);        
                                                                                                                             -- Build of parity Bit
            if    (s_DATA_TO_Transfer(s_SPI_bit_count - 3) = '1' AND s_Parity_out_spi ='0') then  s_Parity_out_spi <= '1';   -- ungerade Anzahl der Bits   
            elsif (s_DATA_TO_Transfer(s_SPI_bit_count - 3) = '0' AND s_Parity_out_spi ='0') then  s_Parity_out_spi <= '0';   -- gerade Anzahl der Bits   
            elsif (s_DATA_TO_Transfer(s_SPI_bit_count - 3) = '0' AND s_Parity_out_spi ='1') then  s_Parity_out_spi <= '1'; 
            elsif (s_DATA_TO_Transfer(s_SPI_bit_count - 3) = '1' AND s_Parity_out_spi ='1') then  s_Parity_out_spi <= '0';   
            end if;
                 
         elsif(s_SPI_bit_count = SPI_bit_count + 3) then 
            SPI_DO                <= s_Parity_out_spi;             -- message of Parity error       
             
         elsif(s_SPI_bit_count = SPI_bit_count + 4) then        
            SPI_DO                <= '1';                          --Stop Bit         
         
         else
            SPI_DO                <= '0';  
           
            if  s_SPI_Write_start  = '0' then 
               s_SPI_DO_STATE        <= EINGANG_SPEICHERN;
               s_SPI_output_counter  <=  0;
               s_Parity_out_spi      <= '0';
            end if;
         end if;
   
      -----------------------------------------------------------------
      when others =>  
      -----------------------------------------------------------------
         s_SPI_DO_STATE  <= EINGANG_SPEICHERN;
      end case;
   
   end if; 
   
end process SPI_out; 

--============================================================================
CLK_out :   process (clk, reset)                  -- Clock fuer SPI wird erzeugt

BEGIN  
   if (reset = '1') then                           
      s_SPI_ckl           <= '0';                   
      s_SPI_clk_counter   <=  0 ; 
      s_SPI_bit_count     <=  0 ;
      s_SPI_Write_start   <= '0';
      SPI_CLK_OUT         <= '1'; 
      s_SPI_ckl           <= '1';
		
      fsm                 <= WaitDt;
      
   elsif (rising_edge (clk)) then     

     case fsm is
	 
	     when  WaitDt =>
		  
		    if (New_DATA_TO_Transfer='1') then
			   fsm<= SendDt;
			 end if;
		 
      	  when  SendDt =>
		  

			 
         if   (s_SPI_bit_count < SPI_bit_count + 6) then             --Verschiebung + Start + Perity + stop + wieder auf Null
            
            if   (s_SPI_clk_counter < SPI_takt)                 then  
               s_SPI_ckl          <= '1';
               SPI_CLK_OUT        <= '1';
               s_SPI_clk_counter  <=  s_SPI_clk_counter + 1 ;
            elsif (s_SPI_clk_counter < SPI_takt + SPI_takt - 1) then
               s_SPI_ckl          <= '0';
               SPI_CLK_OUT        <= '0';
               s_SPI_clk_counter  <=  s_SPI_clk_counter + 1 ;
            else
               s_SPI_Write_start  <= '0';
               s_SPI_clk_counter  <=  0;
               s_SPI_bit_count    <= s_SPI_bit_count + 1 ;
            end if;
         
         elsif (s_SPI_bit_count < SPI_bit_count + 8) then          -- noch drei Takte nach dem Stopbit
            if   (s_SPI_clk_counter < SPI_takt)      then  
               s_SPI_ckl          <= '1';
               SPI_CLK_OUT        <= '1';
               s_SPI_clk_counter  <=  s_SPI_clk_counter + 1 ;
            else
               s_SPI_clk_counter  <= 0;
               s_SPI_bit_count    <= s_SPI_bit_count + 1 ;

					

            end if;
            
         else
            s_SPI_bit_count <= 0;
				
				fsm<=WaitDt;

         end if;
         
         if   (s_SPI_bit_count > SPI_bit_count + 5) then      
            s_SPI_Write_start   <= '0';
         
         elsif (s_SPI_bit_count = 0) then
            s_SPI_Write_start   <= '0';
         else
            s_SPI_Write_start   <= '1';
         end if;
		when others=>
		      s_SPI_ckl           <= '0';                   
			s_SPI_clk_counter   <=  0 ; 
			s_SPI_bit_count     <=  0 ;
			s_SPI_Write_start   <= '0';
			SPI_CLK_OUT         <= '1'; 
			s_SPI_ckl           <= '1';
		
			fsm                 <= WaitDt;
		end case;

   end if; 
end process CLK_out; 

END behavioral;  
