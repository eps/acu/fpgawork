-----------------------------------------------------------------------------------------------------------------------------
-- File name: FForwardDAC_Driver.vhd                                                                                       --
--                                                                                                                         --
-- Author   : D.Rodomonti                                                                                                  --
-- Date     : 21/01/2016                                                                                                   --
--                                                                                                                         --
-- Comments : This component drives the FeedForward DAC card connected to the                                              --
--            ICM module via external data bus socket.                                                                     --
--            It generates the signals required by the DAC7731 chip in according                                           --
--            with the timing constraints provided by the DAC data-sheet.                                                  --
--            The DAC operational mode is hardware fixed to "Continuous Transfer                                           --
--            Control" (CSn='0' always activ)                                                                              --
--            NOTE!!: There is only one sample data input signal used for all four SDI data inputs, so                     --
--            be sure that all of them are aligned somehow to it.                                                          --
-- History  : Start up version 21/01/2016                                                                                  --
-----------------------------------------------------------------------------------------------------------------------------
--            Added mid-scale constant factor and data inversion.10/03/2016                                                --
-----------------------------------------------------------------------------------------------------------------------------
--       __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    --
-- clk _|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__ --
--             ___________                                                                         ___________             --
-- NewSDI ____|           |_______________________________________________________________________|           |___________ --
--                                                                                                                         --
-- intSDI                 X                    A                                                              X  B         --
--                                                                                                                         --
-- SDI                          X        A15            X ... X         A0                                          X  B15 --
--                                          ___________      _             ___________                                     --
-- SCLK                         ___________|           |__... |___________|           |___________________________________ --
--             __________________________________________ ..._                         ____________________________________--
-- LDAC                                                       |_______________________|                                    --
-----------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.ACU_package.all;

Entity FForwardDAC_Driver is
generic  
   (
      gFForwardDAC_DriverVersion      : real := 1.0
   );
  Port (
		Clock                   : in std_logic;                        -- Clock signal @100MHz
		Reset                   : in std_logic;                        -- Asynchronous reset signal active high
		NewDataChannel          : in std_logic;                        -- New Data to sample enable/pulse. Active high.
																							-- Note!! NewSDI signal frequency is up to 6 us (as the USI max speed!)
		DataChannel_0           : in  std_logic_vector(15 downto 0);   -- Digital data to convert channel 0.
		DataChannel_1           : in  std_logic_vector(15 downto 0);   -- Digital data to convert channel 1.
		DataChannel_2           : in  std_logic_vector(15 downto 0);   -- Digital data to convert channel 2.
		DataChannel_3           : in  std_logic_vector(15 downto 0);   -- Digital data to convert channel 3.
		
		DAC_Debug	            : in  std_logic_vector(1 downto 0);		-- Kann Offset und Verstärkung des DACs aendern
																							-- "00" Normaler DAC Mode; "10" DACInput = 00h "11" DAC Input="FFh; 
		SDI_0                   : out  std_logic;                      -- Serial Data In to DACnr 0.
		SDI_1                   : out  std_logic;                      -- Serial Data In to DACnr 1.
		SDI_2                   : out  std_logic;                      -- Serial Data In to DACnr 2.
		SDI_3                   : out  std_logic;                      -- Serial Data In to DACnr 3.
		LDAC                    : out  std_logic;                      -- Load data into DAC Internal Register. Active on the rising edge.
		SCLK                    : out  std_logic;                      -- DAC input clock signal @12,5MHz
		CS								: out  std_logic;  					   	-- CS (wird nicht benutzt, da der DAC nur mit LDAC laeuft)
		DAC_reset					: out  std_logic); 							-- Reset Signal für den DAC
		
End entity FForwardDAC_Driver;

Architecture beh of FForwardDAC_Driver is

-- The DAC chip DAC7731 has a pin dedicated to select the mid or the min-scale mode(RSTSEL).
-- This pin is not driven via FPGA but it is manually changeble. In the PCB card DAC_KARTE_V1 this pin has to be stick to zero, otherwise the card doesn't work.
-- For this reason mid-scale is always selected (0x8000 = 0) and the data in input has to be manipulated accordly with the mid_scale constant below.
-- The data in input are also inverted to compensate the inversion introduced by the DAC chip.

constant mid_scale                : unsigned(15 downto 0):= to_unsigned(32768,16);  -- 0x8000h

constant TCcntShift               : unsigned(6 downto 0) :=to_unsigned(127,7);
constant TCSDIShift               : unsigned(2 downto 0) :=to_unsigned(6,3);
constant TCSCLKLow                : unsigned(2 downto 0) :=to_unsigned(3,3);
constant TCLDAC_high              : unsigned(6 downto 0) :=to_unsigned(118,7);

--
--
-- Signals declaration
signal NewDataChannel_S0          : std_logic;                -- NewDataChannel Sampled signal.
signal NewDataChannel_S1          : std_logic;                -- NewDataChannel_S0 Sampled signal.
signal NewDataChannel_RE          : std_logic;                -- NewDataChannel rising edge.
signal DataChannel_x              : array16bits(3 downto 0);  -- Input Digital data to convert array.

type fsmstate is (Wait4Data, LoadData, ShiftData, AlarmState);
signal fsm_state, next_state : fsmstate;

signal cntShift                   : unsigned(6 downto 0);     -- Each bit shift action takes 8 Clock cycles, so one 16 bits data to shift takes 16*8=128 Clock cycles= 1,28us<<6us
signal cntShiftInt                : unsigned(6 downto 0);     -- Internal cntShift signal.
signal Alarm6usInt                : std_logic;                -- Alarm6us internal signal
signal Alarm6us                   : std_logic;                -- it is high if during a DAC cycle a new data to convert is available.
signal SDI_x                      : array16bits(3 downto 0);  -- Shift data register array

signal LDACInt                    : std_logic;                -- Load data into DAC Internal Register. Active on the rising edge.--
signal LDAC_ShiftReg              : std_logic_vector(7 downto 0);


-- RESET Signal
TYPE RESET_TYPE IS 	(RESET_ON, RESET_OFF);
SIGNAL s_Reset_STATE : RESET_TYPE 				:= RESET_ON;
constant c_reset_cnt : unsigned(15 downto 0) := to_unsigned(65535,16);
SIGNAL s_reset_cnt   : unsigned(15 downto 0);   -- Zähler für Reset - Signal


begin
-- Input sampling process
p_sampInputs:process(Reset,Clock)
begin
	if (Reset='1') then
		NewDataChannel_S0 <= '0';
		NewDataChannel_S1 <= '0';
		NewDataChannel_RE <= '0';
		DataChannel_x     <= (others=>(others=>'0'));
	
	elsif (Clock'event and Clock='1') then
		NewDataChannel_S0 <= NewDataChannel;
		NewDataChannel_S1 <= NewDataChannel_S0;
		NewDataChannel_RE <= NewDataChannel_S0 and not NewDataChannel_S1;
		
		if (DAC_Debug = "00" OR DAC_Debug = "01") then
			DataChannel_x(0)  <= std_logic_vector(unsigned(DataChannel_0) + mid_scale);
			DataChannel_x(1)  <= std_logic_vector(unsigned(DataChannel_1) + mid_scale);
			DataChannel_x(2)  <= std_logic_vector(unsigned(DataChannel_2) + mid_scale);
			DataChannel_x(3)  <= std_logic_vector(unsigned(DataChannel_3) + mid_scale);
		
		elsif DAC_Debug = "10" then
			DataChannel_x(0)  <= std_logic_vector(to_unsigned(0,16) + mid_scale);
			DataChannel_x(1)  <= std_logic_vector(to_unsigned(0,16) + mid_scale);
			DataChannel_x(2)  <= std_logic_vector(to_unsigned(0,16) + mid_scale);
			DataChannel_x(3)  <= std_logic_vector(to_unsigned(0,16) + mid_scale);
		
		else --"11"
			DataChannel_x(0)  <= std_logic_vector(to_unsigned(32767,16) + mid_scale);
			DataChannel_x(1)  <= std_logic_vector(to_unsigned(32767,16) + mid_scale);
			DataChannel_x(2)  <= std_logic_vector(to_unsigned(32767,16) + mid_scale);
			DataChannel_x(3)  <= std_logic_vector(to_unsigned(32767,16) + mid_scale);
		end if;
		
	end if;
end process;

--
-- NextState process
p_fState:process(fsm_state,NewDataChannel_RE,cntShift)
begin
	case fsm_state is
		when Wait4Data =>
			if (NewDataChannel_RE='1') then
				next_state <= ShiftData;
			else
				next_state <= Wait4Data;     
			end if;
		
		when ShiftData =>
			if (NewDataChannel_RE='1') then
				next_state <= AlarmState;
			else
				if (cntShift < TCcntShift) then
					next_state <= ShiftData;
				else
					next_state <= Wait4Data;	
				end if;
			end if;
		
		when AlarmState =>
			next_state <= Wait4Data;
		
		when others=>
			next_state <= Wait4Data;
	end case;
end process; 

--
-- SampleState process
p_xState:process(Reset,Clock)
begin
	if (Reset='1') then
		fsm_state   <= Wait4Data;
	elsif (Clock' event and Clock='1') then
		fsm_state  <= next_state;
	end if;
end process;


p_gState:process(fsm_state,cntShift,Alarm6us)
begin
  --
  -- Default values
  cntShiftInt  <= cntShift;
  Alarm6usInt  <= Alarm6us;
   
	case fsm_state is
		when Wait4Data =>
			cntShiftInt  <= (others=>'0');
			Alarm6usInt  <= '0';
		
		when ShiftData =>
			if (cntShift< TCcntShift) then
				cntShiftInt  <= cntShift+1;
			else
				cntShiftInt  <= (others=>'0');
			end if;
		
		when AlarmState =>
			Alarm6usInt  <= '1';
		when others=>
			cntShiftInt  <= (others=>'0');
			Alarm6usInt  <= '0';
	end case;
end process;


--
-- Output signal generation
p_OutGen: process (Reset,Clock)
begin
	if (Reset='1') then
		cntShift  <= (others=>'0');
		Alarm6us  <= '0';
	elsif (Clock'event and Clock='1') then
		cntShift  <= cntShiftInt;
		Alarm6us  <= Alarm6usInt;
	end if;
end process;

--
-- SDI_x process  
p_SDI_x: process(Reset,Clock)
begin
	if (Reset='1') then
		SDI_x  <=(others=>(others=>'0'));
	
	elsif (Clock'event and Clock='1') then
		if (NewDataChannel_RE='1') then
			SDI_x  <= DataChannel_x;
		elsif(cntShift(2 downto 0) > TCSDIShift) then
			for i in 0 to 3 loop
				SDI_x(i)  <= SDI_x(i)(14 downto 0) & SDI_x(i)(15);
			end loop;
		end if;
  end if;
end process;

--
-- SCLK process
p_SCLK: process(Reset,Clock)
begin
	if (Reset='1') then
		SCLK  <='0';
	elsif (Clock'event and Clock='1') then
		if (cntShift(2 downto 0) > TCSDIShift or cntShift(2 downto 0) < TCSCLKLow) then
			SCLK  <='0';
		else
			SCLK  <='1';
		end if;
	end if;
end process;


p_LDAC: process(Reset,Clock)-- LDAC process
begin
	if (Reset='1') then
		LDAC  <='1';
	elsif (Clock'event and Clock='1') then
		if (cntShift > TCLDAC_high ) then
			LDAC  <='0';
		else
			LDAC  <='1';
		end if;
	end if;
end process;


p_CS:process(Reset,Clock)-- SampleState process
begin
  if (Reset='1') then
		CS   <= '1';
	elsif (Clock' event and Clock='1') then
		CS   <= '0';
  end if;
end process;


p_DAC_reset: process(Reset,Clock)-- Reset Prozess
begin
   if (Reset='1') then
		DAC_reset		<= '1';
		s_Reset_STATE 	<= RESET_ON;

	elsif (Clock' event and Clock='1') then
		case s_Reset_STATE is
			
			when RESET_ON =>
				if (s_reset_cnt < c_reset_cnt) then
					s_reset_cnt  <= s_reset_cnt + 1;
				else
					DAC_reset	 <= '0';
					s_reset_cnt  <= (others=>'0');
					s_Reset_STATE<= RESET_OFF;
				end if;
			
			when RESET_OFF =>	
				Null;
			
			end case;
	end if;
end process p_DAC_reset;



SDI_0  <= SDI_x(0)(15);  --0
SDI_1  <= SDI_x(1)(15);  --1
SDI_2  <= SDI_x(2)(15);  --2 
SDI_3  <= SDI_x(3)(15);  --3

end beh;
