-- Filename: PWM_3L.vhd
-- geaendert am 24.01.2022, A. Wiest

-- Inhalt:   Berechnung der PWM Schema abhaengig von den Groessen:
--              ta, tb, tc, Sektor, Region
--
--           Eing. Groessen:
--              ta die Zeit fuer Schema 1
--              tb die Zeit fuer Schema 2
--              tc die Zeit fuer Schema 3 tc= Tschalt-(ta+tb)
--              alle Zeiten sind 16 Bit- breit und nur positiv
--              Sektor (1..6) des Raumzeigers
--              Region (1..6) aufteilung der Zonen
--
--              pwm_reset  '1'    Alle Schalter Aus
--              pwm_enable '0' -> Alle Schalter Aus
--              pwm_enable '1' -> PWM Ablauf darf fortlaufen(von Anfang An)
--
--            Ausg. Groessen:
--              S1u, S2u, S3u, S4u Schalter des U-Zweiges
--              S1v, S2v, S3v, S4v Schalter des V-Zweiges
--              S1w, S2w, S3w, S4w Schalter des W-Zweiges
--              Data_Out_Valid_pwm ='1' wenn ein Zyklus abgearbeitet ist
--
--            Parametern (GENERIC)
--              takt von FPGA
--              t_ueb0 - Uebergangszeit zwischen den Mustern
--              t_ueb1 - Uebergangszeit im Muster
--            PWM Muster wird beim Regionwechsel weiterhin nicht unterbrochen, was zu den Fehlern fuehren kann?
--            Beim Wechsel von einem Muster in das andere mit dem glichen Enfangsvektor, so gibt es kein Uebergangszeit gut?

LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY PWM_3L IS
   GENERIC(      takt                        : integer  := 100_000_000 ); -- Initialisierung: 100 MHz

   PORT( clk, reset, pwm_enable              : in std_logic :='0'       ; -- System-Eingaenge
      Korrekturtyp                           : in std_logic :='0'       ; -- =0 Grosse und kleine Vektoren werden angepasst ; =1 nur Grosser vektor wird angepasst

      ta, tb, tc, t_ueb                      : in unsigned (15 downto 0); -- Eingaenge (nur positive Werte), Uebergangszeit von einem Schaltzustand zum naechsten *10ns
      T_korr_zwV, T_korr_vV,T_korr_zwVa      : in unsigned (15 downto 0); -- Eingaenge (nur positive Werte), Uebergangszeit von einem Schaltzustand zum naechsten *10ns

      Sektor, Region                         : in unsigned (2  downto 0);

      iUPh_sign,iVPh_sign,iWPh_sign          : in std_logic :='0'       ; -- Stromvorzeichen '1' negativ

      Uce_TTiUPh, Uce_DTiUPh, Uce_DDiUPh     : in signed   (15 downto 0); -- Spannungsabfall am IGBT, hier nur pos. , U-Phase
      Uce_TTiVPh, Uce_DTiVPh, Uce_DDiVPh     : in signed   (15 downto 0); -- Spannungsabfall am IGBT, hier nur pos. , V-Phase
      Uce_TTiWPh, Uce_DTiWPh, Uce_DDiWPh     : in signed   (15 downto 0); -- Spannungsabfall am IGBT, hier nur pos. , W-Phase


      UCE1_Uph, UCE2_Uph, UCE3_Uph, UCE4_Uph : out signed  (15 downto 0); -- UCE Spannungen an dem U-Zweig in den vier positione
      UCE1_Vph, UCE2_Vph, UCE3_Vph, UCE4_Vph : out signed  (15 downto 0); -- UCE Spannungen an dem V-Zweig in den vier positionen
      UCE1_Wph, UCE2_Wph, UCE3_Wph, UCE4_Wph : out signed  (15 downto 0); -- UCE Spannungen an dem W-Zweig in den vier positionenn

      S1u, S2u, S3u, S4u,                                                 -- Ausgaenge (0 oder 1)
      S1v, S2v, S3v, S4v,
      S1w, S2w, S3w, S4w                     : out std_logic := '0';
      
      Data_Out_Valid_pwm                     : out std_logic := '0';
      Data_Out_Valid_Uce                     : out std_logic := '0') ;
END PWM_3L;

--============================================================================
ARCHITECTURE behavioral OF PWM_3L IS

SIGNAL s_t1,s_t2,s_t3,s_t4,s_t5,s_t6,s_t7       : unsigned (15 downto 0) := (others=>'0');
SIGNAL s_tueb                                   : unsigned (15 downto 0) := (others=>'0');
SIGNAL s_T_korr_zwV, s_T_korr_vV, s_T_korr_zwVa : unsigned (15 downto 0) := (others=>'0');
SIGNAL s_T_korr_zwV2, s_T_korr_zwV4             : unsigned (15 downto 0) := (others=>'0');

SIGNAL s_Tdiff2, s_Tdiff4, s_Tdiff8             : unsigned (15 downto 0) := (others=>'0');
SIGNAL s_Tdiff2zw, s_Tdiff4zw, s_Tdiff8zw       : unsigned (15 downto 0) := (others=>'0');
SIGNAL s_Tdiff4zwa, s_Tdiff8zwa                 : unsigned (15 downto 0) := (others=>'0');


SIGNAL s_counter_time1                          : unsigned (15 downto 0) := (others=>'0');
SIGNAL s_ta2,s_ta4, s_tb2, s_tc2, s_tc4         : unsigned (15 downto 0) := (others=>'0');    --Zeiten halb oder viertel, T2_ueb_korr = 2*T_ueb_korr
SIGNAL s_T_ueb_korr, s_T2_ueb_korr              : unsigned (15 downto 0) := (others=>'0');    -- T2_ueb_korr = 2*T_ueb_korr

SIGNAL s_Korrekturtyp                           : std_logic :='0';

---------------------------------------------------------------------------------------------------
SIGNAL s_UCE1_ph_Mult                           : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE2_ph_Mult                           : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE3_ph_Mult                           : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE4_ph_Mult                           : signed   (15 downto 0) := (others=>'0');

SIGNAL s_UCE1_Uph_Mult                          : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE2_Uph_Mult                          : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE3_Uph_Mult                          : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE4_Uph_Mult                          : signed   (15 downto 0) := (others=>'0');

SIGNAL s_UCE1_Vph_Mult                          : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE2_Vph_Mult                          : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE3_Vph_Mult                          : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE4_Vph_Mult                          : signed   (15 downto 0) := (others=>'0');

SIGNAL s_UCE1_Wph_Mult                          : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE2_Wph_Mult                          : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE3_Wph_Mult                          : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE4_Wph_Mult                          : signed   (15 downto 0) := (others=>'0');


SIGNAL s_UCE1_Uph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE2_Uph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE3_Uph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE4_Uph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');

SIGNAL s_UCE1_Vph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE2_Vph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE3_Vph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE4_Vph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');

SIGNAL s_UCE1_Wph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE2_Wph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE3_Wph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');
SIGNAL s_UCE4_Wph_Ergebniss                     : signed   (15 downto 0) := (others=>'0');

--------------------------------------------------------------------------------
TYPE T_MULTIPLIKATION_UCE IS (MULT1_VORBEREITEN,
                              MULT1,MULT2, MULT3, MULT4, MULT5, MULT6,MULT7,MULT8,MULT9,MULT10,MULT11,MULT12,MULT13,
                              MULT_OUT);

SIGNAL s_STATE_UCE : T_MULTIPLIKATION_UCE := MULT1_VORBEREITEN;

SIGNAL s_Mult                                   : signed   ( 31 downto 0) := (others=>'0');
SIGNAL s_Mult_Input1                            : signed   ( 15 downto 0) := (others=>'0');
SIGNAL s_Mult_Input2                            : signed   ( 15 downto 0) := (others=>'0');
---------------------------------------------------------------------------------------------------

SIGNAL S_v, S_v_korr, 
       s_Schalt1, s_Schalt1zw,
       s_Schalt2, s_Schalt2zw,
       s_Schalt3, s_Schalt3zw,
       s_Schalt4, s_Schalt4zw,
       s_Schalt5, s_Schalt5zw,
       s_Schalt6, s_Schalt6zw,
       s_Schalt7                                : std_logic_vector(11 downto 0):=(others=>'0'); -- Zustand aller 12 Vektoren


TYPE T_SEKTOR IS (Sek1, Sek2, Sek3, Sek4, Sek5, Sek6); -- Sektor1 bis 6
SIGNAL s_Sek : T_SEKTOR := Sek1;

TYPE T_REGION IS (Reg1, Reg2, Reg3, Reg4, Reg5, Reg6); -- Sektor1 bis 6
SIGNAL s_Reg : T_REGION := Reg1;

TYPE T_LAST_REGION_SEKTOR IS (Reg0_Sek0,                                                       -- Start aus dem Reset lles AUS
                              Reg1_Sek1, Reg2_Sek1, Reg3_Sek1, Reg4_Sek1, Reg5_Sek1, Reg6_Sek1,
                              Reg1_Sek2, Reg2_Sek2, Reg3_Sek2, Reg4_Sek2, Reg5_Sek2, Reg6_Sek2,
                              Reg1_Sek3, Reg2_Sek3, Reg3_Sek3, Reg4_Sek3, Reg5_Sek3, Reg6_Sek3,
                              Reg1_Sek4, Reg2_Sek4, Reg3_Sek4, Reg4_Sek4, Reg5_Sek4, Reg6_Sek4,
                              Reg1_Sek5, Reg2_Sek5, Reg3_Sek5, Reg4_Sek5, Reg5_Sek5, Reg6_Sek5,
                              Reg1_Sek6, Reg2_Sek6, Reg3_Sek6, Reg4_Sek6, Reg5_Sek6, Reg6_Sek6); -- Letzer Region und Sektor
SIGNAL s_LastRegSek : T_LAST_REGION_SEKTOR := Reg0_Sek0;



TYPE T_STATE_PWM IS (   EINGANG_SPEICHERN,                   --Schritte des PWM- Ablaufs
                        ZEITEN_VEKTOREN_BERECHNEN,
                        UEBERGAN0,
                        STELLUNG1, STELLUNG1_ZW,
                        STELLUNG2, STELLUNG2_ZW,
                        STELLUNG3, STELLUNG3_ZW,
                        STELLUNG4, STELLUNG4_ZW,
                        STELLUNG5, STELLUNG5_ZW,
                        STELLUNG6, STELLUNG6_ZW,
                        STELLUNG7);
SIGNAL  S_STATE : T_STATE_PWM := EINGANG_SPEICHERN;  --vorerst PWM Aus


BEGIN

----------------Ablauf der Schaltzustaende--------------------
p001: process(reset, clk, pwm_enable, S_v)
----------------------------------------
BEGIN

  --Asynchrone Zuweisug an die Ausgaenge
  S1u <= S_v(11); S2u <= S_v(10); S3u <= S_v(9); S4u <= S_v(8);
  S1v <= S_v(7) ; S2v <= S_v(6) ; S3v <= S_v(5); S4v <= S_v(4);
  S1w <= S_v(3) ; S2w <= S_v(2) ; S3w <= S_v(1); S4w <= S_v(0);


  IF (reset ='1' OR pwm_enable ='0')  then
   s_counter_time1<= (others=>'0');        -- Zaehler reseten

   s_t1           <= (others=>'0');
   s_t2           <= (others=>'0');
   s_t3           <= (others=>'0');
   s_t4           <= (others=>'0');
   s_t5           <= (others=>'0');
   s_t6           <= (others=>'0');
   s_t7           <= (others=>'0');

   s_ta2          <= (others=>'0');
   s_ta4          <= (others=>'0');
   s_tb2          <= (others=>'0');
   s_tc2          <= (others=>'0');
   s_tc4          <= (others=>'0');

   s_tueb         <= (others=>'0');
   s_Korrekturtyp <= '0';

   s_LastRegSek   <= Reg0_Sek0;
   s_Sek          <= Sek1;
   s_Reg          <= Reg1;
   S_v            <= b"0000_0000_0000";   -- Alle Schalter AUS
   S_STATE        <= EINGANG_SPEICHERN;   -- Erster Zustand

ELSE

   IF rising_edge(clk) THEN
      case S_STATE is
   ---------------------------------------------------------------------------------------
   when EINGANG_SPEICHERN =>                       --1. Uebernimmt die Daten am Eingang
   ---------------------------------------------------------------------------------------
   if (pwm_enable  = '1') then
      s_ta2           <= '0'  & ta(15 downto 1) ;  
      s_ta4           <= "00" & ta(15 downto 2) ;   
      s_tb2           <= '0'  & tb(15 downto 1) ;  
      s_tc2           <= '0'  & tc(15 downto 1) ;  
      s_tc4           <= "00" & tc(15 downto 2) ;  
      s_tueb          <= t_ueb ;
      
      s_Korrekturtyp  <= Korrekturtyp;
      
      s_T_korr_vV     <= T_korr_vV ;   
      s_T_korr_zwV    <= T_korr_zwV;  
      s_T_korr_zwV2   <= ('0'  &  T_korr_zwV(15 downto 1)) ;
      s_T_korr_zwV4   <= ("00" &  T_korr_zwV(15 downto 2)) ;
      
      
      s_T_korr_zwVa  <= T_korr_zwVa; -- Korrekturzeiten fuer Region 4,5 wirdaktuel nicht benutzt
      s_Tdiff4zwa    <= ('0' &  T_korr_zwVa(15 downto 1));
      s_Tdiff8zwa    <= ("00" & T_korr_zwVa(15 downto 2));
      
      
      if Korrekturtyp = '0' then                     -- Grosser und kleiner Vektor werden korrigiert
         if T_korr_vV > T_korr_zwV then s_Tdiff2 <= T_korr_vV  - T_korr_zwV ;
         else                           s_Tdiff2 <= T_korr_zwV - T_korr_vV  ; end if;
      
      else                                           -- nur grosser Vektor wird korrigiert
         s_Tdiff2   <=         T_korr_vV               ;
         s_Tdiff4   <= ('0' &  T_korr_vV(15 downto 1)) ;
         s_Tdiff8   <= ("00" & T_korr_vV(15 downto 2)) ;
      end if;


      -- s_Tdiff2 <=         T_korr_zwV               +          T_korr_vV              ;
      -- s_Tdiff4 <= ('0' &  T_korr_zwV(15 downto 1)) +  ('0' &  T_korr_vV(15 downto 1));
      -- s_Tdiff8 <= ("00" & T_korr_zwV(15 downto 2)) +  ("00" & T_korr_vV(15 downto 2));
      
 
      CASE Sektor IS when "001"   => s_Sek <= Sek1;
                     when "010"   => s_Sek <= Sek2;
                     when "011"   => s_Sek <= Sek3;
                     when "100"   => s_Sek <= Sek4;
                     when "101"   => s_Sek <= Sek5;
                     when "110"   => s_Sek <= Sek6;
                     when  others => s_Sek <= Sek1; --ungueltig "000", "111"
      END CASE;

      CASE Region IS when "001"   => s_Reg <= Reg1;
                     when "010"   => s_Reg <= Reg2;
                     when "011"   => s_Reg <= Reg3;
                     when "100"   => s_Reg <= Reg4;
                     when "101"   => s_Reg <= Reg5;
                     when "110"   => s_Reg <= Reg6;
                     when  others => s_Reg <= Reg1; --ungueltig "000", "111"

      END CASE;

      S_STATE   <= ZEITEN_VEKTOREN_BERECHNEN;
    end if;

      s_counter_time1    <= (others=>'0');
      Data_Out_Valid_pwm <= '0';
  
   ---------------------------------------------------------------------------------------
   when ZEITEN_VEKTOREN_BERECHNEN =>
   ---------------------------------------------------------------------------------------
   CASE s_Reg IS
      when Reg1 =>    -- Region 1,3,4 ohne korrekturen sind gleich
         s_t1 <= s_ta4 ;
         s_t2 <= s_tb2 ;
         s_t3 <= s_tc2 ;
         s_t4 <= s_ta2 ;
         s_t5 <= s_tc2 ;
         s_t6 <= s_tb2 ;
         s_t7 <= s_ta4 ;

      -----------------------------------------------------------------------------------------------------------------------------------------------------------
      when Reg3  =>  -- muss nicht Upos/Uneg Symmetrie korrigiert werden , da bereits symetrisch
 
         if s_Korrekturtyp = '1' then      
                                                s_t1 <= s_ta4 + s_Tdiff8        ;  -- Einstellung 70; nur grosser Vektor wir korrigiert
                                                s_t2 <= s_tb2 + s_Tdiff4        ;  -- Zwischenvektor            
            if s_tc2  > s_T_korr_vV       then  s_t3 <= s_tc2 - s_T_korr_vV     ;  else s_t3 <=(others=>'0'); end if;
                                                s_t4 <= s_ta2 + s_Tdiff4        ;
            if s_tc2  > s_T_korr_vV       then  s_t5 <= s_tc2 - s_T_korr_vV     ;  else s_t5 <=(others=>'0'); end if;
                                                s_t6 <= s_tb2 + s_Tdiff4        ;  -- Zwischenvektor                                 
                                                s_t7 <= s_ta4 + s_Tdiff8        ;   
         else
                                                s_t1 <= s_ta4 + s_T_korr_zwV4   ; --Einstellung 70 und 90; kleiner und grosser Vektor werden korrigiert
            if s_T_korr_vV > s_T_korr_zwV then  s_t2 <= s_tb2 + s_Tdiff2        ;  elsif s_tb2 > s_Tdiff2 then s_t2 <= s_tb2 - s_Tdiff2; else s_t2 <=(others=>'0'); end if;                           
                                    
            if s_tc2  > s_T_korr_vV       then  s_t3 <= s_tc2 - s_T_korr_vV     ;  else s_t3 <=(others=>'0'); end if;
                                                s_t4 <= s_ta2 + s_T_korr_zwV2   ; 
                                             
            if s_tc2  > s_T_korr_vV       then  s_t5 <= s_tc2 - s_T_korr_vV     ;  else s_t5 <=(others=>'0'); end if;
                                        
            if s_T_korr_vV > s_T_korr_zwV then  s_t6 <= s_tb2 + s_Tdiff2        ;  elsif s_tb2 > s_Tdiff2 then s_t6 <= s_tb2 - s_Tdiff2; else s_t6 <=(others=>'0'); end if;                                
                                                s_t7 <= s_ta4 + s_T_korr_zwV4   ; 
         end if;

--                                             s_t1 <= s_ta4 + s_Tdiff4        ;  -- diese Variante hat nichts gebracht
--         if s_tb2  > s_T_korr_zwV      then  s_t2 <= s_tb2 - s_T_korr_zwV    ;  else s_t2 <=(others=>'0'); end if;          
--         if s_tc2  > s_T_korr_vV       then  s_t3 <= s_tc2 - s_T_korr_vV     ;  else s_t3 <=(others=>'0'); end if;
--                                             s_t4 <= s_ta2 + s_Tdiff2        ;
--         if s_tc2  > s_T_korr_vV       then  s_t5 <= s_tc2 - s_T_korr_vV     ;  else s_t5 <=(others=>'0'); end if;
--         if s_tb2  > s_T_korr_zwV      then  s_t6 <= s_tb2 -  s_T_korr_zwV   ;  else s_t6 <=(others=>'0'); end if;                                
--                                             s_t7 <= s_ta4 + s_Tdiff4        ;  

--                                                                                -- funktioniert in die Falsche Richtung
--         if s_ta4  > s_T_korr_zwV4     then  s_t1 <= s_ta4 - s_T_korr_zwV4   ;  else s_t1 <=(others=>'0'); end if; 
--                                             s_t2 <= s_tb2 + s_Tdiff2        ;          
--         if s_tc2  > s_T_korr_vV       then  s_t3 <= s_tc2 - s_T_korr_vV     ;  else s_t3 <=(others=>'0'); end if;
--         if s_ta2  > s_T_korr_zwV2     then  s_t4 <= s_ta2 - s_T_korr_zwV2   ;  else s_t4 <=(others=>'0'); end if; 
--         if s_tc2  > s_T_korr_vV       then  s_t5 <= s_tc2 - s_T_korr_vV     ;  else s_t5 <=(others=>'0'); end if;
--                                             s_t6 <= s_tb2 + s_Tdiff2        ;                               
--         if s_ta4  > s_T_korr_zwV4     then  s_t7 <= s_ta4 - s_T_korr_zwV4   ;  else s_t7 <=(others=>'0'); end if;                                            

      -------------------------------------------------------------------------------------------------------------------------------------------------------------
      when Reg4 =>
--                                             s_t1 <= s_ta4 + s_Tdiff8zw      ; --diese Variante funktioniert nicht
--         if s_tb2  > s_T_korr_zwV      then  s_t2 <= s_tb2 - s_T_korr_zwV    ;  else s_t2 <=(others=>'0'); end if;
--                                             s_t3 <= s_tc2 + s_Tdiff4zw      ; 
--                                             s_t4 <= s_ta2 + s_Tdiff4zw      ;
--                                             s_t5 <= s_tc2 + s_Tdiff4zw      ;  
--         if s_tb2  > s_T_korr_zwV      then  s_t6 <= s_tb2 - s_T_korr_zwV    ;  else s_t6 <=(others=>'0'); end if;                     
--                                             s_t7 <= s_ta4 + s_Tdiff8zw      ;

                                                                                --diese Variante bringt nichts, wahrscheinlich auf einfach ohne korrektur fahren
          if s_ta4  > s_Tdiff8zwa        then  s_t1 <= s_ta4 - s_Tdiff8zwa     ; else s_t1 <=(others=>'0'); end if; 
                                               s_t2 <= s_tb2 + s_T_korr_zwVa   ; 
          if s_tc2  > s_Tdiff4zwa        then  s_t3 <= s_tc2 - s_Tdiff4zwa     ; else s_t3 <=(others=>'0'); end if; 
          if s_ta2  > s_Tdiff4zwa        then  s_t4 <= s_ta2 - s_Tdiff4zwa     ; else s_t4 <=(others=>'0'); end if; 
          if s_tc2  > s_Tdiff4zwa        then  s_t5 <= s_tc2 - s_Tdiff4zwa     ; else s_t5 <=(others=>'0'); end if;  
                                               s_t6 <= s_tb2 + s_T_korr_zwVa   ;                      
          if s_ta4  > s_Tdiff8zwa        then  s_t7 <= s_ta4 - s_Tdiff8zwa     ; else s_t7 <=(others=>'0'); end if; 

--                                             s_t1 <= s_ta4  ; 
--                                             s_t2 <= s_tb2  ;  
--                                             s_t3 <= s_tc2  ; 
--                                             s_t4 <= s_ta2  ;
--                                             s_t5 <= s_tc2  ;  
--                                             s_t6 <= s_tb2  ;                   
--                                             s_t7 <= s_ta4  ;

      -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      when Reg2 => -- Region 2,5,6 ohne korrekturen sind gleich
         s_t1 <= s_tc4 ;
         s_t2 <= s_ta2 ; 
         s_t3 <= s_tb2 ;
         s_t4 <= s_tc2 ;
         s_t5 <= s_tb2 ;
         s_t6 <= s_ta2 ; 
         s_t7 <= s_tc4 ; 

      ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      when Reg5 =>          
--                                             s_t1 <= s_tc4 + s_Tdiff8zw      ;
--                                             s_t2 <= s_ta2 + s_Tdiff4zw      ; 
--         if s_tb2  > s_T_korr_zwV      then  s_t3 <= s_tb2 - s_T_korr_zwV    ;  else s_t3 <=(others=>'0'); end if;
--                                             s_t4 <= s_tc2 + s_Tdiff4zw      ;
--         if s_tb2  > s_T_korr_zwV      then  s_t5 <= s_tb2 - s_T_korr_zwV    ;  else s_t5 <=(others=>'0'); end if;
--                                             s_t6 <= s_ta2 + s_Tdiff4zw      ;                      
--                                             s_t7 <= s_tc4 + s_Tdiff8zw      ;
                                                                                 -- diese Variante bringt nichts, wahrscheinlich auf einfach ohne korrektur fahren 
          if s_tc4  > s_Tdiff8zwa        then  s_t1 <= s_tc4 - s_Tdiff8zwa     ; else s_t1 <=(others=>'0'); end if; 
                                               s_t2 <= s_ta2 + s_T_korr_zwVa   ; 
          if s_tb2  > s_Tdiff4zwa        then  s_t3 <= s_tb2 - s_Tdiff4zwa     ; else s_t3 <=(others=>'0'); end if; 
          if s_tc2  > s_Tdiff4zwa        then  s_t4 <= s_tc2 - s_Tdiff4zwa     ; else s_t4 <=(others=>'0'); end if; 
          if s_tb2  > s_Tdiff4zwa        then  s_t5 <= s_tb2 - s_Tdiff4zwa     ; else s_t5 <=(others=>'0'); end if;  
                                               s_t6 <= s_ta2 + s_T_korr_zwVa   ;                      
          if s_tc4  > s_Tdiff8zwa        then  s_t7 <= s_tc4 - s_Tdiff8zwa     ; else s_t7 <=(others=>'0'); end if; 

--                                             s_t1 <= s_tc4   ;
--                                             s_t2 <= s_ta2   ; 
--                                             s_t3 <= s_tb2   ; 
--                                             s_t4 <= s_tc2   ;
--                                             s_t5 <= s_tb2   ;  
--                                             s_t6 <= s_ta2   ;                      
--                                             s_t7 <= s_tc4   ;
  
      -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      when Reg6 =>   -- muss nicht korrigiert werden fuer die Upos/Uneg Symmetrie 
      
      if s_Korrekturtyp = '1' then    
                                               s_t1 <= s_tc4 + s_Tdiff8        ; -- bei dieser Variente  ist nur vV korrektur
           if s_ta2  > s_T_korr_vV       then  s_t2 <= s_ta2 - s_T_korr_vV     ;  else s_t2 <=(others=>'0'); end if;
                                               s_t3 <= s_tb2 + s_Tdiff4        ;  -- Zwischenvektor
                                               s_t4 <= s_tc2 + s_Tdiff4        ;
                                               s_t5 <= s_tb2 + s_Tdiff4        ;  -- Zwischenvektor
           if s_ta2  > s_T_korr_vV       then  s_t6 <= s_ta2 - s_T_korr_vV     ;  else s_t6 <=(others=>'0'); end if;                                    
                                               s_t7 <= s_tc4 + s_Tdiff8        ;
         
      else          
                                               s_t1 <= s_tc4 + s_T_korr_zwV4   ;  -- oben zu flach?
          if s_ta2       > s_T_korr_vV   then  s_t2 <= s_ta2 - s_T_korr_vV     ;  else s_t2 <=(others=>'0'); end if;                        
          if s_T_korr_vV > s_T_korr_zwV  then  s_t3 <= s_tb2 + s_Tdiff2        ;  elsif s_tb2 > s_Tdiff2 then s_t3 <= s_tb2 - s_Tdiff2; else s_t3 <=(others=>'0'); end if; 
                                               s_t4 <= s_tc2 + s_T_korr_zwV2   ;  
         if s_T_korr_vV  > s_T_korr_zwV  then  s_t5 <= s_tb2 + s_Tdiff2        ;  elsif s_tb2 > s_Tdiff2 then s_t5 <= s_tb2 - s_Tdiff2; else s_t5 <=(others=>'0'); end if; 
         if s_ta2        > s_T_korr_vV   then  s_t6 <= s_ta2 - s_T_korr_vV     ;  else s_t6 <=(others=>'0'); end if;                                    
                                               s_t7 <= s_tc4 + s_T_korr_zwV4   ; 
      end if;
                                               
--                                             s_t1 <= s_tc4 + s_Tdiff4        ;  -- diese Variante hat nichts gebracht
--         if s_ta2  > s_T_korr_vV       then  s_t2 <= s_ta2 - s_T_korr_vV     ;  else s_t2 <=(others=>'0'); end if;
--         if s_tb2  > s_T_korr_zwV      then  s_t3 <= s_tb2 - s_T_korr_zwV    ;  else s_t3 <=(others=>'0'); end if;
--                                             s_t4 <= s_tc2 + s_Tdiff2        ;
--         if s_tb2  > s_T_korr_zwV      then  s_t5 <= s_tb2 - s_T_korr_zwV    ;  else s_t5 <=(others=>'0'); end if;
--         if s_ta2  > s_T_korr_vV       then  s_t6 <= s_ta2 - s_T_korr_vV     ;  else s_t6 <=(others=>'0'); end if;                                    
--                                             s_t7 <= s_tc4 + s_Tdiff4        ;
                                             
--                                                                                -- funktioniert in die Falsche Richtung
--         if s_tc4  > s_T_korr_zwV4     then  s_t1 <= s_tc4 - s_T_korr_zwV4   ;  else s_t1 <=(others=>'0'); end if;
--         if s_ta2  > s_T_korr_vV       then  s_t2 <= s_ta2 - s_T_korr_vV     ;  else s_t2 <=(others=>'0'); end if;
--                                             s_t3 <= s_tb2 + s_Tdiff2        ; 
--         if s_tc2  > s_T_korr_zwV2     then  s_t4 <= s_tc2 - s_T_korr_zwV2   ;  else s_t4 <=(others=>'0'); end if;
--                                             s_t5 <= s_tb2 + s_Tdiff2        ;  
--         if s_ta2  > s_T_korr_vV       then  s_t6 <= s_ta2 - s_T_korr_vV     ;  else s_t6 <=(others=>'0'); end if;                                    
--         if s_tc4  > s_T_korr_zwV4     then  s_t7 <= s_tc4 - s_T_korr_zwV4   ;  else s_t7 <=(others=>'0'); end if;  
         
   END CASE;
      

   IF (s_Reg = Reg1 OR s_Reg = Reg3 OR s_Reg = Reg4) then
      IF (s_Sek = Sek1) then
         CASE s_LastRegSek IS                -- uebergang in b"1100_0110_0110";                                        -- P  0  0   22
            when Reg0_Sek0                         => S_v <= b"1100_0110_0110"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"1100_0110_0110"; S_STATE <=STELLUNG1;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0100_0110_0010"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0100_0100_0110"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0000_0110_0110"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0100_0110_0100"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0100_0010_0110"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"1100_0100_0110"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0000_0110_0010"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0100_0100_0100"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0000_0010_0110"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"1100_0110_0100"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0100_0010_0010"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;

      ELSIF (s_Sek = Sek2) then
         CASE s_LastRegSek IS                -- uebergang in b"0110_0110_0011";                                        -- 0  0  N   12
            when Reg0_Sek0                         => S_v <= b"0110_0110_0011"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"0100_0110_0010"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0110_0110_0011"; S_STATE <=STELLUNG1;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0110_0100_0010"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0010_0110_0010"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0110_0110_0000"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0110_0010_0010"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"0100_0100_0010"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0010_0110_0011"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0110_0100_0000"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0010_0010_0010"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"0100_0110_0000"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0110_0010_0011"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;

      ELSIF (s_Sek = Sek3) then
         CASE s_LastRegSek IS                -- uebergang in b"0110_1100_0110";                                        -- 0  P  0   16
            when Reg0_Sek0                         => S_v <= b"0110_1100_0110"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"0100_0100_0110"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0110_0110_0010"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0110_1100_0110"; S_STATE <=STELLUNG1;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0010_0100_0110"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0110_0100_0100"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0110_0000_0110"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"0100_1100_0110"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0010_0100_0010"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0110_1100_0100"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0010_0000_0110"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"0100_0100_0100"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0110_0000_0010"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;

      ELSIF (s_Sek = Sek4) then
         CASE s_LastRegSek IS                -- uebergang in b"0011_0110_0110";                                        -- N  0  0   4
            when Reg0_Sek0                         => S_v <= b"0011_0110_0110"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"0000_0110_0110"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0010_0110_0010"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0010_0100_0110"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0011_0110_0110"; S_STATE <=STELLUNG1;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0010_0110_0100"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0010_0010_0110"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"0000_0100_0110"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0011_0110_0010"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0010_0100_0100"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0011_0010_0110"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"0000_0110_0100"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0010_0010_0010"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;

      ELSIF (s_Sek = Sek5) then
         CASE s_LastRegSek IS                -- uebergang in b"0110_0110_1100";                                        -- 0  0  P   14
            when Reg0_Sek0                         => S_v <= b"0110_0110_1100"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"0100_0110_0100"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0110_0110_0000"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0110_0100_0100"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0010_0110_0100"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0110_0110_1100"; S_STATE <=STELLUNG1;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0110_0010_0100"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"0100_0100_0100"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0010_0110_0000"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0110_0100_1100"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0010_0010_0100"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"0100_0110_1100"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0110_0010_0000"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;

      ELSE --(s_Sek = Sek6)
         CASE s_LastRegSek IS                -- uebergang in b"0110_0011_0110";                                        -- 0  N  0   10
            when Reg0_Sek0                         => S_v <= b"0110_0011_0110"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"0100_0010_0110"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0110_0010_0010"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0110_0000_0110"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0010_0010_0110"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0110_0010_0100"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0110_0011_0110"; S_STATE <=STELLUNG1;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"0100_0000_0110"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0010_0010_0010"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0110_0000_0100"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0010_0011_0110"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"0100_0010_0100"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0110_0011_0010"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;
      END IF;

   ELSE --(s_Reg = Reg2 OR s_Reg = Reg5 OR s_Reg = Reg6) then
      IF (s_Sek = Sek1) then
         CASE s_LastRegSek IS                 -- uebergang in b"1100_1100_0110";                                        -- P  P  0   25
             when Reg0_Sek0                         => S_v <= b"1100_1100_0110"; S_STATE <=STELLUNG1;
             when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"1100_0100_0110"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
             when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0100_0100_0010"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
             when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0100_1100_0110"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
             when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0000_0100_0110"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
             when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0100_0100_0100"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
             when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0100_0000_0110"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

             when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"1100_1100_0110"; S_STATE <=STELLUNG1;-- b"1100_1100_0110"  P  P  0   25
             when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0000_0100_0010"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
             when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0100_1100_0100"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
             when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0000_0000_0110"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
             when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"1100_0100_0100"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
             when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0100_0000_0010"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;

      ELSIF (s_Sek = Sek2) then
         CASE s_LastRegSek IS                -- uebergang in b"0011_0110_0011";                                        -- N  0  N   3
            when Reg0_Sek0                         => S_v <= b"0011_0110_0011"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"0000_0110_0010"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0010_0110_0011"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0010_0100_0010"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0011_0110_0010"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0010_0110_0000"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0010_0010_0010"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"0000_0100_0010"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0011_0110_0011"; S_STATE <=STELLUNG1;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0010_0100_0000"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0011_0010_0010"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"0000_0110_0000"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0010_0010_0011"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;

      ELSIF (s_Sek = Sek3) then
         CASE s_LastRegSek IS                -- uebergang in b"0110_1100_1100";                                        -- 0  P  P   17
            when Reg0_Sek0                         => S_v <= b"0110_1100_1100"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"0100_0100_0100"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0110_0100_0000"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0110_1100_0100"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0010_0100_0100"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0110_0100_1100"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0110_0000_0100"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"0100_1100_0100"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0010_0100_0000"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0110_1100_1100"; S_STATE <=STELLUNG1;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0010_0000_0100"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"0100_0100_1100"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0110_0000_0000"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;

      ELSIF (s_Sek = Sek4) then
         CASE s_LastRegSek IS                -- uebergang in b"0011_0011_0110";                                        -- N  N  0   1
            when Reg0_Sek0                         => S_v <= b"0011_0011_0110"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"0000_0010_0110"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0010_0010_0010"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0010_0000_0110"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0011_0010_0110"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0010_0010_0100"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0010_0011_0110"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"0000_0000_0110"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0011_0010_0010"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0010_0000_0100"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0011_0011_0110"; S_STATE <=STELLUNG1;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"0000_0010_0100"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0010_0011_0010"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;

      ELSIF (s_Sek = Sek5) then
         CASE s_LastRegSek IS                -- uebergang in b"1100_0110_1100";                                        -- P  0  P   23
            when Reg0_Sek0                         => S_v <= b"1100_0110_1100"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"1100_0110_0100"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0100_0110_0000"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0100_0100_0100"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0000_0110_0100"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0100_0110_1100"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0100_0010_0100"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"1100_0100_0100"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0000_0110_0000"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0100_0100_1100"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0000_0010_0100"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"1100_0110_1100"; S_STATE <=STELLUNG1;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0100_0010_0000"; S_STATE <=UEBERGAN0;-- b"0110_0011_0011"  0  N  N   9
         END CASE;

      ELSE --(s_Sek = Sek6)
         CASE s_LastRegSek IS                -- uebergang in b"0110_0011_0011";                                        -- 0  N  N   9
            when Reg0_Sek0                         => S_v <= b"0110_0011_0011"; S_STATE <=STELLUNG1;
            when Reg1_Sek1 | Reg3_Sek1 | Reg4_Sek1 => S_v <= b"0100_0010_0010"; S_STATE <=UEBERGAN0;-- b"1100_0110_0110"  P  0  0   22
            when Reg1_Sek2 | Reg3_Sek2 | Reg4_Sek2 => S_v <= b"0110_0010_0011"; S_STATE <=UEBERGAN0;-- b"0110_0110_0011"  0  0  N   12
            when Reg1_Sek3 | Reg3_Sek3 | Reg4_Sek3 => S_v <= b"0110_0000_0010"; S_STATE <=UEBERGAN0;-- b"0110_1100_0110"  0  P  0   16
            when Reg1_Sek4 | Reg3_Sek4 | Reg4_Sek4 => S_v <= b"0010_0010_0010"; S_STATE <=UEBERGAN0;-- b"0011_0110_0110"  N  0  0   4
            when Reg1_Sek5 | Reg3_Sek5 | Reg4_Sek5 => S_v <= b"0110_0010_0000"; S_STATE <=UEBERGAN0;-- b"0110_0110_1100"  0  0  P   14
            when Reg1_Sek6 | Reg3_Sek6 | Reg4_Sek6 => S_v <= b"0110_0011_0010"; S_STATE <=UEBERGAN0;-- b"0110_0011_0110"  0  N  0   10

            when Reg2_Sek1 | Reg5_Sek1 | Reg6_Sek1 => S_v <= b"0100_0000_0010"; S_STATE <=UEBERGAN0;-- b"1100_1100_0110"  P  P  0   25
            when Reg2_Sek2 | Reg5_Sek2 | Reg6_Sek2 => S_v <= b"0010_0010_0011"; S_STATE <=UEBERGAN0;-- b"0011_0110_0011"  N  0  N   3
            when Reg2_Sek3 | Reg5_Sek3 | Reg6_Sek3 => S_v <= b"0110_0000_0000"; S_STATE <=UEBERGAN0;-- b"0110_1100_1100"  0  P  P   17
            when Reg2_Sek4 | Reg5_Sek4 | Reg6_Sek4 => S_v <= b"0010_0011_0010"; S_STATE <=UEBERGAN0;-- b"0011_0011_0110"  N  N  0   1
            when Reg2_Sek5 | Reg5_Sek5 | Reg6_Sek5 => S_v <= b"0100_0010_0000"; S_STATE <=UEBERGAN0;-- b"1100_0110_1100"  P  0  P   23
            when Reg2_Sek6 | Reg5_Sek6 | Reg6_Sek6 => S_v <= b"0110_0011_0011"; S_STATE <=STELLUNG1;-- b"0110_0011_0011"  0  N  N   9
         END CASE;
      END IF;
   END IF;


  CASE s_Reg IS        --Zu den Sektoren & Regionen werden fuer die Multiplikation die Zeiten ausgesucht
   when Reg1 | Reg3 | Reg4 =>
      s_UCE1_ph_Mult <= signed(std_logic_vector('0'& ta(15 downto 1)));   -- ta_2 (1)
      s_UCE2_ph_Mult <= signed(std_logic_vector(tb))                  ;   -- tb   (2)
      s_UCE3_ph_Mult <= signed(std_logic_vector(tc))                  ;   -- tc   (3)
      s_UCE4_ph_Mult <= signed(std_logic_vector('0'& ta(15 downto 1)));   -- ta_2 (4)

   when others =>
      s_UCE1_ph_Mult <= signed(std_logic_vector('0'& tc(15 downto 1)));   -- tc_2 (1)
      s_UCE2_ph_Mult <= signed(std_logic_vector(ta))                  ;   -- ta   (2)
      s_UCE3_ph_Mult <= signed(std_logic_vector(tb))                  ;   -- tb   (3)
      s_UCE4_ph_Mult <= signed(std_logic_vector('0'& tc(15 downto 1)));   -- tc_2 (4)
  END CASE;


CASE s_Reg IS               --Zu den Sektoren & Regionen werden Vektoren ausgesucht
   when Reg1 => --Region 1
      CASE s_Sek IS
         when Sek1  => s_LastRegSek<= Reg1_Sek1;--Region 1, Sektor 1 (A) ---------------
                                                -- Erzeuger                                  Verbaucher
            s_Schalt1   <= b"1100_0110_0110" ;  -- P  0  0   22  t1 = ta_4                   P  0  0   ta_4   Alle p2 werden zu P
            s_Schalt1zw <= b"0100_0110_0110" ;  -- p2 0  0                                   P  0  0               n1 werden zu N
            s_Schalt2   <= b"0110_0110_0110" ;  -- 0  0  0   13  t2 = tb_2                   0  0  0   tb_2
            s_Schalt2zw <= b"0110_0110_0010" ;  -- 0  0  n1                                  0  0  N
            s_Schalt3   <= b"0110_0110_0011" ;  -- 0  0  N   12  t3 = tc_2                   0  0  N   tb_2
            s_Schalt3zw <= b"0110_0010_0011" ;  -- 0  n1 N                                   0  N  N
            s_Schalt4   <= b"0110_0011_0011" ;  -- 0  N  N   9   t4 = ta_2                   0  N  N   ta_4
            s_Schalt4zw <= b"0110_0010_0011" ;  -- 0  n1 N                                   0  N  N
            s_Schalt5   <= b"0110_0110_0011" ;  -- 0  0  N   12  t5 = tc_2                   0  0  N   tb_2
            s_Schalt5zw <= b"0110_0110_0010" ;  -- 0  0  n1                                  0  0  N
            s_Schalt6   <= b"0110_0110_0110" ;  -- 0  0  0   13  t6 = tb_2                   0  0  0   tb_2
            s_Schalt6zw <= b"0100_0110_0110" ;  -- p2 0  0                                   P  0  0
            s_Schalt7   <= b"1100_0110_0110" ;  -- P  0  0   22  t7 = ta_4                   P  0  0   ta_4

            -- positiver Strom ist aus dem Umrichter raus fliessender Strom
            -- TT zwei Transistoren  in Reihe
            -- DD zwei Dioden        in Reihe
            -- DT Transistor + Diode in Reihe

            -- bei P : TT Strom positiv, DD Strom neg
            -- bei N : DD Strom positiv, TT Strom neg
            -- bei 0 : Strom egal        DT


            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_TTiUPh;            -- (1 Position) Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;            -- (2 Position)
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;            -- (3)
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;            -- (4)
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DDiUPh)+1;         -- (1) Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;         -- (2)
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;         -- (3)
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if; -- (4)
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;     -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DDiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1;  -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_TTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DDiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_TTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek2  =>   s_LastRegSek<= Reg1_Sek2;--Region 1, Sektor 2 (B)---------------
                                                                                     --     Verbraucher
            s_Schalt1   <= b"0110_0110_0011" ;  -- 0  0  N   12 ta_4                        n1  n1  N     ta_4
            s_Schalt1zw <= b"0110_0110_0010" ;  -- 0  0  n1                                 n1  n1  N
            s_Schalt2   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2                        n1  n1 (p2)k  tb_2 
            s_Schalt2zw <= b"0110_0100_0110" ;  -- 0  p2 0                                  n1  Pk (p2)
            s_Schalt3   <= b"0110_1100_0110" ;  -- 0  P  0   16 tc_2                        n1  P  (p2)   tc_2
            s_Schalt3zw <= b"0100_1100_0110" ;  -- p2 P  0                                  Pk  P  (p2)
            s_Schalt4   <= b"1100_1100_0110" ;  -- P  P  0   25 ta_2                        P   P  (p2)   ta_2
            s_Schalt4zw <= b"0100_1100_0110" ;  -- p2 P  0                                  Pk  P  (p2)
            s_Schalt5   <= b"0110_1100_0110" ;  -- 0  P  0   16 tc_2                        n1  P  (p2)   tc_2 
            s_Schalt5zw <= b"0110_0100_0110" ;  -- 0  p2 0                                  n1  Pk (p2)
            s_Schalt6   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2                        n1  n1 (p2)   tb_2
            s_Schalt6zw <= b"0110_0110_0010" ;  -- 0  0  n1                                 n1  n1  Nk
            s_Schalt7   <= b"0110_0110_0011" ;  -- 0  0  N   12 ta_4                        n1  n1  N     ta_4


            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_TTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DDiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_TTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DDiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DDiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_TTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------
         
         when Sek3  =>   s_LastRegSek<= Reg1_Sek3;--Region 1, Sektor 3 (C)---------------
            s_Schalt1   <= b"0110_1100_0110" ;  -- 0  P  0   16 ta_4
            s_Schalt1zw <= b"0110_0100_0110" ;  -- 0  p2 0
            s_Schalt2   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt2zw <= b"0010_0110_0110" ;  -- n1 0  0
            s_Schalt3   <= b"0011_0110_0110" ;  -- N  0  0   4  tc_2
            s_Schalt3zw <= b"0011_0110_0010" ;  -- N  0  n1
            s_Schalt4   <= b"0011_0110_0011" ;  -- N  0  N   3  ta_2
            s_Schalt4zw <= b"0011_0110_0010" ;  -- N  0  n1
            s_Schalt5   <= b"0011_0110_0110" ;  -- N  0  0   4  tc_2
            s_Schalt5zw <= b"0010_0110_0110" ;  -- n1 0  0
            s_Schalt6   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2 
            s_Schalt6zw <= b"0110_0100_0110" ;  -- 0  p2 0
            s_Schalt7   <= b"0110_1100_0110" ;  -- 0  P  0   16 ta_4


            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DDiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_TTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_TTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DDiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DDiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_TTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek4  =>   s_LastRegSek<= Reg1_Sek4; --Region 1, Sektor 4 (D)---------------
            s_Schalt1   <= b"0011_0110_0110" ;  -- N  0  0   4  ta_4
            s_Schalt1zw <= b"0010_0110_0110" ;  -- n1 0  0
            s_Schalt2   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt2zw <= b"0110_0110_0100" ;  -- 0  0  p2
            s_Schalt3   <= b"0110_0110_1100" ;  -- 0  0  P   14 tc_2 
            s_Schalt3zw <= b"0110_0100_1100" ;  -- 0  p2 P
            s_Schalt4   <= b"0110_1100_1100" ;  -- 0  P  P   17 ta_2
            s_Schalt4zw <= b"0110_0100_1100" ;  -- 0  p2 P
            s_Schalt5   <= b"0110_0110_1100" ;  -- 0  0  P   14 tc_2 
            s_Schalt5zw <= b"0110_0110_0100" ;  -- 0  0  p2
            s_Schalt6   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2 
            s_Schalt6zw <= b"0010_0110_0110" ;  -- n1 0  0
            s_Schalt7   <= b"0011_0110_0110" ;  -- N  0  0   4  ta_4

        
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DDiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_TTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_TTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DDiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_TTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DDiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek5  =>   s_LastRegSek<= Reg1_Sek5; --Region 1, Sektor 5 (E)---------------
            s_Schalt1   <= b"0110_0110_1100" ;  -- 0  0  P   14 ta_4
            s_Schalt1zw <= b"0110_0110_0100" ;  -- 0  0  p2
            s_Schalt2   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2 
            s_Schalt2zw <= b"0110_0010_0110" ;  -- 0  n1 0
            s_Schalt3   <= b"0110_0011_0110" ;  -- 0  N  0   10 tc_2 
            s_Schalt3zw <= b"0010_0011_0110" ;  -- n1 N  0
            s_Schalt4   <= b"0011_0011_0110" ;  -- N  N  0   1  ta_2
            s_Schalt4zw <= b"0010_0011_0110" ;  -- n1 N  0
            s_Schalt5   <= b"0110_0011_0110" ;  -- 0  N  0   10 tc_2 
            s_Schalt5zw <= b"0110_0010_0110" ;  -- 0  n1 0
            s_Schalt6   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2 
            s_Schalt6zw <= b"0110_0110_0100" ;  -- 0  0  p2
            s_Schalt7   <= b"0110_0110_1100" ;  -- 0  0  P   14 ta_4


            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DDiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_TTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DDiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_TTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_TTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DDiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek6  =>   s_LastRegSek<= Reg1_Sek6; --Region 1, Sektor 6 (F)---------------
          
            s_Schalt1   <= b"0110_0011_0110" ;  -- 0  N  0   10 ta_4
            s_Schalt1zw <= b"0110_0010_0110" ;  -- 0  n1 0
            s_Schalt2   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2 
            s_Schalt2zw <= b"0100_0110_0110" ;  -- p2 0  0
            s_Schalt3   <= b"1100_0110_0110" ;  -- P  0  0   22 tc_2 
            s_Schalt3zw <= b"1100_0110_0100" ;  -- P  0  p2
            s_Schalt4   <= b"1100_0110_1100" ;  -- P  0  P   23 ta_2
            s_Schalt4zw <= b"1100_0110_0100" ;  -- P  0  p2
            s_Schalt5   <= b"1100_0110_0110" ;  -- P  0  0   22 tc_2
            s_Schalt5zw <= b"0100_0110_0110" ;  -- p2 0  0
            s_Schalt6   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt6zw <= b"0110_0010_0110" ;  -- 0  n1 0
            s_Schalt7   <= b"0110_0011_0110" ;  -- 0  N  0   10 ta_4

          
         
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_TTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DDiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DDiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_TTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_TTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DDiWPh)+1; end if;
            -------------------------------------------------------------------
         END CASE;



   when Reg2 => --Region 2
      CASE s_Sek IS
         when Sek1  => s_LastRegSek<= Reg2_Sek1; --Region 2, Sektor 1 (A)---------------
                                                -- Erzeueger                              Verbraucher
            s_Schalt1   <= b"1100_1100_0110" ;  -- P  P  0   25 tc_4                       P  P  0  tc_4 - T_ueb_korr
            s_Schalt1zw <= b"1100_0100_0110" ;  -- P  p2 0                                 P  P  0
            s_Schalt2   <= b"1100_0110_0110" ;  -- P  0  0   22 ta_2                       P  0  0  ta_2 - T_ueb_korr
            s_Schalt2zw <= b"0100_0110_0110" ;  -- p2 0  0                                 P  0  0
            s_Schalt3   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2                       0  0  0  tb_2
            s_Schalt3zw <= b"0110_0110_0010" ;  -- 0  0  n1                                0  0  N
            s_Schalt4   <= b"0110_0110_0011" ;  -- 0  0  N   12 tc_2                       0  0  N  tc_2  - 2*T_ueb_korr
            s_Schalt4zw <= b"0110_0110_0010" ;  -- 0  0  n1
            s_Schalt5   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt5zw <= b"0100_0110_0110" ;  -- p2 0  0
            s_Schalt6   <= b"1100_0110_0110" ;  -- P  0  0   22 ta_2
            s_Schalt6zw <= b"1100_0100_0110" ;  -- P  p2 0
            s_Schalt7   <= b"1100_1100_0110" ;  -- P  P  0   25 tc_4

       
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_TTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DDiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_TTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DDiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DDiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_TTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek2  =>   s_LastRegSek<= Reg2_Sek2; --Region 2, Sektor 2 (B)---------------
          
            s_Schalt1   <= b"0011_0110_0011" ;  -- N  0  N   3  tc_4
            s_Schalt1zw <= b"0010_0110_0011" ;  -- n1 0  N
            s_Schalt2   <= b"0110_0110_0011" ;  -- 0  0  N   12 ta_2
            s_Schalt2zw <= b"0110_0110_0010" ;  -- 0  0  n1
            s_Schalt3   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt3zw <= b"0110_0100_0110" ;  -- 0  p2 0
            s_Schalt4   <= b"0110_1100_0110" ;  -- 0  P  0   16 tc_2
            s_Schalt4zw <= b"0110_0100_0110" ;  -- 0  p2 0
            s_Schalt5   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt5zw <= b"0110_0110_0010" ;  -- 0  0  n1
            s_Schalt6   <= b"0110_0110_0011" ;  -- 0  0  N   12 ta_2
            s_Schalt6zw <= b"0010_0110_0011" ;  -- n1 0  N
            s_Schalt7   <= b"0011_0110_0011" ;  -- N  0  N   3  tc_4


            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DDiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_TTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_TTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DDiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DDiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_TTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek3  =>   s_LastRegSek<= Reg2_Sek3; --Region 2, Sektor 3 (C)---------------
         
            s_Schalt1   <= b"0110_1100_1100" ;  -- 0  P  P   17 tc_4
            s_Schalt1zw <= b"0110_1100_0100" ;  -- 0  P  p2
            s_Schalt2   <= b"0110_1100_0110" ;  -- 0  P  0   16 ta_2
            s_Schalt2zw <= b"0110_0100_0110" ;  -- 0  p2 0
            s_Schalt3   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt3zw <= b"0010_0110_0110" ;  -- n1 0  0
            s_Schalt4   <= b"0011_0110_0110" ;  -- N  0  0   4  tc_2
            s_Schalt4zw <= b"0010_0110_0110" ;  -- n1 0  0
            s_Schalt5   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt5zw <= b"0110_0100_0110" ;  -- 0  p2 0
            s_Schalt6   <= b"0110_1100_0110" ;  -- 0  P  0   16 ta_2
            s_Schalt6zw <= b"0110_1100_0100" ;  -- 0  P  p2
            s_Schalt7   <= b"0110_1100_1100" ;  -- 0  P  P   17 tc_4

           

            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DDiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_TTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_TTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DDiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_TTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DDiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek4  =>   s_LastRegSek<= Reg2_Sek4; --Region 2, Sektor 4 (D)---------------
         
            s_Schalt1   <= b"0011_0011_0110" ;  -- N  N  0   1  tc_4
            s_Schalt1zw <= b"0011_0010_0110" ;  -- N  n1 0
            s_Schalt2   <= b"0011_0110_0110" ;  -- N  0  0   4  ta_2
            s_Schalt2zw <= b"0010_0110_0110" ;  -- n1 0  0
            s_Schalt3   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt3zw <= b"0110_0110_0100" ;  -- 0  0  p2
            s_Schalt4   <= b"0110_0110_1100" ;  -- 0  0  P   14 tc_2
            s_Schalt4zw <= b"0110_0110_0100" ;  -- 0  0  p2
            s_Schalt5   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt5zw <= b"0010_0110_0110" ;  -- n1 0  0
            s_Schalt6   <= b"0011_0110_0110" ;  -- N  0  0   4  ta_2
            s_Schalt6zw <= b"0011_0010_0110" ;  -- N  n1 0
            s_Schalt7   <= b"0011_0011_0110" ;  -- N  N  0   1  tc_4

         
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DDiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_TTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DDiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_TTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_TTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DDiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek5  =>   s_LastRegSek<= Reg2_Sek5; --Region 2, Sektor 5 (E)---------------
         
            s_Schalt1   <= b"1100_0110_1100" ;  -- P  0  P   23 tc_4
            s_Schalt1zw <= b"0100_0110_1100" ;  -- p2 0  P
            s_Schalt2   <= b"0110_0110_1100" ;  -- 0  0  P   14 ta_2 
            s_Schalt2zw <= b"0110_0110_0100" ;  -- 0  0  p2
            s_Schalt3   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt3zw <= b"0110_0010_0110" ;  -- 0  n1 0
            s_Schalt4   <= b"0110_0011_0110" ;  -- 0  N  0   10 tc_2
            s_Schalt4zw <= b"0110_0010_0110" ;  -- 0  n1 0
            s_Schalt5   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2
            s_Schalt5zw <= b"0110_0110_0100" ;  -- 0  0  p2
            s_Schalt6   <= b"0110_0110_1100" ;  -- 0  0  P   14 ta_2 
            s_Schalt6zw <= b"0100_0110_1100" ;  -- p2 0  P
            s_Schalt7   <= b"1100_0110_1100" ;  -- P  0  P   23 tc_4

            -- Zeit vom Start des Musters bis sich der Zustand des jeweil. Transistors aendert (nur bis zu Mitte)
           

            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_TTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DDiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DDiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_TTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_TTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DDiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek6  =>   s_LastRegSek<= Reg2_Sek6; --Region 2, Sektor 6 (F)---------------
         
            s_Schalt1   <= b"0110_0011_0011" ;  -- 0  N  N   9  tc_4
            s_Schalt1zw <= b"0110_0011_0010" ;  -- 0  N  n1
            s_Schalt2   <= b"0110_0011_0110" ;  -- 0  N  0   10 ta_2
            s_Schalt2zw <= b"0110_0010_0110" ;  -- 0  n1 0
            s_Schalt3   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2 
            s_Schalt3zw <= b"0100_0110_0110" ;  -- p2 0  0
            s_Schalt4   <= b"1100_0110_0110" ;  -- P  0  0   22 tc_2
            s_Schalt4zw <= b"0100_0110_0110" ;  -- p2 0  0
            s_Schalt5   <= b"0110_0110_0110" ;  -- 0  0  0   13 tb_2 
            s_Schalt5zw <= b"0110_0010_0110" ;  -- 0  n1 0
            s_Schalt6   <= b"0110_0011_0110" ;  -- 0  N  0   10 ta_2 
            s_Schalt6zw <= b"0110_0011_0010" ;  -- 0  N  n1
            s_Schalt7   <= b"0110_0011_0011" ;  -- 0  N  N   9  tc_4

         
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_TTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DDiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DDiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_TTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DDiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_TTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------
      END CASE;

   when Reg3 => --Region 3 -------------------------------
      CASE s_Sek IS
         when Sek1  => s_LastRegSek<= Reg3_Sek1; --Region 3, Sektor 1 (A)
                                                -- Erzeuger                               Verbraucher
            s_Schalt1   <= b"1100_0110_0110" ;  -- P  0  0   22 ta_4                      P  0  0  ta_4
            s_Schalt1zw <= b"1100_0110_0010" ;  -- P  0  n1                               P  0  N
            s_Schalt2   <= b"1100_0110_0011" ;  -- P  0  N   21 tb_2                      P  0  N  tb_2 - T_ueb_korr
            s_Schalt2zw <= b"1100_0010_0011" ;  -- P  n1 N                                P  N  N
            s_Schalt3   <= b"1100_0011_0011" ;  -- P  N  N   18 tc_2                      P  N  N  tc_2 - 2*T_ueb_korr
            s_Schalt3zw <= b"0100_0011_0011" ;  -- p2 N  N                                P  N  N
            s_Schalt4   <= b"0110_0011_0011" ;  -- 0  N  N   9  ta_2                      0  N  N  ta_2
            s_Schalt4zw <= b"0100_0011_0011" ;  -- p2 N  N
            s_Schalt5   <= b"1100_0011_0011" ;  -- P  N  N   18 tc_2
            s_Schalt5zw <= b"1100_0010_0011" ;  -- P  n1 N
            s_Schalt6   <= b"1100_0110_0011" ;  -- P  0  N   21 tb_2 
            s_Schalt6zw <= b"1100_0110_0010" ;  -- P  0  n1
            s_Schalt7   <= b"1100_0110_0110" ;  -- P  0  0   22 ta_4 

       

            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_TTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DDiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DDiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_TTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DDiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_TTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek2  =>   s_LastRegSek<= Reg3_Sek2; --Region 3, Sektor 2 (B)---------------
          
            s_Schalt1   <= b"0110_0110_0011" ;  -- 0  0  N   12 ta_4 
            s_Schalt1zw <= b"0110_0100_0011" ;  -- 0  p2 N
            s_Schalt2   <= b"0110_1100_0011" ;  -- 0  P  N   15 tb_2 
            s_Schalt2zw <= b"0100_1100_0011" ;  -- p2 P  N
            s_Schalt3   <= b"1100_1100_0011" ;  -- P  P  N   24 tc_2
            s_Schalt3zw <= b"1100_1100_0010" ;  -- P  P  n1
            s_Schalt4   <= b"1100_1100_0110" ;  -- P  P  0   25 ta_2 
            s_Schalt4zw <= b"1100_1100_0010" ;  -- P  P  n1
            s_Schalt5   <= b"1100_1100_0011" ;  -- P  P  N   24 tc_2
            s_Schalt5zw <= b"0100_1100_0011" ;  -- p2 P  N
            s_Schalt6   <= b"0110_1100_0011" ;  -- 0  P  N   15 tb_2 
            s_Schalt6zw <= b"0110_0100_0011" ;  -- 0  p2 N
            s_Schalt7   <= b"0110_0110_0011" ;  -- 0  0  N   12 ta_4 

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_TTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DDiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_TTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DDiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DDiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_TTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek3  =>   s_LastRegSek<= Reg3_Sek3; --Region 3, Sektor 3 (C)---------------
          
            s_Schalt1   <= b"0110_1100_0110" ;  -- 0  P  0   16 ta_4 
            s_Schalt1zw <= b"0010_1100_0110" ;  -- n1 P  0
            s_Schalt2   <= b"0011_1100_0110" ;  -- N  P  0   7  tb_2 
            s_Schalt2zw <= b"0011_1100_0010" ;  -- N  P  n1
            s_Schalt3   <= b"0011_1100_0011" ;  -- N  P  N   6  tc_2
            s_Schalt3zw <= b"0011_0100_0011" ;  -- N  p2 N
            s_Schalt4   <= b"0011_0110_0011" ;  -- N  0  N   3  ta_2 
            s_Schalt4zw <= b"0011_0100_0011" ;  -- N  p2 N
            s_Schalt5   <= b"0011_1100_0011" ;  -- N  P  N   6  tc_2
            s_Schalt5zw <= b"0011_1100_0010" ;  -- N  P  n1
            s_Schalt6   <= b"0011_1100_0110" ;  -- N  P  0   7  tb_2 
            s_Schalt6zw <= b"0010_1100_0110" ;  -- n1 P  0
            s_Schalt7   <= b"0110_1100_0110" ;  -- 0  P  0   16 ta_4 

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DDiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_TTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_TTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DDiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DDiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_TTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek4  =>   s_LastRegSek<= Reg3_Sek4; --Region 3, Sektor 4 (D)---------------
          
            s_Schalt1   <= b"0011_0110_0110" ;  -- N  0  0   4  ta_4 
            s_Schalt1zw <= b"0011_0110_0100" ;  -- N  0  p2
            s_Schalt2   <= b"0011_0110_1100" ;  -- N  0  P   5  tb_2 
            s_Schalt2zw <= b"0011_0100_1100" ;  -- N  p2 P
            s_Schalt3   <= b"0011_1100_1100" ;  -- N  P  P   8  tc_2
            s_Schalt3zw <= b"0010_1100_1100" ;  -- n1 P  P
            s_Schalt4   <= b"0110_1100_1100" ;  -- 0  P  P   17 ta_2 
            s_Schalt4zw <= b"0010_1100_1100" ;  -- n1 P  P
            s_Schalt5   <= b"0011_1100_1100" ;  -- N  P  P   8  tc_2
            s_Schalt5zw <= b"0011_0100_1100" ;  -- N  p2 P
            s_Schalt6   <= b"0011_0110_1100" ;  -- N  0  P   5  tb_2 
            s_Schalt6zw <= b"0011_0110_0100" ;  -- N  0  p2
            s_Schalt7   <= b"0011_0110_0110" ;  -- N  0  0   4  ta_4 

            
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DDiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_TTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_TTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DDiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_TTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DDiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek5  =>   s_LastRegSek<= Reg3_Sek5; --Region 3, Sektor 5 (E)---------------
          
            s_Schalt1   <= b"0110_0110_1100" ;  -- 0  0  P   14 ta_4 - T_ueb_korr
            s_Schalt1zw <= b"0110_0010_1100" ;  -- 0  n1 P
            s_Schalt2   <= b"0110_0011_1100" ;  -- 0  N  P   11 tb_2 - T_ueb_korr
            s_Schalt2zw <= b"0010_0011_1100" ;  -- n1 N  P
            s_Schalt3   <= b"0011_0011_1100" ;  -- N  N  P   2  tc_2
            s_Schalt3zw <= b"0011_0011_0100" ;  -- N  N  p2
            s_Schalt4   <= b"0011_0011_0110" ;  -- N  N  0   1  ta_2 - 2*T_ueb_korr
            s_Schalt4zw <= b"0011_0011_0100" ;  -- N  N  p2
            s_Schalt5   <= b"0011_0011_1100" ;  -- N  N  P   2  tc_2
            s_Schalt5zw <= b"0010_0011_1100" ;  -- n1 N  P
            s_Schalt6   <= b"0110_0011_1100" ;  -- 0  N  P   11 tb_2 - T_ueb_korr
            s_Schalt6zw <= b"0110_0010_1100" ;  -- 0  n1 P
            s_Schalt7   <= b"0110_0110_1100" ;  -- 0  0  P   14 ta_4 - T_ueb_korr


            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DDiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_TTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DDiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_TTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_TTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DDiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek6  =>   s_LastRegSek<= Reg3_Sek6; --Region 3, Sektor 6 (F)---------------
            s_Schalt1   <= b"0110_0011_0110" ;  -- 0  N  0   10 ta_4 - T_ueb_korr
            s_Schalt1zw <= b"0100_0011_0110" ;  -- p2 N  0
            s_Schalt2   <= b"1100_0011_0110" ;  -- P  N  0   19 tb_2 - T_ueb_korr
            s_Schalt2zw <= b"1100_0011_0100" ;  -- P  N  p2
            s_Schalt3   <= b"1100_0011_1100" ;  -- P  N  P   20 tc_2
            s_Schalt3zw <= b"1100_0010_1100" ;  -- P  n1 P
            s_Schalt4   <= b"1100_0110_1100" ;  -- P  0  P   23 ta_2 - 2*T_ueb_korr
            s_Schalt4zw <= b"1100_0010_1100" ;  -- P  n1 P
            s_Schalt5   <= b"1100_0011_1100" ;  -- P  N  P   20 tc_2
            s_Schalt5zw <= b"1100_0011_0100" ;  -- P  N  p2
            s_Schalt6   <= b"1100_0011_0110" ;  -- P  N  0   19 tb_2 - T_ueb_korr
            s_Schalt6zw <= b"0100_0011_0110" ;  -- p2 N  0
            s_Schalt7   <= b"0110_0011_0110" ;  -- 0  N  0   10 ta_4 - T_ueb_korr

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_TTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DDiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DDiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_TTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_TTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DDiWPh)+1; end if;
            -------------------------------------------------------------------
      END CASE;

   when Reg6 => --Region 6 -------------------------------
      CASE s_Sek IS
         when Sek1  => s_LastRegSek<= Reg6_Sek1;--Region 6, Sektor 1 (A)
                                                -- Erzeuger                            Verbraucher
            s_Schalt1   <= b"1100_1100_0110" ;  -- P  P  0   25 tc_4                   P  P  0  tc_4
            s_Schalt1zw <= b"1100_1100_0010" ;  -- P  P  n1                            P  P  N
            s_Schalt2   <= b"1100_1100_0011" ;  -- P  P  N   24 ta_2                   P  P  N  ta_2 
            s_Schalt2zw <= b"1100_0100_0011" ;  -- P  p2 N                             P  P  N
            s_Schalt3   <= b"1100_0110_0011" ;  -- P  0  N   21 tb_2                   P  0  N  tb_2
            s_Schalt3zw <= b"0100_0110_0011" ;  -- p2 0  N                             P  0  N
            s_Schalt4   <= b"0110_0110_0011" ;  -- 0  0  N   12 tc_2                   0  0  N  tc_2
            s_Schalt4zw <= b"0100_0110_0011" ;  -- p2 0  N
            s_Schalt5   <= b"1100_0110_0011" ;  -- P  0  N   21 tb_2 
            s_Schalt5zw <= b"1100_0100_0011" ;  -- P  p2 N
            s_Schalt6   <= b"1100_1100_0011" ;  -- P  P  N   24 ta_2
            s_Schalt6zw <= b"1100_1100_0010" ;  -- P  P  n1
            s_Schalt7   <= b"1100_1100_0110" ;  -- P  P  0   25 tc_4 

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_TTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DDiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_TTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DDiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DDiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_TTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek2  =>   s_LastRegSek<= Reg6_Sek2; --Region 6, Sektor 2 (B)---------------
            s_Schalt1   <= b"0011_0110_0011" ;  -- N  0  N   3  tc_4 
            s_Schalt1zw <= b"0011_0100_0011" ;  -- N  p2 N
            s_Schalt2   <= b"0011_1100_0011" ;  -- N  P  N   6  ta_2
            s_Schalt2zw <= b"0010_1100_0011" ;  -- n1 P  N
            s_Schalt3   <= b"0110_1100_0011" ;  -- 0  P  N   15 tb_2 
            s_Schalt3zw <= b"0110_1100_0010" ;  -- 0  P  n1
            s_Schalt4   <= b"0110_1100_0110" ;  -- 0  P  0   16 tc_2 
            s_Schalt4zw <= b"0110_1100_0010" ;  -- 0  P  n1
            s_Schalt5   <= b"0110_1100_0011" ;  -- 0  P  N   15 tb_2 
            s_Schalt5zw <= b"0010_1100_0011" ;  -- n1 P  N
            s_Schalt6   <= b"0011_1100_0011" ;  -- N  P  N   6  ta_2
            s_Schalt6zw <= b"0011_0100_0011" ;  -- N  p2 N
            s_Schalt7   <= b"0011_0110_0011" ;  -- N  0  N   3  tc_4 

            
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DDiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_TTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_TTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DDiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DDiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_TTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek3  =>   s_LastRegSek<= Reg6_Sek3; --Region 6, Sektor 3 (C)---------------
          
            s_Schalt1   <= b"0110_1100_1100" ;  -- 0  P  P   17 tc_4 
            s_Schalt1zw <= b"0010_1100_1100" ;  -- n1 P  P
            s_Schalt2   <= b"0011_1100_1100" ;  -- N  P  P   8  ta_2
            s_Schalt2zw <= b"0011_1100_0100" ;  -- N  P  p2
            s_Schalt3   <= b"0011_1100_0110" ;  -- N  P  0   7  tb_2 
            s_Schalt3zw <= b"0011_0100_0110" ;  -- N  p2 0
            s_Schalt4   <= b"0011_0110_0110" ;  -- N  0  0   4  tc_2 
            s_Schalt4zw <= b"0011_0100_0110" ;  -- N  p2 0
            s_Schalt5   <= b"0011_1100_0110" ;  -- N  P  0   7  tb_2 
            s_Schalt5zw <= b"0011_1100_0100" ;  -- N  P  p2
            s_Schalt6   <= b"0011_1100_1100" ;  -- N  P  P   8  ta_2
            s_Schalt6zw <= b"0010_1100_1100" ;  -- n1 P  P
            s_Schalt7   <= b"0110_1100_1100" ;  -- 0  P  P   17 tc_4 

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DDiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_TTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_TTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DDiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_TTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DDiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek4  =>   s_LastRegSek<= Reg6_Sek4;--Region 6, Sektor 4 (D)---------------
            s_Schalt1   <= b"0011_0011_0110" ;  -- N  N  0   1  tc_4 
            s_Schalt1zw <= b"0011_0011_0100" ;  -- N  N  p2
            s_Schalt2   <= b"0011_0011_1100" ;  -- N  N  P   2  ta_2
            s_Schalt2zw <= b"0011_0010_1100" ;  -- N  n1 P
            s_Schalt3   <= b"0011_0110_1100" ;  -- N  0  P   5  tb_2 
            s_Schalt3zw <= b"0010_0110_1100" ;  -- n1 0  P
            s_Schalt4   <= b"0110_0110_1100" ;  -- 0  0  P   14 tc_2 
            s_Schalt4zw <= b"0010_0110_1100" ;  -- n1 0  P
            s_Schalt5   <= b"0011_0110_1100" ;  -- N  0  P   5  tb_2 
            s_Schalt5zw <= b"0011_0010_1100" ;  -- N  n1 P
            s_Schalt6   <= b"0011_0011_1100" ;  -- N  N  P   2  ta_2
            s_Schalt6zw <= b"0011_0011_0100" ;  -- N  N  p2
            s_Schalt7   <= b"0011_0011_0110" ;  -- N  N  0   1  tc_4 

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DDiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_TTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DDiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_TTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_TTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DDiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek5  =>   s_LastRegSek<= Reg6_Sek5; --Region 6, Sektor 5 (E)---------------
            s_Schalt1   <= b"1100_0110_1100" ;  -- P  0  P   23 tc_4 
            s_Schalt1zw <= b"1100_0010_1100" ;  -- P  n1 P
            s_Schalt2   <= b"1100_0011_1100" ;  -- P  N  P   20 ta_2
            s_Schalt2zw <= b"0100_0011_1100" ;  -- p2 N  P
            s_Schalt3   <= b"0110_0011_1100" ;  -- 0  N  P   11 tb_2 
            s_Schalt3zw <= b"0110_0011_0100" ;  -- 0  N  p2
            s_Schalt4   <= b"0110_0011_0110" ;  -- 0  N  0   10 tc_2 
            s_Schalt4zw <= b"0110_0011_0100" ;  -- 0  N  p2
            s_Schalt5   <= b"0110_0011_1100" ;  -- 0  N  P   11 tb_2 
            s_Schalt5zw <= b"0100_0011_1100" ;  -- p2 N  P
            s_Schalt6   <= b"1100_0011_1100" ;  -- P  N  P   20 ta_2
            s_Schalt6zw <= b"1100_0010_1100" ;  -- P  n1 P
            s_Schalt7   <= b"1100_0110_1100" ;  -- P  0  P   23 tc_4 

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_TTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DDiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DDiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_TTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_TTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DDiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek6  =>   s_LastRegSek<= Reg6_Sek6; --Region 6, Sektor 6 (F)---------------

            s_Schalt1   <= b"0110_0011_0011" ;  -- 0  N  N   9  tc_4 
            s_Schalt1zw <= b"0100_0011_0011" ;  -- p2 N  N
            s_Schalt2   <= b"1100_0011_0011" ;  -- P  N  N   18 ta_2
            s_Schalt2zw <= b"1100_0011_0010" ;  -- P  N  n1
            s_Schalt3   <= b"1100_0011_0110" ;  -- P  N  0   19 tb_2 
            s_Schalt3zw <= b"1100_0010_0110" ;  -- P  n1 0
            s_Schalt4   <= b"1100_0110_0110" ;  -- P  0  0   22 tc_2
            s_Schalt4zw <= b"1100_0010_0110" ;  -- P  n1 0
            s_Schalt5   <= b"1100_0011_0110" ;  -- P  N  0   19 tb_2
            s_Schalt5zw <= b"1100_0011_0010" ;  -- P  N  n1
            s_Schalt6   <= b"1100_0011_0011" ;  -- P  N  N   18 ta_2
            s_Schalt6zw <= b"0100_0011_0011" ;  -- p2 N  N
            s_Schalt7   <= b"0110_0011_0011" ;  -- 0  N  N   9  tc_4

            

            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_TTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DDiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DDiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_TTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DDiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_TTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------
      END CASE;

   when Reg4 => --Region 4 -------------------------------
      CASE s_Sek IS
         when Sek1  => s_LastRegSek<= Reg4_Sek1; --Region 4, Sektor 1 (A)
                                                -- Erzeuger                               Verbraucher
            s_Schalt1   <= b"1100_0110_0110" ;  -- P  0  0   22 ta_4                      P  0  0  ta_4
            s_Schalt1zw <= b"1100_0110_0010" ;  -- P  0  n1                               P  0  N
            s_Schalt2   <= b"1100_0110_0011" ;  -- P  0  N   21 tb_2                      P  0  N  tb_2 
            s_Schalt2zw <= b"0100_0110_0011" ;  -- p2 0  N                                P  0  N
            s_Schalt3   <= b"0110_0110_0011" ;  -- 0  0  N   12 tc_2                      0  0  N  tc_2
            s_Schalt3zw <= b"0110_0010_0011" ;  -- 0  n1 N                                0  N  N
            s_Schalt4   <= b"0110_0011_0011" ;  -- 0  N  N   9  ta_2                      0  N  N  ta_2 
            s_Schalt4zw <= b"0110_0010_0011" ;  -- 0  n1 N
            s_Schalt5   <= b"0110_0110_0011" ;  -- 0  0  N   12 tc_2 
            s_Schalt5zw <= b"0100_0110_0011" ;  -- p2 0  N
            s_Schalt6   <= b"1100_0110_0011" ;  -- P  0  N   21 tb_2
            s_Schalt6zw <= b"1100_0110_0010" ;  -- P  0  n1
            s_Schalt7   <= b"1100_0110_0110" ;  -- P  0  0   22 ta_4 

          
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_TTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DDiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DDiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_TTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DDiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_TTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek2  =>   s_LastRegSek<= Reg4_Sek2; --Region 4, Sektor 2 (B)---------------

            s_Schalt1   <= b"0110_0110_0011" ;  -- 0  0  N   12 ta_4 
            s_Schalt1zw <= b"0110_0100_0011" ;  -- 0  p2 N
            s_Schalt2   <= b"0110_1100_0011" ;  -- 0  P  N   15 tb_2
            s_Schalt2zw <= b"0110_1100_0010" ;  -- 0  P  n1
            s_Schalt3   <= b"0110_1100_0110" ;  -- 0  P  0   16 tc_2 
            s_Schalt3zw <= b"0100_1100_0110" ;  -- p2 P  0
            s_Schalt4   <= b"1100_1100_0110" ;  -- P  P  0   25 ta_2
            s_Schalt4zw <= b"0100_1100_0110" ;  -- p2 P  0
            s_Schalt5   <= b"0110_1100_0110" ;  -- 0  P  0   16 tc_2 
            s_Schalt5zw <= b"0110_1100_0010" ;  -- 0  P  n1
            s_Schalt6   <= b"0110_1100_0011" ;  -- 0  P  N   15 tb_2
            s_Schalt6zw <= b"0110_0100_0011" ;  -- 0  p2 N
            s_Schalt7   <= b"0110_0110_0011" ;  -- 0  0  N   12 ta_4 

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_TTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DDiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_TTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DDiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DDiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_TTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek3  =>   s_LastRegSek<= Reg4_Sek3; --Region 4, Sektor 3 (C)---------------

            s_Schalt1   <= b"0110_1100_0110" ;  -- 0  P  0   16 ta_4 
            s_Schalt1zw <= b"0010_1100_0110" ;  -- n1 P  0
            s_Schalt2   <= b"0011_1100_0110" ;  -- N  P  0   7  tb_2
            s_Schalt2zw <= b"0011_0100_0110" ;  -- N  p2 0
            s_Schalt3   <= b"0011_0110_0110" ;  -- N  0  0   4  tc_2 
            s_Schalt3zw <= b"0011_0110_0010" ;  -- N  0  n1
            s_Schalt4   <= b"0011_0110_0011" ;  -- N  0  N   3  ta_2
            s_Schalt4zw <= b"0011_0110_0010" ;  -- N  0  n1
            s_Schalt5   <= b"0011_0110_0110" ;  -- N  0  0   4  tc_2 
            s_Schalt5zw <= b"0011_0100_0110" ;  -- N  p2 0
            s_Schalt6   <= b"0011_1100_0110" ;  -- N  P  0   7  tb_2
            s_Schalt6zw <= b"0010_1100_0110" ;  -- n1 P  0
            s_Schalt7   <= b"0110_1100_0110" ;  -- 0  P  0   16 ta_4 

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DDiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_TTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_TTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DDiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DDiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_TTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek4  =>   s_LastRegSek<= Reg4_Sek4; --Region 4, Sektor 4 (D)---------------

            s_Schalt1   <= b"0011_0110_0110" ;  -- N  0  0   4  ta_4 
            s_Schalt1zw <= b"0011_0110_0100" ;  -- N  0  p2
            s_Schalt2   <= b"0011_0110_1100" ;  -- N  0  P   5  tb_2
            s_Schalt2zw <= b"0010_0110_1100" ;  -- n1 0  P
            s_Schalt3   <= b"0110_0110_1100" ;  -- 0  0  P   14 tc_2 
            s_Schalt3zw <= b"0110_0100_1100" ;  -- 0  p2 P
            s_Schalt4   <= b"0110_1100_1100" ;  -- 0  P  P   17 ta_2
            s_Schalt4zw <= b"0110_0100_1100" ;  -- 0  p2 P
            s_Schalt5   <= b"0110_0110_1100" ;  -- 0  0  P   14 tc_2 
            s_Schalt5zw <= b"0010_0110_1100" ;  -- n1 0  P
            s_Schalt6   <= b"0011_0110_1100" ;  -- N  0  P   5  tb_2
            s_Schalt6zw <= b"0011_0110_0100" ;  -- N  0  p2
            s_Schalt7   <= b"0011_0110_0110" ;  -- N  0  0   4  ta_4 


            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DDiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_TTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_TTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DDiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_TTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DDiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek5  =>   s_LastRegSek<= Reg4_Sek5; --Region 4, Sektor 5 (E)---------------

            s_Schalt1   <= b"0110_0110_1100" ;  -- 0  0  P   14 ta_4
            s_Schalt1zw <= b"0110_0010_1100" ;  -- 0  n1 P
            s_Schalt2   <= b"0110_0011_1100" ;  -- 0  N  P   11 tb_2
            s_Schalt2zw <= b"0110_0011_0100" ;  -- 0  N  p2
            s_Schalt3   <= b"0110_0011_0110" ;  -- 0  N  0   10 tc_2
            s_Schalt3zw <= b"0010_0011_0110" ;  -- n1 N  0
            s_Schalt4   <= b"0011_0011_0110" ;  -- N  N  0   1  ta_2
            s_Schalt4zw <= b"0010_0011_0110" ;  -- n1 N  0
            s_Schalt5   <= b"0110_0011_0110" ;  -- 0  N  0   10 tc_2 
            s_Schalt5zw <= b"0110_0011_0100" ;  -- 0  N  p2
            s_Schalt6   <= b"0110_0011_1100" ;  -- 0  N  P   11 tb_2
            s_Schalt6zw <= b"0110_0010_1100" ;  -- 0  n1 P
            s_Schalt7   <= b"0110_0110_1100" ;  -- 0  0  P   14 ta_4 

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DDiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_TTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DDiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_TTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_TTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DDiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek6  =>    s_LastRegSek<= Reg4_Sek6; --Region 4, Sektor 6 (F)---------------
            s_Schalt1   <= b"0110_0011_0110" ;  -- 0  N  0   10 ta_4 
            s_Schalt1zw <= b"0100_0011_0110" ;  -- p2 N  0
            s_Schalt2   <= b"1100_0011_0110" ;  -- P  N  0   19 tb_2
            s_Schalt2zw <= b"1100_0010_0110" ;  -- P  n1 0
            s_Schalt3   <= b"1100_0110_0110" ;  -- P  0  0   22 tc_2 
            s_Schalt3zw <= b"1100_0110_0100" ;  -- P  0  p2
            s_Schalt4   <= b"1100_0110_1100" ;  -- P  0  P   23 ta_2
            s_Schalt4zw <= b"1100_0110_0100" ;  -- P  0  p2
            s_Schalt5   <= b"1100_0110_0110" ;  -- P  0  0   22 tc_2 
            s_Schalt5zw <= b"1100_0010_0110" ;  -- P  n1 0
            s_Schalt6   <= b"1100_0011_0110" ;  -- P  N  0   19 tb_2
            s_Schalt6zw <= b"0100_0011_0110" ;  -- p2 N  0
            s_Schalt7   <= b"0110_0011_0110" ;  -- 0  N  0   10 ta_4 

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_TTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DDiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DDiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_TTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_TTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DDiWPh)+1; end if;
            -------------------------------------------------------------------
         END CASE;

   when Reg5 => --Region 5 -------------------------------
      CASE s_Sek IS
         when Sek1  => s_LastRegSek<= Reg5_Sek1; --Region 5, Sektor 1 (A)
                                                -- Erzeuger                              Verbraucher
            s_Schalt1   <= b"1100_1100_0110" ;  -- P  P  0   25 tc_4                     P  P  0  tc_4 
            s_Schalt1zw <= b"1100_0100_0110" ;  -- P  p2 0                               P  P  0
            s_Schalt2   <= b"1100_0110_0110" ;  -- P  0  0   22 ta_2                     P  0  0  ta_2
            s_Schalt2zw <= b"1100_0110_0010" ;  -- P  0  n1                              P  0  N
            s_Schalt3   <= b"1100_0110_0011" ;  -- P  0  N   21 tb_2                     P  0  N  tb_2 
            s_Schalt3zw <= b"0100_0110_0011" ;  -- p2 0  N                               P  0  N
            s_Schalt4   <= b"0110_0110_0011" ;  -- 0  0  N   12 tc_2                     0  0  N  tc_2
            s_Schalt4zw <= b"0100_0110_0011" ;  -- p2 0  N
            s_Schalt5   <= b"1100_0110_0011" ;  -- P  0  N   21 tb_2
            s_Schalt5zw <= b"1100_0110_0010" ;  -- P  0  n1
            s_Schalt6   <= b"1100_0110_0110" ;  -- P  0  0   22 ta_2 
            s_Schalt6zw <= b"1100_0100_0110" ;  -- P  p2 0
            s_Schalt7   <= b"1100_1100_0110" ;  -- P  P  0   25 tc_4

            
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_TTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DDiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_TTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DDiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DDiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_TTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek2  =>   s_LastRegSek<= Reg5_Sek2; --Region 5, Sektor 2 (B)---------------

            s_Schalt1   <= b"0011_0110_0011" ;  -- N  0  N   3  tc_4
            s_Schalt1zw <= b"0010_0110_0011" ;  -- n1 0  N
            s_Schalt2   <= b"0110_0110_0011" ;  -- 0  0  N   12 ta_2 
            s_Schalt2zw <= b"0110_0100_0011" ;  -- 0  p2 N
            s_Schalt3   <= b"0110_1100_0011" ;  -- 0  P  N   15 tb_2
            s_Schalt3zw <= b"0110_1100_0010" ;  -- 0  P  n1
            s_Schalt4   <= b"0110_1100_0110" ;  -- 0  P  0   16 tc_2 
            s_Schalt4zw <= b"0110_1100_0010" ;  -- 0  P  n1
            s_Schalt5   <= b"0110_1100_0011" ;  -- 0  P  N   15 tb_2
            s_Schalt5zw <= b"0110_0100_0011" ;  -- 0  p2 N
            s_Schalt6   <= b"0110_0110_0011" ;  -- 0  0  N   12 ta_2 
            s_Schalt6zw <= b"0010_0110_0011" ;  -- n1 0  N
            s_Schalt7   <= b"0011_0110_0011" ;  -- N  0  N   3  tc_4

            
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DDiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_TTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_TTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DDiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DDiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DDiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_TTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_TTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek3  =>   s_LastRegSek<= Reg5_Sek3; --Region 5, Sektor 3 (C)---------------

            s_Schalt1   <= b"0110_1100_1100" ;  -- 0  P  P   17 tc_4
            s_Schalt1zw <= b"0110_1100_0100" ;  -- 0  P  p2
            s_Schalt2   <= b"0110_1100_0110" ;  -- 0  P  0   16 ta_2 
            s_Schalt2zw <= b"0010_1100_0110" ;  -- n1 P  0
            s_Schalt3   <= b"0011_1100_0110" ;  -- N  P  0   7  tb_2
            s_Schalt3zw <= b"0011_0100_0110" ;  -- N  p2 0
            s_Schalt4   <= b"0011_0110_0110" ;  -- N  0  0   4  tc_2 
            s_Schalt4zw <= b"0011_0100_0110" ;  -- N  p2 0
            s_Schalt5   <= b"0011_1100_0110" ;  -- N  P  0   7  tb_2
            s_Schalt5zw <= b"0010_1100_0110" ;  -- n1 P  0
            s_Schalt6   <= b"0110_1100_0110" ;  -- 0  P  0   16 ta_2 
            s_Schalt6zw <= b"0110_1100_0100" ;  -- 0  P  p2
            s_Schalt7   <= b"0110_1100_1100" ;  -- 0  P  P   17 tc_4

         
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DDiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_TTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_TTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_TTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DDiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DDiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_TTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DDiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek4  =>   s_LastRegSek<= Reg5_Sek4; --Region 5, Sektor 4 (D)---------------

            s_Schalt1   <= b"0011_0011_0110" ;  -- N  N  0   1  tc_4
            s_Schalt1zw <= b"0011_0010_0110" ;  -- N  n1 0
            s_Schalt2   <= b"0011_0110_0110" ;  -- N  0  0   4  ta_2 
            s_Schalt2zw <= b"0011_0110_0100" ;  -- N  0  p2
            s_Schalt3   <= b"0011_0110_1100" ;  -- N  0  P   5  tb_2
            s_Schalt3zw <= b"0010_0110_1100" ;  -- n1 0  P
            s_Schalt4   <= b"0110_0110_1100" ;  -- 0  0  P   14 tc_2 
            s_Schalt4zw <= b"0010_0110_1100" ;  -- n1 0  P
            s_Schalt5   <= b"0011_0110_1100" ;  -- N  0  P   5  tb_2
            s_Schalt5zw <= b"0011_0110_0100" ;  -- N  0  p2
            s_Schalt6   <= b"0011_0110_0110" ;  -- N  0  0   4  ta_2 
            s_Schalt6zw <= b"0011_0010_0110" ;  -- N  n1 0
            s_Schalt7   <= b"0011_0011_0110" ;  -- N  N  0   1  tc_4

           
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DDiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DDiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_TTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_TTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DDiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_TTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_TTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DDiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek5  =>  s_LastRegSek<= Reg5_Sek5; --Region 5, Sektor 5 (E)---------------
            s_Schalt1   <= b"1100_0110_1100" ;  -- P  0  P   23 tc_4
            s_Schalt1zw <= b"0100_0110_1100" ;  -- p2 0  P
            s_Schalt2   <= b"0110_0110_1100" ;  -- 0  0  P   14 ta_2 
            s_Schalt2zw <= b"0110_0010_1100" ;  -- 0  n1 P
            s_Schalt3   <= b"0110_0011_1100" ;  -- 0  N  P   11 tb_2
            s_Schalt3zw <= b"0110_0011_0100" ;  -- 0  N  p2
            s_Schalt4   <= b"0110_0011_0110" ;  -- 0  N  0   10 tc_2 
            s_Schalt4zw <= b"0110_0011_0100" ;  -- 0  N  p2
            s_Schalt5   <= b"0110_0011_1100" ;  -- 0  N  P   11 tb_2
            s_Schalt5zw <= b"0110_0010_1100" ;  -- 0  n1 P
            s_Schalt6   <= b"0110_0110_1100" ;  -- 0  0  P   14 ta_2 
            s_Schalt6zw <= b"0100_0110_1100" ;  -- p2 0  P
            s_Schalt7   <= b"1100_0110_1100" ;  -- P  0  P   23 tc_4


            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_TTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_DTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DDiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DTiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DTiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DTiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DDiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_DTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_DTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_TTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_TTiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_TTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_DDiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DDiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------

         when Sek6  =>   s_LastRegSek<= Reg5_Sek6; --Region 5, Sektor 6 (F)---------------
            s_Schalt1   <= b"0110_0011_0011" ;  -- 0  N  N   9  tc_4
            s_Schalt1zw <= b"0110_0011_0010" ;  -- 0  N  n1
            s_Schalt2   <= b"0110_0011_0110" ;  -- 0  N  0   10 ta_2 
            s_Schalt2zw <= b"0100_0011_0110" ;  -- p2 N  0
            s_Schalt3   <= b"1100_0011_0110" ;  -- P  N  0   19 tb_2
            s_Schalt3zw <= b"1100_0010_0110" ;  -- P  n1 0
            s_Schalt4   <= b"1100_0110_0110" ;  -- P  0  0   22 tc_2 
            s_Schalt4zw <= b"1100_0010_0110" ;  -- P  n1 0
            s_Schalt5   <= b"1100_0011_0110" ;  -- P  N  0   19 tb_2
            s_Schalt5zw <= b"0100_0011_0110" ;  -- p2 N  0
            s_Schalt6   <= b"0110_0011_0110" ;  -- 0  N  0   10 ta_2 
            s_Schalt6zw <= b"0110_0011_0010" ;  -- 0  N  n1
            s_Schalt7   <= b"0110_0011_0011" ;  -- 0  N  N   9  tc_4

            
            if iUPh_sign = '0' then s_UCE1_Uph_Mult <=     Uce_DTiUPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Uph_Mult <=     Uce_DTiUPh;
                                    s_UCE3_Uph_Mult <=     Uce_TTiUPh;
                                    s_UCE4_Uph_Mult <=     Uce_TTiUPh;
            else                    s_UCE1_Uph_Mult <= NOT(Uce_DTiUPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Uph_Mult <= NOT(Uce_DTiUPh)+1;
                                    s_UCE3_Uph_Mult <= NOT(Uce_DDiUPh)+1;
                                    s_UCE4_Uph_Mult <= NOT(Uce_DDiUPh)+1; end if;
            -------------------------------------------------------------------
            if iVPh_sign = '0' then s_UCE1_Vph_Mult <=     Uce_DDiVPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE3_Vph_Mult <=     Uce_DDiVPh;
                                    s_UCE4_Vph_Mult <=     Uce_DTiVPh;
            else                    s_UCE1_Vph_Mult <= NOT(Uce_TTiVPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE3_Vph_Mult <= NOT(Uce_TTiVPh)+1;
                                    s_UCE4_Vph_Mult <= NOT(Uce_DTiVPh)+1; end if;
            -------------------------------------------------------------------
            if iWPh_sign = '0' then s_UCE1_Wph_Mult <=     Uce_DDiWPh;    -- Stromvorzeichen positiv
                                    s_UCE2_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE3_Wph_Mult <=     Uce_DTiWPh;
                                    s_UCE4_Wph_Mult <=     Uce_DTiWPh;
            else                    s_UCE1_Wph_Mult <= NOT(Uce_TTiWPh)+1; -- Stromvorzeichen negativ
                                    s_UCE2_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE3_Wph_Mult <= NOT(Uce_DTiWPh)+1;
                                    s_UCE4_Wph_Mult <= NOT(Uce_DTiWPh)+1; end if;
            -------------------------------------------------------------------
      END CASE;
   END CASE;


   -----------------------------------------------------------------
   when UEBERGAN0 =>            -- 0. Uebergangs- Schalterstellung
   -----------------------------------------------------------------
      IF (s_counter_time1 <  s_tueb-1 AND s_tueb>0 ) THEN
         s_counter_time1  <= s_counter_time1 + 1;
      ELSE
         s_counter_time1  <= (others=>'0');
         S_v              <= s_Schalt1;
         S_STATE          <= STELLUNG1;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG1 =>            --1. Aktive Schalterstellung
   -----------------------------------------------------------------
      IF (s_counter_time1  <  s_t1-1 AND s_t1>0 ) THEN
         s_counter_time1   <= s_counter_time1 + 1;
      ELSE
         s_counter_time1   <= (others=>'0');
         S_v               <= s_Schalt1zw;
         S_STATE           <= STELLUNG1_ZW;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG1_ZW =>         -- 1. Uebergangs- Schalterstellung
   -----------------------------------------------------------------
      IF s_counter_time1  <  s_tueb-1 AND s_tueb>0  THEN
         s_counter_time1  <= s_counter_time1 + 1;
      ELSE
         s_counter_time1  <= (others=>'0');
         S_v              <= s_Schalt2;
         S_STATE          <= STELLUNG2;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG2 => --2. Aktive Schalterstellung
   -----------------------------------------------------------------
      IF s_counter_time1  <  s_t2-1 AND s_t2>0  THEN
         s_counter_time1  <= s_counter_time1 + 1;
      ELSE
         s_counter_time1  <= (others=>'0'); --Zaehler reseten
         S_v              <= s_Schalt2zw;
         S_STATE          <= STELLUNG2_ZW;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG2_ZW => -- 2. Uebergangs- Schalterstellung
   -----------------------------------------------------------------
      IF s_counter_time1  <  s_tueb-1 AND s_tueb>0 THEN
         s_counter_time1  <= s_counter_time1 + 1;
      ELSE
         s_counter_time1  <= (others=>'0'); --Zaehler reseten
         S_v              <= s_Schalt3;
         S_STATE          <= STELLUNG3;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG3 => --3. Aktive Schalterstellung
   -----------------------------------------------------------------
      IF s_counter_time1  < s_t3-1 AND s_t3>0 THEN
         s_counter_time1  <= s_counter_time1 + 1;
      ELSE
         s_counter_time1  <= (others=>'0'); --Zaehler reseten
         S_v              <= s_Schalt3zw;
         S_STATE          <= STELLUNG3_ZW;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG3_ZW => -- 3. Uebergangs- Schalterstellung
   -----------------------------------------------------------------
      IF  s_counter_time1 < s_tueb-1 AND s_tueb>0   THEN
         s_counter_time1  <= s_counter_time1 + 1;
      ELSE
         s_counter_time1  <= (others=>'0');
         S_v              <= s_Schalt4;
         S_STATE          <= STELLUNG4 ;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG4 => --4. Aktive Schalterstellung
   -----------------------------------------------------------------
      IF s_counter_time1 < s_t4-1 AND s_t4>0    THEN
         s_counter_time1 <= s_counter_time1 + 1;
      ELSE
         s_counter_time1 <= (others=>'0');
         S_v             <= s_Schalt4zw;
         S_STATE         <= STELLUNG4_ZW;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG4_ZW => -- 4. Uebergangs- Schalterstellung
   -----------------------------------------------------------------
      IF s_counter_time1 < s_tueb-1 AND s_tueb>0 THEN
         s_counter_time1 <= s_counter_time1 + 1;
      ELSE
         s_counter_time1 <= (others=>'0');
         S_v             <= s_Schalt5;
         S_STATE         <= STELLUNG5;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG5 =>    --5. Aktive Schalterstellung
   -----------------------------------------------------------------
      IF s_counter_time1 < s_t5-1 AND s_t5>0   THEN
         s_counter_time1 <= s_counter_time1 + 1;
      ELSE
         s_counter_time1 <= (others=>'0');
            S_v          <= s_Schalt5zw;
            S_STATE      <= STELLUNG5_ZW;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG5_ZW => -- 5. Uebergangs- Schalterstellung
   -----------------------------------------------------------------
      IF s_counter_time1 < s_tueb-1 AND s_tueb>0  THEN
         s_counter_time1 <= s_counter_time1 + 1;
      ELSE
         s_counter_time1 <= (others=>'0');
         S_v             <= s_Schalt6;
         S_STATE         <= STELLUNG6;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG6 => --6. Aktive Schalterstellung
   -----------------------------------------------------------------
      IF s_counter_time1 < s_t6-1 AND s_t6>0 THEN
         s_counter_time1 <= s_counter_time1 + 1;
      ELSE
         s_counter_time1 <= (others=>'0');
         S_v             <= s_Schalt6zw;
         S_STATE         <= STELLUNG6_ZW;
      END IF;

   -----------------------------------------------------------------
   when STELLUNG6_ZW => -- 6. Uebergangs- Schalterstellung
   -----------------------------------------------------------------
      IF s_counter_time1 < s_tueb-1 AND s_tueb>0  THEN
         s_counter_time1 <= s_counter_time1 + 1;
      ELSE
         s_counter_time1 <= (others=>'0');
         S_v             <= s_Schalt7;
         S_STATE         <= STELLUNG7;
      END IF;
      
   -----------------------------------------------------------------
   when STELLUNG7 =>      --7. Aktive Schalterstellung
    -----------------------------------------------------------------
      IF s_counter_time1    < s_t7-3 AND s_t7>2 then  -- Letzter Zustand ist um zwei Takte Verkuerzt
         s_counter_time1    <= s_counter_time1 + 1;
      ELSE
         s_counter_time1    <= (others=>'0');
         Data_Out_Valid_pwm <= '1';
         S_STATE            <= EINGANG_SPEICHERN;
      END IF;

   -----------------------------------------------------------------
   when others =>          -- Absicherung fuer FPGA
   -----------------------------------------------------------------
      S_STATE               <= EINGANG_SPEICHERN;
   end case;

END IF;

END IF;
END process p001 ;


UCE_bestimmung: process (clk, reset)
 BEGIN
  if (reset = '1') then

         s_Mult        <=(others=>'0');
         s_Mult_Input1 <=(others=>'0');
         s_Mult_Input2 <=(others=>'0');

         UCE1_Uph      <=(others=>'0');
         UCE2_Uph      <=(others=>'0');
         UCE3_Uph      <=(others=>'0');
         UCE4_Uph      <=(others=>'0');

         UCE1_Vph      <=(others=>'0');
         UCE2_Vph      <=(others=>'0');
         UCE3_Vph      <=(others=>'0');
         UCE4_Vph      <=(others=>'0');

         UCE1_Wph      <=(others=>'0');
         UCE2_Wph      <=(others=>'0');
         UCE3_Wph      <=(others=>'0');
         UCE4_Wph      <=(others=>'0');

         s_STATE_UCE        <= MULT1_VORBEREITEN;
         Data_Out_Valid_Uce <= '0';

  else
   if rising_edge(clk)   THEN
     CASE s_STATE_UCE IS
      ------------------------------------------------------------------
      when MULT1_VORBEREITEN =>   --1.Takt
      ------------------------------------------------------------------
         s_Mult_Input1        <= s_UCE1_ph_Mult;
         s_Mult_Input2        <= s_UCE1_Uph_Mult;
         s_STATE_UCE          <= MULT1;
         Data_Out_Valid_Uce   <= '0';
      ------------------------------------------------------------------
      when MULT1 =>   --2.Takt
      ------------------------------------------------------------------
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE2_ph_Mult;
         s_Mult_Input2        <= s_UCE2_Uph_Mult;
         s_STATE_UCE          <= MULT2;
      ------------------------------------------------------------------
      when MULT2 =>   --3.Takt
      ------------------------------------------------------------------
         s_UCE1_Uph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE3_ph_Mult;
         s_Mult_Input2        <= s_UCE3_Uph_Mult;
         s_STATE_UCE          <= MULT3;
      ------------------------------------------------------------------
      when MULT3 =>   --4.Takt
      ------------------------------------------------------------------
         s_UCE2_Uph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE4_ph_Mult;
         s_Mult_Input2        <= s_UCE4_Uph_Mult;
         s_STATE_UCE          <= MULT4;
      ------------------------------------------------------------------
      when MULT4 =>   --5.Takt
      ------------------------------------------------------------------
         s_UCE3_Uph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE1_ph_Mult;
         s_Mult_Input2        <= s_UCE1_Vph_Mult;
         s_STATE_UCE          <= MULT5;
      ------------------------------------------------------------------
      when MULT5 =>   --6.Takt
      ------------------------------------------------------------------
         s_UCE4_Uph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE2_ph_Mult;
         s_Mult_Input2        <= s_UCE2_Vph_Mult;
         s_STATE_UCE          <= MULT6;
      ------------------------------------------------------------------
      when MULT6 =>   --7.Takt
      ------------------------------------------------------------------
         s_UCE1_Vph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE3_ph_Mult;
         s_Mult_Input2        <= s_UCE3_Vph_Mult;
         s_STATE_UCE          <= MULT7;
      ------------------------------------------------------------------
      when MULT7 =>   --8.Takt
      ------------------------------------------------------------------
         s_UCE2_Vph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE4_ph_Mult;
         s_Mult_Input2        <= s_UCE4_Vph_Mult;
         s_STATE_UCE          <= MULT8;
      ------------------------------------------------------------------
      when MULT8 =>   --9.Takt
      ------------------------------------------------------------------
         s_UCE3_Vph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE1_ph_Mult;
         s_Mult_Input2        <= s_UCE1_Wph_Mult;
         s_STATE_UCE          <= MULT9;
      ------------------------------------------------------------------
      when MULT9 =>   --10.Takt
      ------------------------------------------------------------------
         s_UCE4_Vph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE2_ph_Mult;
         s_Mult_Input2        <= s_UCE2_Wph_Mult;
         s_STATE_UCE          <= MULT10;
      ------------------------------------------------------------------
      when MULT10 =>   --11.Takt
      ------------------------------------------------------------------
         s_UCE1_Wph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE3_ph_Mult;
         s_Mult_Input2        <= s_UCE3_Wph_Mult;
         s_STATE_UCE          <= MULT11;
      ------------------------------------------------------------------
      when MULT11 =>   --12.Takt
      ------------------------------------------------------------------
         s_UCE2_Wph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_Mult_Input1        <= s_UCE4_ph_Mult;
         s_Mult_Input2        <= s_UCE4_Wph_Mult;
         s_STATE_UCE          <= MULT12;
      ------------------------------------------------------------------
      when MULT12 =>   --13.Takt
      ------------------------------------------------------------------
         s_UCE3_Wph_Ergebniss <= s_Mult (27 downto 12);
         s_Mult               <= s_Mult_Input1 * s_Mult_Input2;
         s_STATE_UCE          <= MULT13;
      ------------------------------------------------------------------
      when MULT13 =>   --14.Takt
      -----------------------------------------------------------------
         s_UCE4_Wph_Ergebniss <= s_Mult (27 downto 12);
         s_STATE_UCE          <= MULT_OUT;
      ------------------------------------------------------------------
      when MULT_OUT =>   --15.Takt
      -----------------------------------------------------------------
         UCE1_Uph             <= s_UCE1_Uph_Ergebniss;
         UCE2_Uph             <= s_UCE2_Uph_Ergebniss;
         UCE3_Uph             <= s_UCE3_Uph_Ergebniss;
         UCE4_Uph             <= s_UCE4_Uph_Ergebniss;

         UCE1_Vph             <= s_UCE1_Vph_Ergebniss;
         UCE2_Vph             <= s_UCE2_Vph_Ergebniss;
         UCE3_Vph             <= s_UCE3_Vph_Ergebniss;
         UCE4_Vph             <= s_UCE4_Vph_Ergebniss;

         UCE1_Wph             <= s_UCE1_Wph_Ergebniss;
         UCE2_Wph             <= s_UCE2_Wph_Ergebniss;
         UCE3_Wph             <= s_UCE3_Wph_Ergebniss;
         UCE4_Wph             <= s_UCE4_Wph_Ergebniss;

         s_STATE_UCE          <= MULT1_VORBEREITEN;
         Data_Out_Valid_Uce   <= '1';

      ---------------------------------------------------------------------------
      when others =>
      ----------------------------------------------------------------------------
         s_STATE_UCE          <= MULT1_VORBEREITEN;
     END CASE;
    END IF;
  END IF;

 END PROCESS UCE_bestimmung;

END ARCHITECTURE behavioral;
