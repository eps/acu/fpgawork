-----------------------------------------------------------------------------------
-- File name: Tb_SFRS_FW.vhd                                                     --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 16/04/2024                                                         --
--                                                                               --
-- Comments : Generic SFRS_FW test bench                                         --           
--                                                                               --
-- History  : Start up version 16/04/2024                                        --
-----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


Entity Tb_SFRS_FW is
End entity Tb_SFRS_FW;

Architecture beh of Tb_SFRS_FW is
--
-- Constants declaration
constant Zeros                : std_logic_vector(23 downto 0):=x"000000";							   
--
-- Signals declaration
signal Clock                  :std_logic:='0';
signal Reset                  :std_logic:='1';
signal Enable                 :std_logic:='0';

signal n_CounterEnable        :std_logic:='1';
                                                              
signal Usteuer                : signed(15 downto 0);
signal absUsteuer             : signed(15 downto 0);
signal stdUsteuer             : std_logic_vector(15 downto 0);
signal uUsteuer               : unsigned(15 downto 0);

signal Takt_time              : unsigned(15 downto 0):=x"0000";     -- Zeit fuer eine Periode x 10ns immer +2rechnen 6002 fuer 60us
signal Tot_zeit               : unsigned(7  downto 0):=x"00";       -- Zeit zw. ein Trans. wird abgeschaltet der andered Zugeschaltet
signal time_korr              : unsigned(7  downto 0):=x"00";       -- Korrektur der Zeit fuer den pos. und neg. Aufteilung (Symetrierung der Zw.Kr. Sp) 
signal Korrektur_Vorzeichen   : std_logic:='0';                     -- = Vorzeichen fuer die Korrektur
signal HalbtaktRead           : std_logic:='0';
signal cntOut                 : std_logic_vector(15 downto 0);   
-------------------------------------------------------

signal incr                   : std_logic;
signal incr_sx                : std_logic_vector(15 downto 0);
signal Rx_NewDt2DAC           : std_logic_vector(11 downto 0);
signal Rx_Dt2DAC              : std_logic_vector(479 downto 0);
signal NewData2DAC            : std_logic_vector(1 downto 0);
signal Data2DAC               : std_logic_vector(79 downto 0);
signal DAC_En                 : std_logic:='0';

--
-- Component
Component TiefHoch_3L is                                         -- Entity Declaration
   generic(
      takt                   : integer   := 100_000_000;
      g_TimeResonanz         : integer   := 2;                -- Achtung =2 ist min!! Verschiebung vom Taktbegin zum Resonanztriger
      g_TimeInputRead        : integer   := 2);               -- Achtung =2 ist min!! Verschiebung vom Taktbegin zum Daten einlesen

   port(
      clk, reset, Enable     : in  std_logic ;                -- Mit Enable wirden die Transistoren sofort abgeschaltet
      n_CounterEnable        : in  std_logic ;                -- =0 Saegezahn - Counter laeuft
                                                              -- Zeitsollwerte (Vorgabe des Spannungsmittelwertes)
      Usteuer                : in  unsigned(15 downto 0);     -- =1 Tiefsetzsteller voll ausgesteuert; =0 Hochsetzsteller voll ausgesteuert
      Takt_time              : in  unsigned(15 downto 0);     -- Zeit fuer eine Periode x 10ns immer +2rechnen 6002 fuer 60us
      Ust_immer_lesen        : in  std_logic;                 -- = '1' , Eingang wird staendig gelesen und in dem PWM-Takt sofort korrigiert
                                                              -- = '0' nur wenn der Takt beginnt wird Usteuer eingelesen
      Tot_zeit               : in  unsigned(7  downto 0);     -- Zeit zw. ein Trans. wird abgeschaltet der andered Zugeschaltet
      time_korr              : in  unsigned(7  downto 0);     -- Korrektur der Zeit fuer den pos. und neg. Aufteilung (Symetrierung der Zw.Kr. Sp) 
      Korrektur_Vorzeichen   : in  std_logic;                 -- = Vorzeichen fuer die Korrektur
      HalbtaktRead           : in  std_logic;                 -- =1 der Eingang wird auch bei dem Halben Takt gelesen =0 nur beim ganzen Takt
     
      S1,S2,S3,S4            : out std_logic ;                -- Signale fuer Transistoren
      TH_Data_Out_Valid      : out std_logic ;                -- PWM Output =1 ein Takt ist durchgelaufen
      Resonanz_Trigger       : out std_logic ;                -- Triger fuer Resonanzkonverter
      DataRead_Trigger       : out std_logic);                -- Eingangsdaten werden eingelesen
      
end component TiefHoch_3L;

Component cntFreeWheel is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       enable           : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntFreeWheel;

Component SFRS_ButtonsMngr is
  Generic(
       gPortNr          : integer range 1 to 12:=6;                                    -- Ports number to manage
       gDt2LEMO_Width   : integer range 1 to 16:=4;                                    -- Data to LEMO width pro channel
       gDt2DAC_Width    : integer range 1 to 32:=20                                    -- Data to DAC width pro channel
       
  );
  Port (
       Clock            : in std_logic;
       Reset            : in std_logic;
       Buttons          : in std_logic_vector(9 downto 0);                             -- Active low
                                                                                       -- (9..8) DAC_Sel_UP,DAC_Sel_DW
                                                                                       -- (7..6) CH_UP,CH_DW
                                                                                       -- (5..4) CMD_UP,CMD_DW
                                                                                       -- (3) Send CMD to the selected channel
                                                                                       -- (2) Send CMD to all channels
                                                                                       -- (1..0) LEMO_Sel_UP,LEMO_Sel_DW
       Rx_Dt2LEMO       : in std_logic_vector((gPortNr*gDt2LEMO_Width)-1 downto 0);    --  With default generics: (23..20) Rx_Dt2LEMO_5
                                                                                       --                         (19..16) Rx_Dt2LEMO_4
                                                                                       --                         (15..12) Rx_Dt2LEMO_3
                                                                                       --                          (11..8) Rx_Dt2LEMO_2
                                                                                       --                           (7..4) Rx_Dt2LEMO_1
                                                                                       --                           (3..0) Rx_Dt2LEMO_0                                                                                                                                                                                                                                                                                                                
       Rx_Dt2DAC        : in std_logic_vector((gPortNr*4*gDt2DAC_Width)-1 downto 0);   -- With default generics:  (479..400) Rx_Dt2DAC_5
                                                                                       --                         (399..320) Rx_Dt2DAC_4
                                                                                       --                         (319..240) Rx_Dt2DAC_3
                                                                                       --                         (239..160) Rx_Dt2DAC_2
                                                                                       --                          (159..80) Rx_Dt2DAC_1
                                                                                       --                            (79..0) Rx_Dt2DAC_0   
       Rx_NewDt2DAC     : in std_logic_vector((gPortNr*2)-1 downto 0);                 -- With default generics:  (11) Rx_NewDt2DAC2_3 port 5
                                                                                       --                         (10) Rx_NewDt2DAC0_1 port 5
                                                                                       --                         (9)  Rx_NewDt2DAC2_3 port 4
                                                                                       --                         (8)  Rx_NewDt2DAC2_3 port 4
                                                                                       --                         (7)  Rx_NewDt2DAC2_3 port 3
                                                                                       --                         (6)  Rx_NewDt2DAC2_3 port 3
                                                                                       --                         (5)  Rx_NewDt2DAC2_3 port 2
                                                                                       --                         (4)  Rx_NewDt2DAC2_3 port 2
                                                                                       --                         (3)  Rx_NewDt2DAC2_3 port 1
                                                                                       --                         (2)  Rx_NewDt2DAC2_3 port 1
                                                                                       --                         (1)  Rx_NewDt2DAC2_3 port 0
                                                                                       --                         (0)  Rx_NewDt2DAC2_3 port 0
       SerialCom_x_Cmd  : out std_logic_vector((gPortNr*4)-1  downto 0);               -- With default generics:    (23..20) SerialCom_5_Cmd
                                                                                       --                           (19..16) SerialCom_4_Cmd
                                                                                       --                           (15..12) SerialCom_3_Cmd
                                                                                       --                            (11..8) SerialCom_2_Cmd
                                                                                       --                             (7..4) SerialCom_1_Cmd
                                                                                       --                             (3..0) SerialCom_0_Cmd
       Data2DAC_Sel     : out std_logic_vector(4 downto 0);                            -- Selector to send back                                                                                                                                                                                                                                                                                                                
       Data2LEMO_Sel    : out std_logic_vector(4 downto 0);                            -- Selector to send back
       NewData2DAC      : out std_logic_vector(1 downto 0);                            -- Selected New DAC data 
       Data2DAC         : out std_logic_vector((4*gDt2DAC_Width)-1 downto 0);          -- Selected DAC data                                                                                                                                                                                                                                                                                                                
       Data2LEMO        : out std_logic_vector(gDt2LEMO_Width-1 downto 0);             -- Selected LEMO data
       
       DAC_Sel_7SegIn   : out std_logic_vector(7 downto 0);                            -- 7-seg Driver input (2LEDs)
       Ch_Sel_7SegIn    : out std_logic_vector(3 downto 0);                            -- 7-seg Driver input (1LEDs)
       Cmd_Sel_7SegIn   : out std_logic_vector(7 downto 0);                            -- 7-seg Driver input (2LEDs)
       LEMO_Sel_7SegIn  : out std_logic_vector(7 downto 0)                             -- 7-seg Driver input (2LEDs)       
    );
end component SFRS_ButtonsMngr;

Component cntCmdInDriven is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       incrCmd          : in std_logic;
       enable           : out std_logic;
       cntOutValid      : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntCmdInDriven ;

Component ACU_AD5791_DAC_Driver is
  Generic (
         gFreeWheel              : std_logic:='1';                                           -- When 0, the conversion starts every NewDt2Wr RE pulse otherwise it is done continuosly.
         -- Chip conf
         gLinComp                : std_logic_vector(3 downto 0):=x"0";                       -- Linearity error compensation (See data sheet for more info).
         gSDODIS                 : std_logic:='1';                                           -- SDO pin enable/disable control (See data sheet for more info).
         gDACTRI                 : std_logic:='0';                                           -- DAC tristate control (See data sheet for more info).
         gOPGND                  : std_logic:='0';                                           -- Output ground clamp control (See data sheet for more info).
         gRBUF                   : std_logic:='1';                                           -- Output amplifier configuration control (See data sheet for more info).
         --SPI
         gSCLK_High              : unsigned(7 downto 0):="00000010";                         -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0):="00000010";                         -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0):="00000001";                         -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
         gUseSCLK_BK             : std_logic:='0'                                            -- When 0, SDI is sampled using SCLK as reference clock else SCLK_BK 

  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high
         Enable                  : in  std_logic;                                            -- When high, the driver operates on the SPI lines
         Ext_SPI_SlaveConnected  : in  std_logic;                                            -- It is low when a external slave card is connected to the SPI HUB
         
         NewDt2Wr                : in  std_logic;                                            -- New Data to convert/write pulse.
         Dt2Wr                   : in  std_logic_vector(19 downto 0);                        -- Data to convert/write value.
         	 
         SentProt                : out std_logic;                                            -- Pulse active high every time a protocol is sent.
                                                                                             -- It is used by the main FSM to know when drive the Enable signal.
	 	 
         SCLK                    : out std_logic;                                            -- SPI clock signal
         SCLK_BK                 : in  std_logic;                                            -- SCLK loop back (on the slave chip side) signal
         SCS_n                   : out std_logic;                                            -- Active low SPI Chip Select signal
         SDO                     : out std_logic                                             -- Serial SPI data out (from FPGA) signal
  );
end component ACU_AD5791_DAC_Driver;
begin

Clock  <= not Clock after 5 ns;


i_cntFreeWheel:cntFreeWheel
  generic map(
       gCntOutWidth     => 16,
       gEnTrigValue     => 65535
    )
  port map(
       clock            => Clock,
       reset            => Reset,
       enable           => open,
       cntOut           => cntOut
    
    );
    
Usteuer     <= signed(cntOut);
absUsteuer  <= ABS(Usteuer); 
stdUsteuer  <= std_logic_vector(absUsteuer);
uUsteuer    <= unsigned(stdUsteuer);

i_TiefHoch_3L: TiefHoch_3L

   port map(
      clk                    => Clock,
      reset                  => Reset,
      Enable                 => Enable,
      
      n_CounterEnable        => n_CounterEnable,

      Usteuer                => uUsteuer,
      Takt_time              => Takt_time,
      Ust_immer_lesen        => '0',
                                
      Tot_zeit               => Tot_zeit,
      time_korr              => time_korr, 
      Korrektur_Vorzeichen   => Korrektur_Vorzeichen,
      HalbtaktRead           => HalbtaktRead,
           
      S1                     => open,
      S2                     => open,
      S3                     => open,
      S4                     => open,
      TH_Data_Out_Valid      => open,
      Resonanz_Trigger       => open,
      DataRead_Trigger       => open
);

-----------------------------------------------------------------------------
i_cntFreeWheel5us:cntFreeWheel
  generic map(
       gCntOutWidth     => 16,
       gEnTrigValue     => 500
    )
  port map(
       clock            => Clock,
       reset            => Reset,
       enable           => incr,
       cntOut           => open
    
    );
    
p_samplingIncr:process(Clock,Reset)
begin
  if (Reset='1') then
    incr_sx  <= (others=>'0');
  elsif(Clock'event and Clock='1') then
    incr_sx  <= incr_sx(14 downto 0) & incr;
  end if;
end process p_samplingIncr;

i_cntCmdInDriven_0: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(0),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(0),
       cntOut           => Rx_Dt2DAC(39 downto 0)
    );
i_cntCmdInDriven_1: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(1),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(1),
       cntOut           => Rx_Dt2DAC(79 downto 40)
    );
    
i_cntCmdInDriven_2: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(2),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(2),
       cntOut           => Rx_Dt2DAC(119 downto 80)
    );
i_cntCmdInDriven_3: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(3),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(3),
       cntOut           => Rx_Dt2DAC(159 downto 120)
    );
i_cntCmdInDriven_4: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(4),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(4),
       cntOut           => Rx_Dt2DAC(199 downto 160)
    );
i_cntCmdInDriven_5: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(5),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(5),
       cntOut           => Rx_Dt2DAC(239 downto 200)
    );
i_cntCmdInDriven_6: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(6),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(6),
       cntOut           => Rx_Dt2DAC(279 downto 240)
    );
i_cntCmdInDriven_7: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(7),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(7),
       cntOut           => Rx_Dt2DAC(319 downto 280)
    );
i_cntCmdInDriven_8: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(8),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(8),
       cntOut           => Rx_Dt2DAC(359 downto 320)
    );
i_cntCmdInDriven_9: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(9),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(9),
       cntOut           => Rx_Dt2DAC(399 downto 360)
    );
i_cntCmdInDriven_10: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(10),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(10),
       cntOut           => Rx_Dt2DAC(439 downto 400)
    );
i_cntCmdInDriven_11: cntCmdInDriven
  generic map(
       gCntOutWidth     => 40,
       gEnTrigValue     => 10
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr_sx(11),
       enable           => open,
       cntOutValid      => Rx_NewDt2DAC(11),
       cntOut           => Rx_Dt2DAC(479 downto 440)
    );


i_SFRS_ButtonsMngr: SFRS_ButtonsMngr 
  Port map(
       Clock            => Clock,
       Reset            => Reset,
       Buttons          => Zeros(9 downto 0),
       Rx_Dt2LEMO       => Zeros(23 downto 0),                                                                                                                                                                                                                                                                                                                
       Rx_Dt2DAC        => Rx_Dt2DAC,
       Rx_NewDt2DAC     => Rx_NewDt2DAC,
       SerialCom_x_Cmd  => open,
       Data2DAC_Sel     => open,                                                                                                                                                                                                                                                                                                 
       Data2LEMO_Sel    => open,
       NewData2DAC      => NewData2DAC,
       Data2DAC         => Data2DAC,                                                                                                                                                                                                                                                                                                        
       Data2LEMO        => open,
       
       DAC_Sel_7SegIn   => open,
       Ch_Sel_7SegIn    => open,
       Cmd_Sel_7SegIn   => open,
       LEMO_Sel_7SegIn  => open       
    );

i_InputDrivenDAC_0: ACU_AD5791_DAC_Driver 
  Generic map (
         gFreeWheel              => '0'
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         Enable                  => DAC_En,
         Ext_SPI_SlaveConnected  => '0',
         
         NewDt2Wr                => NewData2DAC(0),
         Dt2Wr                   => Data2DAC(19 downto 0),
         	 
         SentProt                => open,
         	 	 
         SCLK                    => open,
         SCLK_BK                 => '0',
         SCS_n                   => open,
         SDO                     => open
  );
  
i_InputDrivenDAC_1: ACU_AD5791_DAC_Driver 
  Generic map (
         gFreeWheel              => '0'
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         Enable                  => DAC_En,
         Ext_SPI_SlaveConnected  => '0',
         
         NewDt2Wr                => NewData2DAC(0),
         Dt2Wr                   => Data2DAC(39 downto 20),
         	 
         SentProt                => open,
         	 	 
         SCLK                    => open,
         SCLK_BK                 => '0',
         SCS_n                   => open,
         SDO                     => open
  );
i_InputDrivenDAC_2: ACU_AD5791_DAC_Driver 
  Generic map (
         gFreeWheel              => '0'
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         Enable                  => DAC_En,
         Ext_SPI_SlaveConnected  => '0',
         
         NewDt2Wr                => NewData2DAC(1),
         Dt2Wr                   => Data2DAC(59 downto 40),
         	 
         SentProt                => open,
         	 	 
         SCLK                    => open,
         SCLK_BK                 => '0',
         SCS_n                   => open,
         SDO                     => open
  );
  
i_InputDrivenDAC_3: ACU_AD5791_DAC_Driver 
  Generic map (
         gFreeWheel              => '0'
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         Enable                  => DAC_En,
         Ext_SPI_SlaveConnected  => '0',
         
         NewDt2Wr                => NewData2DAC(1),
         Dt2Wr                   => Data2DAC(79 downto 60),
         	 
         SentProt                => open,
         	 	 
         SCLK                    => open,
         SCLK_BK                 => '0',
         SCS_n                   => open,
         SDO                     => open
  );
end beh;
