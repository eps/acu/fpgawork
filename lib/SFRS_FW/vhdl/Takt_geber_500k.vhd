-- Filename: Takt_geber.vhd, Wiest 06.05.2020
-- geaendert 21.02.2022

--  Beschreibung:
--  Teilt den Takt Runter auf die Vorgegebene Zahl
--
--  Eing. Groessen:               
--      clk                 Takt
--      Enable              Gueltigkeit der Eingangsdaten 
--      reset               Berechnung neue starten
--
--  Ausg. Groessen: 
--        trigger           Trigger 

-------------------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY Takt_geber_500k IS
   GENERIC( 
      takt                : integer  := 100_000_000;   -- Initialisierung 100 MHz
      n_takte             : integer  := 6000);         -- Taktzeit 60us 

   PORT(
      clk, reset,  Enable : in  std_logic ;
      trigger             : out std_logic:= '0');      -- Ausgang
END Takt_geber_500k ;

--============================================================================
ARCHITECTURE behavioral OF Takt_geber_500k IS

      SIGNAL s_counter   : UNSIGNED(23 DOWNTO 0):=(others=>'0'); 
      
      TYPE T_STATE_TIMING  IS(WAITFORENABLE,TRIGGERING);
      SIGNAL s_STATE_TIMING:   T_STATE_TIMING := WAITFORENABLE;


BEGIN --===========================================================================

timing: process (clk, reset, Enable) 
 BEGIN 
      if (reset = '1') then
         trigger                 <='0';
         s_counter               <=(others=>'0');
         s_STATE_TIMING          <= WAITFORENABLE;
      else
         if rising_edge(clk)   THEN
            CASE s_STATE_TIMING IS
               ------------------------------------------------------------------ 
               when WAITFORENABLE =>   --1.Takt
               ------------------------------------------------------------------
                  if Enable = '1' THEN
                     s_STATE_TIMING <= TRIGGERING;
                     trigger        <='1';
                  else
                     trigger        <='0'; 
                  end if;
                  
               ------------------------------------------------------------------ 
               when TRIGGERING =>   --2.Takt
               ------------------------------------------------------------------
                  IF s_counter < to_unsigned(n_takte,24)-2 THEN
                     trigger        <='0';
                     s_counter      <= s_counter + 1;    
                  ELSE
                     s_STATE_TIMING <= WAITFORENABLE;
                     s_counter      <= (others=>'0');          
                  END IF;     
            END CASE;  
         end if;
      end if;

END PROCESS timing;
END ARCHITECTURE behavioral;
