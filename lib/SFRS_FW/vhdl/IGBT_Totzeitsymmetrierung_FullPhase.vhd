--		Aufgabe dieses Blocks ist es, mehrere SingleIGBT Totzeitsymmetrierungsmodule zusammenzufassen.

--		deltaIGBTOffOn=(tdOff-tdOn)+(tf-tr)/2
--    tdOff: Ausschaltdelay
-- 	tdOn : Einschaltdelay
--		tf	  : Abfalldauer (wird deswegen nur halb eingerechnet, da hier der Strom linear abnimmt)
--		tr	  : Anstiegsdauer (wird deswegen nur halb eingerechnet, da hier der Strom linear zunimmt)

--    Alle Zeiten sind dabei in Takten bezogen auf 100MHz anzugeben -> 10 ms = 1000 Takte
--		Feineinstellungen sind trotz Berechnung der Parameter notwendig
LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY IGBT_Totzeitsymmetrierung_FullPhase IS

   GENERIC(           
      --Initialisierung 100 MHz
      takt                  : integer  := 100_000_000  ); 
    
   PORT(
      --System-Eingänge:
      clk, reset, 
      enable                : in std_logic ;
        
      --Eingänge:
      IGBT_Schalter1IN      : in std_logic;
		IGBT_Schalter2IN      : in std_logic;
		IGBT_Schalter3IN      : in std_logic;
		IGBT_Schalter4IN      : in std_logic;
      deltaIGBTOffOn        : in unsigned (15 downto 0);
      tOffgreatertOn        : in std_logic;
      
      --Ausgänge (nur positive Werte)
      IGBT_Schalter1OUT     : out std_logic := '0';
		IGBT_Schalter2OUT     : out std_logic := '0';
		IGBT_Schalter3OUT     : out std_logic := '0';
		IGBT_Schalter4OUT     : out std_logic := '0');

END IGBT_Totzeitsymmetrierung_FullPhase ;

--============================================================================
ARCHITECTURE behavioral OF IGBT_Totzeitsymmetrierung_FullPhase IS

component IGBT_Totzeitsymmetrierung_SingleIGBT
   port(clk,reset,enable    : in  std_LOGIC;
      IGBT_SchalterIN       : in  std_logic;
      deltaIGBTOffOn        : in  unsigned (15 downto 0);
	   tOffgreatertOn        : in  std_logic;
      IGBT_SchalterOUT      : out std_logic := '0'); 
   end component;


begin 
Sym1: IGBT_Totzeitsymmetrierung_SingleIGBT port map
   (clk => clk, reset => reset, enable => enable, deltaIGBTOffOn=> deltaIGBTOffOn, tOffgreatertOn=> tOffgreatertOn, IGBT_SchalterIN => IGBT_Schalter1IN, IGBT_SchalterOUT=> IGBT_Schalter1OUT);
	 
Sym2: IGBT_Totzeitsymmetrierung_SingleIGBT port map
   (clk => clk, reset => reset, enable => enable, deltaIGBTOffOn=> deltaIGBTOffOn, tOffgreatertOn=> tOffgreatertOn, IGBT_SchalterIN => IGBT_Schalter2IN, IGBT_SchalterOUT=> IGBT_Schalter2OUT);
	 
Sym3: IGBT_Totzeitsymmetrierung_SingleIGBT port map
   (clk => clk, reset => reset, enable => enable, deltaIGBTOffOn=> deltaIGBTOffOn, tOffgreatertOn=> tOffgreatertOn, IGBT_SchalterIN => IGBT_Schalter3IN, IGBT_SchalterOUT=> IGBT_Schalter3OUT);
	 
Sym4: IGBT_Totzeitsymmetrierung_SingleIGBT port map
   (clk => clk, reset => reset, enable => enable, deltaIGBTOffOn=> deltaIGBTOffOn, tOffgreatertOn=> tOffgreatertOn, IGBT_SchalterIN => IGBT_Schalter4IN, IGBT_SchalterOUT=> IGBT_Schalter4OUT);
------------------------------------------------------------------------------------

end architecture behavioral;