-- Beschreibung:
-- Diese VHDL Datei berechnet den Kurvenverlauf z.B. der Saettigung von Magneten L(i) oder IGBTs. Dafuer werden Stuetzstellen aus dem Flash linearisiert.
--
-- Der Block implementiert folgende Gleichung:
-- L = (y(k+1)-y(k))*(Iph-IStuetz(k)) + b0(k)
-- die Schritte in der x-Achse sind allerdings unterschiedlich (fein biem 0 und werden dann in den Bloecken immer grober)

-- Eing. Groessen: 
-- clk bspw. 100MHz system clock
-- reset Berechnung neu starten, alles auf Null setzen
-- Enable symbolisiert Gueltigkeit der Eingangsdaten

-- Iph       SIGNED    16Bit   
-- yPunkte   UNSIGNED 14 Bit
-- Ausg. Groessen:
-- Uvorsteuerung SIGNED 16 Bit
--========================================================================

LIBRARY ieee;
USE     ieee.std_logic_1164.ALL;
USE     ieee.numeric_std.ALL;

ENTITY Sollwertlinearisierung16Bit IS
   GENERIC ( takt         : INTEGER     := 100_000_000;                  -- Initialisierung 100 MHz
             Aus_max      : INTEGER     := 7500 );                       -- Maximaler werd der An PWM geht
   PORT   (
      clk, reset, Enable  : IN  STD_LOGIC;                               -- System-Eingaenge
      
      Isoll               : IN  SIGNED(15 DOWNTO 0):= (OTHERS => '0');   -- Stromsollwert 32767 entspr. (11/10)*315A  oder  (11/10)*670A
      Uind                : IN  SIGNED(15 DOWNTO 0):= (OTHERS => '0');   -- induktive Lastspannung
      Rippel_50Hz         : IN  SIGNED(15 DOWNTO 0):= (OTHERS => '0');   -- Transistorspannung
      
      ReglerSpannung      : IN  SIGNED(15 DOWNTO 0):= (OTHERS => '0');   -- im idealfall =0
      Faktor              : IN  SIGNED(15 DOWNTO 0):= (OTHERS => '0');   -- Multiplikationsfaktor x Isoll 16383entspr.0,5 32367 entspr. 1
      SPI_Steuerwert      : IN  SIGNED(15 DOWNTO 0):= (OTHERS => '0');   -- 
      SPI_aktiv           : IN  STD_LOGIC;                               -- =0 von dem SPI werden kein Steuerwert eingelesen
                                                                         -- =1 wird nur von dem SPI Steuerwert eingelesen
      Ausgleichsregler    : IN  SIGNED(15 DOWNTO 0):= (OTHERS => '0');   -- falls von dem SPI Steuerwert kommt, wird dazu auch der Ausgleichsregler  addiert                                                               

      Uvorsteuerung       : OUT signed(15 downto 0):= (others => '0');   -- nur Vorsteuerspannung
      UsteuerGesamt       : OUT signed(15 downto 0):= (others => '0');   -- Vorsteuerspannung + Reglerspannung
      UsteuerGesamt_abs   : OUT signed(15 downto 0):= (others => '0');
      
      Data_Out_Valid      : OUT STD_LOGIC          := '0'   );

   END Sollwertlinearisierung16Bit;

   ARCHITECTURE behavioral OF Sollwertlinearisierung16Bit IS
      TYPE yStuetzstellen IS ARRAY(0 TO 46) OF UNSIGNED(14 DOWNTO 0); -- Vorsteuerung
      TYPE xStuetzstellen IS ARRAY(0 TO 46) OF SIGNED  (15 DOWNTO 0); -- Sollwert
      
      CONSTANT yPunkte   : yStuetzstellen := (                         -- Punkte werden ausgemessen
         to_unsigned ( 197, 15),
         to_unsigned ( 252, 15),
         to_unsigned ( 256, 15),
         to_unsigned ( 256, 15),
         to_unsigned ( 258, 15),
         to_unsigned ( 259, 15),
         to_unsigned ( 260, 15),
         to_unsigned ( 262, 15),
         
         to_unsigned ( 263, 15),
         to_unsigned ( 268, 15),
         to_unsigned ( 273, 15),
         to_unsigned ( 279, 15),
         to_unsigned ( 289, 15),
         to_unsigned ( 305, 15),
         
         to_unsigned ( 305, 15),
         to_unsigned ( 413, 15),
         to_unsigned ( 531, 15),
         to_unsigned ( 655, 15),
         to_unsigned ( 786, 15),
         to_unsigned ( 852, 15),
         to_unsigned ( 983, 15),
         to_unsigned ( 1075, 15),
         to_unsigned ( 1212, 15),
         to_unsigned ( 1311, 15),
         to_unsigned ( 1409, 15),
         to_unsigned ( 1524, 15),
         to_unsigned ( 1638, 15),
         to_unsigned ( 1769, 15),
         to_unsigned ( 1835, 15),
         to_unsigned ( 1966, 15),
         to_unsigned ( 2097, 15),
         to_unsigned ( 2228, 15),
         
         to_unsigned ( 2425, 15),
         to_unsigned ( 2654, 15),
         to_unsigned ( 2916, 15),
         to_unsigned ( 3080, 15),
         to_unsigned ( 3309, 15),
         to_unsigned ( 3441, 15),
         
         to_unsigned ( 3867, 15),
         to_unsigned ( 4587, 15),
         to_unsigned ( 4915, 15),
         to_unsigned ( 5308, 15),
         to_unsigned ( 5734, 15),
         to_unsigned ( 6062, 15),
         to_unsigned ( 6455, 15),
         to_unsigned ( 6881, 15),
         to_unsigned ( 7045, 15));

      CONSTANT xPunkte   : xStuetzstellen := ( 
         to_signed ( 0,    16), --16
         to_signed ( 16,   16),
         to_signed ( 32,   16),
         to_signed ( 48,   16),
         to_signed ( 64,   16),
         to_signed ( 80,   16),
         to_signed ( 96,   16),
         to_signed ( 112,  16),
         
         to_signed ( 128,  16), --64
         to_signed ( 192,  16),
         to_signed ( 256,  16),
         to_signed ( 320,  16),
         to_signed ( 384,  16),
         to_signed ( 448,  16),
       
         to_signed ( 512, 16), --512
         to_signed ( 1024, 16),
         to_signed ( 1536, 16),
         to_signed ( 2048, 16),
         to_signed ( 2560, 16),
         to_signed ( 3072, 16),
         to_signed ( 3584, 16),
         to_signed ( 4096, 16),
         to_signed ( 4608, 16),
         to_signed ( 5120, 16),
         to_signed ( 5632, 16),
         to_signed ( 6144, 16),
         to_signed ( 6656, 16),
         to_signed ( 7168, 16),
         to_signed ( 7680, 16),
         to_signed ( 8192, 16),
         to_signed ( 8704, 16),
         to_signed ( 9216, 16),
         
         to_signed ( 10240, 16), --1024
         to_signed ( 11264, 16),
         to_signed ( 12288, 16),
         to_signed ( 13312, 16),
         to_signed ( 14336, 16),
         to_signed ( 15360, 16),
         
         to_signed ( 17408, 16),
         to_signed ( 19456, 16),
         to_signed ( 21504, 16),
         to_signed ( 23552, 16),
         to_signed ( 25600, 16),
         to_signed ( 27648, 16),
         to_signed ( 29696, 16),
         to_signed ( 31744, 16),
         to_signed ( 32767, 16));  -- Achtung hier ist der Zwischenwert nur  1023

      CONSTANT Faktor1_1         : SIGNED(15 DOWNTO 0)  := to_signed (18022,16);    --2-> 32767 1,1 entspricht 1,1/2x32767
      
      SIGNAL s_Kennlinie         : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uind_Kennlinie    : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Uvorsteuerung     : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      
      
      SIGNAL s_UsteuerGesamt     : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_SPI_Ausgleich     : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
            
      SIGNAL s_absIsoll          : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      
      SIGNAL Isoll_diff          : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Summe             : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      
      SIGNAL s_b0                : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_deltaY            : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
   
      SIGNAL s_Mult1             : SIGNED(31 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Mult_Input1       : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Mult_Input2       : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Ergebnis          : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Sign              : STD_LOGIC            := '0';      
      
      SIGNAL s_kBereich          : INTEGER RANGE 0 TO 45:= 0;
      
      TYPE T_STATE_BERECHNUNG IS(EINGANG_SPEICHERN, --Zustaende---------
                                 EINGANG_SKALIEREN,
                                 SKALIEREN_ERGEBNIS,
                                 BEREICH_BESTIMMEN, 
                                 GERADE_BESTIMMEN,
                                 MULT1_VORB, MULT1,
                                 ADDITION,
                                 MULT2_VORB,MULT2,MULT2_ERGEBNIS,
                                 VORZEICHEN, INDUKTIVITAET, TRANSISTOR,
                                 ADDITION_REGLER,
                                 ABS_AUSGANG,
                                 DATA_VALID);
      SIGNAL s_STATE : T_STATE_BERECHNUNG := EINGANG_SPEICHERN;
 
   BEGIN

      berechnung : PROCESS (clk, reset, Enable)
      BEGIN
      
         IF (reset = '1') THEN
            Uvorsteuerung       <= (OTHERS => '0');
            s_Uvorsteuerung     <= (OTHERS => '0');
            
            UsteuerGesamt       <= (OTHERS => '0');
            UsteuerGesamt_abs   <= (OTHERS => '0');
            s_UsteuerGesamt     <= (OTHERS => '0');

            Data_Out_Valid      <= '0';
            s_STATE             <= EINGANG_SPEICHERN;
 
         ELSE
            IF rising_edge(clk) THEN   -- ggf. Outputs zuweisen
               CASE s_STATE IS
                  ------------------------------------------------------------------
                  WHEN EINGANG_SPEICHERN => --1.Takt
                  ------------------------------------------------------------------ 
                     IF (Enable = '1') THEN
                        IF (Isoll  ) < 0 then s_Sign   <='1';  -- Reglerspannung wird nicht liniarisiert
                        ELSE                  s_Sign   <='0';
                        END IF;
                           
                        IF    Isoll > 29787 OR Isoll < -29787 then  s_Mult_Input1 <= to_signed(29788,16); 
                        ELSE                                        s_Mult_Input1 <= ABS(Isoll);
                        END IF;

                        s_Mult_Input2  <= Faktor1_1;     -- x 1,1
                        s_STATE        <= EINGANG_SKALIEREN; 
                     END IF;
                     Data_Out_Valid    <= '0';
                     
                     
                  ------------------------------------------------------------------
                  WHEN EINGANG_SKALIEREN => --2.Takt
                  ------------------------------------------------------------------ 
                     s_Mult1    <= s_Mult_Input1 * s_Mult_Input2;
                     s_STATE    <= SKALIEREN_ERGEBNIS; 
 
                  ------------------------------------------------------------------
                  WHEN SKALIEREN_ERGEBNIS => --3.Takt
                  ------------------------------------------------------------------ 
                     if  s_Mult1(31-1 downto 16-2) > 32766 then s_absIsoll <= to_signed(32767,16); 
                     else                                       s_absIsoll <= s_Mult1(31-2 downto 16-2); 
                     end if;
                     s_STATE    <= BEREICH_BESTIMMEN; 
                     
                  ------------------------------------------------------------------
                  WHEN BEREICH_BESTIMMEN => --4.Takt
                  ------------------------------------------------------------------ 
                     IF    s_absIsoll > xPunkte(45) THEN   s_kBereich <= 45;
                     ELSIF s_absIsoll > xPunkte(44) THEN   s_kBereich <= 44; -- Bestimmt den Bereich in Abhaengigkeit der x-Achsen Stützpunkte
                     ELSIF s_absIsoll > xPunkte(43) THEN   s_kBereich <= 43;
                     ELSIF s_absIsoll > xPunkte(42) THEN   s_kBereich <= 42;
                     ELSIF s_absIsoll > xPunkte(41) THEN   s_kBereich <= 41;
                     ELSIF s_absIsoll > xPunkte(40) THEN   s_kBereich <= 40;
                     ELSIF s_absIsoll > xPunkte(39) THEN   s_kBereich <= 39;
                     ELSIF s_absIsoll > xPunkte(38) THEN   s_kBereich <= 38;
                     
                     ELSIF s_absIsoll > xPunkte(37) THEN   s_kBereich <= 37;
                     ELSIF s_absIsoll > xPunkte(36) THEN   s_kBereich <= 36;
                     ELSIF s_absIsoll > xPunkte(35) THEN   s_kBereich <= 35;
                     ELSIF s_absIsoll > xPunkte(34) THEN   s_kBereich <= 34;
                     ELSIF s_absIsoll > xPunkte(33) THEN   s_kBereich <= 33;
                     ELSIF s_absIsoll > xPunkte(32) THEN   s_kBereich <= 32;

                     ELSIF s_absIsoll > xPunkte(31) THEN   s_kBereich <= 31;
                     ELSIF s_absIsoll > xPunkte(30) THEN   s_kBereich <= 30;
                     ELSIF s_absIsoll > xPunkte(29) THEN   s_kBereich <= 29;
                     ELSIF s_absIsoll > xPunkte(28) THEN   s_kBereich <= 28;
                     ELSIF s_absIsoll > xPunkte(27) THEN   s_kBereich <= 27;
                     ELSIF s_absIsoll > xPunkte(26) THEN   s_kBereich <= 26;
                     ELSIF s_absIsoll > xPunkte(25) THEN   s_kBereich <= 25;
                     ELSIF s_absIsoll > xPunkte(24) THEN   s_kBereich <= 24;
                     ELSIF s_absIsoll > xPunkte(23) THEN   s_kBereich <= 23;
                     ELSIF s_absIsoll > xPunkte(22) THEN   s_kBereich <= 22;
                     ELSIF s_absIsoll > xPunkte(21) THEN   s_kBereich <= 21;
                     ELSIF s_absIsoll > xPunkte(20) THEN   s_kBereich <= 20;
                     ELSIF s_absIsoll > xPunkte(19) THEN   s_kBereich <= 19;
                     ELSIF s_absIsoll > xPunkte(18) THEN   s_kBereich <= 18;
                     ELSIF s_absIsoll > xPunkte(17) THEN   s_kBereich <= 17;
                     ELSIF s_absIsoll > xPunkte(16) THEN   s_kBereich <= 16;
                     ELSIF s_absIsoll > xPunkte(15) THEN   s_kBereich <= 15;
                     ELSIF s_absIsoll > xPunkte(14) THEN   s_kBereich <= 14;
                     
                     ELSIF s_absIsoll > xPunkte(13) THEN   s_kBereich <= 13;
                     ELSIF s_absIsoll > xPunkte(12) THEN   s_kBereich <= 12;
                     ELSIF s_absIsoll > xPunkte(11) THEN   s_kBereich <= 11;
                     ELSIF s_absIsoll > xPunkte(10) THEN   s_kBereich <= 10;
                     ELSIF s_absIsoll > xPunkte(9)  THEN   s_kBereich <= 9 ;
                     ELSIF s_absIsoll > xPunkte(8)  THEN   s_kBereich <= 8 ;
                     
                     ELSIF s_absIsoll > xPunkte(7)  THEN   s_kBereich <= 7 ;
                     ELSIF s_absIsoll > xPunkte(6)  THEN   s_kBereich <= 6 ;
                     ELSIF s_absIsoll > xPunkte(5)  THEN   s_kBereich <= 5 ;
                     ELSIF s_absIsoll > xPunkte(4)  THEN   s_kBereich <= 4 ;
                     ELSIF s_absIsoll > xPunkte(3)  THEN   s_kBereich <= 3 ;
                     ELSIF s_absIsoll > xPunkte(2)  THEN   s_kBereich <= 2 ;
                     ELSIF s_absIsoll > xPunkte(1)  THEN   s_kBereich <= 1 ;
                     ELSE                                  s_kBereich <= 0 ;
                     END IF;                     
                     s_STATE <= GERADE_BESTIMMEN;

                  ------------------------------------------------------------------
                  WHEN GERADE_BESTIMMEN => --5.Takt

                  ------------------------------------------------------------------                  -- Y-Werte fuer den Transistorumkehrfunktion
                     s_b0           <= signed('0'        & std_LOGIC_VECTOR(yPunkte(s_kBereich    )));  -- Ermittlung des Wertes b0 aus dem aktuellen Bereich (Wert der unteren Stütze)
                     s_deltaY       <= resize(signed('0' & std_LOGIC_VECTOR(yPunkte(s_kBereich + 1))) 
                                      - signed('0'       & std_LOGIC_VECTOR(yPunkte(s_kBereich))),16);  -- Differenz der y-Achse zwischen den beiden Stützstellen (y2-y1)
                     Isoll_diff     <= s_absIsoll - xPunkte(s_kBereich);                                -- Differenz des Stromes zur Stuetzstelle
                     s_STATE        <= MULT1_VORB;
                     
                  ------------------------------------------------------------------
                  WHEN MULT1_VORB => --6.Takt
                  ------------------------------------------------------------------                         
                     s_Mult_Input1  <= s_deltaY; 
                     s_Mult_Input2  <= Isoll_diff;     -- Differenz des Stromes zur Stuetzstelle
                     s_STATE        <= MULT1;

                  ------------------------------------------------------------------
                  WHEN MULT1 => --7.Takt
                  ------------------------------------------------------------------ 
                     s_Mult1       <= s_Mult_Input1 * s_Mult_Input2;
                     s_STATE       <= ADDITION;
 
                  ------------------------------------------------------------------
                  WHEN ADDITION => --8.Takt
                  ------------------------------------------------------------------ 
                     IF    (s_kBereich > 44) THEN   s_Summe <= s_b0 + s_Mult1(31-1-5  downto 15-5 ); -- 10Bit *16 Bit
                     ELSIF (s_kBereich > 36) THEN   s_Summe <= s_b0 + s_Mult1(31-1-4  downto 15-4 ); -- 10Bit *16 Bit
                     ELSIF (s_kBereich > 30) THEN   s_Summe <= s_b0 + s_Mult1(31-1-5  downto 15-5 ); -- 10Bit *16 Bit
                     ELSIF (s_kBereich > 13) THEN   s_Summe <= s_b0 + s_Mult1(31-1-6  downto 15-6 ); -- 10Bit *16 Bit
                     ELSIF (s_kBereich > 7 ) THEN   s_Summe <= s_b0 + s_Mult1(31-1-9  downto 15-9 ); -- 10Bit *16 Bit
                     ELSE                           s_Summe <= s_b0 + s_Mult1(31-1-11 downto 15-11); -- 10Bit *16 Bit
                     END IF;
                     s_STATE         <= MULT2_VORB;
                  
                  ------------------------------------------------------------------
                  WHEN MULT2_VORB => --9.Takt
                  ------------------------------------------------------------------ 
                     s_Mult_Input1  <= Faktor; 
                     s_Mult_Input2  <= s_Summe;                  
                     s_STATE        <= MULT2;
                  
                  ------------------------------------------------------------------
                  WHEN MULT2 => --10.Takt
                  ------------------------------------------------------------------ 
                     s_Mult1        <= s_Mult_Input1 * s_Mult_Input2; -- 16Bit *16 Bit
                     s_STATE        <= MULT2_ERGEBNIS;
                     
                  ------------------------------------------------------------------
                  WHEN MULT2_ERGEBNIS => --11.Takt
                  ------------------------------------------------------------------ 
                     s_Ergebnis     <= s_Mult1(30  downto 15); 
                     s_STATE        <= VORZEICHEN;
                     
                  ------------------------------------------------------------------
                  WHEN VORZEICHEN => --12.Takt
                  ------------------------------------------------------------------ 
                     IF s_Sign = '1' THEN --Vorzeichen negativ
                        if s_Mult1(31 downto 15) > 32766 then s_Kennlinie  <= to_signed(-32767,16);
                        else                                  s_Kennlinie  <= NOT(s_Ergebnis) + 1;
                        end if;
                     
                     ELSE -- Vorzeichen Positiv
                        if s_Mult1(31 downto 15) > 32766 then s_Kennlinie  <= to_signed(32767,16);
                        else                                  s_Kennlinie  <= s_Ergebnis;
                        end if;                  
                     END IF;

                        s_STATE     <= INDUKTIVITAET ;   
                        
                   ------------------------------------------------------------------
                  WHEN INDUKTIVITAET => --13.Takt
                  ------------------------------------------------------------------ 
                     if    (s_Kennlinie + Uind >  Aus_max) then s_Uind_Kennlinie  <= to_signed( Aus_max,16);
                     elsif (s_Kennlinie + Uind < -Aus_max) then s_Uind_Kennlinie  <= to_signed(-Aus_max,16);
                     else                                       s_Uind_Kennlinie  <= s_Kennlinie + Uind;
                     end if;    
                     
                     s_STATE    <= TRANSISTOR;     

                   ------------------------------------------------------------------
                  WHEN TRANSISTOR => --14.Takt
                  ------------------------------------------------------------------ 
                     if (s_Uind_Kennlinie > 0) then --Kennlinie + Ind. Spannung groesser Null
                        if    (s_Uind_Kennlinie + Rippel_50Hz >  Aus_max) then s_Uvorsteuerung <= to_signed( Aus_max, 16);
                        else                                                   s_Uvorsteuerung <= s_Uind_Kennlinie + Rippel_50Hz;
                        end if;
                     
                     else                           --Kennlinie + Ind. Spannung kleiner Null
                        if (s_Uind_Kennlinie - Rippel_50Hz < -Aus_max) then    s_Uvorsteuerung <= to_signed(-Aus_max, 16);
                        else                                                   s_Uvorsteuerung <= s_Uind_Kennlinie - Rippel_50Hz;
                        end if;
                     end if;
                     
                     s_STATE             <= ADDITION_REGLER ;
                  
                  ------------------------------------------------------------------
                  WHEN ADDITION_REGLER => --15.Takt
                  ------------------------------------------------------------------ 
                     if    (s_Uvorsteuerung + ReglerSpannung >  Aus_max) then s_UsteuerGesamt  <= to_signed( Aus_max,16);
                     elsif (s_Uvorsteuerung + ReglerSpannung < -Aus_max) then s_UsteuerGesamt  <= to_signed(-Aus_max,16);
                     else                                                     s_UsteuerGesamt  <= s_Uvorsteuerung + ReglerSpannung;
                     end if;    
                     
                     if    (SPI_Steuerwert + Ausgleichsregler >  Aus_max) then s_SPI_Ausgleich <= to_signed( Aus_max,16);
                     elsif (SPI_Steuerwert + Ausgleichsregler < -Aus_max) then s_SPI_Ausgleich <= to_signed(-Aus_max,16);
                     else                                                      s_SPI_Ausgleich <= SPI_Steuerwert + Ausgleichsregler;
                     end if; 
                     
                     s_STATE             <= ABS_AUSGANG;
                        
                  ------------------------------------------------------------------
                  WHEN ABS_AUSGANG => --16.Takt
                  ------------------------------------------------------------------       
                     if SPI_aktiv ='0' then
                        UsteuerGesamt     <=     s_UsteuerGesamt ;
                        UsteuerGesamt_abs <= ABS(s_UsteuerGesamt);
                     else
                        UsteuerGesamt     <=     s_SPI_Ausgleich ;
                        UsteuerGesamt_abs <= ABS(s_SPI_Ausgleich);
                     end if;
                     Uvorsteuerung        <= s_Uvorsteuerung;
                     s_STATE              <= DATA_VALID ;
                     
                  ---------------------------------------------------------------------------------
                  when DATA_VALID =>     --17. Takt
                  ---------------------------------------------------------------------------------  
                     Data_Out_Valid   <= '1';
                     s_STATE          <= EINGANG_SPEICHERN ;

                  ---------------------------------------------------------------------------------
                  WHEN OTHERS => 
                  ----------------------------------------------------------------------------------
                     s_STATE          <= EINGANG_SPEICHERN; 

               END CASE;
            END IF;
         END IF;
      END PROCESS berechnung;

END ARCHITECTURE behavioral;

