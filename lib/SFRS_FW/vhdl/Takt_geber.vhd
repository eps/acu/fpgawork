--  Filename: Takt_geber.vhd, Wiest 06.05.2020
--
--  Beschreibung:
--  Teilt den Takt Runter auf die Vorgegebene Zahl
--
--  Eing. Groessen:               
--      clk                 Takt
--      Enable              Gueltigkeit der Eingangsdaten 
--      reset               Berechnung neue starten
--
--  Ausg. Groessen: 
--        trigger           Trigger 

-------------------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY Takt_geber IS
   GENERIC( 
      takt              : integer  := 100_000_000;   -- Initialisierung 100 MHz
      n_takte           : integer  := 1000);         -- Anzahl der Takte, Achtung maxwert ist 65535 

   PORT(
      --System-Eingaenge: 
      clk, reset, 
      Enable            : in std_logic ;
      
      --Ausgaenge
      trigger           : out std_logic:= '0');
END Takt_geber ;

--============================================================================
ARCHITECTURE behavioral OF Takt_geber IS

   SIGNAL s_counter     : UNSIGNED(16 DOWNTO 0):=(others=>'0'); 

   TYPE T_STATE_TIMING  IS(WAITFORENABLE, TRIGGERING);
   SIGNAL s_STATE_TIMING:   T_STATE_TIMING := WAITFORENABLE;

BEGIN --===========================================================================

timing: process (clk, reset) 
 BEGIN 
      if (reset = '1') then
         s_counter               <=(others=>'0');
         trigger                 <='0';
         s_STATE_TIMING          <= WAITFORENABLE;
      else
         if rising_edge(clk)   THEN
            CASE s_STATE_TIMING IS
               ------------------------------------------------------------------ 
               when WAITFORENABLE =>   --1.Takt
               ------------------------------------------------------------------
                  if Enable = '1' THEN
                     s_STATE_TIMING <= TRIGGERING;
                  end if;
                  trigger        <='0';
                  
               ------------------------------------------------------------------ 
               when TRIGGERING =>   --2.Takt
               ------------------------------------------------------------------
                  IF s_counter < to_unsigned(n_takte,16)-2 THEN
                     s_counter   <= s_counter + 1;
                  ELSE
                     trigger     <='1';
                     s_counter   <= (others=>'0');  
                  END IF; 
                  s_STATE_TIMING <= WAITFORENABLE;    
            END CASE; 
         END IF;
   END IF;

END PROCESS timing;
END ARCHITECTURE behavioral;
