--  Filename: SPI_sender.vhd (VHDL File)
--  Authors : A.Wiest

--  25.10.2021, Created
--  sendet daten ueber SPI
---============================================================================================================================================================

LIBRARY ieee;                                        -- Library Declaration
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;
USE     ieee.math_real.ALL;


ENTITY SPI_sender IS    
                                                     -- Entity Declaration
    GENERIC (        
        takt             : integer  := 100_000_000;  -- 100 MHz
        SPI_takt         : integer  := 3;            -- Anzahl der clocks fuer ein Zustand (high or low) von dem SPI clk
        SPI_bit_count    : integer  := 16;           -- Anzahl der bits, die uebertragen werden         
        SPI_sendepause   : integer  := 10);          -- Anzahl der clocks bei der Sendepause       
 
 
 
    PORT (
        clk, reset       : IN  std_logic ;                                                    -- Control Signals    
        DATA_TO_Transfer : IN  std_logic_vector (SPI_bit_count-1 downto 0):= (others=>'0');   -- Daten zum senden
        SPI_CLK_OUT      : OUT std_logic ;
        SPI_DO           : OUT std_logic);                                                    -- Datenausgang
        
END SPI_sender;
--=================================================================================================================================================================

ARCHITECTURE behavioral OF SPI_sender IS                                               -- Architecture Declaration 
 
   SIGNAL s_DATA_TO_Transfer       : std_logic_vector (SPI_bit_count - 1 downto 0)   := (others=>'0');
   SIGNAL s_SPI_output_counter     : integer     range SPI_bit_count + 8 downto 0    := 0; -- 1 Startbit + 64 Bit Daten + 1 Parity Bit + 1Stop Bit  +5bit pause

   SIGNAL s_Parity_out_spi         : std_logic  := '0';   
   
   SIGNAL s_clk_counter            : integer     range SPI_sendepause downto 0    := 0;
   
   SIGNAL s_SPI_clk                : std_logic  := '0';  
   SIGNAL s_SPI_clk_bit_count      : integer     range SPI_bit_count + 8 downto 0    := 0;
   
   TYPE STATE_SPI_DO IS(EINGANG_SPEICHERN, DATENRAUSGEBEN, SENDEPAUSE); --Zustaende fuer Data Out SPI
   SIGNAL s_SPI_DO_STATE :  STATE_SPI_DO := EINGANG_SPEICHERN;
  
BEGIN


--============================================================================
SPI_out :   process (clk,reset)             -- Daten ueber SPI Rausgeben

--SPI OUTPUT bits declaration
--s_Data_out_reg_spi (0)       = '1' start Bit
--s_Data_out_reg_spi (1 to 64) = '1' Data bits
--s_Data_out_reg_spi (65)      = '1' parity Bit
--s_Data_out_reg_spi (66)      = '1' stop Bit

BEGIN
   if (reset = '1') then                           -- Clock Synchronization
      s_SPI_output_counter <=  0;                  -- Counter of outputs Bits
      s_clk_counter        <=  0;
      
      s_Parity_out_spi     <= '0';
      SPI_DO               <= '0';
      SPI_CLK_OUT          <= '0';
      s_DATA_TO_Transfer   <= (others =>'0');
      s_SPI_DO_STATE       <= EINGANG_SPEICHERN;
     
   elsif (rising_edge (clk) AND clk = '1') then            -- Writting the output data of SPI
      CASE  s_SPI_DO_STATE  IS
      
      ----------------------------------------------------------------------------------
      when EINGANG_SPEICHERN => --1. Takt
      ----------------------------------------------------------------------------------
         s_DATA_TO_Transfer      <= DATA_TO_Transfer; -- Daten zum Uebertragen speichern
         SPI_CLK_OUT             <= '1';
         s_SPI_DO_STATE          <= DATENRAUSGEBEN;

      ----------------------------------------------------------------------------------
      when DATENRAUSGEBEN    => --2. Takt
      ----------------------------------------------------------------------------------
         if (s_clk_counter < SPI_takt - 1 ) then
            s_clk_counter  <= s_clk_counter + 1;
         
         elsif (s_SPI_output_counter = 0) then
            SPI_DO                <= '1';
            s_SPI_output_counter  <= s_SPI_output_counter + 1;
            s_clk_counter         <= 0;                                                                                          -- write the Startbit 
         
         elsif (s_SPI_output_counter < SPI_bit_count +1) then                                                                    -- write the Output data    
            if    (s_DATA_TO_Transfer(s_SPI_output_counter -1) = '1' AND s_Parity_out_spi ='0') then  s_Parity_out_spi <= '1';   -- ungerade Anzahl der Bits   
            elsif (s_DATA_TO_Transfer(s_SPI_output_counter -1) = '0' AND s_Parity_out_spi ='0') then  s_Parity_out_spi <= '0';   -- gerade Anzahl der Bits   
            elsif (s_DATA_TO_Transfer(s_SPI_output_counter -1) = '0' AND s_Parity_out_spi ='1') then  s_Parity_out_spi <= '1'; 
            elsif (s_DATA_TO_Transfer(s_SPI_output_counter -1) = '1' AND s_Parity_out_spi ='1') then  s_Parity_out_spi <= '0';   
            end if;
            
            SPI_DO                <= s_DATA_TO_Transfer(s_SPI_output_counter - 1);    
            s_SPI_output_counter  <= s_SPI_output_counter + 1;          
            s_clk_counter         <= 0;          
                 
         elsif(s_SPI_output_counter = SPI_bit_count + 1) then 
            SPI_DO                <= s_Parity_out_spi;             -- message of Parity error     
            s_SPI_output_counter  <= s_SPI_output_counter + 1;    
            s_clk_counter         <= 0;
             
         elsif(s_SPI_output_counter = SPI_bit_count + 2) then        
            SPI_DO                <= '1';                          --Stop Bit      
            s_SPI_output_counter  <= s_SPI_output_counter + 1;    
            s_clk_counter         <= 0;
         
         else
            SPI_DO                <= '0';   
            s_SPI_output_counter  <=  0;
            s_Parity_out_spi      <= '0';
            SPI_CLK_OUT           <= '0';
            s_SPI_DO_STATE        <= SENDEPAUSE;
         end if;
   
      ----------------------------------------------------------------------------------
      when SENDEPAUSE    => 
      ----------------- -----------------------------------------------------------------
         if (s_clk_counter < SPI_sendepause) then
           s_clk_counter    <= s_clk_counter + 1; 
         else 
            s_SPI_DO_STATE  <= EINGANG_SPEICHERN;
            s_clk_counter   <= 0;
         end if;

      -----------------------------------------------------------------
      when others =>  
      -----------------------------------------------------------------
         s_SPI_DO_STATE  <= EINGANG_SPEICHERN;
      end case;
   
   end if; 
   
end process SPI_out; 

END behavioral;  
