-- Beschreibung:
-- Diese VHDL Datei gibt die Winkel für Harmonics  3 und 6 aus

--========================================================================
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY Harmonics IS
   GENERIC (
      takt                : INTEGER := 100_000_000);

   PORT (
      clk, reset, Enable  : IN  STD_LOGIC;
      Harmonics3_param    : IN  STD_LOGIC_VECTOR (1 downto 0) ;  --"00" nicht verschoben; "01" 90° voreilend; "10" 90° nacheilend
      Harmonics6_param    : IN  STD_LOGIC_VECTOR (1 downto 0) ;  --"00" nicht verschoben; "01" 90° voreilend; "10" 90° nacheilend
            
      Theta               : IN  unsigned(17 downto 0)  := (others => '0');  -- Winkel
      
      Harmonics3          : OUT unsigned(17 downto 0)  := (others => '0');   
      Harmonics6          : OUT unsigned(17 downto 0)  := (others => '0');  
      Data_Out_Valid      : OUT STD_LOGIC              := '0'   );  
   END Harmonics;

   
   ARCHITECTURE behavioral OF Harmonics IS
      CONSTANT c_Harmonics3_g1  : UNSIGNED(17 DOWNTO 0)  := to_unsigned(87381  , 18); --2^18 / 3 =
      CONSTANT c_Harmonics3_g2  : UNSIGNED(17 DOWNTO 0)  := to_unsigned(174762 , 18);
      CONSTANT c_Harmonics3_mu  : UNSIGNED(17 DOWNTO 0)  := to_unsigned(196608 , 18); --multiplikation mit 3 ->3/4 =

      CONSTANT c_Harmonics6_g1  : UNSIGNED(17 DOWNTO 0)  := to_unsigned(43690  , 18); --2^18 /6
      CONSTANT c_Harmonics6_g2  : UNSIGNED(17 DOWNTO 0)  := to_unsigned(87381  , 18);
      CONSTANT c_Harmonics6_g3  : UNSIGNED(17 DOWNTO 0)  := to_unsigned(131072 , 18);
      CONSTANT c_Harmonics6_g4  : UNSIGNED(17 DOWNTO 0)  := to_unsigned(174762 , 18);
      CONSTANT c_Harmonics6_g5  : UNSIGNED(17 DOWNTO 0)  := to_unsigned(218453 , 18);
      CONSTANT c_Harmonics6_mu  : UNSIGNED(17 DOWNTO 0)  := to_unsigned(196608 , 18); --  multiplikation mit 6/8

      CONSTANT c_Harmonics_90   : UNSIGNED(17 DOWNTO 0)  := to_unsigned(65536  , 18);
      
      
      SIGNAL s_Mult_Input1      : UNSIGNED(17 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Mult_Input2      : UNSIGNED(17 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Mult36           : UNSIGNED(35 DOWNTO 0)  := (OTHERS => '0');
     
      SIGNAL s_Harmonics3       : UNSIGNED(17 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Harmonics6       : UNSIGNED(17 DOWNTO 0)  := (OTHERS => '0');
      
      SIGNAL s_Harmonics3b      : UNSIGNED(17 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Harmonics6b      : UNSIGNED(17 DOWNTO 0)  := (OTHERS => '0');
       
      SIGNAL s_Theta            : UNSIGNED(17 DOWNTO 0)  := (OTHERS => '0');
   
      TYPE T_STATE_BERECHNUNG IS(EINGANG_SPEICHERN, --Zustaende---------
                                 WINKEL_BRECHNEN,
                                 WINKEL_MULT, 
                                 MULTIPLIKATION1, MULTIPLIKATION2, MULTIPLIKATION3,
                                 WINKEL_SCHIEBEN);
      SIGNAL s_STATE : T_STATE_BERECHNUNG := EINGANG_SPEICHERN;
 
   
   BEGIN
      berechnung : PROCESS (clk, reset, Enable)
         
      BEGIN
         IF (reset = '1') THEN
            Harmonics3    <= (others => '0');  
            Harmonics6    <= (others => '0');  
 
            s_Harmonics3  <= (others => '0');  
            s_Harmonics6  <= (others => '0');  
            s_STATE       <= EINGANG_SPEICHERN;
 
         ELSE
            IF rising_edge(clk) THEN   -- ggf. Outputs zuweisen
               CASE s_STATE IS
                  
                  ------------------------------------------------------------------
                  WHEN EINGANG_SPEICHERN => --1.Takt
                  ------------------------------------------------------------------ 
                     Data_Out_Valid  <= '0';
                     
                     if (Enable = '1') THEN 
                        s_Theta <= Theta;
                        s_STATE <= WINKEL_BRECHNEN; 
                     end if;
                         
                  ------------------------------------------------------------------
                  WHEN WINKEL_BRECHNEN => --2.Takt
                  ------------------------------------------------------------------ 
                     if    s_Theta < c_Harmonics3_g1   then  s_Harmonics3 <= s_Theta                  ;
                     elsif s_Theta < c_Harmonics3_g2   then  s_Harmonics3 <= s_Theta - c_Harmonics3_g1;
                     else                                    s_Harmonics3 <= s_Theta - c_Harmonics3_g2;   end if;
                     
                     if    s_Theta < c_Harmonics6_g1   then  s_Harmonics6 <= s_Theta                  ;
                     elsif s_Theta < c_Harmonics6_g2   then  s_Harmonics6 <= s_Theta - c_Harmonics6_g1;
                     elsif s_Theta < c_Harmonics6_g3   then  s_Harmonics6 <= s_Theta - c_Harmonics6_g2;
                     elsif s_Theta < c_Harmonics6_g4   then  s_Harmonics6 <= s_Theta - c_Harmonics6_g3; 
                     elsif s_Theta < c_Harmonics6_g5   then  s_Harmonics6 <= s_Theta - c_Harmonics6_g4;               
                     else                                    s_Harmonics6 <= s_Theta - c_Harmonics6_g5;   end if;
  
                     s_STATE <= WINKEL_MULT;

                  ------------------------------------------------------------------
                  WHEN WINKEL_MULT => --3.Takt
                  ------------------------------------------------------------------                         
                     s_Mult_Input1  <= s_Harmonics3;
                     s_MUlt_Input2  <= c_Harmonics3_mu;
                     s_STATE        <= MULTIPLIKATION1; 
                     
                  ------------------------------------------------------------------
                  WHEN MULTIPLIKATION1 => --4.Takt
                  ------------------------------------------------------------------                         
                     s_Mult36       <= s_Mult_Input1 * s_Mult_Input2;
                     s_Mult_Input1  <= s_Harmonics6;
                     s_MUlt_Input2  <= c_Harmonics6_mu;
                     s_STATE        <= MULTIPLIKATION2;  
                 
                  ------------------------------------------------------------------
                  WHEN MULTIPLIKATION2 => --5.Takt
                  ------------------------------------------------------------------                         
                     s_Harmonics3b  <= s_Mult36(35-2 downto 18-2);
                     s_Mult36       <= s_Mult_Input1 * s_Mult_Input2;
                     s_STATE        <= MULTIPLIKATION3;     
                 
                  ------------------------------------------------------------------
                  WHEN MULTIPLIKATION3 => --6.Takt
                  ------------------------------------------------------------------                         
                     s_Harmonics6b  <= s_Mult36(35-3 downto 18-3);
                     s_STATE        <= WINKEL_SCHIEBEN;  
                            
                  ------------------------------------------------------------------
                  WHEN WINKEL_SCHIEBEN => --9.Takt
                  ------------------------------------------------------------------  
                     if    Harmonics3_param  = "01" then  Harmonics3 <= s_Harmonics3b + c_Harmonics_90;
                     elsif Harmonics3_param  = "10" then  Harmonics3 <= s_Harmonics3b - c_Harmonics_90;
                     else                                 Harmonics3 <= s_Harmonics3b;                  end if;
                   
                     if    Harmonics6_param  = "01" then  Harmonics6 <= s_Harmonics6b + c_Harmonics_90;
                     elsif Harmonics6_param  = "10" then  Harmonics6 <= s_Harmonics6b - c_Harmonics_90;
                     else                                 Harmonics6 <= s_Harmonics6b;                   end if;
                     Data_Out_Valid <= '1';
                     s_STATE        <= EINGANG_SPEICHERN ;
                  
                  ---------------------------------------------------------------------------------
                  WHEN OTHERS => 
                  ----------------------------------------------------------------------------------
                     s_STATE <= EINGANG_SPEICHERN; 
               END CASE;
            END IF;
         END IF;
      END PROCESS berechnung;

END ARCHITECTURE behavioral;

