--  Filename: abc_to_ab8_faktor.vhd
--    (22Takte *7 + 24 *8 )*10ns ca. 22*8*10ns  laut simulation 1,71us
--  Beschrebung:
--      Transformation abc -> alpha beta (siehe Glechung B6, Seite 96, Dipl Arbeit TU A.Wiest)
--      alpha = a*2/3 - b*1/3 - c* 1/3
--      beta  = b*1/sqrt(3) - c* 1/sqrt(3)
--
--  Eing. Groessen:
--      a              a Komponente 16bit signed
--      b              b Komponente 16bit signed
--      c              c Komponente 16bit signed
--
--      clk            clock
--      Enable         symbolisiert Gültigkeit der Eingangsdaten
--      reset          Berechnung neue starten,
--                     alle Varablen auf Null setzen
--
--  Ausg. Groessen:
--      alpha          Komponente der ab System, 16bit signed
--      beta           Komponente der ab System, 16bit signed
--
--      Data_Out_Valid_abc  '1' wenn die Berechnung fertig ist
-- Insgesamt werden 8 Systeme Transformiert
-- Faktor ist der Multiplikationsfaktor mit dem Ausgang 32767 =1
-- Sys1_ein -> System wird Ein- oder Ausgeschaltet
-- Ein oder Ausschalten mit oder ohne Rampe
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity abc_to_ab8_factor is
   generic(
      takt             : integer   := 100_000_000;                -- Initalisierung 100 MHz
      Zeitverz1ein     : std_logic := '0';                        -- =1, so waechst der Faktor liniar bis er sein Wert "Faktor erreicht"
      Zeitverz2ein     : std_logic := '0';
      Zeitverz3ein     : std_logic := '0';
      Zeitverz4ein     : std_logic := '0';
      Zeitverz5ein     : std_logic := '1';                        -- =1, so waechst der Faktor liniar bis er sein Wert "Faktor erreicht"
      Zeitverz6ein     : std_logic := '0';
      Zeitverz7ein     : std_logic := '0';
      Zeitverz8ein     : std_logic := '0';
      Dalay_time       : integer   := 200;                        -- Anzahl der Durchlaufe, damit der Faktor um eins erhoeht oder runter gezaehlt wird
      Additionsfaktor  : integer   := 1 
      );                                                          -- Ein Durchlauf entspricht einer Berechnung

   port(
      clk, reset, Enable   : in std_logic;                        --System-Eingaenge:

      abc1_fak             : in std_logic_vector (63 downto 0);   -- Eingang a, b,c, Ausgang wird skaliert, fuer die Skalierung = 1 ->32767
      abc1_Ein_Vorz        : in std_logic_vector (1  downto 0);   -- Ausgang einschalten =1, Vorzeichen = 0 wird nicht negiert
      abc2_fak             : in std_logic_vector (63 downto 0);   -- Eingang a, b,c, Ausgang wird skaliert, fuer die Skalierung = 1 ->32767
      abc2_Ein_Vorz        : in std_logic_vector (1  downto 0);
      abc3_fak             : in std_logic_vector (63 downto 0);   -- Eingang a, b,c, Ausgang wird skaliert, fuer die Skalierung = 1 ->32767
      abc3_Ein_Vorz        : in std_logic_vector (1  downto 0);
      abc4_fak             : in std_logic_vector (63 downto 0);   -- Eingang a, b,c, Ausgang wird skaliert, fuer die Skalierung = 1 ->32767
      abc4_Ein_Vorz        : in std_logic_vector (1  downto 0);
      abc5_fak             : in std_logic_vector (63 downto 0);   -- Eingang a, b,c, Ausgang wird skaliert, fuer die Skalierung = 1 ->32767
      abc5_Ein_Vorz        : in std_logic_vector (1  downto 0);
      abc6_fak             : in std_logic_vector (63 downto 0);   -- Eingang a, b,c, Ausgang wird skaliert, fuer die Skalierung = 1 ->32767
      abc6_Ein_Vorz        : in std_logic_vector (1  downto 0);
      abc7_fak             : in std_logic_vector (63 downto 0);   -- Eingang a, b,c, Ausgang wird skaliert, fuer die Skalierung = 1 ->32767
      abc7_Ein_Vorz        : in std_logic_vector (1  downto 0);
      abc8_fak             : in std_logic_vector (63 downto 0);   -- Eingang a, b,c, Ausgang wird skaliert, fuer die Skalierung = 1 ->32767
      abc8_Ein_Vorz        : in std_logic_vector (1  downto 0);

      --Ausgaenge
      alpha1, beta1        : out signed(15 downto 0);
      alpha2, beta2        : out signed(15 downto 0);
      alpha3, beta3        : out signed(15 downto 0);
      alpha4, beta4        : out signed(15 downto 0);
      alpha5, beta5        : out signed(15 downto 0);
      alpha6, beta6        : out signed(15 downto 0);
      alpha7, beta7        : out signed(15 downto 0);
      alpha8, beta8        : out signed(15 downto 0);

      Data_Out_Valid_abc   : out std_logic );

   end abc_to_ab8_factor;

--============================================================================
architecture behavioral of abc_to_ab8_factor is
   attribute multstyle               : string;
   attribute multstyle of behavioral : architecture is "dsp";    -- Nutzt bevorzugt embedded multiplier

   type     tArray_std  is array(7 downto 0) of std_logic;
   signal   s_Sys_ein      : tArray_std ;
   signal   s_TimeDelay_on : tArray_std ;
   signal   s_Vorzeichen   : tArray_std ;

   type     tArray_signed  is array(7 downto 0) of signed(15 downto 0);
   signal   s_Faktor       : tArray_signed ;
   signal   s_Faktor_in    : tArray_signed ;
   signal   s_a_in         : tArray_signed ;
   signal   s_b_in         : tArray_signed ;
   signal   s_c_in         : tArray_signed ;
   signal   s_count_Fak    : tArray_signed ;


--Matrix Transformationskonstanten:
   constant C1           : signed   (15 downto 0) := to_signed(21845, 16);  -- 2/3 = 0,66 = > (2^15-1) *0,66 = 21845
   constant C2           : signed   (15 downto 0) := to_signed(10922, 16);  -- 1/3 = 0,33 = (2^15-ENTITY abc_to_ab IS1)* 0,33 = 10922
   constant C3           : signed   (15 downto 0) := to_signed(18918, 16);  -- 1/ sqrt(3) = 0,57 = (2^15-1)*0,57  = 18918

   signal s_constMult    : signed   (15 downto 0) := (others => '0');
   signal s_b            : signed   (15 downto 0) := (others => '0');
   signal s_c            : signed   (15 downto 0) := (others => '0');
   signal s_counter_ber  : unsigned (2 downto 0)  := (others => '0'); -- ='0' wird das erste System berechnet =1 das zweite..

  ----------------------------------------------------------
  signal s_a1_in    : signed (15 downto 0) := (others => '0');  signal s_b1_in    : signed (15 downto 0) := (others => '0'); signal s_c1_in    : signed (15 downto 0) := (others => '0');
  signal s_a2_in    : signed (15 downto 0) := (others => '0');  signal s_b2_in    : signed (15 downto 0) := (others => '0'); signal s_c2_in    : signed (15 downto 0) := (others => '0');
  signal s_a3_in    : signed (15 downto 0) := (others => '0');  signal s_b3_in    : signed (15 downto 0) := (others => '0'); signal s_c3_in    : signed (15 downto 0) := (others => '0');
  signal s_a4_in    : signed (15 downto 0) := (others => '0');  signal s_b4_in    : signed (15 downto 0) := (others => '0'); signal s_c4_in    : signed (15 downto 0) := (others => '0');

  signal s_a5_in    : signed (15 downto 0) := (others => '0');  signal s_b5_in    : signed (15 downto 0) := (others => '0'); signal s_c5_in    : signed (15 downto 0) := (others => '0');
  signal s_a6_in    : signed (15 downto 0) := (others => '0');  signal s_b6_in    : signed (15 downto 0) := (others => '0'); signal s_c6_in    : signed (15 downto 0) := (others => '0');
  signal s_a7_in    : signed (15 downto 0) := (others => '0');  signal s_b7_in    : signed (15 downto 0) := (others => '0'); signal s_c7_in    : signed (15 downto 0) := (others => '0');
  signal s_a8_in    : signed (15 downto 0) := (others => '0');  signal s_b8_in    : signed (15 downto 0) := (others => '0'); signal s_c8_in    : signed (15 downto 0) := (others => '0');

  ----------------------------------------------------------
  signal s_alpha           : signed (15 downto 0) := (others => '0');  signal s_beta            : signed (15 downto 0) := (others => '0');
  signal s_alpha_ohne_fak  : signed (15 downto 0) := (others => '0');  signal s_beta_ohne_fak   : signed (15 downto 0) := (others => '0');
  signal s_alpha_fak       : signed (15 downto 0) := (others => '0');  signal s_beta_fak        : signed (15 downto 0) := (others => '0');


  signal s_alpha1 : signed (15 downto 0) := (others => '0');  signal s_beta1    : signed (15 downto 0) := (others => '0');
  signal s_alpha2 : signed (15 downto 0) := (others => '0');  signal s_beta2    : signed (15 downto 0) := (others => '0');
  signal s_alpha3 : signed (15 downto 0) := (others => '0');  signal s_beta3    : signed (15 downto 0) := (others => '0');
  signal s_alpha4 : signed (15 downto 0) := (others => '0');  signal s_beta4    : signed (15 downto 0) := (others => '0');

  signal s_alpha5 : signed (15 downto 0) := (others => '0');  signal s_beta5    : signed (15 downto 0) := (others => '0');
  signal s_alpha6 : signed (15 downto 0) := (others => '0');  signal s_beta6    : signed (15 downto 0) := (others => '0');
  signal s_alpha7 : signed (15 downto 0) := (others => '0');  signal s_beta7    : signed (15 downto 0) := (others => '0');
  signal s_alpha8 : signed (15 downto 0) := (others => '0');  signal s_beta8    : signed (15 downto 0) := (others => '0');

  ----------------------------------------------------------

  signal s_abcMult: signed (15 downto 0) := (others => '0');
  signal s_Mult32 : signed (31 downto 0) := (others => '0');
  signal sa_Mult1 : signed (15 downto 0) := (others => '0');
  signal sa_Mult2 : signed (15 downto 0) := (others => '0');
  signal sa_Mult3 : signed (15 downto 0) := (others => '0');
  signal sa_Mult4 : signed (15 downto 0) := (others => '0');
  signal sa_Mult5 : signed (15 downto 0) := (others => '0');

  signal s_Summe1 : signed (17 downto 0) := (others => '0');  --16 + 16 + 16bit =18bit
  signal s_Summe2 : signed (16 downto 0) := (others => '0');  --16 + 16bit =17 bit

  type STATE_ABC_TO_AB is(
                           EINGANG_SPEICHERN,
                           EINGANG_AUSWAEHLEN,
                           MULTIPLIKATION1,  MULTIPLIKATION1_SPECHERN,
                           MULTIPLIKATION2,  MULTIPLIKATION2_SPECHERN,
                           MULTIPLIKATION3,  MULTIPLIKATION3_SPECHERN,
                           MULTIPLIKATION4,  MULTIPLIKATION4_SPECHERN,
                           MULTIPLIKATION5,  MULTIPLIKATION5_SPECHERN,
                           ADDITION, DATENLIMIT, FAKTOR_BERECHNEN,FACTOR_MULT1_VOR,
                           FACTOR_MULT1_BER,FACTOR_MULT1_SPE,
                           FACTOR_MULT2_BER,FACTOR_MULT2_SPE,
                           ENDERGEBNISS_BERECHNEN, BERECHNUNG_UMSCHALTEN,
                           DATEN_AUSGEBEN, DATEN_BEREIT);
  signal s_STATE : STATE_ABC_TO_AB := EINGANG_SPEICHERN;


begin

  hauptprozess : process (clk, reset, Enable)
  begin
   if (reset = '1') then
      s_alpha            <= (others => '0');   s_beta           <= (others => '0');
      s_alpha_ohne_fak   <= (others => '0');   s_beta_ohne_fak  <= (others => '0');
      s_alpha_fak        <= (others => '0');   s_beta_fak       <= (others => '0');

      s_alpha1      <= (others => '0');   s_beta1   <= (others => '0');
      s_alpha2      <= (others => '0');   s_beta2   <= (others => '0');
      s_alpha3      <= (others => '0');   s_beta3   <= (others => '0');
      s_alpha4      <= (others => '0');   s_beta4   <= (others => '0');
      s_alpha5      <= (others => '0');   s_beta5   <= (others => '0');
      s_alpha6      <= (others => '0');   s_beta6   <= (others => '0');
      s_alpha7      <= (others => '0');   s_beta7   <= (others => '0');
      s_alpha8      <= (others => '0');   s_beta8   <= (others => '0');

      s_abcMult     <= (others => '0');
      s_b           <= (others => '0');
      s_c           <= (others => '0');
      s_constMult   <= (others => '0');

      s_Mult32      <= (others => '0');
      sa_Mult1      <= (others => '0');
      sa_Mult2      <= (others => '0');
      sa_Mult3      <= (others => '0');
      sa_Mult4      <= (others => '0');
      sa_Mult5      <= (others => '0');

      s_Summe1      <= (others => '0');
      s_Summe2      <= (others => '0');

      alpha1        <= (others => '0'); beta1 <= (others => '0');
      alpha2        <= (others => '0'); beta2 <= (others => '0');
      alpha3        <= (others => '0'); beta3 <= (others => '0');
      alpha4        <= (others => '0'); beta4 <= (others => '0');
      alpha5        <= (others => '0'); beta5 <= (others => '0');
      alpha6        <= (others => '0'); beta6 <= (others => '0');
      alpha7        <= (others => '0'); beta7 <= (others => '0');
      alpha8        <= (others => '0'); beta8 <= (others => '0');

      for i in 0 to 7  loop                -- Schieberegister-Daten schieben
         s_Sys_ein(i)      <= '0';
         s_TimeDelay_on(i) <= '0';
         s_Vorzeichen (i)  <= '0';
         s_a_in(i)         <= (others => '0');
         s_b_in(i)         <= (others => '0');
         s_c_in(i)         <= (others => '0');
         s_count_Fak(i)    <= (others => '0');
         s_Faktor(i)       <= (others => '0');
         s_Faktor_in(i)    <= (others => '0');
      end loop;

      s_counter_ber        <= "000";       -- Start der Berechnung mit dem ersten System
      s_STATE              <= EINGANG_SPEICHERN;
      Data_Out_Valid_abc   <= '0';

    elsif rising_edge(clk) then
      case s_STATE is

   ----------------------------------------------------------------------------------
   when EINGANG_SPEICHERN =>       --1.Takt
   ----------------------------------------------------------------------------------
      if (Enable = '1') then
         s_a_in(0) <= signed(abc1_fak(63 downto 48));  s_b_in(0) <= signed(abc1_fak(47 downto 32));  s_c_in(0) <= signed(abc1_fak(31 downto 16)); s_Faktor_in(0) <= signed(abc1_fak(15 downto 0));
         s_a_in(1) <= signed(abc2_fak(63 downto 48));  s_b_in(1) <= signed(abc2_fak(47 downto 32));  s_c_in(1) <= signed(abc2_fak(31 downto 16)); s_Faktor_in(1) <= signed(abc2_fak(15 downto 0));
         s_a_in(2) <= signed(abc3_fak(63 downto 48));  s_b_in(2) <= signed(abc3_fak(47 downto 32));  s_c_in(2) <= signed(abc3_fak(31 downto 16)); s_Faktor_in(2) <= signed(abc3_fak(15 downto 0));
         s_a_in(3) <= signed(abc4_fak(63 downto 48));  s_b_in(3) <= signed(abc4_fak(47 downto 32));  s_c_in(3) <= signed(abc4_fak(31 downto 16)); s_Faktor_in(3) <= signed(abc4_fak(15 downto 0));
         s_a_in(4) <= signed(abc5_fak(63 downto 48));  s_b_in(4) <= signed(abc5_fak(47 downto 32));  s_c_in(4) <= signed(abc5_fak(31 downto 16)); s_Faktor_in(4) <= signed(abc5_fak(15 downto 0));
         s_a_in(5) <= signed(abc6_fak(63 downto 48));  s_b_in(5) <= signed(abc6_fak(47 downto 32));  s_c_in(5) <= signed(abc6_fak(31 downto 16)); s_Faktor_in(5) <= signed(abc6_fak(15 downto 0));
         s_a_in(6) <= signed(abc7_fak(63 downto 48));  s_b_in(6) <= signed(abc7_fak(47 downto 32));  s_c_in(6) <= signed(abc7_fak(31 downto 16)); s_Faktor_in(6) <= signed(abc7_fak(15 downto 0));
         s_a_in(7) <= signed(abc8_fak(63 downto 48));  s_b_in(7) <= signed(abc8_fak(47 downto 32));  s_c_in(7) <= signed(abc8_fak(31 downto 16)); s_Faktor_in(7) <= signed(abc8_fak(15 downto 0));

         s_Sys_ein(0) <= abc1_Ein_Vorz(1); s_Vorzeichen(0)<= abc1_Ein_Vorz(0);
         s_Sys_ein(1) <= abc2_Ein_Vorz(1); s_Vorzeichen(1)<= abc2_Ein_Vorz(0);
         s_Sys_ein(2) <= abc3_Ein_Vorz(1); s_Vorzeichen(2)<= abc3_Ein_Vorz(0);
         s_Sys_ein(3) <= abc4_Ein_Vorz(1); s_Vorzeichen(3)<= abc4_Ein_Vorz(0);
         s_Sys_ein(4) <= abc5_Ein_Vorz(1); s_Vorzeichen(4)<= abc5_Ein_Vorz(0);
         s_Sys_ein(5) <= abc6_Ein_Vorz(1); s_Vorzeichen(5)<= abc6_Ein_Vorz(0);
         s_Sys_ein(6) <= abc7_Ein_Vorz(1); s_Vorzeichen(6)<= abc7_Ein_Vorz(0);
         s_Sys_ein(7) <= abc8_Ein_Vorz(1); s_Vorzeichen(7)<= abc8_Ein_Vorz(0);

         s_TimeDelay_on(0) <= Zeitverz1ein;
         s_TimeDelay_on(1) <= Zeitverz2ein;
         s_TimeDelay_on(2) <= Zeitverz3ein;
         s_TimeDelay_on(3) <= Zeitverz4ein;
         s_TimeDelay_on(4) <= Zeitverz5ein;
         s_TimeDelay_on(5) <= Zeitverz6ein;
         s_TimeDelay_on(6) <= Zeitverz7ein;
         s_TimeDelay_on(7) <= Zeitverz8ein;

         s_STATE          <= EINGANG_AUSWAEHLEN;
         end if;

         Data_Out_Valid_abc <= '0';

   ----------------------------------------------------------------------------------
   when EINGANG_AUSWAEHLEN =>       --2.Takt
   ----------------------------------------------------------------------------------
      if    (s_counter_ber   = "000") then   s_abcMult <= s_a_in(0);   s_b <= s_b_in(0);   s_c <= s_c_in(0);
      elsif (s_counter_ber   = "001") then   s_abcMult <= s_a_in(1);   s_b <= s_b_in(1);   s_c <= s_c_in(1);
      elsif (s_counter_ber   = "010") then   s_abcMult <= s_a_in(2);   s_b <= s_b_in(2);   s_c <= s_c_in(2);
      elsif (s_counter_ber   = "011") then   s_abcMult <= s_a_in(3);   s_b <= s_b_in(3);   s_c <= s_c_in(3);
      elsif (s_counter_ber   = "100") then   s_abcMult <= s_a_in(4);   s_b <= s_b_in(4);   s_c <= s_c_in(4);
      elsif (s_counter_ber   = "101") then   s_abcMult <= s_a_in(5);   s_b <= s_b_in(5);   s_c <= s_c_in(5);
      elsif (s_counter_ber   = "110") then   s_abcMult <= s_a_in(6);   s_b <= s_b_in(6);   s_c <= s_c_in(6);
      else                                   s_abcMult <= s_a_in(7);   s_b <= s_b_in(7);   s_c <= s_c_in(7);
      end if;

      s_constMult   <= C1;
      s_STATE       <= MULTIPLIKATION1;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION1 =>   --3.Takt
   ----------------------------------------------------------------------------------
      s_Mult32      <= s_constMult * s_abcMult;  -- 2/3 * a
      s_STATE       <= MULTIPLIKATION1_SPECHERN;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION1_SPECHERN => --4.Takt
   ----------------------------------------------------------------------------------
      sa_Mult1      <= s_Mult32 (30 downto 15);
      s_constMult   <= C2;
      s_abcMult     <= s_b;
      s_STATE       <= MULTIPLIKATION2;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION2 => --5.Takt
   ----------------------------------------------------------------------------------
      s_Mult32      <= s_constMult * s_abcMult;  -- 1/3 * b
      s_STATE       <= MULTIPLIKATION2_SPECHERN;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION2_SPECHERN =>   --6.Takt
   ----------------------------------------------------------------------------------
      sa_Mult2      <= s_Mult32 (30 downto 15);
      s_abcMult     <= s_c;
      s_STATE       <= MULTIPLIKATION3;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION3 =>   --7.Takt
   ----------------------------------------------------------------------------------
      s_Mult32      <= s_constMult * s_abcMult;  --1/3 * c
      s_STATE       <= MULTIPLIKATION3_SPECHERN;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION3_SPECHERN => --8.Takt
   ----------------------------------------------------------------------------------
      sa_Mult3      <= s_Mult32 (30 downto 15);
      s_constMult   <= c3;
      s_abcMult     <= s_b;
      s_STATE       <= MULTIPLIKATION4;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION4 => --9.Takt
   ----------------------------------------------------------------------------------
      s_Mult32      <= s_constMult * s_abcMult;  --1/sqrt(3) *b
      s_STATE       <= MULTIPLIKATION4_SPECHERN;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION4_SPECHERN => --10.Takt
   ----------------------------------------------------------------------------------
      sa_Mult4      <= s_Mult32 (30 downto 15);
      s_abcMult     <= s_c;
      s_STATE       <= MULTIPLIKATION5;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION5 => --11.Takt
   ----------------------------------------------------------------------------------
      s_Mult32      <= s_constMult * s_abcMult;  --1/sqrt(3) * c
      s_STATE       <= MULTIPLIKATION5_SPECHERN;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION5_SPECHERN => --12.Takt
   ----------------------------------------------------------------------------------
      sa_Mult5      <= s_Mult32 (30 downto 15);
      s_STATE       <= ADDITION;

   ---------------------------------------------------------------------------------
   when ADDITION =>                -- 13. Takt, beide Reihen aufaddieren
   ---------------------------------------------------------------------------------
      s_Summe1      <= resize(sa_Mult1, 18) - resize(sa_Mult2, 18) - resize(sa_Mult3, 18);
      s_Summe2      <= resize(sa_Mult4, 17) - resize(sa_Mult5, 17);
      s_STATE       <= DATENLIMIT;

   ---------------------------------------------------------------------------------
   when DATENLIMIT =>          --14.Takt
   ---------------------------------------------------------------------------------
      if    s_Summe1 > to_signed(32766, 16)  then  s_alpha_ohne_fak <= to_signed(32767, 16);
      elsif s_Summe1 < to_signed(-32766, 16) then  s_alpha_ohne_fak <= to_signed(-32767, 16);
      else                                         s_alpha_ohne_fak <= s_Summe1(15 downto 0);
      end if;

      if    s_Summe2 > to_signed(32766, 16)  then  s_beta_ohne_fak <= to_signed(32767, 16);
      elsif s_Summe2 < to_signed(-32766, 16) then  s_beta_ohne_fak <= to_signed(-32767, 16);
      else                                         s_beta_ohne_fak <= s_Summe2(15 downto 0);
      end if;

      s_STATE  <= FAKTOR_BERECHNEN;

   ---------------------------------------------------------------------------------
   when FAKTOR_BERECHNEN =>          --15.Takt
   ---------------------------------------------------------------------------------
      for i in 0 to 7  loop                           -- Schieberegister-Daten schieben
         if s_Sys_ein(i) = '1' then                   -- system Einschalten
            if s_TimeDelay_on(i) = '1' then           -- Fals Zeitverzoegerung eingeschaltet ist
               if s_count_Fak(i) < Dalay_time then    -- Anzahl der durchläufe bevor es weiter gezählt wird
                  s_count_Fak(i)   <= s_count_Fak(i) + 1;
               else
                  if s_Faktor(i) < s_Faktor_in(i) then
                     if (s_Faktor(i) + Additionsfaktor) < s_Faktor_in(i) then
                        s_Faktor(i) <= s_Faktor(i) + Additionsfaktor;
                     else
                        s_Faktor(i) <= s_Faktor_in(i);
                     end if;
                     s_count_Fak(i) <= (others => '0');
                  else
                     s_count_Fak(i) <= to_signed(Dalay_time,16);
                  end if;
               end if;
            else -- Ohne Zeitverzoegerung
               s_count_Fak(i) <= (others => '0');
               s_Faktor(i)    <= s_Faktor_in(i);
            end if;
         else --System Ausschalten
            if s_TimeDelay_on(i) = '1' then
               if s_count_Fak(i) < Dalay_time then -- Anzahl der durchläufe befor es weiter gezählt wird
                  s_count_Fak(i) <= s_count_Fak(i) + 1;
               else
                  if s_Faktor(i) > 0 then
                     if (s_Faktor(i) - Additionsfaktor) > 0 then
                        s_Faktor(i) <= s_Faktor(i) - Additionsfaktor;
                     else
                        s_Faktor(i) <= (others => '0');
                     end if;
                     s_count_Fak(i) <= (others => '0');
                  else
                     s_count_Fak(i) <= to_signed(Dalay_time,16);
                  end if;
               end if;
            else -- Ohne Zeitverzoegerung
               s_count_Fak(i) <= (others => '0');
               s_Faktor(i)    <= (others => '0');
            end if;
         end if;
      end loop;

      s_STATE  <= FACTOR_MULT1_VOR;

   ---------------------------------------------------------------------------------
   when FACTOR_MULT1_VOR => --16
   ---------------------------------------------------------------------------------
      if    (s_counter_ber = "000") then  s_constMult <= s_Faktor(0);
      elsif (s_counter_ber = "001") then  s_constMult <= s_Faktor(1);
      elsif (s_counter_ber = "010") then  s_constMult <= s_Faktor(2);
      elsif (s_counter_ber = "011") then  s_constMult <= s_Faktor(3);
      elsif (s_counter_ber = "100") then  s_constMult <= s_Faktor(4);
      elsif (s_counter_ber = "101") then  s_constMult <= s_Faktor(5);
      elsif (s_counter_ber = "110") then  s_constMult <= s_Faktor(6);
      else                                s_constMult <= s_Faktor(7);
      end if;

      s_abcMult   <= s_alpha_ohne_fak;
      s_STATE     <= FACTOR_MULT1_BER;

   ---------------------------------------------------------------------------------
   when FACTOR_MULT1_BER => --17
   ---------------------------------------------------------------------------------
      s_Mult32   <= s_constMult * s_abcMult; --s_alpha
      s_STATE    <= FACTOR_MULT1_SPE;

   ---------------------------------------------------------------------------------
   when FACTOR_MULT1_SPE => --18
   ---------------------------------------------------------------------------------
      s_alpha_fak    <= s_Mult32(30 downto 15);
      s_abcMult      <= s_beta_ohne_fak;
      s_STATE        <= FACTOR_MULT2_BER;

   ---------------------------------------------------------------------------------
     when FACTOR_MULT2_BER => --19
   ---------------------------------------------------------------------------------
      s_Mult32 <= s_constMult * s_abcMult;
      s_STATE  <= FACTOR_MULT2_SPE;

   ---------------------------------------------------------------------------------
   when FACTOR_MULT2_SPE => --20
   ---------------------------------------------------------------------------------
      s_beta_fak   <= s_Mult32(30 downto 15);
      s_STATE      <= ENDERGEBNISS_BERECHNEN;

   -------------------------------------------------------------------------------
   when ENDERGEBNISS_BERECHNEN =>          --21.Takt
   ---------------------------------------------------------------------------------
     if    s_alpha_fak > to_signed( 32766, 16) then    s_alpha <= to_signed(32767, 16);
     elsif s_alpha_fak < to_signed(-32766, 16) then    s_alpha <= to_signed(-32767, 16);
     else                                              s_alpha <= s_alpha_fak(15 downto 0);
     end if;

     if    s_beta_fak > to_signed( 32766, 16)  then    s_beta  <= to_signed(32767, 16);
     elsif s_beta_fak < to_signed(-32766, 16)  then    s_beta  <= to_signed(-32767, 16);
     else                                              s_beta  <= s_beta_fak(15 downto 0);
     end if;

     s_STATE <= BERECHNUNG_UMSCHALTEN;

   ---------------------------------------------------------------------------
   when BERECHNUNG_UMSCHALTEN => --22. Takt
   ---------------------------------------------------------------------------
      if    (s_counter_ber = "000") then s_alpha1 <= s_alpha; s_beta1 <= s_beta; S_STATE <= EINGANG_AUSWAEHLEN;
      elsif (s_counter_ber = "001") then s_alpha2 <= s_alpha; s_beta2 <= s_beta; S_STATE <= EINGANG_AUSWAEHLEN;
      elsif (s_counter_ber = "010") then s_alpha3 <= s_alpha; s_beta3 <= s_beta; S_STATE <= EINGANG_AUSWAEHLEN;
      elsif (s_counter_ber = "011") then s_alpha4 <= s_alpha; s_beta4 <= s_beta; S_STATE <= EINGANG_AUSWAEHLEN;
      elsif (s_counter_ber = "100") then s_alpha5 <= s_alpha; s_beta5 <= s_beta; S_STATE <= EINGANG_AUSWAEHLEN;
      elsif (s_counter_ber = "101") then s_alpha6 <= s_alpha; s_beta6 <= s_beta; S_STATE <= EINGANG_AUSWAEHLEN;
      elsif (s_counter_ber = "110") then s_alpha7 <= s_alpha; s_beta7 <= s_beta; S_STATE <= EINGANG_AUSWAEHLEN;
      else                               s_alpha8 <= s_alpha; s_beta8 <= s_beta; S_STATE <= DATEN_AUSGEBEN;
      end if;

      s_counter_ber    <= s_counter_ber + 1;

   ---------------------------------------------------------------------------
   when DATEN_AUSGEBEN => --23. Takt
   ---------------------------------------------------------------------------
      if s_Vorzeichen(0) = '0' then alpha1 <=     s_alpha1;        beta1 <=     s_beta1;
      else                          alpha1 <= NOT(s_alpha1) + 1 ;  beta1 <= NOT(s_beta1) +1; end if;

      if s_Vorzeichen(1) = '0' then alpha2 <=     s_alpha2;        beta2 <=     s_beta2;
      else                          alpha2 <= NOT(s_alpha2) + 1 ;  beta2 <= NOT(s_beta2) +1; end if;

      if s_Vorzeichen(2) = '0' then alpha3 <=     s_alpha3;        beta3 <=     s_beta3;
      else                          alpha3 <= NOT(s_alpha3) + 1 ;  beta3 <= NOT(s_beta3) +1; end if;

      if s_Vorzeichen(3) = '0' then alpha4 <=     s_alpha4;        beta4 <=     s_beta4;
      else                          alpha4 <= NOT(s_alpha4) + 1 ;  beta4 <= NOT(s_beta4) +1; end if;

      if s_Vorzeichen(4) = '0' then alpha5 <=     s_alpha5;        beta5 <=     s_beta5;
      else                          alpha5 <= NOT(s_alpha5) + 1 ;  beta5 <= NOT(s_beta5) +1; end if;

      if s_Vorzeichen(5) = '0' then alpha6 <=     s_alpha6;        beta6 <=     s_beta6;
      else                          alpha6 <= NOT(s_alpha6) + 1 ;  beta6 <= NOT(s_beta6) +1; end if;

      if s_Vorzeichen(6) = '0' then alpha7 <=     s_alpha7;        beta7 <=     s_beta7;
      else                          alpha7 <= NOT(s_alpha7) + 1 ;  beta7 <= NOT(s_beta7) +1; end if;

      if s_Vorzeichen(7) = '0' then alpha8 <=     s_alpha8;        beta8 <=     s_beta8;
      else                          alpha8 <= NOT(s_alpha8) + 1 ;  beta8 <= NOT(s_beta8) +1; end if;

      S_STATE            <= DATEN_BEREIT;

   ---------------------------------------------------------------------------
   when DATEN_BEREIT => --24. Takt
   ---------------------------------------------------------------------------
      Data_Out_Valid_abc <= '1';
      S_STATE            <= EINGANG_SPEICHERN;

   ---------------------------------------------------------------------
   when others =>
   ---------------------------------------------------------------------
      s_STATE <= EINGANG_SPEICHERN;

      end case;
      end if;  --reset
      end process hauptprozess;

   end architecture behavioral;
