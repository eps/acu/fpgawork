--  Filename: Load_Voltage_Calculation.vhd (VHDL File)
--  Datum   : 01.07.2015  
--  Authors : M.Dudhat
--
--  This VHDL algorithm module gives the value of the IGBT Load voltage  (Ulast) of the IGBT module.

--  Ulast (Load Voltage) = (Ltotal*di/dt)+ (Isoll*Rtotal) =   UL (Inductive Volatge part) + UR (Resistive Volatge part)

--          where Ltotal = Lmagnet + LCable,
--                Rtotal = Rmagnet + RCable,
--                Ulast  --> (as per the dipole, quadropole, multipole power convertors specification)
--                Isoll  --> (Set value of the current inculding the oscillated maximum value)
--                mR * L * di/dt 
--                mL * Rlast * s_Isoll    
---========================================================================================================================================================

LIBRARY ieee;                                                             -- Library Declaration
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

                                                                          -- Entity Declaration
ENTITY Load_Voltage_Calculation IS   
      
   --GENERIC(  ); 
    
   PORT(
      --System Signals
      clk, reset, Enable   : in std_logic   := '0';

      --Inputs
      Isoll                : in signed (15 downto 0);                   -- Set value of the current
      mL,mR                : in signed (15 downto 0);                   -- Berechnungs- Konstanten mR fuer Widerstand, mL fuer die Indukt.
      Llast,Rlast          : in signed (15 downto 0);                   -- Kennlinie der Induktivität und des Widerstandes
      Utransistor          : in signed (15 downto 0);                   -- Spannungsabfahl an den Transistoren des Netzgeraetes
     
      -- Outputs
      ULast                : out signed(15 downto 0):=(others=>'0');    -- Generated Load Voltage (Ulast)
      Isoll_diff           : out signed(15 downto 0):=(others=>'0');
      Data_Out_Valid_Ulast : out std_logic          := '0'      );      -- Output Valid Signal 

END Load_Voltage_Calculation ;

--==============================================================================================================================

ARCHITECTURE behavioral OF Load_Voltage_Calculation IS                  -- Architecture Declaration  

-- Load Voltage (Ulast) Process Signals ----------------------------------------------------------------------------------------
SIGNAL s_Isoll_act         : signed  (15 downto 0):=(others=>'0');      -- Actual set value of the input Current
SIGNAL s_Isoll_alt         : signed  (15 downto 0):=(others=>'0');      -- Old set value of the input Current

SIGNAL s_Isoll_diff        : signed  (15 downto 0):=(others=>'0');      -- Difference value of the set input Current

SIGNAL s_mult_a            : signed  (15 downto 0):=(others=>'0'); 
SIGNAL s_mult_b            : signed  (15 downto 0):=(others=>'0'); 
SIGNAL s_mult32            : signed  (31 downto 0):=(others=>'0'); 

SIGNAL s_U_Inductive       : signed  (15 downto 0):=(others=>'0');      -- Inductive Load Voltage Value
SIGNAL s_U_Resistive       : signed  (15 downto 0):=(others=>'0');      -- Resistive Load Voltage Value

SIGNAL s_Summe             : signed  (15 downto 0):=(others=>'0');      -- Uinduktive + Uresistive


TYPE STATE_LOADVOLTAGE IS( EINGANG_SPEICHERN,                           -- Load Voltage State machine 
                           DELTA_I_SOLL,
                           MULT1_VORBEREITEN, MULTIPLIKATION1, 
                           MULT2_VORBEREITEN, MULTIPLIKATION2, MULT2_SPEICHERN,
                           MULTIPLIKATION3,   
                           MULT4_VORBEREITEN,MULTIPLIKATION4, MULT4_SPEICHERN,
                           SUMME, TRANSISTOR );

SIGNAL s_Ulast_STATE : STATE_LOADVOLTAGE := EINGANG_SPEICHERN;



BEGIN --=========================================================================================================================

Load_Voltage_Calculation : Process (clk, reset, Isoll, Enable)     -- Load Voltage (Ulast) Generation Process

 BEGIN   
   if  reset = '1' then                        -- Resetting all the required signals 
      s_Isoll_act           <=(others=>'0');   -- Actual Value of the set current
      s_Isoll_alt           <=(others=>'0');   -- Old Value of the set current
      s_Isoll_diff          <=(others=>'0');   -- Difference value of the set current 
      
      s_mult_a              <=(others=>'0');
      s_mult_b              <=(others=>'0');
      s_mult32              <=(others=>'0'); 
      
      s_U_Inductive         <=(others=>'0');   -- Inductive Load Voltage Value
      s_U_Resistive         <=(others=>'0');   -- Resistive Load Voltage Value

      ULast                 <=(others=>'0');
      Data_Out_Valid_Ulast  <= '0';
      s_Ulast_STATE         <= EINGANG_SPEICHERN; 
        
   else
      if rising_edge(clk)   THEN
        CASE s_Ulast_STATE  IS

   ---------------------------------------------------------------------------------
    when EINGANG_SPEICHERN =>   --1.Takt
   ---------------------------------------------------------------------------------
      Data_Out_Valid_Ulast     <= '0';
     
      If (Enable= '1' ) then                                          
         s_Isoll_act           <= Isoll;              -- Actual Value of the set current
         s_Ulast_STATE         <= DELTA_I_SOLL; 
      end If ;
      
   ---------------------------------------------------------------------------------
   when DELTA_I_SOLL =>   --2.Takt
   ---------------------------------------------------------------------------------
      if    (s_Isoll_act - s_Isoll_alt) >  32765 then s_Isoll_diff <= to_signed( 32766,16);
      elsif (s_Isoll_act - s_Isoll_alt) < -32765 then s_Isoll_diff <= to_signed(-32766,16);
      else                                            s_Isoll_diff <= s_Isoll_act - s_Isoll_alt; -- SIGNED Difference value of the set current
      end if;
      
      s_Ulast_STATE    <= MULT1_VORBEREITEN;
   
   ---------------------------------------------------------------------------------
   when MULT1_VORBEREITEN =>   --3.Takt
   ---------------------------------------------------------------------------------
      s_mult_a         <= Llast;
      s_mult_b         <= s_Isoll_diff;
      s_Ulast_STATE    <= MULTIPLIKATION1;

   ---------------------------------------------------------------------------------
   when MULTIPLIKATION1 => --4.Takt
   ---------------------------------------------------------------------------------
      s_mult32         <= s_mult_a * s_mult_b;      -- L * di/dt 
      s_Ulast_STATE    <= MULT2_VORBEREITEN;
      
   ---------------------------------------------------------------------------------
   when MULT2_VORBEREITEN => --5.Takt
   ---------------------------------------------------------------------------------
      s_mult_a         <= s_mult32(30 downto 15) ;  -- L * di/dt
      s_mult_b         <= mL;
      s_Ulast_STATE    <= MULTIPLIKATION2;
      
    ---------------------------------------------------------------------------------
   when MULTIPLIKATION2 => --6.Takt
   ---------------------------------------------------------------------------------
      s_mult32         <= s_mult_a * s_mult_b;      -- L * di/dt 
      s_Ulast_STATE    <= MULT2_SPEICHERN;
      
   ---------------------------------------------------------------------------------
   when MULT2_SPEICHERN => --7.Takt
   ---------------------------------------------------------------------------------
      s_U_Inductive    <= s_mult32(30 downto 15) ;  -- (L * di/dt) *mL
      
      s_mult_a         <= Rlast;
      s_mult_b         <= s_Isoll_act;
      s_Ulast_STATE    <= MULTIPLIKATION3;
      
   ---------------------------------------------------------------------------------
   when MULTIPLIKATION3 => --8.Takt
   ---------------------------------------------------------------------------------
      s_mult32         <= s_mult_a * s_mult_b;  -- Rlast * s_Isoll 
      s_Ulast_STATE    <= MULT4_VORBEREITEN;
   
   ---------------------------------------------------------------------------------
   when MULT4_VORBEREITEN => --9.Takt
   ---------------------------------------------------------------------------------
      s_mult_a         <= s_mult32(30 downto 15);
      s_mult_b         <= mR;
      s_Ulast_STATE    <= MULTIPLIKATION4;
      
   ---------------------------------------------------------------------------------
   when MULTIPLIKATION4 => --10.Takt
   ---------------------------------------------------------------------------------
      s_mult32         <= s_mult_a * s_mult_b;    -- (Rlast * s_Isoll) *mR 
      s_Ulast_STATE    <= MULT4_SPEICHERN;
   
   ---------------------------------------------------------------------------------
   when MULT4_SPEICHERN => --11.Takt
   ---------------------------------------------------------------------------------
      s_U_Resistive    <= s_mult32(30 downto 15); -- Rlast * s_Isoll *  mR
      s_Ulast_STATE    <= SUMME; 
   
   ---------------------------------------------------------------------------------
    when SUMME =>            --12.Takt
   ---------------------------------------------------------------------------------
      if    (s_U_Resistive + s_U_Inductive >  32765) then s_Summe <= to_signed( 32766, 16);
      elsif (s_U_Resistive + s_U_Inductive < -32765) then s_Summe <= to_signed(-32766, 16);
      else                                                s_Summe <= s_U_Resistive + s_U_Inductive;
      end if;
      
      s_Ulast_STATE          <= TRANSISTOR;
      
    ---------------------------------------------------------------------------------
    when TRANSISTOR =>            --12.Takt
   ---------------------------------------------------------------------------------
      if (s_Summe > 0) then
         if    (s_Summe + Utransistor >  32766) then ULast <= to_signed( 32767, 16);
         else                                        ULast <= s_Summe + Utransistor;
         end if;
      
      else
         if (s_Summe - Utransistor < -32766) then         ULast <= to_signed(-32767, 16);
         else                                             ULast <= s_Summe - Utransistor;
         end if;
      end if;
      Isoll_diff             <= s_Isoll_diff;
      
      s_Isoll_alt            <= s_Isoll_act;
      Data_Out_Valid_Ulast   <= '1';
      s_Ulast_STATE          <= EINGANG_SPEICHERN;

   END CASE;
    
   END IF;  --clk
   END IF;  --reset
   END PROCESS Load_Voltage_Calculation;

END ARCHITECTURE behavioral; 
