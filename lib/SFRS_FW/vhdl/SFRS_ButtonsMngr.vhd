---------------------------------------------------------------------------------------
-- File name: SFRS_ButtonsMngr.vhd                                                   --
--                                                                                   --
-- Author   : D.Rodomonti                                                            --
-- Date     : 27/06/2024                                                             --
--                                                                                   --
-- Comments : No comments                                                            --
--                                                                                   --
-- History  : Start up version 27/06/2024                                            --
---------------------------------------------------------------------------------------
--            Added some generics to better manage the in/outputs width              --
---------------------------------------------------------------------------------------
--            Added Rx_NewDt2DAC and NewData2DAC signals.07/08/2024                  --
---------------------------------------------------------------------------------------
library ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SFRS_ButtonsMngr is
  Generic(
       gPortNr          : integer range 1 to 12:=6;                                    -- Ports number to manage
       gDt2LEMO_Width   : integer range 1 to 16:=4;                                    -- Data to LEMO width pro channel
       gDt2DAC_Width    : integer range 1 to 32:=20                                    -- Data to DAC width pro channel
       
  );
  Port (
       Clock            : in std_logic;
       Reset            : in std_logic;
       Buttons          : in std_logic_vector(9 downto 0);                             -- Active low
                                                                                       -- (9..8) DAC_Sel_UP,DAC_Sel_DW
                                                                                       -- (7..6) CH_UP,CH_DW
                                                                                       -- (5..4) CMD_UP,CMD_DW
                                                                                       -- (3) Send CMD to the selected channel
                                                                                       -- (2) Send CMD to all channels
                                                                                       -- (1..0) LEMO_Sel_UP,LEMO_Sel_DW
       Rx_Dt2LEMO       : in std_logic_vector((gPortNr*gDt2LEMO_Width)-1 downto 0);    --  With default generics: (23..20) Rx_Dt2LEMO_5
                                                                                       --                         (19..16) Rx_Dt2LEMO_4
                                                                                       --                         (15..12) Rx_Dt2LEMO_3
                                                                                       --                          (11..8) Rx_Dt2LEMO_2
                                                                                       --                           (7..4) Rx_Dt2LEMO_1
                                                                                       --                           (3..0) Rx_Dt2LEMO_0                                                                                                                                                                                                                                                                                                                
       Rx_Dt2DAC        : in std_logic_vector((gPortNr*4*gDt2DAC_Width)-1 downto 0);   -- With default generics:  (479..400) Rx_Dt2DAC_5
                                                                                       --                         (399..320) Rx_Dt2DAC_4
                                                                                       --                         (319..240) Rx_Dt2DAC_3
                                                                                       --                         (239..160) Rx_Dt2DAC_2
                                                                                       --                          (159..80) Rx_Dt2DAC_1
                                                                                       --                            (79..0) Rx_Dt2DAC_0   
       Rx_NewDt2DAC     : in std_logic_vector((gPortNr*2)-1 downto 0);                 -- With default generics:  (11) Rx_NewDt2DAC2_3 port 5
                                                                                       --                         (10) Rx_NewDt2DAC0_1 port 5
                                                                                       --                         (9)  Rx_NewDt2DAC2_3 port 4
                                                                                       --                         (8)  Rx_NewDt2DAC2_3 port 4
                                                                                       --                         (7)  Rx_NewDt2DAC2_3 port 3
                                                                                       --                         (6)  Rx_NewDt2DAC2_3 port 3
                                                                                       --                         (5)  Rx_NewDt2DAC2_3 port 2
                                                                                       --                         (4)  Rx_NewDt2DAC2_3 port 2
                                                                                       --                         (3)  Rx_NewDt2DAC2_3 port 1
                                                                                       --                         (2)  Rx_NewDt2DAC2_3 port 1
                                                                                       --                         (1)  Rx_NewDt2DAC2_3 port 0
                                                                                       --                         (0)  Rx_NewDt2DAC2_3 port 0
       SerialCom_x_Cmd  : out std_logic_vector((gPortNr*4)-1  downto 0);               -- With default generics:    (23..20) SerialCom_5_Cmd
                                                                                       --                           (19..16) SerialCom_4_Cmd
                                                                                       --                           (15..12) SerialCom_3_Cmd
                                                                                       --                            (11..8) SerialCom_2_Cmd
                                                                                       --                             (7..4) SerialCom_1_Cmd
                                                                                       --                             (3..0) SerialCom_0_Cmd
       Data2DAC_Sel     : out std_logic_vector(4 downto 0);                            -- Selector to send back                                                                                                                                                                                                                                                                                                                
       Data2LEMO_Sel    : out std_logic_vector(4 downto 0);                            -- Selector to send back
       NewData2DAC      : out std_logic_vector(1 downto 0);                            -- Selected New DAC data 
       Data2DAC         : out std_logic_vector((4*gDt2DAC_Width)-1 downto 0);          -- Selected DAC data                                                                                                                                                                                                                                                                                                                
       Data2LEMO        : out std_logic_vector(gDt2LEMO_Width-1 downto 0);             -- Selected LEMO data
       
       DAC_Sel_7SegIn   : out std_logic_vector(7 downto 0);                            -- 7-seg Driver input (2LEDs)
       Ch_Sel_7SegIn    : out std_logic_vector(3 downto 0);                            -- 7-seg Driver input (1LEDs)
       Cmd_Sel_7SegIn   : out std_logic_vector(7 downto 0);                            -- 7-seg Driver input (2LEDs)
       LEMO_Sel_7SegIn  : out std_logic_vector(7 downto 0)                             -- 7-seg Driver input (2LEDs)       
    );
end entity SFRS_ButtonsMngr;
 

architecture beh of SFRS_ButtonsMngr is
--
-- Consatnt declaration

--
-- Signal declaration
signal DebButtons                : std_logic_vector(9 downto 0);
signal nDebButtons               : std_logic_vector(9 downto 0);
signal intData2DAC_Sel           : std_logic_vector(4 downto 0);
signal intCH_Sel                 : std_logic_vector(3 downto 0);
signal intCmd_Sel                : std_logic_vector(3 downto 0);
signal intData2LEMO_Sel          : std_logic_vector(4 downto 0);

--
-- Component declaration
Component ACU_InputFilter is
generic  
   (
      gACU_InputFilterVersion    : string := "23.04.21";
      gSignInLenght              : integer range 1 to 64 := 10;  
      gDelayFE_h_RE_l            : std_logic := '1';
      gCyclonIII_n_CyclonV       : std_logic := '0';
      gDelWidth                  : integer range 1 to 20 := 16
   );
  Port (
         Clock                   : in std_logic;                                   -- Clock signal (usually @100MHz)
         Reset                   : in std_logic;                                   -- Asynchronous reset signal active high
         SigIn                   : in std_logic_vector(gSignInLenght-1 downto 0);  -- Input signal to filter
	 MaskIn                  : in std_logic_vector(gSignInLenght-1 downto 0);  -- Input signal mask: when MaskIn(y)='1' then SigOut(y)= SigIn(y). (No filter)
         DelayValue              : in std_logic_vector(gDelWidth-1 downto 0);      -- max=(2^gDelWidth)*10us (with 100MHz clock signal)
         SigOut                  : out std_logic_vector(gSignInLenght-1 downto 0)  -- Filtered output signal 
  );
End component ACU_InputFilter;

Component up_dw_cntCmdInDriven is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       incrCmd          : in std_logic;
       decrCmd          : in std_logic;
       incrCmd_out      : out std_logic;
       decrCmd_out      : out std_logic;
       cntOutValid      : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component up_dw_cntCmdInDriven;


begin

-- Debouncing
i_ACU_InputFilter: ACU_InputFilter 
generic map 
   (
      gCyclonIII_n_CyclonV       => '1'
   )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         SigIn                   => Buttons,
	 MaskIn                  => "0000000000",
         DelayValue              => x"000A",                                        -- 100us filter action
         SigOut                  => DebButtons 
  );
  
nDebButtons <= not (DebButtons);

-- DAC selector 
--	Val2FPGA_Board
i_Dt2DAC_Sel_cnt: up_dw_cntCmdInDriven 
  generic map(
       gCntOutWidth     => 5,
       gEnTrigValue     => 32
    )
  port map(
       clock            => Clock,
       reset            => Reset,
       incrCmd          => nDebButtons(9), 
       decrCmd          => nDebButtons(8),
       incrCmd_out      => open,
       decrCmd_out      => open,
       cntOutValid      => open,
       cntOut           => intData2DAC_Sel
    
    );
    
--	Val27SegLEDs    
p_Dt2DAC_SelLED_Mngr: process(Clock,Reset)
begin
  if (Reset='1') then
    DAC_Sel_7SegIn      <= (others=>'0');
  elsif(Clock'event and Clock='1') then
    case intData2DAC_Sel is
      when "00000" => 
        DAC_Sel_7SegIn <= x"00";
      when "00001" => 
        DAC_Sel_7SegIn <= x"01";
      when "00010" => 
        DAC_Sel_7SegIn <= x"02";
      when "00011" => 
        DAC_Sel_7SegIn <= x"03";
      when "00100" => 
        DAC_Sel_7SegIn <= x"04";
      when "00101" => 
        DAC_Sel_7SegIn <= x"05";
      when "00110" => 
        DAC_Sel_7SegIn <= x"06";
      when "00111" => 
        DAC_Sel_7SegIn <= x"07";
      when "01000" => 
        DAC_Sel_7SegIn <= x"08";
      when "01001" => 
        DAC_Sel_7SegIn <= x"09";
      when "01010" => 
        DAC_Sel_7SegIn <= x"10";
      when "01011" => 
        DAC_Sel_7SegIn <= x"11";
      when "01100" => 
        DAC_Sel_7SegIn <= x"12";
      when "01101" => 
        DAC_Sel_7SegIn <= x"13";
      when "01110" => 
        DAC_Sel_7SegIn <= x"14";
      when "01111" => 
        DAC_Sel_7SegIn <= x"15";
      when "10000" => 
        DAC_Sel_7SegIn <= x"16";
      when "10001" => 
        DAC_Sel_7SegIn <= x"17";
      when "10010" => 
        DAC_Sel_7SegIn <= x"18";
      when "10011" => 
        DAC_Sel_7SegIn <= x"19";
      when "10100" => 
        DAC_Sel_7SegIn <= x"20";
      when "10101" => 
        DAC_Sel_7SegIn <= x"21";
      when "10110" => 
        DAC_Sel_7SegIn <= x"22";
      when "10111" => 
        DAC_Sel_7SegIn <= x"23";
      when "11000" => 
        DAC_Sel_7SegIn <= x"24";
      when "11001" => 
        DAC_Sel_7SegIn <= x"25";
      when "11010" => 
        DAC_Sel_7SegIn <= x"26";
      when "11011" => 
        DAC_Sel_7SegIn <= x"27";
      when "11100" => 
        DAC_Sel_7SegIn <= x"28";
      when "11101" => 
        DAC_Sel_7SegIn <= x"29";
      when "11110" => 
        DAC_Sel_7SegIn <= x"30";
      when "11111" => 
        DAC_Sel_7SegIn <= x"31";
      when others => 
        DAC_Sel_7SegIn <= x"00";

    end case;
  end if;
end process p_Dt2DAC_SelLED_Mngr;

-- CH selector 
--	Val2IntSel
i_CH_Sel_cnt: up_dw_cntCmdInDriven 
  generic map(
       gCntOutWidth     => 4,
       gEnTrigValue     => 6
    )
  port map(
       clock            => Clock,
       reset            => Reset,
       incrCmd          => nDebButtons(7), 
       decrCmd          => nDebButtons(6),
       incrCmd_out      => open,
       decrCmd_out      => open,
       cntOutValid      => open,
       cntOut           => intCH_Sel
    
    );
    

-- Command selector 
--	Val2FPGA_Board
i_cmd_Sel_cnt: up_dw_cntCmdInDriven
  generic map(
       gCntOutWidth     => 4,
       gEnTrigValue     => 16
    )
  port map(
       clock            => Clock,
       reset            => Reset,
       incrCmd          => nDebButtons(5), 
       decrCmd          => nDebButtons(4),
       incrCmd_out      => open,
       decrCmd_out      => open,
       cntOutValid      => open,
       cntOut           => intCmd_Sel
    
    );
    
--	Val27SegLEDs    
p_Cmd_SelLED_Mngr: process(Clock,Reset)
begin
  if (Reset='1') then
    Cmd_Sel_7SegIn      <= (others=>'0');
  elsif(Clock'event and Clock='1') then
    case intCmd_Sel is
      when "0000" => 
        Cmd_Sel_7SegIn <= x"00";
      when "0001" => 
        Cmd_Sel_7SegIn <= x"01";
      when "0010" => 
        Cmd_Sel_7SegIn <= x"02";
      when "0011" => 
        Cmd_Sel_7SegIn <= x"03";
      when "0100" => 
        Cmd_Sel_7SegIn <= x"04";
      when "0101" => 
        Cmd_Sel_7SegIn <= x"05";
      when "0110" => 
        Cmd_Sel_7SegIn <= x"06";
      when "0111" => 
        Cmd_Sel_7SegIn <= x"07";
      when "1000" => 
        Cmd_Sel_7SegIn <= x"08";
      when "1001" => 
        Cmd_Sel_7SegIn <= x"09";
      when "1010" => 
        Cmd_Sel_7SegIn <= x"10";
      when "1011" => 
        Cmd_Sel_7SegIn <= x"11";
      when "1100" => 
        Cmd_Sel_7SegIn <= x"12";
      when "1101" => 
        Cmd_Sel_7SegIn <= x"13";
      when "1110" => 
        Cmd_Sel_7SegIn <= x"14";
      when "1111" => 
        Cmd_Sel_7SegIn <= x"15";
      when others => 
        Cmd_Sel_7SegIn <= x"00";

    end case;
  end if;
end process p_Cmd_SelLED_Mngr;


-- LEMO selector 
--	Val2FPGA_Board
i_LEMO_Sel_cnt: up_dw_cntCmdInDriven 
  generic map(
       gCntOutWidth     => 5,
       gEnTrigValue     => 32
    )
  port map(
       clock            => Clock,
       reset            => Reset,
       incrCmd          => nDebButtons(1), 
       decrCmd          => nDebButtons(0),
       incrCmd_out      => open,
       decrCmd_out      => open,
       cntOutValid      => open,
       cntOut           => intData2LEMO_Sel
    
    );
    
--	Val27SegLEDs    
p_LEMO_SelLED_Mngr: process(Clock,Reset)
begin
  if (Reset='1') then
    LEMO_Sel_7SegIn      <= (others=>'0');
  elsif(Clock'event and Clock='1') then
    case intData2LEMO_Sel is
      when "00000" => 
        LEMO_Sel_7SegIn <= x"00";
      when "00001" => 
        LEMO_Sel_7SegIn <= x"01";
      when "00010" => 
        LEMO_Sel_7SegIn <= x"02";
      when "00011" => 
        LEMO_Sel_7SegIn <= x"03";
      when "00100" => 
        LEMO_Sel_7SegIn <= x"04";
      when "00101" => 
        LEMO_Sel_7SegIn <= x"05";
      when "00110" => 
        LEMO_Sel_7SegIn <= x"06";
      when "00111" => 
        LEMO_Sel_7SegIn <= x"07";
      when "01000" => 
        LEMO_Sel_7SegIn <= x"08";
      when "01001" => 
        LEMO_Sel_7SegIn <= x"09";
      when "01010" => 
        LEMO_Sel_7SegIn <= x"10";
      when "01011" => 
        LEMO_Sel_7SegIn <= x"11";
      when "01100" => 
        LEMO_Sel_7SegIn <= x"12";
      when "01101" => 
        LEMO_Sel_7SegIn <= x"13";
      when "01110" => 
        LEMO_Sel_7SegIn <= x"14";
      when "01111" => 
        LEMO_Sel_7SegIn <= x"15";
      when "10000" => 
        LEMO_Sel_7SegIn <= x"16";
      when "10001" => 
        LEMO_Sel_7SegIn <= x"17";
      when "10010" => 
        LEMO_Sel_7SegIn <= x"18";
      when "10011" => 
        LEMO_Sel_7SegIn <= x"19";
      when "10100" => 
        LEMO_Sel_7SegIn <= x"20";
      when "10101" => 
        LEMO_Sel_7SegIn <= x"21";
      when "10110" => 
        LEMO_Sel_7SegIn <= x"22";
      when "10111" => 
        LEMO_Sel_7SegIn <= x"23";
      when "11000" => 
        LEMO_Sel_7SegIn <= x"24";
      when "11001" => 
        LEMO_Sel_7SegIn <= x"25";
      when "11010" => 
        LEMO_Sel_7SegIn <= x"26";
      when "11011" => 
        LEMO_Sel_7SegIn <= x"27";
      when "11100" => 
        LEMO_Sel_7SegIn <= x"28";
      when "11101" => 
        LEMO_Sel_7SegIn <= x"29";
      when "11110" => 
        LEMO_Sel_7SegIn <= x"30";
      when "11111" => 
        LEMO_Sel_7SegIn <= x"31";
      when others => 
        LEMO_Sel_7SegIn <= x"00";

    end case;
  end if;
end process p_LEMO_SelLED_Mngr;

-- Generic (data commands) mngr
p_dtcmdMngr: process(Clock,Reset)
begin
  if (Reset='1') then
    SerialCom_x_Cmd  <= (others=>'0');
    Data2DAC         <= (others=>'0');                                                                                                                                                                                                                                                                                                                
    Data2LEMO        <= (others=>'0');
    NewData2DAC      <= (others=>'0');
  elsif(Clock'event and Clock='1') then
    
    -- Cmd
    SerialCom_x_Cmd  <= (others=>'0');
    if (nDebButtons(3) = '0') then
      case intCH_Sel is
        when "0000" => 
          SerialCom_x_Cmd(3 downto 0)    <= intCmd_Sel;
        when "0001" => 
          SerialCom_x_Cmd(7 downto 4)    <= intCmd_Sel;
        when "0010" => 
          SerialCom_x_Cmd(11 downto 8)   <= intCmd_Sel;
        when "0011" => 
          SerialCom_x_Cmd(15 downto 12)  <= intCmd_Sel;
        when "0100" => 
          SerialCom_x_Cmd(19 downto 16)  <= intCmd_Sel;
        when "0101" => 
          SerialCom_x_Cmd(23 downto 20)  <= intCmd_Sel;
        when others => 
          SerialCom_x_Cmd  <= (others=>'0');
      end case;
    
      
    elsif(nDebButtons(2) = '0') then
      SerialCom_x_Cmd  <= intCmd_Sel & intCmd_Sel & intCmd_Sel & intCmd_Sel & intCmd_Sel & intCmd_Sel;
    end if;
    
    -- Dt2DAC
    NewData2DAC  <= Rx_NewDt2DAC( 1+2*to_integer(unsigned(intCH_Sel))   downto 2*to_integer(unsigned(intCH_Sel)) );  
    Data2DAC     <= Rx_Dt2DAC( (4*gDt2DAC_Width-1)+((4*gDt2DAC_Width)*to_integer(unsigned(intCH_Sel)))   downto (4*gDt2DAC_Width)*to_integer(unsigned(intCH_Sel)) );
    
    -- Dt2LEMO
    Data2LEMO  <= Rx_Dt2LEMO( (gDt2LEMO_Width-1)+(gDt2LEMO_Width*to_integer(unsigned(intCH_Sel)))   downto gDt2LEMO_Width*to_integer(unsigned(intCH_Sel)) );
    
  end if;
end process p_dtcmdMngr;

-- Outputs
Data2DAC_Sel   <= intData2DAC_Sel;
Ch_Sel_7SegIn  <= intCH_Sel;
Data2LEMO_Sel  <= intData2LEMO_Sel;

end beh;
