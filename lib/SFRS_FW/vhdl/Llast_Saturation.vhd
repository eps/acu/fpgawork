-- Beschreibung:
-- Diese VHDL Datei berechnet den Kurvenverlauf der Saettigung von Magneten L(I_magnet).Dafür werden 33 Stützstellen linearisiert.
--
-- Der Block implementiert folgende Gleichung:
-- L = (y(k+1)-y(k))*(Iph-IStuetz(k))*a+b0(k)
-- a entspricht dem Faktor 1/1024, also ein Bit Shift nach rechts um 10 Stellen.
-- Eing. Groessen: 
-- clk bspw. 100MHz system clock
-- reset Berechnung neu starten, alles auf Null setzen
-- Enable symbolisiert Gueltigkeit der Eingangsdaten

-- Iph                SIGNED    16Bit  Achtung, 640A entspricht 32767
-- yPunkte   UNSIGNED 16 Bit
-- Ausg. Groessen:
-- LEinspeisung SIGNED 16Bit:  Signed nicht noetig, aber macht die Verbindung zum Vorsteuerungsblock einfacher, da dort alle Variablen 16Bit Signed sind.
--========================================================================

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY Llast_Saturation IS
   GENERIC (
      takt                : INTEGER := 100_000_000; --Initialisierung 100 MHz
      anzahlStuetzstellen : INTEGER := 33         ;
      Faktor              : INTEGER := 14878 ); --14878 fuer 315A Kopf und 31646 fuer 670A kopf 

   PORT (
      --System-Eingaenge:
      clk, reset, Enable  : IN  STD_LOGIC; -- Bei Enable=1 wird immer eine neue Rechnung durchgefuert (berechnung 9Takte)
                                           
      ILast               : IN  SIGNED (15 DOWNTO 0):= (OTHERS => '0');

      -- Outputs
      Llast               : OUT signed (15 downto 0):= (others => '0');  -- Induktivitaet
      Data_Out_Valid      : OUT STD_LOGIC           := '0'   );
   
   END Llast_Saturation;


-- Die Messung dieser Kennlinie geht bis Sättigung von 0,92 das entspricht 700A 

-- Skalierung Fall1
-- der DCCT Kopf ist 315A = 10V DCCT Elektronik
-- MFU skaliert diesen Wert für USI Uebertragung auf 10/11 =9,09V
-- D.h. bei 2^19=524288 Sollwert muss nur 524288*0,909090 = 476625 als Integerwert mit 20Bit sein

-- Da aber hier nur 16Bit Eingang ist (was auch ausreichend ist, und es werden die 16Bit von dem MSB genommen) sind 315A: 32767 * (10/11) = 29788 als Integer
-- Vorrraus gesetzt, dass DCCT Kopf von 315A angeschlossen ist

-- Fuer diesen Saettigungsblock 
-- Also 315A entsprechen dann 315/700A * 30058 =13526 Als integer Wert
-- Das heisst der 16Bit - Eingang muss mit dem Faktor 13526 / 29788 = 0,45075 multipliziert werden
-- bei einer 16bit signed ist das dann 32767 * 0,45075 = 14878  

-- Skalierung Fall2
-- der DCCT Kopf ist 670A = 10V DCCT Elektronik
-- Da aber hier nur 16Bit Eingang ist (was auch ausreichend ist, und es werden die 16Bit von dem MSB genommen) sind 670A: 32767 * (10/11) = 29788 als Integer
-- Also 670A entsprechen dann 670/700A * 30058 =28770 Als integer Wert
-- Das heisst der 16Bit - Eingang muss mit dem Faktor 28770 / 29788 = 0,9658 multipliziert werden
-- bei einer 16bit signed ist das dann 32767 * 0,9658 = 31646  


   ARCHITECTURE behavioral OF Llast_Saturation IS
      TYPE yStuetzstellen IS ARRAY(0 TO anzahlStuetzstellen - 1) OF SIGNED (15 DOWNTO 0); -- Lastinduktivitaet  (x- Achse) 1..0,92 bei ca. 700A
      TYPE xStuetzstellen IS ARRAY(0 TO anzahlStuetzstellen - 1) OF SIGNED (15 DOWNTO 0); -- Laststrom          (y- Achse)
      
      CONSTANT yPunkte : yStuetzstellen := (
         to_signed ( 32767, 16),
         to_signed ( 32767, 16),
         to_signed ( 32767, 16),
         to_signed ( 32767, 16),
         to_signed ( 32767, 16),
         to_signed ( 32767, 16),
         to_signed ( 32767, 16),
         to_signed ( 32767, 16),
         to_signed ( 32767, 16),
         to_signed ( 32767, 16),
         to_signed ( 32763, 16),
         to_signed ( 32755, 16),
         to_signed ( 32745, 16),
         to_signed ( 32733, 16),
         to_signed ( 32719, 16),
         to_signed ( 32706, 16),
         to_signed ( 32695, 16), -- hier halber Strom, kaum ein Saettigungseffekt
         to_signed ( 32686, 16),
         to_signed ( 32680, 16),
         to_signed ( 32657, 16),
         to_signed ( 32627, 16),
         to_signed ( 32598, 16),
         to_signed ( 32566, 16),
         to_signed ( 32498, 16),
         to_signed ( 32384, 16),
         to_signed ( 32239, 16),
         to_signed ( 32029, 16),
         to_signed ( 31807, 16),
         to_signed ( 31526, 16),
         to_signed ( 31216, 16),
         to_signed ( 30879, 16),
         to_signed ( 30481, 16),
         to_signed ( 30058, 16)); --entspricht einer Saettigung von ca. 0,92


      CONSTANT xPunkte : xStuetzstellen := ( -- Amplitudenwerte 330A -> 32767   
         to_signed ( 0,     16),
         to_signed ( 1024,  16),
         to_signed ( 2048,  16),
         to_signed ( 3072,  16),
         to_signed ( 4096,  16),
         to_signed ( 5120,  16),
         to_signed ( 6144,  16),
         to_signed ( 7168,  16),
         to_signed ( 8192,  16),
         to_signed ( 9216,  16),
         to_signed ( 10240, 16),
         to_signed ( 11264, 16),
         to_signed ( 12288, 16),
         to_signed ( 13312, 16),
         to_signed ( 14336, 16),
         to_signed ( 15360, 16),
         to_signed ( 16384, 16),
         to_signed ( 17407, 16),
         to_signed ( 18431, 16),
         to_signed ( 19455, 16),
         to_signed ( 20479, 16),
         to_signed ( 21503, 16),
         to_signed ( 22527, 16),
         to_signed ( 23551, 16),
         to_signed ( 24575, 16),
         to_signed ( 25599, 16),
         to_signed ( 26623, 16),
         to_signed ( 27647, 16),
         to_signed ( 28671, 16),
         to_signed ( 29695, 16),
         to_signed ( 30719, 16),
         to_signed ( 31743, 16),
         to_signed ( 32767, 16));

      SIGNAL s_absILast      : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_absILastFak   : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_ILast_diff    : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      
      SIGNAL s_b0            : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_deltaY        : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
   
      SIGNAL s_Sign          : STD_LOGIC            := '0';      
      
      SIGNAL s_kBereich      : INTEGER RANGE 0 TO anzahlStuetzstellen - 2:= 0;
      
      SIGNAL s_Mult_a        : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Mult_b        : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Mult          : SIGNED(31 DOWNTO 0)  := (OTHERS => '0');
      SIGNAL s_Summe         : SIGNED(15 DOWNTO 0)  := (OTHERS => '0');
      
   
      TYPE T_STATE_BERECHNUNG IS(EINGANG_SPEICHERN, 
                                 MULTIPLIKATION1_VORBEREITEN,
                                 MULTIPLIKATION1,
                                 BEREICH_BESTIMMEN, 
                                 GERADE_BESTIMMEN,
                                 MULTIPLIKATION2_VORBEREITEN,
                                 MULTIPLIKATION2,
                                 ADDITION,
                                 DATA_VALID);
      SIGNAL s_STATE : T_STATE_BERECHNUNG := EINGANG_SPEICHERN;
 
   
   BEGIN
      berechnung : PROCESS (clk, reset, Enable)
      ALIAS		s_absILastFak :	SIGNED (15 downto 0) IS s_Mult (30 downto 15);  
         
      BEGIN
         IF (reset = '1') THEN
            s_b0            <=(others=>'0');
            s_deltaY        <=(others=>'0');
            s_absILast      <=(others=>'0');
            s_Mult_a        <=(others=>'0');
            s_Mult_b        <=(others=>'0');
            s_Mult          <=(others=>'0');
            Data_Out_Valid  <= '0';
            s_STATE         <= EINGANG_SPEICHERN;
 
         ELSE
            IF rising_edge(clk) THEN   -- ggf. Outputs zuweisen
               CASE s_STATE IS
                  ------------------------------------------------------------------
                  WHEN EINGANG_SPEICHERN => --1.Takt
                  ------------------------------------------------------------------ 
                     Data_Out_Valid   <= '0';
                     
                     if (Enable = '1') THEN 
                        if (ILast > 32765 or ILast < -32765) then s_absILast <= to_signed(32766,16);
                        else                                      s_absILast <= ABS(ILast);
                        end if; 
                        s_STATE       <= MULTIPLIKATION1_VORBEREITEN;  
                     end if;   

                  ------------------------------------------------------------------
                  WHEN MULTIPLIKATION1_VORBEREITEN => --2.Takt
                  ------------------------------------------------------------------  
                     s_Mult_a <= to_signed(Faktor,16);
                     s_Mult_b <= s_absILast          ;
                     s_STATE  <= MULTIPLIKATION1     ;  
                     
                 ------------------------------------------------------------------
                  WHEN MULTIPLIKATION1 => --3.Takt
                  ------------------------------------------------------------------  
                     s_Mult  <= s_Mult_a * s_Mult_b;                                               
                     s_STATE <= BEREICH_BESTIMMEN  ; 
                     
                  ------------------------------------------------------------------
                  WHEN BEREICH_BESTIMMEN => --4.Takt
                  ------------------------------------------------------------------ 
                     -- Bestimmt den Bereich in Abhängigkeit der x-Achsen Stützpunkte
                     IF    s_absILastFak > xPunkte(31) THEN   s_kBereich <= 31;
                     ELSIF s_absILastFak > xPunkte(30) THEN   s_kBereich <= 30;
                     ELSIF s_absILastFak > xPunkte(29) THEN   s_kBereich <= 29;
                     ELSIF s_absILastFak > xPunkte(28) THEN   s_kBereich <= 28;
                     ELSIF s_absILastFak > xPunkte(27) THEN   s_kBereich <= 27;
                     ELSIF s_absILastFak > xPunkte(26) THEN   s_kBereich <= 26;
                     ELSIF s_absILastFak > xPunkte(25) THEN   s_kBereich <= 25;
                     ELSIF s_absILastFak > xPunkte(24) THEN   s_kBereich <= 24;
                     ELSIF s_absILastFak > xPunkte(23) THEN   s_kBereich <= 23;
                     ELSIF s_absILastFak > xPunkte(22) THEN   s_kBereich <= 22;
                     ELSIF s_absILastFak > xPunkte(21) THEN   s_kBereich <= 21;
                     ELSIF s_absILastFak > xPunkte(20) THEN   s_kBereich <= 20;
                     ELSIF s_absILastFak > xPunkte(19) THEN   s_kBereich <= 19;
                     ELSIF s_absILastFak > xPunkte(18) THEN   s_kBereich <= 18;
                     ELSIF s_absILastFak > xPunkte(17) THEN   s_kBereich <= 17;
                     ELSIF s_absILastFak > xPunkte(16) THEN   s_kBereich <= 16;
                     ELSIF s_absILastFak > xPunkte(15) THEN   s_kBereich <= 15;
                     ELSIF s_absILastFak > xPunkte(14) THEN   s_kBereich <= 14;
                     ELSIF s_absILastFak > xPunkte(13) THEN   s_kBereich <= 13;
                     ELSIF s_absILastFak > xPunkte(12) THEN   s_kBereich <= 12;
                     ELSIF s_absILastFak > xPunkte(11) THEN   s_kBereich <= 11;
                     ELSIF s_absILastFak > xPunkte(10) THEN   s_kBereich <= 10;
                     ELSIF s_absILastFak > xPunkte(9)  THEN   s_kBereich <= 9;
                     ELSIF s_absILastFak > xPunkte(8)  THEN   s_kBereich <= 8;
                     ELSIF s_absILastFak > xPunkte(7)  THEN   s_kBereich <= 7;
                     ELSIF s_absILastFak > xPunkte(6)  THEN   s_kBereich <= 6;
                     ELSIF s_absILastFak > xPunkte(5)  THEN   s_kBereich <= 5;
                     ELSIF s_absILastFak > xPunkte(4)  THEN   s_kBereich <= 4;
                     ELSIF s_absILastFak > xPunkte(3)  THEN   s_kBereich <= 3;
                     ELSIF s_absILastFak > xPunkte(2)  THEN   s_kBereich <= 2;
                     ELSIF s_absILastFak > xPunkte(1)  THEN   s_kBereich <= 1;
                     ELSE                                     s_kBereich <= 0;
                     END IF;                     
                     s_STATE <= GERADE_BESTIMMEN;

                  ------------------------------------------------------------------
                  WHEN GERADE_BESTIMMEN => --5.Takt
                  ------------------------------------------------------------------                         
                     s_b0            <= yPunkte(s_kBereich);                                           -- b0 aus dem aktuellen Bereich 
                     s_deltaY        <= resize(signed('0' & std_LOGIC_VECTOR(yPunkte(s_kBereich + 1))) 
                                        - signed('0' & std_LOGIC_VECTOR(yPunkte(s_kBereich))),16);     -- Differenz der y-Achse 
                     s_ILast_diff    <= s_absILastFak - xPunkte(s_kBereich);                           -- Differenz des Stromes zur Stuetzstelle
                     s_STATE         <= MULTIPLIKATION2_VORBEREITEN;
                     
                     
                  ------------------------------------------------------------------
                  WHEN MULTIPLIKATION2_VORBEREITEN =>    --6.Takt
                  ------------------------------------------------------------------ 
                     s_Mult_a        <= s_deltaY       ;
                     s_Mult_b        <= s_ILast_diff   ;
                     s_STATE         <= MULTIPLIKATION2;
                  
                  ------------------------------------------------------------------
                  WHEN MULTIPLIKATION2 =>    --7.Takt
                  ------------------------------------------------------------------ 
                     s_Mult  <= s_Mult_a * s_Mult_b;
                     s_STATE <= ADDITION           ;
 
                  ------------------------------------------------------------------
                  WHEN ADDITION => --8.Takt
                  ------------------------------------------------------------------  
                        if s_b0 + s_Mult(25 DOWNTO 10) > 32766 then s_Summe <= to_signed(32767,16);
                        else                                        s_Summe <= s_b0 + s_Mult(25 DOWNTO 10);
                        end if;                  
                               
                     s_STATE        <= DATA_VALID;

                  ---------------------------------------------------------------------------------
                  when DATA_VALID => --9. Takt
                  ---------------------------------------------------------------------------------  
                     Llast          <= s_Summe ;
                     Data_Out_Valid <= '1';
                     s_STATE        <= EINGANG_SPEICHERN ;
                  
                  ---------------------------------------------------------------------------------
                  WHEN OTHERS => 
                  ----------------------------------------------------------------------------------
                     s_STATE <= EINGANG_SPEICHERN; 
               END CASE;
            END IF;
         END IF;
      END PROCESS berechnung;

END ARCHITECTURE behavioral;

