------------------------------------------------------------------------------------------------------------------------
-- File name: MAX31865_Driver.vhd                                                                                     --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 23/04/2019                                                                                              --
--                                                                                                                    --
-- Comments : This module is used to read and write the MAX31865 registers.                                           --
--            It uses a standard 4 lines SPI bus as communication protocol.                                           --
--            In order to keep the driver design simple, the read and write actions are periodically executed every   --
--            22ms. Each protocol takes 8 us = 16x200ns (SCLK pulses) + 800ns (CSn2SCLK_SetupTime) + 800ns            --
--           (SleepBetween2Protocols) and the total ammount of protocols sent every 22ms is 13 (see table below).     --
--                                                                                                                    --
--            [0]  Write Configuration Register                                                                       --
--            [1]  Write Max Threshold MSBs Register                                                                  --
--            [2]  Write Max Threshold LSBs Register                                                                  --
--            [3]  Write Min Threshold MSBs Register                                                                  --
--            [4]  Write Min Threshold LSBs Register                                                                  --
--            [5]  Read Configuration Register                                                                        --
--            [6]  Read RTD MSBs Register                                                                             --
--            [7]  Read RTD LSBs Register                                                                             --
--            [8]  Read Max Threshold MSBs Register						                      --
--            [9]  Read Max Threshold LSBs Register                                                                   --
--            [10] Read Min Threshold MSBs Register                                                                   --
--            [11] Read Min Threshold LSBs Register                                                                   --
--            [12] Read Fault Status Register                                                                         --
--                                                                                                                    --
-- History  : Start up version 23/04/2019                                                                             --
------------------------------------------------------------------------------------------------------------------------
--        __    __    __    __    __    __    __    __      __    __    __    __    __    __	__    __    __    __  --
-- clk __|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |_.._|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|    --
--              ______                                                                    	                      --
-- TB  ________|     |___________________________________..__________________________________________________________ --
--                                                                                                                    --
-- cntProt                     0							       X	1             --
--        __________________                                                        _________________                 --
-- SCS_n                   |____________________________.._________________________|     	     |_______________ --
--                                _____       _____         _____       _____            		    _____     --
-- SCLK                     _____|     |_____|     |____.._|     |_____|     |_____________________________|	 |___ --
--                                                                                       		    	      --
--                                                                                                                    --
-- SDt_out                       X   DBgL-1  X   DBgL-2 .. X    DB1    X    DB0          		   X   DBgL-1 --
--                                                                                                                    --
-- SDt_in                           X   DBgL-1  X   DBgL-2 .. X    DB1    X    DB0          		   X   DBgL-1 --
--                                                                                       		              --
-- Data2Read                           X   DBgL-1  X   DBgL-2 .. X    DB1    X    DB0          		   X   DBgL-1 --
--                                                                                        _____	                      --
-- NewDt_Received  ______________________________________..______________________________|     |_____________________ --
--                                                                                                                    --
-- Dt_Received      X             abcde							 X	       12345          --
--                                                                                       		              --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ACU_package.all;

Entity MAX31865_Driver is
  Generic (
         gSCLK_High              : unsigned(7 downto 0):= to_unsigned(20,8);                 -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0):= to_unsigned(20,8);                 -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0):= to_unsigned(80,8);                 -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
         gCntOutWidth            : integer := 24;                                            -- Time base counter width
         gEnTrigValue            : integer := 2200000;                                       -- Time base terminal counter(22ms=2200000*10ns)
         
         g_Offset_pos_neg        : std_logic := '0';                                         -- Vorzeichen pos (0) oder negativ? (1)?
         g_Offset_Temp           : integer := 0                                              -- Offsetwert 
  
  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high
         
         MaxThrs                 : in std_logic_vector(15 downto 0);                         -- Temperature max threshold
         MinThrs                 : in std_logic_vector(15 downto 0);                         -- Temperature min threshold
         
         Offset_Temp             : in std_logic_vector(15 downto 0);                         -- To Compensate an Temperaturoffset
         
         NewDtOut                : out std_logic;                                            -- It is one clock pulse high when all the outputs valid are
	      ReadMaxThrs             : out std_logic_vector(15 downto 0);                        -- Temperature max threshold in use
         ReadMinThrs             : out std_logic_vector(15 downto 0);                        -- Temperature min threshold in use
         ReadCfgReg              : out std_logic_vector(7 downto 0);                         -- Configuration register status
         ReadFaultReg            : out std_logic_vector(7 downto 0);                         -- Fault register status
         ReadTemperature         : out std_logic_vector(15 downto 0);                        -- Temperature measured value
         TemperatureInterlock    : out std_logic;                                            -- It is high when the measured temperature is higher or lower the
                                                                                             -- max/min threshold or when there is an over/uder voltage on the 
                                                                                             -- suppy voltage.
	 
         SCLK                    : out std_logic;                                            -- SPI clock signal
         SCS_n                   : out std_logic;                                            -- Active low SPI Chip Select signal
         SDt_in                  : in  std_logic;                                            -- Serial SPI incoming data 
         SDt_out                 : out std_logic                                             -- Serial SPI outgoing data 
  );
end Entity MAX31865_Driver;

architecture beh of MAX31865_Driver is

--
-- Constants declaration
constant RdReg0                 : std_logic_vector(15 downto 0):=x"0000";
constant RdReg1                 : std_logic_vector(15 downto 0):=x"0100";
constant RdReg2                 : std_logic_vector(15 downto 0):=x"0200";
constant RdReg3                 : std_logic_vector(15 downto 0):=x"0300";
constant RdReg4                 : std_logic_vector(15 downto 0):=x"0400";
constant RdReg5                 : std_logic_vector(15 downto 0):=x"0500";
constant RdReg6                 : std_logic_vector(15 downto 0):=x"0600";
constant RdReg7                 : std_logic_vector(15 downto 0):=x"0700";

constant WrReg0                 : std_logic_vector(15 downto 0):=x"80C0";
constant WrReg3                 : std_logic_vector(15 downto 0):=x"8300";
constant WrReg4		           : std_logic_vector(15 downto 0):=x"8400";
constant WrReg5		           : std_logic_vector(15 downto 0):=x"8500";
constant WrReg6		           : std_logic_vector(15 downto 0):=x"8600";

constant Tc_cntProt             : unsigned(3 downto 0):= x"D" ;                              -- 13 protocols to send every 22 ms
constant Tc_cntWD               : unsigned(9 downto 0):= (to_unsigned(850 ,10));             -- 8,5 us wach dog time(0,5us more than necessary)
constant Tc_cntSleep            : unsigned(9 downto 0):= (to_unsigned(80 ,10));              -- 800 ns sleep between two protocols time

-- Signals declaration
signal Dt2Send                  : array16bits(12 downto 0);                                  -- Protocols to send array.
signal DtReceived               : array16bits(12 downto 0);                                  -- Received protocols array.
signal TB_trigger               : std_logic;                                                 -- Pulse active high with 22 ms period
signal Start_SPI                : std_logic;                                                 -- pulse active high every time a SPI protocol has to start
signal cntProt                  : unsigned(3 downto 0);                                      -- It counts the protocols to send every 22 ms
signal NewDt_Received           : std_logic;                                                 -- Protocol sent/new data received
signal Dt_Received              : std_logic_vector(15 downto 0);                             -- New data received.

signal genCnt                   : unsigned(9 downto 0);                                      -- Generic counter. It is used as:
                                                                                             --   * WachDog counter: it has to be longer than the time required
                                                                                             --     by Generic_4lines_SPI_Driver module to send a protocol (8 us)
                                                                                             --   * SleepBetween 2 protocols counter 
signal Dt_In                    : std_logic_vector(15 downto 0);                             -- Data to send.

signal Temp_Calc                : unsigned(15 downto 0) := (others=>'0');                    -- Calculation Signal for Offset
signal Temp_Value               : std_logic_vector(15 downto 0) := (others=>'0');            -- Get Temp Value 

signal s_Temp_Interlock         : std_logic := '0';

type fsmState is (WaitTB, StartSPI, WaitSPI_Done, SleepB2Prot, Alarm);
signal fsm_state                : fsmState;

--
-- Components declaration
Component cntFreeWheel is
  generic (
      gCntOutWidth     : integer :=20;
      gEnTrigValue     : integer :=10
    );
  port (
      clock            : in std_logic;
      reset            : in std_logic;
      enable           : out std_logic;
      cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntFreeWheel;

Component Generic_4lines_SPI_Driver is
  Generic (
         gSCLK_High              : unsigned(7 downto 0);                                     -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0);                                     -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0);                                     -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
	      gDataWidth              : integer range 0 to 256                                    -- Data to send width. It has to be higher or equal to 
	                                                                                     -- nrSCLK_pulses signal value.
  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high
         NewDt_In                : in  std_logic;                                            -- One clock cycle pulse active high when there is a new Dt_In
	                                                                                     -- or when a read action has to start. 
         Dt_In                   : in std_logic_vector(gDataWidth-1 downto 0);               -- Data to send.

         nrSCLK_pulses           : in std_logic_vector (7 downto 0);                         -- Number of SCLK pulses for each NewVal received

         NewDt_Received          : out std_logic;                                            -- One clock cycle pulse active high when there is a new Dt_Received 
         Dt_Received             : out std_logic_vector(gDataWidth-1 downto 0);              -- Data to send.
	 
         SCLK                    : out std_logic;                                            -- SPI clock signal
         SCS_n                   : out std_logic;                                            -- Active low SPI Chip Select signal
         SDt_in                  : in  std_logic;                                            -- Serial SPI incoming data 
         SDt_out                 : out std_logic                                             -- Serial SPI outgoing data 
  );
end component Generic_4lines_SPI_Driver;


begin

--
-- Time base instance
i_cntFreeWheel: cntFreeWheel
  generic map(
       gCntOutWidth     => gCntOutWidth,
       gEnTrigValue     => gEnTrigValue
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       enable           => TB_trigger,
       cntOut           => open
    
    );
--
-- SPI protocol generator

Dt_In  <= Dt2Send(to_integer(cntProt));

i_Generic_4lines_SPI_Driver :Generic_4lines_SPI_Driver
  Generic map(
      gSCLK_High              => gSCLK_High,
      gSCLK_Low               => gSCLK_Low,
      gCSn2SCLK_SetupTime     => gCSn2SCLK_SetupTime,
	   gDataWidth              => 16
	                                                                                     
  )
  Port map(
      Clock                   => Clock,
      Reset                   => Reset,
      NewDt_In                => Start_SPI,
	                                                        
      Dt_In                   => Dt_In,

      nrSCLK_pulses           => x"0F",

      NewDt_Received          => NewDt_Received,
      Dt_Received             => Dt_Received,
	 
      SCLK                    => SCLK,
      SCS_n                   => SCS_n,
      SDt_in                  => SDt_in,
      SDt_out                 => SDt_out
  );


p_FSM:process(Clock,Reset)
begin
  if (Reset='1') then
    -- State evolution
    fsm_state   <= WaitTB;

    -- Signals evolution
    Dt2Send     <= (others=>(others=>'0'));
    DtReceived  <= (others=>(others=>'0'));
    Start_SPI   <= '0';
    genCnt      <= (others=>'0');
    cntProt     <= (others=>'0');
    NewDtOut    <= '0';
    
    Temp_Calc   <= (others=>'0');
    Temp_Value  <= (others=>'0');
    
    s_Temp_Interlock <= '0';
    
  elsif(Clock'event and Clock='1') then
    --
    -- data received array
    if (NewDt_Received='1') then
      DtReceived(to_integer(cntProt)) <= Dt_Received;
    end if;
    
    case fsm_state is
      when WaitTB =>
      
        NewDtOut       <= '0';
      
        if (TB_trigger='1') then
          -- State evolution
          fsm_state  <= StartSPI;
	  
                -- Signals evolution
           Dt2Send(0)   <= WrReg0(15 downto 2) & DtReceived(7)(0) & WrReg0(0);
           Dt2Send(1)   <= WrReg3(15 downto 8) & MaxThrs(15 downto 8);
           Dt2Send(2)   <= WrReg4(15 downto 8) & MaxThrs(7 downto 0);
           Dt2Send(3)   <= WrReg5(15 downto 8) & MinThrs(15 downto 8);
           Dt2Send(4)   <= WrReg6(15 downto 8) & MinThrs(7 downto 0);
           Dt2Send(5)   <= RdReg0;
           Dt2Send(6)   <= RdReg1;
           Dt2Send(7)   <= RdReg2;
           Dt2Send(8)   <= RdReg3;
           Dt2Send(9)   <= RdReg4;
           Dt2Send(10)  <= RdReg5;
           Dt2Send(11)  <= RdReg6;
           Dt2Send(12)  <= RdReg7;
              end if;
            
      when StartSPI  =>

        -- State evolution
        fsm_state   <= WaitSPI_Done;

        -- Signals evolution
        Start_SPI  <= '1';

      when WaitSPI_Done   =>

        -- State evolution
         if (TB_trigger='1') then
           -- unexpected TB_trigger
                fsm_state   <= Alarm;
         else
           if (genCnt < Tc_cntWD-1) then
             if (NewDt_Received='1') then
               fsm_state   <= SleepB2Prot;
                    genCnt      <= (others=>'0');
             else
               genCnt  <= genCnt + 1;
             end if;
             
           else
             -- No answer from Generic_4lines_SPI_Driver module
             fsm_state   <= Alarm;
             
             genCnt      <= (others=>'0');
           end if;
         end if;

              -- Signals evolution
         Start_SPI  <= '0';	
         
      when SleepB2Prot  =>

              -- State evolution
         if (TB_trigger='1') then
           -- unexpected TB_trigger
                fsm_state   <= Alarm;
         else
           if (genCnt < Tc_cntSleep-1) then
                  genCnt  <= genCnt + 1;
           else
             genCnt  <= (others=>'0');
             if (cntProt < Tc_cntProt-1) then
               fsm_state  <= StartSPI;
               cntProt    <= cntProt + 1;
             else
               fsm_state  <= WaitTB;    
               cntProt    <= (others=>'0');
               NewDtOut   <= '1';
             end if;
           end if;
         end if;
	
      when Alarm  =>
        -- State evolution
        fsm_state  <= WaitTB;    

        -- Signals evolution
        genCnt   <= (others=>'0');	
        cntProt  <= (others=>'0');	
      when others =>

        -- State evolution
        fsm_state  <= WaitTB;

        -- Signals evolution
        Dt2Send     <= (others=>(others=>'0'));
        DtReceived  <= (others=>(others=>'0'));
        Start_SPI   <= '0';
        genCnt      <= (others=>'0');
        cntProt     <= (others=>'0');

    end case;
    
    -- The MAX31865 ADC is 15 bits !!
    -- Calculate offset 
    Temp_Value            <= '0' & DtReceived(6)(7 downto 0) & DtReceived(7)(7 downto 1);
    
    if    (g_Offset_pos_neg = '0') then  
         Temp_Calc  <= unsigned(Temp_Value) + unsigned(Offset_Temp) + to_unsigned(g_Offset_Temp, 16); -- Wenn Pos Offset
    else                                 
         Temp_Calc  <= unsigned(Temp_Value) - unsigned(Offset_Temp) - to_unsigned(g_Offset_Temp, 16);
    end if;
    
    -- Bei Ueber- oder Unterschreitung der Schwellenwerte
     if(std_logic_vector(Temp_Calc) > MaxThrs OR std_logic_vector(Temp_Calc) < MinThrs) then
         s_Temp_Interlock <= '1';
     else
         s_Temp_Interlock <= '0';
     end if;
     
  end if;

end process p_FSM;
   
-- Outputs
ReadCfgReg            <= DtReceived(5)(7 downto 0);

--Temp_Value            <= '0' & DtReceived(6)(7 downto 0) & DtReceived(7)(7 downto 1);
--Temp_Calc             <= unsigned(Temp_Value) - to_unsigned(g_Offset_Temp,16);
ReadTemperature       <= std_logic_vector(Temp_Calc) ;

ReadMaxThrs           <= DtReceived(8)(7 downto 0) & DtReceived(9)(7 downto 0);
ReadMinThrs           <= DtReceived(10)(7 downto 0) & DtReceived(11)(7 downto 0);
ReadFaultReg          <= DtReceived(12)(7 downto 0);

TemperatureInterlock  <= s_Temp_Interlock;


--TemperatureInterlock  <= DtReceived(7)(0);


end beh;
