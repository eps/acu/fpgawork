--  Filename: pi_controller.vhd (geeignet nur fuer Winkel PLL)
--
--  Beschreibung:
--      Dieser Regler hat die P-, I-Anteile. Weiterhin 
--      besitzt dieser Regler die sogenannte Anti- WindUp Funktion, d.h. 
--      der Integrator bleibt stehen sobald der Ausgang des Reglers an die 
--      vorgegebene Grenze stoesst.

--  Eing. Groessen:                    
--      clk              bspw. 100MHz system clock
--      reset            Berechnung neue starten, alles auf Null setzten
--      Eingang          Eingang der (soll - ist) Abweichung vom Typ signed die Breite 16 bit
--      Enable   		   symbolisiert Gueltigkeit der Eingangsdaten
--
--      P-Verstaerkung   0 ..1023  Zahlenformat (10 Bit ohne Nachkomma)
--		  I-Verstaerkung   0 ..1023  Zahlenformat (10 Bitohne Nachkomma)

--  Ausg. Groessen: 
--      Ausgang         Reglerausgang: 18bit unsigned
--      Data_Out_Valid  wenn die Daten berechnet sind = '1' 


--=========================================================================
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;


--Entity Declaration======================================================
ENTITY pid_regler IS
   PORT(
      --System-Eingaenge: 
      clk, reset, Enable   : in std_logic  ;

      -- Eingaenge:
      Eingang              : in signed   (15 downto 0); -- der Eingang entspricht e(t) = (Sollwert w(t) - Istwert y(t))
		Kp                   : in unsigned (9 downto 0);  -- P-Verstaerkung 10Bit
      Ki                   : in unsigned (9 downto 0);  -- I-Verstaerkung 10Bit
		Offset               : in unsigned (17 downto 0); -- Offset 18 Bit um die Phasenverschiebung durch einen Elektronik/Berechnungs Delay auszugleichen
		resetIntegrator      : in std_logic:='0';

      -- Ausgaenge:
      Ausgang              : out unsigned(17 downto 0)	:= (others => '0');
		Ausgang_korrigiert   : out unsigned(17 downto 0)	:= (others => '0');
      Data_Out_Valid       : out std_logic             	:= '0' );

END pid_regler;

--===========================================================================
ARCHITECTURE behavioral OF pid_regler IS

SIGNAL   s_Eingang_aktuell_abs  	: unsigned(15 downto 0) :=(others => '0'); 	-- 16 bit Speicherung des Einganges
SIGNAL   s_Eingang_vorzeichen  	: std_logic 				:= '0'; 				 	-- ='0' positiv

SIGNAL   s_PAnteil          		: unsigned(24 downto 0) :=(others => '0');	-- 25Bit e(t)* Kp Ergebnise der Multiplikation      
SIGNAL   s_IAnteil          		: unsigned(24 downto 0) :=(others => '0');   -- 25Bit e(t)* Ki  nicht korrigierte I-Anteil bei der Multiplikation
SIGNAL   s_Summe            		: unsigned(17 downto 0) :=(others => '0');   -- 18 bit Die Summe aller drei Anteile
SIGNAL   s_Integral_vorhin  		: unsigned(17 downto 0) :=(others => '0');   -- 18 bit Speicherung des Integralergebnises

--State Machine Declaration
TYPE STATE_PID_CONTROLLER IS(EINGANG_SPEICHERN, MULTIPLIKATION, ADDITION, DATENRAUSGEBEN);
SIGNAL s_STATE : STATE_PID_CONTROLLER := EINGANG_SPEICHERN;


BEGIN
hauptprozess: process (clk, reset) 							-- Main process starts
   BEGIN 
      if reset = '1' then
			s_Eingang_aktuell_abs<=(others=>'0'); 			-- e(k) Wert fuer Eingang auf Null setzeng
			s_Eingang_vorzeichen <='0'; 						-- Vorzeichen des Einganges('0 '= pos)
			s_PAnteil          	<=(others=>'0'); 			-- Multiplikationsergebnise auf Null setzen
			s_IAnteil          	<=(others=>'0');
			s_Summe            	<=(others=>'0'); 			-- Summe des Mult. ergebnises
         s_Integral_vorhin  	<=(others=>'0');
			Data_Out_Valid    	<= '0';      				-- Berechnung ist nicht abgeschlossen
			s_STATE            	<= EINGANG_SPEICHERN;	-- naechster Schritt 
        
      elsif rising_edge(clk)   THEN
         CASE  s_STATE  IS

----------------------------------------------------------------------------
   when EINGANG_SPEICHERN => --1. Takt  Checking the entered inputvalues
----------------------------------------------------------------------------
      Data_Out_Valid    	<= '0'; 
		if (Enable = '1') then
			s_Eingang_aktuell_abs <= unsigned(std_logic_vector(ABS(Eingang)));   --16bit -> 15Bit unsigned
			
			if Eingang(15) ='0' then   --Vorzeichen positiv
				s_Eingang_vorzeichen<= '0' ;
			else
				s_Eingang_vorzeichen<= '1' ;
			end if;    
			s_STATE               <= MULTIPLIKATION;
	   
      else
			s_STATE               <= EINGANG_SPEICHERN;
		end if;

-----------------------------------------------------------------------------
   when MULTIPLIKATION =>  --2. Takt           
-----------------------------------------------------------------------------
      s_PAnteil <= Kp * s_Eingang_aktuell_abs(14 downto 0);  --10bit * 15bit = 25bit Multiplikation fuer P-Anteil
      s_IAnteil <= Ki * s_Eingang_aktuell_abs(14 downto 0);  --10bit * 15bit = 25bit Multiplikation fuer I-Anteil 
      s_STATE   <= ADDITION;

-----------------------------------------------------------------------------
   when ADDITION =>        --3. Takt
----------------------------------------------------------------------------- 
      if (s_Eingang_vorzeichen= '0') then --Eingang ist positiv
			s_Summe <= s_PAnteil(24 downto 7) + s_IAnteil(24 downto 7) + s_Integral_vorhin;
		else
			s_Summe <= s_Integral_vorhin - s_PAnteil(24 downto 7) - s_IAnteil(24 downto 7);
		end if;
		
      s_STATE <= DATENRAUSGEBEN;
  
-----------------------------------------------------------------------------
   when DATENRAUSGEBEN=>  --4.Takt
-----------------------------------------------------------------------------    
		if resetIntegrator= '1' then
			s_integral_vorhin    <=(others=>'0');
		else
			s_Integral_vorhin    <= s_IAnteil(24 downto 7) + s_Integral_vorhin;  
		end if;
		
		Ausgang                 <= s_Summe; 
		Ausgang_korrigiert      <= s_Summe + Offset;
      Data_Out_Valid          <= '1';  --Daten sind jetzt ausgerechnet
		s_STATE                 <= EINGANG_SPEICHERN;   


----------------------------------------------------------------------------
   when others =>  
----------------------------------------------------------------------------
      s_STATE <= EINGANG_SPEICHERN;    
	END CASE;
   END IF;  		--reset
   END process hauptprozess;
END ARCHITECTURE behavioral;  