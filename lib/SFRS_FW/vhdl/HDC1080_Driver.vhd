-----------------------------------------------------------------------------------
-- File name: HDC1080_Driver.vhd                                                 --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 01/02/2021                                                         --
--                                                                               --
-- Comments : HDC1080 humidity and temperature chip i2c driven.                  --
--            HDC1080_Temp and  HDC1080_Hum have 14 bits resolution (MSB).       --
--            The SCLK clock frequency is 10 KHz.                                --
--            This driver executes the actions below in loop as soon has the     --
--            Enable signal is set to 1:                                         --
--              * Configure the chip: + operation mode                           --
--                                    + Heater disabled                          --
--                                    + Temperature and Humidity monitored       --
--                                    + 14 bit resolution for both               --
--              * Read temperature and humidity registers.                       --
--              * Read chip status.                                              --
--                                                                               --
--            During the read temperature and humidity action, the chip triggers --
--            the measurements when the register address 0x00 is written in the  --
--            pointer. After that 6,5 ms are necessary to validate and read      --
--            after that the temperature and humidity registers values.          --
--            The data in output are updated every 21,6251 ms.                   --           
--                                                                               --
-- History  : Start up version 01/02/2021                                        --
-----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.ACU_package.all;

Entity HDC1080_Driver is
  Port (
         Clock                   : in  std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                               -- Asynchronous reset signal active high
         
         Enable                  : in  std_logic;                              -- HDC1080_Driver enable signal active high

         -- i2c pins
         SCLK                    : out std_logic;                              -- i2c clock signal
         SDA                     : inout std_logic;                            -- bidirectional data line
         SDA_dir                 : out std_logic;                              -- for debug only
	 
         -- outputs
         NewHDC1080_Dt           : out std_logic;
         HDC1080_Status          : out std_logic_vector(15 downto 0);          -- Chip status
         HDC1080_Temp            : out std_logic_vector(15 downto 0);          -- Measured temperature
         HDC1080_Humidity        : out std_logic_vector(15 downto 0)           -- Measured humidity 
	 
  );
End entity HDC1080_Driver;

Architecture beh of HDC1080_Driver is
--
-- Constants declaration
constant Tc_cntProt             : unsigned(1 downto 0) :=to_unsigned(3,2);
constant Tc_cntDtFromHDC1080    : unsigned(2 downto 0) :=to_unsigned(6,3);
constant Tc_cntAddr_Dt_2_WR     : unsigned(3 downto 0) :=to_unsigned(11,4);
constant Tc_cntWaitMeas         : unsigned(19 downto 0):=to_unsigned(650000,20);

constant Addr_Dt_2_WR_array     : array8bits(10 downto 0):=(x"00",x"81",x"02",x"80",
                                                            x"81",x"00",x"80",
                                                            x"00",x"10",x"02",x"80");
							    
type array9bits is array (2 downto 0) of std_logic_vector(8 downto 0);
constant WordPerProt            : array9bits:=("000000101","000000111","000000100");
							   
--
-- Signals declaration
signal cntProt                  : unsigned(1 downto 0);
signal cntDtFromHDC1080         : unsigned(2 downto 0);
signal cntAddr_Dt_2_WR          : unsigned(3 downto 0);
signal cntWaitMeas              : unsigned(19 downto 0);

signal DtFromHDC1080            : array8bits(6 downto 0);
signal intAddr_Dt_2_WR          : std_logic_vector(7 downto 0);
signal EnableI2C                : std_logic; 
signal WxProt                   : std_logic_vector(8 downto 0);
signal intRdWr_l                : std_logic;

signal RdData                   : std_logic_vector(7 downto 0);
signal NewRdData                : std_logic;
signal Dt2WrReq                 : std_logic;
signal EndProtocol              : std_logic;	 
signal IncomingStartRd          : std_logic;
signal waitFlag                 : std_logic;
signal EndProtocol_s0           : std_logic;
signal EndProtocol_RE           : std_logic;
--
-- Component
Component  dr_i2c is
generic  
   (
      gCtrlByte                  : std_logic_vector(7 downto 0):= x"A0";       -- it defines the i2c chip  
      gSCLKPeriodIn_ns           : integer range 20 to 10000:=500              -- it defines the SCLK period. 
                                                                               -- min=20 ns; max 100 us; 50% duty cycle with Clock=100MHz
									       
         );
  Port (
         Clock                   : in std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in std_logic;                               -- Asynchronous reset signal active high
         RdWr_l                  : in std_logic;                               -- command type 1= RD; 0= WR.
         Addr_Dt_2_WR            : in std_logic_vector(7 downto 0);            -- Start address/Data to write
         StartEn                 : in std_logic;                               -- Start enable command
									       
         DataWordToProcess       : in std_logic_vector(8 downto 0);            -- it defines the number of data words to RD/WR each i2c cycle:
                                                                               --     RD => min=4; max=259
                                                                               --     WR => min 3; max=10
									       
         RdData                  : out std_logic_vector(7 downto 0);           -- Read data
         NewRdData               : out std_logic;                              -- New read data pulse.
         SDA_dir                 : out std_logic;                              -- for debug only
         Dt2WrReq                : out std_logic;                              -- in Wr mode, it asks for new data to write.
         EndProtocol             : out std_logic;                              -- it is a pulse active high when the stop command is sent
	 
         IncomingStartRd         : out std_logic;                              -- It is a pulse active high when a read protocol is going to start
	 
         -- i2c pins
         SCLK                    : out std_logic;                              -- i2c clock signal
         SDA                     : inout std_logic                             -- bidirectional data line
  );

end component dr_i2c;

begin


--
-- Data from HDC1080
p_DtFromHDC1080: process (Reset,Clock)
begin
  if (Reset='1') then
    cntDtFromHDC1080  <= (others=>'0');
    DtFromHDC1080     <= (others=>(others=>'0'));
  elsif (Clock'event and Clock='1') then
  
    if (NewRdData= '1') then
      DtFromHDC1080(to_integer(cntDtFromHDC1080))  <= RdData;
      
      if (cntDtFromHDC1080 < Tc_cntDtFromHDC1080 - 1) then
        cntDtFromHDC1080  <= cntDtFromHDC1080 + 1;
      else
        cntDtFromHDC1080  <= (others=>'0');
      end if;
      
    end if;
    
  end if;
end process;

--
-- Addr_Dt_2_WR process
p_Addr_Dt_2_WR: process (Reset,Clock)
begin
  if (Reset='1') then
    cntAddr_Dt_2_WR  <= (others=>'0');
    intAddr_Dt_2_WR  <= (others=>'0');
  elsif (Clock'event and Clock='1') then
    
    intAddr_Dt_2_WR  <= Addr_Dt_2_WR_array(to_integer(cntAddr_Dt_2_WR));
    
    if (Dt2WrReq= '1') then
      
      if (cntAddr_Dt_2_WR < Tc_cntAddr_Dt_2_WR - 1) then
        cntAddr_Dt_2_WR  <= cntAddr_Dt_2_WR + 1;
      else
        cntAddr_Dt_2_WR  <= (others=>'0');
      end if;
      
    end if;
    
  end if;
end process;

--
-- protocol counter
p_cntProt: process (Reset,Clock)
begin
  if (Reset='1') then
    cntProt         <= (others=>'0');
    WxProt          <= (others=>'0');
    intRdWr_l       <= '0';
    NewHDC1080_Dt   <= '0';

    EndProtocol_s0  <= '0';
    EndProtocol_RE  <= '0';
  elsif (Clock'event and Clock='1') then

    EndProtocol_s0  <= EndProtocol;
    EndProtocol_RE  <= EndProtocol and not EndProtocol_s0;
  
    WxProt         <= WordPerProt(to_integer(cntProt));
    NewHDC1080_Dt  <= '0';

    if (EndProtocol_RE= '1') then
      
      if (cntProt < Tc_cntProt - 1) then
        cntProt        <= cntProt + 1;
      else
        cntProt        <= (others=>'0');
        NewHDC1080_Dt  <= '1';
      end if;
      
    end if;
    
    if (cntProt > 0) then
      intRdWr_l  <= '1';
    else
      intRdWr_l  <= '0';  
    end if;
    
  end if;
end process;

--
-- enable i2c process
p_i2cEn: process (Reset,Clock)
begin
  if (Reset='1') then
    EnableI2C    <= '0';
    waitFlag     <= '0';
    cntWaitMeas  <= (others=>'0');
  elsif (Clock'event and Clock='1') then
  
    if (IncomingStartRd = '1' and cntProt="01") then
      waitFlag   <= '1';
    elsif(cntWaitMeas = Tc_cntWaitMeas-2) then
      waitFlag   <= '0';
    end if;
  
  
    if (waitFlag = '1') then
      
      EnableI2C  <= '0';
      
      if (cntWaitMeas < Tc_cntWaitMeas-1) then
        cntWaitMeas  <= cntWaitMeas + 1;
      else
        cntWaitMeas  <= (others=>'0');
      end if;
    
    else
    
      --EnableI2C  <= Enable;
      EnableI2C  <= Enable and not EndProtocol_s0;
      
    end if;
    
     
  end if;
end process;

--
-- i2c Instance
i_dr_i2c : dr_i2c
  generic map 
   (
      gCtrlByte                  => x"80",
      gSCLKPeriodIn_ns           => 10000
   )
  Port map (
         Clock                   => Clock,
         Reset                   => REset,
         RdWr_l                  => intRdWr_l,
         Addr_Dt_2_WR            => intAddr_Dt_2_WR,

         StartEn                 => EnableI2C,
									       
         DataWordToProcess       => WxProt,
									       
         RdData                  => RdData,
         NewRdData               => NewRdData,
         SDA_dir                 => SDA_dir,
         Dt2WrReq                => Dt2WrReq,
         EndProtocol             => EndProtocol,
	 
         IncomingStartRd         => IncomingStartRd,
	 
         -- i2c pins
         SCLK                    => SCLK,
         SDA                     => SDA
  );

--
-- outputs
HDC1080_Temp	  <= DtFromHDC1080(0) & DtFromHDC1080(1);
HDC1080_Humidity  <= DtFromHDC1080(2) & DtFromHDC1080(3);
HDC1080_Status    <= DtFromHDC1080(4) & DtFromHDC1080(5);

end beh;
