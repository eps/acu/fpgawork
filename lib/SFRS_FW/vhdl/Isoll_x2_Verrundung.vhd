--  VERSION kONTROLLE:

-- Isoll_x2_Verrundung.vhd File

--  28.07.2016, Created, A.Wiest

--               ---VERSION KONTROLLE --
--            Bescheibung                                                          Autor                      Datum
--            born                                                                 A.Wiest                28.07.2016
--            Embedded Multiplikatoren sind durch generice Registers ergesetz.     A.Wiest                26.01.2017
--
-- Berechnung des Stromsollwertes mit einem FPGA :

-- Isoll(count) = k1n_round x count             , if  0 count < t1
--              = k2n_round - (count- t2)^2     , if t1 count < t2
--              = k2n_round                     , if t2 count < t3
--              = k2n_round (count -  t3)^2     , if t3 count < t4
--              = k3n_round k1n_round x count   , otherwise
--
--  TDAC_sample= 20 us
--
---- This block is used to generate the Ramp up , Flat and Ramp down (Isoll) signals used for all the Feedforward blocks as input.
--
--

--                             +Ve
--
--    VERRUNDUNG -->      ------------------------   <-- VERRUNDUNG
--                    -----------------------------
--                  --------------------------------
--                 -----------------------------------
--               --------------------------------------
--              ----------------------------------------
--            --------------------------------------------
--          ------------------------------------------------
--        ---------------------------------------------------
--      -------------------------------------------------------
--    ----------------------------------------------------------
--  --------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
--                                                                   --------------------------------------------------------------
--                                                                      ----------------------------------------------------------
--                                                                        -------------------------------------------------------
--                                                                          ---------------------------------------------------
--                                                                           ------------------------------------------------
--                                                                             --------------------------------------------
--                                                                              ----------------------------------------
--                                                                                --------------------------------------
--                                                                                  -----------------------------------
--                                                                                    --------------------------------
--                                                                                      -----------------------------
--                                                                     VERRUNDUNG -->        ------------------------  <-- VERRUNDUNG
--
--                                                                                                     -Ve


---====================================================================================================================================================

LIBRARY   ieee;                                 -- Library Declaration
USE       ieee.std_logic_1164.all;
USE       IEEE.STD_LOGIC_UNSIGNED.ALL;
USE       ieee.numeric_std.ALL;

--=================================================================================================

entity Isoll_x2_Verrundung  is                           -- ENTITY Declaration

GENERIC ( clk_in_hz        : integer  := 100_000_000);   -- 100Mhz system clock (beim Cyclone 4 waren nur 50MHz moeglich)

PORT (
   clk                     : in  std_logic;              -- Clock Signal
   reset                   : in  std_logic;              -- reset Signal
   Enable                  : in  std_logic;

   Only_DC                 : in  std_logic;              -- ='1' nur DC Wert

   Ext_takt_aktiv          : in  std_logic;              -- ='1' Sollwertberechnung von dem externem Takt, somit wird der Abstand zw. den Stuetzpunkten vorgeg.
   Takt_extern             : in  std_logic;              -- ='1' haupt Counter wird erhöht
   Flattop_Konstant        : in  std_logic;              -- ='1' Flattop bleibt konstant ; ='0' es geht weiter
   PauseNachRampe          : in  std_logic;              -- ='1' Nach der Rampe wirs pausiert

   -- Zeiten
   t1                      : in unsigned (23 downto 0);  -- 60 fuer TDAC_sample= 6 us ,  72  fuer TDAC_sample = 5us
   t2                      : in unsigned (23 downto 0);  -- 90 fuer TDAC_sample= 6 us ,  108 fuer TDAC_sample = 5us
   t3                      : in unsigned (23 downto 0);  -- 390 fuer TDAC_sample= 6 us , 468 fuer TDAC_sample = 5us
   t4                      : in unsigned (23 downto 0);  -- 420 fuer TDAC_sample= 6 us , 504 fuer TDAC_sample = 5us
   Zaehler_max             : in unsigned (23 downto 0);  -- 480 fuer TDAC_sample= 6 us , 576 fuer TDAC_sample = 5us
   Abtastzeit              : in unsigned (15 downto 0);  -- 9 Takte abziehn! z.B 991 fuer Ttakt=20us bei 50MHz

   -- Konstanten
   k1n_round               : in unsigned (23 downto 0);  -- 60 fuer TDAC_sample= 6 us , 144 fuer TDAC_sample = 5us
   k2n_round               : in unsigned (23 downto 0);  -- 4500 fuer TDAC_sample= 6 us, 10368 fuer TDAC_sample = 5us
   k3n_round               : in unsigned (23 downto 0);  -- 28800 fuer TDAC_sample= 6 us, 82944  fuer TDAC_sample = 5us
   Offset                  : in   signed (23 downto 0);  -- Achtung Offset darf nicht grösser werden als 20V-Uss_Usoll (Uss Spitze  Spitze Wert)

   -- Konstanten fuer Steigung
   Steigung                : in unsigned (23 downto 0);  -- 39 fuer TDAC_sample= 6 us fuer runduszeit = 360 us ,23 fuer TDAC_sample= 5 us   (23),fuer runduszeit = 360 us
   Amplitude               : in unsigned (23 downto 0);  -- 142 fuer TDAC_sample= 6 us fuer runduszeit = 360 us , 134 fuer TDAC_sample= 5 us  (134), fuer runduszeit = 360 us

   Isoll_out               : out signed  (19 downto 0):= (others => '0');
   Data_Out_Valid          : out std_logic            := '0');      -- ='1' falls ein Durchlauf abgeschlossen
END Isoll_x2_Verrundung;
 --=================================================================================================

ARCHITECTURE  behavioral OF Isoll_x2_Verrundung IS

TYPE state_type IS ( RAMP_UP_STATE,
                     VERRUNDUNG_UP_STATE,
                     FLAT_STATE,
                     VERRUNDUNG_DOWN_STATE,
                     RAMP_DOWN_STATE);
SIGNAL  s_ISOLL_STATE: state_type:= RAMP_UP_STATE;  --Statemaschine Signal


--Required signals -------------------------------------------------------------------------------

SIGNAL s_cnt1        : unsigned (23 downto 0) := (others=>'0');
SIGNAL s_cnt2        : unsigned (23 downto 0) := (others=>'0');
SIGNAL s_cnt3        : unsigned (23 downto 0) := (others=>'0');

SIGNAL s_state_cnt_0 : integer range 0 to 9   := 0;
SIGNAL s_state_cnt_1 : integer range 0 to 9   := 0;
SIGNAL s_state_cnt_2 : integer range 0 to 9   := 0;
SIGNAL s_state_cnt_3 : integer range 0 to 9   := 0;
SIGNAL s_state_cnt_4 : integer range 0 to 9   := 0;

SIGNAL s_c           : unsigned (23 downto 0) := (others => '0');
SIGNAL s_d           : unsigned (23 downto 0) := (others => '0');

SIGNAL s_Data_Out    : unsigned (19 downto 0) := (others=>'0');

SIGNAL s_sub_24      : unsigned (23 downto 0) := (others=>'0');
SIGNAL s_sub_48      : unsigned (47 downto 0) := (others=>'0');
SIGNAL s_mult_48     : unsigned (47 downto 0) := (others=>'0');
SIGNAL s_mult_96     : unsigned (95 downto 0) := (others=>'0');

TYPE Boolean is (positiv, negativ);                     --Isoll half wave state machines
SIGNAL s_halb_wave_zyclus : Boolean;

BEGIN

Isoll_Generation:   process (clk, reset)       --CLOCK Generation process
BEGIN

 if (reset ='1') then
   s_cnt1                  <= (others=>'0');
   s_cnt2                  <= (others=>'0');
   s_cnt3                  <= (others=>'0');
   s_c                     <= (others=>'0');
   s_d                     <= (others=>'0');
   s_sub_24                <= (others=>'0');
   s_sub_48                <= (others=>'0');
   s_mult_48               <= (others=>'0');
   s_mult_96               <= (others=>'0');
   
   s_Data_Out              <= (others=>'0');
   Isoll_out               <= (others=>'0');
   
   s_halb_wave_zyclus      <= positiv;
   Data_Out_Valid          <= '0';
   s_ISOLL_STATE           <= RAMP_UP_STATE;

elsif (rising_edge(clk) and clk = '1') then

   if (Enable = '0') then
      Isoll_out <=(others=>'0');

   elsif (Only_DC= '1') then
      Isoll_out            <= Offset(19 downto 0);
      s_Data_Out           <= (others=>'0');
      
      s_cnt1               <= (others=>'0'); -- Wenn Only_DC wieder auf 0 geht, so startet die Trapez wieder von Anfang an
      s_cnt2               <= (others=>'0'); -- Mann kann das aendern, in dem man diesen Block auskommentier( Achtung nicht geprüft!
      s_Data_Out           <= (others=>'0');
      s_state_cnt_0        <= 0;
      s_state_cnt_1        <= 0;
      s_state_cnt_2        <= 0;
      s_state_cnt_3        <= 0;
      s_state_cnt_4        <= 0;

      s_ISOLL_STATE        <= RAMP_UP_STATE;
      s_halb_wave_zyclus   <= positiv;

      if (s_cnt3 < Abtastzeit) then s_cnt3 <= s_cnt3 + 1;    Data_Out_Valid    <= '0';
      else                          s_cnt3 <= (others=>'0'); Data_Out_Valid    <= '1';
      end if;

   else
      s_cnt3 <= (others=>'0');                                                     -- to generate the negative half wave cycle for the Isoll
      if ( s_halb_wave_zyclus = positiv ) then                                     -- checking half wave cycle
         Isoll_out  <= signed(    std_logic_vector(s_Data_Out(19 downto 0)))    + Offset(19 downto 0);  -- positiv Isoll
      else
         Isoll_out  <= signed(NOT(std_logic_vector(s_Data_Out(19 downto 0)))+1) + Offset(19 downto 0) ; -- making 2's complement of Isoll positiv value
      end if;

      CASE  s_ISOLL_STATE IS        --State Machine Definition

--=================================================================================================
   when RAMP_UP_STATE =>  --[1. Zustand]   Isoll = Steigung_integer * [ k1n_round * count]
--=================================================================================================
      if (s_cnt1 < Abtastzeit AND s_cnt2 <= t1 AND Ext_takt_aktiv= '0' ) then    -- cnt2 zaehlt Anzahl der Ausgaben (Anzahl der Stützpunkten)
         s_cnt1                  <= s_cnt1 + 1;                                  -- Zeit fuer die Ausgabe z.B. alle 20us wird ein neuer wert Ausgegeben
         Data_Out_Valid          <= '0';

      elsif (s_cnt2 <= t1 )  then     -- t1 ist genau der Übergangspunkt
         case (s_state_cnt_0) is
            ------------------------------------
            when 0 =>
               if (Ext_takt_aktiv= '0' OR (Ext_takt_aktiv= '1' AND Takt_extern = '1')) then
                  s_state_cnt_0     <= 1;
               end if;
            when 1 =>
               s_state_cnt_0        <= 2;
            when 2 =>
               s_c                  <= k1n_round;          -- 24 bit
               s_d                  <= s_cnt2;             -- 24 bit entspricht t1 (Anzahl der Punkte im lin. Bereich)
               s_mult_48            <= (others=>'1');
               s_state_cnt_0        <= 3;
            when 3 =>
               s_mult_96            <= s_mult_48 * s_c * s_d;          -- 48 bit
               s_state_cnt_0        <= 4;
            when 4 =>
               s_mult_48            <= s_mult_96(95 downto 48);
               s_c                  <= Steigung;   -- 24 bit
               s_d                  <= Amplitude;  -- 24 bit
               s_state_cnt_0        <= 5;
            when 5 =>
               s_mult_96            <= s_mult_48 * s_c * s_d;  -- 48 + 24 +24
               s_state_cnt_0        <= 6;
            when 6 =>
               s_Data_Out           <= s_mult_96(67-5 downto 48-5);
               s_state_cnt_0        <= 7;
            when 7 =>
               s_state_cnt_0        <= 8;
            when 8 =>
               Data_Out_Valid       <='1';
               s_state_cnt_0        <= 9;
            when 9 =>
               Data_Out_Valid       <= '0';
               s_cnt2               <= s_cnt2 + 1;  -- Hauptzaehler
               s_state_cnt_0        <= 0;
               s_cnt1               <= (others=>'0');
         end case;
         ------------------------------------
      else
         s_state_cnt_0  <= 0;
         s_c            <= (others=>'0');
         s_d            <= (others=>'0');
         s_mult_48      <= (others=>'0');
         s_mult_96      <= (others=>'0');
         s_ISOLL_STATE  <= VERRUNDUNG_UP_STATE;
      end if;

--=================================================================================================
   when VERRUNDUNG_UP_STATE =>  --[2. Zustand] Isoll = Steigung_integer * [ k2n_round - (t2 - count)2]
--=================================================================================================
      if (s_cnt1 < Abtastzeit AND s_cnt2 <= t2 AND Ext_takt_aktiv= '0' ) then  -- +1 weil im Flattop gibts keine Berechnung
         s_cnt1           <= s_cnt1 + 1;
         Data_Out_Valid   <= '0';

      elsif (s_cnt2 <= t2)  then              -- if t2 <= count < t3
         case (s_state_cnt_1) is
            ------------------------------------
            when 0 =>
               if (Ext_takt_aktiv= '0' OR (Ext_takt_aktiv= '1' AND Takt_extern = '1')) then
                  s_state_cnt_1     <= 1;
               end if;
            when 1 =>
               s_sub_24             <= t2 - s_cnt2;
               s_state_cnt_1        <= 2;
            when 2 =>
               s_c                  <= s_sub_24;
               s_d                  <= s_sub_24;
               s_mult_48            <= (others=>'1');
               s_state_cnt_1        <= 3;
            when 3 =>
               s_mult_96            <= s_mult_48 * s_c * s_d;               -- (t2 - count)2
               s_state_cnt_1        <= 4;
            when 4 =>
            -- s_sub_48            <= resize(k2n_round & "00000000",48)          - s_mult_96(95 downto 48);   --  k2n_round - (t2 - count)2, 
               s_sub_48             <= resize(k2n_round & "000000000000000000",48) - s_mult_96(95 downto 48);   --  k2n_round - (t2 - count)2
               s_c                  <= Steigung;
               s_d                  <= Amplitude;
               s_state_cnt_1        <= 5;
            when 5 =>
               s_mult_48            <= s_sub_48;
               s_state_cnt_1        <= 6;
            when 6 =>
               s_mult_96            <= s_mult_48 * s_c * s_d;
               s_state_cnt_1        <= 7;
            when 7 =>
               s_Data_Out           <= s_mult_96(67-5 downto 48-5);
               s_state_cnt_1        <= 8;
            when 8 =>
               Data_Out_Valid       <='1';
               s_state_cnt_1        <= 9;
            when 9 =>
               Data_Out_Valid       <= '0';
               s_cnt2               <= s_cnt2 + 1;
               s_state_cnt_1        <= 0;
               s_cnt1               <= (others=>'0');
         end case;
            ------------------------------------
      else
         s_state_cnt_1  <= 0;
         s_c            <= (others=>'0');
         s_d            <= (others=>'0');
         s_mult_48      <= (others=>'0');
         s_sub_24       <= (others=>'0');
         s_sub_48       <= (others=>'0');
         s_ISOLL_STATE  <= FLAT_STATE;
      end if;
      
--=================================================================================================
   when FLAT_STATE =>  --[3. Zustand]  Isoll = Steigung_integer *  k2n_round
--=================================================================================================
      if (s_cnt1 < Abtastzeit AND s_cnt2 <= t3 AND Ext_takt_aktiv= '0' ) then      -- 6 us time counter    470
         s_cnt1            <= s_cnt1 + 1;
         Data_Out_Valid    <= '0';

      elsif (s_cnt2 <= t3 )  then              -- if t2 <= count < t3
         case (s_state_cnt_2) is
            when 0 =>
               if (Ext_takt_aktiv= '0' OR (Ext_takt_aktiv= '1' AND Takt_extern = '1')) then
                  s_state_cnt_2   <= 1;
               end if;
            when 1 =>   s_state_cnt_2  <= 2;
            when 2 =>   s_state_cnt_2  <= 3;
            when 3 =>   s_state_cnt_2  <= 4;
            when 4 =>   s_state_cnt_2  <= 5;
            when 5 =>   s_state_cnt_2  <= 6;
            when 6 =>   s_state_cnt_2  <= 7;
            when 7 =>   s_state_cnt_2  <= 8;
            when 8 =>   Data_Out_Valid <= '1'; s_state_cnt_2      <= 9;
            when 9 =>   Data_Out_Valid <= '0';
                       
                        if Flattop_Konstant ='0' then
                           s_cnt2      <= s_cnt2 + 1;
                        end if;
                        s_state_cnt_2  <= 0;
                        s_cnt1         <= (others=>'0');
         end case;
            
      else
         s_state_cnt_2      <= 0;
         s_ISOLL_STATE      <= VERRUNDUNG_DOWN_STATE;
      end if;

--=================================================================================================
   when VERRUNDUNG_DOWN_STATE =>  --[4. Zustand] Isoll = Steigung_integer * [ k2n_round - (t3 - count)2 ]
--=================================================================================================

      if (s_cnt1 < Abtastzeit AND s_cnt2 <= t4 AND Ext_takt_aktiv= '0') then                                                 -- 6 us time counter  468
         s_cnt1           <= s_cnt1 + 1;
         Data_Out_Valid   <= '0';

      elsif (s_cnt2 <= t4 ) then
         case (s_state_cnt_3) is
            ------------------------------------
            when 0 =>
               if (Ext_takt_aktiv= '0' OR (Ext_takt_aktiv= '1' AND Takt_extern = '1')) then
                  s_state_cnt_3     <= 1;
               end if;
            when 1 =>
               s_sub_24             <= s_cnt2 - t3;
               s_state_cnt_3        <= 2;
            when 2 =>
               s_c                  <= s_sub_24;
               s_d                  <= s_sub_24;
               s_mult_48            <= (others=>'1');
               s_state_cnt_3        <= 3;
            when 3 =>
               s_mult_96            <= s_mult_48* s_c * s_d;                         -- 48 bit
               s_state_cnt_3        <= 4;
            when 4 =>
             --s_sub_48             <= resize(k2n_round & "00000000",48)         - s_mult_96(95 downto 48); -- 48 bit
               s_sub_48             <= resize(k2n_round & "000000000000000000",48) - s_mult_96(95 downto 48); -- 48 bit
               s_c                  <= Steigung;
               s_d                  <= Amplitude;
               s_state_cnt_3        <= 5;
            when 5 =>
               s_mult_48            <= s_sub_48;
               s_state_cnt_3        <= 6;
            when 6 =>
               s_mult_96            <= s_mult_48* s_c * s_d; --96bit
               s_state_cnt_3        <= 7;
            when 7 =>
               s_Data_Out           <= s_mult_96(67-5 downto 48-5);
               s_state_cnt_3        <= 8;
            when 8 =>
               Data_Out_Valid       <='1';
               s_state_cnt_3        <= 9;
            when 9 =>
               Data_Out_Valid       <='0';
               s_state_cnt_3        <= 0;
               s_cnt2               <= s_cnt2 + 1;
               s_cnt1               <= (others=>'0');
         end case;
            ------------------------------------
      else
         s_state_cnt_3  <= 0;
         s_c            <= (others=>'0');
         s_d            <= (others=>'0');
         s_mult_48      <= (others=>'0');
         s_mult_96      <= (others=>'0');
         s_sub_24       <= (others=>'0');
         s_sub_48       <= (others=>'0');
         s_ISOLL_STATE  <= RAMP_DOWN_STATE;
      end if;
      
--=================================================================================================
   when RAMP_DOWN_STATE =>  --[5. Zustand]  Isoll = Steigung_integer * [ k3n_round - k1n_round * count] otherwise
--=================================================================================================
      if (s_cnt1 < Abtastzeit AND s_cnt2 <= Zaehler_max AND Ext_takt_aktiv = '0') then        -- 6 us time counter   469
         s_cnt1           <= s_cnt1 + 1;
         Data_Out_Valid   <= '0';

      elsif ( s_cnt2 <= Zaehler_max)  then
         case (s_state_cnt_4) is
            ------------------------------------
            when 0 =>
               if (Ext_takt_aktiv= '0' OR (Ext_takt_aktiv= '1' AND Takt_extern = '1')) then
                  s_state_cnt_4     <= 1;
               end if;
            when 1 =>
               s_state_cnt_4        <= 2;
            when 2 =>
               s_c                  <= k1n_round;
               s_d                  <= s_cnt2;
               s_mult_48            <= (others=>'1');
               s_state_cnt_4        <= 3;
            when 3 =>
               s_mult_96            <= s_mult_48 * s_c * s_d;                          -- 48 bit
               s_state_cnt_4        <= 4;
            when 4 =>
             --s_sub_48             <= resize(k3n_round & "00000000",48)         - s_mult_96(95 downto 48);   -- 48 bit
               s_sub_48             <= resize(k3n_round & "000000000000000000",48) - s_mult_96(95 downto 48);   -- 48 bit
               s_c                  <= Steigung;
               s_d                  <= Amplitude;
               s_state_cnt_4        <= 5;
            when 5 =>
               s_mult_48            <= s_sub_48;
               s_state_cnt_4        <= 6;
            when 6 =>
               s_mult_96            <= s_mult_48 * s_c * s_d;   -- 96bit
               s_state_cnt_4        <= 7;
            when 7 =>
               s_Data_Out           <= s_mult_96(67-5 downto 48-5);
               s_state_cnt_4        <= 8;
            when 8 =>
               Data_Out_Valid       <='1';
               s_state_cnt_4        <= 9;
            when 9 =>
               Data_Out_Valid       <='0';
               s_state_cnt_4        <= 0;
               s_cnt1               <= (others=>'0');
               s_cnt2               <= s_cnt2 + 1;

         end case;
            -----------------------------------
      else

         if PauseNachRampe ='0' then                  -- keine Pause, es geht direkt weiter
            if ( s_halb_wave_zyclus = positiv ) then  -- checking half wave cycle
               s_halb_wave_zyclus  <= negativ;        -- to generate the negative half wave cycle for the Isoll
            else
               s_halb_wave_zyclus  <= positiv;
            end if;

            s_state_cnt_4     <= 0;
            s_cnt2            <= to_unsigned(1, 24);
            s_c               <= (others=>'0');
            s_d               <= (others=>'0');
            s_mult_48         <= (others=>'0');
            s_mult_96         <= (others=>'0');
            s_sub_48          <= (others=>'0');
            s_ISOLL_STATE     <= RAMP_UP_STATE;
         end if;
      end if;

   end case ;

   end if; --Enable
   end if; --clk

 end process Isoll_Generation;

END ARCHITECTURE behavioral;
