-- Aufgabe dieses Blocks ist es, die unterschiedlichen An- und Ausschaltzeiten der IGBTs auszubalancieren,
-- sodass dies keine Fehler fuer den Ausgang bedeutet.
-- Als Parameter dafür existiert deltaIGBTOffOn, welcher grob mit folgender Formel aus den IGBT Parametern berechnet werden kann:

--  deltaIGBTOffOn=(tdOff-tdOn)+(tf-tr)/2
--  tdOff: Ausschaltdelay
--  tdOn : Einschaltdelay
--  tf   : Abfalldauer   (wird deswegen nur halb eingerechnet, da hier der Strom linear abnimmt)
--  tr   : Anstiegsdauer (wird deswegen nur halb eingerechnet, da hier der Strom linear zunimmt)

--  Alle Zeiten sind dabei in Takten bezogen auf 100MHz anzugeben -> 10 us = 1000 Takte
--  Feineinstellungen sind trotz Berechnung der Parameter notwendig

LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY IGBT_Totzeitsymmetrierung_SingleIGBT IS
   GENERIC(           
      takt                : integer  := 100_000_000  ); 
    
   PORT(
      clk, reset,enable   : in std_logic ;             --System-Eingaenge:
      
      IGBT_SchalterIN     : in std_logic;               --Eingaenge
      deltaIGBTOffOn      : in unsigned (15 downto 0);
      tOffgreatertOn      : in std_logic;
        
      IGBT_SchalterOUT    : out std_logic  );           --Ausgaenge (nur positive Werte)
END IGBT_Totzeitsymmetrierung_SingleIGBT ;


ARCHITECTURE behavioral OF IGBT_Totzeitsymmetrierung_SingleIGBT IS

SIGNAL counter_delay:UNSIGNED(15 DOWNTO 0):=(others=>'0');


begin 

Einschaltdelay: process(reset, clk, enable, tOffgreatertOn) 
BEGIN
   if reset = '1' then
      IGBT_SchalterOUT    <= '0';
      
      if(tOffgreatertOn= '0') then
         counter_delay<=(others=>'0');
      else
         counter_delay<=to_unsigned(65535,16); -- Darf nicht kleiner sein als deltaIGBTOffOn, sonst permanent 1
      end if;
   
   elsif enable = '0' then
      IGBT_SchalterOUT    <= '0';
     
   elsif rising_edge(clk)  then

      if(tOffgreatertOn= '0') then
         if (counter_delay < deltaIGBTOffOn and IGBT_SchalterIN = '1') then
            counter_delay       <= counter_delay + 1;
            IGBT_SchalterOUT    <= '0';
         
         elsif IGBT_SchalterIN = '1'   then
            IGBT_SchalterOUT    <= '1';
         
         else
            counter_delay       <= (others => '0');
            IGBT_SchalterOUT    <= '0';
         end if;      
      
      else       
         if (counter_delay < deltaIGBTOffOn and IGBT_SchalterIN = '0') then
            counter_delay       <= counter_delay + 1;
            IGBT_SchalterOUT    <= '1';
         
         elsif IGBT_SchalterIN = '1'   then
            counter_delay       <= (others => '0');
            IGBT_SchalterOUT    <= '1';
         
         else 
            IGBT_SchalterOUT    <= '0';
         end if;
      end if;
   end if;
END process Einschaltdelay;

END architecture behavioral;