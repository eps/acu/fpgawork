--  Filename: PI_REG_Mult_dI_20Bit.vhd
--  A.Wiest 05.10.2022
--
--  Beschreibung:
--      Dieser Regler hat die P- und I-Anteile sowie Anti-Wind-UP (24Bit)
--
--  Eing. Groessen:
--      clk                bspw. 100MHz system clock
--      reset              Berechnung neue starten, alles auf Null setzten
--      Eingang            Eingang der (soll - ist) Abweichung vom Typ signed (24Bit)
--
--      Data_In_Valid      Steigende Flanke  Triggert einmalig  die Berechnung an
--
--  Ausg. Groessen:
--      Ausgang            Reglerausgang: 20bit signed
--      Data_Out_Valid_PI  wenn die Daten berechnet sind = '1'

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


--Entity Declaration======================================================
ENTITY PI_REG_Mult_dI_24Bit IS
   GENERIC(
      Max_Eingang_diff   : integer := 32767 ;  -- Abs Wert, max. zulaessige Differenz des Einganges (wenn groesser, so wird der Eingang auf diesen Wert begrenzt)
      Max_Ausgang        : integer := 131060); -- Abs Wert, max Ausgang, der bei der Berechnung ausgegeben wird (wenn groesser, so wird der Ausgang auf diesen Wert begrenzt)

   PORT(
      clk, reset, Enable : in std_logic  ;                                 -- Enable ='1' -> ein Durchlauf der Berechnung

      EingangSoll          : in  signed   (23 downto 0);                   -- Diff =  EingangSoll - EingangIst [-1..1]
      EingangIst           : in  signed   (23 downto 0);

      Kp                   : in  signed   (15 downto 0);                   -- 32767 entspricht x1
      Kp_Verschiebung      : in  signed   (3  downto 0);                   -- Kp_Verschiebung=1 -> Kp*2, Kp_Verschiebung=2 -> Kp*4, etc.; Kp_Verschiebung= -1 -> Kp/2, Kp_Verschiebung= -2 -> Kp/4, etc.

      Ki                   : in  signed   (15 downto 0);                   -- 32767 entspricht x1
      Ki_Verschiebung      : in  signed   (3  downto 0);                   -- Ki_Verschiebung=1 -> Ki*2, Ki_Verschiebung=2 -> Ki*4, etc.; Ki_Verschiebung= -1 -> Ki/2, Ki_Verschiebung= -2 -> Ki/4, etc.

      Eingang_Verschiebung : in  signed   (2  downto 0);                   -- Verschiebung des Ist und Sollwertes nach rechts 0->keine Verschiebung, 1-> um eine Stelle, etc.
                                                                           -- Dadurch koennte das Rauschen kleiner werden
      Ausgang              : out signed   (23 downto 0) := (others => '0');
      Ausgang_abs          : out signed   (23 downto 0) := (others => '0');
      
      P_Anteil             : out signed   (23 downto 0) := (others => '0');
      I_Anteil             : out signed   (23 downto 0) := (others => '0');
      Regelfehler          : out signed   (23 downto 0) := (others => '0'); --EingangSoll - EingangIst [-1..1]
      Data_Out_Valid_PI    : out std_logic              := '0'           );

END PI_REG_Mult_dI_24Bit;

--===========================================================================
ARCHITECTURE behavioral OF PI_REG_Mult_dI_24Bit IS

-- Speicherung des Integralergebnises
SIGNAL s_Integral_aktuell : signed (40 downto 0):=(others=>'0');     -- 41 bit  (40Bit + 40Bit)
SIGNAL s_Integral_vorhin  : signed (40 downto 0):=(others=>'0');     -- 41 bit


-- Multiplikation:
-- e(t)* Ki  nicht korrigierte I-Anteil bei der Multiplikation
-- e(t)* Kp Ergebnise der Multiplikation

SIGNAL s_EingangSoll     : signed(23 downto 0) := (others => '0'); -- 24 bit
SIGNAL s_EingangIst      : signed(23 downto 0) := (others => '0'); -- 24 bit
SIGNAL s_EingangDiff     : signed(23 downto 0) := (others => '0'); -- 24 bit
SIGNAL s_EingangDiff_Kp  : signed(23 downto 0) := (others => '0'); -- 24 bit
SIGNAL s_EingangDiff_Ki  : signed(23 downto 0) := (others => '0'); -- 24 bit


SIGNAL s_Mult_INPUT1     : signed(23 downto 0) := (others => '0'); -- 24 bit
SIGNAL s_Mult_INPUT2     : signed(15 downto 0) := (others => '0'); -- 16 bit
SIGNAL s_Mult40          : signed(39 downto 0) := (others => '0'); -- 40 bit (24+16)

SIGNAL s_PAnteil         : signed(23 downto 0) := (others => '0'); -- 24 bit
SIGNAL s_IAnteil         : signed(39 downto 0) := (others => '0'); -- 40 bit
SIGNAL s_Summe           : signed(23 downto 0) := (others => '0'); -- Summe beider Anteile

TYPE STATE_PID_CONTROLLER  IS(EINGANG_SPEICHERN, 
                              DIFF_BILDEN, DIFF_VERSCHIEBEN, 
                              MULT1_VORBEREITEN, MULTIPLIKATION1, TERM1_SAVE,
                              TERM2_MULT, TERM2_SAVE,
                              INTEGRAL_BILDEN,
                              ADDITION,
                              DATENRAUSGEBEN);

SIGNAL s_STATE : STATE_PID_CONTROLLER := EINGANG_SPEICHERN;


BEGIN
hauptprozess: process (clk, reset)                    
   BEGIN
      if reset = '1' then
         s_EingangDiff_Kp   <=(others=>'0');
         s_EingangDiff_Ki   <=(others=>'0');
         s_EingangDiff      <=(others=>'0');
         
         s_Mult_INPUT1      <=(others=>'0');
         s_Mult_INPUT2      <=(others=>'0');
         s_Mult40           <=(others=>'0');

         s_PAnteil          <=(others=>'0');          -- Multiplikationsergebnise auf Null setzen
         s_IAnteil          <=(others=>'0');
         s_Summe            <=(others=>'0');          -- Summe des Mult. ergebnises
         s_Integral_aktuell <=(others=>'0');          -- Integralergebnise auf Null setzen
         s_Integral_vorhin  <=(others=>'0');

         Ausgang            <=(others=>'0');
         Ausgang_abs        <=(others=>'0');
         Data_Out_Valid_PI  <= '0'         ;         -- Berechnung ist nicht abgeschlossen
         s_STATE            <= EINGANG_SPEICHERN;    -- naechster Schritt

      elsif (clk'event and clk='1') THEN
         CASE  s_STATE  IS

   ----------------------------------------------------------------------------
   when EINGANG_SPEICHERN => --1. Eingaenge Begrenzen
   ----------------------------------------------------------------------------
      Data_Out_Valid_PI   <= '0';
      
      IF (Enable = '1') then
         Case Eingang_Verschiebung IS                                 -- Dadurch koennte es etwas ruhiger werden, da die  Stellen nicht so sehr wackeln
            when "001"  => 
               s_EingangSoll <= resize(EingangSoll(23 downto 1),24) ; -- letzte Stelle abgeschnitten
               s_EingangIst  <= resize(EingangIst (23 downto 1),24) ; -- letzte Stelle abgeschnitten
               
            when "010"  => 
               s_EingangSoll <= resize(EingangSoll(23 downto 2),24) ; -- zwei letzten Stellen abgeschnitten
               s_EingangIst  <= resize(EingangIst (23 downto 2),24) ; -- zwei letzten Stellen abgeschnitten
            
            when "011"  => 
               s_EingangSoll <= resize(EingangSoll(23 downto 3),24) ; -- drei letzten Stellen abgeschnitten
               s_EingangIst  <= resize(EingangIst (23 downto 3),24) ; -- drei letzten Stellen abgeschnitten
               
            when "100"  => 
               s_EingangSoll <= resize(EingangSoll(23 downto 4),24) ; -- vier letzten Stellen abgeschnitten
               s_EingangIst  <= resize(EingangIst (23 downto 4),24) ; -- vier letzten Stellen abgeschnitten
               
            when others => 
               s_EingangSoll <= EingangSoll ;
               s_EingangIst  <= EingangIst  ;
         end case;  
         s_STATE             <= DIFF_BILDEN ;
      END IF;

   ----------------------------------------------------------------------------
   when DIFF_BILDEN => --2. Eingangsdifferenz begrenzen
   ----------------------------------------------------------------------------
      IF    (s_EingangSoll - s_EingangIst) >  Max_Eingang_diff -1 THEN s_EingangDiff <= to_signed ( Max_Eingang_diff, 24);
      ELSIF (s_EingangSoll - s_EingangIst) < -Max_Eingang_diff +1 THEN s_EingangDiff <= to_signed (-Max_Eingang_diff, 24);
      ELSE                                                             s_EingangDiff <= s_EingangSoll - s_EingangIst;
      END IF;
      s_STATE       <= DIFF_VERSCHIEBEN;

----------------------------------------------------------------------------
   when DIFF_VERSCHIEBEN => --3. Eingangsdifferenz *2, *4 etc, oder /2, /4 etc.
   ----------------------------------------------------------------------------
      Case Kp_Verschiebung IS 
         when "0001" => s_EingangDiff_Kp <= s_EingangDiff(22 downto 0) & "0"      ; -- x2
         when "0010" => s_EingangDiff_Kp <= s_EingangDiff(21 downto 0) & "00"     ; -- x4
         when "0011" => s_EingangDiff_Kp <= s_EingangDiff(20 downto 0) & "000"    ; -- x8
         when "0100" => s_EingangDiff_Kp <= s_EingangDiff(19 downto 0) & "0000"   ; -- x16
         when "0101" => s_EingangDiff_Kp <= s_EingangDiff(18 downto 0) & "00000"  ; -- x32
         when "0110" => s_EingangDiff_Kp <= s_EingangDiff(17 downto 0) & "000000" ; -- x64
         when "0111" => s_EingangDiff_Kp <= s_EingangDiff(16 downto 0) & "0000000"; -- x128
         
         when "1111" => s_EingangDiff_Kp <= resize(s_EingangDiff(23 downto 1),24) ; -- /2
         when "1110" => s_EingangDiff_Kp <= resize(s_EingangDiff(23 downto 2),24) ; -- /4
         when "1101" => s_EingangDiff_Kp <= resize(s_EingangDiff(23 downto 3),24) ; -- /8
         when "1100" => s_EingangDiff_Kp <= resize(s_EingangDiff(23 downto 4),24) ; -- /16
         when "1011" => s_EingangDiff_Kp <= resize(s_EingangDiff(23 downto 5),24) ; -- /32
         when "1010" => s_EingangDiff_Kp <= resize(s_EingangDiff(23 downto 6),24) ; -- /64
         when "1001" => s_EingangDiff_Kp <= resize(s_EingangDiff(23 downto 7),24) ; -- /128

         when others => s_EingangDiff_Kp <= s_EingangDiff                ;
      end case;  

      Case Ki_Verschiebung IS 
         when "0001" => s_EingangDiff_Ki <= s_EingangDiff(22 downto 0) & "0"      ; -- x2
         when "0010" => s_EingangDiff_Ki <= s_EingangDiff(21 downto 0) & "00"     ; -- x4
         when "0011" => s_EingangDiff_Ki <= s_EingangDiff(20 downto 0) & "000"    ; -- x8
         when "0100" => s_EingangDiff_Ki <= s_EingangDiff(19 downto 0) & "0000"   ; -- x16
         when "0101" => s_EingangDiff_Ki <= s_EingangDiff(18 downto 0) & "00000"  ; -- x32
         when "0110" => s_EingangDiff_Ki <= s_EingangDiff(17 downto 0) & "000000" ; -- x64
         when "0111" => s_EingangDiff_Ki <= s_EingangDiff(16 downto 0) & "0000000"; -- x128
         
         when "1111" => s_EingangDiff_Ki <= resize(s_EingangDiff(23 downto 1),24) ; -- /2  -1
         when "1110" => s_EingangDiff_Ki <= resize(s_EingangDiff(22 downto 2),24) ; -- /4  -2
         when "1101" => s_EingangDiff_Ki <= resize(s_EingangDiff(21 downto 3),24) ; -- /8  -3
         when "1100" => s_EingangDiff_Ki <= resize(s_EingangDiff(20 downto 4),24) ; -- /16 -4
         when "1011" => s_EingangDiff_Ki <= resize(s_EingangDiff(19 downto 5),24) ; -- /32
         when "1010" => s_EingangDiff_Ki <= resize(s_EingangDiff(18 downto 6),24) ; -- /64
         when "1001" => s_EingangDiff_Ki <= resize(s_EingangDiff(17 downto 7),24) ; -- /128

         when others => s_EingangDiff_Ki <= s_EingangDiff                         ;
      end case;  

      s_STATE       <= MULT1_VORBEREITEN;

   -----------------------------------------------------------------------------
    when MULT1_VORBEREITEN =>  --4. Takt (Multiplikation P-Anteil vorbereiten)
   -----------------------------------------------------------------------------
      s_Mult_INPUT1 <= s_EingangDiff_Kp;
      s_Mult_INPUT2 <= Kp;
      s_STATE       <= MULTIPLIKATION1;

   -----------------------------------------------------------------------------
    when MULTIPLIKATION1 =>  --5. Takt (Multiplikation P-Anteil)
   -----------------------------------------------------------------------------
      s_Mult40     <= s_Mult_INPUT1 * s_Mult_INPUT2;  --24bit * 16bit =40bit
      s_STATE      <= TERM1_SAVE;

   ---------------------------------------------------------------------------------
   when TERM1_SAVE =>   -- 6. Multiplikation vorbereiten
   ---------------------------------------------------------------------------------   
      s_PAnteil      <= s_Mult40(38 downto 15);
      s_Mult_INPUT1  <= s_EingangDiff_Ki;
      s_Mult_INPUT2  <= Ki;
      s_STATE        <= TERM2_MULT;

   -----------------------------------------------------------------------------
   when TERM2_MULT =>  --7. Takt Ki Multiplikation
   -----------------------------------------------------------------------------
      s_Mult40  <= s_Mult_INPUT1 * s_Mult_INPUT2;  --24bit * 16bit =40bit
      s_STATE   <= TERM2_SAVE;

   ---------------------------------------------------------------------------------
   when TERM2_SAVE =>   --8. Ergebniss fuer Multiplikation speichern
   ---------------------------------------------------------------------------------
      s_IAnteil <= s_Mult40;
      s_STATE   <= INTEGRAL_BILDEN;

   --------------------------------------------------------------------------------
   when INTEGRAL_BILDEN =>   --9. Integral bilden
   -----------------------------------------------------------------------------
      s_Integral_aktuell <= s_IAnteil + s_Integral_vorhin; --40Bit +40Bit = 41Bit
      s_STATE            <= ADDITION                     ;
   

   -----------------------------------------------------------------------------
   when ADDITION =>        --10. Takt
   -----------------------------------------------------------------------------
      if    s_PAnteil + s_Integral_aktuell(39 downto 16) >   Max_Ausgang  then s_Summe <= to_signed(Max_Ausgang,24)  ;
         if s_IAnteil < 0 then  s_integral_vorhin <= s_Integral_aktuell;  end if; -- sonst bleibt integral vorhin unveraendert

      elsif s_PAnteil + s_Integral_aktuell(39 downto 16) < - Max_Ausgang  then s_Summe <= to_signed(-Max_Ausgang,24) ;
         if s_IAnteil > 0 then  s_integral_vorhin <= s_Integral_aktuell;  end if; -- sonst bleibt integral vorhin unveraendert

      else
         s_Summe            <= s_PAnteil + s_Integral_aktuell(39 downto 16)     ;
         s_Integral_vorhin  <= s_Integral_aktuell;                                -- Neuer Integrallwert wird gespeichert

      end if;
      s_STATE <= DATENRAUSGEBEN;

   -----------------------------------------------------------------------------
   when DATENRAUSGEBEN=>     --11.Takt
   -----------------------------------------------------------------------------
      Ausgang            <=     s_Summe                      ;  -- 24bit
      Ausgang_abs        <= ABS(s_Summe)                     ;
      P_Anteil           <= s_PAnteil                        ;
      I_Anteil           <= s_Integral_aktuell(39 downto 16) ;
      Regelfehler        <= s_EingangDiff                    ;
      s_STATE            <= EINGANG_SPEICHERN                ;
      Data_Out_Valid_PI  <= '1'                              ; -- Daten sind jetzt ausgerechnet
      
   -----------------------------------------------------------------------------
   when others =>     --11.Takt
   -----------------------------------------------------------------------------
      s_STATE            <= EINGANG_SPEICHERN;

   END CASE;

   END IF;  --reset

   END process hauptprozess;

END ARCHITECTURE behavioral;
