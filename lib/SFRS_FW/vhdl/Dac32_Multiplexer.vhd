--  Filename: Dac32_Multiplexer.vhd
--  Date    : 13.09.2022 born
--  Author  : Wiest
--  1744 Logic

-- Description:
-- This Bloack is used to select the suitable input for the DAC as a input. Its 32 * 4 multiplexer.
-- Mostly used for the Altera Quartus II implimentation purpose.
--============================================================================
 
LIBRARY ieee;                                          --Library Declaration
USE  ieee.std_logic_1164.all; 
USE  ieee.numeric_std.all;

------------------------------------------------------------------------------


entity Dac32_Multiplexer is

   port ( 
		clk, reset : in std_logic;
		COMMAND    : in std_logic_vector (4  downto 0);
		
		MUX0_IN    : in  std_logic_vector(63 downto 0);   
		MUX1_IN    : in  std_logic_vector(63 downto 0);   
		MUX2_IN    : in  std_logic_vector(63 downto 0);   
		MUX3_IN    : in  std_logic_vector(63 downto 0); 

      MUX4_IN    : in  std_logic_vector(63 downto 0);   
		MUX5_IN    : in  std_logic_vector(63 downto 0);   
		MUX6_IN    : in  std_logic_vector(63 downto 0);   
		MUX7_IN    : in  std_logic_vector(63 downto 0); 

      MUX8_IN    : in  std_logic_vector(63 downto 0);   
		MUX9_IN    : in  std_logic_vector(63 downto 0);   
		MUX10_IN   : in  std_logic_vector(63 downto 0);   
		MUX11_IN   : in  std_logic_vector(63 downto 0); 

      MUX12_IN   : in  std_logic_vector(63 downto 0);   
		MUX13_IN   : in  std_logic_vector(63 downto 0);   
		MUX14_IN   : in  std_logic_vector(63 downto 0);   
		MUX15_IN   : in  std_logic_vector(63 downto 0);   
	   
      MUX16_IN   : in  std_logic_vector(63 downto 0);   
		MUX17_IN   : in  std_logic_vector(63 downto 0);   
		MUX18_IN   : in  std_logic_vector(63 downto 0);   
		MUX19_IN   : in  std_logic_vector(63 downto 0); 

      MUX20_IN   : in  std_logic_vector(63 downto 0);   
		MUX21_IN   : in  std_logic_vector(63 downto 0);   
		MUX22_IN   : in  std_logic_vector(63 downto 0);   
		MUX23_IN   : in  std_logic_vector(63 downto 0); 

      MUX24_IN   : in  std_logic_vector(63 downto 0);   
		MUX25_IN   : in  std_logic_vector(63 downto 0);   
		MUX26_IN   : in  std_logic_vector(63 downto 0);   
		MUX27_IN   : in  std_logic_vector(63 downto 0); 

      MUX28_IN   : in  std_logic_vector(63 downto 0);   
		MUX29_IN   : in  std_logic_vector(63 downto 0);   
		MUX30_IN   : in  std_logic_vector(63 downto 0);   
		MUX31_IN   : in  std_logic_vector(63 downto 0); 
   -------------------------------------------------------
		MUX0_OUT   : out std_logic_vector(15 downto 0);
		MUX1_OUT   : out std_logic_vector(15 downto 0);
		MUX2_OUT   : out std_logic_vector(15 downto 0);
		MUX3_OUT   : out std_logic_vector(15 downto 0));
	
end Dac32_Multiplexer;

architecture RTL of Dac32_Multiplexer is
BEGIN

process_DAC32_MUX :  process (clk, reset)
BEGIN

if (reset ='1') then
	MUX0_OUT  <= (others=>'0'); 
	MUX1_OUT  <= (others=>'0');
	MUX2_OUT  <= (others=>'0');   
	MUX3_OUT  <= (others=>'0');
	
elsif (rising_edge(clk) and clk = '1') then
	case COMMAND(4 downto 0) is
	  when "00000" => MUX0_OUT	<= MUX0_IN (63 downto 48);      --Input 0 selected 
                     MUX1_OUT	<= MUX0_IN (47 downto 32);     
                     MUX2_OUT	<= MUX0_IN (31 downto 16);      
                     MUX3_OUT	<= MUX0_IN (15 downto 0 );      
      
     when "00001" => MUX0_OUT	<= MUX1_IN (63 downto 48);      --Input 1 selected 
                     MUX1_OUT	<= MUX1_IN (47 downto 32);      
                     MUX2_OUT	<= MUX1_IN (31 downto 16);       
                     MUX3_OUT	<= MUX1_IN (15 downto 0 );       
      
     when "00010" => MUX0_OUT	<= MUX2_IN (63 downto 48);      --Input 2 selected 
                     MUX1_OUT	<= MUX2_IN (47 downto 32);      
                     MUX2_OUT	<= MUX2_IN (31 downto 16);      
                     MUX3_OUT	<= MUX2_IN (15 downto 0 );      
      
     when "00011" => MUX0_OUT	<= MUX3_IN (63 downto 48);      --Input 3 selected 
                     MUX1_OUT	<= MUX3_IN (47 downto 32);      
                     MUX2_OUT	<= MUX3_IN (31 downto 16);      
                     MUX3_OUT	<= MUX3_IN (15 downto 0 );

     when "00100" => MUX0_OUT	<= MUX4_IN (63 downto 48);      --Input 4 selected 
                     MUX1_OUT	<= MUX4_IN (47 downto 32);     
                     MUX2_OUT	<= MUX4_IN (31 downto 16);      
                     MUX3_OUT	<= MUX4_IN (15 downto 0 );      
      
     when "00101" => MUX0_OUT	<= MUX5_IN (63 downto 48);      --Input 5 selected 
                     MUX1_OUT	<= MUX5_IN (47 downto 32);      
                     MUX2_OUT	<= MUX5_IN (31 downto 16);       
                     MUX3_OUT	<= MUX5_IN (15 downto 0 );       
      
     when "00110" => MUX0_OUT	<= MUX6_IN (63 downto 48);      --Input 6 selected 
                     MUX1_OUT	<= MUX6_IN (47 downto 32);      
                     MUX2_OUT	<= MUX6_IN (31 downto 16);      
                     MUX3_OUT	<= MUX6_IN (15 downto 0 );      
      
     when "00111" => MUX0_OUT	<= MUX7_IN (63 downto 48);      --Input 7 selected 
                     MUX1_OUT	<= MUX7_IN (47 downto 32);      
                     MUX2_OUT	<= MUX7_IN (31 downto 16);      
                     MUX3_OUT	<= MUX7_IN (15 downto 0 );

     when "01000" => MUX0_OUT	<= MUX8_IN (63 downto 48);      --Input 8 selected 
                     MUX1_OUT	<= MUX8_IN (47 downto 32);     
                     MUX2_OUT	<= MUX8_IN (31 downto 16);      
                     MUX3_OUT	<= MUX8_IN (15 downto 0 );      
      
     when "01001" => MUX0_OUT	<= MUX9_IN (63 downto 48);      --Input 9 selected 
                     MUX1_OUT	<= MUX9_IN (47 downto 32);      
                     MUX2_OUT	<= MUX9_IN (31 downto 16);       
                     MUX3_OUT	<= MUX9_IN (15 downto 0 );       
      
     when "01010" => MUX0_OUT	<= MUX10_IN(63 downto 48);      --Input 10 selected 
                     MUX1_OUT	<= MUX10_IN(47 downto 32);      
                     MUX2_OUT	<= MUX10_IN(31 downto 16);      
                     MUX3_OUT	<= MUX10_IN(15 downto 0 );      
      
     when "01011" => MUX0_OUT	<= MUX11_IN(63 downto 48);      --Input 11 selected 
                     MUX1_OUT	<= MUX11_IN(47 downto 32);      
                     MUX2_OUT	<= MUX11_IN(31 downto 16);      
                     MUX3_OUT	<= MUX11_IN(15 downto 0 );

     when "01100" => MUX0_OUT	<= MUX12_IN(63 downto 48);      --Input 12 selected 
                     MUX1_OUT	<= MUX12_IN(47 downto 32);     
                     MUX2_OUT	<= MUX12_IN(31 downto 16);      
                     MUX3_OUT	<= MUX12_IN(15 downto 0 );      
      
     when "01101" => MUX0_OUT	<= MUX13_IN(63 downto 48);      --Input 13 selected 
                     MUX1_OUT	<= MUX13_IN(47 downto 32);      
                     MUX2_OUT	<= MUX13_IN(31 downto 16);       
                     MUX3_OUT	<= MUX13_IN(15 downto 0 );       
      
     when "01110" => MUX0_OUT	<= MUX14_IN(63 downto 48);      --Input 14 selected 
                     MUX1_OUT	<= MUX14_IN(47 downto 32);      
                     MUX2_OUT	<= MUX14_IN(31 downto 16);      
                     MUX3_OUT	<= MUX14_IN(15 downto 0 );      
      
     when "01111" => MUX0_OUT	<= MUX15_IN(63 downto 48);      --Input 15 selected 
                     MUX1_OUT	<= MUX15_IN(47 downto 32);      
                     MUX2_OUT	<= MUX15_IN(31 downto 16);      
                     MUX3_OUT	<= MUX15_IN(15 downto 0 );
	
    -----------------------------

     when "10000" => MUX0_OUT	<= MUX16_IN (63 downto 48);      --Input 16 selected 
                     MUX1_OUT	<= MUX16_IN (47 downto 32);     
                     MUX2_OUT	<= MUX16_IN (31 downto 16);      
                     MUX3_OUT	<= MUX16_IN (15 downto 0 );      
      
     when "10001" => MUX0_OUT	<= MUX17_IN (63 downto 48);      --Input 17 selected 
                     MUX1_OUT	<= MUX17_IN (47 downto 32);      
                     MUX2_OUT	<= MUX17_IN (31 downto 16);       
                     MUX3_OUT	<= MUX17_IN (15 downto 0 );       
      
     when "10010" => MUX0_OUT	<= MUX18_IN (63 downto 48);      --Input 18 selected 
                     MUX1_OUT	<= MUX18_IN (47 downto 32);      
                     MUX2_OUT	<= MUX18_IN (31 downto 16);      
                     MUX3_OUT	<= MUX18_IN (15 downto 0 );      
      
     when "10011" => MUX0_OUT	<= MUX19_IN (63 downto 48);      --Input 19 selected 
                     MUX1_OUT	<= MUX19_IN (47 downto 32);      
                     MUX2_OUT	<= MUX19_IN (31 downto 16);      
                     MUX3_OUT	<= MUX19_IN (15 downto 0 );

     when "10100" => MUX0_OUT	<= MUX20_IN (63 downto 48);      --Input 20 selected 
                     MUX1_OUT	<= MUX20_IN (47 downto 32);     
                     MUX2_OUT	<= MUX20_IN (31 downto 16);      
                     MUX3_OUT	<= MUX20_IN (15 downto 0 );      
      
     when "10101" => MUX0_OUT	<= MUX21_IN (63 downto 48);      --Input 21 selected 
                     MUX1_OUT	<= MUX21_IN (47 downto 32);      
                     MUX2_OUT	<= MUX21_IN (31 downto 16);       
                     MUX3_OUT	<= MUX21_IN (15 downto 0 );       
      
     when "10110" => MUX0_OUT	<= MUX22_IN (63 downto 48);      --Input 22 selected 
                     MUX1_OUT	<= MUX22_IN (47 downto 32);      
                     MUX2_OUT	<= MUX22_IN (31 downto 16);      
                     MUX3_OUT	<= MUX22_IN (15 downto 0 );      
      
     when "10111" => MUX0_OUT	<= MUX23_IN (63 downto 48);      --Input 23 selected 
                     MUX1_OUT	<= MUX23_IN (47 downto 32);      
                     MUX2_OUT	<= MUX23_IN (31 downto 16);      
                     MUX3_OUT	<= MUX23_IN (15 downto 0 );

     when "11000" => MUX0_OUT	<= MUX24_IN (63 downto 48);      --Input 24 selected 
                     MUX1_OUT	<= MUX24_IN (47 downto 32);     
                     MUX2_OUT	<= MUX24_IN (31 downto 16);      
                     MUX3_OUT	<= MUX24_IN (15 downto 0 );      
      
     when "11001" => MUX0_OUT	<= MUX25_IN (63 downto 48);      --Input 25 selected 
                     MUX1_OUT	<= MUX25_IN (47 downto 32);      
                     MUX2_OUT	<= MUX25_IN (31 downto 16);       
                     MUX3_OUT	<= MUX25_IN (15 downto 0 );       
      
     when "11010" => MUX0_OUT	<= MUX26_IN(63 downto 48);      --Input 26 selected 
                     MUX1_OUT	<= MUX26_IN(47 downto 32);      
                     MUX2_OUT	<= MUX26_IN(31 downto 16);      
                     MUX3_OUT	<= MUX26_IN(15 downto 0 );      
      
     when "11011" => MUX0_OUT	<= MUX27_IN(63 downto 48);      --Input 27 selected 
                     MUX1_OUT	<= MUX27_IN(47 downto 32);      
                     MUX2_OUT	<= MUX27_IN(31 downto 16);      
                     MUX3_OUT	<= MUX27_IN(15 downto 0 );

     when "11100" => MUX0_OUT	<= MUX28_IN(63 downto 48);      --Input 28 selected 
                     MUX1_OUT	<= MUX28_IN(47 downto 32);     
                     MUX2_OUT	<= MUX28_IN(31 downto 16);      
                     MUX3_OUT	<= MUX28_IN(15 downto 0 );      
      
     when "11101" => MUX0_OUT	<= MUX29_IN(63 downto 48);      --Input 29 selected 
                     MUX1_OUT	<= MUX29_IN(47 downto 32);      
                     MUX2_OUT	<= MUX29_IN(31 downto 16);       
                     MUX3_OUT	<= MUX29_IN(15 downto 0 );       
      
     when "11110" => MUX0_OUT	<= MUX30_IN(63 downto 48);      --Input 30 selected 
                     MUX1_OUT	<= MUX30_IN(47 downto 32);      
                     MUX2_OUT	<= MUX30_IN(31 downto 16);      
                     MUX3_OUT	<= MUX30_IN(15 downto 0 );      
      
     when "11111" => MUX0_OUT	<= MUX31_IN(63 downto 48);      --Input 31 selected 
                     MUX1_OUT	<= MUX31_IN(47 downto 32);      
                     MUX2_OUT	<= MUX31_IN(31 downto 16);      
                     MUX3_OUT	<= MUX31_IN(15 downto 0 );
	
end case;

	
end if;
end process  process_DAC32_MUX ;
	
end RTL;







