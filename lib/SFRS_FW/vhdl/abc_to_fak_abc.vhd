--  Filename: abc_to_fak_abc.vhd
--  Anzahl der Takte
--  Beschrebung:
--      Multiplikation der Groessen a,b,c mit einem Faktor abc -> Faktor * (a, b, c)

--  Eing. Groessen:
--      a              a Komponente 16bit signed
--      b              b Komponente 16bit signed
--      c              c Komponente 16bit signed
--
--      clk            clock
--      Enable         symbolisiert Gültigkeit der Eingangsdaten
--      reset          Berechnung neue starten, alle Varablen auf Null setzen
--
--  Ausg. Groessen:
--      a_fak          a*Faktor, 16bit signed
--      b_fak          b*Faktor, 16bit signed
--      c_fak          c*Faktor, 16bit signed
--      Data_Out_Valid_abc  '1' wenn die Berechnung fertig ist
-- Faktor ist der Multiplikationsfaktor mit dem Ausgang 32767 =1

--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity abc_to_fak_abc is

   port(
      clk, reset, Enable      : in std_logic;               -- System-Eingaenge:

      a_in                    : in signed (15 downto 0);    -- Eingang a
      b_in                    : in signed (15 downto 0);    -- Eingang b
      c_in                    : in signed (15 downto 0);    -- Eingang c
      Faktor                  : in signed (15 downto 0);    -- Faktor fuer die Multiplikation 1=32767

      --Ausgaenge
      a_out                   : out signed(15 downto 0);
      b_out                   : out signed(15 downto 0);
      c_out                   : out signed(15 downto 0);
      Data_Out_Valid_fak_abc  : out std_logic );

   end abc_to_fak_abc;

--============================================================================
architecture behavioral of abc_to_fak_abc is
   attribute multstyle               : string;
   attribute multstyle of behavioral : architecture is "dsp";    -- Nutzt bevorzugt embedded multiplier

   signal s_a_in          : signed (15 downto 0) := (others => '0');
   signal s_b_in          : signed (15 downto 0) := (others => '0');
   signal s_c_in          : signed (15 downto 0) := (others => '0');
   signal s_Faktor        : signed (15 downto 0) := (others => '0');

   signal s_Mult_in1      : signed (15 downto 0) := (others => '0');
   signal s_Mult_in2      : signed (15 downto 0) := (others => '0');
   signal s_Mult32        : signed (31 downto 0) := (others => '0');
   signal sa_Mult         : signed (15 downto 0) := (others => '0');
   signal sb_Mult         : signed (15 downto 0) := (others => '0');
   signal sc_Mult         : signed (15 downto 0) := (others => '0');

   type STATE_ABC_TO_FAK_ABC is(
                           EINGANG_SPEICHERN,
                           MULTIPLIKATION1_VORBEREITEN, MULTIPLIKATION1,  MULTIPLIKATION1_SPECHERN,
                           MULTIPLIKATION2_VORBEREITEN, MULTIPLIKATION2,  MULTIPLIKATION2_SPECHERN,
                           MULTIPLIKATION3_VORBEREITEN, MULTIPLIKATION3,  MULTIPLIKATION3_SPECHERN,
                           DATEN_AUSGEBEN);
  signal s_STATE : STATE_ABC_TO_FAK_ABC := EINGANG_SPEICHERN;

begin

  hauptprozess : process (clk, reset)
  begin
   if (reset = '1') then
      s_a_in        <= (others => '0');  
      s_b_in        <= (others => '0');
      s_c_in        <= (others => '0');
      s_Faktor      <= (others => '0');

      s_Mult_in1    <= (others => '0');
      s_Mult_in2    <= (others => '0');
      s_Mult32      <= (others => '0');
      sa_Mult       <= (others => '0');
      sb_Mult       <= (others => '0');
      sc_Mult       <= (others => '0');
      
      s_STATE                  <= EINGANG_SPEICHERN;
      Data_Out_Valid_fak_abc   <= '0';

    elsif rising_edge(clk) then
      case s_STATE is

   ----------------------------------------------------------------------------------
   when EINGANG_SPEICHERN =>                --1.Takt
   ----------------------------------------------------------------------------------
      if (Enable = '1') then
         s_a_in   <= a_in;
         s_b_in   <= b_in;
         s_c_in   <= c_in;
         s_Faktor <= Faktor;

         s_STATE  <= MULTIPLIKATION1_VORBEREITEN;
      else
         NULL;
         
      end if;

      Data_Out_Valid_fak_abc <= '0';

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION1_VORBEREITEN =>    --2.Takt
   ----------------------------------------------------------------------------------
      s_Mult_in1    <= s_Faktor;  -- Faktor
      s_Mult_in2    <= s_a_in;    --a
      s_STATE       <= MULTIPLIKATION1;
   
   ----------------------------------------------------------------------------------
   when MULTIPLIKATION1=>                 --3.Takt
   ----------------------------------------------------------------------------------
      s_Mult32     <= s_Mult_in1 * s_Mult_in2 ;  
      s_STATE      <= MULTIPLIKATION1_SPECHERN;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION1_SPECHERN =>       --4.Takt
   ----------------------------------------------------------------------------------
      if s_Mult32 (31 downto 15) > to_signed(32766,16)     then
         sa_Mult <= to_signed(32767,16);
      
      elsif s_Mult32 (31 downto 15) < to_signed(-32766,16) then
         sa_Mult <= to_signed(-32767,16);
      
      else
         sa_Mult       <= s_Mult32 (30 downto 15);
      end if;

      s_STATE       <= MULTIPLIKATION2_VORBEREITEN;
   
   ----------------------------------------------------------------------------------
   when MULTIPLIKATION2_VORBEREITEN =>   --5.Takt
   ----------------------------------------------------------------------------------
      s_Mult_in2    <= s_b_in;    --b
      s_STATE       <= MULTIPLIKATION2;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION2 =>               --6.Takt
   ----------------------------------------------------------------------------------
      s_Mult32     <= s_Mult_in1 * s_Mult_in2 ;  
      s_STATE      <= MULTIPLIKATION2_SPECHERN;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION2_SPECHERN =>      --7.Takt
   ----------------------------------------------------------------------------------
      if s_Mult32 (31 downto 15) > to_signed(32766,16)     then
         sb_Mult <= to_signed(32767,16);
      
      elsif s_Mult32 (31 downto 15) < to_signed(-32766,16) then
         sb_Mult <= to_signed(-32767,16);
      
      else
         sb_Mult       <= s_Mult32 (30 downto 15);
      end if;

      s_STATE       <= MULTIPLIKATION3_VORBEREITEN;
   
----------------------------------------------------------------------------------
   when MULTIPLIKATION3_VORBEREITEN =>   --8.Takt
   ----------------------------------------------------------------------------------
      s_Mult_in2    <= s_c_in;    --c
      s_STATE       <= MULTIPLIKATION3;   

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION3 =>              --9.Takt
   ----------------------------------------------------------------------------------
      s_Mult32     <= s_Mult_in1 * s_Mult_in2 ;  
      s_STATE      <= MULTIPLIKATION3_SPECHERN;

   ----------------------------------------------------------------------------------
   when MULTIPLIKATION3_SPECHERN =>     --10.Takt
   ----------------------------------------------------------------------------------
      if s_Mult32 (31 downto 15) > to_signed(32766,16)     then
         sc_Mult <= to_signed(32767,16);
      
      elsif s_Mult32 (31 downto 15) < to_signed(-32766,16) then
         sc_Mult <= to_signed(-32767,16);
      
      else
         sc_Mult       <= s_Mult32 (30 downto 15);
      end if;

      s_STATE       <= DATEN_AUSGEBEN;

   ---------------------------------------------------------------------------
   when DATEN_AUSGEBEN => --11. Takt
   ---------------------------------------------------------------------------
      a_out <= sa_Mult;
      b_out <= sb_Mult;
      c_out <= sc_Mult;
      
      Data_Out_Valid_fak_abc <= '1';
      S_STATE                <= EINGANG_SPEICHERN;

   ---------------------------------------------------------------------
   when others =>
   ---------------------------------------------------------------------
      s_STATE <= EINGANG_SPEICHERN;

   end case;
      end if;  --reset
      end process hauptprozess;

   end architecture behavioral;
