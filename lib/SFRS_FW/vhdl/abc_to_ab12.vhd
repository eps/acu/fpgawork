--  Filename: abc_to_ab12.vhd
--
--  Beschrebung:
--      Transformation abc ->alpha beta (siehe Glechung B6, Seite 96, Dipl Arbeit TU A.Wiest)
--      alpha = a*2/3 - b*1/3       - c* 1/3
--      beta  =          b*1/sqrt(3) - c* 1/sqrt(3)
--
--  Eing. Groessen:
--      a              a Komponente 16bit signed
--      b              b Komponente 16bit signed
--      c              c Komponente 16bit signed
--
--      clk            clock
--      Enable         symbolisiert Gültigkeit der Eingangsdaten
--      reset          Berechnung neue starten,
--                     alle Varablen auf Null setzen
--
--  Ausg. Groessen:
--      alpha          Komponente der ab System, 16bit signed
--      beta           Komponente der ab System, 16bit signed
--
--      Data_Out_Valid_abc  '1' wenn die Berechnung fertig ist
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity abc_to_ab12 is
   generic(
      takt : integer := 100_000_000);

   port(
      --System-Eingaenge:
      clk, reset, Enable   : in std_logic;

      --Eingaenge:
      a1_in, b1_in, c1_in  : in signed (15 downto 0);
      a2_in, b2_in, c2_in  : in signed (15 downto 0);
      a3_in, b3_in, c3_in  : in signed (15 downto 0);
      a4_in, b4_in, c4_in  : in signed (15 downto 0);

      --Ausgaenge
      alpha1, beta1        : out signed(15 downto 0);
      alpha2, beta2        : out signed(15 downto 0);
      alpha3, beta3        : out signed(15 downto 0);
      alpha4, beta4        : out signed(15 downto 0);
      
      alpha_summe          : out signed(15 downto 0);
      beta_summe           : out signed(15 downto 0);
      
      Null_summe           : out signed(15 downto 0);
      Null_Komp1           : out signed(15 downto 0);
      Null_Komp2           : out signed(15 downto 0);
      Null_Komp3           : out signed(15 downto 0);
      Null_Komp4           : out signed(15 downto 0);

      Data_Out_Valid_abc   : out std_logic);

   end abc_to_ab12;

--============================================================================
architecture behavioral of abc_to_ab12 is
  attribute multstyle               : string;
  attribute multstyle of behavioral : architecture is "dsp";  -- Nutzt bevorzugt embedded multiplier
  
  --Matrix Transformationskonstanten:
  constant C1          : signed (15 downto 0) := to_signed(21845, 16);  -- 2/3        = 0,66 = (2^15-1) * 0,66 = 21845
  constant C2          : signed (15 downto 0) := to_signed(10922, 16);  -- 1/3        = 0,33 = (2^15-1) * 0,33 = 10922
  constant C3          : signed (15 downto 0) := to_signed(18918, 16);  -- 1/ sqrt(3) = 0,57 = (2^15-1) * 0,57 = 18918

  signal s_constMult   : signed (15 downto 0) := (others => '0');
  signal s_a           : signed (15 downto 0) := (others => '0');
  signal s_b           : signed (15 downto 0) := (others => '0');
  signal s_c           : signed (15 downto 0) := (others => '0');

  SIGNAL s_counter_ber : integer RANGE 0 to 3 := 0; -- = 0 wird das erste System berechnet =1 das zweite

  ----------------------------------------------------------
  signal s_a1_in       : signed (15 downto 0) := (others => '0');
  signal s_b1_in       : signed (15 downto 0) := (others => '0');
  signal s_c1_in       : signed (15 downto 0) := (others => '0');
  signal s_a2_in       : signed (15 downto 0) := (others => '0');
  signal s_b2_in       : signed (15 downto 0) := (others => '0');
  signal s_c2_in       : signed (15 downto 0) := (others => '0');
  signal s_a3_in       : signed (15 downto 0) := (others => '0');
  signal s_b3_in       : signed (15 downto 0) := (others => '0');
  signal s_c3_in       : signed (15 downto 0) := (others => '0');
  signal s_a4_in       : signed (15 downto 0) := (others => '0');
  signal s_b4_in       : signed (15 downto 0) := (others => '0');
  signal s_c4_in       : signed (15 downto 0) := (others => '0');
 
  ----------------------------------------------------------
  signal s_alpha       : signed (15 downto 0) := (others => '0');
  signal s_beta        : signed (15 downto 0) := (others => '0');
  signal s_alpha1      : signed (15 downto 0) := (others => '0');
  signal s_beta1       : signed (15 downto 0) := (others => '0');
  signal s_alpha2      : signed (15 downto 0) := (others => '0');
  signal s_beta2       : signed (15 downto 0) := (others => '0');
  signal s_alpha3      : signed (15 downto 0) := (others => '0');
  signal s_beta3       : signed (15 downto 0) := (others => '0');
  signal s_alpha4      : signed (15 downto 0) := (others => '0');
  signal s_beta4       : signed (15 downto 0) := (others => '0');
  
  ----------------------------------------------------------
  signal s_abcMult     : signed (15 downto 0) := (others => '0');
  signal s_Mult32      : signed (31 downto 0) := (others => '0');
  signal sa_Mult1      : signed (15 downto 0) := (others => '0');
  signal sa_Mult2      : signed (15 downto 0) := (others => '0');
  signal sa_Mult3      : signed (15 downto 0) := (others => '0');
  signal sa_Mult4      : signed (15 downto 0) := (others => '0');
  signal sa_Mult5      : signed (15 downto 0) := (others => '0');
  
  signal sa_Null       : signed (15 downto 0) := (others => '0');
  signal s_Null1       : signed (15 downto 0) := (others => '0');
  signal s_Null2       : signed (15 downto 0) := (others => '0');
  signal s_Null3       : signed (15 downto 0) := (others => '0');
  signal s_Null4       : signed (15 downto 0) := (others => '0');
  
  signal s_Summe1      : signed (17 downto 0) := (others => '0');  --16 + 16 + 16bit =18bit
  signal s_Summe2      : signed (16 downto 0) := (others => '0');  --16 + 16bit =17 bit

  type STATE_ABC_TO_AB is( EINGANG_SPEICHERN,
                           EINGANG_AUSWAEHLEN,
                           MULTIPLIKATION1_VORBEREITEN, MULTIPLIKATION1, 
                           MULTIPLIKATION2, 
                           MULTIPLIKATION3, 
                           MULTIPLIKATION4, 
                           MULTIPLIKATION5, 
                           MULTIPLIKATION6, MULTIPLIKATION6_SPEICHERN,
                           ADDITION,
                           ENDERGEBNISS_BERECHNEN,
                           BERECHNUNG_UMSCHALTEN,
                           DATEN_AUSGEBEN);
  signal s_STATE : STATE_ABC_TO_AB := EINGANG_SPEICHERN;


begin
  hauptprozess : process (clk, reset, Enable)
  begin
    if (reset = '1') then
      s_a1_in    <= (others => '0');
      s_b1_in    <= (others => '0');
      s_c1_in    <= (others => '0');
      s_a2_in    <= (others => '0');
      s_b2_in    <= (others => '0');
      s_c2_in    <= (others => '0');
      s_a3_in    <= (others => '0');
      s_b3_in    <= (others => '0');
      s_c3_in    <= (others => '0');
      s_a4_in    <= (others => '0');
      s_b4_in    <= (others => '0');
      s_c4_in    <= (others => '0');

      s_alpha1   <= (others => '0');
      s_beta1    <= (others => '0');
      s_alpha2   <= (others => '0');
      s_beta2    <= (others => '0');
      s_alpha3   <= (others => '0');
      s_beta3    <= (others => '0');
      s_alpha4   <= (others => '0');
      s_beta4    <= (others => '0');

      s_abcMult  <= (others => '0');
      s_a        <= (others => '0');
      s_b        <= (others => '0');
      s_c        <= (others => '0');
      s_constMult<= (others => '0');
      s_Mult32   <= (others => '0');
      sa_Mult1   <= (others => '0');
      sa_Mult2   <= (others => '0');
      sa_Mult3   <= (others => '0');
      sa_Mult4   <= (others => '0');
      sa_Mult5   <= (others => '0');

      s_Summe1   <= (others => '0');
      s_Summe2   <= (others => '0');

      s_counter_ber   <= 0;              -- Start der Berechnung mit dem ersten System

      alpha1     <= (others => '0');
      beta1      <= (others => '0');
      alpha2     <= (others => '0');
      beta2      <= (others => '0');
      alpha3     <= (others => '0');
      beta3      <= (others => '0');
      alpha4     <= (others => '0');
      beta4      <= (others => '0');
      
      sa_Null    <= (others => '0');
      s_Null1    <= (others => '0');
      s_Null2    <= (others => '0');
      s_Null3    <= (others => '0');
      s_Null4    <= (others => '0');

      s_STATE            <= EINGANG_SPEICHERN;
      Data_Out_Valid_abc <= '0';

    elsif rising_edge(clk) then
      case s_STATE is
      ----------------------------------------------------------------------------------
      when EINGANG_SPEICHERN =>       --1.Takt
      ----------------------------------------------------------------------------------
         if (Enable = '1') then
            s_a1_in     <= a1_in;
            s_b1_in     <= b1_in;
            s_c1_in     <= c1_in;

            s_a2_in     <= a2_in;
            s_b2_in     <= b2_in;
            s_c2_in     <= c2_in;

            s_a3_in     <= a3_in;
            s_b3_in     <= b3_in;
            s_c3_in     <= c3_in;

            s_a4_in     <= a4_in;
            s_b4_in     <= b4_in;
            s_c4_in     <= c4_in;

            s_STATE     <= EINGANG_AUSWAEHLEN;
         end if;

         Data_Out_Valid_abc <= '0';

      ----------------------------------------------------------------------------------
      when EINGANG_AUSWAEHLEN =>       --2.Takt
      ----------------------------------------------------------------------------------
         if    (s_counter_ber   = 0) then
            s_a       <= s_a1_in;
            s_b       <= s_b1_in;
            s_c       <= s_c1_in;

         elsif (s_counter_ber   = 1) then
            s_a       <= s_a2_in;
            s_b       <= s_b2_in;
            s_c       <= s_c2_in;

         elsif (s_counter_ber   = 2) then
            s_a       <= s_a3_in;
            s_b       <= s_b3_in;
            s_c       <= s_c3_in;

         else
            s_a       <= s_a4_in;
            s_b       <= s_b4_in;
            s_c       <= s_c4_in;
         end if;

         s_STATE     <= MULTIPLIKATION1_VORBEREITEN;
      
      ----------------------------------------------------------------------------------
      when MULTIPLIKATION1_VORBEREITEN =>         -- 3.Takt
      ----------------------------------------------------------------------------------
         s_constMult <= C1;                       -- 2/3
         s_abcMult   <= s_a;                      -- a
         s_STATE     <= MULTIPLIKATION1;
      
      ----------------------------------------------------------------------------------
      when MULTIPLIKATION1 =>                     -- 4.Takt
      ----------------------------------------------------------------------------------
         s_Mult32    <= s_constMult * s_abcMult;  -- 2/3 * a
         s_constMult <= C2;                       -- 1/3
         s_abcMult   <= s_b;                      -- b
         s_STATE     <= MULTIPLIKATION2;
      
      ----------------------------------------------------------------------------------
      when MULTIPLIKATION2 =>                    -- 5.Takt
      ----------------------------------------------------------------------------------
         sa_Mult1    <= s_Mult32 (30 downto 15); -- 2/3 * a
         s_Mult32    <= s_constMult * s_abcMult; -- 1/3 * b
         s_constMult <= C2;                      -- 1/3
         s_abcMult   <= s_c;                     -- c
         s_STATE     <= MULTIPLIKATION3;

      ----------------------------------------------------------------------------------
      when MULTIPLIKATION3 =>                     -- 6.Takt
      ----------------------------------------------------------------------------------
         sa_Mult2    <= s_Mult32 (30 downto 15);  -- 1/3 * b
         s_Mult32    <= s_constMult * s_abcMult;  -- 1/3 * c
         s_constMult <= C3;                       -- 1/sqrt(3)
         s_abcMult   <= s_b;                      -- b
         s_STATE     <= MULTIPLIKATION4;

      ----------------------------------------------------------------------------------
      when MULTIPLIKATION4 =>                     -- 7.Takt
      ----------------------------------------------------------------------------------
         sa_Mult3    <= s_Mult32 (30 downto 15);  -- 1/3 * c
         s_Mult32    <= s_constMult * s_abcMult;  -- 1/sqrt(3) * b
         s_constMult <= C3;                       -- 1/sqrt(3)
         s_abcMult   <= s_c;                      -- c
         s_STATE     <= MULTIPLIKATION5;
         
      ----------------------------------------------------------------------------------
      when MULTIPLIKATION5 =>                     -- 8.Takt
      ----------------------------------------------------------------------------------
         sa_Mult4    <= s_Mult32 (30 downto 15);  -- 1/sqrt(3) * b
         s_Mult32    <= s_constMult * s_abcMult;  -- 1/sqrt(3) * c
         
         s_constMult <= C2;                       -- 1/3
                                                  -- (a+b+c)
         if    s_a + s_b + s_c >  32767 then 
            s_abcMult <= to_signed( 32767,16); 
         elsif s_a + s_b + s_c < -32767 then 
            s_abcMult <= to_signed(-32767,16);
         else                               
            s_abcMult <= s_a + s_b + s_c;
         end if;
         s_STATE     <= MULTIPLIKATION6;

      ----------------------------------------------------------------------------------
      when MULTIPLIKATION6 =>                     -- 9.Takt
      ----------------------------------------------------------------------------------
         sa_Mult5    <= s_Mult32 (30 downto 15);  -- 1/sqrt(3) * c
         s_Mult32    <= s_constMult * s_abcMult;  -- 1/3 (a+b+c)
         s_STATE     <= MULTIPLIKATION6_SPEICHERN;

      ----------------------------------------------------------------------------------
      when MULTIPLIKATION6_SPEICHERN =>           -- 10.Takt
      ----------------------------------------------------------------------------------
         sa_Null     <= s_Mult32 (30 downto 15);  -- 1/3 (a+b+c)
         s_STATE     <= ADDITION;

      ---------------------------------------------------------------------------------
      when ADDITION =>                            -- 11. Takt, beide Reihen aufaddieren
      ---------------------------------------------------------------------------------
         s_Summe1    <= resize(sa_Mult1, 18) - resize(sa_Mult2, 18) - resize(sa_Mult3, 18);
         s_Summe2    <= resize(sa_Mult4, 17) - resize(sa_Mult5, 17);
         s_STATE     <= ENDERGEBNISS_BERECHNEN;

      ---------------------------------------------------------------------------------
      when ENDERGEBNISS_BERECHNEN =>              -- 12.Takt
      ---------------------------------------------------------------------------------
         if    s_Summe1 > to_signed( 32767, 16) then     s_alpha <= to_signed( 32767, 16);
         elsif s_Summe1 < to_signed(-32767, 16) then     s_alpha <= to_signed(-32767, 16);
         else                                            s_alpha <= s_Summe1(15 downto 0);
         end if;

         if    s_Summe2 > to_signed( 32767, 16) then     s_beta <= to_signed( 32767, 16);
         elsif s_Summe2 < to_signed(-32767, 16) then     s_beta <= to_signed(-32767, 16);
         else                                            s_beta <= s_Summe2(15 downto 0);
         end if;
          s_STATE     <= BERECHNUNG_UMSCHALTEN;

      ---------------------------------------------------------------------------
      when BERECHNUNG_UMSCHALTEN => --13. Takt
      ---------------------------------------------------------------------------
      if (s_counter_ber = 0) then
         s_alpha1         <= s_alpha;
         s_beta1          <= s_beta;
         s_Null1          <= sa_Null;
         s_counter_ber    <= 1;
         S_STATE          <= EINGANG_AUSWAEHLEN;

      elsif (s_counter_ber = 1) then
         s_alpha2         <= s_alpha;
         s_beta2          <= s_beta;
         s_Null2          <= sa_Null;
         s_counter_ber    <= 2;
         S_STATE          <= EINGANG_AUSWAEHLEN;

      elsif (s_counter_ber = 2) then
         s_alpha3         <= s_alpha;
         s_beta3          <= s_beta;
         s_Null3          <= sa_Null;
         s_counter_ber    <= 3;
         S_STATE          <= EINGANG_AUSWAEHLEN;

      else
         s_alpha4         <= s_alpha;
         s_beta4          <= s_beta;
         s_Null4          <= sa_Null;
         s_counter_ber    <= 0;
         S_STATE          <= DATEN_AUSGEBEN;
      end if;

      ---------------------------------------------------------------------------
      when DATEN_AUSGEBEN => --14. Takt
      ---------------------------------------------------------------------------
         alpha1           <= s_alpha1;
         beta1            <= s_beta1 ;

         alpha2           <= s_alpha2;
         beta2            <= s_beta2 ;

         alpha3           <= s_alpha3;
         beta3            <= s_beta3 ;

         alpha4           <= s_alpha4;
         beta4            <= s_beta4 ;
         
         Null_Komp1       <= s_Null1 ;
         Null_Komp2       <= s_Null2 ;
         Null_Komp3       <= s_Null3 ;
         Null_Komp4       <= s_Null4 ;
         
         if     s_alpha1 + s_alpha2 + s_alpha3 + s_alpha4 >  32767 then alpha_summe <= to_signed( 32767, 16);
         elsif  s_alpha1 + s_alpha2 + s_alpha3 + s_alpha4 < -32767 then alpha_summe <= to_signed(-32767, 16);
         else                                                           alpha_summe <= s_alpha1 + s_alpha2 + s_alpha3 + s_alpha4 ;
         end if;

         if     s_beta1 + s_beta2 + s_beta3 + s_beta4 >  32767 then beta_summe <= to_signed( 32767, 16);
         elsif  s_beta1 + s_beta2 + s_beta3 + s_beta4 < -32767 then beta_summe <= to_signed(-32767, 16);
         else                                                       beta_summe <= s_beta1 + s_beta2 + s_beta3 + s_beta4 ;
         end if;
         
         if     s_Null1 + s_Null2 + s_Null3 + s_Null4 >  32767 then Null_summe <= to_signed( 32767, 16);
         elsif  s_Null1 + s_Null2 + s_Null3 + s_Null4 < -32767 then Null_summe <= to_signed(-32767, 16);
         else                                                       Null_summe <= s_Null1 + s_Null2 + s_Null3 + s_Null4 ;
         end if;

         Data_Out_Valid_abc   <= '1';
         S_STATE              <= EINGANG_SPEICHERN;

      ---------------------------------------------------------------------
      when others =>
      ---------------------------------------------------------------------
         s_STATE <= EINGANG_SPEICHERN;

      end case;
   end if;  --reset
   end process hauptprozess;

end architecture behavioral;
