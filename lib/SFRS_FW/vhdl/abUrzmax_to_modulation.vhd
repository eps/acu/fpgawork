-- Date: 17.09.2018
-- Author: J. Mair, 
-- 03.04.2020 A.Wiest (sequentielle Multiplikation)
-- 
-- This module calculates the modulation based on alpha,beta,uzw1 and uzw2.
--
-- Following equation is implemented:
--
--     					sqrt(alpha^2 + beta^2)
--  modulation =     ____________________________
--							(uzw1+uzw2)*(2/3)*cos(30°)
--
-- (2/3)*cos(30°)=(2/3)*sqrt(3)/2=1/sqrt(3)=0,57 * 32767 = 18918
--
-- Additionally it calculates the difference of uzw1 and uzw2 where 32767 means no difference. Not Signed!
--
-- uzw_diff:
--
--_____________2^16-1 	-> uzw1 = max ; uzw2 = 0
--		.
--		.						-> uzw1>uzw2
--		.
--_____________2^15   	-> uzw1=uzw2
--		.
--		.						-> uzw1<uzw2
--		.
--_____________0			-> uzw1 = 0; uzw2 = max
--
--
-- And it calculates the sum of uzw1 and uzw:2
-- uzw_ges = uzw1+uzw2

-- Timing :
-- Data_Out_Valid ->Data_Out_Valid = 219  + 9 fuer Multiplikatio = 228 Takte =2,28us
--	SQRT_Calculation ~ 83 Takte
-- DIV_Calculation ~ 133 Takte
-- Rest : 3 Takte (SAVE_INPUT,MULTICALC,OUTPUT)
--823 LE

LIBRARY ieee;
USE     ieee.std_logic_1164.all;
USE     ieee.numeric_std.all;

ENTITY abUrzmax_to_modulation IS
GENERIC(
		clk_hz 							: integer               		:= 100_000_000;      -- 100 MHz
	   bit_sqrt  						: natural               		:= 32;             	-- Required Number of Bits for the square root process
		bit_div   						: natural  							:= 32 );            	-- Required Number of Bits for the division process

PORT(	clk,reset, enable 			: IN  STD_LOGIC:= '0';
		alpha,beta 						: IN  SIGNED  (15 DOWNTO 0);  	--(alpha^2 + beta^2) <= 1 Der Betrag ist kleiner gleich 1 32767=350V
		uzw1,uzw2						: IN  SIGNED  (15 DOWNTO 0);  	-- uzw1 + uzw2 < 2^15-1 32767= 500V
		c_FakUzw_Urz               : IN  UNSIGNED(15 DOWNTO 0);     -- Skalierungsfaktor zw. Urz und Uzw, siehe Berechnung

      modulation 						: OUT UNSIGNED(15 DOWNTO 0);
      modulation_fak 				: OUT UNSIGNED(15 DOWNTO 0);     --Modulation x cos(30deg)
      uzw_diff							: OUT SIGNED  (15 DOWNTO 0);
		uzw_ges							: OUT SIGNED  (15 DOWNTO 0);
		Regler_Grenze              : OUT SIGNED  (15 DOWNTO 0);
      Data_Out_Valid_modulation	: OUT STD_LOGIC);
END ENTITY abUrzmax_to_modulation;


ARCHITECTURE behavioral OF abUrzmax_to_modulation IS
CONSTANT c_1sqrt3                : UNSIGNED(15 DOWNTO 0)	:= to_unsigned(56755,16);  -- cos(30deg)
---Main Process Signals------------------------------
SIGNAL s_alpha 						: UNSIGNED(15 DOWNTO 0)	:=(others=>'0');
SIGNAL s_beta  						: UNSIGNED(15 DOWNTO 0)	:=(others=>'0');

SIGNAL s_urz_max 						: UNSIGNED(31 DOWNTO 0)	:=(others=>'0');
SIGNAL s_udc 							: UNSIGNED(15 DOWNTO 0)	:=(others=>'0');  -- 65535 = 1000V
SIGNAL s_uzwdiff						: SIGNED  (15 DOWNTO 0)	:=(others=>'0');

SIGNAL s_alpha_squared 				: UNSIGNED(31 DOWNTO 0)	:=(others=>'0'); 	--2^16^2 = 2^32
SIGNAL s_beta_squared 				: UNSIGNED(31 DOWNTO 0)	:=(others=>'0');

SIGNAL s_absolute_val_scaled		: UNSIGNED(31 DOWNTO 0)	:=(others=>'0');  -- After square-root
SIGNAL s_normed_absolute_val		: UNSIGNED(15 DOWNTO 0)	:=(others=>'0'); 	-- Divided by s_urz_max(31 DOWNTO 16)(precomma) -> 32Bit-16Bit

SIGNAL s_modulation 					: UNSIGNED(15 DOWNTO 0)	:=(others=>'0');
SIGNAL s_modulation_fak				: UNSIGNED(15 DOWNTO 0)	:=(others=>'0');

SIGNAL s_Mult32 						: UNSIGNED(31 DOWNTO 0)	:=(others=>'0');
SIGNAL s_Mult_a 						: UNSIGNED(15 DOWNTO 0)	:=(others=>'0');
SIGNAL s_Mult_b 						: UNSIGNED(15 DOWNTO 0)	:=(others=>'0');


TYPE STATE_MAIN IS(
	MAIN_SAVE_INPUT,
	MULT1_VORBEREITEN, MULT1_AUSFUEHREN, MULT1_SPEICHERN,
							 MULT2_AUSFUEHREN, MULT2_SPEICHERN,
							 MULT3_AUSFUEHREN, MULT3_SPEICHERN,
	MAIN_SQRT_START,
	MAIN_SQRT,
	MAIN_DIV_START,
	MAIN_DIV,
   MAIN_MODULATION,
   MULT4_VORBEREITEN, MULT4_AUSFUEHREN, MULT4_SPEICHERN,
   MULT5_VORBEREITEN, MULT5_AUSFUEHREN,
	MAIN_OUTPUT);

SIGNAL s_main_STATE 					: STATE_MAIN 				:= MAIN_SAVE_INPUT;

---SQRT_Calculation Process Signals----------

---Extern-------------------
SIGNAL s_sqrt_Data_In    			: UNSIGNED(31 downto 0) 			:=(others=>'0'); 			-- Input for the data to square root
SIGNAL s_sqrt_Data_Out   			: UNSIGNED(15 downto 0) 			:=(others=>'0');  		-- Output of the square rooted
SIGNAL s_start_sqrt 					: STD_LOGIC 							:= '0';
SIGNAL s_sqrt_finish					: STD_LOGIC 							:= '0';
---Intern----------------
constant m            				: natural               			:=  bit_sqrt/2 + 2;  	-- 18   (19 bit)
SIGNAL s_sqrt_input 					: UNSIGNED(bit_sqrt-1 downto 0) 	:= (others => '0');     -- Square root input (32 bit)
SIGNAL s_sqrt_output					: UNSIGNED(m-3 downto 0)   		:= (others => '0');     -- Square root output  (16 bit)

SIGNAL s_sqrt_reg_1					: UNSIGNED(m-1 downto 0)   		:= (others => '0');     -- Square root remainder (18 bit)
SIGNAL s_sqrt_reg_2 					: UNSIGNED(m-1 downto 0)   		:= (others => '0');     -- Square root remainder (18 bit)
SIGNAL s_sqrt_reg_3 					: UNSIGNED(m-1 downto 0)   		:= (others => '0');     -- Square root remainder (18 bit)

SIGNAL bit_cnt        				: integer range 0 to 40    		:= 0;


TYPE STATE_SQRT_CALC IS  (
	SQRT_SAVE_INPUT,
	SQRT_REG_2,
	SQRT_REG_1,
	SQRT_MSB_CHECK,
	SQRT_RESULT,
	SQRT_LOAD_NEW_BITS,
   SQRT_OUTPUT_LIMIT,
	SQRT_OUTPUT);

SIGNAL s_sqrt_STATE 				: STATE_SQRT_CALC 					:= SQRT_SAVE_INPUT;

---Division  Process( a/b ) Signals--------------------------------------------------------------------

---Extern-----
SIGNAL s_div_numerator_Data_In   	: UNSIGNED(bit_div-1 downto 0) 	:= (others => '0'); --
SIGNAL s_div_denominator_Data_In		: UNSIGNED(bit_div-1 downto 0) 	:= (others => '0');
SIGNAL s_div_Data_Out					: UNSIGNED(bit_div-1 downto 0) 	:= (others => '0');
SIGNAL s_start_division 				: STD_LOGIC					 			:= '0';
SIGNAL s_div_finish 						: STD_LOGIC					 			:= '0';

---Intern-----
SIGNAL s_division_dividend				: UNSIGNED(bit_div-1 downto 0) 	:= (others => '0'); --  Numerator (Dividend) (32 bit) signal
SIGNAL s_division_divisior 			: UNSIGNED(bit_div-1 downto 0) 	:= (others => '0'); --  Denominator(Divisior) (32 bit) signal
SIGNAL s_division_remainder			: UNSIGNED(bit_div   downto 0) 	:= (others => '0'); --  Division remainder (33 bit) signal
SIGNAL s_bit_cnt 							: integer range 0 to bit_div 	 	:= 0;

TYPE STATE_DIVISION_CALC IS  (
	DIV_SAVE_INPUT,
	DIV_REMAINDER_BITS,
	DIV_DIVIDEND_BITS_INPUT,
	DIV_SUBTRACTION,
	DIV_CHECKING_MSB,
	DIV_OUTPUT,
	DIV_DATA_VALID,
	DIV_WAIT_TAKT);

SIGNAL s_div_STATE 						: STATE_DIVISION_CALC 				:= DIV_SAVE_INPUT;

BEGIN

main : PROCESS (clk,reset,enable,alpha,beta,uzw1,uzw2)
BEGIN

if  reset = '1' then                                     -- Resetting all the required signals
	s_alpha 							<= (others=>'0');
	s_beta  							<= (others=>'0');
	s_alpha_squared 				<= (others=>'0');
	s_beta_squared 				<= (others=>'0');
   
   s_udc   							<= (others=>'0');

	Modulation						<= (others=>'0');
   Modulation_fak             <= (others=>'0');
	s_modulation   				<= (others=>'0');
	s_modulation_fak 				<= (others=>'0');
   
	Data_Out_Valid_modulation	<='0';
	s_main_STATE    				<= MAIN_SAVE_INPUT;

else
	if rising_edge(clk)   THEN
		CASE s_main_STATE  IS

   ---------------------------------------------------------------------------------
		WHEN MAIN_SAVE_INPUT =>  --1.
   ---------------------------------------------------------------------------------
			Data_Out_Valid_modulation	<='0';
			IF enable = '1' THEN
				if    alpha < to_signed(-32760, 16) then s_alpha <= to_unsigned(32761,16); --sonst bei sqrt berechnung Ueberlauf bei ca. 32765
				elsif alpha > to_signed( 32760, 16) then s_alpha <= to_unsigned(32761,16);
				else	                                   s_alpha <= UNSIGNED(STD_LOGIC_VECTOR(ABS(alpha))); 		-- All SIGNED values to UNSIGNED
				end if;
            
            if    beta < to_signed(-32760, 16) then s_beta <= to_unsigned(32761,16); --sonst bei sqrt berechnung Ueberlauf bei ca. 32765
				elsif beta > to_signed( 32760, 16) then s_beta <= to_unsigned(32761,16);
				else	                                  s_beta <= UNSIGNED(STD_LOGIC_VECTOR(ABS(beta))); 		-- All SIGNED values to UNSIGNED
				end if;
            
				s_udc 						<= UNSIGNED(STD_LOGIC_VECTOR(ABS(uzw1)))+ UNSIGNED(STD_LOGIC_VECTOR(ABS(uzw2)));
				s_uzwdiff            	<= uzw1 - uzw2;
				s_main_STATE 				<= MULT1_VORBEREITEN;
			END IF;

	---------------------------------------------------------------------------------
		WHEN MULT1_VORBEREITEN =>
   ---------------------------------------------------------------------------------
			s_Mult_a 		<= s_alpha;
			s_Mult_b 		<= s_alpha;
			s_main_STATE 	<= MULT1_AUSFUEHREN;
	---------------------------------------------------------------------------------
		WHEN MULT1_AUSFUEHREN =>
	---------------------------------------------------------------------------------
			s_Mult32 			<= s_Mult_a * s_Mult_b;
			s_main_STATE 		<= MULT1_SPEICHERN;
	---------------------------------------------------------------------------------
		WHEN MULT1_SPEICHERN =>
	---------------------------------------------------------------------------------
			s_alpha_squared 	<= s_Mult32;
			s_Mult_a 		 	<= s_beta;
			s_Mult_b 		 	<= s_beta;
			s_main_STATE 	 	<= MULT2_AUSFUEHREN;

	---------------------------------------------------------------------------------
		WHEN MULT2_AUSFUEHREN =>
	---------------------------------------------------------------------------------
			s_Mult32 		   <= s_Mult_a * s_Mult_b;
			s_main_STATE 	   <= MULT2_SPEICHERN;
	---------------------------------------------------------------------------------
		WHEN MULT2_SPEICHERN =>
	---------------------------------------------------------------------------------
			s_beta_squared    <= s_Mult32;
         s_Mult_a 		   <= s_udc;             --Absolutwert
			s_Mult_b 		   <= c_FakUzw_Urz;
			s_main_STATE 	   <= MULT3_AUSFUEHREN;
   
   ---------------------------------------------------------------------------------
		WHEN MULT3_AUSFUEHREN =>
	---------------------------------------------------------------------------------
			s_Mult32 		<= s_Mult_a * s_Mult_b;
			s_main_STATE 	<= MULT3_SPEICHERN;
   
   ---------------------------------------------------------------------------------
		WHEN MULT3_SPEICHERN =>
	---------------------------------------------------------------------------------
			s_urz_max 		<= s_Mult32;
         s_main_STATE 	<= MAIN_SQRT_START;
         
   ---------------------------------------------------------------------------------
		WHEN MAIN_SQRT_START =>
   ---------------------------------------------------------------------------------
			s_sqrt_Data_In 	<= (s_alpha_squared + s_beta_squared); --32Bit + 32bit kann aber kein Ueberlauf geben, da bis 32767 aber als 16bit unsigned
			s_start_sqrt 		<= '1';                                -- Starte Wurzel Berechnung
			s_main_STATE 		<= MAIN_SQRT;

	---------------------------------------------------------------------------------
		WHEN MAIN_SQRT =>
   ---------------------------------------------------------------------------------
			s_start_sqrt 	<= '0';

			IF (s_sqrt_finish = '1') THEN
				s_absolute_val_scaled	<= s_sqrt_Data_Out & X"0000";--Scale * 2^16
				s_main_STATE  				<= MAIN_DIV_START;
			END IF;

	---------------------------------------------------------------------------------
		WHEN MAIN_DIV_START =>   --4.
   ---------------------------------------------------------------------------------
			s_div_numerator_Data_In		<= s_absolute_val_scaled;
			s_div_denominator_Data_In	<= X"0000" & s_urz_max(30 DOWNTO 15);
			s_start_division 				<= '1';
			s_main_STATE  					<= MAIN_DIV;

	---------------------------------------------------------------------------------
		WHEN MAIN_DIV =>   		--5.
   ---------------------------------------------------------------------------------
			s_start_division				<= '0';
			IF (s_div_finish = '1') THEN
				s_normed_absolute_val	<= s_div_Data_Out(15 DOWNTO 0);
				s_main_STATE  				<= MAIN_MODULATION;
		   END IF;
      
   ---------------------------------------------------------------------------------
		WHEN MAIN_MODULATION =>  	--6.
   ---------------------------------------------------------------------------------
         IF (s_sqrt_Data_Out >= s_urz_max(30 DOWNTO 15)) THEN -- Abfangen wenn die Modulation groesser als 1 ist
            s_modulation 					<= to_unsigned (65535,16);
			ELSE
            s_modulation 					<= s_normed_absolute_val;
         END IF;   
         
         s_main_STATE 	<= MULT4_VORBEREITEN;

	---------------------------------------------------------------------------------
		WHEN MULT4_VORBEREITEN =>
	---------------------------------------------------------------------------------
			s_Mult_a 		<= s_modulation; --Absolutwert
			s_Mult_b 		<= c_1sqrt3;
         s_main_STATE 	<= MULT4_AUSFUEHREN;
         
	---------------------------------------------------------------------------------
		WHEN MULT4_AUSFUEHREN =>
	---------------------------------------------------------------------------------
         s_Mult32 		<= s_Mult_a * s_Mult_b;
			s_main_STATE 	<= MULT4_SPEICHERN;
	
   ---------------------------------------------------------------------------------
		WHEN MULT4_SPEICHERN =>
	---------------------------------------------------------------------------------
         s_modulation_fak <= s_Mult32(31 downto 16);  
         s_main_STATE 	  <= MULT5_VORBEREITEN;
   
   ---------------------------------------------------------------------------------
		WHEN MULT5_VORBEREITEN =>
	---------------------------------------------------------------------------------
			s_Mult_a 		<= unsigned('0' & STD_LOGIC_VECTOR(s_udc(15 downto 1))); -- gesamte Zwischenkreisspannung
			s_Mult_b 		<= c_FakUzw_Urz;
         s_main_STATE 	<= MULT5_AUSFUEHREN;
         
	---------------------------------------------------------------------------------
		WHEN MULT5_AUSFUEHREN =>
	---------------------------------------------------------------------------------
         s_Mult32 		<= s_Mult_a * s_Mult_b; 
         s_main_STATE 	<= MAIN_OUTPUT;  
     
   ---------------------------------------------------------------------------------
		WHEN MAIN_OUTPUT =>  	--6.
   ---------------------------------------------------------------------------------
         uzw_ges                    <= signed('0' & STD_LOGIC_VECTOR(s_udc(15 downto 1))); -- wird durch zwei geteilt
         uzw_diff          			<= s_uzwdiff;
         modulation                 <= s_modulation;
         modulation_fak             <= s_modulation_fak;
         Regler_Grenze              <= signed(STD_LOGIC_VECTOR(s_Mult32(30 downto 15))); 
         
         Data_Out_Valid_modulation	<= '1';
			s_main_STATE  					<= MAIN_SAVE_INPUT;
 
		WHEN OTHERS =>
			s_main_STATE 					<= MAIN_SAVE_INPUT;
	   END CASE;

	END IF;  --clk
END IF;  --reset
END PROCESS main;




SQRT_Calculation: PROCESS (clk, reset, s_start_sqrt)         -- Process for the  Square root Calculation
   BEGIN
      if  reset = '1' then                                   -- Resetting all the required signals
         s_sqrt_input  		<=  (others => '0');
         s_sqrt_output 		<=  (others => '0');
         s_sqrt_reg_3  		<=  (others => '0');
         s_sqrt_reg_2  		<=  (others => '0');
         s_sqrt_reg_1  		<=  (others => '0');
			s_sqrt_Data_Out   <=  (others => '0');
			s_sqrt_finish 		<= '0';
         s_sqrt_STATE    	<=  SQRT_SAVE_INPUT;
      else

			if rising_edge(clk)   THEN
				CASE s_sqrt_STATE  IS

   ---------------------------------------------------------------------------------
   when SQRT_SAVE_INPUT=>  	 --1.
   ---------------------------------------------------------------------------------
      s_sqrt_output 			<=  (others => '0');
      s_sqrt_reg_3  			<=  (others => '0');
      s_sqrt_reg_2  			<=  (others => '0');
      s_sqrt_reg_1  			<=  (others => '0');
      s_sqrt_finish 			<= '0';

      if (s_start_sqrt = '1') then
         s_sqrt_input 		<= s_sqrt_Data_In;                                  --Getting square root input from the main process
         s_sqrt_STATE   	<= SQRT_REG_2;
      end if;

   ---------------------------------------------------------------------------------
   when SQRT_REG_2 =>   		--2.
   ---------------------------------------------------------------------------------
   	--1) Input Register
		s_sqrt_reg_2(1 downto 0)  		<=  s_sqrt_input(bit_sqrt-1 downto bit_sqrt-2);   -- Providing the obtained input to the input shift register
		s_sqrt_reg_2(m-1 downto 2) 	<=  s_sqrt_reg_3(m-3 downto 0);                   -- Connecting the total 9 generated bits from the Partial Remainder Register to the Input Register
      s_sqrt_STATE  					   <=  SQRT_REG_1;

   ---------------------------------------------------------------------------------
   when SQRT_REG_1=>   			--3.
   ---------------------------------------------------------------------------------
		--2) Square root register
		s_sqrt_reg_1(0)           		<=  '1';                          	-- Setting first LSB of Square Root Register to '1' as we substract '01' from the input bits
		s_sqrt_reg_1(1)           		<=  s_sqrt_reg_3(m-1);           	-- Giving the result of the MSB of the Partial Remainder Register to the second bit of the Square Root Register
		s_sqrt_reg_1(m-1 downto 2) 	<=  s_sqrt_output;              		-- Loading the 'required sqrt result' to the Square Root Register
      s_sqrt_STATE  						<=  SQRT_MSB_CHECK;

   ---------------------------------------------------------------------------------
   when SQRT_MSB_CHECK=>   	--4.
   ---------------------------------------------------------------------------------
		--3) Partial Remainder  Register
		if (s_sqrt_reg_3(m-1) = '0') then                         			-- It substracts only when the partial remainder result is "0" otherwise it adds
			s_sqrt_reg_3 					<=  s_sqrt_reg_2 - s_sqrt_reg_1;
		else
			s_sqrt_reg_3 					<=  s_sqrt_reg_2 + s_sqrt_reg_1;
		end if;
      s_sqrt_STATE  						<=   SQRT_RESULT;

	---------------------------------------------------------------------------------
   when SQRT_RESULT=>   		--5.
   ---------------------------------------------------------------------------------
   	--4) Square root Register bit shifting to get the final result
		s_sqrt_output(m-3 downto 1)	<= s_sqrt_output   (m-4 downto 0);  -- shifting by 1 bit to the left.
		s_sqrt_output(0)  				<= NOT s_sqrt_reg_3(m-1);           -- Connecting the feedback from MSB of the the partial remainder register
      s_sqrt_STATE  						<= SQRT_LOAD_NEW_BITS;

	---------------------------------------------------------------------------------
   when SQRT_LOAD_NEW_BITS=>  --6.
   ---------------------------------------------------------------------------------
		if (bit_cnt <= m-3) then
   		-- 5) getting new 2 bits from the input
			s_sqrt_input(bit_sqrt-1 downto 2)  <= s_sqrt_input(bit_sqrt-3 downto 0);  -- shifting by 2 bit to the left to get the new input bits
         bit_cnt 									  <= bit_cnt + 1;

         if (bit_cnt < m - 3 ) then
           s_sqrt_STATE  		<=  SQRT_REG_2;
			elsif (bit_cnt = m - 3) then
				bit_cnt 			<= 0;
				s_sqrt_STATE  	<= SQRT_OUTPUT_LIMIT;
         end if;

      end if;

	---------------------------------------------------------------------------------
   when SQRT_OUTPUT_LIMIT=>   		--7.
   ---------------------------------------------------------------------------------
      if s_sqrt_output > to_unsigned(32767,16) then
         s_sqrt_Data_Out <= to_unsigned(32767,16);
      else
         s_sqrt_Data_Out <= s_sqrt_output;
      end if;

      s_sqrt_finish 	<= '1';
      s_sqrt_STATE  	<= SQRT_OUTPUT;
      
   ---------------------------------------------------------------------------------
   when SQRT_OUTPUT=>   		--8.
   ---------------------------------------------------------------------------------
      s_sqrt_STATE   <=  SQRT_SAVE_INPUT;

   END CASE;
   END IF;  --clk
   END IF;  --reset

END PROCESS SQRT_Calculation;

 --Division Calculation Process
  Division_Calculation: PROCESS (clk, reset, s_start_division)
   BEGIN
      if  reset = '1' then  -- Resetting all the required signals

			s_division_dividend   		<= (others => '0');
			s_division_divisior  		<= (others => '0');
			s_division_remainder  		<= (others => '0');
			s_div_Data_Out 			<= (others => '0');

			s_bit_cnt                  <= 0;
			s_div_finish  		<= '0';
			s_div_STATE 					<=  DIV_SAVE_INPUT;

      else
			if rising_edge(clk)   THEN
				CASE s_div_STATE IS

   ---------------------------------------------------------------------------------
   when DIV_SAVE_INPUT =>   	--1.Takt
   ---------------------------------------------------------------------------------
		If (s_start_division = '1' ) then

			s_division_remainder			<=  (others => '0');
         s_division_dividend 			<= s_div_numerator_Data_In ;  -- beide muessen gleiche Bitzahl haben
         s_division_divisior 			<= s_div_denominator_Data_In;

			s_div_finish 		<= '0';
			s_bit_cnt              		<= 0;
			s_div_STATE 		  		   <= DIV_REMAINDER_BITS;

      end If;

   ---------------------------------------------------------------------------------
   when DIV_REMAINDER_BITS=>   --2.Takt
   ---------------------------------------------------------------------------------
		-- (1) Doing 1 bit left shift of Remainder (Accumulator) register to go further in  the division process
      s_division_remainder (bit_div-1 downto 1) <= s_division_remainder (bit_div-2 downto 0);

      -- (2) Assigning a bit value to the LSB of the Remainder from the MSB of the dividend register (highest bit)
		s_division_remainder (0) <= s_division_dividend (bit_div-1);

      s_div_STATE <=   DIV_DIVIDEND_BITS_INPUT;

   ---------------------------------------------------------------------------------
    when DIV_DIVIDEND_BITS_INPUT=>   --3.Takt
   ---------------------------------------------------------------------------------
      -- (3) Doing 1 bit left shift of the dividend register to get new input bits
      s_division_dividend (bit_div-1 downto 1) <= s_division_dividend (bit_div-2 downto 0);

      s_div_STATE <=   DIV_SUBTRACTION;
   ---------------------------------------------------------------------------------
    when DIV_SUBTRACTION=>   --4.Takt
   ---------------------------------------------------------------------------------
      -- (4) Doing the subtraction for starting the division iteration process
		s_division_remainder <=  s_division_remainder - s_division_divisior;

      s_div_STATE <=   DIV_CHECKING_MSB;

   ---------------------------------------------------------------------------------
   when DIV_CHECKING_MSB=>   --5.Takt
   ---------------------------------------------------------------------------------
      if (s_bit_cnt <= bit_div-1) then

			-- (5) Checking the MSB of Remainder for assigning the LSB value to the dividend register
			if(s_division_remainder(bit_div) ='1') then -- If MSB of Remainder is '1' (negativ) then dividend LSB is '0'
				s_division_dividend(0) <='0';
				s_division_remainder <= s_division_remainder + s_division_divisior;

         else  -- If MSB of Remainder is '0' (positiv) then dividend LSB is '1'
					s_division_dividend(0) <='1';
         end if;

         if (s_bit_cnt < bit_div-1 ) then
				s_div_STATE <=  DIV_REMAINDER_BITS;
         elsif (s_bit_cnt = bit_div-1) then
				s_div_STATE <=  DIV_OUTPUT;
         end if;

			s_bit_cnt <= s_bit_cnt + 1;

		else
         s_bit_cnt 		<= 0;
         s_div_STATE<= DIV_OUTPUT;

		end if;

   ---------------------------------------------------------------------------------
   when DIV_OUTPUT=>   	--6.Takt
   ---------------------------------------------------------------------------------
		s_div_Data_Out<= s_division_dividend;
		s_div_finish  <= '0';
		s_div_STATE   <= DIV_DATA_VALID;

   ---------------------------------------------------------------------------------
   when DIV_DATA_VALID=>   --7.Takt
   ---------------------------------------------------------------------------------
      s_div_finish  <= '1';
		s_div_STATE  <= DIV_WAIT_TAKT;

	---------------------------------------------------------------------------------
   when DIV_WAIT_TAKT=>   	--8.Takt
   ---------------------------------------------------------------------------------
      s_div_finish  <= '0';
		s_div_STATE   <=  DIV_SAVE_INPUT;

   ---------------------------------------------------------------------------------
   when OTHERS=>   			--9.Takt
   ----------------------------------------------------------------------------------
		s_div_STATE <=  DIV_SAVE_INPUT;

   END CASE;
   END IF;  --clk
	END IF;  --reset
END PROCESS Division_Calculation;

END ARCHITECTURE behavioral;
