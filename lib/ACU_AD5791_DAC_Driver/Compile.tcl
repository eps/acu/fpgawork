###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work

pwd
         
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/SimFolder/vhdl/cntFreeWheel.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/SimFolder/vhdl/cntCmdInDriven.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_SPI/vhdl/ACU_Generic_4lines_SPI_Master.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_DelaySignal/vhdl/ACU_DelaySignal.vhd 

vcom -93 $path/ACU_AD5791_DAC_Driver.vhd

vcom -93 $path/Tb_ACU_AD5791_DAC_Driver.vhd
