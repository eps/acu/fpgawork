------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_AD5791_DAC_Driver.vhd                                                                               --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 26/06/2024                                                                                              --
--                                                                                                                    --
-- Comments : This module drives the AD5791 20b DAC chip.                                                             --
--            In order to reduce the driver size, only write actions are performed (it is not possible to read the    --
--            chip registers.                                                                                         --
--            The write action consists in two steps:                                                                 --
--              1) Configure the chip using the dedicated generic values.                                             --
--              2) Send the signal to convert.                                                                        --
--            The configuration register is written only once when the EnableRE signal is detected.                   --
--            The data to convert is written in free wheel mode gFreeWheel='1' or driven by a input pulse (NewDt2Wr)  --
--            In this last case, be awre that the NewDt2Wr period can't be shorter than 1,04 us (conversion period).  --
--            The data to write is shifted out on the SDO line on the SCLK RE (because the chip samples it on SCLKFE) -- 
--            Below there is the choosen timing for the SPI lines:                                                    --
--                t1=40ns                                                                                             --   
--                t2=t3=20ns                                                                                          --   
--                t4=10+20=30ns                                                                                       --   
--                t5=20ns                                                                                             --   
--                t6=gSYNCn*10ns (Default=50ns)                                                                       --   
--                t8=t9=20ns                                                                                          --   
--                                                                                                                    --
-- History  : Start up version 26/06/2024                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_AD5791_DAC_Driver is
  Generic (
         gFreeWheel              : std_logic:='1';                                           -- When 0, the conversion starts every NewDt2Wr RE pulse otherwise it is done continuosly.
         -- Chip conf
         gLinComp                : std_logic_vector(3 downto 0):=x"0";                       -- Linearity error compensation (See data sheet for more info).
         gSDODIS                 : std_logic:='1';                                           -- SDO pin enable/disable control (See data sheet for more info).
         gDACTRI                 : std_logic:='0';                                           -- DAC tristate control (See data sheet for more info).
         gOPGND                  : std_logic:='0';                                           -- Output ground clamp control (See data sheet for more info).
         gRBUF                   : std_logic:='1';                                           -- Output amplifier configuration control (See data sheet for more info).
         --SPI
         gSCLK_High              : unsigned(7 downto 0):="00000010";                         -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0):="00000010";                         -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0):="00000001";                         -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
         gUseSCLK_BK             : std_logic:='0'                                            -- When 0, SDI is sampled using SCLK as reference clock else SCLK_BK 

  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high
         Enable                  : in  std_logic;                                            -- When high, the driver operates on the SPI lines
         Ext_SPI_SlaveConnected  : in  std_logic;                                            -- It is low when a external slave card is connected to the SPI HUB
         
         NewDt2Wr                : in  std_logic;                                            -- New Data to convert/write pulse (It can't be faster than 1,04 us).
         Dt2Wr                   : in  std_logic_vector(19 downto 0);                        -- Data to convert/write value.
         	 
         SentProt                : out std_logic;                                            -- Pulse active high every time a protocol is sent.
                                                                                             -- It is used by the main FSM to know when drive the Enable signal.
	 	 
         SCLK                    : out std_logic;                                            -- SPI clock signal
         SCLK_BK                 : in  std_logic;                                            -- SCLK loop back (on the slave chip side) signal
         SCS_n                   : out std_logic;                                            -- Active low SPI Chip Select signal
         SDO                     : out std_logic                                             -- Serial SPI data out (from FPGA) signal
  );
end Entity ACU_AD5791_DAC_Driver;

architecture beh of ACU_AD5791_DAC_Driver is

--
-- Constants declaration
constant CtrlRegAddr            : std_logic_vector(3 downto 0):=x"2";                        -- Write control address
constant DtRegAddr              : std_logic_vector(3 downto 0):=x"1";                        -- Write data address
constant nrSCLK_pulses          : std_logic_vector(7 downto 0):=x"17";                       -- 23d

-- 
-- Signals declaration
signal Enable_s0                : std_logic;                                                 -- Enable sampled signal

signal NewDt2Wr_s0              : std_logic;                                                 -- NewDt2Wr sampled signal
signal NewDt2Wr_RE              : std_logic;                                                 -- NewDt2Wr RE signal

signal SPI_done                 : std_logic;                                                 -- End of a SPI protocol
signal SPI_done_s0              : std_logic;                                                 -- SPI_done sampled signal
signal SPI_done_RE              : std_logic;                                                 -- SPI_done RE signal

signal Ext_SPI_SlC_sx           : std_logic_vector(7 downto 0);                              -- Ext_SPI_SlaveConnected sampling vector

signal StartSPI                 : std_logic;                                                 -- SPI master start signal
signal SPI_Dt2Wr                : std_logic_vector(23 downto 0);                             -- SPI master data to send

signal cfgFlag                  : std_logic;                                                 -- It flags if the DAC is (1) or not (0) configured
signal busy                     : std_logic;                                                 -- It is high during the SPI protocol transmission

-- 		
-- Component declaration
Component ACU_Generic_4lines_SPI_Master is
  Generic (
         gSCLK_High              : unsigned(7 downto 0):="00000011";                         -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0):="00000011";                         -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0):="00000011";                         -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
         gDataWidth              : integer range 0 to 256:= 200;                             -- Data to send width. It has to be lower or equal to 
	                                                                                     -- nrSCLK_pulses signal value.
         gSDIonRE                : std_logic:='0';                                           -- When 0, SDI is sampled on SCLK or SCLK_BK falling edge else on
                                                                                             -- the rising edge
         gSDOonRE                : std_logic:='0';                                           -- When 0, SDO is shifted on SCLK falling edge else on
                                                                                             -- the rising edge
         gUseSCLK_BK             : std_logic:='0';                                           -- When 0, SDI is sampled using SCLK as reference clock else SCLK_BK 
         gSCLK_startLevel        : std_logic:='0'                                            -- it defines the SCLK level after CSn goes low

  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high
	 LostConnection          : in  std_logic;                                            -- When high, the protocol is aborted and the FSM reset
         NewDt_In                : in  std_logic;                                            -- One clock cycle pulse active high when there is a new Dt_In
	                                                                                          -- or when a read action has to start. 
         Dt_In                   : in std_logic_vector(gDataWidth-1 downto 0);               -- Data to send.

         nrSCLK_pulses           : in std_logic_vector (7 downto 0);                         -- Number of SCLK pulses for each NewVal received

         NewDt_Received          : out std_logic;                                            -- One clock cycle pulse active high when there is a new Dt_Received 
         Dt_Received             : out std_logic_vector(gDataWidth-1 downto 0);              -- Data to send.
	 
         SCLK                    : out std_logic;                                            -- SPI clock signal
         SCLK_BK                 : in std_logic;                                             -- SCLK loop back (on the slave chip side) signal
         SCS_n                   : out std_logic;                                            -- Active low SPI Chip Select signal
         SDI                     : in  std_logic;                                            -- Serial SPI data in (to FPGA) signal
         SDO                     : out std_logic                                             -- Serial SPI data out (from FPGA) signal
  );
end component ACU_Generic_4lines_SPI_Master;

begin
--
-- sampling process
p_sampling:process(Clock,Reset)
begin
  if (Reset='1') then
    
    Enable_s0       <= '0';
    
    NewDt2Wr_s0     <= '0';
    NewDt2Wr_RE     <= '0';
    
    SPI_done_s0     <= '0';
    SPI_done_RE     <= '0';
    
    Ext_SPI_SlC_sx  <= (others=>'0');
    
  elsif (Clock'event and Clock='1') then
    
    Enable_s0       <= Enable;

    NewDt2Wr_s0     <= NewDt2Wr;
    NewDt2Wr_RE     <= NewDt2Wr and not NewDt2Wr_s0;
    
    SPI_done_s0     <= SPI_done;
    SPI_done_RE     <= SPI_done and not SPI_done_s0;
    
    Ext_SPI_SlC_sx  <= Ext_SPI_SlC_sx(6 downto 0) & Ext_SPI_SlaveConnected; 
  
  end if;
    
end process p_sampling;

p_DataMngr:process(Clock,Reset)
begin
  if (Reset='1') then
    StartSPI   <= '0';
    SPI_Dt2Wr  <= (others=>'0');
    cfgFlag    <= '0';
    
    busy       <= '0';
  elsif (Clock'event and Clock='1') then
  
    if (StartSPI = '1') then
      busy       <= '1';
    elsif(SPI_done ='1') then
      busy       <= '0';
    end if;
  
  
    StartSPI   <= '0';
    
    if (Enable_s0='1') then
    
      if (busy = '0') then
      
        if (cfgFlag='0') then
          -- DAC configuration
          StartSPI   <= '1';
          SPI_Dt2Wr  <= CtrlRegAddr & "0000000000" & gLinComp & gSDODIS & "0" & gDACTRI & gOPGND & gRBUF & "0";
          cfgFlag    <= '1';
        else
          -- DAC conversion
          --SPI_Dt2Wr  <= DtRegAddr & Dt2Wr;
        
          if (gFreeWheel='1') then
            if (SPI_done_RE='1') then
              StartSPI   <= '1';
              SPI_Dt2Wr  <= DtRegAddr & Dt2Wr;
            end if;
          else
            if (NewDt2Wr_RE='1') then
              StartSPI   <= '1';
              SPI_Dt2Wr  <= DtRegAddr & Dt2Wr;
            end if;
          end if;        
        
        end if;
        
      end if;
      
    else
      cfgFlag    <= '0';
    end if;

--    if (Enable_RE='1') then
--      SPI_Dt2Wr  <= CtrlRegAddr & "0000000000" & gLinComp & gSDODIS & "0" & gDACTRI & gOPGND & gRBUF & "0";
--      StartSPI   <= '1';
--    else
--      if (Enable_s0='1') then
--        if (gFreeWheel='1') then
--          if (SPI_done_RE='1') then
--            SPI_Dt2Wr  <= DtRegAddr & Dt2Wr;
--            StartSPI   <= '1';
--          end if;
--        else
--          if (NewDt2Wr_RE='1') then
--            SPI_Dt2Wr  <= DtRegAddr & Dt2Wr;
--            StartSPI   <= '1';
--          end if;
--        end if;
--      end if;
--    end if;
       
  end if;
end process p_DataMngr;

--
-- driver instance
i_ACU_Generic_4lines_SPI_Master: ACU_Generic_4lines_SPI_Master
  Generic map(
         gSCLK_High              => gSCLK_High,
         gSCLK_Low               => gSCLK_Low,
         
         gCSn2SCLK_SetupTime     => gCSn2SCLK_SetupTime,	 

         gDataWidth              => 24,
         gSDIonRE                => '0',
         gSDOonRE                => '1',
         gUseSCLK_BK             => gUseSCLK_BK,
         gSCLK_startLevel        => '0'

  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         LostConnection          => Ext_SPI_SlC_sx(7),
         NewDt_In                => StartSPI,
	                                                                                    
         Dt_In                   => SPI_Dt2Wr,

         nrSCLK_pulses           => nrSCLK_pulses,

         NewDt_Received          => SPI_done,
         Dt_Received             => open,
	 
         SCLK                    => SCLK,
         SCLK_BK                 => SCLK_BK,
         SCS_n                   => SCS_n,
         SDI                     => '0',
         SDO                     => SDO
  );

--Output
SentProt  <= SPI_done_RE;

end beh;
