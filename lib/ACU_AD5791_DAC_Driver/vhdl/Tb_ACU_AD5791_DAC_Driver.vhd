------------------------------------------------------------------------------------------------------------------------
-- File name: Tb_ACU_AD5791_DAC_Driver.vhd                                                                            --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 26/06/2024                                                                                              --
--                                                                                                                    --
-- Comments : ACU_AD5791_DAC_Driver testbench                                                                         --   
--                                                                                                                    --
-- History  : Start up version 26/06/2024                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity Tb_ACU_AD5791_DAC_Driver is
end Entity Tb_ACU_AD5791_DAC_Driver;

architecture beh of Tb_ACU_AD5791_DAC_Driver is

--
-- Constants declaration

-- 
-- Signals declaration
signal Clock                     : std_logic:='0';
signal Reset                     : std_logic:='1';
signal Enable                    : std_logic:='0';

signal incr                      : std_logic;
signal NewDt2Wr                  : std_logic;
signal Dt2Wr                     : std_logic_vector(19 downto 0);

signal nReset                    : std_logic_vector(0 downto 0);
signal DAC_En                    : std_logic_vector(0 downto 0);

-- 		
-- Component declaration
Component cntCmdInDriven is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       incrCmd          : in std_logic;
       enable           : out std_logic;
       cntOutValid      : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntCmdInDriven;

Component cntFreeWheel is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       enable           : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntFreeWheel;

Component ACU_AD5791_DAC_Driver is
  Generic (
         gFreeWheel              : std_logic:='1';                                           -- When 0, the conversion starts every NewDt2Wr RE pulse otherwise it is done continuosly.
         -- Chip conf
         gLinComp                : std_logic_vector(3 downto 0):=x"0";                       -- Linearity error compensation (See data sheet for more info).
         gSDODIS                 : std_logic:='1';                                           -- SDO pin enable/disable control (See data sheet for more info).
         gDACTRI                 : std_logic:='0';                                           -- DAC tristate control (See data sheet for more info).
         gOPGND                  : std_logic:='0';                                           -- Output ground clamp control (See data sheet for more info).
         gRBUF                   : std_logic:='1';                                           -- Output amplifier configuration control (See data sheet for more info).
         --SPI
         gSCLK_High              : unsigned(7 downto 0):="00000010";                         -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0):="00000010";                         -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0):="00000001";                         -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
         gUseSCLK_BK             : std_logic:='0'                                            -- When 0, SDI is sampled using SCLK as reference clock else SCLK_BK 

  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high
         Enable                  : in  std_logic;                                            -- When high, the driver operates on the SPI lines
         Ext_SPI_SlaveConnected  : in  std_logic;                                            -- It is low when a external slave card is connected to the SPI HUB
         
         NewDt2Wr                : in  std_logic;                                            -- New Data to convert/write pulse.
         Dt2Wr                   : in  std_logic_vector(19 downto 0);                        -- Data to convert/write value.
         	 
         SentProt                : out std_logic;                                            -- Pulse active high every time a protocol is sent.
                                                                                             -- It is used by the main FSM to know when drive the Enable signal.
	 	 
         SCLK                    : out std_logic;                                            -- SPI clock signal
         SCLK_BK                 : in  std_logic;                                            -- SCLK loop back (on the slave chip side) signal
         SCS_n                   : out std_logic;                                            -- Active low SPI Chip Select signal
         SDO                     : out std_logic                                             -- Serial SPI data out (from FPGA) signal
  );
end component ACU_AD5791_DAC_Driver;

Component ACU_DelaySignal is
  Generic(

        gNrClkDelay      : integer range 1 to 16  := 5;                  -- number of clock cycles delay
        gDtWidth         : integer range 1 to 32  := 12                  -- Input data width
	
  );
  Port (
        Clock            : in std_logic;                                 -- Clock signal (usually @100MHz)
        Reset            : in std_logic;                                 -- Asynchronous reset signal active high
        Input2Delay      : in std_logic_vector(gDtWidth-1 downto 0);     -- input signal to delay.
        DelayedOut       : out std_logic_vector(gDtWidth-1 downto 0)     -- Delayed output signal.
  );
End component ACU_DelaySignal;

begin

Clock  <= not Clock after 5 ns;

i_500nsTimebase: cntFreeWheel 
  generic map(
       gCntOutWidth     => 8,
       gEnTrigValue     => 150
  )
  port map(
       clock            => Clock,
       reset            => Reset,
       enable           => incr,
       cntOut           => open    
    );


i_datagen: cntCmdInDriven 
  generic map(
       gCntOutWidth     => 20,
       gEnTrigValue     => 100
    )
  port map(
       clock            => Clock,
       reset            => Reset,
       incrCmd          => incr,
       enable           => open,
       cntOutValid      => NewDt2Wr,
       cntOut           => Dt2Wr
    
    );

nReset(0)  <= not Reset;

i_ACU_DelaySignal: ACU_DelaySignal 
  Generic map(

        gNrClkDelay      => 10,
        gDtWidth         => 1
  )
  Port map(
        Clock            => Clock,
        Reset            => Reset,
        Input2Delay      => nReset,
        DelayedOut       => DAC_En
  );

i_freeWheelDAC: ACU_AD5791_DAC_Driver 
  Generic map (
         gFreeWheel              => '1'
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         Enable                  => DAC_En(0),
         Ext_SPI_SlaveConnected  => '0',
         
         NewDt2Wr                => NewDt2Wr,
         Dt2Wr                   => Dt2Wr,
         	 
         SentProt                => open,
         	 	 
         SCLK                    => open,
         SCLK_BK                 => '0',
         SCS_n                   => open,
         SDO                     => open
  );

i_InputDrivenDAC: ACU_AD5791_DAC_Driver 
  Generic map (
         gFreeWheel              => '0'
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         Enable                  => DAC_En(0),
         Ext_SPI_SlaveConnected  => '0',
         
         NewDt2Wr                => NewDt2Wr,
         Dt2Wr                   => Dt2Wr,
         	 
         SentProt                => open,
         	 	 
         SCLK                    => open,
         SCLK_BK                 => '0',
         SCS_n                   => open,
         SDO                     => open
  );
end beh;
