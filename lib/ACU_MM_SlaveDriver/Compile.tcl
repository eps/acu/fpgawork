###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work
pwd


vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_package_numericLib.vhd  

vcom -93 $path/ACU_MM_SlaveDriver.vhd

vcom -93 $path/Tb_ACU_MM_SlaveDriver.vhd




