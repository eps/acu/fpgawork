 restart -f
 run 50 ns

force -freeze sim:/tb_acu_mm_slavedriver/i_ACU_MM_SlaveDriver/Reset 0 0
run 1 us

# RD
force -freeze sim:/tb_acu_mm_slavedriver/i_ACU_MM_SlaveDriver/Enable 1 0
run 1 us
force -freeze sim:/tb_acu_mm_slavedriver/i_ACU_MM_SlaveDriver/Enable 0 0


# WR
force -freeze sim:/tb_acu_mm_slavedriver/i_ACU_MM_SlaveDriver/WrRdn 1 0
force -freeze sim:/tb_acu_mm_slavedriver/i_ACU_MM_SlaveDriver/Address 01000110 0
force -freeze sim:/tb_acu_mm_slavedriver/i_ACU_MM_SlaveDriver/Dt2Wr 00001010111111100100110110101101 0
run 1 us
force -freeze sim:/tb_acu_mm_slavedriver/i_ACU_MM_SlaveDriver/Enable 1 0
run 1 us
