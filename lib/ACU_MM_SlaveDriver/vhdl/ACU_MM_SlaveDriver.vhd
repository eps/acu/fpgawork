------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_MM_SlaveDriver.vhd                                                                                  --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 17/08/2023                                                                                              --
--                                                                                                                    --
-- Comments : This module generates a memory mapped read/write action on the avalone bus.                             --
--            The action is triggered by the Enable signal rising edge.                                               --
--            Via genreic it is possible to define the address and the data width.				      --
--            Read:                                                                                                   --
--                    ____     ____     ____     ____                                                                 --
--  Clock          __|   |____|   |____|   |____|   |____                                                             --
--                             __________________                                                                     --
--  mmRd           ___________|                 |________                                                             --
--  mmAddr                    X      Addr0                                                                            --
--  mmRdDt                             X   Dt0  X                                                                     --
--  DtFromMM_Slave                              X    Dt0                                                              --
--                                                                                                                    --
--            Write:                                                                                                  --
--                    ____     ____     ____     ____                                                                 --
--  Clock          __|   |____|   |____|   |____|   |____                                                             --
--                             ________                                                                               --
--  mmWr           ___________|       |________                                                                       --
--  mmAddr                    X      Addr0                                                                            --
--  mmWrDt                    X   Dt0                                                                                 --
--                                                                                                                    --
-- History  : Start up version 17/08/2023                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


Entity ACU_MM_SlaveDriver is
  Generic(
    gAddrWidth         : integer:=8;                                  -- Address width
    gDataWidth         : integer:=32                                  -- Data width       
                                                                 
  );
  Port(
    Reset              : in std_logic;                                -- Asynchronous reset active high
    Clock              : in std_logic;                                -- System clock
    
    Enable             : in std_logic;                                -- The Enable rising edge triggers the driver action on the bus
    
    WrRdn              : in std_logic;                                -- 0=> Read action configured
                                                                      -- 1=> Write action configured
    Address            : in std_logic_vector(gAddrWidth-1 downto 0);  -- Address configured
    Dt2Wr              : in std_logic_vector(gDataWidth-1 downto 0);  -- Data to write configured
    DtFromMM_Slave     : out std_logic_vector(gDataWidth-1 downto 0); -- Sampled mmRdDt
    
    -- Avalone MM Slave bus interface
    mmRd               : out std_logic;                               -- Avalone MM Slave Read enable
    mmWr               : out std_logic;                               -- Avalone MM Slave Write enable
    mmWrDt             : out std_logic_vector(gDataWidth-1 downto 0); -- Avalone MM Slave data to write
    mmRdDt             : in std_logic_vector(gDataWidth-1 downto 0);  -- Avalone MM Slave data read
    mmAddr             : out std_logic_vector(gAddrWidth-1 downto 0)  -- Avalone MM Slave address
  );
end Entity ACU_MM_SlaveDriver;

architecture beh of ACU_MM_SlaveDriver is

--
-- Constants declaration
constant TcCnt    : unsigned(1 downto 0) := "11";

--
-- Signals declaration
signal Enable_s0  : std_logic;   -- samples Enable signal
signal Enable_s1  : std_logic;   -- samples Enable_s1 signal
signal Enable_RE  : std_logic;   -- Enable signal rising edge
signal cnt        : unsigned(1 downto 0);  -- internal counter


begin

p_inputSampling: process(Clock, Reset)
begin
  if (Reset='1') then
    Enable_s0  <='0';
    Enable_s1  <='0';
    Enable_RE  <='0';
    cnt        <= (others=>'0');  
  elsif(Clock'event and Clock='1') then
    Enable_s0  <=Enable;
    Enable_s1  <=Enable_s0;
    Enable_RE  <=Enable_s0 and not Enable_s1;
    
    if (Enable_RE='1') then
      cnt        <= (others=>'0');
    else
      if (cnt< TcCnt) then
        cnt  <= cnt+1;
      end if;
    end if;  
  
  end if;
end process p_inputSampling;

--output
p_out:process(Clock, Reset)
begin
  if (Reset='1') then
    mmRd            <= '1';
    mmWr            <= '1';
    mmWrDt          <= (others=>'0');
    --mmRdDt             : in std_logic_vector(gDataWidth-1 downto 0);  -- Avalone MM Slave data read
    mmAddr          <= (others=>'0');
    DtFromMM_Slave  <= (others=>'0');  
  elsif(Clock'event and Clock='1') then
    mmAddr          <= Address;
    mmWrDt          <= Dt2Wr;
    
    -- sample the data from bus
    if (WrRdn='0' and cnt="10") then
      DtFromMM_Slave  <= mmRdDt;
    end if;
    
    -- generate the bus commands
    mmRd  <= '0';
    if (WrRdn='0' and cnt<"10") then
      mmRd  <= '1';
    end if;
    
    mmWr  <= '0';
    if (WrRdn='1' and cnt<"01") then
      mmWr  <= '1';
    end if;
    
  end if;
end process p_out;

end beh;
