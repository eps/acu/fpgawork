------------------------------------------------------------------------------------------------------------------------
-- File name: Tb_ACU_MM_SlaveDriver.vhd                                                                               --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 18/08/2023                                                                                              --
--                                                                                                                    --
-- Comments : ACU_MM_SlaveDriver test bench file.                                                                     --
--                                                                                                                    --
-- History  : Start up version 18/08/2023                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

USE work.ACU_package_numericLib.all;

Entity Tb_ACU_MM_SlaveDriver is
end Entity Tb_ACU_MM_SlaveDriver;

architecture beh of Tb_ACU_MM_SlaveDriver is

--
-- Constants declaration

--
-- Signals declaration
signal Clock      : std_logic:='0';   
signal Reset      : std_logic:='1';
signal Enable     : std_logic:='0';   
signal WrRdn      : std_logic:='0';   
signal Address    : std_logic_vector(7 downto 0):=x"00";
signal Dt2Wr      : std_logic_vector(31 downto 0):=x"CAFE4DAD";
signal EmulmmRdDt : array32bits(3 downto 0):=(x"12345678",x"4658CAFE",x"DEADBEEF",x"ABCDEF65");
--
-- Component declaration
Component ACU_MM_SlaveDriver is
  Generic(
    gAddrWidth         : integer:=8;                                  -- Address width
    gDataWidth         : integer:=32                                  -- Data width       
                                                                 
  );
  Port(
    Reset              : in std_logic;                                -- Asynchronous reset active high
    Clock              : in std_logic;                                -- System clock
    
    Enable             : in std_logic;                                -- The Enable rising edge triggers the driver action on the bus
    
    WrRdn              : in std_logic;                                -- 0=> Read action configured
                                                                      -- 1=> Write action configured
    Address            : in std_logic_vector(gAddrWidth-1 downto 0);  -- Address configured
    Dt2Wr              : in std_logic_vector(gDataWidth-1 downto 0);  -- Data to write configured
    DtFromMM_Slave     : out std_logic_vector(gDataWidth-1 downto 0); -- Sampled mmRdDt
    
    -- Avalone MM Slave bus interface
    mmRd               : out std_logic;                               -- Avalone MM Slave Read enable
    mmWr               : out std_logic;                               -- Avalone MM Slave Write enable
    mmWrDt             : out std_logic_vector(gDataWidth-1 downto 0); -- Avalone MM Slave data to write
    mmRdDt             : in std_logic_vector(gDataWidth-1 downto 0);  -- Avalone MM Slave data read
    mmAddr             : out std_logic_vector(gAddrWidth-1 downto 0)  -- Avalone MM Slave address
  );
end component ACU_MM_SlaveDriver;

begin

Clock  <= not Clock after 5 ns;

i_ACU_MM_SlaveDriver: ACU_MM_SlaveDriver

  Port map(
    Reset              => Reset,
    Clock              => Clock,
    
    Enable             => Enable,
    
    WrRdn              => WrRdn,
    
    Address            => Address,
    Dt2Wr              => Dt2Wr,
    DtFromMM_Slave     => open,
    
    -- Avalone MM Slave bus interface
    mmRd               => open,
    mmWr               => open,
    mmWrDt             => open,
    mmRdDt             => EmulmmRdDt(0),
    mmAddr             => open
  );
  
p_emulmmRdDt: process(Reset,Clock)
begin
  if (Reset='1') then
    EmulmmRdDt  <= (x"12345678",x"4658CAFE",x"DEADBEEF",x"ABCDEF65");
  elsif(Clock'event and Clock='1') then
    EmulmmRdDt  <= EmulmmRdDt(2 downto 0) & EmulmmRdDt(3);
  end if;
end process p_emulmmRdDt;

end beh;
