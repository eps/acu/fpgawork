------------------------------------------------------------------------------------------------------
-- File name: Tb_ACU_AvalonMMM_USI_Bridge.vhd                                                       --
--                                                                                                  --
-- Author   : D.Rodomonti                                                                           --
-- EMail    : d.rodomonti@gsi.de                                                                    --
-- Date     : 19/08/2021                                                                            --
--                                                                                                  --
-- Comments : ACU_AvalonMMM_USI_Bridge test bench file.                                             --
--            The HPS is emulated with two AvalonMM master ports (one for the RAM and one for the   --
--            fifo), while the USI interface is driven by an ACU_mUSIC emulator.                    --             
--                                                                                                  --
-- History  : V1.0 DR => Start up version 19.08.2021                                                --
------------------------------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

USE work.ACU_package_numericLib.all;

Entity Tb_ACU_AvalonMMM_USI_Bridge is
End entity Tb_ACU_AvalonMMM_USI_Bridge;

Architecture beh of Tb_ACU_AvalonMMM_USI_Bridge is

--
-- Constant declaration

--
-- Signals declaration
signal ClockFPGA                 : std_logic:='0';
signal ClockHPS                  : std_logic:='0';
signal Reset                     : std_logic:='1';
signal nReset                    : std_logic;
signal USI_itf_Address           : std_logic_vector(7 downto 0):=x"FF";
signal USI_itf_DataEn            : std_logic:='0';
signal USI_itf_RWn               : std_logic:='1';
signal USI_itf_Data              : std_logic_vector(7 downto 0);
signal RAM_Addr                  : std_logic_vector(9 downto 0);
signal RAM_WrCmd                 : std_logic;
signal RAM_WrData                : std_logic_vector(31 downto 0);
signal RAM_RdData                : std_logic_vector(31 downto 0);
signal FIFO_WrCmd                : std_logic;
signal FIFO_WrData               : std_logic_vector(31 downto 0);
signal FPGA_DtFtchdEn            : std_logic:='0';
--signal FPGA_NewDtFtchd           : std_logic:='0';
signal FPGA_DataFetched          : std_logic_vector(31 downto 0):=(others=>'0');


signal mUSIC_Emul_FSPnr          : std_logic_vector (7 downto 0):=(others=>'0');
signal mUSIC_Emul_FSP_RdWrn      : std_logic:='1';
signal mUSIC_Emul_FSP_Dt2Wr      : std_logic_vector(2047 downto 0):=(others=>'0');
signal RemainingBytes            : std_logic_vector(11 downto 0);
signal mUSIC_Emul_FSP_Start      : std_logic:='0';

signal HPS_FIFO_rdreq            : std_logic:='0';
signal HPS_FIFO_data             : std_logic_vector(31 downto 0);

signal HPS_RAM_Addr              : std_logic_vector(9 downto 0);
signal HPS_RAM_RdData            : std_logic_vector(31 downto 0);
signal HPS_RAM_WrData            : std_logic_vector(31 downto 0);
signal HPS_RAM_WrCmd             : std_logic:='0';
 
signal Incr_1M                   : std_logic;
signal NewDt1M                   : std_logic;
signal Dt1M                      : std_logic_vector(31 downto 0);


--
-- Components declaration
Component ACU_AvalonMMM_USI_Bridge is
  Generic (
    gNrFSPs              : integer range 127   downto 1 := 5;         -- Number of output FSPs(registers) emulated inside the bridge
    gMaxFSPDepth         : integer range 4096  downto 1 := 256        -- It is used to define the file chunk size transferred from
                                                                      -- USI to HPS (in Byte)
  );
  Port (
    Clock                : in std_logic;                              -- Clock signal (usually @100MHz)
    Reset                : in std_logic;                              -- Asynchronous reset signal active high

    -- USI interface
    Address              : in  std_logic_vector (7 downto 0);         -- Register address
    DataEn               : in  std_logic;                             -- Data enable signal (aka ClockData)
    RWn                  : in  std_logic;                             -- It reports if a register has to be read (1) or written (0)                       -- Data shift out command
    Data                 : inout std_logic_vector (7 downto 0);       -- Data bus to USI driver.
    RemainingBytes       : out std_logic_vector (11 downto 0);        -- This counter reports the Data transmission between the USI 
                                                                      -- driver and the FSP (register) instance.
    Available            : out std_logic_vector(gNrFSPs-1 downto 0);  -- It reports the FSP instance to the USI driver.                                                                         --ist
    ReadOnly             : out std_logic_vector(gNrFSPs-1 downto 0);  -- It is 1 when the selected FSP is a read only one. 
                                                                      -- NOTE:In this module there are no Read only FSPs(registers)! 

    -- RAM interface (AMM_M)
    RAM_Addr             : out std_logic_vector(9 downto 0);          -- RAM address
    RAM_WrCmd            : out std_logic;                             -- RAM write command (active high)
    RAM_WrData           : out std_logic_vector(31 downto 0);         -- RAM data to write
    RAM_RdData           : in  std_logic_vector(31 downto 0);         -- RAM read data. It has 2 clock cycles latency

    -- FIFO interface (AMM_M)
    FIFO_WrCmd           : out std_logic;                             -- Command to push in data
    FIFO_WrData          : out std_logic_vector(31 downto 0);         -- FIFO data to write

    -- Misc
    FPGA_DtFtchdEn       : in std_logic;                              -- Enable the FPGA_DataFetched data to be sent to the FIFO.
    FPGA_NewDtFtchd      : in std_logic;                              -- New FPGA_DataFetched available (pulse active high).
    FPGA_DataFetched     : in std_logic_vector(31 downto 0)           -- Data from FPGA to HPS (through FIFO)

  );
End component ACU_AvalonMMM_USI_Bridge;



Component mUSIC_Emulator is
  Generic (
    gTimeBetween2DataEn  : integer range 256 downto 3 :=10;           -- It defines the DataEn generation period
    gMaxFSPDepth         : integer range 4096  downto 1 := 256        -- It is used to define the file chunk size transferred from
                                                                      -- USI to HPS (in Byte)
  );
  Port (
    Clock                : in std_logic;                              -- Clock signal (usually @100MHz)
    Reset                : in std_logic;                              -- Asynchronous reset signal active high
    
    -- Emulator driver signals
    FSPnr                : in  std_logic_vector (7 downto 0);         -- FSP number to select
    FSP_RdWrn            : in  std_logic;                             -- Read or write action flag
    FSP_Dt2Wr            : in  std_logic_vector((8*gMaxFSPDepth)-1 downto 0); -- Data to write in to the selected FSP
    RemainingBytes       : in  std_logic_vector (11 downto 0);        -- This counter reports the Data transmission between the USI 
    FSP_Start            : in  std_logic;                             -- Emulator start signal
    FSP_NewDtRead        : out std_logic;                             -- New FSP_DtRead pulse (active high)
    FSP_DtRead           : out std_logic_vector((8*gMaxFSPDepth)-1 downto 0); -- Data read from the selected FSP
    -- USI interface
    Address              : out std_logic_vector (7 downto 0);         -- Register address
    DataEn               : out std_logic;                             -- Data enable signal (aka ClockData)
    RWn                  : out std_logic;                             -- It reports if a register has to be read (1) or written (0)                       -- Data shift out command
    Data                 : inout std_logic_vector (7 downto 0)       -- Data bus to USI driver.

  );
End component mUSIC_Emulator;

Component FSP_Output is                                                 -- Entity - Blockbeschreibung Ein- und Ausgaenge
   generic 
   (
      gFSPName                : string  :="FSP Name";                   -- Manueller Eintrag des FSP Namen
      gFSPNumber              : integer range 16#FF# downto 1  := 1;    -- Nummer des FSP im Verbund (Konvergenz mit 'Address')
      gFSPDepth               : integer range 16#FFF# downto 0 := 3;    -- Tiefe des FSP als Vielfaches von Byte
      gUseInputFromPeripheral : std_logic := '0';                       -- Wenn '0' werden 'InputFromPeripheral' und 'LoadDataFromPeripheral' ignoriert
      gReset                  : std_logic_vector := X"000000"           -- Standard-Ausgabewert fuer 'OutputToPeripheral' nach dem 'Reset'
   );
----------------------------------------------------------------
   port 
   (
      Clock                      : in     std_logic;
      Reset                      : in     std_logic;

      Address                    : in     std_logic_vector (7 downto 0);   -- korreliert die angelegte Adresse mit gFSPNumber wird FSP aktiviert
      Data                       : inout  std_logic_vector (7 downto 0);   -- Daten von/zum 'FSPInterconnectionHandler'
      RWn                        : in     std_logic;                       -- '1'=FSP gibt seine Daten an FSP-Handler, '0'=FSP erwartet Daten vom FSP-Handler 
      CRCOK                      : in     std_logic;                       -- Wenn '1' ist die Datenpruefsumme OK, Daten an OutputToPeripheral

      InputFromPeripheral        : in     std_logic_vector (((gFSPDepth * 8)-1) downto 0); -- Port von der Peripherie
      LoadDataFromPeripheral     : in     std_logic;                       -- Ladeeingang -> Highflanke laedt Daten von InputFromPeripheral

      NewDataAvailable           : out    std_logic;                       -- '1'=neue Daten im FSP
      OutputToPeripheral         : out    std_logic_vector (((gFSPDepth * 8)-1) downto 0); -- Port zur Peripherie

      RemainingBytes             : out    std_logic_vector (11 downto 0);  -- Anzahl noch vorhandener Bytes beim lesen, bzw. noch schreibbarer Bytes
      Available                  : out    std_logic;                       -- Wenn '1' signalisiert dies dem 'ICH', dass dieses FSP vorhanden ist
      ReadOnly                   : out    std_logic;                       -- Bleibt '0' wenn FSP selektiert wird, da dieser les- und schreibbar ist

      ClockData                  : in     std_logic                        -- Taktet einzelne Bytes in/aus den/dem FSP (Abh. von RWn)
   );
end  component FSP_Output;

Component FIFO32b16wDual IS
	PORT
	(
		aclr		: IN STD_LOGIC  := '0';
		data		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		rdclk		: IN STD_LOGIC ;
		rdreq		: IN STD_LOGIC ;
		wrclk		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		rdempty		: OUT STD_LOGIC ;
		rdfull		: OUT STD_LOGIC ;
		rdusedw		: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		wrempty		: OUT STD_LOGIC ;
		wrfull		: OUT STD_LOGIC ;
		wrusedw		: OUT STD_LOGIC_VECTOR (3 DOWNTO 0)
	);
END component FIFO32b16wDual;

--Component RAM32b1024wDual IS
--	PORT
--	(
--		aclr_a		: IN STD_LOGIC  := '0';
--		aclr_b		: IN STD_LOGIC  := '0';
--		address_a		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
--		address_b		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
--		clock_a		: IN STD_LOGIC  := '1';
--		clock_b		: IN STD_LOGIC ;
--		data_a		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
--		data_b		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
--		wren_a		: IN STD_LOGIC  := '0';
--		wren_b		: IN STD_LOGIC  := '0';
--		q_a		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
--		q_b		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
--	);
--END component RAM32b1024wDual;

Component cntCmdInDriven is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       incrCmd          : in std_logic;
       enable           : out std_logic;
       cntOutValid      : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntCmdInDriven;

Component cntFreeWheel is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       enable           : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntFreeWheel;

begin

ClockFPGA <= not ClockFPGA after 5 ns;                                        -- 100 MHz ClockFPGA
ClockHPS  <= not ClockHPS after 10 ns;                                        -- 50 MHz ClockHPS
nReset    <= not Reset;

--------------------------------------------------------------------------------------------------
-- FPGA side DATA emulator (to FIFO)
i_cntFreeWheel_1MHz: cntFreeWheel
  generic map(
       gCntOutWidth     => 16,
       gEnTrigValue     => 50
    )
  port map(
       clock            => ClockFPGA,
       reset            => Reset,
       enable           => Incr_1M,
       cntOut           => open
    
 );

i_cntCmdInDriven: cntCmdInDriven
  generic map (
       gCntOutWidth     => 32,
       gEnTrigValue     => 1000
    )
  port map(
       clock            => ClockFPGA,
       reset            => Reset,
       incrCmd          => Incr_1M,
       enable           => open,
       cntOutValid      => NewDt1M,
       cntOut           => Dt1M
    
    );
--------------------------------------------------------------------------------------------------

--i_mUSIC_Emulator: mUSIC_Emulator
--  Port map(
--    Clock                => ClockFPGA,
--    Reset                => Reset,
    
--    -- Emulator driver signals
--    FSPnr                => mUSIC_Emul_FSPnr,
--    FSP_RdWrn            => mUSIC_Emul_FSP_RdWrn,
--    FSP_Dt2Wr            => mUSIC_Emul_FSP_Dt2Wr,
--    RemainingBytes       => RemainingBytes,
--    FSP_Start            => mUSIC_Emul_FSP_Start,
--    FSP_NewDtRead        => open,
--    FSP_DtRead           => open,
--    -- USI interface
--    Address              => USI_itf_Address,
--    DataEn               => USI_itf_DataEn,
--    RWn                  => USI_itf_RWn,
--    Data                 => USI_itf_Data
--  );
USI_itf_Address  <=(others=>'0');
USI_itf_DataEn   <='0';
USI_itf_RWn      <='1';
--
-- The FSP instance below is used to compare the ACU_AvalonMMM_USI_Bridge USI interface with an "official" one
-- FSP1 3 B


i_FSP_Output: FSP_Output                 
   Generic map(
      gReset                     => x"F1CA46"
   )                                
   Port map 
   (
      Clock                      => ClockFPGA,
      Reset                      => Reset,

      Address                    => USI_itf_Address,
      Data                       => USI_itf_Data,
      RWn                        => USI_itf_RWn,
      CRCOK                      => '1',

      InputFromPeripheral        => (others=>'0'),
      LoadDataFromPeripheral     => '0', 

      NewDataAvailable           => open,
      OutputToPeripheral         => open,

      RemainingBytes             => RemainingBytes,
      Available                  => open,
      ReadOnly                   => open,

      ClockData                  => USI_itf_DataEn
   );
   
   
   FPGA_DataFetched  <= Dt1M;

i_ACU_AvalonMMM_USI_Bridge: ACU_AvalonMMM_USI_Bridge 

  Port map(
    Clock                => ClockFPGA,
    Reset                => Reset,

    -- USI interface
    Address              => USI_itf_Address,
    DataEn               => USI_itf_DataEn,
    RWn                  => USI_itf_RWn,
    Data                 => USI_itf_Data,
    RemainingBytes       => RemainingBytes,
    Available            => open,
    ReadOnly             => open,

    -- RAM interface (AMM_M)
    RAM_Addr             => RAM_Addr,
    RAM_WrCmd            => RAM_WrCmd,
    RAM_WrData           => RAM_WrData,
    RAM_RdData           => RAM_RdData,

    -- FIFO interface (AMM_M)
    FIFO_WrCmd           => FIFO_WrCmd,
    FIFO_WrData          => FIFO_WrData,

    -- Misc
    FPGA_DtFtchdEn       => FPGA_DtFtchdEn,
    FPGA_NewDtFtchd      => NewDt1M,
    FPGA_DataFetched     => FPGA_DataFetched


  );

i_FIFO32b16wDual: FIFO32b16wDual 
	PORT map
	(
		aclr		=> Reset,
		data		=> FIFO_WrData,
		rdclk		=> ClockHPS,
		rdreq		=> HPS_FIFO_rdreq,
		wrclk		=> ClockFPGA,
		wrreq		=> FIFO_WrCmd,
		q		=> HPS_FIFO_data,
		rdempty	=> open,
		rdfull		=> open,
		rdusedw	=> open,
		wrempty	=> open,
		wrfull		=> open,
		wrusedw	=> open
	);

--i_RAM32b1024wDual: RAM32b1024wDual 
--	PORT map
--	(
--		aclr_a		=> Reset,
--		aclr_b		=> Reset,
--		address_a	=> RAM_Addr,
--		address_b	=> HPS_RAM_Addr,
--		clock_a	=> ClockFPGA,
--		clock_b	=> ClockHPS,
--		data_a		=> RAM_WrData,
--		data_b		=> HPS_RAM_WrData,
--		wren_a		=> RAM_WrCmd,
--		wren_b		=> HPS_RAM_WrCmd,
--		q_a		=> RAM_RdData,
--		q_b		=> HPS_RAM_RdData
--	);


end beh;

