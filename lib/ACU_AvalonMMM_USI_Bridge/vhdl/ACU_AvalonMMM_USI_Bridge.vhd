------------------------------------------------------------------------------------------------------
-- File name: ACU_AvalonMMM_USI_Bridge.vhd                                                          --
--                                                                                                  --
-- Author   : D.Rodomonti                                                                           --
-- EMail    : d.rodomonti@gsi.de                                                                    --
-- Date     : 13/08/2021                                                                            --
--                                                                                                  --
-- Comments : This module is a bridge between the Avalon Memory Mapped Master and the USI protocol. --
--            The goal is to share an on chip RAM and FIFO between two entities that communicate    --
--            with two different protocols (Avalon MM and USI).                                     --
--            The RAM (1024 Word 32 b) is used as common space for the HPS and the USI master to    --
--            exchange commands and status information, while the FIFO (64 Word 32b) is used to     --
--            transfer data from the FPGA to the HPS side.                                          --
--            In case in the future it will be necessary to send data from the HPS to the FPGA side,--
--            another FIFO instance can be used.                                                    --
--            From the USI master device point of view, this bridge can be considered as a          --
--            collection of Output FSPs that can be read and written.                               --
--            Both RAM and FIFO are double port devices and, in oder to avoid conflicts,            --
--            the RAM locations can be written in an exclusive way by the HSP and the USI master.   --
--            The read action is not managed as the write one because in the worst case a master    --
--            will fetch an old status value.                                                       -- 
--            The data transferred between the FIFO and the HSP through the FIFO can be selected    --
--            between data fetched from the FPGA (ADC values) or data coming from the USI (rbf file --
--            or calibration results).                                                              --
--                                                                                                  --
--            RAM driver behavior: the USI interface address signal is compared with a register     --
--            address list hard coded (through a constant array) in the module. As soon as there is --
--            a match, the relative RAM location is selected. Depending of the action to perform    --
--            on that location (read or write), the following steps will be performed:              --
--              RD => Status information from HPS to USI => the selected RAM location is read and   --
--                    the four Byte data are shifted out every USI interface DataEn rising edge.    --
--              WR => Commands and configurations from USI to HPS => The USI interface Data signal  --
--                    is sampled every USI interface DataEn rising edge and temporary stored in     --
--                    a register. As soon as all 4 Bytes are received, the temporary register is    --
--                    written in the RAM. In case the register selected has a size higher than 4B,  --
--                    another strategy has to be adopted. First of all this means that the data have--
--                    not to be sent to the RAM but to the FIFO. Because the FIFO is 32 bits wide,  --
--                    the bridge will collect on a temporary register 4 USI interface Data          --
--                    transmissions before pushing them into the FIFO.                              --
--                                                                                                  --
--           FIFO driver behavior: one possible FIFO driver functionality was already mentioned     --
--           above, but on the FIFO can be pushed in also data fetched on the FPGA side. It is      --
--           important that those FPGA data are synchronous to an "enable" signal that report to    --
--           the bridge that a new value has to be transferred.                                     -- 
--           NOTE: In case one FSP is used for the data transfer between USI and HPS, it has to be, --
--                 in size, multiple of 4B.                                                         --             
--                                                                                                  --
-- History  : V1.0 DR => Start up version 13.08.2021                                                --
------------------------------------------------------------------------------------------------------
--            V1.1 DR => Changed the FIFO_WrData length from 64 to 32 bit.                          --
--                       The 64 bit input signal is written in the FIFO with two 32 bit write data  --
--                       actions. 09.12.2022                                                        --
------------------------------------------------------------------------------------------------------
--            V1.2 DR => Changed the FPGA_DataFetched length from 64 to 32 bit. 18.08.2023          --
------------------------------------------------------------------------------------------------------
--            V1.3 DR => Changed the FSP/memory mapping (from 70-74 to 22-26).  19.02.2025          --
------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

USE work.ACU_package_numericLib.all;


Entity ACU_AvalonMMM_USI_Bridge is
  Generic (
    gNrFSPs              : integer range 127   downto 1 := 5;         -- Number of output FSPs(registers) emulated inside the bridge
    gMaxFSPDepth         : integer range 4096  downto 1 := 256        -- It is used to define the file chunk size transferred from
                                                                      -- USI to HPS (in Byte)
  );
  Port (
    Clock                : in std_logic;                              -- Clock signal (usually @100MHz)
    Reset                : in std_logic;                              -- Asynchronous reset signal active high

    -- USI interface
    Address              : in  std_logic_vector (7 downto 0);         -- Register address
    DataEn               : in  std_logic;                             -- Data enable signal (aka ClockData)
    RWn                  : in  std_logic;                             -- It reports if a register has to be read (1) or written (0)
    Data                 : inout std_logic_vector (7 downto 0);       -- Data bus to USI driver.
    RemainingBytes       : out std_logic_vector (11 downto 0);        -- This counter reports the Data transmission between the USI 
                                                                      -- driver and the FSP (register) instance.
    Available            : out std_logic_vector(gNrFSPs-1 downto 0);  -- It reports the FSP instance to the USI driver.             
    ReadOnly             : out std_logic_vector(gNrFSPs-1 downto 0);  -- It is 1 when the selected FSP is a read only one. 
                                                                      -- NOTE:In this module there are no Read only FSPs(registers)! 

    -- RAM interface (AMM_M)
    RAM_Addr             : out std_logic_vector(9 downto 0);          -- RAM address
    RAM_WrCmd            : out std_logic;                             -- RAM write command (active high)
    RAM_WrData           : out std_logic_vector(31 downto 0);         -- RAM data to write
    RAM_RdData           : in  std_logic_vector(31 downto 0);         -- RAM read data. It has 2 clock cycles latency

    -- FIFO interface (AMM_M)
    FIFO_WrCmd           : out std_logic;                             -- Command to push in data
    FIFO_WrData          : out std_logic_vector(31 downto 0);         -- FIFO data to write

    -- Misc
    FPGA_DtFtchdEn       : in std_logic;                              -- Enable the FPGA_DataFetched data to be sent to the FIFO.
    FPGA_NewDtFtchd      : in std_logic;                              -- New FPGA_DataFetched available (pulse active high).
    FPGA_DataFetched     : in std_logic_vector(31 downto 0)           -- Data from FPGA to HPS (through FIFO)


  );
End entity ACU_AvalonMMM_USI_Bridge;

Architecture beh of ACU_AvalonMMM_USI_Bridge is
--
-- Constant declaration

-- Note!!: The matrix below represents the correlation between the FSPs and the RAM locations.
--         Please check first still available FSPs on your design and than adapt the list accordingly.
-- addr 0x000 => USI2HPS command location => FPGA2HPS data transfer commands and configurations:
--               [31..28] File type (rbf,txt,...)
--               [27..26] Unused
--               [25]     Ready to send data from FPGA 2 HPS (through FIFO)????? maybe not necessary ????
--               [24]     Generate a file
--               [23..0]  File size
-- addr 0x001 => HPS2USI Status info location => FPGA2HPS data transfer status:
--               [31..3]  Unused
--               [2]      File saved
--               [1]      Data received
--               [0]      Ready to receive data
-- addr 0x002 => USI2HPS command location => FPGA reconfiguration command:
--               [31..2]  Unused
--               [1]      Reconfigure FPGA with Factory image (Debug only!!)
--               [0]      Reconfigure FPGA
-- addr 0x003 => HPS2USI Status info location => FPGA reconfiguration status:
--               [31..4]  Unused
--               [3..0]   FPGA configuration status
-- addr 0x004 => USI2HPS Data transfer => It is equivalent to a "SerialFSPOut". The file chunk size is defined by gMaxFSPDepth. 
--                                                              FSP nr, FSP Depth
constant addrList   :arrayHPS_2_USI(gNrFSPs-1 downto 0):= (  (x"01A",std_logic_vector(to_unsigned(gMaxFSPDepth,12)) ),  -- FSP26 
                                                             (x"019",x"004"),         -- FSP25  
                                                             (x"018",x"004"),         -- FSP24  
                                                             (x"017",x"004"),         -- FSP23  
                                                             (x"016",x"004"));        -- FSP22

constant RAM_locSize       : unsigned(11 downto 0):=to_unsigned(4,12);            -- RAM location size is 4 B
--
-- Signals declaration
signal Address_s0          : std_logic_vector (7 downto 0);                       -- Address sampled signal
signal Address_s1          : std_logic_vector (7 downto 0);                       -- Address_s0 sampled signal
signal RWn_s0              : std_logic;                                           -- RWn sampled signal
signal cntGeneric          : unsigned (11 downto 0);                              -- generic counter used to generate the RemainingBytes signal
signal Tc_cntGeneric       : unsigned (11 downto 0);                              -- cntGeneric terminal counter
signal addrRAM             : std_logic_vector(9 downto 0);                        -- RAM address
signal DataEn_s0           : std_logic;                                           -- DataEn sampled signal
signal DataEn_s1           : std_logic;                                           -- DataEn_s0 sampled signal
signal DataEn_RE           : std_logic;                                           -- DataEn rising edge
signal foundFlag           : std_logic;                                           -- Active high when the FSP sent is one inside the addrList
signal RAM_DtOut           : std_logic_vector (31 downto 0);                      -- RAM read data
signal Avail               : std_logic_vector(gNrFSPs-1 downto 0);                -- Internal Available signal
signal tmpReg              : std_logic_vector(31 downto 0);                       -- Temporary register where the Bytes from/to HPS
                                                                                  -- are stored
--signal FPGA_NewDtFtchd_s0  : std_logic;                                           -- FPGA_NewDtFtchd sampled signal
--signal SplitFlag           : std_logic;                                           -- It is a flag used to slit the incoming 64 signal 
--                                                                                  -- in two 32 fifo write actions

type fsmState is (WaitAddress, CheckAddrList, WaitShiftOutCmd, CheckNrShift);
signal fsm_state                : fsmState;

begin

-- Input sampling
p_inSamp: process (Reset,Clock)
begin
  if (Reset = '1') then
    Address_s0    <= (others => '0');
    Address_s1    <= (others => '0');
    DataEn_s0  <= '0';
    DataEn_s1  <= '0';
    DataEn_RE  <= '0';
  elsif (Clock'event and Clock='1') then
    Address_s0    <= Address;
    Address_s1    <= Address_s0;
    DataEn_s0     <= DataEn;
    DataEn_s1     <= DataEn_s0;
    DataEn_RE     <= DataEn_s0 and not DataEn_s1;
  end if;
end process;

-- FSM
p_FSM: process(Reset,Clock)
begin
  if (Reset = '1') then
    fsm_state           <= WaitAddress;

    cntGeneric          <= (others=>'0');
    RAM_WrCmd           <= '0';
    FIFO_WrCmd          <= '0';
    addrRAM             <= (others=>'0');
    Avail               <= (others=>'0');
    --RO             <= (others=>'0');
    foundFlag           <= '0';
    Tc_cntGeneric       <= (others=>'0');
    RWn_s0              <= '0';
    tmpReg              <= (others=>'0');
    FIFO_WrData         <= (others => '0');
    
    --SplitFlag           <= '0';
    --FPGA_NewDtFtchd_s0  <= '0';

  elsif (Clock'event and Clock='1') then
    
    FIFO_WrCmd          <= '0';
    --FPGA_NewDtFtchd_s0  <= FPGA_NewDtFtchd;

    if (FPGA_DtFtchdEn ='0') then
      -- FIFO data coming from USI
      FIFO_WrData   <= tmpReg;
    else
      -- FIFO data coming from FPGA module/s
      FIFO_WrCmd    <= FPGA_NewDtFtchd;
      FIFO_WrData   <= FPGA_DataFetched;
           
--      if (FPGA_NewDtFtchd = '1' and SplitFlag = '0') then
--        SplitFlag     <= '1';
--        FIFO_WrCmd    <= FPGA_NewDtFtchd;
--        FIFO_WrData   <= FPGA_DataFetched(63 downto 32);
--      elsif(SplitFlag = '1') then
--        SplitFlag     <= '0';
--        FIFO_WrCmd    <= FPGA_NewDtFtchd_s0;
--        FIFO_WrData   <= FPGA_DataFetched(31 downto 0);     
--      end if;
    end if;

    case fsm_state is

      when WaitAddress =>

        if (Address_s1 /= Address_s0) then
          fsm_state  <= CheckAddrList;

          foundFlag  <= '0';
          Avail      <= (others=>'0');
          --RO         <= (others=>'0');

          for i in 0 to gNrFSPs-1 loop
            if ( Address_s0 =  addrList(i)(1)(7 downto 0) ) then
              foundFlag      <= '1';
              --cntGeneric     <= unsigned(resize(signed(addrList(i)(0)), cntGeneric'length));
              --Tc_cntGeneric  <= unsigned(resize(signed(addrList(i)(0)), cntGeneric'length));
              cntGeneric     <= unsigned(addrList(i)(0));
              Tc_cntGeneric  <= unsigned(addrList(i)(0));
              addrRAM        <= std_logic_vector(to_unsigned(i,10));
            end if;
          end loop;
        end if;

        RAM_WrCmd     <= '0';
        -- if (FPGA_DtFtchdEn ='1') then
        --   FIFO_WrCmd    <= '0';
        --   FIFO_WrData   <= tmpReg;
        -- else
        --   FIFO_WrCmd    <= FPGA_NewDtFtchd;
        -- end if;  

      when CheckAddrList =>

        if (foundFlag  = '1') then                     -- The FSP is one of the list
          fsm_state                              <= WaitShiftOutCmd;
          --wrenRAM                                <= '1';
          RWn_s0                                 <= RWn;
          Avail(to_integer(unsigned(addrRAM)))   <= '1';
          --RO   (to_integer(unsigned(addrRAM)))   <= '0';
        else                                           -- The FSP is not part of the list
          fsm_state                              <= WaitAddress;
        end if;

      when WaitShiftOutCmd =>

        RAM_WrCmd     <= '0';
        --wrenRAM              <= '0';
        if (RWn_s0 = '1') then                     -- Read process
          tmpReg(31 downto 0)  <= RAM_RdData;            -- Sampling data from RAM
        end if;

        if (Address_s0 /= Address) then
          fsm_state <= WaitAddress;                    -- Back to the main address checker
          tmpReg    <= (others=>'0');                  -- Washout the temporary register 
        else
          if (DataEn_RE = '1') then
  
            fsm_state   <= CheckNrShift;
            cntGeneric  <= cntGeneric - 1;

            if (RWn_s0 = '0') then                     -- Write process
              if (cntGeneric > 0) then
                --############ Here I need to convert to integer the last 3 bits of cntGeneric -1
                --tmpReg(7+8*(to_integer(cntGeneric(2 downto 0)-1)) downto 8*(to_integer(cntGeneric(2 downto 0)-1))) <= Data;
                tmpReg(7+8*(to_integer(cntGeneric(1 downto 0)-1)) downto 8*(to_integer(cntGeneric(1 downto 0)-1))) <= Data;
              end if;
            end if;

          end if;
        end if;

      when CheckNrShift =>

        -- if (cntGeneric > 0) then
        --   fsm_state   <= WaitShiftOutCmd;
        -- else
        --   fsm_state   <= WaitAddress;
        --   RAM_WrCmd   <= '1';
        --   if (FPGA_DtFtchdEn ='1') then
        --     FIFO_WrCmd    <= '1';
        --   else
        --     FIFO_WrCmd    <= FPGA_NewDtFtchd;
        --   end if;  

        --   --foundFlag   <= '0';
        -- end if;
        if (cntGeneric > 0) then
          fsm_state   <= WaitShiftOutCmd;
          if (RWn_s0 = '0') then 
            --if (to_integer(Tc_cntGeneric) > 8 and to_integer(cntGeneric(2 downto 0))=0) then
            if (to_integer(Tc_cntGeneric) > 8 and to_integer(cntGeneric(1 downto 0))=0) then
              RAM_WrCmd   <= '1';
              if (FPGA_DtFtchdEn ='0') then
                FIFO_WrCmd    <= '1';
              end if;
            end if;
          end if;
        else
          fsm_state   <= WaitAddress;
          if (RWn_s0 = '0') then 
            RAM_WrCmd   <= '1';
            if (FPGA_DtFtchdEn ='0' and to_integer(Tc_cntGeneric) > 8) then
              FIFO_WrCmd    <= '1';
            end if;  
          end if;

        end if;
      when others=>
        fsm_state      <= WaitAddress;

        FIFO_WrData    <= (others=>'0');
        cntGeneric     <= (others=>'0');
        RAM_WrCmd      <= '0';
        FIFO_WrCmd     <= '0';
        addrRAM        <= (others=>'0');
        Avail          <= (others=>'0');
        --RO             <= (others=>'0');
        foundFlag      <= '0';
        Tc_cntGeneric  <= (others=>'0');
        RWn_s0         <= '0';

    end case;
  end if;
end process;

-- Outputs
--pOutputMapping: process(foundFlag, cntGeneric, RAM_DtOut ,Avail , RO, Tc_cntGeneric)
pOutputMapping: process(foundFlag, cntGeneric, tmpReg ,Avail , Tc_cntGeneric, RWn_s0)

begin

 if (foundFlag = '1') then

    RemainingBytes  <= std_logic_vector(cntGeneric);

    if ((cntGeneric < Tc_cntGeneric) and (RWn_s0='1')) then
      --Data          <= tmpReg(7+8*to_integer(cntGeneric(2 downto 0)) downto 8*to_integer(cntGeneric(2 downto 0)));
      Data          <= tmpReg(7+8*to_integer(cntGeneric(1 downto 0)) downto 8*to_integer(cntGeneric(1 downto 0)));
    else
      Data          <= (others=>'Z');
    end if;
    Available       <= Avail;
    --ReadOnly        <= RO;


 else

    Data	          <= (others => 'Z');
    RemainingBytes  <= (others => 'Z');
    Available       <= (others => '0');
    --ReadOnly        <= (others => '0');

 end if;

end process pOutputMapping;

ReadOnly        <= (others=>'0');
RAM_Addr        <= addrRAM;
RAM_WrData      <= tmpReg(31 downto 0);

end beh;

