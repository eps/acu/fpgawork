###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work

vmap altera_mf.all work
vmap altera_mf_component.all work



pwd
         
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_package_numericLib.vhd  
#vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/SimFolder/vhdl/mUSIC_Emulator.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/SimFolder/vhdl/cntFreeWheel.vhd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/SimFolder/vhdl/cntCmdInDriven.vhd

vcom -93 ../quartus/FIFO32b16wDual.vhd
#vcom -93 ../quartus/RAM32b1024wDual.qip

vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_mUSIC/FSP_Output/vhdl/FSP_Output.vhd

vcom -93 $path/ACU_AvalonMMM_USI_Bridge.vhd

vcom -93 $path/Tb_ACU_AvalonMMM_USI_Bridge.vhd
