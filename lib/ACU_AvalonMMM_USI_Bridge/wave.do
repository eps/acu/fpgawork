onerror {resume}
quietly virtual signal -install /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge { /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(2 downto 0)} Splitter
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider FSP1_3B_Output
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/Clock
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/Reset
add wave -noupdate -radix unsigned /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/Address
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/Data
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/ClockData
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/RWn
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/CRCOK
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/InputFromPeripheral
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/LoadDataFromPeripheral
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/NewDataAvailable
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/OutputToPeripheral
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/RemainingBytes
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/Available
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/ReadOnly
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sState_FSP
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sDresser
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sDataOut
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sAddress
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sOutputToPeripheralTemp
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sOutputToPeripheral
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sClockData
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sCRCOK
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sFSPSelected
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sFSPByteCounter
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sRemainingBytes
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sNewDataAvailable
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sEdgeDetArray
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FSP_Output/sLoadEdgeDetArray
add wave -noupdate -divider mUSIC_Emulator
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/Clock
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/Reset
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSP_RdWrn
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSP_Dt2Wr
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/RemainingBytes
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSP_Start
add wave -noupdate -color Magenta -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSP_NewDtRead
add wave -noupdate -color Magenta -radix unsigned /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSPnr
add wave -noupdate -color Magenta -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSP_DtRead
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/Address
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/DataEn
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/RWn
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/Data
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSP_Start_s0
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSP_Start_s1
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSP_Start_RE
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSP_Start_RE_s0
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/FSP_Start_RE_s1
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/tmpCntByte
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/cntTimeBetween2DataEn
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_mUSIC_Emulator/tmpDt2Wr
add wave -noupdate -divider AvalonMMM2USI_Bridge
add wave -noupdate /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/FPGA_NewDtFtchd_s0
add wave -noupdate /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/FPGA_NewDtFtchd_s1
add wave -noupdate /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/SplitFlag
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/FIFO_WrCmd
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/FIFO_WrData
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/Clock
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/Reset
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/Address
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/DataEn
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/RWn
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/Data
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/RemainingBytes
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/Available
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/ReadOnly
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/RAM_Addr
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/RAM_WrCmd
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/RAM_WrData
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/RAM_RdData
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/FPGA_DtFtchdEn
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/FPGA_NewDtFtchd
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/FPGA_DataFetched
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/Address_s0
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/Address_s1
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/RWn_s0
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/Splitter
add wave -noupdate -radix hexadecimal -childformat {{/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(11) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(10) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(9) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(8) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(7) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(6) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(5) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(4) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(3) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(2) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(1) -radix hexadecimal} {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(0) -radix hexadecimal}} -subitemconfig {/tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(11) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(10) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(9) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(8) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(7) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(6) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(5) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(4) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(3) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(2) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(1) {-height 16 -radix hexadecimal} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric(0) {-height 16 -radix hexadecimal}} /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/cntGeneric
add wave -noupdate -color Gold -itemcolor Gold -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/Tc_cntGeneric
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/fsm_state
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/dt2WrRAM
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/addrRAM
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/DataEn_s0
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/DataEn_s1
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/DataEn_RE
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/foundFlag
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/RAM_DtOut
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/Avail
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_ACU_AvalonMMM_USI_Bridge/tmpReg
add wave -noupdate -divider FIFO
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/aclr
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/rdclk
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/rdreq
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/wrclk
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/wrreq
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/data
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/q
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/rdempty
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/rdfull
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/rdusedw
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/wrempty
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/wrfull
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/wrusedw
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/sub_wire0
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/sub_wire1
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/sub_wire2
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/sub_wire3
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/sub_wire4
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/sub_wire5
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_FIFO32b16wDual/sub_wire6
add wave -noupdate -divider RAM
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/aclr_a
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/aclr_b
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/address_a
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/address_b
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/clock_a
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/clock_b
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/data_a
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/data_b
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/wren_a
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/wren_b
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/q_a
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/q_b
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/sub_wire0
add wave -noupdate -radix hexadecimal /tb_acu_avalonmmm_usi_bridge/i_RAM32b1024wDual/sub_wire1
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2414835000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2315656122 ps} {2498738743 ps}
