-- Copyright (C) 1991-2010 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "10/04/2011 15:09:33"
                                                            
-- Vhdl Test Bench template for design  :  SPI_Master
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY SPI_Master_vhd_tst IS
END SPI_Master_vhd_tst;

ARCHITECTURE SPI_Master_arch OF SPI_Master_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL Busy : STD_LOGIC;
SIGNAL DATA_In : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL DATA_OUT : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL Daten_Da : STD_LOGIC;
SIGNAL Freigabe : STD_LOGIC := '0';
SIGNAL Reset : STD_LOGIC := '1';
SIGNAL SCK : STD_LOGIC;
SIGNAL SDI : STD_LOGIC;
SIGNAL SDO : STD_LOGIC;
SIGNAL System_Clock : STD_LOGIC := '0';

COMPONENT SPI_Master
	PORT (
	Busy : OUT STD_LOGIC;
	DATA_In : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	DATA_OUT : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	Daten_Da : OUT STD_LOGIC;
	Freigabe : IN STD_LOGIC;
	Reset : IN STD_LOGIC;
	SCK : OUT STD_LOGIC;
	SDI : IN STD_LOGIC;
	SDO : OUT STD_LOGIC;
	System_Clock : IN STD_LOGIC
	);
END COMPONENT;

BEGIN
	i1 : SPI_Master
	PORT MAP (
-- list connections between master ports and signals
	Busy => Busy,
	DATA_In => DATA_In,
	DATA_OUT => DATA_OUT,
	Daten_Da => Daten_Da,
	Freigabe => Freigabe,
	Reset => Reset,
	SCK => SCK,
	SDI => SDI,
	SDO => SDO,
	System_Clock => System_Clock
	);

clock : process
begin
  System_Clock <= not System_Clock;
  wait for 5 ns;
end process clock;
  

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once 
        wait for 100 ns;
        SDI <= '1';
        Reset <= '0';        
        DATA_IN <= "01011001";     
        wait for 200 ns;
        Freigabe <= '1';
        wait for 20 ns;
        Freigabe <= '0';                 
WAIT;                                                       
END PROCESS init;                                           

always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list  
WAIT;                                                        
END PROCESS always;                                          

END SPI_Master_arch;
