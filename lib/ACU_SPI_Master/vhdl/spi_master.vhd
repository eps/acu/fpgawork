library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity SPI_Master is



generic  (
      gCPHA             : std_logic          :=  '0'              ;     --| Daten werden bei LOW mit der 1. Flake bei HIGH mit der 2. Flake Eingelessen 
      gCPOL             : std_logic          :=  '0'              ;     --| Pegel des Ruhezustand (LOW = LOW HIGH = HIGH)
      gSystem_Takt      : natural            :=  100_000_000      ;     --| 
      gSCK_soll         : natural            :=   33_000_000      ;     --| 
      gBit_Anzahl       : natural            :=  8                      --| 
         );

Port(
   System_Clock   :  In    std_logic                                    ;     --| System-/Arbeitstakt
   Freigabe       :  In    std_logic                                    ;     --| high gibt ein bayt daten zum senden und entfangen frei
   Reset          :  In    std_logic                                    ;     --| Die Reset Funktion (HIGH = Resetzustand)
   Chip_Adresse   :  In    std_logic_vector(1 downto 0)                 ;     --| Für die Wahl des Chips 
   DATA_In        :  In    std_logic_vector(gBit_Anzahl - 1 downto 0)   ;     --| Paralle Eingange Die zu sendeden (8 Bit)
   SDI            :  In    std_logic                                    ;     --| Serieller Dateneingang 
   DATA_OUT       :  Out   std_logic_vector(gBit_Anzahl - 1 downto 0)   ;     --| Paralle Ausgange (8 Bit)   
   Daten_Da       :  Out   std_logic                                    ;     --| Daten sind da
   Busy           :  Out   std_logic                                    ;     --| Daten werden gelesen und oder gesendet
   SCK            :  Out   std_logic                                    ;     --| Serial Clock
   SDO            :  Out   std_logic                                    ;     --| Serieller Datenausgang
   Chip0          :  Out   std_logic                                    ;     --| High = Chip0 Gewählt
   Chip1          :  Out   std_logic                                    ;     --| High = Chip1 Gewählt
   Chip2          :  Out   std_logic                                          --| High = Chip2 Gewählt
     );

End   SPI_Master;

Architecture   rtl   of SPI_Master  is 

constant cSCK_Halb_Periode             :  natural                       := gSystem_Takt / (gSCK_soll * 2) - 1 ;
constant cSCK_Periode                  :  natural                       := gSystem_Takt / gSCK_soll - 1 ;
constant cEchte_Frequenz               :  natural                       := gSystem_Takt / (cSCK_Periode + 1) ;
constant cSCK_Tastgrad                 :  natural range 0 to 100        := (cSCK_Halb_Periode + 1) * 100 / (cSCK_Periode + 1)  ;
constant cMax_Frequenz                 :  natural                       := gSystem_Takt / 3 ;

Signal   sSCK_Vorteiler                :  integer                                      ;     --| Wird für die bestimmung des Taktes von SCK benätigt
Signal   sBit_Zaehler                  :  integer range gBit_Anzahl - 1 downto 0       ;     --| Register zum Sicherstellen dass, das richtig Bit ausgegeben/eingelesen wird
Signal   sSDI_Speicher                 :  std_logic_vector(gBit_Anzahl - 1 downto 0)   ;     --| Zwischen Speicher für die entfangen bits von SDI
Signal   sSDO_Speicher                 :  std_logic_vector(gBit_Anzahl - 1 downto 0)   ;     --| Zwischen Speicher für die zusendenen bits von SDO

type     tSPI  is (
                        Warte_auf_freigabe,
                        Anfang_des_bits,
                        Flanke_des_bits,
                        Ende_des_bits
                        );

Signal   sSPI     :        tSPI;

Begin

   assert (gSCK_soll <= cMax_Frequenz )
      report "Achtung die SCK Frequenz darf nicht Groesser als ein Drittel des System_Clock eingestellt werden. Aber sie ist auf " & integer'image(gSCK_soll)
         & " Hz eingestellt. Bei momentanen System_Clock ist die Maximale Frequenz " & integer'image(cMax_Frequenz) & "Hz"
   severity Error;

   assert (cEchte_Frequenz = gSCK_soll)
      report "Achtung die soll Frequenz ist auf " & integer'image(gSCK_soll)
         & " Hz gesetzt. Aber die echte Frequenz ist " & integer'image(cEchte_Frequenz) & "Hz"
   severity Warning;

   assert (0 = 1)
      report "Tastgrad ist " & integer'image(cSCK_Tastgrad) & "%"
   severity Note;

SPI : Process(Reset, System_Clock, sSCK_Vorteiler, sBit_Zaehler)
   Begin
      If ( Reset = '1') Then                                         --| Ist Reset aktivert ?
         sSPI <= Warte_auf_freigabe;                                 --|   Ja: Nächster zu bearbeitende Stade =
         Daten_Da <= '0';                                            --|       
         Busy <= '0';                                                --|
         SDO <= '1';                                                 --|       Versetze Serial Daten ausgang in den Ruhe Zustand
         SCK <= gCPOL;                                               --|       Versetze Serial Clock in den Ruhe Zustand
         sSDI_Speicher <= (others => '0');                           --|       Versetze inhalt des zwischenspeichers auf 0
         sSDO_Speicher <= (others => '0');                           --|       Versetze inhalt des zwischenspeichers auf 0
         sBit_Zaehler <= gBit_Anzahl - 1;                            --| Starte Auflessen mit MSB
      Else                                                           --| Nein: Arbeitszustand
         if rising_edge(System_Clock)  Then  
            case (sSPI) is
            ---------------------------------------
            when  Warte_auf_freigabe =>                                 --| Warte auf Freigabe
                  Busy <= '0';
                  Daten_Da <= '0';
                  SCK <= gCPOL;                                         --| Versetze Serial Daten ausgang in den Ruhe Zustand
                  sSCK_Vorteiler <= 0;                                  --| Setze SCK Zählerstand auf 0
                  SDO <= '1';                                           --|       Versetze Serial Daten ausgang in den Ruhe Zustand
                  sBit_Zaehler <= gBit_Anzahl - 1;                      --| Starte Auflessen mit MSB
                  if ( Freigabe = '1') Then                             --|   Ja: Ist Freigabe Aktivert ?
                     sSDO_Speicher <= DATA_In;                          --|       Speichere eingang zwischen
                     Busy <= '1';
                     sSPI <= Anfang_des_bits;                           --|       Nächster Stade
                  else                                                  --| Nein: Freigabe ist nicht Aktive 
                     sSPI <= Warte_auf_freigabe;                        --|       verweile in momentanen Stade
                  end if;
            ---------------------------------------
            when  Anfang_des_bits =>                                    --| Anfang des Bits
                  SDO <= sSDO_Speicher(sBit_Zaehler);
                  sSCK_Vorteiler <= sSCK_Vorteiler + 1;                  
                  if (gCPHA = '0') Then                                 --| Soll bei der ersten Flake die Daten übernommen werden ?
                     SCK <= gCPOL;                                      --|   Ja: 
                  else                                                  --| Nein: Daten Sollen Bei der Zweiten Flanke übernommen werden
                     SCK <= not(gCPOL);                                 --|       
                  end if;
                  if (sSCK_Vorteiler >= cSCK_Halb_Periode) Then         --| Halbe Periode Um ?
                     sSPI <= Flanke_des_bits;                           --|   Ja: Nächster Stade
                  else                                                  --| Nein: Freigabe ist nicht Aktive 
                     sSPI <= Anfang_des_bits;                           --|       verweile in momentanen Stade
                  end if;
            ---------------------------------------
            when  Flanke_des_bits =>                                    --| Flanke des Bits Wird ausgeführt
                  if (gCPHA = '0') Then                                 --| Soll bei der ersten Flake die Daten übernommen werden ?
                     SCK <= not(gCPOL);                                 --|   Ja: 
                  else                                                  --| Nein: Daten Sollen Bei der Zweiten Flanke übernommen werden
                     SCK <= gCPOL;                                      --|       
                  end if;
                  sSCK_Vorteiler <= sSCK_Vorteiler + 1;
                  sSDI_Speicher(sBit_Zaehler) <= SDI;                   --| übernehme eingangs Daten
                  sSPI <= Ende_des_bits;                                --| Nächster Stade
            ---------------------------------------
            when  Ende_des_bits =>                                      --| Mitte des Bits
                  sSCK_Vorteiler <= sSCK_Vorteiler + 1;                 
                  if (sSCK_Vorteiler >= cSCK_Periode) Then              --| Periode Um ?
                     if (sBit_Zaehler = 0) Then                         --|   Ja: Sind alle Bits eingelessen
                        sSPI <= Warte_auf_freigabe;                     --|       Warte auf Freigabe
                        DATA_OUT <= sSDI_Speicher;                      --|       Eingelesenen Daten an Ausgang übernehen
                        Busy <= '0';                                    --|       
                        Daten_Da <= '1';                                --|       Beenden des Einlesens Bestäticken
                     else                                               --| Nein: Es stehen noch Bits aus
                        sBit_Zaehler <= sBit_Zaehler - 1;
                        sSCK_Vorteiler <= 0;
                        sSPI <= Anfang_des_bits;                        --|     Nächster Stade
                     end if;
                  else                                                  --| Nein: Freigabe ist nicht Aktive 
                     sSPI <= Ende_des_bits;                             --|       verweile in momentanen Stade
                  end if;
            ---------------------------------------
            when others =>
               sSPI <= Warte_auf_freigabe;                              --|   Ja: Nächster zu bearbeitende Stade =
               SDO <= '1';                                              --|       Versetze Serial Daten ausgang in den Ruhe Zustand
               SCK <= gCPOL;
            end case;
         else
            sSCK_Vorteiler <= sSCK_Vorteiler;                           --|
            sBit_Zaehler <= sBit_Zaehler;                               --|
         end if;
      End If;
   End Process;

Baustein_wahl :  Process(Reset,Chip_Adresse) 
   
   Begin
      If (Reset = '1') Then                                 --| Ist Reset Aktive ?
         Chip0 <= '0';                                      --|   Ja: Alle ausgänge auf Low
         Chip1 <= '0';
         Chip2 <= '0';
      Else                                                  --| Reset ist nicht Aktive
         if (Chip_Adresse = "00") Then                      --| Ist Adresse = 00
            Chip0 <= '0';                                   --|   Ja: Alle ausgänge auf Low
            Chip1 <= '0';
            Chip2 <= '0';
         elsif (Chip_Adresse = "01") Then                   --| Oder: Adresse = 01
            Chip0 <= '1';                                   --|       Chip0 = High Chip1 und 2 = Low
            Chip1 <= '0';
            Chip2 <= '0';
         elsif (Chip_Adresse = "10") Then                   --| Oder: Adresse = 10
            Chip0 <= '0';                                   --|       Chip1 = High Chip0 und 2 = Low
            Chip1 <= '1';
            Chip2 <= '0';
         elsif (Chip_Adresse = "11") Then                   --| Oder: Adresse = 11
            Chip0 <= '0';                                   --|       Chip2 = High Chip0 und 1 = Low
            Chip1 <= '0';
            Chip2 <= '1';
         else
            Chip0 <= '0';                                   --| Nein: Alle ausgänge auf Low
            Chip1 <= '0';
            Chip2 <= '0';
         end if;
      End If;
   End Process;
   
   
END rtl;