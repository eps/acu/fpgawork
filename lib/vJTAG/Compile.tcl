###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

#set path../vhdl"

path="../vhdl"

vlib work
vmap work work

vcom -93 ../../ACU_package.vhd

vcom -93 $path/vJTAG.vhd
vcom -93 $path/test.vhd
vcom -93 $path/vJTAG_Itf.vhd

vcom -93 $path/Tb_vJTAG.vhd
