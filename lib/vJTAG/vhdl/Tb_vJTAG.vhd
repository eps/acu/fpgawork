---------------------------------------------------------------
-- File name: Tb_vJTAG.vhd                                   --
--                                                           --
-- Author   : D.Rodomonti                                    --
-- Date     : 17/03/2016                                     --
--                                                           --
-- Comments : !!!  TO ADD               --
--                                                           --
-- History  : Start up version 17/03/2016                    --
---------------------------------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.all;

library modelsim_lib;
use modelsim_lib.util.all;

LIBRARY altera_mf;
USE altera_mf.all;

ENTITY Tb_vJTAG IS
END ENTITY Tb_vJTAG;

ARCHITECTURE beh OF Tb_vJTAG IS
-- Constants declaration
--
-- Signals declaration
--
signal tdi                : std_logic;
signal tdo                : std_logic;
signal tck                : std_logic;
signal DRureg             : std_logic_vector (15 downto 0);
signal DRsreg             : std_logic_vector (15 downto 0);
signal virtual_state_udr  : std_logic;
signal virtual_state_sdr  : std_logic;
signal virtual_state_cdr  : std_logic;
signal ir_in              : std_logic_vector(6 downto 0);
-- Components declaration
--
Component vJTAG IS
	PORT
	(
		ir_out		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		tdo		: IN STD_LOGIC ;
		ir_in		: OUT STD_LOGIC_VECTOR (6 DOWNTO 0);
		tck		: OUT STD_LOGIC ;
		tdi		: OUT STD_LOGIC ;
		virtual_state_cdr		: OUT STD_LOGIC ;
		virtual_state_cir		: OUT STD_LOGIC ;
		virtual_state_e1dr		: OUT STD_LOGIC ;
		virtual_state_e2dr		: OUT STD_LOGIC ;
		virtual_state_pdr		: OUT STD_LOGIC ;
		virtual_state_sdr		: OUT STD_LOGIC ;
		virtual_state_udr		: OUT STD_LOGIC ;
		virtual_state_uir		: OUT STD_LOGIC 
	);
END component vJTAG;

component test IS
	PORT
	(
		ir_out		: IN STD_LOGIC_VECTOR (6 DOWNTO 0);
		tdo		: IN STD_LOGIC ;
		ir_in		: OUT STD_LOGIC_VECTOR (6 DOWNTO 0);
		jtag_state_cdr		: OUT STD_LOGIC ;
		jtag_state_cir		: OUT STD_LOGIC ;
		jtag_state_e1dr		: OUT STD_LOGIC ;
		jtag_state_e1ir		: OUT STD_LOGIC ;
		jtag_state_e2dr		: OUT STD_LOGIC ;
		jtag_state_e2ir		: OUT STD_LOGIC ;
		jtag_state_pdr		: OUT STD_LOGIC ;
		jtag_state_pir		: OUT STD_LOGIC ;
		jtag_state_rti		: OUT STD_LOGIC ;
		jtag_state_sdr		: OUT STD_LOGIC ;
		jtag_state_sdrs		: OUT STD_LOGIC ;
		jtag_state_sir		: OUT STD_LOGIC ;
		jtag_state_sirs		: OUT STD_LOGIC ;
		jtag_state_tlr		: OUT STD_LOGIC ;
		jtag_state_udr		: OUT STD_LOGIC ;
		jtag_state_uir		: OUT STD_LOGIC ;
		tck		: OUT STD_LOGIC ;
		tdi		: OUT STD_LOGIC ;
		tms		: OUT STD_LOGIC ;
		virtual_state_cdr		: OUT STD_LOGIC ;
		virtual_state_cir		: OUT STD_LOGIC ;
		virtual_state_e1dr		: OUT STD_LOGIC ;
		virtual_state_e2dr		: OUT STD_LOGIC ;
		virtual_state_pdr		: OUT STD_LOGIC ;
		virtual_state_sdr		: OUT STD_LOGIC ;
		virtual_state_udr		: OUT STD_LOGIC ;
		virtual_state_uir		: OUT STD_LOGIC 
	);
end component;

component vJTAG_Itf is
generic  
   (
      gvJTAG_ItfVersion      : real := 1.0
   );
  Port (
         tck                   : in std_logic;                      -- JTAG Clock signal max 10 MHz
         Reset                 : in std_logic;                      -- Asynchronous reset signal active high
         tdi                   : in std_logic;                      -- JTAG data in.
	 ir_in                 : in std_logic_vector(6 downto 0);   -- JTAG Instruction Register.
	 v_cdr                 : in std_logic;                      -- Virtual JTAG state Capture Data Register. It is used to load the data to send back via tdo.
	 v_sdr                 : in std_logic;                      -- Virtual JTAG state Shift Data Register. It is used to enable the internal shift register to 
	                                                            -- hold tdi values.
	 v_udr                 : in std_logic;                      -- Virtual JTAG state Update Data Register. It is used to swap the tdi collected data to a parallel register.

         tdo                   : out std_logic;                      -- JTAG data out.

         LEDs                  : out std_logic_vector(7 downto 0)   -- LEDs driving signals. DEBUG PURPOSE ONLY!!!
  );
End component vJTAG_Itf;


BEGIN
--i_vJTAG : vJTAG	
--  PORT MAP
--	(
--		ir_out		   => (others=>'0'),
--		tdo		   => tdi,
--		ir_in		   => open,
--		tck		   => open,
--		tdi		   => tdi,
--		virtual_state_cdr  => open,
--		virtual_state_cir  => open,
--		virtual_state_e1dr => open,
--		virtual_state_e2dr => open,
--		virtual_state_pdr  => open,
--		virtual_state_sdr  => open,
--		virtual_state_udr  => open,
--		virtual_state_uir  => open
--	);

i_test:test
	PORT map
	(
		ir_out		 =>(others=>'0'),
		tdo		 => tdo,
		ir_in		 => ir_in,
		jtag_state_cdr	 => open,
		jtag_state_cir	 => open,
		jtag_state_e1dr	 => open,
		jtag_state_e1ir	 => open,
		jtag_state_e2dr	 => open,
		jtag_state_e2ir	 => open,
		jtag_state_pdr	 => open,
		jtag_state_pir	 => open,
		jtag_state_rti	 => open,
		jtag_state_sdr	 => open,
		jtag_state_sdrs	 => open,
		jtag_state_sir	 => open,
		jtag_state_sirs	 => open,
		jtag_state_tlr	 => open,
		jtag_state_udr	 => open,
		jtag_state_uir   => open,
		tck		 => tck,
		tdi		 => tdi,
		tms		 => open,
		virtual_state_cdr		=> virtual_state_cdr,
		virtual_state_cir		=> open,
		virtual_state_e1dr		=> open,
		virtual_state_e2dr		=> open,
		virtual_state_pdr		=> open,
		virtual_state_sdr		=> virtual_state_sdr,
		virtual_state_udr		=> virtual_state_udr,
		virtual_state_uir		=> open
	);

p_dr:process (tck)
begin
  if (tck'event and tck='1') then
    if (virtual_state_sdr='1') then
      DRsreg(15)          <= tdo;
      DRsreg(14 downto 0) <= DRsreg(15 downto 1);
    end if;

  
    if (virtual_state_udr='1') then
      DRureg  <= DRsreg;
    end if;
  end if;
end process;

i_vJTAG_Itf:vJTAG_Itf 
  Port map(
         tck        => tck,
         Reset      => '0',      
         tdi        => tdi,
	 ir_in      => ir_in,
	 v_cdr      => virtual_state_cdr,
	 v_sdr      => virtual_state_sdr,
	 v_udr      => virtual_state_udr,
         tdo        => tdo,
         LEDs       => open
  );
END beh;
