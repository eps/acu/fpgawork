restart -f
run 1 us
force -freeze sim:/tb_acu_shiftreg_driver/i_ACU_ShiftReg_Driver/Reset 0 0
run 10 us

force -freeze sim:/tb_acu_shiftreg_driver/i_ACU_ShiftReg_Driver/Enable 1 0
run 1 us
noforce sim:/tb_acu_shiftreg_driver/i_ACU_ShiftReg_Driver/Enable
run 20 us

force -freeze sim:/tb_acu_shiftreg_driver/i_ACU_ShiftReg_Driver/Enable 1 0
run 40 us

force -freeze sim:/tb_acu_shiftreg_driver/LathedVal 1111100011100101001001101101011010110010 0
run 40 us
