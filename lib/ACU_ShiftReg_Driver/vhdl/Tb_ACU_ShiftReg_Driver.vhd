--------------------------------------------------------------------------------------------------------
-- File name: Tb_ACU_ShiftReg_Driver.vhd                                                              --
--                                                                                                    --
-- Author   : D.Rodomonti                                                                             --
-- EMail    : d.rodomonti@gsi.de                                                                      --
-- Date     : 06/12/2023                                                                              --
--                                                                                                    --
-- Comments : ACU_ShiftReg_Driver test bench file                                                     --
--                                                                                                    -- 
-- History  : V1.0 DR => Start up version 06.12.2023                                                  --
--------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity Tb_ACU_ShiftReg_Driver is
End entity Tb_ACU_ShiftReg_Driver;

Architecture beh of Tb_ACU_ShiftReg_Driver is
--
-- Constant declaration
--
-- Signals declaration
signal Clock         : std_logic:='0';
signal Reset         : std_logic:='1';
signal eulShifReg    : std_logic_vector(39 downto 0);
signal ShReg_SH_LDn  : std_logic;
signal ShReg_CLK     : std_logic;
signal ShReg_CLK_s0  : std_logic;
signal ShReg_CLK_RE  : std_logic;    
signal Enable        : std_logic:='0';
signal LathedVal     : std_logic_vector(39 downto 0):=x"657F26D6A3"; -- CAFE4DAD46 with an initial shift compensated
--
-- Component declaration
Component ACU_ShiftReg_Driver is
  Generic (
    gLatchLevel       : std_logic:='0';                              -- ShReg_SH_LDn activation level
    gSH_LDnTw         : unsigned(7 downto 0):=x"14";                 -- ShReg_SH_LDn pulse duration (20 clock cycles by default=200ns)
    gCLK_H_Tw         : unsigned(7 downto 0):=x"14";                 -- ShReg_CLK high pulse duration (20 clock cycles by default=200ns)
    gCLK_L_Tw         : unsigned(7 downto 0):=x"14";                 -- ShReg_CLK high low duration (20 clock cycles by default=200ns)
    gDataWidth        : integer range 1 to 255:= 40                  -- Number of bits to shift out
  );
  Port (
    Clock             : in std_logic;                                -- Clock signal (usually @100MHz)
    Reset             : in std_logic;                                -- Asynchronous reset signal active high

    Enable            : in std_logic;                                -- Enable signal active high (level)
    
    ShReg_DtIn        : in std_logic;                                -- Input data line coming from the shif register chip.
    ShReg_CLK         : out std_logic;                               -- Clock signal going to the shif register chip.
    ShReg_SH_LDn      : out std_logic;                               -- Latch command line going to the shif register chip.
    
    NewShRegData      : out std_logic;                               -- One clock cycle pulse active high when a new ShRegData is available.
    ShRegData         : out std_logic_vector(gDataWidth-1 downto 0)  -- Data fetched from the shif register
    
  );
End component ACU_ShiftReg_Driver;

begin

Clock  <= not Clock after 5 ns;

p_emulShReg:process(Clock,Reset)
begin
  if (Reset='1') then
    eulShifReg    <= (others=>'0');
    ShReg_CLK_RE  <= '0';
    ShReg_CLK_s0  <= '0';
  elsif (Clock' event and Clock='1') then
    ShReg_CLK_RE  <= ShReg_CLK and not ShReg_CLK_s0;
    ShReg_CLK_s0  <= ShReg_CLK;
  
    if (ShReg_SH_LDn='0') then
      eulShifReg  <= LathedVal;
    elsif (ShReg_CLK_RE='1') then
      eulShifReg  <= eulShifReg(38 downto 0) & eulShifReg(39);
    end if;
    
  end if;
end process p_emulShReg;

i_ACU_ShiftReg_Driver: ACU_ShiftReg_Driver

  Port map(
    Clock             => Clock,
    Reset             => Reset,
    Enable            => Enable,
    
    ShReg_DtIn        => eulShifReg(39),
    ShReg_CLK         => ShReg_CLK,
    ShReg_SH_LDn      => ShReg_SH_LDn,
    
    NewShRegData      => open,
    ShRegData         => open
    
  );
end beh;

