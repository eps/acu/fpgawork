--------------------------------------------------------------------------------------------------------
-- File name: ACU_ShiftReg_Driver.vhd                                                                 --
--                                                                                                    --
-- Author   : D.Rodomonti                                                                             --
-- EMail    : d.rodomonti@gsi.de                                                                      --
-- Date     : 06/12/2023                                                                              --
--                                                                                                    --
-- Comments : This module is a generic shift register driver designed to control the SN74HC165 chip,  --
--            but it can be used also for other chip vendors adjusting the generic values.            --
--            The driver triggers the shif register latch action when an input Enable signal is high. --
--            The latch signal activation level is defined via generic (gLatchLevel) and its pulse    --
--            duration too (gSH_LDnTw).                                                               --
--            The clock period is also defined via gnerics (gCLK_H_Tw and gCLK_L_Tw).                 --
--            The number of shift to perform, equivalent to the data width to fetch is selectable via --
--            gDataWidth.                                                                             --
--            Every time a new Data vector is available in output, a NewData pulse                    --
--            (one clock cycle long) is generated.                                                    --
--            Please note that if a data fetching is in execution, the unset of the Enable signal will--
--            not stop the current acquisition.                                                       --
--            The acquisition period (excludinf a couple of clock cycles for resampling) is made of:  --
--                   T= gDataWidth * (gCLK_H_Tw + gCLK_L_Tw) + gSH_LDnTw                              --
--                                                                                                    -- 
-- History  : V1.0 DR => Start up version 06.12.2023                                                  --
--------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_ShiftReg_Driver is
  Generic (
    gLatchLevel       : std_logic:='0';                              -- ShReg_SH_LDn activation level
    gSH_LDnTw         : unsigned(7 downto 0):=x"14";                 -- ShReg_SH_LDn pulse duration (20 clock cycles by default=200ns)
    gCLK_H_Tw         : unsigned(7 downto 0):=x"14";                 -- ShReg_CLK high pulse duration (20 clock cycles by default=200ns)
    gCLK_L_Tw         : unsigned(7 downto 0):=x"14";                 -- ShReg_CLK high low duration (20 clock cycles by default=200ns)
    gDataWidth        : integer range 1 to 255:= 40                  -- Number of bits to shift out
  );
  Port (
    Clock             : in std_logic;                                -- Clock signal (usually @100MHz)
    Reset             : in std_logic;                                -- Asynchronous reset signal active high

    Enable            : in std_logic;                                -- Enable signal active high (level)
    
    ShReg_DtIn        : in std_logic;                                -- Input data line coming from the shif register chip.
    ShReg_CLK         : out std_logic;                               -- Clock signal going to the shif register chip.
    ShReg_SH_LDn      : out std_logic;                               -- Latch command line going to the shif register chip.
    
    NewShRegData      : out std_logic;                               -- One clock cycle pulse active high when a new ShRegData is available.
    ShRegData         : out std_logic_vector(gDataWidth-1 downto 0)  -- Data fetched from the shif register
    
  );
End entity ACU_ShiftReg_Driver;

Architecture beh of ACU_ShiftReg_Driver is
--
-- Constant declaration
--
-- Signals declaration
signal cnt_pulse        : unsigned(7 downto 0);                       -- Pulse signals counter (used for ShReg_SH_LDn and ShReg_CLK generation)
signal cnt_Dt           : unsigned(7 downto 0);                       -- Data to shift out counter
signal tmpShRegData     : std_logic_vector(gDataWidth-1 downto 0);    -- Temporary data fetched signal

type fsmState is (WaitEnable, SetSH_LDn, SetCLK, UnsetCLK);
signal fsm_state                : fsmState;

begin

-- FSM
p_FSM: process(Reset,Clock)
begin
  if (Reset = '1') then
    fsm_state           <= WaitEnable;
    cnt_pulse           <= (others=>'0');
    cnt_Dt              <= (others=>'0');
    tmpShRegData        <= (others=>'0');
    ShReg_SH_LDn        <= not(gLatchLevel);
    ShReg_CLK           <= '0';
    NewShRegData        <= '0';
    ShRegData           <= (others=>'0');
    
  elsif (Clock'event and Clock='1') then
    
    NewShRegData  <= '0';

    case fsm_state is

      when WaitEnable =>
        ShReg_CLK           <= '0';
        
        if (Enable='1') then
          fsm_state           <= SetSH_LDn;
          cnt_pulse           <= (others=>'0');
          cnt_Dt              <= (others=>'0');
          tmpShRegData        <= (others=>'0');
          ShReg_SH_LDn        <= gLatchLevel;
        end if;


      when SetSH_LDn =>

        if (cnt_pulse <  gSH_LDnTw-1) then
          cnt_pulse           <= cnt_pulse + 1;
        else                                           
          ShReg_SH_LDn        <= not(gLatchLevel);
          cnt_pulse           <= (others=>'0'); 
          fsm_state           <= UnsetCLK;
        end if;

      when UnsetCLK =>
        ShReg_CLK           <= '0';
        
        if (cnt_Dt < to_unsigned(gDataWidth,8)) then
      
          if (cnt_pulse <  gCLK_L_Tw-1) then
            cnt_pulse           <= cnt_pulse + 1;
          else                                           
            ShReg_CLK           <= '1';
            cnt_pulse           <= (others=>'0'); 
            fsm_state           <= SetCLK;
            
            tmpShRegData        <= tmpShRegData(gDataWidth-2 downto 0) & ShReg_DtIn;
          end if;
          
        else
            fsm_state           <= WaitEnable; 
            NewShRegData        <= '1';
            ShRegData           <= tmpShRegData;   
        
        end if;

      when SetCLK =>
      
        if (cnt_pulse <  gCLK_H_Tw-1) then
          cnt_pulse           <= cnt_pulse + 1;
        else                                           
          ShReg_CLK           <= '0';
          cnt_pulse           <= (others=>'0'); 
          --tmpShRegData        <= tmpShRegData(gDataWidth-2 downto 0) & ShReg_DtIn;
          cnt_Dt              <= cnt_Dt + 1;
          fsm_state           <= UnsetCLK;         
          
        end if;

      when others=>
          fsm_state           <= WaitEnable;
          cnt_pulse           <= (others=>'0');
          cnt_Dt              <= (others=>'0');
          tmpShRegData        <= (others=>'0');
          ShReg_SH_LDn        <= not(gLatchLevel);
          ShReg_CLK           <= '0';
          NewShRegData        <= '0';
          ShRegData           <= (others=>'0');

    end case;
  end if;
end process p_FSM;

end beh;

