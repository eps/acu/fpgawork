
module ACU_HPS_KdriverTest_Qsys (
	clk_clk,
	memory_mem_a,
	memory_mem_ba,
	memory_mem_ck,
	memory_mem_ck_n,
	memory_mem_cke,
	memory_mem_cs_n,
	memory_mem_ras_n,
	memory_mem_cas_n,
	memory_mem_we_n,
	memory_mem_reset_n,
	memory_mem_dq,
	memory_mem_dqs,
	memory_mem_dqs_n,
	memory_mem_odt,
	memory_mem_dm,
	memory_oct_rzqin,
	hps_io_hps_io_emac0_inst_TX_CLK,
	hps_io_hps_io_emac0_inst_TXD0,
	hps_io_hps_io_emac0_inst_TXD1,
	hps_io_hps_io_emac0_inst_TXD2,
	hps_io_hps_io_emac0_inst_TXD3,
	hps_io_hps_io_emac0_inst_RXD0,
	hps_io_hps_io_emac0_inst_MDIO,
	hps_io_hps_io_emac0_inst_MDC,
	hps_io_hps_io_emac0_inst_RX_CTL,
	hps_io_hps_io_emac0_inst_TX_CTL,
	hps_io_hps_io_emac0_inst_RX_CLK,
	hps_io_hps_io_emac0_inst_RXD1,
	hps_io_hps_io_emac0_inst_RXD2,
	hps_io_hps_io_emac0_inst_RXD3,
	hps_io_hps_io_sdio_inst_CMD,
	hps_io_hps_io_sdio_inst_D0,
	hps_io_hps_io_sdio_inst_D1,
	hps_io_hps_io_sdio_inst_D4,
	hps_io_hps_io_sdio_inst_D5,
	hps_io_hps_io_sdio_inst_D6,
	hps_io_hps_io_sdio_inst_D7,
	hps_io_hps_io_sdio_inst_CLK,
	hps_io_hps_io_sdio_inst_D2,
	hps_io_hps_io_sdio_inst_D3,
	hps_io_hps_io_usb1_inst_D0,
	hps_io_hps_io_usb1_inst_D1,
	hps_io_hps_io_usb1_inst_D2,
	hps_io_hps_io_usb1_inst_D3,
	hps_io_hps_io_usb1_inst_D4,
	hps_io_hps_io_usb1_inst_D5,
	hps_io_hps_io_usb1_inst_D6,
	hps_io_hps_io_usb1_inst_D7,
	hps_io_hps_io_usb1_inst_CLK,
	hps_io_hps_io_usb1_inst_STP,
	hps_io_hps_io_usb1_inst_DIR,
	hps_io_hps_io_usb1_inst_NXT,
	hps_io_hps_io_uart0_inst_RX,
	hps_io_hps_io_uart0_inst_TX,
	hps_io_hps_io_can0_inst_RX,
	hps_io_hps_io_can0_inst_TX,
	hps_reset_reset_n,
	fifo_fpga2hps_clk_in_clk,
	fifo_fpga2hps_reset_in_reset_n,
	fifo_hps2fpga_out_readdata,
	fifo_hps2fpga_out_read,
	fifo_hps2fpga_out_csr_address,
	fifo_hps2fpga_out_csr_read,
	fifo_hps2fpga_out_csr_writedata,
	fifo_hps2fpga_out_csr_write,
	fifo_hps2fpga_out_csr_readdata,
	fifo_hps2fpga_out_irq_irq,
	fifo_hps2fpga_clk_out_clk,
	fifo_hps2fpga_reset_out_reset_n,
	fifo_fpga2hps_in_writedata,
	fifo_fpga2hps_in_write,
	fifo_fpga2hps_in_csr_address,
	fifo_fpga2hps_in_csr_read,
	fifo_fpga2hps_in_csr_writedata,
	fifo_fpga2hps_in_csr_write,
	fifo_fpga2hps_in_csr_readdata,
	fifo_fpga2hps_in_irq_irq,
	pio_fifosourcesel_external_connection_export);	

	input		clk_clk;
	output	[14:0]	memory_mem_a;
	output	[2:0]	memory_mem_ba;
	output		memory_mem_ck;
	output		memory_mem_ck_n;
	output		memory_mem_cke;
	output		memory_mem_cs_n;
	output		memory_mem_ras_n;
	output		memory_mem_cas_n;
	output		memory_mem_we_n;
	output		memory_mem_reset_n;
	inout	[31:0]	memory_mem_dq;
	inout	[3:0]	memory_mem_dqs;
	inout	[3:0]	memory_mem_dqs_n;
	output		memory_mem_odt;
	output	[3:0]	memory_mem_dm;
	input		memory_oct_rzqin;
	output		hps_io_hps_io_emac0_inst_TX_CLK;
	output		hps_io_hps_io_emac0_inst_TXD0;
	output		hps_io_hps_io_emac0_inst_TXD1;
	output		hps_io_hps_io_emac0_inst_TXD2;
	output		hps_io_hps_io_emac0_inst_TXD3;
	input		hps_io_hps_io_emac0_inst_RXD0;
	inout		hps_io_hps_io_emac0_inst_MDIO;
	output		hps_io_hps_io_emac0_inst_MDC;
	input		hps_io_hps_io_emac0_inst_RX_CTL;
	output		hps_io_hps_io_emac0_inst_TX_CTL;
	input		hps_io_hps_io_emac0_inst_RX_CLK;
	input		hps_io_hps_io_emac0_inst_RXD1;
	input		hps_io_hps_io_emac0_inst_RXD2;
	input		hps_io_hps_io_emac0_inst_RXD3;
	inout		hps_io_hps_io_sdio_inst_CMD;
	inout		hps_io_hps_io_sdio_inst_D0;
	inout		hps_io_hps_io_sdio_inst_D1;
	inout		hps_io_hps_io_sdio_inst_D4;
	inout		hps_io_hps_io_sdio_inst_D5;
	inout		hps_io_hps_io_sdio_inst_D6;
	inout		hps_io_hps_io_sdio_inst_D7;
	output		hps_io_hps_io_sdio_inst_CLK;
	inout		hps_io_hps_io_sdio_inst_D2;
	inout		hps_io_hps_io_sdio_inst_D3;
	inout		hps_io_hps_io_usb1_inst_D0;
	inout		hps_io_hps_io_usb1_inst_D1;
	inout		hps_io_hps_io_usb1_inst_D2;
	inout		hps_io_hps_io_usb1_inst_D3;
	inout		hps_io_hps_io_usb1_inst_D4;
	inout		hps_io_hps_io_usb1_inst_D5;
	inout		hps_io_hps_io_usb1_inst_D6;
	inout		hps_io_hps_io_usb1_inst_D7;
	input		hps_io_hps_io_usb1_inst_CLK;
	output		hps_io_hps_io_usb1_inst_STP;
	input		hps_io_hps_io_usb1_inst_DIR;
	input		hps_io_hps_io_usb1_inst_NXT;
	input		hps_io_hps_io_uart0_inst_RX;
	output		hps_io_hps_io_uart0_inst_TX;
	input		hps_io_hps_io_can0_inst_RX;
	output		hps_io_hps_io_can0_inst_TX;
	output		hps_reset_reset_n;
	input		fifo_fpga2hps_clk_in_clk;
	input		fifo_fpga2hps_reset_in_reset_n;
	output	[31:0]	fifo_hps2fpga_out_readdata;
	input		fifo_hps2fpga_out_read;
	input	[2:0]	fifo_hps2fpga_out_csr_address;
	input		fifo_hps2fpga_out_csr_read;
	input	[31:0]	fifo_hps2fpga_out_csr_writedata;
	input		fifo_hps2fpga_out_csr_write;
	output	[31:0]	fifo_hps2fpga_out_csr_readdata;
	output		fifo_hps2fpga_out_irq_irq;
	input		fifo_hps2fpga_clk_out_clk;
	input		fifo_hps2fpga_reset_out_reset_n;
	input	[31:0]	fifo_fpga2hps_in_writedata;
	input		fifo_fpga2hps_in_write;
	input	[2:0]	fifo_fpga2hps_in_csr_address;
	input		fifo_fpga2hps_in_csr_read;
	input	[31:0]	fifo_fpga2hps_in_csr_writedata;
	input		fifo_fpga2hps_in_csr_write;
	output	[31:0]	fifo_fpga2hps_in_csr_readdata;
	output		fifo_fpga2hps_in_irq_irq;
	output	[31:0]	pio_fifosourcesel_external_connection_export;
endmodule
