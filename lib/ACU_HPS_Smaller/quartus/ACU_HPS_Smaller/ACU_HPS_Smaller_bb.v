
module ACU_HPS_Smaller (
	clk_clk,
	memory_mem_a,
	memory_mem_ba,
	memory_mem_ck,
	memory_mem_ck_n,
	memory_mem_cke,
	memory_mem_cs_n,
	memory_mem_ras_n,
	memory_mem_cas_n,
	memory_mem_we_n,
	memory_mem_reset_n,
	memory_mem_dq,
	memory_mem_dqs,
	memory_mem_dqs_n,
	memory_mem_odt,
	memory_mem_dm,
	memory_oct_rzqin,
	hps_io_hps_io_emac0_inst_TX_CLK,
	hps_io_hps_io_emac0_inst_TXD0,
	hps_io_hps_io_emac0_inst_TXD1,
	hps_io_hps_io_emac0_inst_TXD2,
	hps_io_hps_io_emac0_inst_TXD3,
	hps_io_hps_io_emac0_inst_RXD0,
	hps_io_hps_io_emac0_inst_MDIO,
	hps_io_hps_io_emac0_inst_MDC,
	hps_io_hps_io_emac0_inst_RX_CTL,
	hps_io_hps_io_emac0_inst_TX_CTL,
	hps_io_hps_io_emac0_inst_RX_CLK,
	hps_io_hps_io_emac0_inst_RXD1,
	hps_io_hps_io_emac0_inst_RXD2,
	hps_io_hps_io_emac0_inst_RXD3,
	hps_io_hps_io_sdio_inst_CMD,
	hps_io_hps_io_sdio_inst_D0,
	hps_io_hps_io_sdio_inst_D1,
	hps_io_hps_io_sdio_inst_D4,
	hps_io_hps_io_sdio_inst_D5,
	hps_io_hps_io_sdio_inst_D6,
	hps_io_hps_io_sdio_inst_D7,
	hps_io_hps_io_sdio_inst_CLK,
	hps_io_hps_io_sdio_inst_D2,
	hps_io_hps_io_sdio_inst_D3,
	hps_io_hps_io_usb1_inst_D0,
	hps_io_hps_io_usb1_inst_D1,
	hps_io_hps_io_usb1_inst_D2,
	hps_io_hps_io_usb1_inst_D3,
	hps_io_hps_io_usb1_inst_D4,
	hps_io_hps_io_usb1_inst_D5,
	hps_io_hps_io_usb1_inst_D6,
	hps_io_hps_io_usb1_inst_D7,
	hps_io_hps_io_usb1_inst_CLK,
	hps_io_hps_io_usb1_inst_STP,
	hps_io_hps_io_usb1_inst_DIR,
	hps_io_hps_io_usb1_inst_NXT,
	hps_io_hps_io_uart0_inst_RX,
	hps_io_hps_io_uart0_inst_TX,
	hps_io_hps_io_can0_inst_RX,
	hps_io_hps_io_can0_inst_TX,
	hps_io_hps_io_gpio_inst_GPIO48,
	hps_io_hps_io_gpio_inst_GPIO63,
	hps_io_hps_io_gpio_inst_GPIO64,
	hps_io_hps_io_gpio_inst_GPIO65,
	pio_led_out_export,
	onchip_ram_reset_reset,
	onchip_ram_reset_reset_req,
	onchip_ram_s1_address,
	onchip_ram_s1_clken,
	onchip_ram_s1_chipselect,
	onchip_ram_s1_write,
	onchip_ram_s1_readdata,
	onchip_ram_s1_writedata,
	onchip_ram_s1_byteenable,
	onchip_ram_clk1_clk,
	fifo_mm_irq_irq,
	fifo_mm_st_address,
	fifo_mm_st_read,
	fifo_mm_st_writedata,
	fifo_mm_st_write,
	fifo_mm_st_readdata,
	fifo_mm_in_writedata,
	fifo_mm_in_write,
	fifo_mm_reset_reset_n,
	fifo_mm_sysclk_clk,
	hps_0_h2f_reset_reset_n);	

	input		clk_clk;
	output	[14:0]	memory_mem_a;
	output	[2:0]	memory_mem_ba;
	output		memory_mem_ck;
	output		memory_mem_ck_n;
	output		memory_mem_cke;
	output		memory_mem_cs_n;
	output		memory_mem_ras_n;
	output		memory_mem_cas_n;
	output		memory_mem_we_n;
	output		memory_mem_reset_n;
	inout	[31:0]	memory_mem_dq;
	inout	[3:0]	memory_mem_dqs;
	inout	[3:0]	memory_mem_dqs_n;
	output		memory_mem_odt;
	output	[3:0]	memory_mem_dm;
	input		memory_oct_rzqin;
	output		hps_io_hps_io_emac0_inst_TX_CLK;
	output		hps_io_hps_io_emac0_inst_TXD0;
	output		hps_io_hps_io_emac0_inst_TXD1;
	output		hps_io_hps_io_emac0_inst_TXD2;
	output		hps_io_hps_io_emac0_inst_TXD3;
	input		hps_io_hps_io_emac0_inst_RXD0;
	inout		hps_io_hps_io_emac0_inst_MDIO;
	output		hps_io_hps_io_emac0_inst_MDC;
	input		hps_io_hps_io_emac0_inst_RX_CTL;
	output		hps_io_hps_io_emac0_inst_TX_CTL;
	input		hps_io_hps_io_emac0_inst_RX_CLK;
	input		hps_io_hps_io_emac0_inst_RXD1;
	input		hps_io_hps_io_emac0_inst_RXD2;
	input		hps_io_hps_io_emac0_inst_RXD3;
	inout		hps_io_hps_io_sdio_inst_CMD;
	inout		hps_io_hps_io_sdio_inst_D0;
	inout		hps_io_hps_io_sdio_inst_D1;
	inout		hps_io_hps_io_sdio_inst_D4;
	inout		hps_io_hps_io_sdio_inst_D5;
	inout		hps_io_hps_io_sdio_inst_D6;
	inout		hps_io_hps_io_sdio_inst_D7;
	output		hps_io_hps_io_sdio_inst_CLK;
	inout		hps_io_hps_io_sdio_inst_D2;
	inout		hps_io_hps_io_sdio_inst_D3;
	inout		hps_io_hps_io_usb1_inst_D0;
	inout		hps_io_hps_io_usb1_inst_D1;
	inout		hps_io_hps_io_usb1_inst_D2;
	inout		hps_io_hps_io_usb1_inst_D3;
	inout		hps_io_hps_io_usb1_inst_D4;
	inout		hps_io_hps_io_usb1_inst_D5;
	inout		hps_io_hps_io_usb1_inst_D6;
	inout		hps_io_hps_io_usb1_inst_D7;
	input		hps_io_hps_io_usb1_inst_CLK;
	output		hps_io_hps_io_usb1_inst_STP;
	input		hps_io_hps_io_usb1_inst_DIR;
	input		hps_io_hps_io_usb1_inst_NXT;
	input		hps_io_hps_io_uart0_inst_RX;
	output		hps_io_hps_io_uart0_inst_TX;
	input		hps_io_hps_io_can0_inst_RX;
	output		hps_io_hps_io_can0_inst_TX;
	inout		hps_io_hps_io_gpio_inst_GPIO48;
	inout		hps_io_hps_io_gpio_inst_GPIO63;
	inout		hps_io_hps_io_gpio_inst_GPIO64;
	inout		hps_io_hps_io_gpio_inst_GPIO65;
	output	[7:0]	pio_led_out_export;
	input		onchip_ram_reset_reset;
	input		onchip_ram_reset_reset_req;
	input	[9:0]	onchip_ram_s1_address;
	input		onchip_ram_s1_clken;
	input		onchip_ram_s1_chipselect;
	input		onchip_ram_s1_write;
	output	[31:0]	onchip_ram_s1_readdata;
	input	[31:0]	onchip_ram_s1_writedata;
	input	[3:0]	onchip_ram_s1_byteenable;
	input		onchip_ram_clk1_clk;
	output		fifo_mm_irq_irq;
	input	[2:0]	fifo_mm_st_address;
	input		fifo_mm_st_read;
	input	[31:0]	fifo_mm_st_writedata;
	input		fifo_mm_st_write;
	output	[31:0]	fifo_mm_st_readdata;
	input	[31:0]	fifo_mm_in_writedata;
	input		fifo_mm_in_write;
	input		fifo_mm_reset_reset_n;
	input		fifo_mm_sysclk_clk;
	output		hps_0_h2f_reset_reset_n;
endmodule
