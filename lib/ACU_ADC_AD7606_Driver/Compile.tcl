###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work

vmap altera_mf work
vmap cyclonev work




pwd
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_package_numericLib.vhd 
vcom -93 ../../../../SVN/FPGAWork/Quartus_111/lib/ACU_SPI/vhdl/ACU_Generic_4lines_SPI_Master.vhd

vcom -93 ../../SFRS_FW/vhdl/ADC_Takt.vhd
vcom -93 ../../SFRS_FW/vhdl/adc4_7606_6Ch_Serial.vhd 


vcom -93 $path/ACU_ADC_AD7606_Driver.vhd
vcom -93 $path/ACU_ADC_AD7608_Driver.vhd

vcom -93 $path/Tb_ACU_ADC_AD7606_Driver.vhd
