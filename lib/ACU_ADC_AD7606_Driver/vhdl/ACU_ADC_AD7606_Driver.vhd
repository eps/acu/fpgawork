-----------------------------------------------------------------------------------
-- File name: ACU_ADC_AD7606_Driver.vhd                                          --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 17/01/2024                                                         --
--                                                                               --
-- Comments : This file is a driver module for the three AD7606 chips mounted    --
--            on the SFRS ADC_Sensing board. A common StCnv, SCLK, ADC_Rst and   --
--            CSn signals are used for all the chips. Two serial data lines for  --
--            each chip (6 lines) are connected in input for the data fetching.  --
--            Channel 1 to 3 are fetched from DoutA line and 4 to 6 from DoutB.  -- 
--            The AD7606 chip is a 4/6/8 channels multiplexed 16b ADC.           --
--            This driver is compliant with the 6 channel version (but it is     --
--            easy to adapt it to the other two versions).                       --
--            The ACU_Generic_4lines_SPI_Master.vhd file is used as core module  --
--            to drive the SCLK and the CSn signals.                             --
--            StCnv and ADC_Rst are generated on this wrap file.                 --
--                                                                               --
--            Every time the Enable rising edge is detected, a gADC_RstLength    --
--            long ADC_Rst pulse is generated.                                   --
--            As soon as it goes low, after gADC_RstLength pulses the driver is  --
--            enabled.                                                           --
--            The StCnv signal goes low for gStCnvLowLength and after that the   --
--            driver waits for the conversion time (gConvTimeLength).            --
--            Please note that the busy signal from the ADC is not used, so      --
--            setup gConvTimeLength with some marging respect the chip data      --
--            sheet value.                                                       --
--            When the conversion is done, the CSn goes low and 3*16 SCLK pulses --
--            are generated. The SCLK period, adjustable via generic, is set to  --
--            100 ns (10 MHz) in order to be compliant with the differential     --
--            to single ended data delay transceivers used on the SFRS boards.   --
--            The 6 SDI lines in input are sampled on the SCLK falling edge.     --
--            The data rate, with the default generic values, is 111,11 KHz.     --    
--                                                                               --
-- History  : Start up version 17/01/2024                                        --
-----------------------------------------------------------------------------------
--            Added external timebase input(and functionality) 05/02/24 DR       --  
-----------------------------------------------------------------------------------
--            Fixed channl outputs 1 to 6 and not 3,2,1,6,5,4 29/08/24 DR        --
-----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.ACU_package_numericLib.all;

Entity ACU_ADC_AD7606_Driver is
  Generic (
         gADC_RstLength          : unsigned(3 downto 0):=x"A";                  -- ADC_Rst high pulse length(min 25ns, default=100ns)
         gStCnvLowLength         : unsigned(11 downto 0):=x"005";               -- StCnv low pulse length(min 25ns, default 50ns)
         gConvTimeLength         : unsigned(11 downto 0):=x"258";               -- Conversion time(min 3045ns, default 6000ns)
         gUseExtTimeBase         : std_logic:='0'                               -- When high, the driver uses an external time base (with a period longer than 9us), otherwise an
                                                                                -- internal free running timebase is used.
  );
  Port (
         Clock                   : in  std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                               -- Asynchronous reset signal active high
         
         Enable                  : in  std_logic;                               -- ADC_AD7606_Driver enable signal active high
         ExtTimeBase             : in  std_logic;                               -- ADC_AD7606_Driver external timebase pulse.

         -- SPI pins
         ADC_Rst                 : out std_logic;                               -- AD7606 reset signal active once when the Enable_RE is detected.
         SCS_n                   : out std_logic;                               -- AD7606 chip select signal (active low).
         StCnv                   : out std_logic;                               -- AD7606 start conversion signal.
         SCLK                    : out std_logic;                               -- AD7606 clock signal
         SDinA                   : in  std_logic_vector(2 downto 0);            -- AD7606 data A input signal (0=> ADC1; 1=> ADC2 and 2=> ADC3).
         SDinB                   : in  std_logic_vector(2 downto 0);            -- AD7606 data B input signal (0=> ADC1; 1=> ADC2 and 2=> ADC3).
         
         -- outputs
         NewAD7606_Dt            : out std_logic;                               -- New data out.
         AD7606_Dt_0             : out std_logic_vector(95 downto 0);           -- ADC1 data fetched ([95..80]=>channel6
                                                                                --                    [79..64]=>channel5
                                                                                --                    [63..48]=>channel4
                                                                                --                    [47..32]=>channel3
                                                                                --                    [31..16]=>channel2
                                                                                --                    [15..0] =>channel1)
         AD7606_Dt_1             : out std_logic_vector(95 downto 0);           -- ADC2 data fetched ([95..80]=>channel6
                                                                                --                    [79..64]=>channel5
                                                                                --                    [63..48]=>channel4
                                                                                --                    [47..32]=>channel3
                                                                                --                    [31..16]=>channel2
                                                                                --                    [15..0] =>channel1)
         AD7606_Dt_2             : out std_logic_vector(95 downto 0)            -- ADC3 data fetched ([95..80]=>channel6
                                                                                --                    [79..64]=>channel5
                                                                                --                    [63..48]=>channel4
                                                                                --                    [47..32]=>channel3
                                                                                --                    [31..16]=>channel2
                                                                                --                    [15..0] =>channel1)	 
  );
End entity ACU_ADC_AD7606_Driver;

Architecture beh of ACU_ADC_AD7606_Driver is
--
-- Constants declaration

							   
--
-- Signals declaration
signal Enable_s0                               : std_logic;                                 -- Enable sampled signal.
signal Enable_RE                               : std_logic;                                 -- Enable rising edge signal.
signal Enable_n                                : std_logic;                                 -- Inverted Enable signal.
signal EnDriver                                : std_logic;                                 -- Signal active high after 2xgADC_RstLength pulses used to enable the driver.
signal StartSPI                                : std_logic;                                 -- Pulse active high used to trigger the ACU_Generic_4lines_SPI_Master module.
signal EndProtocol                             : std_logic;                                 -- Signal used to flag the end of a SPI protocol.
signal FetchSDI                                : std_logic;                                 -- Signal used to sample SDinA and SDinB.
signal SDinA_sx                                : array3bits(3 downto 0);                    -- SDinA sampling vector.
signal SDinB_sx                                : array3bits(3 downto 0);                    -- SDinB sampling vector.
signal cntRst                                  : unsigned(3 downto 0);                      -- Reset internal counter.
signal cntGeneric                              : unsigned(11 downto 0);                     -- Generic internal counter.
signal tmpAD7606_DtA_0                         : std_logic_vector(47 downto 0);             -- Internal data fetch
signal tmpAD7606_DtB_0                         : std_logic_vector(47 downto 0);             -- Internal data fetch
signal tmpAD7606_DtA_1                         : std_logic_vector(47 downto 0);             -- Internal data fetch
signal tmpAD7606_DtB_1                         : std_logic_vector(47 downto 0);             -- Internal data fetch
signal tmpAD7606_DtA_2                         : std_logic_vector(47 downto 0);             -- Internal data fetch
signal tmpAD7606_DtB_2                         : std_logic_vector(47 downto 0);             -- Internal data fetch

signal ExtTimeBase_s0                          : std_logic;                                 -- ExtTimeBase sampled signal
signal ExtTimeBase_s1                          : std_logic;                                 -- ExtTimeBase_s0 sampled signal
signal ExtTimeBase_RE                          : std_logic;                                 -- ExtTimeBase rising edge signal


type fsmRstState is (WaitForEn,RstADC, EnADC_Driver);
signal fsmRst_state                            : fsmRstState;

type fsmState is (WaitForFlag, SetStCnv, WaitTconv, WaitSPI);
signal fsm_state                : fsmState;

--
-- Component
Component ACU_Generic_4lines_SPI_Master is
  Generic (
         gSCLK_High              : unsigned(7 downto 0):="00000011";                         -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0):="00000011";                         -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0):="00000011";                         -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
	 gDataWidth              : integer range 0 to 256:= 200;                             -- Data to send width. It has to be lower or equal to 
	                                                                                     -- nrSCLK_pulses signal value.
         gSDIonRE                : std_logic:='0';                                           -- When 0, SDI is sampled on SCLK or SCLK_BK falling edge else on
                                                                                             -- the rising edge
         gSDOonRE                : std_logic:='0';                                           -- When 0, SDO is shifted on SCLK falling edge else on
                                                                                             -- the rising edge
	 gUseSCLK_BK             : std_logic:='0';                                           -- When 0, SDI is sampled using SCLK as reference clock else SCLK_BK 
         gSCLK_startLevel        : std_logic:='0'                                            -- It defines the SCLK level after CSn goes low
                                                                                             
  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high
	 LostConnection          : in  std_logic;                                            -- When high, the protocol is aborted and the FSM reset
         NewDt_In                : in  std_logic;                                            -- One clock cycle pulse active high when there is a new Dt_In
	                                                                                     -- or when a read action has to start. 
         Dt_In                   : in std_logic_vector(gDataWidth-1 downto 0);               -- Data to send.

         nrSCLK_pulses           : in std_logic_vector (7 downto 0);                         -- Number of SCLK pulses for each NewVal received
         
         FetchSDI                : out std_logic;                                            -- It can be used to sample extra SDI lines not connected to this instance, but driven by SCLK.
         NewDt_Received          : out std_logic;                                            -- Pulse active high when there is a new Dt_Received.NOTE: It can be longer than one clock cycle, 
                                                                                             -- so it is better on the receiver side to detect the rising edge. 
         Dt_Received             : out std_logic_vector(gDataWidth-1 downto 0);              -- Data received.
	 
         SCLK                    : out std_logic;                                            -- SPI clock signal
         SCLK_BK                 : in std_logic;                                             -- SCLK loop back (on the slave chip side) signal
         SCS_n                   : out std_logic;                                            -- Active low SPI Chip Select signal
         SDI                     : in  std_logic;                                            -- Serial SPI data in (to FPGA) signal
         SDO                     : out std_logic                                             -- Serial SPI data out (from FPGA) signal
  );
end component ACU_Generic_4lines_SPI_Master;

begin

i_ACU_Generic_4lines_SPI_Master: ACU_Generic_4lines_SPI_Master
  Generic map(
         gSCLK_High               => x"06", 
         gSCLK_Low                => x"04",
         gCSn2SCLK_SetupTime      => x"08",
	 gDataWidth               => 48,
         gSDIonRE                 => '0',
         gSDOonRE                 => '0',
	 gUseSCLK_BK              => '0', 
         gSCLK_startLevel         => '1'                                                     
  )
  Port map(
         Clock                    => Clock,
         Reset                    => Reset,
	 LostConnection           => Enable_n,
         NewDt_In                 => StartSPI, 
         Dt_In                    => (others=>'0'),

         nrSCLK_pulses            => x"2F",
         
         FetchSDI                 => FetchSDI,
         NewDt_Received           => EndProtocol, 
         Dt_Received              => open,

         SCLK                     => SCLK,
         SCLK_BK                  => '0',
         SCS_n                    => SCS_n,
         SDI                      => '0',
         SDO                      => open
  );
  
p_samp: process(Clock,Reset)
begin
  if (Reset='1') then
    Enable_s0  <= '0';
    Enable_RE  <= '0';
    Enable_n   <= '0';
    SDinA_sx   <= (others=>(others=>'0'));
    SDinB_sx   <= (others=>(others=>'0'));
    
    ExtTimeBase_s0  <= '0';
    ExtTimeBase_s1  <= '0';
    ExtTimeBase_RE  <= '0';
    
  elsif(Clock'event and Clock='1') then
    Enable_s0  <= Enable;
    Enable_RE  <= Enable and not Enable_s0;
    Enable_n   <= not(Enable);
    SDinA_sx   <= SDinA_sx(2 downto 0) & SDinA;
    SDinB_sx   <= SDinB_sx(2 downto 0) & SDinB;

    ExtTimeBase_s0  <= ExtTimeBase;
    ExtTimeBase_s1  <= ExtTimeBase_s0;
    ExtTimeBase_RE  <= ExtTimeBase_s0 and not ExtTimeBase_s1;

  end if;
end process p_samp;

p_fsmRst: process(Clock,Reset)
begin
  if (Reset='1') then  
    fsmRst_state  <= WaitForEn;
    ADC_Rst       <= '0';
    cntRst        <= (others=>'0');
    EnDriver      <= '0';
    
  elsif(Clock'event and Clock='1') then
  
      EnDriver      <= '0';

  
    case fsmRst_state is
                
      when WaitForEn =>
      
        if (Enable_RE = '1') then
          fsmRst_state  <= RstADC;
          ADC_Rst       <= '1';
        end if;
      
      when RstADC => 

        if (cntRst < gADC_RstLength-1) then
          cntRst    <= cntRst + 1;
        else
          cntRst    <= (others=>'0');
          ADC_Rst       <= '0';
          fsmRst_state  <= EnADC_Driver;
        end if;
        
      when EnADC_Driver =>
      
        if (Enable_s0='0') then
          fsmRst_state  <= WaitForEn;
          cntRst        <= (others=>'0');
        end if;
        
        if (cntRst < gADC_RstLength-1) then
          cntRst      <= cntRst + 1;
        else
          EnDriver    <= '1';
        end if;
      
      when others =>
        fsmRst_state  <= WaitForEn;
        ADC_Rst       <= '0';
        cntRst        <= (others=>'0');
        EnDriver      <= '0';
      
    end case;
  end if;
end process p_fsmRst;

p_fsm: process(Clock,Reset)
begin
  if (Reset='1') then  
    fsm_state   <= WaitForFlag;
    StCnv       <= '1';
    cntGeneric  <= (others=>'0');
    StartSPI    <= '0';
                  
  elsif(Clock'event and Clock='1') then
  
    case fsm_state is
        
      
      when WaitForFlag =>
      
        if (EnDriver='1') then
        
          if (gUseExtTimeBase='1') then
            if (ExtTimeBase_RE='1') then
              fsm_state  <= SetStCnv;
              StCnv      <= '0';
            end if;
          else
            fsm_state  <= SetStCnv;
            StCnv      <= '0';
          end if;        
        end if;
      
      when SetStCnv =>
      
        if (cntGeneric < gStCnvLowLength-1) then
          cntGeneric  <= cntGeneric + 1;
        else
          cntGeneric  <= (others=>'0');    
          StCnv       <= '1';    
          fsm_state   <= WaitTconv;          
        end if; 
        
      when WaitTconv =>

        if (cntGeneric < gConvTimeLength-1) then
          cntGeneric  <= cntGeneric + 1;
        else
          cntGeneric  <= (others=>'0');    
          StartSPI    <= '1';    
          fsm_state   <= WaitSPI;          
        end if; 
      
      when WaitSPI =>           
          StartSPI    <= '0';   
          
          if (EndProtocol='1' or Enable_s0='0') then
             fsm_state   <= WaitForFlag;                   
          end if;
                 
      when others =>
        fsm_state   <= WaitForFlag;
        StCnv       <= '1';
        cntGeneric  <= (others=>'0');
        StartSPI    <= '0';
      
    end case;
  end if;
end process p_fsm;

p_dtFetch: process(Clock,Reset)
begin
  if (Reset='1') then  
    tmpAD7606_DtA_0  <= (others=>'0');   
    tmpAD7606_DtB_0  <= (others=>'0');                                     
    tmpAD7606_DtA_1  <= (others=>'0');   
    tmpAD7606_DtB_1  <= (others=>'0');                                     
    tmpAD7606_DtA_2  <= (others=>'0');   
    tmpAD7606_DtB_2  <= (others=>'0');   
                                      
    NewAD7606_Dt     <= '0';
    
    AD7606_Dt_0      <= (others=>'0');
    AD7606_Dt_1      <= (others=>'0');
    AD7606_Dt_2      <= (others=>'0');

  elsif(Clock'event and Clock='1') then
    
    NewAD7606_Dt     <= '0';  
    if (EndProtocol='1') then
       NewAD7606_Dt     <= '1';

       AD7606_Dt_0  <= tmpAD7606_DtB_0(15 downto 0) & tmpAD7606_DtB_0(31 downto 16) & tmpAD7606_DtB_0(47 downto 32) & 
                       tmpAD7606_DtA_0(15 downto 0) & tmpAD7606_DtA_0(31 downto 16) & tmpAD7606_DtA_0(47 downto 32);
       AD7606_Dt_1  <= tmpAD7606_DtB_1(15 downto 0) & tmpAD7606_DtB_1(31 downto 16) & tmpAD7606_DtB_1(47 downto 32) & 
                       tmpAD7606_DtA_1(15 downto 0) & tmpAD7606_DtA_1(31 downto 16) & tmpAD7606_DtA_1(47 downto 32);
       AD7606_Dt_2  <= tmpAD7606_DtB_2(15 downto 0) & tmpAD7606_DtB_2(31 downto 16) & tmpAD7606_DtB_2(47 downto 32) & 
                       tmpAD7606_DtA_2(15 downto 0) & tmpAD7606_DtA_2(31 downto 16) & tmpAD7606_DtA_2(47 downto 32);
       
    end if;
  
    --NewAD7606_Dt     <= EndProtocol;
    
    if (FetchSDI='1') then
      tmpAD7606_DtA_0  <= tmpAD7606_DtA_0(46 downto 0) & SDinA_sx(1)(0);
      tmpAD7606_DtA_1  <= tmpAD7606_DtA_1(46 downto 0) & SDinA_sx(1)(1);
      tmpAD7606_DtA_2  <= tmpAD7606_DtA_2(46 downto 0) & SDinA_sx(1)(2);

      tmpAD7606_DtB_0  <= tmpAD7606_DtB_0(46 downto 0) & SDinB_sx(1)(0);
      tmpAD7606_DtB_1  <= tmpAD7606_DtB_1(46 downto 0) & SDinB_sx(1)(1);
      tmpAD7606_DtB_2  <= tmpAD7606_DtB_2(46 downto 0) & SDinB_sx(1)(2);

    end if;
    
  end if;
end process p_dtFetch;

--Outputs

end beh;
