-----------------------------------------------------------------------------------
-- File name: Tb_ACU_ADC_AD7606_Driver.vhd                                       --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 17/01/2024                                                         --
--                                                                               --
-- Comments : ACU_ADC_AD7606_Driver test bench file.                             --    
--                                                                               --
-- History  : Start up version 17/01/2024                                        --
-----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity Tb_ACU_ADC_AD7606_Driver is
End entity Tb_ACU_ADC_AD7606_Driver;

Architecture beh of Tb_ACU_ADC_AD7606_Driver is
--
-- Constants declaration

							   
--
-- Signals declaration
signal Clock                               : std_logic:='0';                                 
signal Reset                               : std_logic:='1';                                 

signal tmpAD7606_DtA_0                     : std_logic_vector(47 downto 0):=x"123456789046";             -- Internal data fetch
signal tmpAD7606_DtB_0                     : std_logic_vector(47 downto 0):=x"ABCDEF123456";             -- Internal data fetch
signal tmpAD7606_DtA_1                     : std_logic_vector(47 downto 0):=x"123456ABCDEF";             -- Internal data fetch
signal tmpAD7606_DtB_1                     : std_logic_vector(47 downto 0):=x"CAFE4DAD1234";             -- Internal data fetch
signal tmpAD7606_DtA_2                     : std_logic_vector(47 downto 0):=x"1234CAFE4DAD";             -- Internal data fetch
signal tmpAD7606_DtB_2                     : std_logic_vector(47 downto 0):=x"DEADBEEF0123";             -- Internal data fetch

signal Enable                              : std_logic:='0';    
signal SCLK                                : std_logic;

signal SDinA                               : std_logic_vector(2 downto 0);
signal SDinB                               : std_logic_vector(2 downto 0);

signal Start_ADC                           : std_logic;

signal tmpAD7606_DtA_0_ExtTb               : std_logic_vector(47 downto 0):=x"123456789046";             -- Internal data fetch
signal tmpAD7606_DtB_0_ExtTb               : std_logic_vector(47 downto 0):=x"ABCDEF123456";             -- Internal data fetch
signal tmpAD7606_DtA_1_ExtTb               : std_logic_vector(47 downto 0):=x"123456ABCDEF";             -- Internal data fetch
signal tmpAD7606_DtB_1_ExtTb               : std_logic_vector(47 downto 0):=x"CAFE4DAD1234";             -- Internal data fetch
signal tmpAD7606_DtA_2_ExtTb               : std_logic_vector(47 downto 0):=x"1234CAFE4DAD";             -- Internal data fetch
signal tmpAD7606_DtB_2_ExtTb               : std_logic_vector(47 downto 0):=x"DEADBEEF0123";             -- Internal data fetch
signal SCLK_ExtTb                          : std_logic;

signal SDinA_ExtTb                         : std_logic_vector(2 downto 0);
signal SDinB_ExtTb                         : std_logic_vector(2 downto 0);

signal SCLK_7608                           : std_logic;
signal SDinA_7608                          : std_logic_vector(2 downto 0);
signal SDinB_7608                          : std_logic_vector(2 downto 0);
  
signal tmpAD7608_DtA_0                     : std_logic_vector(53 downto 0);
signal tmpAD7608_DtB_0                     : std_logic_vector(53 downto 0);
signal tmpAD7608_DtA_1                     : std_logic_vector(53 downto 0);
signal tmpAD7608_DtB_1                     : std_logic_vector(53 downto 0);
signal tmpAD7608_DtA_2                     : std_logic_vector(53 downto 0);
signal tmpAD7608_DtB_2                     : std_logic_vector(53 downto 0);
  

Component ADC_Takt IS
    
GENERIC (
   clk_input             : integer  := 100_000_000; -- 100MHz system clock  
   ckl_time_FIR          : integer  := 125;         -- 1,25us x 2= 2,5us -> 400kHz
   clk_time_BandPass_FIR : integer  := 111;         -- 
   clk_time_DAC          : integer  := 500;         -- 5us (als DAC Takt       -> kurzer Puls 10ns)
   ADC_schleifen         : integer  := 2;           -- 5*2= 10us als ADC Takt  -> kurzer Puls 10ns 
   Regler_schleifen      : integer  := 12);                                               
    
PORT(
   reset                 : IN std_logic;            -- System   
   clk                   : IN std_logic;
   Enable                : IN std_logic;            -- '0'Ausgang ist ='0'
   
   PWM_Takt_Enable       : IN std_logic; 
   PWM_Takt              : IN std_logic; 
                                                    --Ausgang
   clk_out_DAC,                                     -- DAC TAkt
   clk_out_ADC,                                     -- ADC Takt
   clk_out_Regler,
   clk_out_FIR,
   clk_BandPass_FIR      : OUT std_logic);
            
END Component ADC_Takt ;

--
-- Component
Component adc4_7606_6Ch_Serial is                        					-- Entity Declaration

GENERIC (
   	clk_in_hz     		: integer  := 100_000_000;    				-- 100MHz system clock  
	c_treset		: integer  := 5;             				   -- RESET high pulse with (treset, Anzahl der Takte
	c_conv_wait     	: integer  := 5;             				   -- Minimum delay between Reset low to convst high     (t7)
	c_busy_read_wait	: integer  := 600;             				-- Zeit bevor man die Busy Leitung zurückliesst
	c_tslck_2		: integer  := 3;             			   	-- Zeit für den high oder lowzustand des slck -Signals
	c_t4			: integer  := 3;             			   	-- Zeit t4 zw. busy=0 and cs =0
	c_t23			: integer  := 3;             			   	-- Zeit t23 zw. alle bits sind geschriebend cs =1
	c_bit_count		: integer  := 47);             			   -- Anzahl aller ADC bits 6*16=96/2 -1
   
PORT (
   	clk           		: in  std_logic;                        	-- Clock Signal für FPGA Entity
   	reset         	 	: in  std_logic;                        	-- reset Signal für FPGA Entity
   	Enable	      		: in  std_logic;                        	-- Startet den Konvertierungsablauf
   	
   	-- Ausgänge der ausgewerteten Kanäle  
   	adc1_channel_1   	: out std_logic_vector (15 downto 0):= (others => '0');   
   	adc1_channel_2    	: out std_logic_vector (15 downto 0):= (others => '0');
   	adc1_channel_3    	: out std_logic_vector (15 downto 0):= (others => '0');
   	adc1_channel_4    	: out std_logic_vector (15 downto 0):= (others => '0');
	adc1_channel_5    	: out std_logic_vector (15 downto 0):= (others => '0');
	adc1_channel_6    	: out std_logic_vector (15 downto 0):= (others => '0');
	
	adc2_channel_1    	: out std_logic_vector (15 downto 0):= (others => '0');   
   	adc2_channel_2    	: out std_logic_vector (15 downto 0):= (others => '0');
   	adc2_channel_3    	: out std_logic_vector (15 downto 0):= (others => '0');
   	adc2_channel_4    	: out std_logic_vector (15 downto 0):= (others => '0');
	adc2_channel_5    	: out std_logic_vector (15 downto 0):= (others => '0');
	adc2_channel_6    	: out std_logic_vector (15 downto 0):= (others => '0');
	
	adc3_channel_1    	: out std_logic_vector (15 downto 0):= (others => '0');   
   	adc3_channel_2    	: out std_logic_vector (15 downto 0):= (others => '0');
   	adc3_channel_3    	: out std_logic_vector (15 downto 0):= (others => '0');
   	adc3_channel_4    	: out std_logic_vector (15 downto 0):= (others => '0');
	adc3_channel_5    	: out std_logic_vector (15 downto 0):= (others => '0');
	adc3_channel_6    	: out std_logic_vector (15 downto 0):= (others => '0');
	
	adc4_channel_1    	: out std_logic_vector (15 downto 0):= (others => '0');   
   	adc4_channel_2    	: out std_logic_vector (15 downto 0):= (others => '0');
   	adc4_channel_3    	: out std_logic_vector (15 downto 0):= (others => '0');
   	adc4_channel_4    	: out std_logic_vector (15 downto 0):= (others => '0');
	adc4_channel_5    	: out std_logic_vector (15 downto 0):= (others => '0');
	adc4_channel_6    	: out std_logic_vector (15 downto 0):= (others => '0');
		
	--Anschlüse an ADC
   	adc1_DOUTA        	: in  std_logic;                        	-- serial data transfer V1,V2,V3
	adc1_DOUTB        	: in  std_logic;                        	-- serial data transfer V4,V5,V6
	
   	adc2_DOUTA        	: in  std_logic;                        	-- serial data transfer V1,V2,V3
	adc2_DOUTB       	: in  std_logic;                        	-- serial data transfer V4,V5,V6
	
   	adc3_DOUTA        	: in  std_logic;                        	-- serial data transfer V1,V2,V3
	adc3_DOUTB        	: in  std_logic;                        	-- serial data transfer V4,V5,V6
	
	adc4_DOUTA        	: in  std_logic;                        	-- serial data transfer V1,V2,V3
	adc4_DOUTB        	: in  std_logic;                        	-- serial data transfer V4,V5,V6

   	n_cs          		: out std_logic;                        	-- chip select enables tri state databus
   	n_sclk        		: out std_logic;                        	-- fireset falling edge after busy clocks data out
  	start_conv 		: out std_logic;                        	-- fireset falling edge after busy clocks data out
   	adc_reset     		: out std_logic;                        	-- Startet die ADC Konversion
   	Data_Out_Valid_ADC	: out std_logic);     			 				-- ='1' falls alles fertig 
  
END Component adc4_7606_6Ch_Serial;  


Component ACU_ADC_AD7606_Driver is
  Generic (
         gADC_RstLength          : unsigned(3 downto 0):=x"A";                  -- ADC_Rst high pulse length(min 25ns, default=100ns)
         gStCnvLowLength         : unsigned(11 downto 0):=x"005";               -- StCnv low pulse length(min 25ns, default 50ns)
         gConvTimeLength         : unsigned(11 downto 0):=x"258";               -- Conversion time(min 3045ns, default 6000ns)
         gUseExtTimeBase         : std_logic:='0'                               -- When high, the driver uses an external time base (with a period longer than 9us), otherwise an
                                                                                -- internal free running timebase is used.
  );
  Port (
         Clock                   : in  std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                               -- Asynchronous reset signal active high
         
         Enable                  : in  std_logic;                               -- ADC_AD7606_Driver enable signal active high
         ExtTimeBase             : in  std_logic;                               -- ADC_AD7606_Driver external timebase pulse.

         -- SPI pins
         ADC_Rst                 : out std_logic;                               -- AD7606 reset signal active once when the Enable_RE is detected.
         SCS_n                   : out std_logic;                               -- AD7606 chip select signal (active low).
         StCnv                   : out std_logic;                               -- AD7606 start conversion signal.
         SCLK                    : out std_logic;                               -- AD7606 clock signal
         SDinA                   : in  std_logic_vector(2 downto 0);            -- AD7606 data A input signal (0=> ADC1; 1=> ADC2 and 2=> ADC3).
         SDinB                   : in  std_logic_vector(2 downto 0);            -- AD7606 data B input signal (0=> ADC1; 1=> ADC2 and 2=> ADC3).
         
         -- outputs
         NewAD7606_Dt            : out std_logic;                               -- New data out.
         AD7606_Dt_0             : out std_logic_vector(95 downto 0);           -- ADC1 data fetched ([95..80]=>channel6
                                                                                --                    [79..64]=>channel5
                                                                                --                    [63..48]=>channel1
                                                                                --                    [47..32]=>channel3
                                                                                --                    [31..16]=>channel2
                                                                                --                    [15..0] =>channel1)
         AD7606_Dt_1             : out std_logic_vector(95 downto 0);           -- ADC2 data fetched ([95..80]=>channel6
                                                                                --                    [79..64]=>channel5
                                                                                --                    [63..48]=>channel1
                                                                                --                    [47..32]=>channel3
                                                                                --                    [31..16]=>channel2
                                                                                --                    [15..0] =>channel1)
         AD7606_Dt_2             : out std_logic_vector(95 downto 0)            -- ADC3 data fetched ([95..80]=>channel6
                                                                                --                    [79..64]=>channel5
                                                                                --                    [63..48]=>channel1
                                                                                --                    [47..32]=>channel3
                                                                                --                    [31..16]=>channel2
                                                                                --                    [15..0] =>channel1)	 
  );
end component ACU_ADC_AD7606_Driver;


Component ACU_ADC_AD7608_Driver is
  Generic (
         gADC_RstLength          : unsigned(3 downto 0):=x"A";                  -- ADC_Rst high pulse length(min 25ns, default=100ns)
         gStCnvLowLength         : unsigned(11 downto 0):=x"005";               -- StCnv low pulse length(min 25ns, default 50ns)
         gConvTimeLength         : unsigned(11 downto 0):=x"258";               -- Conversion time(min 3045ns, default 6000ns)
         gUseExtTimeBase         : std_logic:='0'                               -- When high, the driver uses an external time base (with a period longer than 9us), otherwise an
                                                                                -- internal free running timebase is used.
  );
  Port (
         Clock                   : in  std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                               -- Asynchronous reset signal active high
         
         Enable                  : in  std_logic;                               -- ADC_AD7606_Driver enable signal active high
         ExtTimeBase             : in  std_logic;                               -- ADC_AD7606_Driver external timebase pulse.

         -- SPI pins
         ADC_Rst                 : out std_logic;                               -- AD7606 reset signal active once when the Enable_RE is detected.
         SCS_n                   : out std_logic;                               -- AD7606 chip select signal (active low).
         StCnv                   : out std_logic;                               -- AD7606 start conversion signal.
         SCLK                    : out std_logic;                               -- AD7606 clock signal
         SDinA                   : in  std_logic_vector(2 downto 0);            -- AD7606 data A input signal (0=> ADC1; 1=> ADC2 and 2=> ADC3).
         SDinB                   : in  std_logic_vector(2 downto 0);            -- AD7606 data B input signal (0=> ADC1; 1=> ADC2 and 2=> ADC3).
         
         -- outputs
         NewAD7608_Dt            : out std_logic;                               -- New data out.
         AD7608_Dt_0             : out std_logic_vector(107 downto 0);          -- ADC1 data fetched ([107..90]=>channel6
                                                                                --                    [89..72]=>channel5
                                                                                --                    [71..54]=>channel1
                                                                                --                    [53..36]=>channel3
                                                                                --                    [35..18]=>channel2
                                                                                --                    [17..0] =>channel1)
         AD7608_Dt_1             : out std_logic_vector(107 downto 0);          -- ADC2 data fetched ([107..90]=>channel6
                                                                                --                    [89..72]=>channel5
                                                                                --                    [71..54]=>channel1
                                                                                --                    [53..36]=>channel3
                                                                                --                    [35..18]=>channel2
                                                                                --                    [17..0] =>channel1)
         AD7608_Dt_2             : out std_logic_vector(107 downto 0)           -- ADC3 data fetched ([107..90]=>channel6
                                                                                --                    [[89..72]=>channel5
                                                                                --                    [71..54]=>channel1
                                                                                --                    [53..36]=>channel3
                                                                                --                    [35..18]=>channel2
                                                                                --                    [17..0] =>channel1) 
  );
End component ACU_ADC_AD7608_Driver;

begin

Clock  <= not(Clock) after 5 ns;

i_ADC_Takt: ADC_Takt
    
PORT MAP(
   reset                => Reset,   
   clk                  => Clock,
   Enable               => '1',
   
   PWM_Takt_Enable      => '0', 
   PWM_Takt             => '0', 
                                                    --Ausgang
   clk_out_DAC		=> open,
   clk_out_ADC		=> Start_ADC,                                     -- ADC Takt
   clk_out_Regler	=> open,
   clk_out_FIR		=> open,
   clk_BandPass_FIR 	=> open
);



i_adc4_7606_6Ch_Serial: adc4_7606_6Ch_Serial                        					-- Entity Declaration


PORT MAP (
   	clk           		=> Clock,
   	reset         	 	=> Reset,  
   	Enable	      		=> Start_ADC,
   	
   	-- Ausgänge der ausgewerteten Kanäle  
   	adc1_channel_1   	=> open,
   	adc1_channel_2    	=> open,
   	adc1_channel_3    	=> open,
   	adc1_channel_4    	=> open,
	adc1_channel_5    	=> open,
	adc1_channel_6    	=> open,
	
	adc2_channel_1    	=> open,
   	adc2_channel_2    	=> open,
   	adc2_channel_3    	=> open,
   	adc2_channel_4    	=> open,
	adc2_channel_5    	=> open,
	adc2_channel_6    	=> open,
	
	adc3_channel_1    	=> open,
   	adc3_channel_2    	=> open,
   	adc3_channel_3    	=> open,
   	adc3_channel_4    	=> open,
	adc3_channel_5    	=> open,
	adc3_channel_6    	=> open,
	
	adc4_channel_1    	=> open,
   	adc4_channel_2    	=> open,
   	adc4_channel_3    	=> open,
   	adc4_channel_4    	=> open,
	adc4_channel_5    	=> open,
	adc4_channel_6    	=> open,
		
	--Anschlüse an ADC
   	adc1_DOUTA        	=> '0',
	adc1_DOUTB        	=> '0',
	
   	adc2_DOUTA        	=> '0',
	adc2_DOUTB       	=> '0',
	
   	adc3_DOUTA        	=> '0',
	adc3_DOUTB        	=> '0',
	
	adc4_DOUTA        	=> '0',
	adc4_DOUTB        	=> '0',
	
   	n_cs          		=> open,
   	n_sclk        		=> open,
  	start_conv 		=> open,
   	adc_reset     		=> open,
   	Data_Out_Valid_ADC	=> open
  ); 



i_ACU_ADC_AD7606_Driver: ACU_ADC_AD7606_Driver
  Generic map(
    gConvTimeLength              =>x"190"
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         
         Enable                  => Enable,
         ExtTimeBase             => '0',             

         -- SPI pins
         ADC_Rst                 => open,
         SCS_n                   => open,
         StCnv                   => open,
         SCLK                    => SCLK,
         SDinA                   => SDinA,
         SDinB                   => SDinB,
         
         -- outputs
         NewAD7606_Dt            => open,
         AD7606_Dt_0             => open,
         AD7606_Dt_1             => open,
         AD7606_Dt_2             => open	 
  );
  
p_sclk:process(SCLK,Reset,Enable)
begin
  if (Reset='1'or Enable='0') then
    
    tmpAD7606_DtA_0  <= x"123456789046";
    tmpAD7606_DtB_0  <= x"ABCDEF123456";
    tmpAD7606_DtA_1  <= x"123456ABCDEF";
    tmpAD7606_DtB_1  <= x"CAFE4DAD1234";
    tmpAD7606_DtA_2  <= x"1234CAFE4DAD";
    tmpAD7606_DtB_2  <= x"DEADBEEF0123";
        
  elsif(SCLK' event and SCLK='1') then
  
    tmpAD7606_DtA_0  <= tmpAD7606_DtA_0(46 downto 0) & tmpAD7606_DtA_0(47);
    tmpAD7606_DtB_0  <= tmpAD7606_DtB_0(46 downto 0) & tmpAD7606_DtB_0(47);
    tmpAD7606_DtA_1  <= tmpAD7606_DtA_1(46 downto 0) & tmpAD7606_DtA_1(47);
    tmpAD7606_DtB_1  <= tmpAD7606_DtB_1(46 downto 0) & tmpAD7606_DtB_1(47);
    tmpAD7606_DtA_2  <= tmpAD7606_DtA_2(46 downto 0) & tmpAD7606_DtA_2(47);
    tmpAD7606_DtB_2  <= tmpAD7606_DtB_2(46 downto 0) & tmpAD7606_DtB_2(47);
  
  end if;
end process p_sclk;

SDinA  <= tmpAD7606_DtA_2(47) & tmpAD7606_DtA_1(47) & tmpAD7606_DtA_0(47);
SDinB  <= tmpAD7606_DtB_2(47) & tmpAD7606_DtB_1(47) & tmpAD7606_DtB_0(47);

---------------------------------------------------------------------------------------
i_ACU_ADC_AD7606_Driver_ExtTb: ACU_ADC_AD7606_Driver
  Generic map(
    gUseExtTimeBase              => '1',
    gConvTimeLength              =>x"190"
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         
         Enable                  => Enable,
         ExtTimeBase             => Start_ADC,   
                   
         -- SPI pins
         ADC_Rst                 => open,
         SCS_n                   => open,
         StCnv                   => open,
         SCLK                    => SCLK_ExtTb,
         SDinA                   => SDinA_ExtTb,
         SDinB                   => SDinB_ExtTb,
         
         -- outputs
         NewAD7606_Dt            => open,
         AD7606_Dt_0             => open,
         AD7606_Dt_1             => open,
         AD7606_Dt_2             => open	 
  );
  
p_sclk_ExtTb:process(SCLK_ExtTb,Reset,Enable)
begin
  if (Reset='1'or Enable='0') then
    
    tmpAD7606_DtA_0_ExtTb  <= x"123456789046";
    tmpAD7606_DtB_0_ExtTb  <= x"ABCDEF123456";
    tmpAD7606_DtA_1_ExtTb  <= x"123456ABCDEF";
    tmpAD7606_DtB_1_ExtTb  <= x"CAFE4DAD1234";
    tmpAD7606_DtA_2_ExtTb  <= x"1234CAFE4DAD";
    tmpAD7606_DtB_2_ExtTb  <= x"DEADBEEF0123";
        
  elsif(SCLK_ExtTb' event and SCLK_ExtTb='1') then
  
    tmpAD7606_DtA_0_ExtTb  <= tmpAD7606_DtA_0_ExtTb(46 downto 0) & tmpAD7606_DtA_0_ExtTb(47);
    tmpAD7606_DtB_0_ExtTb  <= tmpAD7606_DtB_0_ExtTb(46 downto 0) & tmpAD7606_DtB_0_ExtTb(47);
    tmpAD7606_DtA_1_ExtTb  <= tmpAD7606_DtA_1_ExtTb(46 downto 0) & tmpAD7606_DtA_1_ExtTb(47);
    tmpAD7606_DtB_1_ExtTb  <= tmpAD7606_DtB_1_ExtTb(46 downto 0) & tmpAD7606_DtB_1_ExtTb(47);
    tmpAD7606_DtA_2_ExtTb  <= tmpAD7606_DtA_2_ExtTb(46 downto 0) & tmpAD7606_DtA_2_ExtTb(47);
    tmpAD7606_DtB_2_ExtTb  <= tmpAD7606_DtB_2_ExtTb(46 downto 0) & tmpAD7606_DtB_2_ExtTb(47);
  
  end if;
end process p_sclk_ExtTb;

SDinA_ExtTb  <= tmpAD7606_DtA_2_ExtTb(47) & tmpAD7606_DtA_1_ExtTb(47) & tmpAD7606_DtA_0_ExtTb(47);
SDinB_ExtTb  <= tmpAD7606_DtB_2_ExtTb(47) & tmpAD7606_DtB_1_ExtTb(47) & tmpAD7606_DtB_0_ExtTb(47);


i_ACU_ADC_AD7608_DriverExtTb:ACU_ADC_AD7608_Driver 
  Generic map(
    gUseExtTimeBase              => '1',
    gStCnvLowLength              => x"004",               -- StCnv low pulse length(min 25ns, default 50ns)    
    gConvTimeLength              => x"1BD"                                       -- 4450 ns
  )
  Port map(
         Clock                   => Clock,
         Reset                   => Reset,
         
         Enable                  => Enable,
         ExtTimeBase             => Start_ADC,   
                   
         -- SPI pins
         ADC_Rst                 => open,
         SCS_n                   => open,
         StCnv                   => open,
         SCLK                    => SCLK_7608,
         SDinA                   => SDinA_7608,
         SDinB                   => SDinB_7608,
                  
         -- outputs
         NewAD7608_Dt            => open,
         AD7608_Dt_0             => open,
         AD7608_Dt_1             => open,
         AD7608_Dt_2             => open	 
  );
  
p_sclk_7608:process(SCLK_7608,Reset,Enable)
begin
  if (Reset='1'or Enable='0') then
    
    tmpAD7608_DtA_0  <= "01" & x"A" & x"123456789046";
    tmpAD7608_DtB_0  <= "11" & x"B" & x"ABCDEF123456";
    tmpAD7608_DtA_1  <= "00" & x"C" & x"123456ABCDEF";
    tmpAD7608_DtB_1  <= "01" & x"D" & x"CAFE4DAD1234";
    tmpAD7608_DtA_2  <= "11" & x"E" & x"1234CAFE4DAD";
    tmpAD7608_DtB_2  <= "10" & x"F" & x"DEADBEEF0123";
        
  elsif(SCLK_7608' event and SCLK_7608='1') then
  
    tmpAD7608_DtA_0  <= tmpAD7608_DtA_0(52 downto 0) & tmpAD7608_DtA_0(53);
    tmpAD7608_DtB_0  <= tmpAD7608_DtB_0(52 downto 0) & tmpAD7608_DtB_0(53);
    tmpAD7608_DtA_1  <= tmpAD7608_DtA_1(52 downto 0) & tmpAD7608_DtA_1(53);
    tmpAD7608_DtB_1  <= tmpAD7608_DtB_1(52 downto 0) & tmpAD7608_DtB_1(53);
    tmpAD7608_DtA_2  <= tmpAD7608_DtA_2(52 downto 0) & tmpAD7608_DtA_2(53);
    tmpAD7608_DtB_2  <= tmpAD7608_DtB_2(52 downto 0) & tmpAD7608_DtB_2(53);
  
  end if;
end process p_sclk_7608;

SDinA_7608  <= tmpAD7608_DtA_2(53) & tmpAD7608_DtA_1(53) & tmpAD7608_DtA_0(53);
SDinB_7608  <= tmpAD7608_DtB_2(53) & tmpAD7608_DtB_1(53) & tmpAD7608_DtB_0(53);
  
end beh;
