-----------------------------------------------------------------------------------
-- File name: ACU_ADC_AD7608_Driver.vhd                                          --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 28/02/2024                                                         --
--                                                                               --
-- Comments : This file is a driver module for the three AD7608 chips.           --
--            It is derived from the ACU_ADC_AD7606_Driver with the following    --
--            differences:                                                       --
--              * 18b pro channels are fetched                                   --
--              * Even if it is a 8 channels ADC, only the first 6 are fetched.  --
--              * the conversion time is set to 4,5us (300ns more than the max)  --
--            For more info, please refer to the ACU_ADC_AD7606_Driver file.     --    
--                                                                               --
-- History  : Start up version 28/02/2024                                        --
-----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.ACU_package_numericLib.all;

Entity ACU_ADC_AD7608_Driver is
  Generic (
         gADC_RstLength          : unsigned(3 downto 0):=x"A";                  -- ADC_Rst high pulse length(min 25ns, default=100ns)
         gStCnvLowLength         : unsigned(11 downto 0):=x"005";               -- StCnv low pulse length(min 25ns, default 50ns)
         gConvTimeLength         : unsigned(11 downto 0):=x"258";               -- Conversion time(min 3045ns, default 6000ns)
         gUseExtTimeBase         : std_logic:='0'                               -- When high, the driver uses an external time base (with a period longer than 9us), otherwise an
                                                                                -- internal free running timebase is used.
  );
  Port (
         Clock                   : in  std_logic;                               -- Clock signal (usually @100MHz)
         Reset                   : in  std_logic;                               -- Asynchronous reset signal active high
         
         Enable                  : in  std_logic;                               -- ADC_AD7608_Driver enable signal active high
         ExtTimeBase             : in  std_logic;                               -- ADC_AD7608_Driver external timebase pulse.

         -- SPI pins
         ADC_Rst                 : out std_logic;                               -- AD7608 reset signal active once when the Enable_RE is detected.
         SCS_n                   : out std_logic;                               -- AD7608 chip select signal (active low).
         StCnv                   : out std_logic;                               -- AD7608 start conversion signal.
         SCLK                    : out std_logic;                               -- AD7608 clock signal
         SDinA                   : in  std_logic_vector(2 downto 0);            -- AD7608 data A input signal (0=> ADC1; 1=> ADC2 and 2=> ADC3).
         SDinB                   : in  std_logic_vector(2 downto 0);            -- AD7608 data B input signal (0=> ADC1; 1=> ADC2 and 2=> ADC3).
         
         -- outputs
         NewAD7608_Dt            : out std_logic;                               -- New data out.
         AD7608_Dt_0             : out std_logic_vector(107 downto 0);          -- ADC1 data fetched ([107..90]=>channel6
                                                                                --                    [89..72]=>channel5
                                                                                --                    [71..54]=>channel4
                                                                                --                    [53..36]=>channel3
                                                                                --                    [35..18]=>channel2
                                                                                --                    [17..0] =>channel1)
         AD7608_Dt_1             : out std_logic_vector(107 downto 0);          -- ADC2 data fetched ([107..90]=>channel6
                                                                                --                    [89..72]=>channel5
                                                                                --                    [71..54]=>channel4
                                                                                --                    [53..36]=>channel3
                                                                                --                    [35..18]=>channel2
                                                                                --                    [17..0] =>channel1)
         AD7608_Dt_2             : out std_logic_vector(107 downto 0)           -- ADC3 data fetched ([107..90]=>channel6
                                                                                --                    [[89..72]=>channel5
                                                                                --                    [71..54]=>channel4
                                                                                --                    [53..36]=>channel3
                                                                                --                    [35..18]=>channel2
                                                                                --                    [17..0] =>channel1) 
  );
End entity ACU_ADC_AD7608_Driver;

Architecture beh of ACU_ADC_AD7608_Driver is
--
-- Constants declaration

							   
--
-- Signals declaration
signal Enable_s0                               : std_logic;                                 -- Enable sampled signal.
signal Enable_RE                               : std_logic;                                 -- Enable rising edge signal.
signal Enable_n                                : std_logic;                                 -- Inverted Enable signal.
signal EnDriver                                : std_logic;                                 -- Signal active high after 2xgADC_RstLength pulses used to enable the driver.
signal StartSPI                                : std_logic;                                 -- Pulse active high used to trigger the ACU_Generic_4lines_SPI_Master module.
signal EndProtocol                             : std_logic;                                 -- Signal used to flag the end of a SPI protocol.
signal FetchSDI                                : std_logic;                                 -- Signal used to sample SDinA and SDinB.
signal SDinA_sx                                : array3bits(3 downto 0);                    -- SDinA sampling vector.
signal SDinB_sx                                : array3bits(3 downto 0);                    -- SDinB sampling vector.
signal cntRst                                  : unsigned(3 downto 0);                      -- Reset internal counter.
signal cntGeneric                              : unsigned(11 downto 0);                     -- Generic internal counter.
signal tmpAD7608_DtA_0                         : std_logic_vector(53 downto 0);             -- Internal data fetch
signal tmpAD7608_DtB_0                         : std_logic_vector(53 downto 0);             -- Internal data fetch
signal tmpAD7608_DtA_1                         : std_logic_vector(53 downto 0);             -- Internal data fetch
signal tmpAD7608_DtB_1                         : std_logic_vector(53 downto 0);             -- Internal data fetch
signal tmpAD7608_DtA_2                         : std_logic_vector(53 downto 0);             -- Internal data fetch
signal tmpAD7608_DtB_2                         : std_logic_vector(53 downto 0);             -- Internal data fetch

signal ExtTimeBase_s0                          : std_logic;                                 -- ExtTimeBase sampled signal
signal ExtTimeBase_s1                          : std_logic;                                 -- ExtTimeBase_s0 sampled signal
signal ExtTimeBase_RE                          : std_logic;                                 -- ExtTimeBase rising edge signal


type fsmRstState is (WaitForEn,RstADC, EnADC_Driver);
signal fsmRst_state                            : fsmRstState;

type fsmState is (WaitForFlag, SetStCnv, WaitTconv, WaitSPI);
signal fsm_state                : fsmState;

--
-- Component
Component ACU_Generic_4lines_SPI_Master is
  Generic (
         gSCLK_High              : unsigned(7 downto 0):="00000011";                         -- SCLK high semiperiod length (in number of Clock pulses) 
         gSCLK_Low               : unsigned(7 downto 0):="00000011";                         -- SCLK low  semiperiod length (in number of Clock pulses)
         gCSn2SCLK_SetupTime     : unsigned(7 downto 0):="00000011";                         -- Time between CS_n FE to SCLK RE (in number of Clock pulses)
	 gDataWidth              : integer range 0 to 256:= 200;                             -- Data to send width. It has to be lower or equal to 
	                                                                                     -- nrSCLK_pulses signal value.
         gSDIonRE                : std_logic:='0';                                           -- When 0, SDI is sampled on SCLK or SCLK_BK falling edge else on
                                                                                             -- the rising edge
         gSDOonRE                : std_logic:='0';                                           -- When 0, SDO is shifted on SCLK falling edge else on
                                                                                             -- the rising edge
	 gUseSCLK_BK             : std_logic:='0';                                           -- When 0, SDI is sampled using SCLK as reference clock else SCLK_BK 
         gSCLK_startLevel        : std_logic:='0'                                            -- It defines the SCLK level after CSn goes low
                                                                                             
  );
  Port(
         Clock                   : in  std_logic;                                            -- Clock signal
         Reset                   : in  std_logic;                                            -- Asynchronous reset signal active high
	 LostConnection          : in  std_logic;                                            -- When high, the protocol is aborted and the FSM reset
         NewDt_In                : in  std_logic;                                            -- One clock cycle pulse active high when there is a new Dt_In
	                                                                                     -- or when a read action has to start. 
         Dt_In                   : in std_logic_vector(gDataWidth-1 downto 0);               -- Data to send.

         nrSCLK_pulses           : in std_logic_vector (7 downto 0);                         -- Number of SCLK pulses for each NewVal received
         
         FetchSDI                : out std_logic;                                            -- It can be used to sample extra SDI lines not connected to this instance, but driven by SCLK.
         NewDt_Received          : out std_logic;                                            -- Pulse active high when there is a new Dt_Received.NOTE: It can be longer than one clock cycle, 
                                                                                             -- so it is better on the receiver side to detect the rising edge. 
         Dt_Received             : out std_logic_vector(gDataWidth-1 downto 0);              -- Data received.
	 
         SCLK                    : out std_logic;                                            -- SPI clock signal
         SCLK_BK                 : in std_logic;                                             -- SCLK loop back (on the slave chip side) signal
         SCS_n                   : out std_logic;                                            -- Active low SPI Chip Select signal
         SDI                     : in  std_logic;                                            -- Serial SPI data in (to FPGA) signal
         SDO                     : out std_logic                                             -- Serial SPI data out (from FPGA) signal
  );
end component ACU_Generic_4lines_SPI_Master;

begin

i_ACU_Generic_4lines_SPI_Master: ACU_Generic_4lines_SPI_Master
  Generic map(
         gSCLK_High               => x"06", 
         gSCLK_Low                => x"04",
         gCSn2SCLK_SetupTime      => x"01",
	 gDataWidth               => 54,
         gSDIonRE                 => '0',
         gSDOonRE                 => '0',
	 gUseSCLK_BK              => '0', 
         gSCLK_startLevel         => '1'                                                     
  )
  Port map(
         Clock                    => Clock,
         Reset                    => Reset,
	 LostConnection           => Enable_n,
         NewDt_In                 => StartSPI, 
         Dt_In                    => (others=>'0'),

         nrSCLK_pulses            => x"35",
         
         FetchSDI                 => FetchSDI,
         NewDt_Received           => EndProtocol, 
         Dt_Received              => open,

         SCLK                     => SCLK,
         SCLK_BK                  => '0',
         SCS_n                    => SCS_n,
         SDI                      => '0',
         SDO                      => open
  );
  
p_samp: process(Clock,Reset)
begin
  if (Reset='1') then
    Enable_s0  <= '0';
    Enable_RE  <= '0';
    Enable_n   <= '0';
    SDinA_sx   <= (others=>(others=>'0'));
    SDinB_sx   <= (others=>(others=>'0'));
    
    ExtTimeBase_s0  <= '0';
    ExtTimeBase_s1  <= '0';
    ExtTimeBase_RE  <= '0';
    
  elsif(Clock'event and Clock='1') then
    Enable_s0  <= Enable;
    Enable_RE  <= Enable and not Enable_s0;
    Enable_n   <= not(Enable);
    SDinA_sx   <= SDinA_sx(2 downto 0) & SDinA;
    SDinB_sx   <= SDinB_sx(2 downto 0) & SDinB;

    ExtTimeBase_s0  <= ExtTimeBase;
    ExtTimeBase_s1  <= ExtTimeBase_s0;
    ExtTimeBase_RE  <= ExtTimeBase_s0 and not ExtTimeBase_s1;

  end if;
end process p_samp;

p_fsmRst: process(Clock,Reset)
begin
  if (Reset='1') then  
    fsmRst_state  <= WaitForEn;
    ADC_Rst       <= '0';
    cntRst        <= (others=>'0');
    EnDriver      <= '0';
    
  elsif(Clock'event and Clock='1') then
  
      EnDriver      <= '0';

  
    case fsmRst_state is
                
      when WaitForEn =>
      
        if (Enable_RE = '1') then
          fsmRst_state  <= RstADC;
          ADC_Rst       <= '1';
        end if;
      
      when RstADC => 

        if (cntRst < gADC_RstLength-1) then
          cntRst    <= cntRst + 1;
        else
          cntRst    <= (others=>'0');
          ADC_Rst       <= '0';
          fsmRst_state  <= EnADC_Driver;
        end if;
        
      when EnADC_Driver =>
      
        if (Enable_s0='0') then
          fsmRst_state  <= WaitForEn;
          cntRst        <= (others=>'0');
        end if;
        
        if (cntRst < gADC_RstLength-1) then
          cntRst      <= cntRst + 1;
        else
          EnDriver    <= '1';
        end if;
      
      when others =>
        fsmRst_state  <= WaitForEn;
        ADC_Rst       <= '0';
        cntRst        <= (others=>'0');
        EnDriver      <= '0';
      
    end case;
  end if;
end process p_fsmRst;

p_fsm: process(Clock,Reset)
begin
  if (Reset='1') then  
    fsm_state   <= WaitForFlag;
    StCnv       <= '1';
    cntGeneric  <= (others=>'0');
    StartSPI    <= '0';
                  
  elsif(Clock'event and Clock='1') then
  
    case fsm_state is
        
      
      when WaitForFlag =>
      
        if (EnDriver='1') then
        
          if (gUseExtTimeBase='1') then
            if (ExtTimeBase_RE='1') then
              fsm_state  <= SetStCnv;
              StCnv      <= '0';
            end if;
          else
            fsm_state  <= SetStCnv;
            StCnv      <= '0';
          end if;        
        end if;
      
      when SetStCnv =>
      
        if (cntGeneric < gStCnvLowLength-1) then
          cntGeneric  <= cntGeneric + 1;
        else
          cntGeneric  <= (others=>'0');    
          StCnv       <= '1';    
          fsm_state   <= WaitTconv;          
        end if; 
        
      when WaitTconv =>

        if (cntGeneric < gConvTimeLength-1) then
          cntGeneric  <= cntGeneric + 1;
        else
          cntGeneric  <= (others=>'0');    
          StartSPI    <= '1';    
          fsm_state   <= WaitSPI;          
        end if; 
      
      when WaitSPI =>           
          StartSPI    <= '0';   
          
          if (EndProtocol='1' or Enable_s0='0') then
             fsm_state   <= WaitForFlag;                   
          end if;
                 
      when others =>
        fsm_state   <= WaitForFlag;
        StCnv       <= '1';
        cntGeneric  <= (others=>'0');
        StartSPI    <= '0';
      
    end case;
  end if;
end process p_fsm;

p_dtFetch: process(Clock,Reset)
begin
  if (Reset='1') then  
    tmpAD7608_DtA_0  <= (others=>'0');   
    tmpAD7608_DtB_0  <= (others=>'0');                                     
    tmpAD7608_DtA_1  <= (others=>'0');   
    tmpAD7608_DtB_1  <= (others=>'0');                                     
    tmpAD7608_DtA_2  <= (others=>'0');   
    tmpAD7608_DtB_2  <= (others=>'0');   
                                      
    NewAD7608_Dt     <= '0';
    
    AD7608_Dt_0      <= (others=>'0');
    AD7608_Dt_1      <= (others=>'0');
    AD7608_Dt_2      <= (others=>'0');

  elsif(Clock'event and Clock='1') then
    
    NewAD7608_Dt     <= '0';  
    if (EndProtocol='1') then
       NewAD7608_Dt     <= '1';

       AD7608_Dt_0  <= tmpAD7608_DtB_0(17 downto 0) & tmpAD7608_DtB_0(35 downto 18) & tmpAD7608_DtB_0(53 downto 36) & 
                       tmpAD7608_DtA_0(17 downto 0) & tmpAD7608_DtA_0(35 downto 18) & tmpAD7608_DtA_0(53 downto 36);
       AD7608_Dt_1  <= tmpAD7608_DtB_1(17 downto 0) & tmpAD7608_DtB_1(35 downto 18) & tmpAD7608_DtB_1(53 downto 36) & 
                       tmpAD7608_DtA_1(17 downto 0) & tmpAD7608_DtA_1(35 downto 18) & tmpAD7608_DtA_1(53 downto 36);
       AD7608_Dt_2  <= tmpAD7608_DtB_2(17 downto 0) & tmpAD7608_DtB_2(35 downto 18) & tmpAD7608_DtB_2(53 downto 36) & 
                       tmpAD7608_DtA_2(17 downto 0) & tmpAD7608_DtA_2(35 downto 18) & tmpAD7608_DtA_2(53 downto 36);
       
    end if;
  
    --NewAD7606_Dt     <= EndProtocol;
    
    if (FetchSDI='1') then
      tmpAD7608_DtA_0  <= tmpAD7608_DtA_0(52 downto 0) & SDinA_sx(1)(0);
      tmpAD7608_DtA_1  <= tmpAD7608_DtA_1(52 downto 0) & SDinA_sx(1)(1);
      tmpAD7608_DtA_2  <= tmpAD7608_DtA_2(52 downto 0) & SDinA_sx(1)(2);

      tmpAD7608_DtB_0  <= tmpAD7608_DtB_0(52 downto 0) & SDinB_sx(1)(0);
      tmpAD7608_DtB_1  <= tmpAD7608_DtB_1(52 downto 0) & SDinB_sx(1)(1);
      tmpAD7608_DtB_2  <= tmpAD7608_DtB_2(52 downto 0) & SDinB_sx(1)(2);

    end if;
    
  end if;
end process p_dtFetch;

--Outputs

end beh;
