------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_NRZI_decoder.vhd                                                                                    --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 17/02/2022                                                                                              --
--                                                                                                                    --
-- Comments : This module implemets a Not Returning Zero Inverted decoder.                                            -- 
--            The decoding action is based on the compare between two consecutives received bits; if they are the     --
--            same, the output takes '0' otherwise '1'.                                                               --
--            Please consider that this module introduces one clock cycle delay and that the average data output      --
--            stream frequency is double of the input one.                                                            --
--                                                                                                                    --
-- History  : Start up version 17/02/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------
--        __    __    __    __    __    __    __    __    __    __    __    __    __    __ 	 __    __    __    __    --
-- clk __|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|      --
--     __                   _____             ___________             ___________             ________                --
-- D     |_________________|     |___________|           |___________|           |___________|                        --
--     ________                   _____             ___________             ___________             ________          --
-- D0          |_________________|     |___________|           |___________|           |___________|                  --
--              _____             ___________       _____       _____       _____       _____       _____       __    --
-- Q   ________|     |___________|           |_____|     |_____|     |_____|     |_____|     |_____|     |_____|      --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_NRZI_decoder is
  Generic(
    gClkRE_l_FE_H : std_logic:='0'
  );
  Port(
    Clock         : in  std_logic;  -- Clock signal @ 100 MHz in phase with the data
    Reset         : in  std_logic;  -- Asynchronous reset signal active high

    D             : in  std_logic;  -- Data to decode (already resynchronised)
    Q             : out std_logic   -- Decoded data
  );

end Entity ACU_NRZI_decoder;

architecture beh of ACU_NRZI_decoder is

-- 
-- Signals declaration
signal D_s0              : std_logic;                                -- previous input status

begin

-- NRZI_de
g_CLKRE:if gClkRE_l_FE_H ='0' generate
  p_NRZI_de:process(Clock,Reset)
  begin
    if (Reset='1') then
      D_s0  <= '0';
      Q     <= '0';

    elsif(Clock'event and Clock='1') then

      D_s0  <= D;  
      if (D=D_s0) then
        Q  <= '0';
      else
        Q  <= '1';
      end if;

    end if;
  end process;
end generate g_CLKRE;

g_CLKFE:if gClkRE_l_FE_H ='1' generate
  p_NRZI_de:process(Clock,Reset)
  begin
    if (Reset='1') then
      D_s0  <= '0';
      Q     <= '0';
    elsif(Clock'event and Clock='0') then

      D_s0  <= D;
  
      if (D=D_s0) then
        Q  <= '0';
      else
        Q  <= '1';
      end if;

    end if;
  end process;
end generate g_CLKFE;

end beh;
