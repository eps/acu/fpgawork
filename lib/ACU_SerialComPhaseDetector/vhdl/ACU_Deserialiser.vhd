------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_Deserialiser.vhd                                                                                    --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 18/02/2022                                                                                              --
--                                                                                                                    --
-- Comments : This module receives a serial 100Mbps dataflow @100MHz and generates 10 bits symbols @10 MHz.           --
--            On rxDt_align rising edge, the Tc_cnt_DesDtValid is inremented by one.                                  --
--                                                                                                                    --
-- History  : Start up version 18/02/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------
--        __    __    __    __    __    __    __    __    __    __    __    __    __    __ 	 __    __    __    __    --
-- clk __|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|      --
--                                                                                                                    --
-- cnt_Des     X  0  X  1  X  2  X  3  X  4  X  5  X  6  X  7  X  8  X  9  X  0  X  1  X  2  X  3  X  4  X  5  X  6   --
--              _____                                                       _____                                     --
-- rx_outValid_|     |_____________________________________________________|     |___________________________________ --
--                                                                                                                    --
-- rx_out      X  dt0_0                                                    X          dt0_1                           -- 
--                                                                                                                    --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_Deserialiser is
  Generic(
    gClkRE_l_FE_H : std_logic:='0'
  );
  Port(
    Clock        : in  std_logic;                     -- Clock signal @ 100 MHz in phase with the data
    Reset        : in  std_logic;                     -- Asynchronous reset signal active high
    
    rx_in        : in  std_logic;                     -- Serial data 
    rxDt_align   : in  std_logic;                     -- Pulse active high used to offset the cnt_Des counter
                                                      -- It has to be more than one clock cycle long in order to properly manage the
                                                      -- clock cross domain.

    rx_outValid  : out std_logic;                     -- rx_out valid signal (one pulse active high)
    rx_out       : out std_logic_vector(9 downto 0)   -- Symblol (deserialised data) 
  );

end Entity ACU_Deserialiser;

architecture beh of ACU_Deserialiser is

--
-- Constants declaration
constant Tc_cnt_Des       : unsigned(3 downto 0):=to_unsigned(10,4);  -- It can be considered as the serialisator factor

-- 
-- Signals declaration
signal cnt_Des            : unsigned(3 downto 0);                     -- Deserialisator counter
signal Int_rx_out         : std_logic_vector(9 downto 0);             -- Internal SIPO register 

signal rxDt_align_sx      : std_logic_vector(2 downto 0);             -- rxDt_align sampled signal
signal rxDt_align_RE      : std_logic;                                -- rxDt_align rising edge
signal Tc_cnt_DesDtValid  : unsigned(3 downto 0);                     -- cnt_Des offset counter

begin

-- Deserializer
g_CLKRE:if gClkRE_l_FE_H ='0' generate

  p_offset:process(Clock,Reset)
  begin
    if (Reset='1') then

      rxDt_align_sx      <= (others=>'0');
      rxDt_align_RE      <= '0';
      --Tc_cnt_DesDtValid  <= (others=>'0');
      Tc_cnt_DesDtValid  <= Tc_cnt_Des-1;
    elsif(Clock'event and Clock='1') then
      rxDt_align_sx  <= rxDt_align_sx(1 downto 0) & rxDt_align;
      rxDt_align_RE  <= rxDt_align_sx(1) and not rxDt_align_sx(2);

      if (rxDt_align_RE='1') then
        if (Tc_cnt_DesDtValid < Tc_cnt_Des-1) then
          Tc_cnt_DesDtValid  <= Tc_cnt_DesDtValid + 1;
        else
          Tc_cnt_DesDtValid  <= (others=>'0');
        end if;
      end if;
    end if;
  end process;

  p_deserializer:process(Clock,Reset)
  begin
    if (Reset='1') then

      cnt_Des      <= (others=>'0');
      Int_rx_out   <= (others=>'0');
      rx_outValid  <= '0';

    elsif(Clock'event and Clock='1') then

      rx_outValid  <= '0';
      if (cnt_Des = Tc_cnt_DesDtValid) then
        rx_outValid  <= '1';
      end if;
        
      if (cnt_Des < Tc_cnt_Des-1) then
        cnt_Des  <= cnt_Des + 1;
      else
        cnt_Des  <= (others=>'0');
      end if;

      Int_rx_out  <= Int_rx_out(8 downto 0) & rx_in;

    end if;
  end process;
end generate g_CLKRE;

g_CLKFE:if gClkRE_l_FE_H ='1' generate
  p_offset:process(Clock,Reset)
  begin
    if (Reset='1') then

      rxDt_align_sx      <= (others=>'0');
      rxDt_align_RE      <= '0';
      --Tc_cnt_DesDtValid  <= (others=>'0');
      Tc_cnt_DesDtValid  <= Tc_cnt_Des-1;
    elsif(Clock'event and Clock='0') then
      rxDt_align_sx  <= rxDt_align_sx(1 downto 0) & rxDt_align;
      rxDt_align_RE  <= rxDt_align_sx(1) and not rxDt_align_sx(2);

      if (rxDt_align_RE='1') then
        if (Tc_cnt_DesDtValid < Tc_cnt_Des-1) then
          Tc_cnt_DesDtValid  <= Tc_cnt_DesDtValid + 1;
        else
          Tc_cnt_DesDtValid  <= (others=>'0');
        end if;
      end if;
    end if;
  end process;

  p_deserializer:process(Clock,Reset)
  begin
    if (Reset='1') then

      cnt_Des      <= (others=>'0');
      Int_rx_out   <= (others=>'0');
      rx_outValid  <= '0';

    elsif(Clock'event and Clock='0') then

      rx_outValid  <= '0';
      if (cnt_Des = Tc_cnt_DesDtValid) then
        rx_outValid  <= '1';
      end if;
        
      if (cnt_Des < Tc_cnt_Des-1) then
        cnt_Des  <= cnt_Des + 1;
      else
        cnt_Des  <= (others=>'0');
      end if;

      Int_rx_out  <= Int_rx_out(8 downto 0) & rx_in;

    end if;
  end process;
end generate g_CLKFE;

-- Output
rx_out   <= Int_rx_out;

end beh;
