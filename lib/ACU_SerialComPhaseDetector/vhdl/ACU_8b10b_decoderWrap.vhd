------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_8b10b_decoderWrap.vhd                                                                               --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 03/03/2022                                                                                              --
--                                                                                                                    --
-- Comments : This module wraps the 8b10b decoder developed by Frans Schreuder. For more info about the license,      --
--            please check directly his file.                                                                         --
--                                                                                                                    --
-- History  : Start up version 03/03/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------
--            Removed RstError, IllegalSymbol and DispError signals: having a ACU_8b10b_decoderWrap module for each   --
--            branch, makes the error latching unuseful at this stage. The error latching has to be performed after   --
--            the branch selector module. 26/04/2022
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.ACU_package_numericLib.all;

Entity ACU_8b10b_decoderWrap is

  Port(
    Clock                        : in  std_logic;                                     -- Clock signal
    Reset                        : in  std_logic;                                     -- Asynchronous reset signal active high

    NewInPhaseSymbol             : in  std_logic;                                     -- New in phase data valid symbol 
    InPhaseSymbol                : in  std_logic_vector(9 downto 0);                  -- In phase data symbol

    parityFlagIn                 : in  std_logic;                                     -- Selected parity flag
    parityFlagOut                : out std_logic;                                     -- parityFlagIn sampled signal
    CommaFlag                    : out std_logic;                                     -- Active high when the decoder detects a comma symbol

    --RstError                     : in  std_logic;                                     -- The RE resets IllegalSymbol and DispError
    --IllegalSymbol                : out std_logic;                                     -- code_err latched signal
    --DispError                    : out std_logic;                                     -- disp_err latched signal
    
    code_errOut                  : out std_logic;                                     --Indication for illegal character
    disp_errOut                  : out std_logic;                                     --Indication for disparity error

    aso_8b10b_decoder_valid      : out std_logic;                                     -- Data valid to parity checker 
    aso_8b10b_decoder_data       : out std_logic_vector (7 downto 0)                  -- Data to parity checker 
  );
end Entity ACU_8b10b_decoderWrap;

architecture beh of ACU_8b10b_decoderWrap is

--
-- Constants declaration

--
-- Signals declaration
signal code_err                    : std_logic;                                      -- Illegal symbol detected
signal disp_err                    : std_logic;                                      -- Disparity error detected
--signal RstError_s0                 : std_logic;                                      -- RstError sampled signal
--signal RstError_RE                 : std_logic;                                      -- RstError rising edge

signal NewInPhaseSymbol_s0         : std_logic;                                      -- NewInPhaseSymbol sampled signal
signal NewInPhaseSymbol_RE         : std_logic;                                      -- NewInPhaseSymbol rising edge
signal InPhaseSymbol_s0            : std_logic_vector(9 downto 0);                   -- InPhaseSymbol sampled signal
signal parityFlagIn_s0             : std_logic;                                      -- parityFlagIn sampled signal

--
-- Component declaration
Component dec_8b10b is	 
    port( 
        reset : in std_logic; --Active high reset
        clk : in std_logic;   --Clock to register output and disparity
        datain : in std_logic_vector(9 downto 0); --10b data input
        ena : in std_logic;              -- Enable registers for output and disparity
        ko : out std_logic ;             -- Active high K indication
        dataout : out std_logic_vector(7 downto 0); --Decoded output
        code_err : out std_logic;                   --Indication for illegal character
        disp_err : out std_logic                    --Indication for disparity error
    ); 
end Component dec_8b10b;

begin

i_dec_8b10b: dec_8b10b	 
    port map( 
        reset     => Reset,
        clk       => Clock,
        datain    => InPhaseSymbol_s0,
        ena       => NewInPhaseSymbol_RE,
        ko        => CommaFlag,
        dataout   => aso_8b10b_decoder_data,
        code_err  => code_err,
        disp_err  => disp_err
    ); 

p_errorLatch:process(Reset,Clock)
begin
  if (Reset='1') then
    --IllegalSymbol        <= '0';
    --DispError            <= '0';
    --RstError_s0          <= '0';
    --RstError_RE          <= '0';

    NewInPhaseSymbol_s0  <= '0';
    NewInPhaseSymbol_RE  <= '0';
    InPhaseSymbol_s0     <= (others=>'0');
    
    parityFlagIn_s0      <= '0';
    parityFlagOut        <= '0';
     
    aso_8b10b_decoder_valid  <= '0';
  elsif (Clock'event and Clock='1') then

    NewInPhaseSymbol_s0      <= NewInPhaseSymbol;
    NewInPhaseSymbol_RE      <= NewInPhaseSymbol and not NewInPhaseSymbol_s0;
    InPhaseSymbol_s0         <= InPhaseSymbol;

    parityFlagIn_s0          <= parityFlagIn;
    parityFlagOut            <= parityFlagIn_s0;

    --RstError_s0              <= RstError;
    --RstError_RE              <= RstError and not RstError_s0;

    aso_8b10b_decoder_valid  <= NewInPhaseSymbol_RE;

    --if (RstError_RE='1') then
    --  IllegalSymbol  <= '0';
    --  DispError      <= '0';
    --else
    --  if (code_err='1') then
    --    IllegalSymbol  <= '1';
    --  end if;

    --  if (disp_err='1') then
    --    DispError      <= '1';
    --  end if;
    --end if;

  end if;
end process p_errorLatch;

-- Output
code_errOut  <= code_err;
disp_errOut  <= disp_err;

end beh;
