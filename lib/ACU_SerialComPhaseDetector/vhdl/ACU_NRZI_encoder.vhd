------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_NRZI_encoder.vhd                                                                                    --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 17/02/2022                                                                                              --
--                                                                                                                    --
-- Comments : This module implemets a Not Returning Zero Inverted encoder.                                            -- 
--            The encoding strategy consists to change the output polarity only in case a '1' is in input.            --
--            Please consider that this module introduces one clock cycle delay and that the average data output      --
--            stream frequency is half of the input one.                                                              --
--                                                                                                                    --
-- History  : Start up version 17/02/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------
--        __    __    __    __    __    __    __    __    __    __    __    __    __    __ 	 __    __    __    __    --
-- clk __|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|      --
--              _____             ___________       _____       _____       _____       _____       _____       __    --
-- D   ________|     |___________|           |_____|     |_____|     |_____|     |_____|     |_____|     |_____|      --
--     ______________                   _____             ___________             ___________             ________    --
-- Q                 |_________________|     |___________|           |___________|           |___________|            --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_NRZI_encoder is

  Port(
    Clock    : in  std_logic;  -- Clock signal @ 100 MHz in phase with the data
    Reset    : in  std_logic;  -- Asynchronous reset signal active high

    D        : in  std_logic;  -- Data to encode 
    Q        : out std_logic   -- Encoded data
  );

end Entity ACU_NRZI_encoder;

architecture beh of ACU_NRZI_encoder is

-- 
-- Signals declaration
signal Int_Q              : std_logic;                                -- previous bit status

begin

-- NRZI_en
p_NRZI_en:process(Clock,Reset)
begin
  if (Reset='1') then
    Int_Q  <= '0';
  elsif(Clock'event and Clock='1') then
      
    if (D='1') then
      Int_Q  <= not(Int_Q);
    end if;

  end if;
end process;

-- Output
Q  <= Int_Q;

end beh;
