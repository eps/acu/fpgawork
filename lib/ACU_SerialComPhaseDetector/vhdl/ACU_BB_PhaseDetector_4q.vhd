------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_BB_PhaseDetector_4q.vhd                                                                             --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 02/03/2022                                                                                              --
--                                                                                                                    --
-- Comments : This module implements a bang bang or also known as Alexander phase detector at 4 quadrants.            --
--            The phase detector shows the branch to select, but the selection is triggered only when on the next     --
--            branch to selects Tc_cnt_Stabil valid symbols are received (stability check).                           --
--            If during the stability check the current selcted branch receives an invalid symbol or a disparity      --
--            error or a word disalignement, the selection is forced. This case should in theory never happen, but if --
--            that is the case, at least the number of corrupted pakages should not be huge.                          --
--                                                                                                                    --
-- History  : Start up version 02/03/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.ACU_package_numericLib.all;

Entity ACU_BB_PhaseDetector_4q is
  Port(
    Reset                        : in  std_logic;                                             -- Asynchronous reset signal active high
    Clock                        : in  std_logic;                                             -- System clock
    Clock_0                      : in  std_logic;                                             -- Clock signal 0°
    Clock_90                     : in  std_logic;                                             -- Clock signal 90°

    dtIn                         : in  std_logic;                                             -- Serial data in (rx) 

    Branch_0_valid               : in  std_logic;                                             --Branch 0 valid signal
    Branch_0                     : in  std_logic_vector(12 downto 0);                         --(12)    Comma flag
                                                                                              --(11)    Word allignment
                                                                                              --(10)    Parity flag
                                                                                              --(9)     Disparity error
                                                                                              --(8)     Illegal symbol
                                                                                              --(7..0)  Decoded data

    Branch_180_valid             : in  std_logic;                                             --Branch 180 valid signal
    Branch_180                   : in  std_logic_vector(12 downto 0);                         --(12)    Comma flag
                                                                                              --(11)    Word allignment
                                                                                              --(10)    Parity flag
                                                                                              --(9)     Disparity error
                                                                                              --(8)     Illegal symbol
                                                                                              --(7..0)  Decoded data

    Branch_90_valid              : in  std_logic;                                             --Branch 90 valid signal
    Branch_90                    : in  std_logic_vector(12 downto 0);                         --(12)    Comma flag
                                                                                              --(11)    Word allignment
                                                                                              --(10)    Parity flag
                                                                                              --(9)     Disparity error
                                                                                              --(8)     Illegal symbol
                                                                                              --(7..0)  Decoded data

    Branch_270_valid             : in  std_logic;                                             --Branch 270 valid signal
    Branch_270                   : in  std_logic_vector(12 downto 0);                         --(12)    Comma flag
                                                                                              --(11)    Word allignment
                                                                                              --(10)    Parity flag
                                                                                              --(9)     Disparity error
                                                                                              --(8)     Illegal symbol
                                                                                              --(7..0)  Decoded data

    RstError                     : in  std_logic;                                             -- The RE resets IllegalSymbol and DispError
    IllegalSymbol                : out std_logic;                                             -- code_err latched signal
    DispError                    : out std_logic;                                             -- disp_err latched signal
    WordAlignL                   : out std_logic;                                             -- word align latched signal

    Tc_cntEL_Pulses              : in  std_logic_vector(15 downto 0);                         -- Phase detector filter value
    Tc_cnt_Stabil                : in  std_logic_vector(15 downto 0);                         -- Number of symbols that have to be stable in order
                                                                                              -- to trigger the selection

    SelBranch                    : out std_logic_vector(1 downto 0);                          -- 0 => 270°
                                                                                              -- 1 => 180°
                                                                                              -- 2 => 0°
                                                                                              -- 3 => 90°

    CommaFlag                    : out std_logic;                                             -- Selected comma flag
    ParityFlag                   : out std_logic;                                             -- Selected parity flag 

    DtInSync_0                   : out std_logic;                                             -- dtIn synchronised with Clock_0 RE
    DtInSync_180                 : out std_logic;                                             -- dtIn synchronised with Clock_0 FE
    DtInSync_90                  : out std_logic;                                             -- dtIn synchronised with Clock_90 RE
    DtInSync_270                 : out std_logic;                                             -- dtIn synchronised with Clock_90 FE

    DtOut_valid                  : out std_logic;                                             -- Selected data valid 
    DtOut                        : out std_logic_vector(7 downto 0)                           -- Selected data 


  );
end Entity ACU_BB_PhaseDetector_4q;

architecture beh of ACU_BB_PhaseDetector_4q is

--
-- Constants declaration

--
-- Signals declaration
signal DtIn_Sync_0         : std_logic_vector(1 downto 0);
signal DtIn_nSync_0        : std_logic_vector(1 downto 0);
signal DtIn_Sync_90        : std_logic_vector(1 downto 0);
signal DtIn_nSync_90       : std_logic_vector(1 downto 0);

signal S1_0                : std_logic;
signal S2_0                : std_logic;
signal S3_0                : std_logic;
signal S4_0                : std_logic;

signal Early_0             : std_logic;
signal Late_0              : std_logic;

signal S1_90               : std_logic;
signal S2_90               : std_logic;
signal S3_90               : std_logic;
signal S4_90               : std_logic;

signal Early_90            : std_logic;
signal Late_90             : std_logic;

signal EarlyLateFlag_0     : std_logic;                 -- 0=Early; 1=Late
signal cntEL_Pulses_0      : unsigned(15 downto 0);

signal EarlyLateFlag_90    : std_logic;                 -- 0=Early; 1=Late
signal cntEL_Pulses_90     : unsigned(15 downto 0);

signal IntSel              : std_logic_vector(1 downto 0);

signal IntSelNxt           : std_logic_vector(1 downto 0);  -- Next to be selected branch
signal IntSelNxt_s0        : std_logic_vector(1 downto 0);  -- IntSelNxt sampled signal
signal ChangedSel          : std_logic_vector(1 downto 0);  -- xor between IntSelNxt and IntSelNxt_s0
signal cnt_Stabil          : unsigned(15 downto 0);         -- Stability (with no error) symbol counter

signal selNxtWA            : std_logic;                     -- Next to be selected word alignement
signal selNxtDiss          : std_logic;                     -- Next to be selected disparity error
signal selNxtIll           : std_logic;                     -- Next to be selected illegal symbol
signal selNxtBranch_valid  : std_logic;                     -- Next to be selected data valid

signal extDt2Dec_valid     : array5bits(3 downto 0);        --(0)= Extended dt valid 0°
                                                            --(1)= Extended dt valid 180°
                                                            --(2)= Extended dt valid 90°
                                                            --(3)= Extended dt valid 270°

signal selWA               : std_logic;                     -- Selected word alignement
signal selDiss             : std_logic;                     -- Selected disparity error
signal selIll              : std_logic;                     -- Selected illegal symbol

signal Branch_0_s0         : array13bits(1 downto 0);       -- Branch_0 sampled signal
signal Branch_180_s0       : array13bits(1 downto 0);       -- Branch_180 sampled signal
signal Branch_90_s0        : array13bits(1 downto 0);       -- Branch_90 sampled signal
signal Branch_270_s0       : array13bits(1 downto 0);       -- Branch_270 sampled signal

signal Branch_0_valid_sx   : std_logic_vector(1 downto 0);  -- Branch_0_valid sampled signal
signal Branch_180_valid_sx : std_logic_vector(1 downto 0);  -- Branch_180_valid sampled signal
signal Branch_90_valid_sx  : std_logic_vector(1 downto 0);  -- Branch_90_valid sampled signal
signal Branch_270_valid_sx : std_logic_vector(1 downto 0);  -- Branch_270_valid sampled signal

signal RstError_s0         : std_logic;                     -- RstError sampled signal
signal RstError_RE         : std_logic;                     -- RstError rising edge

begin

p_ffd4:process(Clock_0,Reset)
begin
  if (Reset='1') then
    DtIn_Sync_0   <= (others=>'0');

    S1_0            <= '0';
    S3_0            <= '0';
    S4_0            <= '0';

    Early_0         <= '0';
    Late_0          <= '0';

  elsif (Clock_0'event and Clock_0='1') then
    DtIn_Sync_0(0)  <= dtIn;
    DtIn_Sync_0(1)  <= DtIn_Sync_0(0);   
    S1_0            <= DtIn_Sync_0(1);
    S3_0            <= S1_0;
    S4_0            <= S2_0;

    Early_0         <= not(S1_0 xor S4_0) and (S4_0 xor S3_0);
    Late_0          <= (S1_0 xor S4_0) and not(S4_0 xor S3_0);

  end if;

  if (Reset='1') then
    DtIn_nSync_0     <= (others=>'0');
    S2_0             <= '0';
  elsif (Clock_0'event and Clock_0='0') then
    DtIn_nSync_0(0)  <= dtIn;
    DtIn_nSync_0(1)  <= DtIn_nSync_0(0);
    S2_0             <= DtIn_nSync_0(1);
  end if;

end process p_ffd4;

--X_0      <= S1_0 xor S4_0;
--Y_0      <= S4_0 xor S3_0;

--Early_0  <= not(X_0) and Y_0;
--Late_0   <= X_0 and not(Y_0);

p_EL_filter_0: process(Clock_0,Reset)
begin
  if (Reset='1') then
    cntEL_Pulses_0   <= (others=> '0');
    EarlyLateFlag_0  <= '0';
    
  elsif (Clock_0'event and Clock_0='1') then

    if (EarlyLateFlag_0 = '0') then
      -- Early
      if (Late_0 = '1') then
        if (cntEL_Pulses_0 < unsigned(Tc_cntEL_Pulses)-1) then
          cntEL_Pulses_0  <= cntEL_Pulses_0 +1;
        else
          cntEL_Pulses_0   <= (others=>'0');
          EarlyLateFlag_0  <= '1';
        end if;
      elsif (Early_0 = '1') then
        if (cntEL_Pulses_0 > 0) then
          cntEL_Pulses_0  <= cntEL_Pulses_0 - 1;
        end if;
      end if;
    else
      -- Late
      if (Early_0 = '1') then
        if (cntEL_Pulses_0 < unsigned(Tc_cntEL_Pulses)-1) then
          cntEL_Pulses_0  <= cntEL_Pulses_0 +1;
        else
          cntEL_Pulses_0   <= (others=>'0');
          EarlyLateFlag_0  <= '0';
        end if;
      elsif (Late_0 = '1') then
        if (cntEL_Pulses_0 > 0) then
          cntEL_Pulses_0  <= cntEL_Pulses_0 - 1;
        end if;
      end if;

    end if;
  end if;

end process p_EL_filter_0;

-- 90°
p_ffd4_90:process(Clock_90,Reset)
begin
  if (Reset='1') then
    DtIn_Sync_90     <= (others=>'0');

    S1_90            <= '0';
    S3_90            <= '0';
    S4_90            <= '0';

    Early_90         <= '0';
    Late_90          <= '0';

  elsif (Clock_90'event and Clock_90='1') then
    DtIn_Sync_90(0)  <= dtIn;
    DtIn_Sync_90(1)  <= DtIn_Sync_90(0);

    S1_90            <= DtIn_Sync_90(1);
    S3_90            <= S1_90;
    S4_90            <= S2_90;

    Early_90         <= not(S1_90 xor S4_90) and (S4_90 xor S3_90);
    Late_90          <= (S1_90 xor S4_90) and not(S4_90 xor S3_90);

  end if;

  if (Reset='1') then
    DtIn_nSync_90     <= (others=>'0');

    S2_90             <= '0';
  elsif (Clock_90'event and Clock_90='0') then
    DtIn_nSync_90(0)  <= dtIn; 
    DtIn_nSync_90(1)  <= DtIn_nSync_90(0);

    S2_90             <= DtIn_nSync_90(1);
  end if;

end process p_ffd4_90;

--X_90      <= S1_90 xor S4_90;
--Y_90      <= S4_90 xor S3_90;

--Early_90  <= not(X_90) and Y_90;
--Late_90   <= X_90 and not(Y_90);

p_EL_filter_90: process(Clock_90,Reset)
begin
  if (Reset='1') then
    cntEL_Pulses_90   <= (others=> '0');
    EarlyLateFlag_90  <= '0';
  elsif (Clock_90'event and Clock_90='1') then

    if (EarlyLateFlag_90 = '0') then
      -- Early
      if (Late_90 = '1') then
        if (cntEL_Pulses_90 < unsigned(Tc_cntEL_Pulses)-1) then
          cntEL_Pulses_90  <= cntEL_Pulses_90 +1;
        else
          cntEL_Pulses_90   <= (others=>'0');
          EarlyLateFlag_90  <= '1';
        end if;
      elsif (Early_90 = '1') then
        if (cntEL_Pulses_90 > 0) then
          cntEL_Pulses_90  <= cntEL_Pulses_90 - 1;
        end if;
      end if;
    else
      -- Late
      if (Early_90 = '1') then
        if (cntEL_Pulses_90 < unsigned(Tc_cntEL_Pulses)-1) then
          cntEL_Pulses_90  <= cntEL_Pulses_90 +1;
        else
          cntEL_Pulses_90   <= (others=>'0');
          EarlyLateFlag_90  <= '0';
        end if;
      elsif (Late_90 = '1') then
        if (cntEL_Pulses_90 > 0) then
          cntEL_Pulses_90  <= cntEL_Pulses_90 - 1;
        end if;
      end if;

    end if;
  end if;

end process p_EL_filter_90;

-- Selector
p_sel:process(Clock,Reset)
begin
  if (Reset='1') then
    IntSel           <= (others =>'0');

    DtOut_valid      <= '0';
    DtOut            <= (others =>'0');

    ParityFlag       <= '0';
    CommaFlag        <= '0';




    selWA            <= '0';
    selDiss          <= '0';
    selIll           <= '0';

    Branch_0_s0      <= (others=>(others=>'0'));
    Branch_180_s0    <= (others=>(others=>'0'));
    Branch_90_s0     <= (others=>(others=>'0'));
    Branch_270_s0    <= (others=>(others=>'0'));

    Branch_0_valid_sx    <= (others =>'0');
    Branch_180_valid_sx  <= (others =>'0');
    Branch_90_valid_sx   <= (others =>'0');
    Branch_270_valid_sx  <= (others =>'0');


    IntSelNxt            <= (others =>'0');
    IntSelNxt_s0         <= (others =>'0');
    ChangedSel           <= (others =>'0');
    cnt_Stabil           <= (others =>'0');

    selNxtWA             <= '0';
    selNxtDiss           <= '0';
    selNxtIll            <= '0';
    selNxtBranch_valid   <= '0';

  elsif (Clock'event and Clock='1') then
    -- Check selector changes
    IntSelNxt         <= EarlyLateFlag_0 & EarlyLateFlag_90;
    IntSelNxt_s0      <= IntSelNxt;
    ChangedSel        <= (IntSelNxt(1) xor IntSelNxt_s0(1)) & 
                         (IntSelNxt(0) xor IntSelNxt_s0(0));

    -- Next branch to be selected allarm and data valid selection
    case IntSelNxt_s0 is
      when "00" =>
        -- 270°
        selNxtWA             <= Branch_270(11);
        selNxtDiss           <= Branch_270(9);
        selNxtIll            <= Branch_270(8);
        selNxtBranch_valid   <= Branch_270_valid;
 
      when "01" =>
        -- 180°
        selNxtWA             <= Branch_180(11);
        selNxtDiss           <= Branch_180(9);
        selNxtIll            <= Branch_180(8);
        selNxtBranch_valid   <= Branch_180_valid;
 
      when "10" =>
        -- 0°
        selNxtWA             <= Branch_0(11);
        selNxtDiss           <= Branch_0(9);
        selNxtIll            <= Branch_0(8);
        selNxtBranch_valid   <= Branch_0_valid;
 
      when "11" =>
        -- 90°
        selNxtWA             <= Branch_90(11);
        selNxtDiss           <= Branch_90(9);
        selNxtIll            <= Branch_90(8);
        selNxtBranch_valid   <= Branch_90_valid;
 
      when others =>
        -- 270°
        selNxtWA             <= Branch_270(11);
        selNxtDiss           <= Branch_270(9);
        selNxtIll            <= Branch_270(8);
        selNxtBranch_valid   <= Branch_270_valid;
 
    end case;

    -- Check next branch to be selected stability
    if (ChangedSel(1) ='1' or ChangedSel(0) ='1') then
      -- The PD wants to change selector
      cnt_Stabil           <= (others =>'0');
    elsif(selNxtWA='0' or selNxtDiss='1' or selNxtIll='1') then
      -- The next to be selected branch has still errors (it can't be selected yet)
      cnt_Stabil           <= (others =>'0');
    elsif (selWA='0' or selDiss='1' or selIll='1') then
      -- The selected branch has an error, so the selector is forced to change
      IntSel  <= IntSelNxt_s0;
    elsif selNxtBranch_valid ='1' then
      -- There is a new error free symbol received on the next to be selected branch
      if (cnt_Stabil < unsigned(Tc_cnt_Stabil)-1) then
        -- It is still monitoring the next to be selected branch stability
        cnt_Stabil  <= cnt_Stabil + 1;
      else
        -- The next to be selected branch is stable and can be selected
        IntSel  <= IntSelNxt_s0;
      end if;
    end if;
    
    -- Branches data valid resampling
    Branch_0_valid_sx(0)    <= Branch_0_valid;
    Branch_0_valid_sx(1)    <= Branch_0_valid_sx(0);
    Branch_180_valid_sx(0)  <= Branch_180_valid;
    Branch_180_valid_sx(1)  <= Branch_180_valid_sx(0);
    Branch_90_valid_sx(0)   <= Branch_90_valid;
    Branch_90_valid_sx(1)   <= Branch_90_valid_sx(0);
    Branch_270_valid_sx(0)  <= Branch_270_valid;
    Branch_270_valid_sx(1)  <= Branch_270_valid_sx(0);
    
    -- Branches "record" sampled on the respective datat valid
    if (Branch_0_valid = '1') then
      Branch_0_s0(0)        <= Branch_0;
      Branch_0_s0(1)        <= Branch_0_s0(0);
    end if;

    if (Branch_180_valid = '1') then
      Branch_180_s0(0)      <= Branch_180;
      Branch_180_s0(1)      <= Branch_180_s0(0);
    end if;

    if (Branch_90_valid = '1') then
      Branch_90_s0(0)       <= Branch_90;
      Branch_90_s0(1)       <= Branch_90_s0(0);
    end if;

    if (Branch_270_valid = '1') then
      Branch_270_s0(0)      <= Branch_270;
      Branch_270_s0(1)      <= Branch_270_s0(0);
    end if;

    -- Branch selection (Actual selection)
    case IntSel is
      when "00" =>
        -- 270°
        selWA            <= Branch_270(11);
        selDiss          <= Branch_270(9);
        selIll           <= Branch_270(8);

        CommaFlag        <= Branch_270_s0(1)(12);
        ParityFlag       <= Branch_270_s0(1)(10);
        DtOut_valid      <= Branch_270_valid_sx(1);
        DtOut            <= Branch_270_s0(1)(7 downto 0);

      when "01" =>
        -- 180°
        selWA            <= Branch_180(11);
        selDiss          <= Branch_180(9);
        selIll           <= Branch_180(8);

        CommaFlag        <= Branch_180_s0(1)(12);
        ParityFlag       <= Branch_180_s0(1)(10);
        DtOut_valid      <= Branch_180_valid_sx(1);
        DtOut            <= Branch_180_s0(1)(7 downto 0);

      when "10" =>
        -- 0°
        selWA            <= Branch_0(11);
        selDiss          <= Branch_0(9);
        selIll           <= Branch_0(8);

        CommaFlag        <= Branch_0_s0(1)(12);
        ParityFlag       <= Branch_0_s0(1)(10);
        DtOut_valid      <= Branch_0_valid_sx(1);
        DtOut            <= Branch_0_s0(1)(7 downto 0);

      when "11" =>
        -- 90°
        selWA            <= Branch_90(11);
        selDiss          <= Branch_90(9);
        selIll           <= Branch_90(8);

        CommaFlag        <= Branch_90_s0(1)(12);
        ParityFlag       <= Branch_90_s0(1)(10);
        DtOut_valid      <= Branch_90_valid_sx(1);
        DtOut            <= Branch_90_s0(1)(7 downto 0);

      when others =>
        -- 270°
        selWA            <= Branch_270(11);
        selDiss          <= Branch_270(9);
        selIll           <= Branch_270(8);

        CommaFlag        <= Branch_270_s0(1)(12);
        ParityFlag       <= Branch_270_s0(1)(10);
        DtOut_valid      <= Branch_270_valid_sx(1);
        DtOut            <= Branch_270_s0(1)(7 downto 0);
    end case;
  end if;
end process p_sel;

-- Error latching
p_errLatch: process (Clock,Reset) 
begin
  if (Reset='1') then  
    RstError_s0          <= '0';
    RstError_RE          <= '0';

    IllegalSymbol        <= '0';
    DispError            <= '0';
    WordAlignL           <= '0';

  elsif (Clock'event and Clock='1') then
    RstError_s0              <= RstError;
    RstError_RE              <= RstError and not RstError_s0;


    if (RstError_RE='1') then
      IllegalSymbol        <= '0';
      DispError            <= '0';
      WordAlignL           <= '1';
    else
      if (selIll='1') then
        IllegalSymbol  <= '1';
      end if;

      if (selDiss='1') then
        DispError      <= '1';
      end if;

      if (selWA='0') then
        WordAlignL     <= '0';
      end if;
    end if;

  end if;
end process p_errLatch;

SelBranch  <= IntSel;
    
DtInSync_0    <= DtIn_Sync_0(1);
DtInSync_180  <= DtIn_nSync_0(1);
DtInSync_90   <= DtIn_Sync_90(1);
DtInSync_270  <= DtIn_nSync_90(1);

end beh;
