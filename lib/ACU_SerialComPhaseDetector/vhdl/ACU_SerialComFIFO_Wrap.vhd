------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_SerialComFIFO_Wrap.vhd                                                                              --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 18/02/2022                                                                                              --
--                                                                                                                    --
-- Comments : This module wraps the SerialComFIFO component in order to generate a data valid based on the rdempty    --
--            signal.
--                                                                                                                    --
-- History  : Start up version 18/02/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_SerialComFIFO_Wrap is
  Generic(
      dtWidth               : integer range 1 to 64:= 12;
      gCyclonIII_n_CyclonV  : std_logic := '0'        -- 0 = Module implemented in a CyclonIII FPGA; 1= module implemented in a CyclonV FPGA		
  );
  Port(
		aclr		   : IN STD_LOGIC ;
		data		   : IN STD_LOGIC_VECTOR (dtWidth-1 DOWNTO 0);
		rdclk		   : IN STD_LOGIC ;
		rdreq		   : IN STD_LOGIC ;
		wrclk		   : IN STD_LOGIC ;
		wrreq		   : IN STD_LOGIC ;
      q_valid     : OUT STD_LOGIC ;
		q		      : OUT STD_LOGIC_VECTOR (dtWidth-1 DOWNTO 0);
		rdempty		: OUT STD_LOGIC ;
		wrfull		: OUT STD_LOGIC 
	);  
end Entity ACU_SerialComFIFO_Wrap;

architecture beh of ACU_SerialComFIFO_Wrap is

--
-- Constants declaration

--
-- Signals declaration
signal Int_rdempty          : std_logic;                        -- internal rdempty signal        
--
-- Component declaration
Component SerialComFIFO_bbpd IS
   Generic(
      dtWidth     : integer range 1 to 64:= 12
   ); 
	PORT
	(
		aclr		: IN STD_LOGIC  := '0';
		data		: IN STD_LOGIC_VECTOR (dtWidth-1 DOWNTO 0);
		rdclk		: IN STD_LOGIC ;
		rdreq		: IN STD_LOGIC ;
		wrclk		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (dtWidth-1 DOWNTO 0);
		rdempty		: OUT STD_LOGIC ;
		wrfull		: OUT STD_LOGIC 
	);
END Component SerialComFIFO_bbpd;

Component SerialComFIFO_bbpd_CV IS
   Generic(
      dtWidth     : integer range 1 to 64:= 12
   ); 
	PORT
	(
		aclr		: IN STD_LOGIC  := '0';
		data		: IN STD_LOGIC_VECTOR (dtWidth-1 DOWNTO 0);
		rdclk		: IN STD_LOGIC ;
		rdreq		: IN STD_LOGIC ;
		wrclk		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (dtWidth-1 DOWNTO 0);
		rdempty		: OUT STD_LOGIC ;
		wrfull		: OUT STD_LOGIC 
	);
END Component SerialComFIFO_bbpd_CV;

begin


--
-- CyclonIII Implementation
i_CyclonIII_ifgen : if gCyclonIII_n_CyclonV ='0' generate

  i_SerialComFIFO_bbpd: SerialComFIFO_bbpd
   Generic map(
      dtWidth     => dtWidth
   )
	PORT map
	(
		aclr		=> aclr,
		data		=> data,
		rdclk		=> rdclk,
		rdreq		=> rdreq,
		wrclk		=> wrclk,
		wrreq		=> wrreq,
		q		   => q,
		rdempty	=> Int_rdempty,
		wrfull	=> wrfull
	);  

end generate i_CyclonIII_ifgen;


--
-- CyclonV Implementation
i_CyclonV_ifgen : if gCyclonIII_n_CyclonV ='1' generate
  i_SerialComFIFO_bbpd_CV: SerialComFIFO_bbpd_CV
   Generic map(
      dtWidth     => dtWidth
   )
	PORT map
	(
		aclr		=> aclr,
		data		=> data,
		rdclk		=> rdclk,
		rdreq		=> rdreq,
		wrclk		=> wrclk,
		wrreq		=> wrreq,
		q		   => q,
		rdempty	=> Int_rdempty,
		wrfull	=> wrfull
	);  

end generate i_CyclonV_ifgen;


p_genEn:process(rdclk,aclr)
begin
  if (aclr = '1') then
    q_valid  <= '0';
  elsif (rdclk'event and rdclk='1') then
    q_valid  <= not(Int_rdempty);
  end if;
end process p_genEn;

-- Output
rdempty  <= Int_rdempty;

end beh;
