------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_SerialCommParityChecker.vhd                                                                         --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 21/02/2022                                                                                              --
--                                                                                                                    --
-- Comments : This module calculates the parity over the payload symbols and compares the results with the received   --
--            parity symbol. In case of parity error, the payload is not forwarded in output and after                --
--            gMaxParityErrorNr consecutives errors, the comunication is declared as failed.                          --
--                                                                                                                    --
-- History  : Start up version 21/02/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_SerialCommParityChecker is
  Generic (
    gMaxParityErrorNr  : unsigned(3 downto 0):=to_unsigned(5,4)  -- Maximum number of consecutives parity errors before
                                                                          -- declaring communication failed
  );
  Port(
    Clock              : in  std_logic;                          -- Clock signal
    Reset              : in  std_logic;                          -- Asynchronous reset signal active high

    --WordAligned        : in std_logic;                           -- Active high when the receiver is word aligned
    parityFlag         : in std_logic;                           -- It is high when asi_decoder_data carries out the received parity byte.
    CommaFlag          : in std_logic;                           -- Active high when the decoder detects a comma symbol

    asi_decoder_valid  : in std_logic;                           -- Data valid to parity checker 
    asi_decoder_data   : in std_logic_vector (7 downto 0);       -- Data to parity checker 


    RstParityCntError  : in std_logic;                           -- Parity error counter reset signal
    ParityCntError     : out std_logic_vector(3 downto 0);       -- Parity error counter
    CommFailed         : out std_logic;                          -- Active high when ParityCntError=gMaxParityErrorNr
    aso_payload_valid  : out std_logic;                          -- Data valid to parity checker 
    aso_payload_data   : out std_logic_vector (23 downto 0)      -- Data to parity checker 

  );
end Entity ACU_SerialCommParityChecker;

architecture beh of ACU_SerialCommParityChecker is

--
-- Constants declaration
constant Tc_cntSymbols                 :unsigned(3 downto 0):=(to_unsigned(4,4));
--
-- Signals declaration
signal parity                          :std_logic_vector(7 downto 0);     -- internal parity value
signal cntSymbols                      :unsigned(3 downto 0);             -- Symbols counter
signal cntParityError                  :unsigned(3 downto 0);             -- parity error counter

signal RstParityCntError_s0            : std_logic;                       -- RstParityCntError sampled signal
signal RstParityCntError_RE            : std_logic;                       -- RstParityCntError rising edge

signal asi_decoder_valid_s0            : std_logic;                       --asi_decoder_valid sampled signal
signal asi_decoder_valid_RE            : std_logic;                       --asi_decoder_valid rising edge

begin

p_parityCalcAndCheck:process(Reset,Clock)
begin

  if (Reset='1') then

    parity                <= (others=>'0');
    cntSymbols            <= (others=>'0');
    cntParityError        <= (others=>'0');

    aso_payload_valid     <= '0';
    aso_payload_data      <= (others=>'0');

    CommFailed            <= '0';
    RstParityCntError_s0  <= '0';
    RstParityCntError_RE  <= '0';

    asi_decoder_valid_s0  <= '0';
    asi_decoder_valid_RE  <= '0';

  elsif(Clock'event and Clock='1') then
    asi_decoder_valid_s0  <= asi_decoder_valid;
    asi_decoder_valid_RE  <= asi_decoder_valid and not asi_decoder_valid_s0;


    RstParityCntError_s0  <= RstParityCntError;
    RstParityCntError_RE  <= RstParityCntError and not RstParityCntError_s0;

    aso_payload_valid   <= '0';

    if (RstParityCntError_RE='1') then
      cntParityError      <= (others=>'0');
      CommFailed          <= '0';
    end if;

    if (asi_decoder_valid_RE='1' and CommaFlag = '0') then

      if (parityFlag='1') then
        cntSymbols          <= (others=>'0');
        parity              <= (others=>'0');
        if (parity=asi_decoder_data) then
--          if (WordAligned='1') then
--            aso_payload_valid   <= '1';
--          end if;
          aso_payload_valid   <= '1';

          --cntParityError      <= (others=>'0');
          --CommFailed          <= '0';
        else
          if (cntParityError < gMaxParityErrorNr-1) then
            cntParityError  <= cntParityError + 1; 
          else
            CommFailed      <= '1';
          end if;
        end if;
      else
        if (cntSymbols < Tc_cntSymbols-2) then
          cntSymbols  <= cntSymbols + 1;
        else
          cntSymbols  <= (others=>'0');
        end if;

        parity  <= parity xor asi_decoder_data;

        aso_payload_data(23-(to_integer(cntSymbols)*8) downto 16-(to_integer(cntSymbols)*8))  <= asi_decoder_data;
      end if;

    end if;

  end if;
end process p_parityCalcAndCheck;

-- Outputs
ParityCntError  <= std_logic_vector(cntParityError);

end beh;
