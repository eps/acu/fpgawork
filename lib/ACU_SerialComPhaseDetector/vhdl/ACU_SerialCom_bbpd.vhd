------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_SerialCom_bbpd.vhd                                                                                  --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 26/04/2022                                                                                              --
--                                                                                                                    --
-- Comments : ACU_SerialCom IP: it incapsulates all the modules necessary for the 100Mbps 50MHz serial data           --
--            comunication:                                                                                           -- 
--              TX->                                                                                                  --
--                * ACU_8b10b_encoderWrap                                                                             --
--                * ACU_SerialComFIFO_Wrap                                                                            --
--                * ACU_Serialiser                                                                                    --
--                * ACU_NRZI_encoder                                                                                  --
--              RX->                                                                                                  --
--                * ACU_NRZI_decoder                                                                                  --
--                * ACU_Deserialiser                                                                                  --
--                * ACU_WordAlign                                                                                     --
--                * ACU_SerialComFIFO_Wrap                                                                            --
--                * ACU_8b10b_decoderWrap                                                                             --
--                * ACU_BB_PhaseDetector_4q                                                                           --
--                * ACU_SerialCommParityChecker                                                                       --
--                                                                                                                    --
--            At the moment the module accepts 24b data input up to 2 MHz rate. Slower frequecies are allowed, but    -- 
--            they have to be submultiple of 2MHz (1MHz, 500KHz, 250KHz, ...)                                         --
--                                                                                                                    --
-- History  : Start up version 14/02/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------
--            Added generic to drive the FIFO implementation (CIII oe CV). DR 25.05.2024                              --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_SerialCom_bbpd is
  Generic(
      gCyclonIII_n_CyclonV  : std_logic := '0'        -- 0 = Module implemented in a CyclonIII FPGA; 1= module implemented in a CyclonV FPGA		
  );

  Port(
    SystemClock              : in  std_logic;                          -- System Clock signal @100 MHz
    Reset                    : in  std_logic;                          -- Asynchronous reset signal active high

    PayloadTx_Valid          : in  std_logic;                          -- Data valid from source 
    PayloadTx                : in  std_logic_vector(23 downto 0);      -- Data from source

    ClockSerialCom_0         : in  std_logic;                          -- Serial comunication Clock signal @100 MHz 0°
    ClockSerialCom_90        : in  std_logic;                          -- Serial comunication Clock signal @100 MHz 90°

    Tx_out                   : out std_logic;                          -- Serial data output NRZI encoded (100Mbps @50MHz)

    Rx_in                    : in  std_logic;                          -- Serial data input

    Tc_cntEL_Pulses          : in  std_logic_vector(15 downto 0);      -- Phase detector filter value: it is used to define the branch selection
                                                                       -- direction
    Tc_cnt_Stabil            : in  std_logic_vector(15 downto 0);      -- Stability symbols value: it is used to confirm the stability on the 
                                                                       -- next to be selected branch
    RstError                 : in  std_logic;                          -- The RE resets IllegalSymbol, DispError and WordAlignL
    IllegalSymbol            : out std_logic;                          -- code_err latched signal
    DispError                : out std_logic;                          -- disp_err latched signal
    WordAlignL               : out std_logic;                          -- word align latched signal

    SelBranch                : out std_logic_vector(1 downto 0);       -- Selected branch:
                                                                       -- 0 => 270°
                                                                       -- 1 => 180°
                                                                       -- 2 => 0°
                                                                       -- 3 => 90°

    ParityCntError           : out std_logic_vector(3 downto 0);       -- Parity error counter
    CommFailed               : out std_logic;                          -- Active high when ParityCntError=gMaxParityErrorNr

    PayloadRx_Valid          : out std_logic;                          -- Data valid from sender 
    PayloadRx                : out std_logic_vector(23 downto 0)       -- Data from sender


  );
end Entity ACU_SerialCom_bbpd;

architecture beh of ACU_SerialCom_bbpd is

--
-- Constants declaration

--
-- Signals declaration

signal aso_out_8b10b_encoder_valid  : std_logic;                       -- 8b10b encoded data valid 
signal aso_out_8b10b_encoder_data   : std_logic_vector (9 downto 0);   -- 8b10b encoded data to FIFO TX

signal rdemptyTX                    : std_logic;                               -- FIFO TX read empty signal used by the serialiser module
signal rdreqTX                      : std_logic;                               -- FIFO TX read request from serialiser module

signal SeriaTx_in                   : std_logic_vector(9 downto 0);            -- Data from FIFO TX to serialise
signal SeriaTx_out                  : std_logic;                               -- Serialised data to the NRZI encoder

signal rx_in_0                      : std_logic;                               -- Serial data input 0° NRZI decoded
signal rx_in_180                    : std_logic;                               -- Serial data input 180° NRZI decoded
signal rx_in_90                     : std_logic;                               -- Serial data input 90° NRZI decoded
signal rx_in_270                    : std_logic;                               -- Serial data input 270° NRZI decoded

signal rxDt_align_0                 : std_logic;                               -- Word alignment pulse 0°
signal rxDt_align_180               : std_logic;                               -- Word alignment pulse 180°
signal rxDt_align_90                : std_logic;                               -- Word alignment pulse 90°
signal rxDt_align_270               : std_logic;                               -- Word alignment pulse 270°

signal rx_out_0_Valid               : std_logic;                               -- Deserialised data valid 0°
signal rx_out_0                     : std_logic_vector(9 downto 0);            -- Deserialised data 0°
signal rx_out_180_Valid             : std_logic;                               -- Deserialised data valid 180°
signal rx_out_180                   : std_logic_vector(9 downto 0);            -- Deserialised data 180°
signal rx_out_90_Valid              : std_logic;                               -- Deserialised data valid 90°
signal rx_out_90                    : std_logic_vector(9 downto 0);            -- Deserialised data 90°
signal rx_out_270_Valid             : std_logic;                               -- Deserialised data valid 270°
signal rx_out_270                   : std_logic_vector(9 downto 0);            -- Deserialised data 270°

signal WA_NewSymbolOut_0            : std_logic;                               -- Word aligned data valid 0°
signal WA_SymbolOut_0               : std_logic_vector(11 downto 0);           -- Word aligned data 0°: (11)   Word aligned flag
                                                                               --                       (10)   Parity symbol flag
                                                                               --                       (9..0) Word aligned symbol
signal WA_NewSymbolOut_180          : std_logic;                               -- Word aligned data valid 180°
signal WA_SymbolOut_180             : std_logic_vector(11 downto 0);           -- Word aligned data 180°
signal WA_NewSymbolOut_90           : std_logic;                               -- Word aligned data valid 90°
signal WA_SymbolOut_90              : std_logic_vector(11 downto 0);           -- Word aligned data 90°
signal WA_NewSymbolOut_270          : std_logic;                               -- Word aligned data valid 270°
signal WA_SymbolOut_270             : std_logic_vector(11 downto 0);           -- Word aligned data 270°

signal RxSymbol_0_Valid             : std_logic;                               -- FIFO RX data valid output 0°
signal RxSymbol_0                   : std_logic_vector(11 downto 0);           -- FIFO RX data output 0°
signal RxSymbol_180_Valid           : std_logic;                               -- FIFO RX data valid output 180°
signal RxSymbol_180                 : std_logic_vector(11 downto 0);           -- FIFO RX data output 180°
signal RxSymbol_90_Valid            : std_logic;                               -- FIFO RX data valid output 90°
signal RxSymbol_90                  : std_logic_vector(11 downto 0);           -- FIFO RX data output 90°
signal RxSymbol_270_Valid           : std_logic;                               -- FIFO RX data valid output 270°
signal RxSymbol_270                 : std_logic_vector(11 downto 0);           -- FIFO RX data output 270°

signal parityFlagOut_0              : std_logic;                               -- 8b10b decoder parity flag out 0°
signal CommaFlag_0                  : std_logic;                               -- 8b10b decoder comma flag out 0°
signal IllegalSymbol_0              : std_logic;                               -- 8b10b decoder illegal symbol error 0°
signal DispError_0                  : std_logic;                               -- 8b10b decoder disparity error 0°
signal aso_8b10b_decoder_valid_0    : std_logic;                               -- 8b10b decoder data decoded valid 0°
signal aso_8b10b_decoder_data_0     : std_logic_vector (7 downto 0);           -- 8b10b decoder data decoded 0°

signal parityFlagOut_180            : std_logic;                               -- 8b10b decoder parity flag out 180°
signal CommaFlag_180                : std_logic;                               -- 8b10b decoder comma flag out 180°
signal IllegalSymbol_180            : std_logic;                               -- 8b10b decoder illegal symbol error 180°
signal DispError_180                : std_logic;                               -- 8b10b decoder disparity error 180°
signal aso_8b10b_decoder_valid_180  : std_logic;                               -- 8b10b decoder data decoded valid 180°
signal aso_8b10b_decoder_data_180   : std_logic_vector (7 downto 0);           -- 8b10b decoder data decoded 180°

signal parityFlagOut_90             : std_logic;                               -- 8b10b decoder parity flag out 90°
signal CommaFlag_90                 : std_logic;                               -- 8b10b decoder comma flag out 90°
signal IllegalSymbol_90             : std_logic;                               -- 8b10b decoder illegal symbol error 90°
signal DispError_90                 : std_logic;                               -- 8b10b decoder disparity error 90°
signal aso_8b10b_decoder_valid_90   : std_logic;                               -- 8b10b decoder data decoded valid 90°
signal aso_8b10b_decoder_data_90    : std_logic_vector (7 downto 0);           -- 8b10b decoder data decoded 90°

signal parityFlagOut_270            : std_logic;                               -- 8b10b decoder parity flag out 270°
signal CommaFlag_270                : std_logic;                               -- 8b10b decoder comma flag out 270°
signal IllegalSymbol_270            : std_logic;                               -- 8b10b decoder illegal symbol error 270°
signal DispError_270                : std_logic;                               -- 8b10b decoder disparity error 270°
signal aso_8b10b_decoder_valid_270  : std_logic;                               -- 8b10b decoder data decoded valid 270°
signal aso_8b10b_decoder_data_270   : std_logic_vector (7 downto 0);           -- 8b10b decoder data decoded 270°

signal Branch_0                     : std_logic_vector(12 downto 0);           -- Record data 0° in input to the phase detector:
                                                                               --(12)    Comma flag
                                                                               --(11)    Word allignment
                                                                               --(10)    Parity flag
                                                                               --(9)     Disparity error
                                                                               --(8)     Illegal symbol
                                                                               --(7..0)  Decoded data 
signal Branch_180                   : std_logic_vector(12 downto 0);           -- Record data 180° in input to the phase detector:
                                                                               --(12)    Comma flag
                                                                               --(11)    Word allignment
                                                                               --(10)    Parity flag
                                                                               --(9)     Disparity error
                                                                               --(8)     Illegal symbol
                                                                               --(7..0)  Decoded data  
signal Branch_90                    : std_logic_vector(12 downto 0);           -- Record data 90° in input to the phase detector:
                                                                               --(12)    Comma flag
                                                                               --(11)    Word allignment
                                                                               --(10)    Parity flag
                                                                               --(9)     Disparity error
                                                                               --(8)     Illegal symbol
                                                                               --(7..0)  Decoded data  
signal Branch_270                   : std_logic_vector(12 downto 0);           -- Record data 270° in input to the phase detector:
                                                                               --(12)    Comma flag
                                                                               --(11)    Word allignment
                                                                               --(10)    Parity flag
                                                                               --(9)     Disparity error
                                                                               --(8)     Illegal symbol
                                                                               --(7..0)  Decoded data  

signal CommaFlag                    : std_logic;                               -- Selected comma flag                                 
signal parityFlag                   : std_logic;                               -- Selected parity flag
signal PD_RxSymbol_Valid            : std_logic;                               -- Selected Decoded data valid
signal PD_RxSymbol                  : std_logic_vector (7 downto 0);           -- Selected Decoded data   

--
-- Components declaration

Component ACU_8b10b_encoderWrap is
  Generic (
    gKxy                        : std_logic_vector(7 downto 0):=x"FC";                      -- K.28.7
    gNrByte                     : integer range 1 to 10:= 3                                 -- Payload Byte number
  );
  Port(
    Clock                       : in  std_logic;                                            -- Clock signal
    Reset                       : in  std_logic;                                            -- Asynchronous reset signal active high

    asi_in_8b10b_encoder_valid   : in  std_logic;                                            -- Data valid from source 
    asi_in_8b10b_encoder_data    : in  std_logic_vector ((gNrByte*8)-1 downto 0);            -- Data from source

    aso_out_8b10b_encoder_valid  : out std_logic;                                            -- Data valid to sink
    aso_out_8b10b_encoder_data   : out std_logic_vector(9 downto 0)                          -- 2x4b5bsymbols to sink 
  );
end Component ACU_8b10b_encoderWrap;

Component ACU_SerialComFIFO_Wrap is
  Generic(
      dtWidth               : integer range 1 to 64:= 12;
      gCyclonIII_n_CyclonV  : std_logic := '0'        -- 0 = Module implemented in a CyclonIII FPGA; 1= module implemented in a CyclonV FPGA		
  );
  Port(
		aclr		   : IN STD_LOGIC ;
		data		   : IN STD_LOGIC_VECTOR (dtWidth-1 DOWNTO 0);
		rdclk		   : IN STD_LOGIC ;
		rdreq		   : IN STD_LOGIC ;
		wrclk		   : IN STD_LOGIC ;
		wrreq		   : IN STD_LOGIC ;
      q_valid     : OUT STD_LOGIC ;
		q		      : OUT STD_LOGIC_VECTOR (dtWidth-1 DOWNTO 0);
		rdempty		: OUT STD_LOGIC ;
		wrfull		: OUT STD_LOGIC 
	);  
end component ACU_SerialComFIFO_Wrap;

Component ACU_Serialiser is

  Port(
    Clock    : in  std_logic;                     -- Clock signal @ 100 MHz in phase with the data
    Reset    : in  std_logic;                     -- Asynchronous reset signal active high

    tx_in    : in  std_logic_vector(9 downto 0);  -- Data from FIFO 
    rdempty  : in  std_logic;                     -- FIFO empty status

    rdreq    : out std_logic;                     -- FIFO read request
    tx_out   : out std_logic                      -- Serialised data 
  );

end Component ACU_Serialiser;

Component ACU_NRZI_encoder is

  Port(
    Clock    : in  std_logic;  -- Clock signal @ 100 MHz in phase with the data
    Reset    : in  std_logic;  -- Asynchronous reset signal active high

    D        : in  std_logic;  -- Data to encode 
    Q        : out std_logic   -- Encoded data
  );

end Component ACU_NRZI_encoder;

Component ACU_NRZI_decoder is
  Generic(
    gClkRE_l_FE_H : std_logic:='0'
  );
  Port(
    Clock    : in  std_logic;  -- Clock signal @ 100 MHz in phase with the data
    Reset    : in  std_logic;  -- Asynchronous reset signal active high

    D        : in  std_logic;  -- Data to decode 
    Q        : out std_logic   -- Decoded data
  );

end Component ACU_NRZI_decoder;

Component ACU_Deserialiser is
  Generic(
    gClkRE_l_FE_H : std_logic:='0'
  );
  Port(
    Clock        : in  std_logic;                     -- Clock signal @ 100 MHz in phase with the data
    Reset        : in  std_logic;                     -- Asynchronous reset signal active high

    rx_in        : in  std_logic;                     -- Serial data 
    rxDt_align   : in  std_logic;                     -- Pulse active high used to offset the cnt_Des counter
                                                      -- It has to be more than one clock cycle long in order to properly manage the
                                                      -- clock cross domain.

    rx_outValid  : out std_logic;                     -- rx_out valid signal (one pulse active high)
    rx_out       : out std_logic_vector(9 downto 0)   -- Symblol (deserialised data) 
  );

end Component ACU_Deserialiser;

Component ACU_WordAlign is
  Generic (
    gClkRE_l_FE_H                : std_logic:='0';                                            -- Clock catching edge
    gNrSymbProPck                : integer range 1 to 64:= 5;                                 -- Number of symbols in a package
    gKxy                         : std_logic_vector(9 downto 0):="0011111000";                -- Preamble to search (0x0F8)
    gNrKey4Alig                  : unsigned(2 downto 0):=to_unsigned(5,3)                     -- Number key words to find to declare the alignement
  ); 
  Port(
    Clock                        : in  std_logic;                                             -- Clock signal
    Reset                        : in  std_logic;                                             -- Asynchronous reset signal active high

    NewSymbolIn                  : in  std_logic;                                             -- New data valid symbol 
    SymbolIn                     : in  std_logic_vector(9 downto 0);                          -- Data symbol

    rxDt_align                   : out std_logic;                                             -- 4 Clock pulses long signal used to align the
                                                                                              -- word
    NewSymbolOut                 : out std_logic;                                             -- New data valid out symbol 
    SymbolOut                    : out std_logic_vector(11 downto 0)                          -- Data out symbol: (11) WordAligned
                                                                                              --                  (10) Parity flag
                                                                                              --                  (9 downto 0) Symbol
  );
end Component ACU_WordAlign;

Component ACU_8b10b_decoderWrap is

  Port(
    Clock                        : in  std_logic;                                     -- Clock signal
    Reset                        : in  std_logic;                                     -- Asynchronous reset signal active high

    NewInPhaseSymbol             : in  std_logic;                                     -- New in phase data valid symbol 
    InPhaseSymbol                : in  std_logic_vector(9 downto 0);                  -- In phase data symbol

    parityFlagIn                 : in  std_logic;                                     -- Selected parity flag
    parityFlagOut                : out std_logic;                                     -- parityFlagIn sampled signal
    CommaFlag                    : out std_logic;                                     -- Active high when the decoder detects a comma symbol

    code_errOut                  : out std_logic;                                     --Indication for illegal character
    disp_errOut                  : out std_logic;                                     --Indication for disparity error

    aso_8b10b_decoder_valid      : out std_logic;                                     -- Data valid to parity checker 
    aso_8b10b_decoder_data       : out std_logic_vector (7 downto 0)                  -- Data to parity checker 
  );
end Component ACU_8b10b_decoderWrap;

Component ACU_BB_PhaseDetector_4q is
  Port(
    Reset                        : in  std_logic;                                             -- Asynchronous reset signal active high
    Clock                        : in  std_logic;                                             -- System clock
    Clock_0                      : in  std_logic;                                             -- Clock signal 0°
    Clock_90                     : in  std_logic;                                             -- Clock signal 90°

    dtIn                         : in  std_logic;                                             -- Serial data in (rx) 

    Branch_0_valid               : in  std_logic;                                             --Branch 0 valid signal
    Branch_0                     : in  std_logic_vector(12 downto 0);                         --(12)    Comma flag
                                                                                              --(11)    Word allignment
                                                                                              --(10)    Parity flag
                                                                                              --(9)     Disparity error
                                                                                              --(8)     Illegal symbol
                                                                                              --(7..0)  Decoded data

    Branch_180_valid             : in  std_logic;                                             --Branch 180 valid signal
    Branch_180                   : in  std_logic_vector(12 downto 0);                         --(12)    Comma flag
                                                                                              --(11)    Word allignment
                                                                                              --(10)    Parity flag
                                                                                              --(9)     Disparity error
                                                                                              --(8)     Illegal symbol
                                                                                              --(7..0)  Decoded data

    Branch_90_valid              : in  std_logic;                                             --Branch 90 valid signal
    Branch_90                    : in  std_logic_vector(12 downto 0);                         --(12)    Comma flag
                                                                                              --(11)    Word allignment
                                                                                              --(10)    Parity flag
                                                                                              --(9)     Disparity error
                                                                                              --(8)     Illegal symbol
                                                                                              --(7..0)  Decoded data

    Branch_270_valid             : in  std_logic;                                             --Branch 270 valid signal
    Branch_270                   : in  std_logic_vector(12 downto 0);                         --(12)    Comma flag
                                                                                              --(11)    Word allignment
                                                                                              --(10)    Parity flag
                                                                                              --(9)     Disparity error
                                                                                              --(8)     Illegal symbol
                                                                                              --(7..0)  Decoded data

    RstError                     : in  std_logic;                                             -- The RE resets IllegalSymbol and DispError
    IllegalSymbol                : out std_logic;                                             -- code_err latched signal
    DispError                    : out std_logic;                                             -- disp_err latched signal
    WordAlignL                   : out std_logic;                                             -- word align latched signal

    Tc_cntEL_Pulses              : in  std_logic_vector(15 downto 0);                         -- Phase detector filter value
    Tc_cnt_Stabil                : in  std_logic_vector(15 downto 0);                         -- Number of symbols that have to be stable in order
                                                                                              -- to trigger the selection

    SelBranch                    : out std_logic_vector(1 downto 0);                          -- 0 => 270°
                                                                                              -- 1 => 180°
                                                                                              -- 2 => 0°
                                                                                              -- 3 => 90°

    CommaFlag                    : out std_logic;                                             -- Selected comma flag
    ParityFlag                   : out std_logic;                                             -- Selected parity flag 

    DtOut_valid                  : out std_logic;                                             -- Selected data valid 
    DtOut                        : out std_logic_vector(7 downto 0)                           -- Selected data 


  );
end Component ACU_BB_PhaseDetector_4q;

Component ACU_SerialCommParityChecker is
  Generic (
    gMaxParityErrorNr  : unsigned(3 downto 0):=to_unsigned(5,4)  -- Maximum number of consecutives parity errors before
                                                                          -- declaring communication failed
  );
  Port(
    Clock              : in  std_logic;                          -- Clock signal
    Reset              : in  std_logic;                          -- Asynchronous reset signal active high

    parityFlag         : in std_logic;                           -- It is high when asi_decoder_data carries out the received parity byte.
    CommaFlag          : in std_logic;                           -- Active high when the decoder detects a comma symbol

    asi_decoder_valid  : in std_logic;                           -- Data valid to parity checker 
    asi_decoder_data   : in std_logic_vector (7 downto 0);       -- Data to parity checker 


    RstParityCntError  : in std_logic;                           -- Parity error counter reset signal
    ParityCntError     : out std_logic_vector(3 downto 0);       -- Parity error counter
    CommFailed         : out std_logic;                          -- Active high when ParityCntError=gMaxParityErrorNr
    aso_payload_valid  : out std_logic;                          -- Data valid to parity checker 
    aso_payload_data   : out std_logic_vector (23 downto 0)      -- Data to parity checker 

  );
end Component ACU_SerialCommParityChecker;

begin


------------------------------------------------------------------------------------------------------------
--                                                  TX                                                    --
------------------------------------------------------------------------------------------------------------

-- Splitter and 8b10b encoder
i_ACU_8b10b_encoderWrap: ACU_8b10b_encoderWrap

  Port map(
    Clock                        => SystemClock,
    Reset                        => Reset,

    asi_in_8b10b_encoder_valid   => PayloadTx_Valid, 
    asi_in_8b10b_encoder_data    => PayloadTx,

    aso_out_8b10b_encoder_valid  => aso_out_8b10b_encoder_valid,
    aso_out_8b10b_encoder_data   => aso_out_8b10b_encoder_data
  );


-- TX FIFO: it splits the SystemClock and the ClockSerialCom_0, domains
i_SerialComTxFIFO: ACU_SerialComFIFO_Wrap 
   Generic map(
      dtWidth               => 10,
      gCyclonIII_n_CyclonV  => gCyclonIII_n_CyclonV
   )
	Port map
	(
		aclr     => Reset,

		wrclk    => SystemClock,
		wrreq    => aso_out_8b10b_encoder_valid,
		data     => aso_out_8b10b_encoder_data,

		rdclk    => ClockSerialCom_0,
		rdreq    => rdreqTX,

      q_valid  => open,
		q        => SeriaTx_in,
		rdempty  => rdemptyTX,
		wrfull   => open
	);


-- Serializer

i_ACU_Serialiser: ACU_Serialiser

  Port map(
    Clock    => ClockSerialCom_0,
    Reset    => Reset,

    tx_in    => SeriaTx_in,
    rdempty  => rdemptyTX,

    rdreq    => rdreqTX,
    tx_out   => SeriaTx_out
  );

-- NRZI encoder

i_ACU_NRZI_encoder: ACU_NRZI_encoder
  Port map(
    Clock    => ClockSerialCom_0,
    Reset    => Reset,

    D        => SeriaTx_out, 
    Q        => Tx_out
  );


------------------------------------------------------------------------------------------------------------
--                                                  RX                                                    --
------------------------------------------------------------------------------------------------------------


-- NRZI 0° decoder
i_ACU_NRZI_decoder_0: ACU_NRZI_decoder 
  Generic map(
    gClkRE_l_FE_H  => '0'
  )
  Port map(
    Clock          => ClockSerialCom_0,
    Reset          => Reset,

    D              => Rx_in,
    Q              => rx_in_0
  );

-- Deserialiser 0°
i_ACU_Deserialiser_0: ACU_Deserialiser 
  Generic map(
    gClkRE_l_FE_H => '0'
  )
  Port map(
    Clock        => ClockSerialCom_0,

    Reset        => Reset,

    rx_in        => rx_in_0,
    rxDt_align   => rxDt_align_0,


    rx_outValid  => rx_out_0_Valid,
    rx_out       => rx_out_0
  );

-- Word aligner 0°
i_ACU_WordAlign_0: ACU_WordAlign
  Generic map(
    gClkRE_l_FE_H  => '0'
  )
  Port map(
    Clock          => ClockSerialCom_0,

    Reset          => Reset,

    NewSymbolIn    => rx_out_0_Valid,
    SymbolIn       => rx_out_0,

    rxDt_align     => rxDt_align_0,
                                                                                              
    NewSymbolOut   => WA_NewSymbolOut_0,
    SymbolOut      => WA_SymbolOut_0
  );

-- RX FIFO 0
i_SerialComRxFIFO_0: ACU_SerialComFIFO_Wrap 
   Generic map(
      dtWidth               => 12,
      gCyclonIII_n_CyclonV  => gCyclonIII_n_CyclonV
   )

	Port map
	(
		aclr     => Reset,
		data     => WA_SymbolOut_0,
		wrclk    => ClockSerialCom_0,
		wrreq    => WA_NewSymbolOut_0,

		rdclk    => SystemClock,
		rdreq    => '1',
      q_valid  => RxSymbol_0_Valid,
		q        => RxSymbol_0,
		rdempty  => open,
		wrfull   => open
	);

-- 8b10b decoder 0
i_ACU_8b10b_decoderWrap_0: ACU_8b10b_decoderWrap 

  Port map(

    Clock                        => SystemClock,
    Reset                        => Reset,


    NewInPhaseSymbol             => RxSymbol_0_Valid,
    InPhaseSymbol                => RxSymbol_0(9 downto 0),

    parityFlagIn                 => RxSymbol_0(10),
    parityFlagOut                => parityFlagOut_0,

    CommaFlag                    => CommaFlag_0,

    code_errOut                  => IllegalSymbol_0,
    disp_errOut                  => DispError_0,

    aso_8b10b_decoder_valid      => aso_8b10b_decoder_valid_0,
    aso_8b10b_decoder_data       => aso_8b10b_decoder_data_0
  );


-- NRZI 180° decoder
i_ACU_NRZI_decoder_180: ACU_NRZI_decoder 
  Generic map(
    gClkRE_l_FE_H  => '1'
  )
  Port map(
    Clock          => ClockSerialCom_0,

    Reset          => Reset,

    D              => Rx_in,
    Q              => rx_in_180
  );


-- Deserialiser 180°
i_ACU_Deserialiser_180: ACU_Deserialiser 
  Generic map(
    gClkRE_l_FE_H => '1'
  )
  Port map(
    Clock        => ClockSerialCom_0,

    Reset        => Reset,

    rx_in        => rx_in_180,
    rxDt_align   => rxDt_align_180,


    rx_outValid  => rx_out_180_Valid,
    rx_out       => rx_out_180
  );

-- Word aligner 180°
i_ACU_WordAlign_180: ACU_WordAlign
  Generic map(
    gClkRE_l_FE_H  => '1'
  )
  Port map(
    Clock          => ClockSerialCom_0,

    Reset          => Reset,

    NewSymbolIn    => rx_out_180_Valid,
    SymbolIn       => rx_out_180,

    rxDt_align     => rxDt_align_180,
                                                                                              
    NewSymbolOut   => WA_NewSymbolOut_180,
    SymbolOut      => WA_SymbolOut_180
  );

-- RX FIFO 180
i_SerialComRxFIFO_180: ACU_SerialComFIFO_Wrap 
   Generic map(
      dtWidth               => 12,
      gCyclonIII_n_CyclonV  => gCyclonIII_n_CyclonV
   )

	Port map
	(
		aclr     => Reset,
		data     => WA_SymbolOut_180,
		wrclk    => ClockSerialCom_0,
		wrreq    => WA_NewSymbolOut_180,

		rdclk    => SystemClock,
		rdreq    => '1',
      q_valid  => RxSymbol_180_Valid,
		q        => RxSymbol_180,
		rdempty  => open,
		wrfull   => open
	);

-- 8b10b decoder 180
i_ACU_8b10b_decoderWrap_180: ACU_8b10b_decoderWrap 

  Port map(

    Clock                        => SystemClock,
    Reset                        => Reset,


    NewInPhaseSymbol             => RxSymbol_180_Valid,
    InPhaseSymbol                => RxSymbol_180(9 downto 0),

    parityFlagIn                 => RxSymbol_180(10),
    parityFlagOut                => parityFlagOut_180,

    CommaFlag                    => CommaFlag_180,

    code_errOut                  => IllegalSymbol_180,
    disp_errOut                  => DispError_180,

    aso_8b10b_decoder_valid      => aso_8b10b_decoder_valid_180,
    aso_8b10b_decoder_data       => aso_8b10b_decoder_data_180
  );

-- NRZI 90° decoder
i_ACU_NRZI_decoder_90: ACU_NRZI_decoder 
  Generic map(
    gClkRE_l_FE_H  => '0'
  )
  Port map(
    Clock          => ClockSerialCom_90,
    Reset          => Reset,

    D              => Rx_in,
    Q              => rx_in_90
  );

-- Deserialiser 90°
i_ACU_Deserialiser_90: ACU_Deserialiser 
  Generic map(
    gClkRE_l_FE_H => '0'
  )
  Port map(
    Clock          => ClockSerialCom_90,

    Reset        => Reset,

    rx_in        => rx_in_90,
    rxDt_align   => rxDt_align_90,

    rx_outValid  => rx_out_90_Valid,
    rx_out       => rx_out_90
  );

-- Word aligner 90°
i_ACU_WordAlign_90: ACU_WordAlign
  Generic map(
    gClkRE_l_FE_H  => '0'
  )
  Port map(
    Clock          => ClockSerialCom_90,

    Reset          => Reset,

    NewSymbolIn    => rx_out_90_Valid,
    SymbolIn       => rx_out_90,

    rxDt_align     => rxDt_align_90,
                                                                                              
    NewSymbolOut   => WA_NewSymbolOut_90,
    SymbolOut      => WA_SymbolOut_90
  );

-- RX FIFO 90
i_SerialComRxFIFO_90: ACU_SerialComFIFO_Wrap 
   Generic map(
      dtWidth               => 12,
      gCyclonIII_n_CyclonV  => gCyclonIII_n_CyclonV
   )

	Port map
	(
		aclr     => Reset,
		data     => WA_SymbolOut_90,
		wrclk    => ClockSerialCom_90,
		wrreq    => WA_NewSymbolOut_90,

		rdclk    => SystemClock,
		rdreq    => '1',
      q_valid  => RxSymbol_90_Valid,
		q        => RxSymbol_90,
		rdempty  => open,
		wrfull   => open
	);


-- 8b10b decoder 90
i_ACU_8b10b_decoderWrap_90: ACU_8b10b_decoderWrap 

  Port map(

    Clock                        => SystemClock,
    Reset                        => Reset,


    NewInPhaseSymbol             => RxSymbol_90_Valid,
    InPhaseSymbol                => RxSymbol_90(9 downto 0),

    parityFlagIn                 => RxSymbol_90(10),
    parityFlagOut                => parityFlagOut_90,

    CommaFlag                    => CommaFlag_90,

    code_errOut                  => IllegalSymbol_90,
    disp_errOut                  => DispError_90,

    aso_8b10b_decoder_valid      => aso_8b10b_decoder_valid_90,
    aso_8b10b_decoder_data       => aso_8b10b_decoder_data_90
  );


-- NRZI 270° decoder
i_ACU_NRZI_decoder_270: ACU_NRZI_decoder 
  Generic map(
    gClkRE_l_FE_H  => '1'
  )
  Port map(
    Clock          => ClockSerialCom_90,

    Reset          => Reset,

    D              => Rx_in,
    Q              => rx_in_270
  );

-- Deserialiser 270°
i_ACU_Deserialiser_270: ACU_Deserialiser 
  Generic map(
    gClkRE_l_FE_H => '1'
  )
  Port map(
    Clock          => ClockSerialCom_90,

    Reset        => Reset,

    rx_in        => rx_in_270,
    rxDt_align   => rxDt_align_270,


    rx_outValid  => rx_out_270_Valid,
    rx_out       => rx_out_270
  );

-- Word aligner 270°
i_ACU_WordAlign_270: ACU_WordAlign
  Generic map(
    gClkRE_l_FE_H  => '1'
  )
  Port map(
    Clock          => ClockSerialCom_90,

    Reset          => Reset,

    NewSymbolIn    => rx_out_270_Valid,
    SymbolIn       => rx_out_270,

    rxDt_align     => rxDt_align_270,
                                                                                              
    NewSymbolOut   => WA_NewSymbolOut_270,
    SymbolOut      => WA_SymbolOut_270
  );

-- RX FIFO 270
i_SerialComRxFIFO_270: ACU_SerialComFIFO_Wrap 
   Generic map(
      dtWidth               => 12,
      gCyclonIII_n_CyclonV  => gCyclonIII_n_CyclonV
   )

	Port map
	(
		aclr     => Reset,
		data     => WA_SymbolOut_270,
		wrclk    => ClockSerialCom_90,
		wrreq    => WA_NewSymbolOut_270,

		rdclk    => SystemClock,
		rdreq    => '1',
      q_valid  => RxSymbol_270_Valid,
		q        => RxSymbol_270,
		rdempty  => open,
		wrfull   => open
	);

-- 8b10b decoder 270
i_ACU_8b10b_decoderWrap_270: ACU_8b10b_decoderWrap 

  Port map(

    Clock                        => SystemClock,
    Reset                        => Reset,


    NewInPhaseSymbol             => RxSymbol_270_Valid,
    InPhaseSymbol                => RxSymbol_270(9 downto 0),

    parityFlagIn                 => RxSymbol_270(10),
    parityFlagOut                => parityFlagOut_270,

    CommaFlag                    => CommaFlag_270,

    code_errOut                  => IllegalSymbol_270,
    disp_errOut                  => DispError_270,

    aso_8b10b_decoder_valid      => aso_8b10b_decoder_valid_270,
    aso_8b10b_decoder_data       => aso_8b10b_decoder_data_270
  );


--
-- Phase Detector
             
Branch_0  <= CommaFlag_0 &
             RxSymbol_0(11) &
             parityFlagOut_0 &
             DispError_0 &
             IllegalSymbol_0 &
             aso_8b10b_decoder_data_0;

Branch_180  <= CommaFlag_180 &
             RxSymbol_180(11) &
             parityFlagOut_180 &
             DispError_180 &
             IllegalSymbol_180 &
             aso_8b10b_decoder_data_180;

Branch_90  <= CommaFlag_90 &
             RxSymbol_90(11) &
             parityFlagOut_90 &
             DispError_90 &
             IllegalSymbol_90 &
             aso_8b10b_decoder_data_90;

Branch_270  <= CommaFlag_270 &
             RxSymbol_270(11) &
             parityFlagOut_270 &
             DispError_270 &
             IllegalSymbol_270 &
             aso_8b10b_decoder_data_270;

i_ACU_BB_PhaseDetector_4q: ACU_BB_PhaseDetector_4q 
  Port map(

    Reset                        => Reset,
    Clock                        => SystemClock,
    Clock_0                      => ClockSerialCom_0,
    Clock_90                     => ClockSerialCom_90,


    dtIn                         => Rx_in,
    
    Branch_0_valid               => aso_8b10b_decoder_valid_0,
    Branch_0                     => Branch_0,
    Branch_180_valid             => aso_8b10b_decoder_valid_180,
    Branch_180                   => Branch_180,
    Branch_90_valid              => aso_8b10b_decoder_valid_90,
    Branch_90                    => Branch_90,
    Branch_270_valid             => aso_8b10b_decoder_valid_270,
    Branch_270                   => Branch_270,
    
    RstError                     => RstError,
    IllegalSymbol                => IllegalSymbol,
    DispError                    => DispError,
    WordAlignL                   => WordAlignL,

    Tc_cntEL_Pulses              => Tc_cntEL_Pulses,
    Tc_cnt_Stabil                => Tc_cnt_Stabil,

    CommaFlag                    => CommaFlag,
    ParityFlag                   => ParityFlag,


    SelBranch                    => SelBranch,

    DtOut_valid                  => PD_RxSymbol_Valid,
    DtOut                        => PD_RxSymbol

  );

i_ACU_SerialCommParityChecker: ACU_SerialCommParityChecker 
  Port map(
    Clock                   => SystemClock,
    Reset                   => Reset,

    parityFlag              => ParityFlag,
    CommaFlag               => CommaFlag,
    asi_decoder_valid       => PD_RxSymbol_Valid, 
    asi_decoder_data        => PD_RxSymbol,

    RstParityCntError       => RstError,
    ParityCntError          => ParityCntError,
    CommFailed              => CommFailed,

    aso_payload_valid       => PayloadRx_Valid,
    aso_payload_data        => PayloadRx
  );

end beh;
