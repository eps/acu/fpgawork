------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_WordAlign.vhd                                                                                       --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 03/03/2022                                                                                              --
--                                                                                                                    --
-- Comments : This module performs the data word alignement: the deserialiser doesn't know when a complete symbol is  --
--            received because it is asynchronous with the serial data source. Sending a key word before the payload  --
--            helps to align the data on the deserialiser. Every rxDt_align pulse there will be a bit shift on the    --
--            deserialiser side. This module checks up to gNrSymbProPck received symbols and if no key word is        --
--            detected, than generates a rxDt_align pulse. This process goes on till a key word is detected.          --
--            In order to declare the WordAligned status, the key word has to be detected, in the same position,      --
--            for gNrKey4Alig times. If this is not the case, another rxDt_align pulse is generated and the search    --
--            restarts.                                                                                               --
--            In WordAligned status, if a key word is not detected, the status is lost immediatelly in order to       --
--            restart the search activity immediatelly (and as consequence have less corrupted packages received).    --
--            NOTE!: 1) In this implementation the symbol is 10 bits long.                                            --
--                   2) The parity flag and the WordAligned status information will be concatenated to the output     --
--                      data on the MSBs                                                                              --
--                                                                                                                    --
-- History  : Start up version 03/03/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


Entity ACU_WordAlign is
  Generic (
    gClkRE_l_FE_H                : std_logic:='0';                                            -- Clock catching edge
    gNrSymbProPck                : integer range 1 to 64:= 5;                                 -- Number of symbols in a package
    gKxy                         : std_logic_vector(9 downto 0):="0011111000";                -- Preamble to search (0x0F8)
    gNrKey4Alig                  : unsigned(2 downto 0):=to_unsigned(5,3)                     -- Number key words to find to declare the alignement
  ); 
  Port(
    Clock                        : in  std_logic;                                             -- Clock signal
    Reset                        : in  std_logic;                                             -- Asynchronous reset signal active high

    NewSymbolIn                  : in  std_logic;                                             -- New data valid symbol 
    SymbolIn                     : in  std_logic_vector(9 downto 0);                          -- Data symbol

    rxDt_align                   : out std_logic;                                             -- 4 Clock pulses long signal used to align the
                                                                                              -- word
    NewSymbolOut                 : out std_logic;                                             -- New data valid out symbol 
    SymbolOut                    : out std_logic_vector(11 downto 0)                          -- Data out symbol: (11) WordAligned
                                                                                              --                  (10) Parity flag
                                                                                              --                  (9 downto 0) Symbol
  );
end Entity ACU_WordAlign;

architecture beh of ACU_WordAlign is

--
-- Constants declaration
--constant SymbolSearchArrayLength   : unsigned(3 downto 0):= to_unsigned(10,4);       -- The comma is in 2 consecutive packages searched
constant SymbolSearchArrayLength   : unsigned(3 downto 0):= to_unsigned(6,4);        -- The comma is in 1 package+1 symbol searched
constant PackageLength             : unsigned(3 downto 0):= to_unsigned(5,4);        -- One package is made of 5 symbols

--
-- Signals declaration
type fsmState is (CheckSymbols, WaitNextPackage, GenAlign);
signal fsm_state                   : fsmState;

signal cntSymbols                  : unsigned(3 downto 0);                           -- Symbol counter
signal cntAlign                    : unsigned(3 downto 0);                           -- Word aligned counter
signal AlignFlag                   : std_logic;                                      -- It goes high after gNrKey4Alig key word found
                                                                                     -- and it goes low after the first key word not found
signal parityFlag                  : std_logic;                                      -- Active high when the symbol is a parity one.
signal NewSymbolIn_s0              : std_logic;                                      -- NewSymbolIn sampled signal
signal SymbolIn_s0                 : std_logic_vector(9 downto 0);                   -- SymbolIn sampled signal
begin

g_CLKRE:if gClkRE_l_FE_H ='0' generate

  p_fsm:process(Reset,Clock)
  begin

    if (Reset='1') then

      fsm_state               <= CheckSymbols;

      cntSymbols              <= (others=>'0');
      cntAlign                <= (others=>'0');
      AlignFlag               <= '0';
      NewSymbolOut            <= '0';
      SymbolOut               <= (others=>'0');
      parityFlag              <= '0';
      rxDt_align              <= '0';
      NewSymbolIn_s0          <= '0';
      SymbolIn_s0             <= (others=>'0');
    elsif(Clock'event and Clock='1') then
      
      NewSymbolIn_s0  <= NewSymbolIn;
      SymbolIn_s0     <= SymbolIn;

      NewSymbolOut  <= '0';
      if (NewSymbolIn_s0='1') then
        SymbolOut     <= AlignFlag & parityFlag & SymbolIn_s0;
        NewSymbolOut  <= '1';
      end if;

      case fsm_state is

        when  CheckSymbols =>

          parityFlag    <= '0';
          rxDt_align    <= '0';

          if (NewSymbolIn='1') then

            --Signals evolution
            if (cntSymbols < SymbolSearchArrayLength-1) then
              cntSymbols  <= cntSymbols + 1;
            else
              cntSymbols  <= (others=>'0');
            end if;   

            if (cntAlign= 0) then
              AlignFlag <= '0';
            elsif (cntAlign = gNrKey4Alig-1) then
              AlignFlag <= '1';
            end if;
          
            -- State evolution
            if ((SymbolIn = gKxy) or (SymbolIn = not(gKxy))) then
              fsm_state   <= WaitNextPackage;
              cntSymbols  <= (others=>'0');
              if (cntAlign < gNrKey4Alig-1) then
                cntAlign    <= cntAlign + 1;
              end if;
            else
              cntAlign    <= (others=>'0');
              if (cntSymbols = SymbolSearchArrayLength-1) then
                fsm_state   <= GenAlign;
                cntSymbols  <= (others=>'0');
              end if;

            end if;

          end if;
        
        when WaitNextPackage =>


          if (NewSymbolIn='1') then

            if (cntSymbols < PackageLength-2) then
              cntSymbols  <= cntSymbols + 1;
              parityFlag  <= '0';
            else
              cntSymbols  <= (others=>'0');
              fsm_state   <= CheckSymbols;

              parityFlag  <= '1';
            end if; 

          end if;

        when GenAlign =>
          rxDt_align  <= '1';
          fsm_state   <= CheckSymbols;

        when others =>
          fsm_state               <= CheckSymbols;

          cntSymbols              <= (others=>'0');
          cntAlign                <= (others=>'0');
          AlignFlag               <= '0';
          parityFlag              <= '0';
          rxDt_align              <= '0';

      end case;
 
    end if;
  end process p_fsm;
end generate g_CLKRE;


g_CLKFE:if gClkRE_l_FE_H ='1' generate

  p_fsm:process(Reset,Clock)
  begin

    if (Reset='1') then

      fsm_state               <= CheckSymbols;

      cntSymbols              <= (others=>'0');
      cntAlign                <= (others=>'0');
      AlignFlag               <= '0';
      NewSymbolOut            <= '0';
      SymbolOut               <= (others=>'0');
      parityFlag              <= '0';
      rxDt_align              <= '0';
      NewSymbolIn_s0          <= '0';
      SymbolIn_s0             <= (others=>'0');
    elsif(Clock'event and Clock='0') then
      
      NewSymbolIn_s0  <= NewSymbolIn;
      SymbolIn_s0     <= SymbolIn;

      NewSymbolOut  <= '0';
      if (NewSymbolIn_s0='1') then
        SymbolOut     <= AlignFlag & parityFlag & SymbolIn_s0;
        NewSymbolOut  <= '1';
      end if;

      case fsm_state is

        when  CheckSymbols =>

          parityFlag    <= '0';
          rxDt_align    <= '0';

          if (NewSymbolIn='1') then

            --Signals evolution
            if (cntSymbols < SymbolSearchArrayLength-1) then
              cntSymbols  <= cntSymbols + 1;
            else
              cntSymbols  <= (others=>'0');
            end if;   

            if (cntAlign= 0) then
              AlignFlag <= '0';
            elsif (cntAlign = gNrKey4Alig-1) then
              AlignFlag <= '1';
            end if;
          
            -- State evolution
            if ((SymbolIn = gKxy) or (SymbolIn = not(gKxy))) then
              fsm_state   <= WaitNextPackage;
              cntSymbols  <= (others=>'0');
              if (cntAlign < gNrKey4Alig-1) then
                cntAlign    <= cntAlign + 1;
              end if;
            else
              cntAlign    <= (others=>'0');
              if (cntSymbols = SymbolSearchArrayLength-1) then
                fsm_state   <= GenAlign;
                cntSymbols  <= (others=>'0');
              end if;

            end if;

          end if;
        
        when WaitNextPackage =>


          if (NewSymbolIn='1') then

            if (cntSymbols < PackageLength-2) then
              cntSymbols  <= cntSymbols + 1;
              parityFlag  <= '0';
            else
              cntSymbols  <= (others=>'0');
              fsm_state   <= CheckSymbols;

              parityFlag  <= '1';
            end if; 

          end if;

        when GenAlign =>
          rxDt_align  <= '1';
          fsm_state   <= CheckSymbols;

        when others =>
          fsm_state               <= CheckSymbols;

          cntSymbols              <= (others=>'0');
          cntAlign                <= (others=>'0');
          AlignFlag               <= '0';
          parityFlag              <= '0';
          rxDt_align              <= '0';

      end case;
 
    end if;
  end process p_fsm;

end generate g_CLKFE;

end beh;
