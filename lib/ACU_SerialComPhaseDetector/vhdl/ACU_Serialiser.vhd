------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_Serialiser.vhd                                                                                      --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 17/02/2022                                                                                              --
--                                                                                                                    --
-- Comments : This module asks for 10b symbols with a frequency of 10MHz and seialises it generating a 100Mbps        --
--            dataflow (@100MHz). It is placed after a FIFO that collects the splitted and encoded data.              --
--            The serialisator is sincronised with the FIFO's rd_empty falling edge; in this way, as soon as a symbol --
--            is available, it is immediatelly sent.                                                                  -- 
--                                                                                                                    --
-- History  : Start up version 17/02/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------
--        __    __    __    __    __    __    __    __    __    __    __    __    __    __ 	 __    __    __    __    --
-- clk __|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|      --
--         __________                                                                                                 --
-- rdempty           |_______________________________________________________________________________________________ --
--                    _____                                                       _____                               --
-- rdreq   __________|     |_____________________________________________________|     |_____________________________ --
--                                                                                                                    --
-- tx_in                   X  dt0_0                                                    X          dt0_1               -- 
--                                                                                                                    --   
-- tx_in_s0                      X                              dt0_0                        X        dt0_1           --
--                                                                                                                    --
-- cnt_Ser                       X  0  X  1  X  2  X  3  X  4  X  5  X  6  X  7  X  8  X  9  X  0  X  1  X  2  X  3   --
--                                                                                                                    --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity ACU_Serialiser is

  Port(
    Clock    : in  std_logic;                     -- Clock signal @ 100 MHz in phase with the data
    Reset    : in  std_logic;                     -- Asynchronous reset signal active high

    tx_in    : in  std_logic_vector(9 downto 0);  -- Data from FIFO 
    rdempty  : in  std_logic;                     -- FIFO empty status

    rdreq    : out std_logic;                     -- FIFO read request
    tx_out   : out std_logic                      -- Serialised data 
  );

end Entity ACU_Serialiser;

architecture beh of ACU_Serialiser is

--
-- Constants declaration
constant Tc_cnt_Ser       : unsigned(3 downto 0):=to_unsigned(10,4);  -- It can be considered as the serialisator factor

constant Sync_cnt_Ser     : unsigned(3 downto 0):=to_unsigned(7,4);   -- cnt_Ser sync value

constant DtSamp_cnt_Ser   : unsigned(3 downto 0):=to_unsigned(9,4);   -- cnt_Ser data sampling position
-- 
-- Signals declaration
signal rdempty_s0         : std_logic;                                -- rdempty sampled signal
signal rdempty_FE         : std_logic;                                -- rdempty falling edge signal
signal tx_in_s0           : std_logic_vector(9 downto 0);             -- PISO register
signal cnt_Ser            : unsigned(3 downto 0);                     -- serialisator counter


begin

-- Serializer
p_serializer:process(Clock,Reset)
begin
  if (Reset='1') then
    rdreq       <= '0';
    cnt_Ser     <= (others=>'0');
    tx_in_s0    <= (others=>'0');
    rdempty_s0  <= '0';
    rdempty_FE  <= '0';
  elsif(Clock'event and Clock='1') then
    rdempty_s0  <= rdempty;
    rdempty_FE  <= rdempty_s0 and not rdempty;
   
    --if (rdempty_FE='1') then
    --  cnt_Ser  <= Sync_cnt_Ser;
    --else
      if (cnt_Ser < Tc_cnt_Ser-1) then
        cnt_Ser  <= cnt_Ser + 1;
      else
        cnt_Ser  <= (others=>'0');
      end if;
    --end if;
    
    rdreq  <= '0';
    if (cnt_Ser = Sync_cnt_Ser) then
      rdreq  <= '1';
    end if;

    if (cnt_Ser = DtSamp_cnt_Ser) then
      tx_in_s0 <= tx_in;
    else
      tx_in_s0 <= tx_in_s0(8 downto 0) & tx_in_s0(9);
    end if;

  end if;
end process;

-- Output
tx_out   <= tx_in_s0(9);

end beh;
