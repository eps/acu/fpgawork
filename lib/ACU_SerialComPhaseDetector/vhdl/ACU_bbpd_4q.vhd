library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.ceil;
use ieee.math_real.log2;

use ieee.std_logic_misc.all;

--use work.helper_pkg.assign_pattern;
--use work.helper_pkg.nor_reduce;
--use work.helper_pkg.or_reduce;

--! Modified 4 Quadrant Alexander Bang-Bang Phase Detector
--! All output signals are synchronous to the "clk" input signal
entity ACU_bbpd_4q is
    port (
        clk        : in std_logic;                     --! input clock
        clk90deg   : in std_logic;                     --! input clock which is shifted 90 degree with respect to clk
        reset_n    : in std_logic;                     --! Synchronous reset. Active low.
        din        : in std_logic;                     --! Serialized data stream input
        mux_select : out std_logic_vector(3 downto 0); --! One-Hot encoded quadrant selector. [3]: much late, [2]: late, [1]: early, [0]: much early.
        locked     : out std_logic;                    --! asserted when the beste quadrant has been selected for sampling. Indicates that dout and mux_select signals are valid

		  serialDtOut  : out std_logic;

        dout       : out std_logic_vector(9 downto 0)  --! data sampled on the "correct" edge which is ideally shifted 180deg to the din transition.
    );
end entity ACU_bbpd_4q;

architecture rtl of ACU_bbpd_4q is

    signal s_pd0_din_z1_fe        : std_logic; -- samples in din on the falling edge (180 deg shifted to din change) (wrt. clk)
    signal s_pd0_din_z2_fe_180deg : std_logic; -- propagates s_pd0_din_z1_fe on the rising edge  (wrt. clk)
    signal s_pd0_din_z1_re        : std_logic; -- samples in din on the rising edge (0 deg shifted to din change)  (wrt. clk)
    signal s_pd0_din_z2_re        : std_logic; -- propagates s_pd0_din_z1_re on the rising edge  (wrt. clk)

    signal s_pd0_early : std_logic;
    signal s_pd0_late  : std_logic;
    -- signal s_pd0_hold  : std_logic;

    signal s_pd90_din_z1_fe        : std_logic; -- samples in din on the falling edge (180 deg shifted to din change)  (wrt. clk90deg)
    signal s_pd90_din_z2_fe_180deg : std_logic; -- propagates s_pd0_din_z1_fe on the rising edge  (wrt. clk90deg)
    signal s_pd90_din_z1_re        : std_logic; -- samples in din on the rising edge (0 deg shifted to din change)  (wrt. clk90deg)
    signal s_pd90_din_z2_re        : std_logic; -- propagates s_pd0_din_z1_re on the rising edge  (wrt. clk90deg)

    signal s_pd90_early : std_logic;
    signal s_pd90_late  : std_logic;
    -- signal s_pd90_hold  : std_logic;

    signal s_quadrant          : std_logic_vector(3 downto 0); -- [0]: much early, [1]: early, [2]: late, [3]: much late.
    signal s_quadrant_z1       : std_logic_vector(3 downto 0); -- registered signal of s_quadrant and therefore delayed by 1 clk cycle
    alias s_much_early         : std_logic is s_quadrant(0);
    alias s_early              : std_logic is s_quadrant(1);
    alias s_late               : std_logic is s_quadrant(2);
    alias s_much_late          : std_logic is s_quadrant(3);
    signal s_locked            : std_logic;
    signal s_shiftreg_selector : std_logic_vector(3 downto 0);

    -- Data sampling shift registers
    signal s_sample_sreg_much_early : std_logic_vector(9 downto 0);
    signal s_sample_sreg_early      : std_logic_vector(9 downto 0);
    signal s_sample_sreg_late       : std_logic_vector(9 downto 0);
    signal s_sample_sreg_much_late  : std_logic_vector(9 downto 0);

    constant c_counter_bitwidth        : positive                                  := positive(ceil(log2(real(10))));
    constant c_num_transitions_to_lock : unsigned(c_counter_bitwidth - 1 downto 0) := to_unsigned(2, c_counter_bitwidth); -- 2 is after sending comma K28.5
    constant c_zero                    : unsigned(c_counter_bitwidth - 1 downto 0) := to_unsigned(0, c_counter_bitwidth);
    constant c_one                     : unsigned(c_counter_bitwidth - 1 downto 0) := to_unsigned(1, c_counter_bitwidth);
    constant c_top                     : unsigned(c_counter_bitwidth - 1 downto 0) := to_unsigned(9, c_counter_bitwidth);

    signal s_much_early_counter : unsigned(c_counter_bitwidth - 1 downto 0);
    signal s_early_counter      : unsigned(c_counter_bitwidth - 1 downto 0);
    signal s_late_counter       : unsigned(c_counter_bitwidth - 1 downto 0);
    signal s_much_late_counter  : unsigned(c_counter_bitwidth - 1 downto 0);

    signal s_much_early_counter_is_zero : std_logic;
    signal s_early_counter_is_zero      : std_logic;
    signal s_late_counter_is_zero       : std_logic;
    signal s_much_late_counter_is_zero  : std_logic;

    -- counter direction to keep track of the delta. "10" counting up, "01" counting down, "00" no change, "11" invalid/undefined
    type quadrant_t is array (0 to 1) of std_logic_vector(3 downto 0); -- [0]: much early, [1]: early, [2]: late, [3]: much late.
    signal s_quadrant_counting_up   : quadrant_t;
    signal s_quadrant_counting_down : std_logic_vector(3 downto 0);

    signal s_data_out : std_logic_vector(9 downto 0);

	 signal s_serialDtOut  : std_logic;
begin

    --mux_select <= s_shiftreg_selector;
    mux_select <= s_pd0_early & s_pd0_late & s_pd90_early & s_pd90_late;
    ------------------------------------------------------------------
    -- Start of the basic modified/4-quadrant bang bang Phase detector
    ------------------------------------------------------------------

--    s_pd0_early <= not (s_pd0_din_z2_re xor s_pd0_din_z2_fe_180deg) and (s_pd0_din_z2_fe_180deg xor s_pd0_din_z1_re);
--    s_pd0_late  <= (s_pd0_din_z2_re xor s_pd0_din_z2_fe_180deg) and not (s_pd0_din_z2_fe_180deg xor s_pd0_din_z1_re);
    s_pd0_late  <= not (s_pd0_din_z2_re xor s_pd0_din_z2_fe_180deg) and (s_pd0_din_z2_fe_180deg xor s_pd0_din_z1_re);
    s_pd0_early <= (s_pd0_din_z2_re xor s_pd0_din_z2_fe_180deg) and not (s_pd0_din_z2_fe_180deg xor s_pd0_din_z1_re);


    -- s_pd0_hold  <= (s_pd0_din_z2_re xor s_pd0_din_z2_fe_180deg) xnor (s_pd0_din_z2_fe_180deg xor s_pd0_din_z1_re);

--    s_pd90_early <= not (s_pd90_din_z2_re xor s_pd90_din_z2_fe_180deg) and (s_pd90_din_z2_fe_180deg xor s_pd90_din_z1_re);
--    s_pd90_late  <= (s_pd90_din_z2_re xor s_pd90_din_z2_fe_180deg) and not (s_pd90_din_z2_fe_180deg xor s_pd90_din_z1_re);
    s_pd90_late   <= not (s_pd90_din_z2_re xor s_pd90_din_z2_fe_180deg) and (s_pd90_din_z2_fe_180deg xor s_pd90_din_z1_re);
    s_pd90_early  <= (s_pd90_din_z2_re xor s_pd90_din_z2_fe_180deg) and not (s_pd90_din_z2_fe_180deg xor s_pd90_din_z1_re);

    -- s_pd90_hold  <= (s_pd90_din_z2_re xor s_pd90_din_z2_fe_180deg) xnor (s_pd90_din_z2_fe_180deg xor s_pd90_din_z1_re);

    s_much_early <= s_pd0_early and s_pd90_early;
    s_early      <= s_pd0_early and s_pd90_late;
    s_much_late  <= s_pd0_late and s_pd90_early;
    s_late       <= s_pd0_late and s_pd90_late;

    -- 0 Degree Phase shifted phase detector start

    process (clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                s_pd0_din_z2_re        <= '0';
                s_pd0_din_z2_fe_180deg <= '0';
                s_pd0_din_z1_re        <= '0';
            else
                s_pd0_din_z1_re        <= din;
                s_pd0_din_z2_re        <= s_pd0_din_z1_re;
                s_pd0_din_z2_fe_180deg <= s_pd0_din_z1_fe;
            end if;
        end if;
    end process;

    process (clk)
    begin
        if falling_edge(clk) then
            if reset_n = '0' then
                s_pd0_din_z1_fe <= '0';
            else
                s_pd0_din_z1_fe <= din;
            end if;
        end if;
    end process;

    -- 90 Degree Phase shifted phase detector start

    process (clk90deg)
    begin
        if rising_edge(clk90deg) then
            if reset_n = '0' then
                s_pd90_din_z2_re        <= '0';
                s_pd90_din_z2_fe_180deg <= '0';
                s_pd90_din_z1_re        <= '0';
            else
                s_pd90_din_z1_re        <= din;
                s_pd90_din_z2_re        <= s_pd90_din_z1_re;
                s_pd90_din_z2_fe_180deg <= s_pd90_din_z1_fe;
            end if;
        end if;
    end process;

    process (clk90deg)
    begin
        if falling_edge(clk90deg) then
            if reset_n = '0' then
                s_pd90_din_z1_fe <= '0';
            else
                s_pd90_din_z1_fe <= din;
            end if;
        end if;
    end process;

    ------------------------------------------------------------------
    -- End of the basic modified/4-quadrant bang bang Phase detector.
    ------------------------------------------------------------------
    -- Everything below is used to determine what samples should be 
    -- propagated to the output of this module and when.
    ------------------------------------------------------------------

    locked <= s_locked;
    dout   <= s_data_out;
	 
	 serialDtOut  <= s_serialDtOut;

    -- Data is taken from the output of the second FF
    s_sample_sreg_much_early(0) <= s_pd0_din_z2_fe_180deg;
    s_sample_sreg_early(0)      <= s_pd90_din_z2_re;
    s_sample_sreg_late(0)       <= s_pd0_din_z2_re;
    s_sample_sreg_much_late(0)  <= s_pd90_din_z2_fe_180deg;

    --s_much_early_counter_is_zero <= nor_reduce(s_much_early_counter);
    --s_early_counter_is_zero      <= nor_reduce(s_early_counter);
    --s_late_counter_is_zero       <= nor_reduce(s_late_counter);
    --s_much_late_counter_is_zero  <= nor_reduce(s_much_late_counter);

    s_much_early_counter_is_zero <= not(or_reduce(std_logic_vector(s_much_early_counter)));
    s_early_counter_is_zero      <= not(or_reduce(std_logic_vector(s_early_counter)));
    s_late_counter_is_zero       <= not(or_reduce(std_logic_vector(s_late_counter)));
    s_much_late_counter_is_zero  <= not(or_reduce(std_logic_vector(s_much_late_counter)));
   
  lock_evaluation_proc : process (clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                s_locked            <= '0';
                s_shiftreg_selector <= (others => '0');
                s_quadrant_z1       <= (others => '0');
                s_data_out          <= (others => '0');

					 s_serialDtOut       <= '0';
            else

                s_quadrant_z1 <= s_quadrant;

                if (s_locked = '1') and (s_quadrant_counting_up(1) = s_quadrant_counting_up(0)) then
                    case s_shiftreg_selector is
                        when "1000"           => 
									s_data_out     <= s_sample_sreg_much_late;
									s_serialDtOut  <= s_sample_sreg_much_late(0);
                        when "0100"           => 
									s_data_out     <= s_sample_sreg_late;
									s_serialDtOut  <= s_sample_sreg_late(0);
                        when "0010"           => 
									s_data_out     <= s_sample_sreg_early;
									s_serialDtOut  <= s_sample_sreg_early(0);
                        when "0001"           => 
									s_data_out 		<= s_sample_sreg_much_early;
									s_serialDtOut  <= s_sample_sreg_much_early(0);
                        when others           =>
                            s_data_out    <= (others => '0');
									 s_serialDtOut <= '0';
                    end case;
                else
                    if s_quadrant_z1 = "1000" then
                        if s_much_late_counter >= c_num_transitions_to_lock then
                            s_locked            <= '1';
                            s_data_out          <= s_sample_sreg_much_late;
 									 s_serialDtOut       <= s_sample_sreg_much_late(0);
                            s_shiftreg_selector <= s_quadrant_z1;
                        else
                            s_locked            <= '0';
                            s_data_out          <= (others => '0');
									 s_serialDtOut       <= '0';
                            s_shiftreg_selector <= (others => '0');
                        end if;
                    elsif s_quadrant_z1 = "0100" then
                        if s_late_counter >= c_num_transitions_to_lock then
                            s_locked            <= '1';
                            s_data_out          <= s_sample_sreg_late;
 									 s_serialDtOut       <= s_sample_sreg_late(0);
                            s_shiftreg_selector <= s_quadrant_z1;
                        else
                            s_locked            <= '0';
                            s_data_out          <= (others => '0');
									 s_serialDtOut       <= '0';
                            s_shiftreg_selector <= (others => '0');
                        end if;
                    elsif s_quadrant_z1 = "0010" then
                        if s_early_counter >= c_num_transitions_to_lock then
                            s_locked            <= '1';
                            s_data_out          <= s_sample_sreg_early;
 									 s_serialDtOut       <= s_sample_sreg_early(0);
                            s_shiftreg_selector <= s_quadrant_z1;
                        else
                            s_locked            <= '0';
                            s_data_out          <= (others => '0');
									 s_serialDtOut       <= '0';
                            s_shiftreg_selector <= (others => '0');
                        end if;
                    elsif s_quadrant_z1 = "0001" then
                        if s_much_early_counter >= c_num_transitions_to_lock then
                            s_locked            <= '1';
                            s_data_out          <= s_sample_sreg_much_early;
 									 s_serialDtOut       <= s_sample_sreg_much_early(0);
                            s_shiftreg_selector <= s_quadrant_z1;
                        else
                            s_locked            <= '0';
                            s_data_out          <= (others => '0');
									 s_serialDtOut       <= '0';
                            s_shiftreg_selector <= (others => '0');
                        end if;

                    end if;
                end if;
            end if;
        end if;
    end process;

    preamble_counting_proc : process (clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                s_much_early_counter   <= (others => '0');
                s_early_counter        <= (others => '0');
                s_late_counter         <= (others => '0');
                s_much_late_counter    <= (others => '0');
                s_quadrant_counting_up <= (others => (others => '0'));
            else
                -- whenever a bit pulse was identified shift in the counting "history"
                if (or_reduce(s_quadrant) = '1') then
                    s_quadrant_counting_up(1) <= s_quadrant_counting_up(0);
                end if;

                if (s_much_early) = '1' then
                    s_quadrant_counting_up(0) <= "0001";
                    if s_much_early_counter < c_top then
                        s_much_early_counter <= s_much_early_counter + c_one;
                    end if;
                    if s_early_counter > c_zero then
                        s_early_counter <= s_early_counter - c_one;
                    end if;
                    if s_late_counter > c_zero then
                        s_late_counter <= s_late_counter - c_one;
                    end if;
                    if s_much_late_counter > c_zero then
                        s_much_late_counter <= s_much_late_counter - c_one;
                    end if;
                elsif (s_early) = '1' then
                    s_quadrant_counting_up(0) <= "0010";
                    if s_much_early_counter > c_zero then
                        s_much_early_counter <= s_much_early_counter - c_one;
                    end if;
                    if s_early_counter < c_top then
                        s_early_counter <= s_early_counter + c_one;
                    end if;
                    if s_late_counter > c_zero then
                        s_late_counter <= s_late_counter - c_one;
                    end if;
                    if s_much_late_counter > c_zero then
                        s_much_late_counter <= s_much_late_counter - c_one;
                    end if;
                elsif (s_late) = '1' then
                    s_quadrant_counting_up(0) <= "0100";
                    if s_much_early_counter > c_zero then
                        s_much_early_counter <= s_much_early_counter - c_one;
                    end if;
                    if s_early_counter > c_zero then
                        s_early_counter <= s_early_counter - c_one;
                    end if;
                    if s_late_counter < c_top then
                        s_late_counter <= s_late_counter + c_one;
                    end if;
                    if s_much_late_counter > c_zero then
                        s_much_late_counter <= s_much_late_counter - c_one;
                    end if;
                elsif (s_much_late) = '1' then
                    s_quadrant_counting_up(0) <= "1000";
                    if s_much_early_counter > c_zero then
                        s_much_early_counter <= s_much_early_counter - c_one;
                    end if;
                    if s_early_counter > c_zero then
                        s_early_counter <= s_early_counter - c_one;
                    end if;
                    if s_late_counter > c_zero then
                        s_late_counter <= s_late_counter - c_one;
                    end if;
                    if s_much_late_counter < c_top then
                        s_much_late_counter <= s_much_late_counter + c_one;
                    end if;
                end if;
            end if;
        end if;
    end process;

    process (clk)
    begin
        if rising_edge(clk) then
            if reset_n = '0' then
                s_sample_sreg_much_late(s_sample_sreg_much_late'left downto 1)   <= (others => '0');
                s_sample_sreg_late(s_sample_sreg_late'left downto 1)             <= (others => '0');
                s_sample_sreg_early(s_sample_sreg_early'left downto 1)           <= (others => '0');
                s_sample_sreg_much_early(s_sample_sreg_much_early'left downto 1) <= (others => '0');
            else
                s_sample_sreg_much_late(s_sample_sreg_much_late'left downto 1)   <= s_sample_sreg_much_late(s_sample_sreg_much_late'left - 1 downto 0);
                s_sample_sreg_late(s_sample_sreg_late'left downto 1)             <= s_sample_sreg_late(s_sample_sreg_late'left - 1 downto 0);
                s_sample_sreg_early(s_sample_sreg_early'left downto 1)           <= s_sample_sreg_early(s_sample_sreg_early'left - 1 downto 0);
                s_sample_sreg_much_early(s_sample_sreg_much_early'left downto 1) <= s_sample_sreg_much_early(s_sample_sreg_much_early'left - 1 downto 0);
            end if;
        end if;
    end process;

end architecture;

--library ieee;
--use ieee.std_logic_1164.all;

--package bbpd_4q_pkg is
--    --! Alexander Bang-Bang Phase Detector
--    component bbpd_4q is
--        port (
--            clk        : in std_logic;
--            clk90deg   : in std_logic;
--            reset_n    : in std_logic;
--            din        : in std_logic;
--            mux_select : out std_logic_vector(3 downto 0);
--            locked     : out std_logic;
--            dout       : out std_logic_vector(9 downto 0)
--        );
--    end component bbpd_4q;

--end package;
