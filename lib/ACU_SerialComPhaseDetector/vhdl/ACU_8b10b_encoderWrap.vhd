------------------------------------------------------------------------------------------------------------------------
-- File name: ACU_8b10b_encoderWrap.vhd                                                                                    --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 24/02/2022                                                                                              --
--                                                                                                                    --
-- Comments : This module wraps the 8b10b encoder developed by Frans Schreuder. For more info about the license,      --
--            please check directly his file.                                                                         --
--                                                                                                                    --
-- History  : Start up version 24/02/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.ACU_package_numericLib.all;

Entity ACU_8b10b_encoderWrap is
  Generic (
    gKxy                        : std_logic_vector(7 downto 0):=x"FC";                      -- K.28.7
    gNrByte                     : integer range 1 to 10:= 3                                 -- Payload Byte number
  );
  Port(
    Clock                       : in  std_logic;                                            -- Clock signal
    Reset                       : in  std_logic;                                            -- Asynchronous reset signal active high

    asi_in_8b10b_encoder_valid  : in  std_logic;                                            -- Data valid from source 
    asi_in_8b10b_encoder_data   : in  std_logic_vector ((gNrByte*8)-1 downto 0);            -- Data from source

    aso_out_8b10b_encoder_valid : out std_logic;                                            -- Data valid to sink
    aso_out_8b10b_encoder_data  : out std_logic_vector(9 downto 0)                          -- 1x8b10b symbols to sink 
  );
end Entity ACU_8b10b_encoderWrap;

architecture beh of ACU_8b10b_encoderWrap is

--
-- Constants declaration
constant Tc_cntSplitter         : unsigned (3 downto 0):= to_unsigned(gNrByte+1,4);         -- splitter counter max value
constant Tc_cntTimebase         : unsigned (5 downto 0):= to_unsigned(50,6);                -- 50*10ns=500ns
--
-- Signals declaration
signal parity                   : array8bits(gNrByte downto 0);                             -- internal parity array(the last location  
                                                                                            -- contains the result to send)
signal cntSplitter              : unsigned (3 downto 0);                                    -- Each parallel data in input is splitted in
                                                                                            -- 8 bit data
signal intDataOutvalid          : std_logic;                                                -- internal data out valid signal
signal intDataOutvalid_s0       : std_logic;                                                -- intDataOutvalid sampled signal
signal cntTimebase              : unsigned (5 downto 0);                                    -- 500 ns internal time base counter
signal intDtValid               : std_logic;                                                -- Internal input data valid signal
signal intKI                    : std_logic;                                                -- When high, the data in input to the encoder is a
                                                                                            -- comma symbol (gKxy)
signal dt2Encoder               : std_logic_vector(7 downto 0);                             -- data to encoder

-- 
-- Component declaration
Component enc_8b10b is     
    port( 
        reset : in std_logic;          -- Active high reset
        clk : in std_logic ;           -- Clock to register dataout
        ena : in std_logic ;           -- To validate datain and register dataout and disparity
        KI : in std_logic ;            -- Control (K) input(active high) 
        datain : in std_logic_vector(7 downto 0); --8 bit input data
        dataout : out std_logic_vector(9 downto 0) --10 bit encoded output
        ); 
end component enc_8b10b; 

begin

p_intTimebase:process(Reset,Clock)
begin
  if (Reset='1') then
    cntTimebase  <= (others=>'0');
    intDtValid   <= '0';
  elsif (Clock' event and Clock='1') then

    if (asi_in_8b10b_encoder_valid ='1') then
      cntTimebase  <= (others=>'0');
    else
      if (cntTimebase < Tc_cntTimebase-1) then
        cntTimebase  <= cntTimebase + 1;
      else
        cntTimebase  <= (others=>'0');
      end if;
    end if;

    intDtValid   <= '0';
    if (cntTimebase = Tc_cntTimebase-2) then
      intDtValid   <= '1';
    end if;
  end if; 
end process p_intTimebase;

parity(0)  <= (others=>'0');

parityGen: for i in 0 to gNrByte-1 generate

begin

  p_intParity:process(Reset,Clock)
  begin
    if (Reset='1') then
      parity(i+1)  <= (others=>'0');
    elsif (Clock' event and Clock='1') then
      parity(i+1)  <= parity(i) xor asi_in_8b10b_encoder_data(7+(8*i) downto (8*i));
    end if;     
  end process p_intParity;

end generate parityGen;

p_splitAndEncode: process(Reset,Clock)
begin
  if (Reset='1') then
    cntSplitter                 <= (others=>'0');
    aso_out_8b10b_encoder_valid  <= '0';
    dt2Encoder                  <= (others=>'0');
    intDataOutvalid             <= '0';
    intDataOutvalid_s0          <= '0';
    intKI                       <= '0';
  elsif (Clock' event and Clock='1') then
    -- Splitter counter
    -- [0]       comma
    -- [1]       MSByte
    --  :
    -- [gNrByte] LSByte
    if (intDtValid ='1') then
      cntSplitter  <= (others=>'0');
    elsif (cntSplitter < Tc_cntSplitter) then
      cntSplitter  <= cntSplitter + 1;
    end if;

    -- data out valid    
    intDataOutvalid_s0           <= intDataOutvalid;
    aso_out_8b10b_encoder_valid  <= intDataOutvalid_s0;
    if (intDtValid ='1') then
      intDataOutvalid  <= '1';
    elsif (cntSplitter = Tc_cntSplitter) then
      intDataOutvalid  <= '0';
    end if;

    -- data out
    intKI  <= '0';
    if (cntSplitter = 0) then
      -- comma
      dt2Encoder  <= gKxy;
      intKI       <= '1';
    elsif (cntSplitter = Tc_cntSplitter) then
      -- parity
      dt2Encoder  <= parity(gNrByte)(7 downto 0);    
    else
      -- payload
      dt2Encoder  <= asi_in_8b10b_encoder_data( (gNrByte*8)-1 - to_integer(cntSplitter-1)*8 downto (gNrByte*8)-8 - to_integer(cntSplitter-1)*8);

    end if;

  end if;
end process;

i_encoder: enc_8b10b      
    port map ( 
        reset    => Reset,
        clk      => Clock,
        ena      => intDataOutvalid_s0,
        KI       => intKI,
        datain   => dt2Encoder,
        dataout  => aso_out_8b10b_encoder_data
        ); 


end beh;
