------------------------------------------------------------------------------------------------------------------------
-- File name: Tb_ACU_SerialComPhaseDetector.vhd                                                                                    --
--                                                                                                                    --
-- Author   : D.Rodomonti                                                                                             --
-- Date     : 14/02/2022                                                                                              --
--                                                                                                                    --
-- Comments : ACU_SerialCom_bbpd Testbench                                                                                 -- 
--                                                                                                                    --
-- History  : Start up version 14/02/2022                                                                             --
------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.ACU_package_numericLib.all;

Entity Tb_ACU_SerialComPhaseDetector is
end Entity Tb_ACU_SerialComPhaseDetector;

architecture beh of Tb_ACU_SerialComPhaseDetector is

--
-- Constants declaration

--
-- Signals declaration
  -- Note: in a single FPGA project, just 2 clock signals are necessary for TX and RX side, but in order to emulate a data transmission between
  --       two boards (without any phase relationship each other) 3 clock signal are generated, one for the sender and two for the receiver.
  --       In a single FPGA project, ClockSystem0 and ClockSystem0_RX are the same signal.
signal ClockSystem0       : std_logic:='0';                 -- 100MHz System clock 0°
--signal ClockSystem0_RX    : std_logic:='0';                 -- 100MHz System clock 0°
--signal ClockSystem90_RX   : std_logic:='0';                 -- 100MHz System clock 90°

--signal TX_RX_PhaseShift   : time:=0 ps;

signal Reset              : std_logic:='1';

signal FgStrobe           : std_logic;
signal FgStrobe_sx        : std_logic_vector(7 downto 0);
signal payload            : std_logic_vector(23 downto 0);
signal payload2x12b       : std_logic_vector(23 downto 0);

signal aso_out_8b10b_encoder_valid  : std_logic;  
signal aso_out_8b10b_encoder_data   : std_logic_vector (9 downto 0); 

signal rdemptyTX            : std_logic;
signal rdreqTX              : std_logic;
signal tx_out               : std_logic;
signal tx_outNRZI           : std_logic;
signal tx_in                : std_logic_vector(9 downto 0);

signal ErrGenEnable         : std_logic:='0';
signal Bit2Togle            : std_logic_vector(9 downto 0):="1000000000";
signal Symb2Corrupt         : std_logic_vector(5 downto 0):="000000";
signal NrErrors             : std_logic_vector(3 downto 0):="0000";
--signal tx_out_T             : std_logic;
signal rx_in_0_T            : std_logic;
signal rx_in_180_T          : std_logic;
signal rx_in_90_T           : std_logic;
signal rx_in_270_T          : std_logic;

signal DtInSync_0           : std_logic;                                             -- dtIn synchronised with Clock_0 RE
signal DtInSync_180         : std_logic;                                             -- dtIn synchronised with Clock_0 FE
signal DtInSync_90          : std_logic;                                             -- dtIn synchronised with Clock_90 RE
signal DtInSync_270         : std_logic;                                             -- dtIn synchronised with Clock_90 FE


signal DataPathDel          : time:=0 ps;

--signal rx_in              : std_logic;
signal rx_inNRZI            : std_logic;
signal rx_in_0              : std_logic;
signal rx_in_180            : std_logic;
signal rx_in_90             : std_logic;
signal rx_in_270            : std_logic;

signal rx_out_0_Valid       : std_logic;
signal rx_out_0             : std_logic_vector(9 downto 0);
signal rx_out_180_Valid     : std_logic;
signal rx_out_180           : std_logic_vector(9 downto 0);
signal rx_out_90_Valid      : std_logic;
signal rx_out_90            : std_logic_vector(9 downto 0);
signal rx_out_270_Valid     : std_logic;
signal rx_out_270           : std_logic_vector(9 downto 0);

signal WA_NewSymbolOut_0    : std_logic;
signal WA_SymbolOut_0       : std_logic_vector(11 downto 0);
signal WA_NewSymbolOut_180  : std_logic;
signal WA_SymbolOut_180     : std_logic_vector(11 downto 0);
signal WA_NewSymbolOut_90   : std_logic;
signal WA_SymbolOut_90      : std_logic_vector(11 downto 0);
signal WA_NewSymbolOut_270  : std_logic;
signal WA_SymbolOut_270     : std_logic_vector(11 downto 0);

signal RxSymbol_0           : std_logic_vector(11 downto 0);
signal RxSymbol_180         : std_logic_vector(11 downto 0);
signal RxSymbol_90          : std_logic_vector(11 downto 0);
signal RxSymbol_270         : std_logic_vector(11 downto 0);

signal RxSymbol_0_Valid     : std_logic;
signal RxSymbol_180_Valid   : std_logic;
signal RxSymbol_90_Valid    : std_logic;
signal RxSymbol_270_Valid   : std_logic;


signal rxDt_align_0         : std_logic;
signal rxDt_align_180       : std_logic;
signal rxDt_align_90        : std_logic;
signal rxDt_align_270       : std_logic;

signal parityFlagOut_0            : std_logic;
signal CommaFlag_0                : std_logic;
signal IllegalSymbol_0            : std_logic;
signal DispError_0                : std_logic;
signal aso_8b10b_decoder_valid_0  : std_logic;
signal aso_8b10b_decoder_data_0   : std_logic_vector (7 downto 0);

signal parityFlagOut_180            : std_logic;
signal CommaFlag_180                : std_logic;
signal IllegalSymbol_180            : std_logic;
signal DispError_180                : std_logic;
signal aso_8b10b_decoder_valid_180  : std_logic;
signal aso_8b10b_decoder_data_180   : std_logic_vector (7 downto 0);

signal parityFlagOut_90            : std_logic;
signal CommaFlag_90                : std_logic;
signal IllegalSymbol_90            : std_logic;
signal DispError_90                : std_logic;
signal aso_8b10b_decoder_valid_90  : std_logic;
signal aso_8b10b_decoder_data_90   : std_logic_vector (7 downto 0);

signal parityFlagOut_270            : std_logic;
signal CommaFlag_270                : std_logic;
signal IllegalSymbol_270            : std_logic;
signal DispError_270                : std_logic;
signal aso_8b10b_decoder_valid_270  : std_logic;
signal aso_8b10b_decoder_data_270   : std_logic_vector (7 downto 0);

signal Branch_0             : std_logic_vector(12 downto 0); 
signal Branch_180           : std_logic_vector(12 downto 0); 
signal Branch_90            : std_logic_vector(12 downto 0); 
signal Branch_270           : std_logic_vector(12 downto 0); 

signal PD_RxSymbol          : std_logic_vector (7 downto 0);  
signal PD_RxSymbol_Valid    : std_logic;
signal Tc_cntEL_Pulses      : std_logic_vector(15 downto 0); 
signal Tc_cnt_Stabil        : std_logic_vector(15 downto 0);
signal parityFlag           : std_logic;   

signal RstError8b10b        : std_logic:='0';
signal parityFlagOut        : std_logic;
signal WordAligned          : std_logic;


signal CommaFlag             : std_logic;                                     

signal ReceivedStream        : std_logic_vector(23 downto 0);

signal aso_payload_valid     : std_logic;
signal aso_payload_data      : std_logic_vector(23 downto 0);

signal ReceivedStreamArr     : array24bits(3 downto 0);
signal SentStreamArr         : array24bits(3 downto 0);

signal Alarm                 : std_logic;

signal FreeRunning           : std_logic:='0';
signal Start                 : std_logic:='0';
signal phasestep             : std_logic;
signal phaseupdown           : std_logic;

signal ClockSerialCom_Tx     : std_logic;
signal ClockSerialCom_Rx_0   : std_logic;
signal ClockSerialCom_Rx_90  : std_logic;

signal reset_n               : std_logic:='0';
signal SerialDt              : std_logic;

signal PayloadRx_Valid       : std_logic;
signal PayloadRx             : std_logic_vector(23 downto 0);
signal PayloadRx_Valid_CV    : std_logic;
signal PayloadRx_CV          : std_logic_vector(23 downto 0);

-------------------------------------------------------------------------------------

signal serialDtOut_bbpd                  : std_logic;
signal NRZId_bbpd                        : std_logic;
signal rxDt_align_bbpd                   : std_logic;
signal des_rx_outValid_bbpd              : std_logic;
signal des_rx_out_bbpd                   : std_logic_vector(9 downto 0);
signal wa_NewSymbolOut_bbpd              : std_logic;
signal wa_SymbolOut_bbpd                 : std_logic_vector(11 downto 0);
signal dec_parityFlagOut_bbpd            : std_logic;
signal dec_CommaFlag_bbpd                : std_logic;
signal dec_aso_8b10b_decoder_valid_bbpd  : std_logic;
signal dec_aso_8b10b_decoder_data        : std_logic_vector(7 downto 0);
signal NewPkg_bbpd                       : std_logic;
signal Pkg_bbpd                          : std_logic_vector(23 downto 0);
signal D_NRZId_bbpd                      : std_logic;
signal locked_bbpd                       : std_logic;
-------------------------------------------------------------------------------------
--
-- Components declaration
Component cntFreeWheel is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       enable           : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntFreeWheel;

Component cntCmdInDriven is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       incrCmd          : in std_logic;
       enable           : out std_logic;
       cntOutValid      : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end component cntCmdInDriven;

Component ACU_NRZI_decoder is
  Generic(
    gClkRE_l_FE_H : std_logic:='0'
  );
  Port(
    Clock    : in  std_logic;  -- Clock signal @ 100 MHz in phase with the data
    Reset    : in  std_logic;  -- Asynchronous reset signal active high

    D        : in  std_logic;  -- Data to decode 
    Q        : out std_logic   -- Decoded data
  );

end Component ACU_NRZI_decoder;

Component ACU_Deserialiser is
  Generic(
    gClkRE_l_FE_H : std_logic:='0'
  );
  Port(
    Clock        : in  std_logic;                     -- Clock signal @ 100 MHz in phase with the data
    Reset        : in  std_logic;                     -- Asynchronous reset signal active high

    rx_in        : in  std_logic;                     -- Serial data 
    rxDt_align   : in  std_logic;                     -- Pulse active high used to offset the cnt_Des counter
                                                      -- It has to be more than one clock cycle long in order to properly manage the
                                                      -- clock cross domain.

    rx_outValid  : out std_logic;                     -- rx_out valid signal (one pulse active high)
    rx_out       : out std_logic_vector(9 downto 0)   -- Symblol (deserialised data) 
  );

end Component ACU_Deserialiser;


Component ACU_WordAlign is
  Generic (
    gClkRE_l_FE_H                : std_logic:='0';                                            -- Clock catching edge
    gNrSymbProPck                : integer range 1 to 64:= 5;                                 -- Number of symbols in a package
    gKxy                         : std_logic_vector(9 downto 0):="0011111000";                -- Preamble to search (0x0F8)
    gNrKey4Alig                  : unsigned(2 downto 0):=to_unsigned(5,3)                     -- Number key words to find to declare the alignement
  ); 
  Port(
    Clock                        : in  std_logic;                                             -- Clock signal
    Reset                        : in  std_logic;                                             -- Asynchronous reset signal active high

    NewSymbolIn                  : in  std_logic;                                             -- New data valid symbol 
    SymbolIn                     : in  std_logic_vector(9 downto 0);                          -- Data symbol

    rxDt_align                   : out std_logic;                                             -- 4 Clock pulses long signal used to align the
                                                                                              -- word
    NewSymbolOut                 : out std_logic;                                             -- New data valid out symbol 
    SymbolOut                    : out std_logic_vector(11 downto 0)                          -- Data out symbol: (11) WordAligned
                                                                                              --                  (10) Parity flag
                                                                                              --                  (9 downto 0) Symbol
  );
end Component ACU_WordAlign;

Component ACU_8b10b_decoderWrap is

  Port(
    Clock                        : in  std_logic;                                     -- Clock signal
    Reset                        : in  std_logic;                                     -- Asynchronous reset signal active high

    NewInPhaseSymbol             : in  std_logic;                                     -- New in phase data valid symbol 
    InPhaseSymbol                : in  std_logic_vector(9 downto 0);                  -- In phase data symbol

    parityFlagIn                 : in  std_logic;                                     -- Selected parity flag
    parityFlagOut                : out std_logic;                                     -- parityFlagIn sampled signal
    CommaFlag                    : out std_logic;                                     -- Active high when the decoder detects a comma symbol

    code_errOut                  : out std_logic;                                     --Indication for illegal character
    disp_errOut                  : out std_logic;                                     --Indication for disparity error

    aso_8b10b_decoder_valid      : out std_logic;                                     -- Data valid to parity checker 
    aso_8b10b_decoder_data       : out std_logic_vector (7 downto 0)                  -- Data to parity checker 
  );
end Component ACU_8b10b_decoderWrap;

Component ACU_SerialCommParityChecker is
  Generic (
    gMaxParityErrorNr  : unsigned(3 downto 0):=to_unsigned(5,4)  -- Maximum number of consecutives parity errors before
                                                                          -- declaring communication failed
  );
  Port(
    Clock              : in  std_logic;                          -- Clock signal
    Reset              : in  std_logic;                          -- Asynchronous reset signal active high

    --WordAligned        : in std_logic;                           -- Active high when the receiver is word aligned
    parityFlag         : in std_logic;                           -- It is high when asi_decoder_data carries out the received parity byte.
    CommaFlag          : in std_logic;                           -- Active high when the decoder detects a comma symbol

    asi_decoder_valid  : in std_logic;                           -- Data valid to parity checker 
    asi_decoder_data   : in std_logic_vector (7 downto 0);       -- Data to parity checker 


    RstParityCntError  : in std_logic;                           -- Parity error counter reset signal
    ParityCntError     : out std_logic_vector(3 downto 0);       -- Parity error counter
    CommFailed         : out std_logic;                          -- Active high when ParityCntError=gMaxParityErrorNr
    aso_payload_valid  : out std_logic;                          -- Data valid to parity checker 
    aso_payload_data   : out std_logic_vector (23 downto 0)      -- Data to parity checker 

  );
end Component ACU_SerialCommParityChecker;

Component SerialDataErrorGen is
  Generic (
    gClkRE_l_FE_H               : std_logic:='0';                                  -- clocl sampling edge
    gBitXsymb                   : integer range 1 to 64 := 10;                     -- Number of bits per symbol
    gSymbXpack                  : integer range 1 to 64 := 5                       -- Number of symbol per package
  );
  Port(
    Clock                       : in  std_logic;                                   -- Clock signal
    Reset                       : in  std_logic;                                   -- Asynchronous reset signal active high

    Enable                      : in  std_logic;                                   -- Active high module enable
    Bit2Togle                   : in  std_logic_vector(gBitXsymb-1 downto 0);      -- One hot bit togle mask
    Symb2Corrupt                : in  std_logic_vector(5 downto 0);                -- Symbol in which the bit has to be togled
    NrErrors                    : in  std_logic_vector(3 downto 0);                -- Number of consecutive pakages to be corupted

    serialIn                    : in  std_logic;                                   -- Serial data input
    serialOut                   : out std_logic                                    -- Serial data output
  );
end Component SerialDataErrorGen;

Component ACU_PLL_SeialCom IS
	PORT
	(
		areset		: IN STD_LOGIC  := '0';
		inclk0		: IN STD_LOGIC  := '0';
		phasecounterselect		: IN STD_LOGIC_VECTOR (2 DOWNTO 0) :=  (OTHERS => '0');
		phasestep		: IN STD_LOGIC  := '0';
		phaseupdown		: IN STD_LOGIC  := '0';
		scanclk		: IN STD_LOGIC  := '1';
		c0		: OUT STD_LOGIC ;
		c1		: OUT STD_LOGIC ;
		c2		: OUT STD_LOGIC ;
		locked		: OUT STD_LOGIC ;
		phasedone		: OUT STD_LOGIC 
	);
END Component ACU_PLL_SeialCom;

Component phaseShiftDriver is
  generic (
       gTcCntPhasestep      : integer range 1 to 256 :=128;
       gBurstLength         : integer range 1 to 256 :=32;
       gTimeBtwnPhaseSteps  : integer range 1 to 65536 :=1000    -- 10 us with clock @100MHz
    );
  port (
       clock                : in std_logic;
       reset                : in std_logic;
       FreeRunning          : in std_logic;
       Start                : in std_logic;
       Phasestep            : out std_logic
    
    );
end Component phaseShiftDriver;

Component PayloadChecker is
  Generic (
    gCounterInInput             : std_logic:='1';                                  -- Expected a running up counter +1 incremented in input
    gDataWidth                  : integer range 1 to 64 := 32                      -- Number of bits per package
  );
  Port(
    Clock                       : in  std_logic;                                   -- Clock signal
    Reset                       : in  std_logic;                                   -- Asynchronous reset signal active high
    
    DataIn_valid                : in  std_logic;                                   -- DataIn valid (pulse active high)
    DataIn                      : in  std_logic_vector(gDataWidth-1 downto 0);     -- Received data
    Delta                       : in  std_logic_vector(gDataWidth-1 downto 0);     -- Difference between two consecutive received data

    RstAlarm                    : in  std_logic;                                   -- Reset LatchAlarm signal active high

    LatchAlarm                  : out std_logic;                                   -- Alarm latched signal
    Alarm                       : out std_logic                                    -- active high when DataIn1/=DataIn2

  );  
end Component PayloadChecker;

Component ACU_SerialCom_bbpd is
  Generic(
      gCyclonIII_n_CyclonV  : std_logic := '0'        -- 0 = Module implemented in a CyclonIII FPGA; 1= module implemented in a CyclonV FPGA		
  );

  Port(
    SystemClock              : in  std_logic;                          -- System Clock signal @100 MHz
    Reset                    : in  std_logic;                          -- Asynchronous reset signal active high

    PayloadTx_Valid          : in  std_logic;                          -- Data valid from source 
    PayloadTx                : in  std_logic_vector(23 downto 0);      -- Data from source

    ClockSerialCom_0         : in  std_logic;                          -- Serial comunication Clock signal @100 MHz 0°
    ClockSerialCom_90        : in  std_logic;                          -- Serial comunication Clock signal @100 MHz 90°

    Tx_out                   : out std_logic;                          -- Serial data output NRZI encoded (100Mbps @50MHz)

    Rx_in                    : in  std_logic;                          -- Serial data input

    Tc_cntEL_Pulses          : in  std_logic_vector(15 downto 0);      -- Phase detector filter value: it is used to define the branch selection
                                                                       -- direction
    Tc_cnt_Stabil            : in  std_logic_vector(15 downto 0);      -- Stability symbols value: it is used to confirm the stability on the 
                                                                       -- next to be selected branch
    RstError                 : in  std_logic;                          -- The RE resets IllegalSymbol, DispError and WordAlignL
    IllegalSymbol            : out std_logic;                          -- code_err latched signal
    DispError                : out std_logic;                          -- disp_err latched signal
    WordAlignL               : out std_logic;                          -- word align latched signal

    SelBranch                : out std_logic_vector(1 downto 0);       -- Selected branch:
                                                                       -- 0 => 270°
                                                                       -- 1 => 180°
                                                                       -- 2 => 0°
                                                                       -- 3 => 90°

    ParityCntError           : out std_logic_vector(3 downto 0);       -- Parity error counter
    CommFailed               : out std_logic;                          -- Active high when ParityCntError=gMaxParityErrorNr

    PayloadRx_Valid          : out std_logic;                          -- Data valid from sender 
    PayloadRx                : out std_logic_vector(23 downto 0)       -- Data from sender


  );
end Component ACU_SerialCom_bbpd;

Component ACU_bbpd_4q is
    port (
        clk        : in std_logic;                     --! input clock
        clk90deg   : in std_logic;                     --! input clock which is shifted 90 degree with respect to clk
        reset_n    : in std_logic;                     --! Synchronous reset. Active low.
        din        : in std_logic;                     --! Serialized data stream input
        mux_select : out std_logic_vector(3 downto 0); --! One-Hot encoded quadrant selector. [3]: much late, [2]: late, [1]: early, [0]: much early.
        locked     : out std_logic;                    --! asserted when the beste quadrant has been selected for sampling. Indicates that dout and mux_select signals are valid
		  serialDtOut  : out std_logic;
        dout       : out std_logic_vector(9 downto 0)  --! data sampled on the "correct" edge which is ideally shifted 180deg to the din transition.
    );

end component ACU_bbpd_4q;


begin

ClockSystem0      <= not ClockSystem0 after 5 ns;

Tc_cntEL_Pulses  <= x"00FF";
Tc_cnt_Stabil    <= x"0032";   -- 10 packages=50 symbols

i_phaseShiftDriver: phaseShiftDriver

  port map(
       clock                => ClockSystem0,
       reset                => Reset,
       FreeRunning          => FreeRunning,
       Start                => Start,
       Phasestep            => phasestep
    
    );

-- PLL PHASE SHIFT DRIVEN
phaseupdown  <= '1';
i_ACU_PLL_SeialCom : ACU_PLL_SeialCom
	PORT map
	(
		areset		        => Reset,
		inclk0		        => ClockSystem0,
		phasecounterselect  => "010",             -- C0
		phasestep		     => phasestep,
		phaseupdown		     => phaseupdown,
		scanclk		        => ClockSystem0,
		c0		              => ClockSerialCom_Tx,
		c1                  => ClockSerialCom_Rx_0,
		c2                  => ClockSerialCom_Rx_90,
		locked              => open,
		phasedone           => open
	);

-- 2MHz source timebase
i_cntFreeWheel: cntFreeWheel
  generic map(
       gCntOutWidth     => 16,
       gEnTrigValue     => 50          -- 2MHz
--       gEnTrigValue     => 100         -- 1MHz
--       gEnTrigValue     => 200         -- 500KHz
--       gEnTrigValue     => 6250        -- 16KHz
    )
  port map (
       clock            => ClockSystem0,
       reset            => Reset,
       enable           => FgStrobe,
       cntOut           => open
    
    );

-- fg emul
i_cntCmdInDriven: cntCmdInDriven 
  generic map(
       gCntOutWidth     => 24,
       gEnTrigValue     => 16777215
    )
  port map(
       clock            => ClockSystem0,
       reset            => Reset,
       incrCmd          => FgStrobe,
       enable           => open,
       cntOutValid      => open,
       cntOut           => payload
    );

p_resampling:process(ClockSystem0,Reset)
begin
  if (Reset='1') then
    FgStrobe_sx  <= (others=>'0');
  elsif(ClockSystem0'event and ClockSystem0='1') then
    FgStrobe_sx(0)           <= FgStrobe;
    FgStrobe_sx(7 downto 1)  <= FgStrobe_sx(6 downto 0);
  end if;
end process;

-- IP instance
i_ACU_SerialComTX: ACU_SerialCom_bbpd

  Port map(
    SystemClock              => ClockSystem0,
    Reset                    => Reset,

    PayloadTx_Valid          => FgStrobe_sx(2), 
    PayloadTx                => payload,

    ClockSerialCom_0         => ClockSerialCom_Tx,
    ClockSerialCom_90        => ClockSerialCom_Rx_90,

    Tx_out                   => SerialDt,

    Rx_in                    => '0',

    Tc_cntEL_Pulses          => Tc_cntEL_Pulses,
    Tc_cnt_Stabil            => Tc_cnt_Stabil,

    RstError                 => RstError8b10b,
    IllegalSymbol            => open,
    DispError                => open,
    WordAlignL               => open,

    SelBranch                => open,

    ParityCntError           => open,
    CommFailed               => open,

    PayloadRx_Valid          => open,
    PayloadRx                => open


  );

i_ACU_SerialComRX: ACU_SerialCom_bbpd

  Port map(
    SystemClock              => ClockSystem0,
    Reset                    => Reset,

    PayloadTx_Valid          => FgStrobe_sx(2), 
    PayloadTx                => payload,

    ClockSerialCom_0         => ClockSerialCom_Rx_0,
    ClockSerialCom_90        => ClockSerialCom_Rx_90,

    Tx_out                   => open,

    Rx_in                    => SerialDt,

    Tc_cntEL_Pulses          => Tc_cntEL_Pulses,
    Tc_cnt_Stabil            => Tc_cnt_Stabil,

    RstError                 => RstError8b10b,
    IllegalSymbol            => open,
    DispError                => open,
    WordAlignL               => open,

    SelBranch                => open,

    ParityCntError           => open,
    CommFailed               => open,

    PayloadRx_Valid          => PayloadRx_Valid,
    PayloadRx                => PayloadRx


  );



i_ACU_SerialComRX_PayloadChecker: PayloadChecker
  Generic map (
    gCounterInInput             => '1',
    gDataWidth                  => 24
  )
  Port map(
    Clock                       => ClockSystem0,
    Reset                       => Reset,
    
    DataIn_valid                => PayloadRx_Valid,
    DataIn                      => PayloadRx,
    Delta                       => x"000001",

    RstAlarm                    => RstError8b10b,

    LatchAlarm                  => open,
    Alarm                       => open

  );  

---------------------------------------------------------
i_ACU_SerialComRX_CV: ACU_SerialCom_bbpd
  Generic map(
      gCyclonIII_n_CyclonV  => '1'
  )

  Port map(
    SystemClock              => ClockSystem0,
    Reset                    => Reset,

    PayloadTx_Valid          => FgStrobe_sx(2), 
    PayloadTx                => payload,

    ClockSerialCom_0         => ClockSerialCom_Rx_0,
    ClockSerialCom_90        => ClockSerialCom_Rx_90,

    Tx_out                   => open,

    Rx_in                    => SerialDt,

    Tc_cntEL_Pulses          => Tc_cntEL_Pulses,
    Tc_cnt_Stabil            => Tc_cnt_Stabil,

    RstError                 => RstError8b10b,
    IllegalSymbol            => open,
    DispError                => open,
    WordAlignL               => open,

    SelBranch                => open,

    ParityCntError           => open,
    CommFailed               => open,

    PayloadRx_Valid          => PayloadRx_Valid_CV,
    PayloadRx                => PayloadRx_CV


  );



i_ACU_SerialComRX_PayloadChecker_CV: PayloadChecker
  Generic map (
    gCounterInInput             => '1',
    gDataWidth                  => 24
  )
  Port map(
    Clock                       => ClockSystem0,
    Reset                       => Reset,
    
    DataIn_valid                => PayloadRx_Valid_CV,
    DataIn                      => PayloadRx_CV,
    Delta                       => x"000001",

    RstAlarm                    => RstError8b10b,

    LatchAlarm                  => open,
    Alarm                       => open

  );  
---------------------------------------------------------

i_ACU_bbpd_4q: ACU_bbpd_4q
    port map(
        clk        	=> ClockSerialCom_Rx_0,
        clk90deg   	=> ClockSerialCom_Rx_90,
        reset_n    	=> reset_n,
        din        	=> SerialDt,
        mux_select 	=> open,
        locked     	=> locked_bbpd,
	     serialDtOut  => serialDtOut_bbpd,
        dout       	=> open
    );

D_NRZId_bbpd  <= locked_bbpd and serialDtOut_bbpd;

i_ACU_NRZI_decoder: ACU_NRZI_decoder
  Port map(
    Clock    => ClockSerialCom_Rx_0,
    Reset    => Reset,

    D        => serialDtOut_bbpd,
    Q        => NRZId_bbpd
  );



i_ACU_Deserialiser: ACU_Deserialiser
  Port map(
    Clock        => ClockSerialCom_Rx_0,
    Reset        => Reset,

    rx_in        => NRZId_bbpd,
    rxDt_align   => rxDt_align_bbpd,

    rx_outValid  => des_rx_outValid_bbpd,
    rx_out       => des_rx_out_bbpd
  );


i_ACU_WordAlign: ACU_WordAlign
  Port map(
    Clock                        => ClockSerialCom_Rx_0,
    Reset                        => Reset,

    NewSymbolIn                  => des_rx_outValid_bbpd,
    SymbolIn                     => des_rx_out_bbpd,

    rxDt_align                   => rxDt_align_bbpd,

    NewSymbolOut                 => wa_NewSymbolOut_bbpd,
    SymbolOut                    => wa_SymbolOut_bbpd
  );

i_ACU_8b10b_decoderWrap: ACU_8b10b_decoderWrap

  Port map(
    Clock                        => ClockSerialCom_Rx_0,
    Reset                        => Reset,

    NewInPhaseSymbol             => wa_NewSymbolOut_bbpd,
    InPhaseSymbol                => wa_SymbolOut_bbpd(9 downto 0),

    parityFlagIn                 => wa_SymbolOut_bbpd(10),
    parityFlagOut                => dec_parityFlagOut_bbpd,
    CommaFlag                    => dec_CommaFlag_bbpd,

    code_errOut                  => open,
    disp_errOut                  => open,

    aso_8b10b_decoder_valid      => dec_aso_8b10b_decoder_valid_bbpd,
    aso_8b10b_decoder_data       => dec_aso_8b10b_decoder_data 
  );

i_ACU_SerialCommParityChecker: ACU_SerialCommParityChecker
  
  Port map(
    Clock              => ClockSerialCom_Rx_0,
    Reset              => Reset,


    parityFlag         => dec_parityFlagOut_bbpd,
    CommaFlag          => dec_CommaFlag_bbpd,

    asi_decoder_valid  => dec_aso_8b10b_decoder_valid_bbpd,
    asi_decoder_data   => dec_aso_8b10b_decoder_data,


    RstParityCntError  => RstError8b10b,
    ParityCntError     => open,
    CommFailed         => open,
    aso_payload_valid  => NewPkg_bbpd,
    aso_payload_data   => Pkg_bbpd

  );

i_bbpdPayloadChecker: PayloadChecker
  Generic map (
    gCounterInInput             => '1',
    gDataWidth                  => 24
  )
  Port map(
    Clock                       => ClockSerialCom_Rx_0,
    Reset                       => Reset,
    
    DataIn_valid                => NewPkg_bbpd,
    DataIn                      => Pkg_bbpd,
    Delta                       => x"000001",

    RstAlarm                    => RstError8b10b,

    LatchAlarm                  => open,
    Alarm                       => open

  );







end beh;
