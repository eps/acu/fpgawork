restart -f
run 10 us
force -freeze sim:/tb_acu_serialcomphasedetector/i_ACU_NRZI_decoder/Reset 0 0
run 10 us
force -freeze sim:/tb_acu_serialcomphasedetector/i_ACU_bbpd_4q/reset_n 1 0
run 200 us
force -freeze sim:/tb_acu_serialcomphasedetector/i_bbpdPayloadChecker/RstAlarm 1 0
run 10 us
force -freeze sim:/tb_acu_serialcomphasedetector/i_bbpdPayloadChecker/RstAlarm 0 0
run 200 us
force -freeze sim:/tb_acu_serialcomphasedetector/i_phaseShiftDriver/FreeRunning 1 0
force -freeze sim:/tb_acu_serialcomphasedetector/i_phaseShiftDriver/Start 1 0
run -all
