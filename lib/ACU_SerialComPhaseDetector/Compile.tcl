###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work
pwd

vcom -93 ../../ACU_package_numericLib.vhd
vcom -93 ../../SimFolder/vhdl/PayloadChecker.vhd

vcom -93 ../../SimFolder/vhdl/cntCmdInDriven.vhd
vcom -93 ../../SimFolder/vhdl/cntFreeWheel.vhd
vcom -93 ../../SimFolder/vhdl/phaseShiftDriver.vhd

vcom -93 $path/SerialComFIFO_bbpd.vhd 
vcom -93 $path/SerialComFIFO_bbpd_CV.vhd 
vcom -93 $path/ACU_SerialComFIFO_Wrap.vhd 


vcom -93 $path/enc_8b10b.vhd
vcom -93 $path/ACU_8b10b_encoderWrap.vhd
vcom -93 $path/dec_8b10b.vhd
vcom -93 $path/ACU_8b10b_decoderWrap.vhd

vcom -93 $path/ACU_Serialiser.vhd
vcom -93 $path/ACU_NRZI_decoder.vhd
vcom -93 $path/ACU_NRZI_encoder.vhd
vcom -93 $path/ACU_Deserialiser.vhd
vcom -93 $path/ACU_SerialCommParityChecker.vhd

vcom -93 $path/ACU_WordAlign.vhd

vcom -93 $path/ACU_bbpd_4q.vhd
vcom -93 $path/ACU_BB_PhaseDetector_4q.vhd


vcom -93 $path/ACU_PLL_SeialCom.vhd

vcom -93 $path/ACU_SerialCom_bbpd.vhd


vcom -93 $path/Tb_ACU_SerialComPhaseDetector.vhd




