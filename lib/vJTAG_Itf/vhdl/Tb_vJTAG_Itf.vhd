-------------------------------------------------------------------------------------------------------------------------------------------------------------
-- File name: Tb_vJTAG_Itf.vhd                                                                                                                               --
--                                                                                                                                                         --
-- Author   : D.Rodomonti                                                                                                                                  --
-- Date     : 30/12/2016                                                                                                                                   --
--                                                                                                                                                         --
-- Comments : vJTAG_Itf.vhd  test bench file
--                                                                                                                                                         --
-- History  : Start up version 30/12/2016                                                                                                                 --
-------------------------------------------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

USE work.ACU_package.all;


Entity Tb_vJTAG_Itf is
End entity Tb_vJTAG_Itf;

Architecture beh of Tb_vJTAG_Itf is

--
-- Signal declaration
		      
signal tck	      : std_logic:='0';       -- 10 MHz clock signal	     
signal Reset          : std_logic;
signal tdi            : std_logic;
signal ir_in          : std_logic_vector(8 downto 0);
signal v_cdr          : std_logic;
signal v_sdr          : std_logic;
signal v_udr          : std_logic;
signal DataMeas       : std_logic_vector(15 downto 0);   

signal dtReadyEEPROM  : std_logic;
signal dtEEPROM       : std_logic_vector(2047 downto 0);
signal EEPROM_emul    : array16bits(3 downto 0);
signal intEEPROM_emul : std_logic_vector(15 downto 0);

--
--Component Declaration
component vJTAG_Itf is
generic  
   (
      gvJTAG_ItfVersion      : real := 1.1
   );
  Port (
      tck                   : in std_logic;                         -- JTAG Clock signal max 10 MHz
      Reset                 : in std_logic;                         -- Asynchronous reset signal active high
      tdi                   : in std_logic;                         -- JTAG data in.
      ir_in                 : in std_logic_vector(8 downto 0);      -- JTAG Instruction Register.
                                                                    -- (8) => Bypass bit (active high).
                                                                    -- (7) => RD = 0 ; WR = 1 bit
                                                                    -- (6 downto 0) 64 registers address
      v_cdr                 : in std_logic;                         -- Virtual JTAG state Capture Data Register. It is used to load the data to send back via tdo.
      v_sdr                 : in std_logic;                         -- Virtual JTAG state Shift Data Register. It is used to enable the internal shift register to 
	                                                            -- hold tdi values.
      v_udr                 : in std_logic;                         -- Virtual JTAG state Update Data Register. It is used to swap the tdi collected data to a parallel register.

      tdo                   : out std_logic;                        -- JTAG data out.
      
      DataMeas              : in std_logic_vector(15 downto 0);     -- Data from FIFO to PC
      rdReq                 : out std_logic;                        -- FIFO read request (only the rising edge has to be used!!!)
      StartMeas             : out std_logic;                        -- 1 tck pulse signal active high used to start the FIFO filling
      FlagCommands          : out std_logic_vector(11 downto 0);    -- Flag commands active high
      
      -- EEPROM
      wrEEPROM              : out std_logic;                        -- EEPROM write command
      dt2WrEEPROM           : out std_logic_vector(2047 downto 0);  -- EEPROM data to write
      rdEEPROM              : out std_logic;                        -- EEPROM read command
      dtReadyEEPROM         : in  std_logic;                        -- Data read from EEPROM ready: it is high from the rd action conclusion till the next rd action performed.
                                                                    -- in this way we can sample the signal easily in the tck clock domain
      dtEEPROM              : in  std_logic_vector(2047 downto 0);  -- Data read from EEPROM. They are stable between a rd action and the other one.
      
      LEDs                  : out std_logic_vector(7 downto 0)      -- LEDs driving signals. DEBUG PURPOSE ONLY!!!
  );
end component;

component vJTAG_Itf_EEPromBased is
generic  
   (
      gvJTAG_Itf_EEPromBased      : real := 1.2
   );
  Port (
    tck            : in std_logic;                         -- JTAG Clock signal max 10 MHz
    Reset          : in std_logic;                         -- Asynchronous reset signal active high
    tdi            : in std_logic;                         -- JTAG data in.
    ir_in          : in std_logic_vector(8 downto 0);      -- JTAG Instruction Register.
                                                           -- (8) => Bypass bit (active high).
                                                           -- (7) => RD = 0 ; WR = 1 bit
                                                           -- (6 downto 0) 128 registers address
    v_cdr          : in std_logic;                         -- Virtual JTAG state Capture Data Register. It is used to load the data to send back via tdo.
    v_sdr          : in std_logic;                         -- Virtual JTAG state Shift Data Register. It is used to enable the internal shift register to 
                                                           -- hold tdi values.
    v_udr          : in std_logic;                         -- Virtual JTAG state Update Data Register. 
                                                           -- It is used to swap the tdi collected data to a parallel register.

    tdo            : out std_logic;                        -- JTAG data out.

    FlagCommands   : out std_logic_vector(11 downto 0);    -- Flag commands active high

    -- EEPROM
    wrEEPROM       : out std_logic;                        -- EEPROM write command
    dt2WrEEPROM    : out std_logic_vector(15 downto 0);    -- EEPROM data to write
    addrEEPROM     : out std_logic_vector(6 downto 0);     -- EEPROM location to RD/WR
    
    dtEEPROM       : in  std_logic_vector(15 downto 0)     -- Data read from EEPROM. They are stable between a rd action and the other one.
  	  
  );
End component vJTAG_Itf_EEPromBased;

begin

tck            <= not tck after 50 ns;
Reset          <= '1';
tdi            <= '0';
ir_in          <= "000000000";
v_cdr          <= '0';
v_sdr          <= '0';
v_udr          <= '0';
DataMeas       <= x"F1CA";

dtReadyEEPROM  <= '0';
dtEEPROM       <= (others=>'1');

EEPROM_emul(0) <= x"CAFE";
EEPROM_emul(1) <= x"4658";
EEPROM_emul(2) <= x"6529";
EEPROM_emul(3) <= x"F1CA";

i_vJTAG_Itf: vJTAG_Itf
  Port map(
         tck               => tck,
         Reset             => Reset,
         tdi               => tdi,
	 ir_in             => ir_in,
	 v_cdr             => v_cdr,
	 v_sdr             => v_sdr,
	
	 v_udr             => v_udr,

         tdo               => open,
			
	 DataMeas          => DataMeas,
	 rdReq             => open,
         StartMeas         => open,
	 FlagCommands      => open,
         
	 wrEEPROM          => open,
         dt2WrEEPROM       => open,
         rdEEPROM          => open,
         dtReadyEEPROM     => dtReadyEEPROM,
         dtEEPROM          => dtEEPROM,
      
	 
         LEDs              => open
  );

intEEPROM_emul  <= EEPROM_emul(to_integer(unsigned(ir_in(6 downto 0))));

i_vJTAG_Itf_EEPromBased: vJTAG_Itf_EEPromBased 

  Port map(
    tck            	   => tck,
    Reset          	   => Reset,
    tdi            	   => tdi,
    ir_in          	   => ir_in,
                   
    v_cdr          	   => v_cdr,
    v_sdr          	   => v_sdr,
                   
    v_udr          	   => v_udr,
                   

    tdo                    => open,

    FlagCommands           => open,

    -- EEPROM
    wrEEPROM               => open,
    dt2WrEEPROM            => open,
    addrEEPROM             => open,
    
    dtEEPROM               => intEEPROM_emul
  	  
  );
  
end beh;
