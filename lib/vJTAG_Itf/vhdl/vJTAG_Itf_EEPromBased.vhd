-------------------------------------------------------------------------------------------------------------------------------------------------------------
-- File name: vJTAG_Itf_EEPromBased.vhd                                                                                                                    --
--                                                                                                                                                         --
-- Author   : D.Rodomonti                                                                                                                                  --
-- Date     : 05/11/2018                                                                                                                                   --
--                                                                                                                                                         --
-- Comments : In order to reduce the ammount of logic resources used by this component, an external EEPROM is used to hold the parameters necessary in the --
--            project. It is possible to consider this module as an EEPROM driver interface where rd/wr commands to a specific EEPROM address are          --
--            generated. Without the EEPROM driver this component is almost useless. If you don't want to use the external EEPROM, please implement the    --
--            vJTAG_itf.vhd file to handle the project parameters.                                                                                         --
--                                                                                                                                                         --
-- History  : Start up version 05/11/2018                                                                                                                  --
--            v 1.3 Added measure signals back 16/11/2018                                                                                                  --
--            v 1.4 Improved read data measured speed: the RAM is written by the RAM_Interface module as real RAM and it is read by this module as a FIFO. --
--                  06/12/2018                                                                                                                             --
-------------------------------------------------------------------------------------------------------------------------------------------------------------
--       __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    _ --
-- tck _|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  --
--                                                                                                                                                         --
-- ir_in      X       Addr2WR                                                                                                                              --
--                  _________________________________________________                                                                                      --
-- vs_sdr__________|                                                |_____________________________________________________________________________________ --
--                                                                                                                                                         --
--s_reg                   X     X     X     X     X     X     X     X ABCD                                                                                 --
--                                                                         ______                                                                          --
--vs_udr _________________________________________________________________|     |_________________________________________________________________________ --
--                                                                                                                                                         --
--dt2wr                                                                         X ABCD                                                                     --
--                                                                               ______                                                                    --
--wrEEPROM _____________________________________________________________________|     |___________________________________________________________________ --                                                                      


-- ir_in      X       Addr2RD                                                                                                                              --
--                                                                                                                                                         --
-- u_reg            X 0123                                                                                                                                 --
--                               ______                                                                                                                    --
--vs_cdr _______________________|     |___________________________________________________________________________________________________________________ --
--                                     _________________________________________________                                                                   --
-- vs_sdr_____________________________|                                                |__________________________________________________________________ --
--                                                                                                                                                         --
--s_reg                                0123 X     X     X     X     X     X     X     X                                                                    --
-------------------------------------------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

USE work.ACU_package.all;


Entity vJTAG_Itf_EEPromBased is
generic  
   (
      gvJTAG_Itf_EEPromBased      : real := 1.4
   );
  Port (
		tck            : in std_logic;                     -- JTAG Clock signal max 10 MHz
		Reset          : in std_logic;                     -- Asynchronous reset signal active high
		tdi            : in std_logic;                     -- JTAG data in.
		ir_in          : in std_logic_vector(8 downto 0);  -- JTAG Instruction Register.
                                                         -- (8) => Bypass bit (active high).
                                                         -- (7) => RD = 0 ; WR = 1 bit
                                                         -- (6 downto 0) 128 registers address
		v_cdr          : in std_logic;                     -- Virtual JTAG state Capture Data Register. It is used to load the data to send back via tdo.
		v_sdr          : in std_logic;                     -- Virtual JTAG state Shift Data Register. It is used to enable the internal shift register to 
                                                         -- hold tdi values.
		v_udr          : in std_logic;                     -- Virtual JTAG state Update Data Register. 
                                                         -- It is used to swap the tdi collected data to a parallel register.

		tdo            : out std_logic;                    -- JTAG data out.
    
   -- Measure signals
		DataMeas       : in std_logic_vector(15 downto 0); -- Data from FIFO to PC
		rdRAM          : out std_logic;                    -- 1 tck pulse signal active high used as RAM rd enable
		addrRAM        : out std_logic_vector(12 downto 0);-- 8192w16b RAM address


		FlagCommands   : out std_logic_vector(11 downto 0);-- Flag commands active high

   -- EEPROM
		wrEEPROM       : out std_logic;                    -- EEPROM write command
		dt2WrEEPROM    : out std_logic_vector(15 downto 0);-- EEPROM data to write
		addrEEPROM     : out std_logic_vector(6 downto 0); -- EEPROM location to RD/WR
    
		dtEEPROM       : in  std_logic_vector(15 downto 0) -- Data read from EEPROM. They are stable between a rd action and the other one.
  	  
   );
End entity vJTAG_Itf_EEPromBased;

Architecture beh of vJTAG_Itf_EEPromBased is
--
-- Constant declaration
-- constant Tc_addr        : unsigned(12 downto 0):=(others=>'1');  -- 8192 locations

--
-- Signals declaration
signal s_dreg           : std_logic_vector(15 downto 0);    -- Shift Data Register
signal u_dreg           : array16bits(2 downto 0);          -- Updated Data Register:
                                                            --   (0) => Flag & Swap commands
                                                            --   (1) => DataMeas ''' TO DO!! '''
                                                            --   (2) => Data to rd/wr
signal tdx_dreg         : std_logic;                        -- Bypass register.

signal u_dreg_0_14_s0   : std_logic;                        -- u_dreg(0)(14) sampled signal
signal u_dreg_0_14_s1   : std_logic;                        -- u_dreg_0_14_s0 sampled signal

signal intAddrRAM       : unsigned(12 downto 0);            -- Internal read RAM address

signal S_DataMeas       : std_logic_vector(15 downto 0);    -- DataMeas sampled signal


begin

--
-- dr process
p_dr:process(Reset,tck)
begin
	if (Reset='1') then
		s_dreg          <= (others=>'0');
		u_dreg          <= (others=>(others=>'0'));
		tdx_dreg        <= '0';

		S_DataMeas      <= (others=>'0');
 
		wrEEPROM        <= '0';
		addrEEPROM      <= (others=>'0');
    
		rdRAM           <= '0';
		intAddrRAM      <= (others=>'0');

		u_dreg_0_14_s0  <= '0';
		u_dreg_0_14_s1  <= '0';

	elsif(tck'event and tck='1') then
		tdx_dreg    <= tdi;                                            -- if the ir_in is different from the expected ones or it is equal to 
                                                                     -- 0x100 (== Bypass) this register allows the JTAG data continuity.								      
		wrEEPROM    <= '0';                                            -- EEPROM write command
		S_DataMeas  <= DataMeas;                                       -- data from RAM sample
		rdRAM       <= '0';                                            -- RAM rd command default value
    
		-- reset the RAM read adrress every time a data acquisition process starts   
		u_dreg_0_14_s0  <= u_dreg(0)(14);
		u_dreg_0_14_s1  <= u_dreg_0_14_s0;
   if (u_dreg_0_14_s0 ='0' and u_dreg_0_14_s1 ='1') then
      rdRAM       <= '1';                                            -- to read address 0
      intAddrRAM  <= (others=>'0');                                  -- Address RAM reset
   end if;
    
   if (ir_in(8)='0') then                                            -- no bypass
  
      addrEEPROM       <= ir_in(6 downto 0);                         -- EEPROM read/write address.

      if (ir_in(7)='0') then                                         -- Read instruction
          
			if (v_cdr='1') then					      							-- Load the EEPROM value selected by ir_in
				if (ir_in(6 downto 0) = "0000001") then                  -- Address 1 selected 
					s_dreg      <= S_DataMeas;                            -- Data Meas loaded
					rdRAM       <= '1';                                   -- read RAM command
					intAddrRAM  <= intAddrRAM + 1;                        -- The RAM is read as a FIFO: all the value from address 0 to address 8191 are shifted
                                                                     -- out during the read action.
               else
						s_dreg <= dtEEPROM;                                -- Load the EEPROM value selected by ir_in
				end if;
			elsif (v_sdr='1') then				              					-- shift out the s_dreg content
				s_dreg <= s_dreg(0) & s_dreg(15 downto 1);
			end if;
	  
      elsif (ir_in(7)='1') then                                      -- Write instruction
          
			if (v_sdr='1') then                                         -- Store the tdi into a shift register
				s_dreg(15)	      	<= tdi;
				s_dreg(14 downto 0) 	<= s_dreg(15 downto 1);
			elsif (v_udr='1') then                                      -- swap the shift register to a parallel register (u_dreg)
				if to_integer(unsigned(ir_in(6 downto 0))) > 1 then
					u_dreg(2)       	<= s_dreg;
					wrEEPROM        	<= '1';
			else
            u_dreg(to_integer(unsigned(ir_in(6 downto 0)))) <= s_dreg;  
			end if;
      end if;
	  
   end if; 
  end if;
 end if;
end process;

-- tdo process
p_tdo:process(ir_in,tdx_dreg,s_dreg)
begin
	if (ir_in(8)='0') then                                              -- no bypass
		tdo  <= s_dreg(0);
	else                                                                --  bypass
		tdo  <= tdx_dreg;
	end if;
end process;

--
-- outputs
AddrRAM  <= std_logic_vector(intAddrRAM);

-- They can be used as enable signals for debug purposes
FlagCommands <= u_dreg(0)(11 downto 0);

dt2WrEEPROM  <= u_dreg(2);



end beh;
