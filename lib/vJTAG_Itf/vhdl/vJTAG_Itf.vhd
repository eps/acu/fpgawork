-------------------------------------------------------------------------------------------------------------------------------------------------------------
-- File name: vJTAG_Itf.vhd                                                                                                                               --
--                                                                                                                                                         --
-- Author   : D.Rodomonti                                                                                                                                  --
-- Date     : 16/03/2016                                                                                                                                   --
--                                                                                                                                                         --
-- Comments : ---TO ADD!!!!!!!!!!!!!!
--                                                                                                                                                         --
-- History  : Start up version 16/03/2016                                                                                                                 --
-------------------------------------------------------------------------------------------------------------------------------------------------------------
--   TO CHANGE
--        __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __    __     --
-- clk _|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  --
--             ______                                                                                                                                      --
-- startCmd___|     |____________________________________________________________________________________________________________________________________  --
--                                                  _______________________                         _______________________                                --
-- SCLK                         __________________|                       |__________ ..__________|                       |______________________________  --
--             __________________                                                                                                      __________________  --
-- nCS                          |____________________________________________________ ..______________________________________________|                    --
--                                                                                                                                                         --
-- MOSI   ---------------------X                  0                       X     1     ..         15                       X------------------------------  --                     
--                                                                                                                                                         --
-- MISO   ---------------------X                  0                       X     1     ..         15                       X------------------------------  --
--                                                                                                                                      _____              --
-- NewVR  ____________________________________________________________________________________________________________________________|     |____________  --
--                                                                                                                                                         --
-- VR     ----------------------------------------------------------------------------------------------------------------------------X        NewData     --
-------------------------------------------------------------------------------------------------------------------------------------------------------------
-- DR 01/10/2018: added read and write action to EEPROM
-------------------------------------------------------------------------------------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

USE work.ACU_package.all;


Entity vJTAG_Itf is
generic  
   (
      gvJTAG_ItfVersion      : real := 1.2
   );
  Port (
    tck            : in std_logic;                         -- JTAG Clock signal max 10 MHz
    Reset          : in std_logic;                         -- Asynchronous reset signal active high
    tdi            : in std_logic;                         -- JTAG data in.
    ir_in          : in std_logic_vector(8 downto 0);      -- JTAG Instruction Register.
                                                           -- (8) => Bypass bit (active high).
                                                           -- (7) => RD = 0 ; WR = 1 bit
                                                           -- (6 downto 0) 128 registers address
    v_cdr          : in std_logic;                         -- Virtual JTAG state Capture Data Register. It is used to load the data to send back via tdo.
    v_sdr          : in std_logic;                         -- Virtual JTAG state Shift Data Register. It is used to enable the internal shift register to 
                                                           -- hold tdi values.
    v_udr          : in std_logic;                         -- Virtual JTAG state Update Data Register. 
                                                           -- It is used to swap the tdi collected data to a parallel register.

    tdo            : out std_logic;                        -- JTAG data out.

    DataMeas       : in std_logic_vector(15 downto 0);     -- Data from FIFO to PC
    rdReq          : out std_logic;                        -- FIFO read request (only the rising edge has to be used!!!)
    StartMeas      : out std_logic;                        -- 1 tck pulse signal active high used to start the FIFO filling
    FlagCommands   : out std_logic_vector(11 downto 0);    -- Flag commands active high

    -- EEPROM
    wrEEPROM       : out std_logic;                        -- EEPROM write command
    dt2WrEEPROM    : out std_logic_vector(2047 downto 0);  -- EEPROM data to write
    rdEEPROM       : out std_logic;                        -- EEPROM read command
    dtReadyEEPROM  : in  std_logic;                        -- Data read from EEPROM ready: it is high from the rd action conclusion till the next rd 
                                                           -- action performed. in this way we can sample the signal easily in the tck clock domain
    dtEEPROM       : in  std_logic_vector(2047 downto 0);  -- Data read from EEPROM. They are stable between a rd action and the other one.
  	  
    --Param1_8b      : out std_logic_vector(7 downto 0);
    --Param0_8b      : out std_logic_vector(7 downto 0);
    --Param129_8b    : out std_logic_vector(7 downto 0);
    --Param128_8b    : out std_logic_vector(7 downto 0);
    --Param255_8b    : out std_logic_vector(7 downto 0);
    --Param254_8b    : out std_logic_vector(7 downto 0);
  	 Addr2WrRd      : out std_logic_vector(23 downto 0);
    Dt2Wr          : out std_logic_vector(15 downto 0);
	 
    LEDs           : out std_logic_vector(7 downto 0)      -- LEDs driving signals. DEBUG PURPOSE ONLY!!!
  );
End entity vJTAG_Itf;

Architecture beh of vJTAG_Itf is
--
-- Constant declaration
--constant Tc_watchdog   : unsigned(31 downto 0):= (others=>'1');          -- 430 sec with tck @ 10 MHz

--
-- Signals declaration
signal s_dreg           : array16bits(127 downto 0);        -- Shift Data Register
signal u_dreg           : array16bits(127 downto 0);        -- Updated Data Register
signal tdx_dreg         : std_logic;                        -- Bypass register.

signal swap_dreg        : array16bits(127 downto 0);        -- Swap register: it gets the content of u_dreg when the rising edge of the swap 
                                                            -- bit(u_dreg(0)(15)) is detected.
signal u_dreg_0_15_s0   : std_logic;                        -- u_dreg(0)(15) sampled signal
signal u_dreg_0_15_s1   : std_logic;                        -- u_dreg_0_15_s0 sampled signal
signal swapCmd          : std_logic;                        -- u_dreg(0)(15) rising edge

signal u_dreg_0_14_s0   : std_logic;                        -- u_dreg(0)(14) sampled signal
signal u_dreg_0_14_s1   : std_logic;                        -- u_dreg_0_14_s0 sampled signal

signal doubleRD         : std_logic;                        -- This signal is used to monitor the rd actions to reg nr 1
signal S_DataMeas       : std_logic_vector(15 downto 0);    -- DataMeas sampled signal

signal u_dreg_0_13_s0   : std_logic;                        -- u_dreg(0)(13) sampled signal
signal u_dreg_0_13_s1   : std_logic;                        -- u_dreg_0_13_s0 sampled signal

signal u_dreg_0_12_s0   : std_logic;                        -- u_dreg(0)(12) sampled signal
signal u_dreg_0_12_s1   : std_logic;                        -- u_dreg_0_12_s0 sampled signal

signal IntRdEEPROM      : std_logic;                        -- Internal read EEPROM command
signal rdEEPROM_Status  : std_logic;                        -- it is 1 during the EEPROM read action

--signal watchdog        : unsigned (31 downto 0);                         -- It monitors the EEPROM driver activity
signal rdEEPROM_Done    : std_logic;                        -- It is one when the watchdog counter expires.
signal dtReadyEEPROM_s0 : std_logic;                        -- dtReadyEEPROM sampled signal
signal dtReadyEEPROM_s1 : std_logic;                        -- dtReadyEEPROM_s0 sampled signal
signal dtReadyEEPROM_RE : std_logic;                        -- dtReadyEEPROM rising edge

begin

--
-- dtReadyEEPROM_RE
p_dtReadyEEPROM_RE: process(Reset,tck)
begin
  if (Reset='1') then
    dtReadyEEPROM_s0  <= '0';
    dtReadyEEPROM_s1  <= '0';
    dtReadyEEPROM_RE  <= '0';
  elsif(tck'event and tck='1') then
    dtReadyEEPROM_s0  <= dtReadyEEPROM;
    dtReadyEEPROM_s1  <= dtReadyEEPROM_s0;
    dtReadyEEPROM_RE  <= dtReadyEEPROM_s0 and not dtReadyEEPROM_s1;
  end if;
end process;
--
-- dr process
p_dr:process(Reset,tck)
begin
  if (Reset='1') then
    s_dreg           <= (others=>(others=>'0'));
    u_dreg           <= (others=>(others=>'0'));
    rdReq            <= '0';
    S_DataMeas       <= (others=>'0');
    doubleRD         <= '0';
    rdEEPROM_Status  <= '0';
    rdEEPROM_Done    <= '0';
  elsif(tck'event and tck='1') then
    tdx_dreg  <= tdi;                                                 -- if the ir_in is different from the expected ones or it is equal to 
                                                                      -- 0x100 (== Bypass) this register allows the JTAG data continuity.								      
    rdReq       <= '0';                                               -- default value
    S_DataMeas  <= DataMeas;
    
    --if (IntRdEEPROM ='1') then                                        -- EEPROM read action started
    if (dtReadyEEPROM_RE ='1') then                                   -- EEPROM read action started
      rdEEPROM_Status  <= '1';
    end if;
    rdEEPROM_Done    <= '0';
	 
    if (rdEEPROM_Status = '0') then 
    
      if (ir_in(8)='0') then                                          -- no bypass
        if (ir_in(7)='0') then                                        -- Read instruction
          
	  if (ir_in(6 downto 0) = "0000001") then                     -- Address 1 selected   
	    if (v_cdr='1') then                                       -- Load the sampled meas value in order to send the content back via tdo
	      if (doubleRD='0') then                                  -- first rd action: rd request to fifo generated
                rdReq     <= '1';
	      else                                                    -- second rd action: Data Meas loaded
	        s_dreg(to_integer(unsigned(ir_in(6 downto 0)))) <= S_DataMeas;
	      end if;
	      doubleRD  <= not doubleRD;                              -- toggle doubleRD signal
            elsif (v_sdr='1') then                                    -- shift out the s_dreg content
              s_dreg(to_integer(unsigned(ir_in(6 downto 0)))) <= s_dreg(to_integer(unsigned(ir_in(6 downto 0)))) (0) & 
                                                                 s_dreg(to_integer(unsigned(ir_in(6 downto 0)))) (15 downto 1);
            end if;
	  else                                                        -- Other adtresses selected
            if (v_cdr='1') then                                       -- Load the current value of u_dreg to s_dreg in order to send the content back via tdo
              s_dreg(to_integer(unsigned(ir_in(6 downto 0)))) <= u_dreg(to_integer(unsigned(ir_in(6 downto 0)))); 
            elsif (v_sdr='1') then                                    -- shift out the s_dreg content
              s_dreg(to_integer(unsigned(ir_in(6 downto 0)))) <= s_dreg(to_integer(unsigned(ir_in(6 downto 0)))) (0) & 
                                                                 s_dreg(to_integer(unsigned(ir_in(6 downto 0)))) (15 downto 1);
            end if;
	  end if;
	  
        elsif (ir_in(7)='1') then                                     -- Write instruction
          
	  if (v_sdr='1') then                                         -- Store the tdi into a shift register
            s_dreg(to_integer(unsigned(ir_in(6 downto 0))))(15)          <= tdi;
  	    s_dreg(to_integer(unsigned(ir_in(6 downto 0))))(14 downto 0) <= s_dreg(to_integer(unsigned(ir_in(6 downto 0))))(15 downto 1);
          elsif (v_udr='1') then                                      -- swap the shift register to a parallel register (u_dreg)
            u_dreg(to_integer(unsigned(ir_in(6 downto 0)))) <= s_dreg(to_integer(unsigned(ir_in(6 downto 0))));
          end if;
	  
        end if; 
      end if;
      
    else   -- EEPROM rd action started.
     
      for i in 2 to 127 loop 
        u_dreg(i)           <= dtEEPROM(15+i*16 downto i*16);
      end loop;
      u_dreg(0)           <= (others=>'0');
      u_dreg(1)           <= (others=>'0');
	  
      rdEEPROM_Status  <= '0';       -- EEPROM rd action ended.
      rdEEPROM_Done    <= '1';
    
    end if;

  end if;
end process;

--
-- tdo process
p_tdo:process(ir_in,tdx_dreg,s_dreg)
begin
  if (ir_in(8)='0') then                                              -- no bypass
    tdo  <= s_dreg(to_integer(unsigned(ir_in(6 downto 0))))(0);
  else                                                                --  bypass
    tdo  <= tdx_dreg;
  end if;
end process;


p_swap:process(Reset,tck)
begin
  if (Reset='1') then
    swap_dreg       <= (others=>(others=>'0'));
    u_dreg_0_15_s0  <= '0';
    u_dreg_0_15_s1  <= '0';
    swapCmd         <= '0';
  elsif(tck'event and tck='1') then
    u_dreg_0_15_s0  <= u_dreg(0)(15);
    u_dreg_0_15_s1  <= u_dreg_0_15_s0;
    swapCmd         <= u_dreg_0_15_s0 and not u_dreg_0_15_s1;
    if (swapCmd='1' or rdEEPROM_Done='1') then
      swap_dreg     <= u_dreg;
    end if;
  end if;
end process;

--
-- Start meas pulse
p_stMeas:process(Reset,tck)
begin
  if (Reset='1') then
    StartMeas       <= '0';
    u_dreg_0_14_s0  <= '0';
    u_dreg_0_14_s1  <= '0';
  elsif(tck'event and tck='1') then
    u_dreg_0_14_s0  <= u_dreg(0)(14);
    u_dreg_0_14_s1  <= u_dreg_0_14_s0;
    StartMeas       <= u_dreg_0_14_s0 and not u_dreg_0_14_s1;
  end if;
end process;

--
-- Write EEPROM
p_wrEEPROM:process(Reset,tck)
begin
  if (Reset='1') then
    wrEEPROM        <= '0';
    u_dreg_0_13_s0  <= '0';
    u_dreg_0_13_s1  <= '0';
  elsif(tck'event and tck='1') then
    u_dreg_0_13_s0  <= u_dreg(0)(13);
    u_dreg_0_13_s1  <= u_dreg_0_13_s0;
    wrEEPROM        <= u_dreg_0_13_s0 and not u_dreg_0_13_s1;
  end if;
end process;

--
-- Read EEPROM
p_rdEEPROM:process(Reset,tck)
begin
  if (Reset='1') then
    IntRdEEPROM      <= '0';
    u_dreg_0_12_s0   <= '0';
    u_dreg_0_12_s1   <= '0';
  elsif(tck'event and tck='1') then
    u_dreg_0_12_s0   <= u_dreg(0)(12);
    u_dreg_0_12_s1   <= u_dreg_0_12_s0;
    IntRdEEPROM      <= u_dreg_0_12_s0 and not u_dreg_0_12_s1;
  end if;
end process;

--
-- Only for debug

--LEDs         <= u_dreg(0)(13 downto 10) & u_dreg(0)(3 downto 0);
LEDs          <= swap_dreg(0)(13 downto 10) & swap_dreg(0)(3 downto 0);


--Param1_8b    <= swap_dreg(2)(15 downto 8);
--Param0_8b    <= swap_dreg(2)(7 downto 0);
--Param129_8b  <= swap_dreg(64)(15 downto 8);
--Param128_8b  <= swap_dreg(64)(7 downto 0);
--Param255_8b  <= swap_dreg(127)(15 downto 8);
--Param254_8b  <= swap_dreg(127)(7 downto 0);

Addr2WrRd  <= swap_dreg(3)(7 downto 0) & swap_dreg(2);
Dt2Wr      <= swap_dreg(4)(7 downto 0) & swap_dreg(3)(15 downto 8);

-- They can be used as enable signals for debug purposes
FlagCommands <= u_dreg(0)(11 downto 0);
--FlagCommands <= swap_dreg(0)(11 downto 0);
rdEEPROM     <= IntRdEEPROM;

-- array to parallel conversion 
p_arr2parallel:process(swap_dreg)
begin

  for i in 2 to 127 loop 
    dt2WrEEPROM(15+i*16 downto i*16)  <= swap_dreg(i);
  end loop;

  dt2WrEEPROM(31 downto 0)  <= (others=>'0');

end process;

end beh;
