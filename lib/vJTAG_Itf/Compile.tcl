###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work
pwd
vcom -93 ../../ACU_package.vhd

vcom -93 $path/vJTAG_Itf.vhd
vcom -93 $path/vJTAG_Itf_EEPromBased.vhd

vcom -93 $path/Tb_vJTAG_Itf.vhd
