restart -f
run 1 us

force -freeze sim:/tb_vjtag_itf/Reset 0 0
run 200 ns

# WR ADDR 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(7) 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 1 0
run 500 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/tdi 1 0
run 500 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/tdi 0 0
run 700 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 0 0
run 100 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_udr 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_udr 0 0
run 1 us

# WR ADDR 1
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(0) 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/tdi 1 0
run 800 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/tdi 0 0
run 700 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 0 0
run 100 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_udr 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_udr 0 0
run 1 us

# WR ADDR 2
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(0) 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(1) 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 1 0
run 800 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/tdi 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/tdi 0 0
run 700 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 0 0
run 100 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_udr 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_udr 0 0
run 1 us

# WR ADDR 8
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(0) 1 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(1) 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 1 0
run 500 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/tdi 1 0
run 500 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/tdi 0 0
run 700 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 0 0
run 100 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_udr 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_udr 0 0
run 1 us

#RD ADDR 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(7) 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(0) 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(1) 0 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_cdr 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_cdr 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 1 0
run 1700 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 0 0
run 1 us

#RD ADDR 1
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(7) 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(0) 1 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(1) 0 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_cdr 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_cdr 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 1 0
run 1700 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 0 0
run 1 us

#RD ADDR 2
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(7) 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(0) 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(1) 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_cdr 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_cdr 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 1 0
run 1700 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 0 0
run 1 us

#RD ADDR 3
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(7) 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(0) 1 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/ir_in(1) 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_cdr 1 0
run 200 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_cdr 0 0
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 1 0
run 1700 ns
force -freeze sim:/tb_vjtag_itf/i_vJTAG_Itf_EEPromBased/v_sdr 0 0
run 1 us
