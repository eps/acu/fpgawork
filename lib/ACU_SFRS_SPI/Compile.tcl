###########################################
# Compile.tcl file                        #
# It contains all design file to compile. #
###########################################

path="../vhdl"

vlib work
vmap work work

#vmap altera_mf work
#vmap cyclonev work




pwd
vcom -93 ../../../../SVN/Quartus_111/lib/SimFolder/vhdl/cntCmdInDriven.vhd
vcom -93 ../../../../SVN/Quartus_111/lib/SimFolder/vhdl/cntFreeWheel.vhd
  
vcom -93 ../../SFRS_FW/vhdl/SPI_empfaenger.vhd 
vcom -93 ../../SFRS_FW/vhdl/SPI_sender.vhd 

vcom -93 $path/Tb_ACU_SFRS_SPI.vhd
