-----------------------------------------------------------------------------------
-- File name: Tb_ACU_SFRS_SPI.vhd                                                --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 08/01/2024                                                         --
--                                                                               --
-- Comments : SPI_sender and SPI_empfaenger test bench                           --           
--                                                                               --
-- History  : Start up version 08/01/2024                                        --
-----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


Entity Tb_ACU_SFRS_SPI is
End entity Tb_ACU_SFRS_SPI;

Architecture beh of Tb_ACU_SFRS_SPI is
--
-- Constants declaration
							   
--
-- Signals declaration
signal Clock             : std_logic:='0';
signal Reset             : std_logic:='1';
signal SPI_CLK           : std_logic;
signal SPI_SDA           : std_logic;
signal DATA_TO_Transfer  : std_logic_vector(55 downto 0);
signal IncrCnt           : std_logic;
signal New_cntOut        : std_logic;
signal cntOut            : std_logic_vector(23 downto 0);

--
-- Component
Component SPI_sender IS    
                                                     -- Entity Declaration
    GENERIC (        
      takt             : integer  := 100_000_000;  -- 100 MHz
      SPI_takt         : integer  := 4;            -- Anzahl der clocks fuer ein Zustand (high or low) von dem SPI ckl
      SPI_bit_count    : integer  := 56 );         -- Anzahl der bits, die uebertragen werden         
 
   PORT (
      clk, reset       : IN  std_logic ;                                                    -- Control Signals    
      DATA_TO_Transfer : IN  std_logic_vector (SPI_bit_count-1 downto 0):= (others=>'0');   -- Daten zum senden
      SPI_CLK_OUT      : OUT std_logic ;
      SPI_DO           : OUT std_logic);                                                    -- Datenausgang
        

end component SPI_sender;

Component SPI_empfaenger IS    
                                                   -- Entity Declaration
    GENERIC (        
      takt             : integer  := 100_000_000;  -- 100 MHz
      SPI_takt         : integer  := 4;            -- Anzahl der clocks fuer ein Zustand (high or low) von dem SPI ckl
      SPI_bit_count    : integer  := 56 );         -- Anzahl der bits, die uebertragen werden        
 

   PORT (
      clk, reset       : IN  std_logic;            -- Control Signals    
      SPI_CLK_IN       : IN  std_logic;    
      SPI_DI           : IN  std_logic;
      SPI_CSn          : OUT std_logic;
      SPI_DI_OK        : OUT std_logic;
      DATA_Output      : OUT std_logic_vector (SPI_bit_count - 1 downto 0));    -- Ausgabe der Empfangenen Daten

end component SPI_empfaenger;

Component cntCmdInDriven is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       incrCmd          : in std_logic;
       enable           : out std_logic;
       cntOutValid      : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end Component cntCmdInDriven;

Component cntFreeWheel is
  generic (
       gCntOutWidth     : integer :=20;
       gEnTrigValue     : integer :=10
    );
  port (
       clock            : in std_logic;
       reset            : in std_logic;
       enable           : out std_logic;
       cntOut           : out std_logic_vector(gCntOutWidth-1 downto 0)
    
    );
end Component cntFreeWheel;

begin

Clock  <= not Clock after 5 ns;


i_6us_cntFreeWheel: cntFreeWheel
  generic map(
       gCntOutWidth     => 16,
       gEnTrigValue     => 1000
    )
  port map (
       clock            => Clock,
       reset            => Reset,
       enable           => IncrCnt,
       cntOut           => open
    
    );

i_cntCmdInDriven: cntCmdInDriven
  generic map(
       gCntOutWidth     => 24,
       gEnTrigValue     => 16777216
    )
  port map(
       clock            => Clock,
       reset            => Reset,
       incrCmd          => IncrCnt,
       enable           => open,
       cntOutValid      => New_cntOut,
       cntOut           => cntOut
    
    );

DATA_TO_Transfer  <=cntOut(7 downto 0) & cntOut & cntOut;



i_SPI_sender: SPI_sender    

    GENERIC map(        

        SPI_takt         => 4,
        SPI_bit_count    => 56
    )
 
    PORT map(
        clk              => Clock,
        reset            => Reset,               
        DATA_TO_Transfer => DATA_TO_Transfer,
        SPI_CLK_OUT      => SPI_CLK,
        SPI_DO           => SPI_SDA
    );

i_SPI_empfaenger: SPI_empfaenger

   GENERIC map (        

      SPI_takt         => 4,
      SPI_bit_count    => 56
   )

   PORT map(
      clk              => Clock,
      reset            => Reset,      
      SPI_CLK_IN       => SPI_CLK,
      SPI_DI           => SPI_SDA,
      SPI_CSn          => open,
      SPI_DI_OK        => open,      
      DATA_Output      => open
    );
     
end beh;
