###########################################################################
#                         FindModuleInstance.tcl                          #
###########################################################################
# This tcl script can be used to check where in the prj directory a vhdl  #
# file is used. It generates a .txt file(FindModuleInstanceList.txt) with #
# the matching project names.                                             #
# It has to be launched from the Quartus tcl console (in Windows          #
# environment) after you opened a project file.                           #
# Follow the steps listed below:                                          #
#    1) launch Quartus II                                                 #
#    2) open a project                                                    #
#    3) open tcl console View -> Utility Windows -> Tcl console           #
#    4) copy and paste the procedure below in the tcl console and press   #
#       return                                                            #
#                                                                         #
#       proc src {file args} {                                            #
#         set argv $::argv                                                #
#         set argc $::argc                                                # 
#         set ::argv $args                                                #
#         set ::argc [llength $args]                                      # 
#         set code [catch {uplevel [list source $file]} return]           #
#         set ::argv $argv                                                #
#         set ::argc $argc                                                #
#         return -code $code $return                                      #
#       }                                                                 #
#                                                                         #
#     Note: the procedure above is necessary because the source command   #
#           only accepts a filename in input (and we have file name and   #
#           parameter)                                                    #
#                                                                         #
#    5) launch in the tcl console the command below                       #
#       src ../../../lib/FindModuleInstance.tcl  <vhdlFileNameToSearch>   #
#       i.e.                                                              # 
#           src ../../../lib/FindModuleInstance.tcl  PLLReconfDriver.vhd  #
#                                                                         #
#    6) Open FindModuleInstanceList.txt for checking the result           #         
###########################################################################


#proc src {file args} {
#set argv $::argv
#set argc $::argc
#set ::argv $args
#set ::argc [llength $args]
#set code [catch {uplevel [list source $file]} return]
#set ::argv $argv
#set ::argc $argc
#return -code $code $return
#}


proc findFiles { basedir pattern } {

    # Fix the directory name, this ensures the directory name is in the
    # native format for the platform and contains a final directory seperator
    set basedir [string trimright [file join [file normalize $basedir] { }]]
    set fileList {}

    # Look in the current directory for matching files, -type {f r}
    # means ony readable normal files are looked at, -nocomplain stops
    # an error being thrown if the returned list is empty
    foreach fileName [glob -nocomplain -type {f r} -path $basedir $pattern] {
        lappend fileList $fileName
    }

    # Now look for any sub direcories in the current directory
    foreach dirName [glob -nocomplain -type {d  r} -path $basedir *] {
        # Recusively call the routine on the sub directory and append any
        # new files to the results
        set subDirList [findFiles $dirName $pattern]
        if { [llength $subDirList] > 0 } {
            foreach subDirFile $subDirList {
                lappend fileList $subDirFile
            }
        }
    }
    return $fileList
 }
 
 
if { $argc != 1 } {
    puts "The FindModuleInstance.tcl script requires one parameter in input."
    puts "For example, tclsh FindModuleInstance.tcl ACU_Hysteresis"
    puts "Please try again."
} else {
    
    set File2Find [lindex $argv 0]
    puts "Looking for....[lindex $argv 0]"
    set dir "../../."
    set qsfFileList [findFiles $dir *.qsf]
    puts "\n"
    puts "qsfFileList= $qsfFileList"
    puts "\n"
    puts "Length qsfFileList= [llength $qsfFileList]"
    puts "\n"
    set i 0
    set found 0
    set wrFileId [open FindModuleInstanceList.txt "w"]
    close $wrFileId
    foreach qsfFile $qsfFileList {
      puts "$i) $qsfFile"
      incr i
      # Open the file generated above just to create the file (later we will fill it) 

      set rdFileId [open $qsfFile "r"]
      # Read the bdf file
      set file_data [read $rdFileId]
      #  Process data file
      set data [split $file_data "\n"]
      #search for string

      foreach line $data {
        set field [split $line "/"]
	     foreach fieldValue $field {
	      if {[string match $File2Find $fieldValue]} {
	        set wrFileId [open FindModuleInstanceList.txt "a"]
	        puts $wrFileId $qsfFile
	        close $wrFileId
	        set found 1
	      }
	    #if {$found == 1} {
	    #  break
	    #}
	  }
	  #if {$found == 1} {
	  #  break
	  #}
      }
      set found 0
    }
}
 
