--==================================================================================================
-- GSI USI VHDL Module
-- -------------------------------------------------------------------------------------------------
-- This module implements the USI interface for the GSI Resonance Sextupole Power Supply Project.
-- Details about the USI interface are documented in the respective FSP documentation.
-- This module is based on the mUSIc_Example VHDL module provided by GSI.
--==================================================================================================

-- Disable Linter rules that check case of signal, generic and port names:
-- vsg_off use_clause_501 generic_007 port_010 component_008 component_012 signal_004 generic_map_002 port_map_002 instantiation_008 instantiation_009

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library work;
    use work.ACU_package.array8bits;

entity gsi_usi is
    generic (
        gmUSIcExample_Version           : string           := "10.05.22";
        gFSPImgStartAddress             : std_logic_vector := X"00_0000";  -- defines the start address within the serial M25P flash device when FSPImg-Function is used
        gModuleNumber                   : integer          := 1;           -- defines the modulenumber on the usi
        gNumberOfFSPs                   : integer          := 71;          -- this defines the total number of fsps
        gMainClockInHz                  : integer          := 100_000_000; -- the main clock frequency
        gTimeoutInSeconds               : integer          := 1;           -- if during an usi receive process the transfer interrupts, after this time the usi resets and waits for a new STX
        gRxFIFODepth                    : integer          := 50;          -- the depth of the usi receiver fifo
        gTxFIFODepth                    : integer          := 4;           -- the depth of the usi transmitter fifo
        gUse_HighSpeedPort_NewDataPulse : integer          := 0;           -- if set: the user must setup a pulse on USI_Merger_Distributor'IN'HighSpeedPort_NewDataPulse to take over new values on USI_Merger_Distributor'IN'HighSpeedPort_In

        gUseGenericModuleNumber        : std_logic := '1'; -- if set: music_shell uses 'gModuleNumber' instead of 'ModuleNumber[3..0]' port
        gUseRAMForRxFIFO               : integer   := 0;   -- if set: compiler builds usi rx fifo with ram insted of LEs
        gUseRAMForTxFIFO               : integer   := 0;   -- if set: compiler builds usi tx fifo with ram insted of LEs
        gShellNoInitSearchRun          : std_logic := '1'; --( 07.09.16) if set: mUSIc_Shell does not perform an init search run to find next free space in serial flash (M25P)
        gAttrib_Bit7_nFac_Applic_Image : std_logic := '0'  -- if set: compiled project has to be understood as an application image, otherwise a factory image (08.03.17 - DS)
    );
    port (
        clock  : in    std_logic;
        reset  : in    std_logic;
        usi_rx : in    std_logic;
        usi_tx : out   std_logic;

        usi_ready : in    std_logic; -- USI Ready Signal - gets active when Serial Number is ready (or after startup timeout)

        highspeed_in  : in    std_logic_vector(31 downto 0);
        highspeed_out : out   std_logic_vector(31 downto 0);

        interlocks_in : in    std_logic_vector(191 downto 0);

        FSP002_Warnings                    : in    std_logic_vector(31 downto 0);
        FSP009_ModuleSerialNumber          : in    std_logic_vector(47 downto 0);
        FSP020_ActualValue_A               : in    std_logic_vector(15 downto 0);  -- 16bit signed value +/- 880A
        FSP030_SetValue_A                  : in    std_logic_vector(23 downto 0);  -- 16bit signed value +/- 880A  (8bit LSB always 0)
        FSP053_ModuleTemperatures          : in    std_logic_vector(63 downto 0);
        FSP060_CurrentPowerStages          : in    std_logic_vector(255 downto 0); -- 16x 16bit signed value +/- 250A
        FSP061_OffsetCorrectionPowerStages : in    std_logic_vector(255 downto 0); -- 16x 16bit signed value +/- 250A
        FSP062_OffsetCorrectionGeneral     : in    std_logic_vector(31 downto 0);  -- 2x 16bit signed value +/- 860A bzw. +/- 900A
        FSP063_VoltageDCLinkLowerHalf      : in    std_logic_vector(15 downto 0);  -- 16bit unsigned value 0 - 450V
        FSP064_WaterValveControl           : out   std_logic_vector(7 downto 0);
        FSP065_ServiceControl              : out   std_logic_vector(15 downto 0);
        FSP066_ActualValue_OutputCurrent   : in    std_logic_vector(15 downto 0);  -- 16bit signed value +/- 860A
        FSP067_ActualValue_OutputVoltage   : in    std_logic_vector(15 downto 0);  -- 16bit signed value +/- 900V
        FSP068_ActualValue_DCLinkVoltage   : in    std_logic_vector(15 downto 0);  -- 16bit unsigned value 0 - 900V
        FSP069_ActualValue_SumPSCurrent    : in    std_logic_vector(15 downto 0);  -- 16bit signed value +/- 2000A
        FSP070_WaterFlowMeasurement        : in    std_logic_vector(15 downto 0);
        FSP071_HighSpeedSelect             : out   std_logic_vector(15 downto 0);

        sum_interlock_n      : out   std_logic;
        interlocks_pending   : out   std_logic_vector(191 downto 0);
        interlocks_memorized : out   std_logic_vector(191 downto 0);

        reset_from_mfu : out   std_logic
    );
end entity gsi_usi;

architecture rtl of gsi_usi is

    -- the following component represents an fsp input (data from module to host), data of this fsp can be read from the host, for example status information
    component fsp_input is
        generic (
            gFSP_InputVersion : REAL;
            gFSPDepth         : integer;
            gFSPName          : STRING;
            gFSPNumber        : integer
        );
        port (
            Clock               : in    std_logic;
            Reset               : in    std_logic;
            ClockData           : in    std_logic;
            Address             : in    std_logic_vector(7 downto 0);
            InputFromPeripheral : in    std_logic_vector;
            Available           : out   std_logic;
            ReadOnly            : out   std_logic;
            Data                : out   std_logic_vector(7 downto 0);
            RemainingBytes      : out   std_logic_vector(11 downto 0)
        );
    end component;

    -- the following component represents an fsp output (data from host to module), data of this fsp can be written or read from the host, for example module parameters
    -- YOU NEED SOME INSTANCES OF THIS MODULE IN EVERY CASE!!
    -- PLEASE REFER TO THE FSP DESCRIPTION BELOW
    component fsp_output is
        generic (
            gFSP_OutputVersion      : REAL;
            gFSPDepth               : integer;
            gFSPName                : STRING;
            gFSPNumber              : integer;
            gReset                  : std_logic_vector;
            gUseInputFromPeripheral : std_logic
        );
        port (
            Clock                  : in    std_logic;
            Reset                  : in    std_logic;
            RWn                    : in    std_logic;
            CRCOK                  : in    std_logic;
            LoadDataFromPeripheral : in    std_logic;
            ClockData              : in    std_logic;
            Address                : in    std_logic_vector(7 downto 0);
            Data                   : inout std_logic_vector(7 downto 0);
            InputFromPeripheral    : in    std_logic_vector(((gFSPDepth * 8) - 1) downto 0);
            NewDataAvailable       : out   std_logic;
            Available              : out   std_logic;
            ReadOnly               : out   std_logic;
            OutputToPeripheral     : out   std_logic_vector(((gFSPDepth * 8) - 1) downto 0);
            RemainingBytes         : out   std_logic_vector(11 downto 0)
        );
    end component;

    -- the following component represents the ModuleDescritionStructure which holds main module information
    -- YOU NEED THIS MODULE IN EVERY CASE!!
    component fsp_mds is
        generic (
            bDeviceMaxSpeed                : integer;
            gFSP_MDSVersion                : REAL;
            gFSPName                       : STRING;
            lMaxPower                      : integer;
            lProductID                     : integer;
            sDescription                   : STRING;
            wAttributes                    : integer;
            gAttrib_Bit7_nFac_Applic_Image : std_logic;
            wModuleClass                   : integer;
            wModuleSubClass                : integer;
            wUSI                           : integer;
            wVendorID                      : integer
        );
        port (
            Clock                   : in    std_logic;
            Reset                   : in    std_logic;
            ClockData               : in    std_logic;
            Address                 : in    std_logic_vector(7 downto 0);
            FIFOStartpointRAMDataQ  : in    std_logic_vector(31 downto 0);
            lHWMinorRelease         : in    std_logic_vector(15 downto 0);
            lFWMinorRelease         : in    std_logic_vector(15 downto 0);
            sHWDate                 : in    std_logic_vector(23 downto 0);
            sSerial                 : in    std_logic_vector(47 downto 0);
            sFWDate                 : in    std_logic_vector(23 downto 0);
            wHWMajorRelease         : in    std_logic_vector(7 downto 0);
            wFWMajorRelease         : in    std_logic_vector(7 downto 0);
            OutputValueIsASCII      : out   std_logic;
            Available               : out   std_logic;
            ReadOnly                : out   std_logic;
            Data                    : out   std_logic_vector(7 downto 0);
            FIFOStartpointRAMRDAddr : out   std_logic_vector(7 downto 0);
            RemainingBytes          : out   std_logic_vector(11 downto 0)
        );
    end component;

    -- the following component combines many smaller sub modules to a higher level module
    -- these combined modules control the FSPs and communication between Host and Sub-module
    -- YOU NEED THIS MODULE IN EVERY CASE!!
    component music_shell is
        generic (
            gmUSIC_ShellVersion                  : string;
            gShellFSPImgStartAddress             : std_logic_vector(23 downto 0);
            gShellModuleNumber                   : integer;
            gShellNumberOfFSPs                   : integer;
            gShellMainClockInHz                  : integer;
            gShellTimeoutInSeconds               : integer;
            gShellRxFIFODepth                    : integer;
            gShellTxFIFODepth                    : integer;
            gShellUse_HighSpeedPort_NewDataPulse : integer;
            gShellUseGenericModuleNumber         : std_logic;
            gShellUseRAMForRxFIFO                : integer;
            gShellUseRAMForTxFIFO                : integer;
            gShellNoInitSearchRun                : std_logic
        );
        port (
            Clock                       : in    std_logic;
            Reset                       : in    std_logic;
            USI_RX                      : in    std_logic;
            USIInHighSpeedIn            : in    std_logic;
            SetUSIMode                  : in    std_logic;
            SetNewBaudRate              : in    std_logic;
            ValueIsASCII                : in    std_logic;
            FSPImgMakeImage             : in    std_logic;
            FSPImgLoadImage             : in    std_logic;
            FSPImgLoadImageAfterPowerUp : in    std_logic;
            FSPImgEraseAll              : in    std_logic;
            FSPImgUseSectorAddress      : in    std_logic;
            HighSpeedPort_NewDataPulse  : in    std_logic;
            -- FROM_EPCS_DATA                : IN    STD_LOGIC;
            BaudRateIn              : in    std_logic_vector(2 downto 0);
            Data                    : inout std_logic_vector(7 downto 0);
            FIFOStartpointRAMRDAddr : in    std_logic_vector(7 downto 0);
            FSPAvailable            : in    std_logic_vector(gNumberOfFSPs downto 0);
            FSPImgFSPNumber         : in    std_logic_vector(7 downto 0);
            FSPImgSectorAddress     : in    std_logic_vector(7 downto 0);
            FSPReadOnly             : in    std_logic_vector(gNumberOfFSPs downto 0);
            HighSpeed_In            : in    std_logic_vector(31 downto 0);
            HS_Ext_Bits_In          : in    std_logic_vector(1 downto 0);
            ModuleNumber            : in    std_logic_vector(3 downto 0);
            RemainingBytes          : in    std_logic_vector(11 downto 0);
            USI_TX                  : out   std_logic;
            USI_TxDriverEnable      : out   std_logic;
            USIInHighSpeedMode      : out   std_logic;
            RWn                     : out   std_logic;
            ClockData               : out   std_logic;
            CRCOK                   : out   std_logic;
            FSPImgReady             : out   std_logic;
            -- TO_EPCS_nCS                   : OUT   STD_LOGIC;
            -- TO_EPCS_ASDI                  : OUT   STD_LOGIC;
            -- TO_EPCS_DCLK                  : OUT   STD_LOGIC;
            FIFOStartpointRAMDataQ      : out   std_logic_vector(31 downto 0);
            HighSpeed_Out               : out   std_logic_vector(31 downto 0);
            HighSpeedPort_NewDataStrobe : out   std_logic_vector(1 downto 0);
            HS_Ext_Bits_Out             : out   std_logic_vector(1 downto 0);
            LED_Control                 : out   std_logic_vector(3 downto 0);
            ReceivedFSP                 : out   std_logic_vector(7 downto 0);
            USIControlStatusTri         : out   std_logic_vector(15 downto 0)
        );
    end component;

    -- This component ist OPTIONAL and not bound, take a look on the .bdf or ask ACU_Support for help.
    component ACU_Command_Decoder is
        generic (
            gACU_Command_Decoder_Version : string    := "28.05.21"
        );
        port (
            Clock                    : in    std_logic;
            Reset                    : in    std_logic;
            Command                  : in    std_logic_vector(3 downto 0);
            Command_ON               : out   std_logic;
            Command_OFF              : out   std_logic;
            Command_Reset            : out   std_logic;
            Command_ControllerLocked : out   std_logic;
            Command_TriggerSomething : out   std_logic
        );
    end component ACU_Command_Decoder;

    -- This component ist OPTIONAL and not bound, take a look on the .bdf or ask ACU_Support for help.
    component ACU_InterlockMemory is
        generic (
            gACU_InterlockMemoryVersion   : real      := 2.4;
            gMainClockInHz                : integer   := 100_000_000;
            gMaskSwitchDisableDelay_in_ns : integer   := 500;
            gByteWidthOfInterlockBus      : integer   := 3;
            gNrInterlocksArrSeqElements   : integer   := 5;
            -- Nachfolgend muss eigentlich
            -- gCalc                               : PARAMETER_UNKNOWN (erscheint als AUTO)   := ceil(log2(gNrInterlocksArrSeqElements))
            -- stehen.
            -- Quartus kann mit dieser Angabe aber KEIN Symbol erstellen.
            -- Daher gilt es nach der Symbolgenerierung die Parameter manuell im Symbol zu editieren.
            gCalc : integer   := 3
        );
        port (
            Clock             : in    std_logic;
            Reset             : in    std_logic;
            MemoryReset       : in    std_logic;
            MaskSwitchDisable : in    std_logic;

            Interlocks_n              : in    std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);
            InterlocksMaskMemorized_n : in    std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);
            --| nicht zu beachtende Interlocks muessen mit '1' maskiert sein
            InterlocksMaskPending_n : in    std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);
            --| nicht zu beachtende Interlocks muessen mit '1' maskiert sein
            InterlocksSwitchMaskPending_n : in    std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);
            --| nicht zu beachtende Interlocks muessen mit '1' maskiert sein
            InterlocksPending_n   : out   std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);
            InterlocksMemorized_n : out   std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);
            InterlocksArrSeq      : out   array8bits(gNrInterlocksArrSeqElements - 1 downto 0);
            --| is the first arrived and InterlocksArrSeq(0) is the last one.
            InterlocksArrSeqNmb : out   std_logic_vector(gCalc - 1 downto 0);

            SumInterlock_n : out   std_logic
        );
    end component ACU_InterlockMemory;

    signal FSP001_ModuleStatus : std_logic_vector(23 downto 0);

    signal FSP012_USIConfig        : std_logic_vector(7 downto 0); -- one of the most important fsps for usi interface configuration
    signal FSP012_NewDataAvailable : std_logic;

    signal FIFOStartpointRAMDataQ  : std_logic_vector(31 downto 0);
    signal FIFOStartpointRAMRDAddr : std_logic_vector(7 downto 0);

    -- the following signals connect fsps with the music shell
    -- not all signals are needed for every fsp, it depends if the fsp is an input or output or an mds
    signal FSP_Available      : std_logic_vector(gNumberOfFSPs downto 0); -- all fsps
    signal FSP_ReadOnly       : std_logic_vector(gNumberOfFSPs downto 0); -- all fsps
    signal ReceivedFSP        : std_logic_vector(7 downto 0);             -- all fsps
    signal FSP_Data           : std_logic_vector(7 downto 0);             -- all fsps
    signal FSP_ClockData      : std_logic;                                -- all fsps
    signal FSP_RemainingBytes : std_logic_vector(11 downto 0);            -- all fsps
    signal FSP_CRCOK          : std_logic;                                -- output fsps only
    signal FSP_RWn            : std_logic;                                -- output fsps only
    signal FSP_ValueIsASCII   : std_logic;                                -- mds only

    -- status information
    signal NoWarnings     : std_logic;
    signal SumInterlock_n : std_logic;

    signal USIIsHighSpeed : std_logic; -- becomes '1' when usi communication is high speed

    signal Cmd_ResetFromMFU : std_logic;                               -- It is the reset decoded command coming from the MFU through the High Speed bus.
    -- It is used to clear the memorized interlocks (latched interlocks) in the
    -- ACU_InterlockMemory block.
    signal HighSpeedReceivedData : std_logic_vector(31 downto 0);      -- HighSpeed data bus from MFU (to the slave module). Over this bus data and commands from
    -- MFU to the slave modules are provided. The bud structure is defined in PowerConfigAdvance
    -- (MFU -> HIGHSpeedChannel tab). The last 4 LSBs are always used to carry commands.
    signal Interlocks_n              : std_logic_vector(191 downto 0); -- Interlock bus (bit active low)
    signal InterlockMask             : std_logic_vector(575 downto 0);
    signal FSP004_Interlock_Detected : std_logic_vector(383 downto 0);
    signal FSP006_InterlockArrivSeq  : std_logic_vector(31 downto 0) := X"00000000";
    signal InterlocksArrSeq          : array8bits(2 downto 0);

    signal clock_100mhz  : std_logic;
    signal powerup_reset : std_logic;

begin

    clock_100mhz  <= clock;
    powerup_reset <= reset when usi_ready = '1' else
                     '1';

    FSP001_ModuleStatus <= (
                            8 => USIIsHighSpeed,
                            5 => SumInterlock_n,
                            3 => NoWarnings,
                            others => '1'
                        );

    NoWarnings <= '1' when FSP002_Warnings = x"FFFFFFFF" else
                  '0';

    Interlocks_n <= interlocks_in;

    -- FSP11 is not used
    InterlockMask <= (others => '0');

    -- Output Assignments
    highspeed_out        <= HighSpeedReceivedData;
    reset_from_mfu       <= Cmd_ResetFromMFU;
    sum_interlock_n      <= SumInterlock_n;
    interlocks_pending   <= FSP004_Interlock_Detected(191 downto 0);
    interlocks_memorized <= FSP004_Interlock_Detected(383 downto 192);

    ------------------------------------------------- The mUISc Shell -----------------------------------------------------
    -------------------------------          YOU NEED THIS MODULE IN EVERY CASE!!             -----------------------------
    -- the following instance is the so called mUSIc Shell and represents the centre of the usi communication
    -- inside this module there are many different smaller modules which control usi communication, bit-rate, crc, fsp addressing, data mapping and so on...
    -- all fsps are tied to this module via dedicated signals (usi bus)

    b2v_inst_music_shell : component music_shell
        generic map (
            gmUSIC_ShellVersion                  => "07.05.21",
            gShellMainClockInHz                  => gMainClockInHz,
            gShellTimeoutInSeconds               => gTimeoutInSeconds,
            gShellNumberOfFSPs                   => gNumberOfFSPs,
            gShellUseGenericModuleNumber         => gUseGenericModuleNumber,
            gShellModuleNumber                   => gModuleNumber,
            gShellUseRAMForRxFIFO                => gUseRAMForRxFIFO,
            gShellUseRAMForTxFIFO                => gUseRAMForTxFIFO,
            gShellRxFIFODepth                    => gRxFIFODepth,
            gShellTxFIFODepth                    => gTxFIFODepth,
            gShellFSPImgStartAddress             => gFSPImgStartAddress,
            gShellUse_HighSpeedPort_NewDataPulse => gUse_HighSpeedPort_NewDataPulse,
            gShellNoInitSearchRun                => gShellNoInitSearchRun
        )
        port map (
            clock                       => clock_100mhz,
            reset                       => powerup_reset,
            usi_rx                      => usi_rx,
            usiinhighspeedin            => FSP012_USIConfig(7),
            setusimode                  => FSP012_NewDataAvailable,
            setnewbaudrate              => FSP012_NewDataAvailable,
            valueisascii                => FSP_ValueIsASCII,
            fspimgmakeimage             => '0',
            fspimgloadimage             => '0',
            fspimgloadimageafterpowerup => '0',
            fspimgeraseall              => '0',
            fspimgusesectoraddress      => '0',
            highspeedport_newdatapulse  => '0',
            -- FROM_EPCS_DATA                => FROM_SERIAL_FLASH_SDIN,
            baudratein              => FSP012_USIConfig(2 DOWNTO 0),
            data                    => FSP_Data,
            fifostartpointramrdaddr => FIFOStartpointRAMRDAddr,
            fspavailable            => FSP_Available,
            fspimgfspnumber         => (others => '0'),
            fspimgsectoraddress     => (others => '0'),
            fspreadonly             => FSP_ReadOnly,
            highspeed_in            => highspeed_in,
            hs_ext_bits_in          => (others => '0'),
            modulenumber            => (others => '0'),
            remainingbytes          => FSP_RemainingBytes,
            usi_tx                  => usi_tx,
            usi_txdriverenable      => open,
            usiinhighspeedmode      => USIIsHighSpeed,
            rwn                     => FSP_RWn,
            clockdata               => FSP_ClockData,
            crcok                   => FSP_CRCOK,
            highspeed_out           => HighSpeedReceivedData,
            fspimgready             => open,
            -- TO_EPCS_nCS                   => TO_SERIAL_FLASH_CSn,
            -- TO_EPCS_ASDI                  => TO_SERIAL_FLASH_SDOUT,
            -- TO_EPCS_DCLK                  => TO_SERIAL_FLASH_SCLK,
            fifostartpointramdataq => FIFOStartpointRAMDataQ,
            receivedfsp            => ReceivedFSP
        );

    ------------------------------------------ Command Decoder --------------------------------
    ---------------------------------------------- OPTIONAL------------------------------------
    -- Command decoder
    b2v_inst_ACU_Command_Decoder : component ACU_Command_Decoder
        port map (
            Clock   => clock_100mhz,
            Reset   => powerup_reset,
            Command => HighSpeedReceivedData(3 downto 0),

            Command_ON               => open,
            Command_OFF              => open,
            Command_Reset            => Cmd_ResetFromMFU,
            Command_ControllerLocked => open,
            Command_TriggerSomething => open
        );

    ----------------------------------------- Interlock memory --------------------------------
    --------------------------------------------- OPTIONAL------------------------------------

    -- Interlock Memory
    b2v_inst_ACU_InterlockMemory : component ACU_InterlockMemory
        generic  map (
            gACU_InterlockMemoryVersion   => 2.4,
            gMainClockInHz                => gMainClockInHz,
            gMaskSwitchDisableDelay_in_ns => 500,
            gByteWidthOfInterlockBus      => 24,
            gNrInterlocksArrSeqElements   => 3,
            gCalc                         => 2
        )
        port map (
            Clock             => clock_100mhz,
            Reset             => powerup_reset,
            MemoryReset       => Cmd_ResetFromMFU,
            MaskSwitchDisable => '1',

            Interlocks_n                  => Interlocks_n,
            InterlocksMaskMemorized_n     => InterlockMask(191 downto 0),
            InterlocksMaskPending_n       => InterlockMask(383 downto 192),
            InterlocksSwitchMaskPending_n => InterlockMask(575 downto 384),

            InterlocksPending_n   => FSP004_Interlock_Detected(191 downto 0),
            InterlocksMemorized_n => FSP004_Interlock_Detected(383 downto 192),

            InterlocksArrSeq    => InterlocksArrSeq,
            InterlocksArrSeqNmb => FSP006_InterlockArrivSeq(1 downto 0),

            SumInterlock_n => SumInterlock_n
        );

    FSP006_InterlockArrivSeq(31 downto 24) <= InterlocksArrSeq(2);
    FSP006_InterlockArrivSeq(23 downto 16) <= InterlocksArrSeq(1);
    FSP006_InterlockArrivSeq(15 downto 8)  <= InterlocksArrSeq(0);

    ------------------------------- FSP 000 (MDS) -----------------------------
    --------------------  YOU NEED THIS MODULE IN EVERY CASE!!  ---------------

    b2v_inst_MDS : component fsp_mds
        generic map (
            bDeviceMaxSpeed                => 1,
            gFSP_MDSVersion                => 0.3,
            gFSPName                       => "FSP000_MDS_REQUIRED_FSP",
            lMaxPower                      => 0,
            lProductID                     => 1,
            sDescription                   => "Resonance Sextupole",
            wAttributes                    => 2,
            gAttrib_Bit7_nFac_Applic_Image => gAttrib_Bit7_nFac_Applic_Image,
            wModuleClass                   => 33,
            wModuleSubClass                => 0,
            wUSI                           => 17,
            wVendorID                      => 5
        )
        port map (
            Clock           => clock_100mhz,
            Reset           => powerup_reset,
            ClockData       => FSP_ClockData,
            Address         => ReceivedFSP,
            sHWDate         => x"010122",
            wHWMajorRelease => x"01",
            lHWMinorRelease => x"0000",
            sFWDate         => x"120423",
            wFWMajorRelease => x"07",
            lFWMinorRelease => x"0100",
            sSerial         => FSP009_ModuleSerialNumber,

            OutputValueIsASCII => FSP_ValueIsASCII,
            Available          => FSP_Available(0),
            ReadOnly           => FSP_ReadOnly(0),
            Data               => FSP_Data,
            RemainingBytes     => FSP_RemainingBytes,

            FIFOStartpointRAMDataQ  => FIFOStartpointRAMDataQ,
            FIFOStartpointRAMRDAddr => FIFOStartpointRAMRDAddr
        );

    ------------------------------- FSP 001 -----------------------------

    b2v_inst_FSP1 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 3,
            gFSPName          => "FSP001_ModuleStatus",
            gFSPNumber        => 1
        )
        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP001_ModuleStatus,
            Available           => FSP_Available(1),
            ReadOnly            => FSP_ReadOnly(1),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 002 -----------------------------

    b2v_inst_FSP2 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 4,
            gFSPName          => "FSP002_ModuleWarnings",
            gFSPNumber        => 2
        )
        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP002_Warnings,
            Available           => FSP_Available(2),
            ReadOnly            => FSP_ReadOnly(2),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 003 -----------------------------

    FSP_Available(3) <= '0';
    FSP_ReadOnly(3)  <= '0';

    ------------------------------- FSP 004 -----------------------------
    -----------------  YOU NEED THIS MODULE IN EVERY CASE!!  ------------

    b2v_inst_FSP4 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 48,
            gFSPName          => "FSP004_ModuleInterlocks_REQUIRED_FSP",
            gFSPNumber        => 4
        )
        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP004_Interlock_Detected,
            Available           => FSP_Available(4),
            ReadOnly            => FSP_ReadOnly(4),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 005 -----------------------------

    FSP_Available(5) <= '0';
    FSP_ReadOnly(5)  <= '0';

    ------------------------------- FSP 006 -----------------------------
    -------- it is used to store the interlock arrival sequence  --------

    b2v_inst_FSP6 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 4,
            gFSPName          => "FSP006_InterlocksArrivalSequence",
            gFSPNumber        => 6
        )
        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP006_InterlockArrivSeq,
            Available           => FSP_Available(6),
            ReadOnly            => FSP_ReadOnly(6),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------ FSP 007 to FSP 008 -------------------------

    FSP_Available(7) <= '0';
    FSP_ReadOnly(7)  <= '0';

    FSP_Available(8) <= '0';
    FSP_ReadOnly(8)  <= '0';

    ------------------------------- FSP 009 -----------------------------
    ----------- Your module should have a unique serial number, so you need this fsp usually

    b2v_inst_FSP9 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 6,
            gFSPName          => "FSP009_ModuleSerialNumber_USUALLY_REQUIRED_FSP",
            gFSPNumber        => 9
        )
        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP009_ModuleSerialNumber,
            Available           => FSP_Available(9),
            ReadOnly            => FSP_ReadOnly(9),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 010 -----------------------------
    FSP_Available(10) <= '0';
    FSP_ReadOnly(10)  <= '0';

    ------------------------------- FSP 011 -----------------------------
    FSP_Available(11) <= '0';
    FSP_ReadOnly(11)  <= '0';

    ------------------------------- FSP 012 -----------------------------
    ---------------  YOU NEED THIS MODULE IN EVERY CASE!!  --------------

    b2v_inst_FSP12 : component fsp_output
        generic map (
            gFSP_OutputVersion      => 0.1,
            gFSPDepth               => 1,
            gFSPName                => "FSP012_USIConfig_REQUIRED_FSP",
            gFSPNumber              => 12,
            gReset                  => "00000000",
            gUseInputFromPeripheral => '0'
        )
        port map (
            Clock                  => clock_100mhz,
            Reset                  => powerup_reset,
            RWn                    => FSP_RWn,
            CRCOK                  => FSP_CRCOK,
            ClockData              => FSP_ClockData,
            Address                => ReceivedFSP,
            Data                   => FSP_Data,
            NewDataAvailable       => FSP012_NewDataAvailable,
            Available              => FSP_Available(12),
            ReadOnly               => FSP_ReadOnly(12),
            OutputToPeripheral     => FSP012_USIConfig,
            LoadDataFromPeripheral => '0',
            InputFromPeripheral    => X"00",
            RemainingBytes         => FSP_RemainingBytes
        );

    ------------------------ FSP 013 to FSP 019 -------------------------

    FSP_Available(19 downto 13) <= (others => '0');
    FSP_ReadOnly(19 downto 13)  <= (others => '0');

    ------------------------------- FSP 020 -----------------------------

    b2v_inst_FSP20 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 2,
            gFSPName          => "FSP020_ActualValue_A",
            gFSPNumber        => 20
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP020_ActualValue_A,
            Available           => FSP_Available(20),
            ReadOnly            => FSP_ReadOnly(20),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 066 -----------------------------

    b2v_inst_FSP66 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 2,
            gFSPName          => "FSP066_ActualValue_OutputCurrent",
            gFSPNumber        => 66
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP066_ActualValue_OutputCurrent,
            Available           => FSP_Available(66),
            ReadOnly            => FSP_ReadOnly(66),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 067 -----------------------------

    b2v_inst_FSP67 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 2,
            gFSPName          => "FSP067_ActualValue_OutputVoltage",
            gFSPNumber        => 67
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP067_ActualValue_OutputVoltage,
            Available           => FSP_Available(67),
            ReadOnly            => FSP_ReadOnly(67),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 068 -----------------------------

    b2v_inst_FSP68 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 2,
            gFSPName          => "FSP068_ActualValue_DCLinkVoltage",
            gFSPNumber        => 68
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP068_ActualValue_DCLinkVoltage,
            Available           => FSP_Available(68),
            ReadOnly            => FSP_ReadOnly(68),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 068 -----------------------------

    b2v_inst_FSP69 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 2,
            gFSPName          => "FSP069_ActualValue_SumPsCurrent",
            gFSPNumber        => 69
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP069_ActualValue_SumPSCurrent,
            Available           => FSP_Available(69),
            ReadOnly            => FSP_ReadOnly(69),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------ FSP 024 to FSP 029 -------------------------

    FSP_Available(29 downto 24) <= (others => '0');
    FSP_ReadOnly(29 downto 24)  <= (others => '0');

    ------------------------------- FSP 030 -----------------------------

    b2v_inst_FSP30 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 3,
            gFSPName          => "FSP030_SetValue_A",
            gFSPNumber        => 30
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP030_SetValue_A,
            Available           => FSP_Available(30),
            ReadOnly            => FSP_ReadOnly(30),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------ FSP 032 to FSP 052 -------------------------

    FSP_Available(52 downto 32) <= (others => '0');
    FSP_ReadOnly(52 downto 32)  <= (others => '0');

    ------------------------------- FSP 053 -----------------------------

    b2v_inst_FSP53 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 8,
            gFSPName          => "FSP053_ModuleTemperatures",
            gFSPNumber        => 53
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP053_ModuleTemperatures,
            Available           => FSP_Available(53),
            ReadOnly            => FSP_ReadOnly(53),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 054 -----------------------------

    FSP_Available(54) <= '0';
    FSP_ReadOnly(54)  <= '0';

    ------------------------------- FSP 055 -----------------------------

    b2v_inst_FSP70 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 2,
            gFSPName          => "FSP070_WaterFlowMeasurement",
            gFSPNumber        => 70
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP070_WaterFlowMeasurement,
            Available           => FSP_Available(70),
            ReadOnly            => FSP_ReadOnly(70),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------ FSP 056 to FSP 059 -------------------------

    FSP_Available(59 downto 56) <= (others => '0');
    FSP_ReadOnly(59 downto 56)  <= (others => '0');

    ------------------------------- FSP 060 -----------------------------

    b2v_inst_FSP60 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 32,
            gFSPName          => "FSP060_CurrentPowerStages",
            gFSPNumber        => 60
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP060_CurrentPowerStages,
            Available           => FSP_Available(60),
            ReadOnly            => FSP_ReadOnly(60),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 061 -----------------------------

    b2v_inst_FSP61 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 32,
            gFSPName          => "FSP061_OffsetCorrectionPowerStages",
            gFSPNumber        => 61
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP061_OffsetCorrectionPowerStages,
            Available           => FSP_Available(61),
            ReadOnly            => FSP_ReadOnly(61),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 062 -----------------------------

    b2v_inst_FSP62 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 8,
            gFSPName          => "FSP062_OffsetCorrectionGeneral",
            gFSPNumber        => 62
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP062_OffsetCorrectionGeneral & x"00000000",
            Available           => FSP_Available(62),
            ReadOnly            => FSP_ReadOnly(62),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 063 -----------------------------

    b2v_inst_FSP63 : component fsp_input
        generic map (
            gFSP_InputVersion => 0.1,
            gFSPDepth         => 2,
            gFSPName          => "FSP063_VoltageDCLinkLowerHalf",
            gFSPNumber        => 63
        )

        port map (
            Clock               => clock_100mhz,
            Reset               => powerup_reset,
            ClockData           => FSP_ClockData,
            Address             => ReceivedFSP,
            InputFromPeripheral => FSP063_VoltageDCLinkLowerHalf,
            Available           => FSP_Available(63),
            ReadOnly            => FSP_ReadOnly(63),
            Data                => FSP_Data,
            RemainingBytes      => FSP_RemainingBytes
        );

    ------------------------------- FSP 064 -----------------------------

    b2v_inst_FSP64 : component fsp_output
        generic map (
            gFSP_OutputVersion      => 0.1,
            gFSPDepth               => 1,
            gFSPName                => "FSP064_WaterValveControl",
            gFSPNumber              => 64,
            gReset                  => "00000000",
            gUseInputFromPeripheral => '0'
        )

        port map (
            Clock                  => clock_100mhz,
            Reset                  => powerup_reset,
            RWn                    => FSP_RWn,
            CRCOK                  => FSP_CRCOK,
            ClockData              => FSP_ClockData,
            Address                => ReceivedFSP,
            Data                   => FSP_Data,
            Available              => FSP_Available(64),
            ReadOnly               => FSP_ReadOnly(64),
            OutputToPeripheral     => FSP064_WaterValveControl,
            LoadDataFromPeripheral => '0',
            InputFromPeripheral    => (others => '0'),
            RemainingBytes         => FSP_RemainingBytes
        );

    ------------------------------- FSP 066 -----------------------------

    b2v_inst_FSP65 : component fsp_output
        generic map (
            gFSP_OutputVersion      => 0.1,
            gFSPDepth               => 2,
            gFSPName                => "FSP065_ServiceControl",
            gFSPNumber              => 65,
            gReset                  => X"0000",
            gUseInputFromPeripheral => '0'
        )

        port map (
            Clock                  => clock_100mhz,
            Reset                  => powerup_reset,
            RWn                    => FSP_RWn,
            CRCOK                  => FSP_CRCOK,
            ClockData              => FSP_ClockData,
            Address                => ReceivedFSP,
            Data                   => FSP_Data,
            Available              => FSP_Available(65),
            ReadOnly               => FSP_ReadOnly(65),
            OutputToPeripheral     => FSP065_ServiceControl,
            LoadDataFromPeripheral => '0',
            InputFromPeripheral    => (others => '0'),
            RemainingBytes         => FSP_RemainingBytes
        );

    ------------------------------- FSP 071 -----------------------------

    b2v_inst_FSP71 : component fsp_output
        generic map (
            gFSP_OutputVersion      => 0.1,
            gFSPDepth               => 2,
            gFSPName                => "FSP071_HighSpeed_ReturnChannel_SourceSelectionMux",
            gFSPNumber              => 71,
            gReset                  => x"0000",
            gUseInputFromPeripheral => '0'
        )

        port map (
            Clock                  => clock_100mhz,
            Reset                  => powerup_reset,
            RWn                    => FSP_RWn,
            CRCOK                  => FSP_CRCOK,
            ClockData              => FSP_ClockData,
            Address                => ReceivedFSP,
            Data                   => FSP_Data,
            Available              => FSP_Available(71),
            ReadOnly               => FSP_ReadOnly(71),
            OutputToPeripheral     => FSP071_HighSpeedSelect,
            LoadDataFromPeripheral => '0',
            InputFromPeripheral    => (others => '0'),
            RemainingBytes         => FSP_RemainingBytes
        );

end architecture rtl;
