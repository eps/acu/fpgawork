--------------------------------------------------------------------------------
-- (C) GE Power Conversion - 2022															--
--																										--
-- The copyright in this software is the property of GE Power Conversion.		--
--																										--
-- It is supplied on the express terms that it may not be copied, used or		--
-- disclosed to others for any purpose except as authorised by the above		--
-- named Company.																					--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- File 		: Module.vhd																		--
-- Date 		: 09 Dec 2022																		--
-- Author 	: A.Britto (GE) & GSI															--
-- Project 	: GSI																					--
-- Board 	: 																						--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Modifications :																				--	 
-- 20-12-2022	A.Britto	- Updated according to the requirements					--
-- 02-01-2023	A.Britto - Updated based on comments from GSI						--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

LIBRARY work;
USE work.ACU_package.all;

ENTITY Module IS
GENERIC (
         gMainClockInHz                                        : INTEGER := 100000000;
         gMDNumberOfHighSpeedReceiverSlots                     : INTEGER := 1;
         gMDNumberOfHighSpeedTransmitterSlots                  : INTEGER := 1;
         gModuleNumbersOfFSPs                                  : INTEGER := 68
        );
   PORT
   (
      Reset                                     :  IN   STD_LOGIC;
      Clock                                     :  IN   STD_LOGIC;
		
		--USI related
      USIRxSerial                               :  IN   STD_LOGIC;
 
		--Signals from Peripheral to USI
		Measurements										:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes   Measurements to MFU via HS 
		Status 												:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes   PC status to MFU via HS
		ModuleStatus										:  IN	  STD_LOGIC_VECTOR(79 DOWNTO 0); -- 10 bytes FSP001 input
		ModuleWarnings										:  IN   STD_LOGIC_VECTOR(79 DOWNTO 0); -- 10 bytes FSP002 input
		ModuleInterlocks_n								:  IN   STD_LOGIC_VECTOR(79 DOWNTO 0); -- 10 bytes FSP0004 input 
		CurrentValue1										:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP060 input
		CurrentValue2										:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP061 input
		CurrentValue3										:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP062 input
		CurrentValue4										:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP063 input
		CurrentValue5										:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP064 input
		CurrentValue6										:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP065 input
		CurrentValue7										:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP066 input
		
		--USI related
      USITxSerial                               :  OUT  STD_LOGIC;
		USIInHighSpeedMode								:  OUT  STD_LOGIC;
		
		--Signals from USI to Peripheral
		Command												:  OUT  STD_LOGIC_VECTOR( 7 DOWNTO 0); -- 1 byte Received latched command via HS
		USIConfig											:  OUT  STD_LOGIC_VECTOR( 7 DOWNTO 0); -- 1 byte USI configuration from FSP012
		AnalogValSelector									:  OUT  STD_LOGIC_VECTOR( 7 DOWNTO 0);	-- 1 byte selector for analog values FSP067 input 
		SetvalueToleranceFactor							:  OUT  STD_LOGIC_VECTOR(23 DOWNTO 0)  -- 3 bytes Set value for Tolerance factor FSP068 output 
		
   );
END Module;

ARCHITECTURE a OF Module IS

----------------------------------------------------------------------------
COMPONENT music_shell
GENERIC (
         gM25PFSPImgStartAddress                               : STD_LOGIC_VECTOR(23 DOWNTO 0);
         gM25PNoInitSearchRun                                  : STD_LOGIC;
         gMainClockInHz                                        : INTEGER;
         gmCoreModuleNumber                                    : INTEGER;
         gmCoreNumberOfFSPs                                    : INTEGER;
         gmCoreTimeoutInSeconds                                : INTEGER;
         gmCoreUseGenericModuleNumber                          : STD_LOGIC;
         gMDHighSpeedPort_ByteIndexForNewDataTransmittedPulse  : unsigned(15 downto 0);
         gMDNumberOfHighSpeedReceiverSlots                     : INTEGER;
         gMDNumberOfHighSpeedTransmitterSlots                  : INTEGER;
         gMDUse_HighSpeedPort_AcceptNewDataRisingEdge          : BIT_VECTOR(3 downto 0);
         gUSIRxFIFODepth                                       : INTEGER;
         gUSITxFIFODepth                                       : INTEGER;
         gUSIUseRAMForRxFIFO                                   : INTEGER;
         gUSIUseRAMForTxFIFO                                   : INTEGER
        );
   PORT(
      Clock                                       		:  IN    STD_LOGIC;
         Reset                                        :  IN    STD_LOGIC;
         --mShell
         LED_Control                                  :  OUT   STD_LOGIC_VECTOR(3 DOWNTO 0);
         --USI
         USIRxSerial                                  :  IN    STD_LOGIC;
         USIControlStatus_Tri                         :  OUT   STD_LOGIC_VECTOR(15 DOWNTO 0);
         USITxSerial                                  :  OUT   STD_LOGIC;
         USITxDriverEnable                            :  OUT   STD_LOGIC;
         --M/D
         MDSelect_HighSpeedSlot                       :  IN    STD_LOGIC_VECTOR(1 DOWNTO 0);
         MDHighSpeedPort_In                           :  IN    STD_LOGIC_VECTOR(gMDNumberOfHighSpeedTransmitterSlots*32-1 DOWNTO 0);
         MDHighSpeedPort_AcceptNewDataRisingEdge      :  IN    STD_LOGIC_VECTOR(gMDNumberOfHighSpeedTransmitterSlots-1 DOWNTO 0);
         MDHighSpeedSlotFreeWheel                     :  IN    STD_LOGIC;
         MDHighSpeedPort_Out                          :  OUT   STD_LOGIC_VECTOR(gMDNumberOfHighSpeedReceiverSlots*32-1 DOWNTO 0);
         MDHighSpeedPort_NewDataReceivedPulse         :  OUT   STD_LOGIC_VECTOR(gMDNumberOfHighSpeedReceiverSlots-1 DOWNTO 0);
         MDHighSpeedPort_NewDataTransmittedPulse      :  OUT   STD_LOGIC_VECTOR(gMDNumberOfHighSpeedTransmitterSlots-1 DOWNTO 0);
         --mCore
         ModuleNumber                                 :  IN    STD_LOGIC_VECTOR(3 DOWNTO 0);
         USIHighSpeedEnable                           :  IN    STD_LOGIC;
         SetUSIMode                                   :  IN    STD_LOGIC;
         BitRate                                      :  IN    STD_LOGIC_VECTOR(2 DOWNTO 0);
         SetNewBitRate                                :  IN    STD_LOGIC;
         InputValueIsASCII                            :  IN    STD_LOGIC;
         RemainingBytes                               :  IN    STD_LOGIC_VECTOR(11 DOWNTO 0);
         FSPAvailable                                 :  IN    STD_LOGIC_VECTOR(gmCoreNumberOfFSPs DOWNTO 0);
         FSPReadOnly                                  :  IN    STD_LOGIC_VECTOR(gmCoreNumberOfFSPs DOWNTO 0);
         FIFOStartpointRAMRDAddr                      :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgMakeImage                              :  IN    STD_LOGIC;
         FSPImgFSPNumber                              :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgLoadImage                              :  IN    STD_LOGIC;
         FSPImgLoadImageAfterPowerUp                  :  IN    STD_LOGIC;
         USIInHighSpeedMode                           :  OUT   STD_LOGIC;
         ReceivedFSP                                  :  OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
         RWn                                          :  OUT   STD_LOGIC;
         Data                                         :  INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         ClockData                                    :  OUT   STD_LOGIC;
         CRCOK                                        :  OUT   STD_LOGIC;
         FIFOStartpointRAMDataQ                       :  OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);
         --M25P_Access
         FSPImgEraseAll                               :  IN    STD_LOGIC;
         FSPImgUseSectorAddress                       :  IN    STD_LOGIC;
         FSPImgSectorAddress                          :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgReady                                  :  OUT   STD_LOGIC;
         --EPCS_Control
         FROM_EPCS_DATA                               :  IN    STD_LOGIC;
         TO_EPCS_nCS                                  :  OUT   STD_LOGIC;
         TO_EPCS_ASDI                                 :  OUT   STD_LOGIC;
         TO_EPCS_DCLK                                 :  OUT   STD_LOGIC
        );
END COMPONENT;

----------------------------------------------------------------------------
COMPONENT fsp_mds
GENERIC (
         bDeviceMaxSpeed                  : INTEGER;
         gAttrib_Bit7_nFac_Applic_Image   : STD_LOGIC;
         gFSPName                         : STRING;
         lMaxPower                        : INTEGER;
         lProductID                       : INTEGER;
         sDescription                     : STRING;
         wAttributes                      : INTEGER;
         wModuleClass                     : INTEGER;
         wModuleSubClass                  : INTEGER;
         wUSI                             : INTEGER;
         wVendorID                        : INTEGER
        );
   PORT(
         Clock                   : IN  STD_LOGIC;
         Reset                   : IN  STD_LOGIC;
         ClockData               : IN  STD_LOGIC;
         Address                 : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         FIFOStartpointRAMDataQ  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
         lFWMinorRelease         : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
         lHWMinorRelease         : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
         sFWDate                 : IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
         sHWDate                 : IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
         sSerial                 : IN  STD_LOGIC_VECTOR(47 DOWNTO 0);
         wFWMajorRelease         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         wHWMajorRelease         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         OutputValueIsASCII      : OUT STD_LOGIC;
         Available               : OUT STD_LOGIC;
         ReadOnly                : OUT STD_LOGIC;
         Data                    : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         FIFOStartpointRAMRDAddr : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         RemainingBytes          : OUT STD_LOGIC_VECTOR(11 DOWNTO 0)
        );
END COMPONENT;

----------------------------------------------------------------------------
COMPONENT fsp_input
GENERIC (
         gFSPDepth         : INTEGER;
         gFSPName          : STRING;
         gFSPNumber        : INTEGER
        );
   PORT(
		Clock                      : in     std_logic;
		Reset                      : in     std_logic;
		Address                    : in     std_logic_vector (7 downto 0); 
		Data                       : out    std_logic_vector (7 downto 0);  
		InputFromPeripheral        : in     std_logic_vector (((gFSPDepth * 8)-1) downto 0);
		RemainingBytes             : out    std_logic_vector (11 downto 0);
		Available                  : out    std_logic;
		ReadOnly                   : out    std_logic;
		ClockData                  : in     std_logic 
        );
END COMPONENT;

----------------------------------------------------------------------------
COMPONENT fsp_output
GENERIC (
         gFSPDepth               : INTEGER;
         gFSPName                : STRING;
         gFSPNumber              : INTEGER;
         gReset                  : STD_LOGIC_VECTOR;
         gUseInputFromPeripheral : STD_LOGIC
         );
   PORT(
		Clock                      : in     std_logic;
		Reset                      : in     std_logic;
		Address                    : in     std_logic_vector (7 downto 0);
		Data                       : inout  std_logic_vector (7 downto 0);
		RWn                        : in     std_logic; 
		CRCOK                      : in     std_logic;
		InputFromPeripheral        : in     std_logic_vector (((gFSPDepth * 8)-1) downto 0);
		LoadDataFromPeripheral     : in     std_logic; 
		NewDataAvailable           : out    std_logic;
		OutputToPeripheral         : out    std_logic_vector (((gFSPDepth * 8)-1) downto 0);
		RemainingBytes             : out    std_logic_vector (11 downto 0); 
		Available                  : out    std_logic; 
		ReadOnly                   : out    std_logic;
		ClockData                  : in     std_logic          
       );
END COMPONENT;

----------------------------------------------------------------------------

CONSTANT VCC_BUS                                 	:  STD_LOGIC_VECTOR(79 DOWNTO 0) := (others => '1');
CONSTANT GND_BUS                               		:  STD_LOGIC_VECTOR(79 DOWNTO 0) := X"00000000000000000000";
CONSTANT HWMajorRelease                       	   :  STD_LOGIC_VECTOR(7 DOWNTO 0) := x"01"; 
	
SIGNAL   FIFOStartpointRAMDataQ                  	:  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   FIFOStartpointRAMRDAddr                 	:  STD_LOGIC_VECTOR(7 DOWNTO 0);

SIGNAL	sMDHighSpeedPort_In             				:  STD_LOGIC_VECTOR(gMDNumberOfHighSpeedTransmitterSlots*32-1 DOWNTO 0);
SIGNAL	sMDHighSpeedPort_Out           				:  STD_LOGIC_VECTOR(gMDNumberOfHighSpeedReceiverSlots*32-1 DOWNTO 0);
SIGNAL	sMDHighSpeedPort_NewDataReceivedPulse		:  STD_LOGIC_VECTOR(gMDNumberOfHighSpeedReceiverSlots-1 DOWNTO 0);

SIGNAL	FSP004_ModuleInterlocks_n  				 : STD_LOGIC_VECTOR(159 DOWNTO 0);

SIGNAL   FSP012_USIConfig                        :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   FSP012_NewDataAvailable                 :  STD_LOGIC;

SIGNAL   ReceivedFSP                             :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   FSP_Available                           :  STD_LOGIC_VECTOR(gModuleNumbersOfFSPs DOWNTO 0);
SIGNAL   FSP_ClockData                           :  STD_LOGIC;
SIGNAL   FSP_CRCOK                               :  STD_LOGIC;
SIGNAL   FSP_Data                                :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   FSP_ReadOnly                            :  STD_LOGIC_VECTOR(gModuleNumbersOfFSPs DOWNTO 0);
SIGNAL   FSP_RemainingBytes                      :  STD_LOGIC_VECTOR(11 DOWNTO 0);
SIGNAL   FSP_RWn                                 :  STD_LOGIC;
SIGNAL   FSP_ValueIsASCII                        :  STD_LOGIC;

BEGIN

----------------------------------------------------------------------------
b2v_inst_mUSIc_Shell : music_shell
GENERIC MAP(
            gM25PFSPImgStartAddress                               => "000000000000000000000000",
            gM25PNoInitSearchRun                                  => '0',
            gMainClockInHz                                        => gMainClockInHz,
            gmCoreModuleNumber                                    => 1,
            gmCoreNumberOfFSPs                                    => gModuleNumbersOfFSPs,
            gmCoreTimeoutInSeconds                                => 1,
            gmCoreUseGenericModuleNumber                          => '1',
            gMDHighSpeedPort_ByteIndexForNewDataTransmittedPulse  => X"0000",
            gMDNumberOfHighSpeedReceiverSlots                     => gMDNumberOfHighSpeedReceiverSlots,
            gMDNumberOfHighSpeedTransmitterSlots                  => gMDNumberOfHighSpeedTransmitterSlots,
            gMDUse_HighSpeedPort_AcceptNewDataRisingEdge          => "0000", 
            gUSIRxFIFODepth                                       => 50,
            gUSITxFIFODepth                                       => 4,
            gUSIUseRAMForRxFIFO                                   => 0,
            gUSIUseRAMForTxFIFO                                   => 0
         )
PORT MAP(
         Clock                                     => Clock,
         Reset                                     => Reset,
         ModuleNumber                              => GND_BUS(3 DOWNTO 0),
         USIHighSpeedEnable                        => FSP012_USIConfig(7),
         SetUSIMode                                => FSP012_NewDataAvailable,
         BitRate                                   => FSP012_USIConfig(2 DOWNTO 0),
         SetNewBitRate                             => FSP012_NewDataAvailable,
         MDSelect_HighSpeedSlot                    => GND_BUS(1 DOWNTO 0), 
         MDHighSpeedPort_In                        => sMDHighSpeedPort_In,
         MDHighSpeedPort_AcceptNewDataRisingEdge   => GND_BUS(0 downto 0),
         MDHighSpeedSlotFreeWheel                  => GND_BUS(0),
         USIRxSerial                               => USIRxSerial,
         FIFOStartpointRAMRDAddr                   => FIFOStartpointRAMRDAddr,
         InputValueIsASCII                         => FSP_ValueIsASCII,
         RemainingBytes                            => FSP_RemainingBytes,
         FSPAvailable                              => FSP_Available,
         FSPReadOnly                               => FSP_ReadOnly,
         FSPImgUseSectorAddress                    => GND_BUS(0),
         FSPImgSectorAddress                       => GND_BUS(7 downto 0),
         FSPImgEraseAll                            => GND_BUS(0),
         FSPImgFSPNumber                           => GND_BUS(7 downto 0),
         FSPImgMakeImage                           => GND_BUS(0),
         FSPImgLoadImage                           => GND_BUS(0),
         FSPImgLoadImageAfterPowerUp               => GND_BUS(0),
         FROM_EPCS_DATA                            => GND_BUS(0),
         LED_Control                               => open,
         USIInHighSpeedMode                        => USIInHighSpeedMode,
         MDHighSpeedPort_Out                       => sMDHighSpeedPort_Out, 
         MDHighSpeedPort_NewDataReceivedPulse      => sMDHighSpeedPort_NewDataReceivedPulse,
         MDHighSpeedPort_NewDataTransmittedPulse   => open,
         USITxSerial                               => USITxSerial,
         USITxDriverEnable                         => open, 
         USIControlStatus_Tri                      => open,
         FIFOStartpointRAMDataQ                    => FIFOStartpointRAMDataQ,
         ReceivedFSP                               => ReceivedFSP,
         RWn                                       => FSP_RWn,
         Data                                      => FSP_Data,
         ClockData                                 => FSP_ClockData,
         CRCOK                                     => FSP_CRCOK,
         FSPImgReady                               => open,
         TO_EPCS_nCS                               => open,
         TO_EPCS_ASDI                              => open,
         TO_EPCS_DCLK                              => open
        );


----------------------------------------------------------------------------
b2v_inst_MDS : fsp_mds
GENERIC MAP(
            bDeviceMaxSpeed                  => 4,  -- this has to be set to 4 => 5Mbps or
			                                        --                       5 => 2Mbps or
			                                        --                       6 => 1Mbps or
			                                        --                       7 => 115,2Kbps
													-- I suggest to try with 4 and if the HW doesn't support, we reduce it to 5
            gAttrib_Bit7_nFac_Applic_Image   => '1',
            gFSPName                         => "FSP000_MDS",
            lMaxPower                        => 0,
            lProductID                       => 86,
            sDescription                     => "GE SIS100",
            wAttributes                      => 2,
            wModuleClass                     => 13,
            wModuleSubClass                  => 0,
            wUSI                             => 17,
            wVendorID                        => 1
           )
PORT MAP(
         Clock                   => Clock,
         Reset                   => Reset,
         ClockData               => FSP_ClockData,
         Address                 => ReceivedFSP,
         FIFOStartpointRAMDataQ  => FIFOStartpointRAMDataQ,
         lFWMinorRelease         => GND_BUS(15 DOWNTO 0),
         lHWMinorRelease         => GND_BUS(15 DOWNTO 0),
         sFWDate                 => GND_BUS(23 DOWNTO 0),
         sHWDate                 => GND_BUS(23 DOWNTO 0),
         sSerial                 => GND_BUS(47 DOWNTO 0),
         wFWMajorRelease         => GND_BUS(23 DOWNTO 16),
         wHWMajorRelease         => HWMajorRelease,
         OutputValueIsASCII      => FSP_ValueIsASCII,
         Available               => FSP_Available(0),
         ReadOnly                => FSP_ReadOnly(0),
         Data                    => FSP_Data,
         FIFOStartpointRAMRDAddr => FIFOStartpointRAMRDAddr,
         RemainingBytes          => FSP_RemainingBytes
        );
     
----------------------------------------------------------------------------
inst_FSP001 : fsp_input
GENERIC MAP(
            gFSPDepth   => 10,
            gFSPName    => "FSP001_ModuleStatus",
            gFSPNumber  => 1
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         InputFromPeripheral  => ModuleStatus,
         Address              => ReceivedFSP,
         ClockData            => FSP_ClockData,
         Data                 => FSP_Data,
         RemainingBytes       => FSP_RemainingBytes,
         Available            => FSP_Available(1),
         ReadOnly             => FSP_ReadOnly(1)
        );
		  
----------------------------------------------------------------------------
inst_FSP002 : fsp_input
GENERIC MAP(
            gFSPDepth   => 10,
            gFSPName    => "FSP002_ModuleWarnings",
            gFSPNumber  => 2
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         InputFromPeripheral  => ModuleWarnings,
         Address              => ReceivedFSP,
         ClockData            => FSP_ClockData,
         Data                 => FSP_Data,
         RemainingBytes       => FSP_RemainingBytes,
         Available            => FSP_Available(2),
         ReadOnly             => FSP_ReadOnly(2)
        );
		  
----------------------------------------------------------------------------     
FSP_Available(3) <= '0';
FSP_ReadOnly(3)  <= '0';

----------------------------------------------------------------------------
b2v_inst_FSP004 : fsp_input
GENERIC MAP(
            gFSPDepth   => 20,
            gFSPName    => "FSP004_ModuleInterlocks",
            gFSPNumber  => 4
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         InputFromPeripheral  => FSP004_ModuleInterlocks_n,
         Address              => ReceivedFSP,
         ClockData            => FSP_ClockData,
         Data                 => FSP_Data,
         RemainingBytes       => FSP_RemainingBytes,
         Available            => FSP_Available(4),
         ReadOnly             => FSP_ReadOnly(4)
        );

---------------------------------------------------------------------------     
FSP_Available(11 downto 5) <= (others => '0');
FSP_ReadOnly(11 downto 5)  <= (others => '0');

----------------------------------------------------------------------------
b2v_inst_FSP12 : fsp_output
GENERIC MAP(
            gFSPDepth               => 1,
            gFSPName                => "FSP012_USIConfig",
            gFSPNumber              => 12,
            gReset                  => X"00",
            gUseInputFromPeripheral => '0'
          )
PORT MAP(
         Clock                   => Clock,
         Reset                   => Reset,
         Address                 => ReceivedFSP,
         RWn                     => FSP_RWn,
         CRCOK                   => FSP_CRCOK,
         InputFromPeripheral     => GND_BUS(7 downto 0),
         LoadDataFromPeripheral  => GND_BUS(0),
         ClockData               => FSP_ClockData,
         Data                    => FSP_Data,
         NewDataAvailable        => FSP012_NewDataAvailable,
         OutputToPeripheral      => FSP012_USIConfig,
         RemainingBytes          => FSP_RemainingBytes,
         Available               => FSP_Available(12),
         ReadOnly                => FSP_ReadOnly(12)
        );

----------------------------------------------------------------------------     
FSP_Available(59 downto 13) <= (others => '0');
FSP_ReadOnly(59 downto 13)  <= (others => '0');

----------------------------------------------------------------------------
inst_FSP60 : fsp_input
GENERIC MAP(
            gFSPDepth         => 2,
            gFSPName          => "FSP060_CurrentValue1",
            gFSPNumber    		=> 60
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         ClockData            => FSP_ClockData,
         Address              => ReceivedFSP,
         InputFromPeripheral  => CurrentValue1,
         Available            => FSP_Available(60),
         ReadOnly             => FSP_ReadOnly(60),
         Data                 => FSP_Data,
         RemainingBytes       => FSP_RemainingBytes
        );

----------------------------------------------------------------------------
inst_FSP61 : fsp_input
GENERIC MAP(
            gFSPDepth         => 2,
            gFSPName          => "FSP061_CurrentValue2",
            gFSPNumber  	   => 61
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         ClockData            => FSP_ClockData,
         Address              => ReceivedFSP,
         InputFromPeripheral  => CurrentValue2,
         Available            => FSP_Available(61),
         ReadOnly             => FSP_ReadOnly(61),
         Data                 => FSP_Data,
         RemainingBytes       => FSP_RemainingBytes
        );

----------------------------------------------------------------------------
inst_FSP62 : fsp_input
GENERIC MAP(
            gFSPDepth         => 2,
            gFSPName          => "FSP062_CurrentValue3",
            gFSPNumber    		=> 62
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         ClockData            => FSP_ClockData,
         Address              => ReceivedFSP,
         InputFromPeripheral  => CurrentValue3,
         Available            => FSP_Available(62),
         ReadOnly             => FSP_ReadOnly(62),
         Data                 => FSP_Data,
         RemainingBytes       => FSP_RemainingBytes
        );

----------------------------------------------------------------------------
inst_FSP63 : fsp_input
GENERIC MAP(
            gFSPDepth         => 2,
            gFSPName          => "FSP063_CurrentValue4",
            gFSPNumber    		=> 63
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         ClockData            => FSP_ClockData,
         Address              => ReceivedFSP,
         InputFromPeripheral  => CurrentValue4,
         Available            => FSP_Available(63),
         ReadOnly             => FSP_ReadOnly(63),
         Data                 => FSP_Data,
         RemainingBytes       => FSP_RemainingBytes
        );

----------------------------------------------------------------------------
inst_FSP64 : fsp_input
GENERIC MAP(
            gFSPDepth         => 2,
            gFSPName          => "FSP064_CurrentValue5",
            gFSPNumber 			=> 64
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         ClockData            => FSP_ClockData,
         Address              => ReceivedFSP,
         InputFromPeripheral  => CurrentValue5,
         Available            => FSP_Available(64),
         ReadOnly             => FSP_ReadOnly(64),
         Data                 => FSP_Data,
         RemainingBytes       => FSP_RemainingBytes
        );

----------------------------------------------------------------------------
inst_FSP65 : fsp_input
GENERIC MAP(
            gFSPDepth         => 2,
            gFSPName          => "FSP065_CurrentValue6",
            gFSPNumber   		=> 65
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         ClockData            => FSP_ClockData,
         Address              => ReceivedFSP,
         InputFromPeripheral  => CurrentValue6,
         Available            => FSP_Available(65),
         ReadOnly             => FSP_ReadOnly(65),
         Data                 => FSP_Data,
         RemainingBytes       => FSP_RemainingBytes
        );

----------------------------------------------------------------------------
inst_FSP66 : fsp_input
GENERIC MAP(
            gFSPDepth         => 2,
            gFSPName          => "FSP066_CurrentValue7",
            gFSPNumber   		=> 66
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         ClockData            => FSP_ClockData,
         Address              => ReceivedFSP,
         InputFromPeripheral  => CurrentValue7,
         Available            => FSP_Available(66),
         ReadOnly             => FSP_ReadOnly(66),
         Data                 => FSP_Data,
         RemainingBytes       => FSP_RemainingBytes
        );
		  
----------------------------------------------------------------------------
inst_FSP67 : fsp_output
GENERIC MAP(
            gFSPDepth               => 1,
            gFSPName                => "FSP067_AnalogValSelector",
            gFSPNumber              => 67,
            gReset                  => x"00",
            gUseInputFromPeripheral => '0'
            )
PORT MAP(
         Clock                   => Clock,
         Reset                   => Reset,
         RWn                     => FSP_RWn,
         CRCOK                   => FSP_CRCOK,
         LoadDataFromPeripheral  => GND_BUS(0),
         ClockData               => FSP_ClockData,
         Address                 => ReceivedFSP,
         Data                    => FSP_Data,
         InputFromPeripheral     => GND_BUS(7 downto 0),
         Available               => FSP_Available(67),
         ReadOnly                => FSP_ReadOnly(67),
         OutputToPeripheral      => AnalogValSelector, 
         RemainingBytes          => FSP_RemainingBytes
        );

----------------------------------------------------------------------------
inst_FSP68 : fsp_output
GENERIC MAP(
            gFSPDepth               => 3,
            gFSPName                => "FSP068_SetvalueToleranceFactor",
            gFSPNumber              => 68,
            gReset                  => x"000000",
            gUseInputFromPeripheral => '0'
            )
PORT MAP(
         Clock                   => Clock,
         Reset                   => Reset,
         RWn                     => FSP_RWn,
         CRCOK                   => FSP_CRCOK,
         LoadDataFromPeripheral  => GND_BUS(0),
         ClockData               => FSP_ClockData,
         Address                 => ReceivedFSP,
         Data                    => FSP_Data,
         InputFromPeripheral     => GND_BUS(23 downto 0),
         Available               => FSP_Available(68),
         ReadOnly                => FSP_ReadOnly(68),
         OutputToPeripheral      => SetvalueToleranceFactor, 
         RemainingBytes          => FSP_RemainingBytes
        );

----------------------------------------------------------------------------
-- Command Latch process
----------------------------------------------------------------------------

Cmd_Latch : process(Clock, Reset)
begin
	if(Reset = '1') then
		Command		<= (others => '0');
	elsif(rising_edge(Clock)) then
		if(sMDHighSpeedPort_NewDataReceivedPulse = "1" and sMDHighSpeedPort_Out(3 downto 0) /= GND_BUS(3 downto 0)) then
			Command	<= GND_BUS(3 downto 0) & sMDHighSpeedPort_Out(3 downto 0);
		end if;
	end if;
end process;
----------------------------------------------------------------------------
--Direct Instatiation
----------------------------------------------------------------------------
sMDHighSpeedPort_In				<= Measurements & Status;
FSP004_ModuleInterlocks_n 		<= ModuleInterlocks_n & VCC_BUS(79 downto 0);
USIConfig							<= FSP012_USIConfig;


END a;