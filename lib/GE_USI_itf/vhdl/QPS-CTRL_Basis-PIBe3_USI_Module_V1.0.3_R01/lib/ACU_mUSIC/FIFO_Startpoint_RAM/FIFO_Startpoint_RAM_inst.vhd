FIFO_Startpoint_RAM_inst : FIFO_Startpoint_RAM PORT MAP (
      clock  => clock_sig,
      data   => data_sig,
      rdaddress    => rdaddress_sig,
      wraddress    => wraddress_sig,
      wren   => wren_sig,
      q   => q_sig
   );
