LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
--USE IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : FSP_Input                                                                                       *
--*                                                                                                               *
--* Beschreibung: Dieses Modul stellt einen Eingangs FSP (Fifo-Start-Punkt) zur Verfuegung.                       *
--*               Sollen Daten an die Peripherie ausgegeben werden -> FSP_Output verwenden.                       *
--*               Dieser FSP ist mittels Generics konfigurierbar im Hinblick auf dessen Tiefe und Nummer.         *
--*               Die Tiefe ist dabei ein Vielfaches von Bytes.                                                   *
--*               FSP_Input ist ein parallel FSP, soll heissen, alle Datenbit sollen zeitgleich am Eingang liegen.*
--*               Sollen Daten hingegen seriell uebertragen werden -> FSP_Input/Output_Seriell verwenden.         *
--*               Die Daten innerhalb des FSP werden hexadezimal vorgehalten.                                     *
--*               Input FSPs sind nur lesbar.                                                                     *
--*                                                                                                               *
--* Version     : 18.05.10 Born (DS)                                                                              *
--*               31.05.10 'RemainingBytes' auf 12 Bit erweitert (DS)                                             *
--*               18.02.14 Meldung 'Ignored Power-Up Level option on the following registers'                     *
--*                        -> 'Critical Warning (18010): Register xxx will power up to GND/High'                  *
--*                        durch gezieltes setzen eines Initwertes der betroffen Variable(n) beseitigt. (DS)      *
--*               15.09.21 Generic ..Version entfernt (DS)                                                        *
--*                                                                                                               *
--* Logikelemente   : 68 (bei gFSPDepth:= 4)                                                                      *
--* Register        : 60                                                                                          *
--* Zyklen/Wandlung :                                                                                             *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************

-- Die Daten am FSPInterconnectionHandlers liegen stets als 8 Bit Hexadezimalwert an und werden innerhalb von
-- FSP_Input von der Groesse 'InputFromPeripherie' auf (gFSPDepth * 8 Bit) zerlegt.

-- Funktion:
-- Ist das Modul nicht adressiert, sind die Ports 'RemainingBytes'und 'Data' in Tristate, 'Available' und 'ReadOnly' = '0'.
-- Die am Port 'Address' angelegte 8-Bit Adresse wird mit 'gFSPNumber' verglichen. Ist diese stimmig, werden die Tristatports zugeschaltet. 
-- 'RemainingBytes' wird 'gFSPDepth', 'Available' und 'ReadOnly' werden '1', 
-- Der Leser setzt 'ClockData' kurz auf '1' und das Modul legt das MSByte des 'InputFromPeripheral' Ports 
-- an 'Data'. Der Leser wartet bis 'RemainingBytes' um 1 reduziert wird, dann kann er 'Data' lesen. Anschliessend setzt er 'ClockData' 
-- erneut kurz auf '1' sofern 'RemainingBytes' nicht Null ist und an 'Data' wird das naechste Byte ausgegeben.


entity FSP_Input is                                               -- Entity - Blockbeschreibung Ein- und Ausgaenge
   generic 
   (
      gFSPName             : string  := "FSP Name";               -- Manueller Eintrag des FSp Namen
      gFSPNumber           : integer range 16#FF# downto 1 := 1;  -- Nummer des FSP im Verbund (Konvergenz mit 'Address')
      gFSPDepth            : integer range 16#FFF# downto 1 := 4  -- Tiefe des FSP als Vielfaches von Byte
   );
----------------------------------------------------------------
   port 
   (
      Clock                      : in     std_logic;
      Reset                      : in     std_logic;

      Address                    : in     std_logic_vector (7 downto 0);   -- korreliert die angelegte Adresse mit gFSPNumber wird FSP aktiviert
      Data                       : out    std_logic_vector (7 downto 0);   -- Daten von/zum 'FSPInterconnectionHandler' 

      InputFromPeripheral        : in     std_logic_vector (((gFSPDepth * 8)-1) downto 0); -- Port von der Peripherie

      RemainingBytes             : out    std_logic_vector (11 downto 0);  -- Anzahl noch vorhandener Bytes beim lesen
      Available                  : out    std_logic;                       -- Wenn '1' signalisiert dies dem 'ICH', dass dieses FSP vorhanden ist
      ReadOnly                   : out    std_logic;                       -- Wird immer '1' wenn FSP selektiert, da dieser nur lesbar ist

      ClockData                  : in     std_logic                        -- Taktet einzelne Bytes aus dem FSP
   );
end FSP_Input;

architecture RTL of FSP_Input is

-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer  pFSP_Input
-------------------------------------------------------------------
  
   type     tState_FSP   is ( WAIT_FOR_ADDRESSING,
                              WAIT_FOR_CLOCK_DATA_HIGH,
                              HANDLE_DATA,
                              REDUCE_FSP_BYTE_COUNTER,
                              WAIT_FOR_DESELCT
                             );

   signal   sState_FSP : tState_FSP;

   subtype  drawer is std_logic_vector(7 downto 0);
   type     stack_array  is array(integer range gFSPDepth-1 downto 0) of drawer;
   signal   sDresser : stack_array;
   
   --Zugriff via sDresser(n)

   signal   sDataOut                   : std_logic_vector (7 downto 0);
   signal   sAddress                   : integer range 0 to 255 := 0;
   signal   sInputFromPeripheralTemp   : std_logic_vector (((gFSPDepth * 8)-1) downto 0);   
   signal   sInputFromPeripheral       : std_logic_vector (((gFSPDepth * 8)-1) downto 0);   
   signal   sEdgeDetArray              : std_logic_vector (1 downto 0);
   signal   sFSPSelected               : std_logic := '0';                    -- (internes Signale) wird '1' sobald die angelegte Adresse den FSP andressiert
   signal   sFSPByteCounter            : integer range 0 to gFSPDepth-1 := gFSPDepth-1;

   signal sRemainingBytes              : integer range 0 to gFSPDepth := gFSPDepth;

   constant cFSPByteCounter            : integer := gFSPDepth-1;

   begin

      pFSP : process (Clock, Reset)
      begin

         if (Reset = '1') then

            sState_FSP           <= WAIT_FOR_ADDRESSING ;                     -- Statemachine
            sAddress             <= 0;                                        -- FSP Nummer
            sFSPByteCounter      <= cFSPByteCounter;                          -- Zaehler fuer Anzahl im stack_array eingetragender Bytes (da MSB first => gFSPDepth-1)
            sEdgeDetArray        <= (others => '0');                          -- dient der Flankenerkennung von 'ClockData' 
            sRemainingBytes      <= gFSPDepth;                                -- Zaehler fuer noch auslesbare Bytes
            sInputFromPeripheral <= (others => '0');                          -- Eingabelatch fuer Paralleldaten
            sDataOut             <= (others => '0');
         else
            if (Clock'event and Clock = '1') then 

               sAddress          <= to_integer(unsigned(Address));            -- angelegte Adresse lesen

               if (sAddress /= gFSPNumber) then                               -- falsche Adresse -> verharren
                  sState_FSP        <= WAIT_FOR_ADDRESSING;
                  sFSPByteCounter   <= cFSPByteCounter;                       -- Bytezaehler auf maximalen Wert
                  sRemainingBytes   <= gFSPDepth; 
                  sFSPSelected      <= '0';
               else

                  case (sState_FSP) is

                     when WAIT_FOR_ADDRESSING =>

                        if (sAddress = gFSPNumber) then                          -- Wenn FSP adressiert, dann:
                           sInputFromPeripheral <= InputFromPeripheral;          -- Schnappschuss des Eingangsports
                           sFSPSelected         <= '1';                          -- Selectinfo auf '1'
                           sState_FSP           <= WAIT_FOR_CLOCK_DATA_HIGH;     -- in naechsten State
                        end if;

                     when WAIT_FOR_CLOCK_DATA_HIGH =>

                        sEdgeDetArray  <= (sEdgeDetArray(0) & ClockData);     -- Flankenerkennung

                        if (sEdgeDetArray = "01") then                        -- steigende Flanke an ClockData erkannt 
                           sState_FSP <= HANDLE_DATA;
                        else
                           sState_FSP <= WAIT_FOR_CLOCK_DATA_HIGH;            -- FSP noch adressiert, also warten
                        end if;

                     when HANDLE_DATA =>

                        sRemainingBytes      <= sRemainingBytes - 1;
                        sDataOut(7 downto 0) <= sDresser(sFSPByteCounter);    -- Daten interner Bus an externen Bus
                        sState_FSP           <= REDUCE_FSP_BYTE_COUNTER;
   
                     when REDUCE_FSP_BYTE_COUNTER =>

                        if (sFSPByteCounter > 0) then
                           sFSPByteCounter <= sFSPByteCounter - 1;
                           sState_FSP      <= WAIT_FOR_CLOCK_DATA_HIGH;       -- weitere Bytes ausgeben
                        else
                           sState_FSP      <= WAIT_FOR_DESELCT;               -- alle Bytes ausgegeben -> auf Deselektion warten
                        end if;

                     when WAIT_FOR_DESELCT =>

                        if (sAddress = gFSPNumber) then
                           sState_FSP <= WAIT_FOR_DESELCT;
                        else 
                           sState_FSP <= WAIT_FOR_ADDRESSING;
                        end if;                        

                     when others => 
                        sState_FSP <= WAIT_FOR_ADDRESSING;

                  end case;
               end if;
            end if ;
         end if ;

      end process pFSP;


--------------------------------------------------------------------
--Parallel nach Array Wandlung
--------------------------------------------------------------------      

      parallel_to_parallel_gen : for i in 0 to gFSPDepth-1 generate
         sDresser(i) <= sInputFromPeripheral(((i * 8)+7) downto (i * 8));
      end generate;

--------------------------------------------------------------------
--Diese Ausgaben sind u.a. TriState faehig 
--------------------------------------------------------------------
   pOutputMapping: process(sFSPSelected, sRemainingBytes, sDataOut )
   begin

      if (sFSPSelected = '1') then

         Data           <= sDataOut;
         RemainingBytes <= std_logic_vector(to_unsigned(sRemainingBytes, 12));
         Available     <= '1';
         ReadOnly      <= '1';

      else

         Data           <= (others => 'Z');
         RemainingBytes <= (others => 'Z');
         Available     <= '0';
         ReadOnly      <= '0';

      end if;

   end process pOutputMapping;

end RTL;
