-- Copyright (C) 1991-2009 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "05/07/2010 10:20:07"
                                                            
-- Vhdl Test Bench template for design  :  mUSIC_Core
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY mUSIC_Core_vhd_tst IS
END mUSIC_Core_vhd_tst;
ARCHITECTURE mUSIC_Core_arch OF mUSIC_Core_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL BaudRateChangeDonePulse : STD_LOGIC;
SIGNAL BaudRateIn : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL BaudRateOut : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL BaudRateValid : STD_LOGIC;
SIGNAL Clock : STD_LOGIC;
SIGNAL ClockData : STD_LOGIC;
SIGNAL CRCOK : STD_LOGIC;
SIGNAL Data : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL Host_MD_Acknowledge_Out : STD_LOGIC;
SIGNAL Host_MD_RxReadEnable : STD_LOGIC;
SIGNAL Host_MD_Strobe_Out : STD_LOGIC;
SIGNAL Host_MD_Tunnel_Data_Out : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL Host_MD_TxData_Out : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL Host_MD_TxWriteEnable : STD_LOGIC;
SIGNAL InputValueIsASCII : STD_LOGIC;
SIGNAL MD_Host_Acknowledge_In : STD_LOGIC;
SIGNAL MD_Host_Strobe_In : STD_LOGIC;
SIGNAL MD_host_TunnelData_In : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL MD_USI_RxReadEnable : STD_LOGIC;
SIGNAL MD_USI_TxWriteEnable : STD_LOGIC;
SIGNAL nFSPAvailable : STD_LOGIC;
SIGNAL nFSPReadOnly : STD_LOGIC;
SIGNAL ReceivedFSP : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL ReceivedMA : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL ReceivedPID : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL RemainingBytes : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL Reset : STD_LOGIC;
SIGNAL RWn : STD_LOGIC;
SIGNAL SendAnswer : STD_LOGIC;
SIGNAL SetNewBaudRate : STD_LOGIC;
SIGNAL SetUSI : STD_LOGIC;
SIGNAL USI_RxData_In : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL USI_RxFIFOnEmpty : STD_LOGIC;
SIGNAL USI_TxFIFOFull : STD_LOGIC;
SIGNAL USI_TxFIFOnEmpty : STD_LOGIC;
SIGNAL USI_TxIdle : STD_LOGIC;
SIGNAL USIInHighSpeedMode : STD_LOGIC;
COMPONENT mUSIC_Core
	PORT (
	BaudRateChangeDonePulse : OUT STD_LOGIC;
	BaudRateIn : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
	BaudRateOut : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
	BaudRateValid : IN STD_LOGIC;
	Clock : IN STD_LOGIC;
	ClockData : OUT STD_LOGIC;
	CRCOK : OUT STD_LOGIC;
	Data : INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	Host_MD_Acknowledge_Out : OUT STD_LOGIC;
	Host_MD_RxReadEnable : OUT STD_LOGIC;
	Host_MD_Strobe_Out : OUT STD_LOGIC;
	Host_MD_Tunnel_Data_Out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	Host_MD_TxData_Out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	Host_MD_TxWriteEnable : OUT STD_LOGIC;
	InputValueIsASCII : IN STD_LOGIC;
	MD_Host_Acknowledge_In : IN STD_LOGIC;
	MD_Host_Strobe_In : IN STD_LOGIC;
	MD_host_TunnelData_In : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	MD_USI_RxReadEnable : IN STD_LOGIC;
	MD_USI_TxWriteEnable : IN STD_LOGIC;
	nFSPAvailable : IN STD_LOGIC;
	nFSPReadOnly : IN STD_LOGIC;
	ReceivedFSP : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	ReceivedMA : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	ReceivedPID : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	RemainingBytes : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	Reset : IN STD_LOGIC;
	RWn : OUT STD_LOGIC;
	SendAnswer : OUT STD_LOGIC;
	SetNewBaudRate : IN STD_LOGIC;
	SetUSI : OUT STD_LOGIC;
	USI_RxData_In : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	USI_RxFIFOnEmpty : IN STD_LOGIC;
	USI_TxFIFOFull : IN STD_LOGIC;
	USI_TxFIFOnEmpty : IN STD_LOGIC;
	USI_TxIdle : IN STD_LOGIC;
	USIInHighSpeedMode : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : mUSIC_Core
	PORT MAP (
-- list connections between master ports and signals
	BaudRateChangeDonePulse => BaudRateChangeDonePulse,
	BaudRateIn => BaudRateIn,
	BaudRateOut => BaudRateOut,
	BaudRateValid => BaudRateValid,
	Clock => Clock,
	ClockData => ClockData,
	CRCOK => CRCOK,
	Data => Data,
	Host_MD_Acknowledge_Out => Host_MD_Acknowledge_Out,
	Host_MD_RxReadEnable => Host_MD_RxReadEnable,
	Host_MD_Strobe_Out => Host_MD_Strobe_Out,
	Host_MD_Tunnel_Data_Out => Host_MD_Tunnel_Data_Out,
	Host_MD_TxData_Out => Host_MD_TxData_Out,
	Host_MD_TxWriteEnable => Host_MD_TxWriteEnable,
	InputValueIsASCII => InputValueIsASCII,
	MD_Host_Acknowledge_In => MD_Host_Acknowledge_In,
	MD_Host_Strobe_In => MD_Host_Strobe_In,
	MD_host_TunnelData_In => MD_host_TunnelData_In,
	MD_USI_RxReadEnable => MD_USI_RxReadEnable,
	MD_USI_TxWriteEnable => MD_USI_TxWriteEnable,
	nFSPAvailable => nFSPAvailable,
	nFSPReadOnly => nFSPReadOnly,
	ReceivedFSP => ReceivedFSP,
	ReceivedMA => ReceivedMA,
	ReceivedPID => ReceivedPID,
	RemainingBytes => RemainingBytes,
	Reset => Reset,
	RWn => RWn,
	SendAnswer => SendAnswer,
	SetNewBaudRate => SetNewBaudRate,
	SetUSI => SetUSI,
	USI_RxData_In => USI_RxData_In,
	USI_RxFIFOnEmpty => USI_RxFIFOnEmpty,
	USI_TxFIFOFull => USI_TxFIFOFull,
	USI_TxFIFOnEmpty => USI_TxFIFOnEmpty,
	USI_TxIdle => USI_TxIdle,
	USIInHighSpeedMode => USIInHighSpeedMode
	);
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list  
WAIT;                                                        
END PROCESS always;                                          
END mUSIC_Core_arch;
