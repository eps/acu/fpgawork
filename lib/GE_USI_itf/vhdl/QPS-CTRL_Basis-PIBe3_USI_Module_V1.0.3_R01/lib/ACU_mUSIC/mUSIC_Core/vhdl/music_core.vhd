-- CREATED     "Mon Oct 07 11:20:05 2013"
-- REVISED     "Thu Sep 16 08:22:09 2021"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY mUSIC_Core IS 
GENERIC (
         gICHMainClockInHz            : INTEGER;
         gICHModuleNumber             : INTEGER;
         gICHNumberOfFSPs             : INTEGER;
         gICHTimeoutInSeconds         : INTEGER;
         gICHUseGenericModuleNumber   : STD_LOGIC
        );
   PORT
   (
      Clock                         :  IN    STD_LOGIC;
      Reset                         :  IN    STD_LOGIC;
      ModuleNumber                  :  IN    STD_LOGIC_VECTOR(3 DOWNTO 0);
      USIHighSpeedEnable            :  IN    STD_LOGIC;
      SetUSIMode                    :  IN    STD_LOGIC;
      BitRate                       :  IN    STD_LOGIC_VECTOR(2 DOWNTO 0);
      SetNewBitRate                 :  IN    STD_LOGIC;
      BitRateValid                  :  IN    STD_LOGIC;
      InputValueIsASCII             :  IN    STD_LOGIC;
      RemainingBytes                :  IN    STD_LOGIC_VECTOR(11 DOWNTO 0);
      FSPAvailable                  :  IN    STD_LOGIC_VECTOR(gICHNumberOfFSPs DOWNTO 0);
      FSPReadOnly                   :  IN    STD_LOGIC_VECTOR(gICHNumberOfFSPs DOWNTO 0);
      FIFOStartpointRAMRDAddr       :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
      USI_RxFIFOnEmpty              :  IN    STD_LOGIC;
      USI_RxData                    :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
      USI_RxFIFODataNew             :  IN    STD_LOGIC;
      USI_TxIdle                    :  IN    STD_LOGIC;
      USI_TxFIFOnEmpty              :  IN    STD_LOGIC;
      USI_TxFIFOFull                :  IN    STD_LOGIC;
      USI_TxFIFODataFetched         :  IN    STD_LOGIC;
      MD_USI_TxGetDataFromTxData    :  IN    STD_LOGIC;
      MD_USI_RxPutDataToRxData      :  IN    STD_LOGIC;
      MD_Host_TunnelData            :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
      MD_Host_Strobe                :  IN    STD_LOGIC;
      MD_Host_Acknowledge           :  IN    STD_LOGIC;
      FSPImgMakeImage               :  IN    STD_LOGIC;
      FSPImgFSPNumber               :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
      FSPImgLoadImage               :  IN    STD_LOGIC;
      FSPImgLoadImageAfterPowerUp   :  IN    STD_LOGIC;
      FSPImgDataIn                  :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
      FSPImgReadDone                :  IN    STD_LOGIC;
      FSPImgBusy                    :  IN    STD_LOGIC;
      FSPImgReady                   :  IN    STD_LOGIC;
      USIInHighSpeedMode            :  OUT   STD_LOGIC;
      CurrentBitRate                :  OUT   STD_LOGIC_VECTOR(2 DOWNTO 0);
      SetUSI                        :  OUT   STD_LOGIC;
      BitRateChangeDonePulse        :  OUT   STD_LOGIC;
      ReceivedPID                   :  OUT   STD_LOGIC_VECTOR(15 DOWNTO 0);
      ReceivedMA                    :  OUT   STD_LOGIC_VECTOR(3 DOWNTO 0);
      ReceivedFSP                   :  OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
      RWn                           :  OUT   STD_LOGIC;
      SendAnswer                    :  OUT   STD_LOGIC;
      Data                          :  INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      ClockData                     :  OUT   STD_LOGIC;
      CRCOK                         :  OUT   STD_LOGIC;
      FIFOStartpointRAMDataQ        :  OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);
      Host_MD_TunnelData            :  OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
      Host_MD_Strobe                :  OUT   STD_LOGIC;
      Host_MD_Acknowledge           :  OUT   STD_LOGIC;
      Host_MD_RxReadEnable          :  OUT   STD_LOGIC;
      Host_MD_TxWriteEnable         :  OUT   STD_LOGIC;
      Host_MD_TxData                :  OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
      FSPImgWriteEnable             :  OUT   STD_LOGIC;
      FSPImgWriteData               :  OUT   STD_LOGIC;
      FSPImgReadEnable              :  OUT   STD_LOGIC;
      FSPImgReadData                :  OUT   STD_LOGIC;
      FSPImgDataOut                 :  OUT   STD_LOGIC_VECTOR(7 DOWNTO 0)
   );
END mUSIC_Core;

ARCHITECTURE bdf_type OF mUSIC_Core IS 

COMPONENT ascii2hex_decoder
   PORT(
         Clock          : IN STD_LOGIC;      
         Reset          : IN STD_LOGIC;
         ASCII          : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         StartConvert   : IN STD_LOGIC;
         Hex            : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
         ConversionDone : OUT STD_LOGIC;
         Error          : OUT STD_LOGIC
       );
END COMPONENT;

COMPONENT autofallback
GENERIC (
         gCounts : INTEGER
        );
   PORT(
         Clock                   : IN STD_LOGIC;
         Reset                   : IN STD_LOGIC;
         Enable                  : IN STD_LOGIC;
         TriggerIncrementCounter : IN STD_LOGIC;
         TriggerResetCounter     : IN STD_LOGIC;
         SetUSIToDefaults        : OUT STD_LOGIC
       );
END COMPONENT;

COMPONENT bitratecontrol
GENERIC (
         gDefaultBitRate : STD_LOGIC_VECTOR(2 DOWNTO 0)
        );
   PORT(
         Clock                   : IN STD_LOGIC;
         Reset                   : IN STD_LOGIC;
         BitRate                 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
         SetNewBitRate           : IN STD_LOGIC;
         BitRateValid            : IN STD_LOGIC;
         SetDefaultBitdRate      : IN STD_LOGIC;
         USI_TxIdle              : IN STD_LOGIC;
         USI_TxFIFOnEmpty        : IN STD_LOGIC;
         CurrentBitRate          : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
         SetUSI                  : OUT STD_LOGIC;
         BitRateChangeDonePulse  : OUT STD_LOGIC
       );
END COMPONENT;

COMPONENT fifo_startpoint_ram
   PORT(
         data        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
         wraddress   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         wren        : IN STD_LOGIC;
         rdaddress   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         clock       : IN STD_LOGIC;
         q           : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
       );
END COMPONENT;

COMPONENT fsp_interconnection
GENERIC (
         gMainClockInHz          : INTEGER;
         gModuleNumber           : INTEGER;
         gNumberOfFSPs           : INTEGER;
         gTimeoutInSeconds       : INTEGER;
         gUseGenericModuleNumber : STD_LOGIC
        );
   PORT(
         Clock                         : IN STD_LOGIC;
         Reset                         : IN STD_LOGIC;
         ModuleNumber                  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
         USIInHighSpeedMode            : IN STD_LOGIC;
         ReceivedData                  : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         NewDataAvailable              : IN STD_LOGIC;
         TransmitterFree               : IN STD_LOGIC;
         HexIn                         : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
         H2AConvDone                   : IN STD_LOGIC;
         InputValueIsASCII             : IN STD_LOGIC;
         ASCIIIn                       : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         A2HConvDone                   : IN STD_LOGIC;
         A2HError                      : IN STD_LOGIC;
         RemainingBytes                : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
         FSPAvailable                  : IN STD_LOGIC_VECTOR(gICHNumberOfFSPs DOWNTO 0);
         FSPReadOnly                   : IN STD_LOGIC_VECTOR(gICHNumberOfFSPs DOWNTO 0);
         FSPImgFSPNumber               : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgMakeImage               : IN STD_LOGIC;
         FSPImgLoadImage               : IN STD_LOGIC;
         FSPImgDataIn                  : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgBusy                    : IN STD_LOGIC;
         FSPImgReadDone                : IN STD_LOGIC;
         FSPImgReady                   : IN STD_LOGIC;
         FSPImgLoadImageAfterPowerUp   : IN STD_LOGIC;
         SendByteASCII                 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         NewDataTransferable           : OUT STD_LOGIC;
         FIFOStartpointRAMWREnable     : OUT STD_LOGIC;
         FIFOStartpointRAMWRAddr       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         FIFOStartpointRAMData         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
         ReceivedPID                   : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
         ReceivedMA                    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
         ReceivedFSP                   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         Error                         : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         RWn                           : OUT STD_LOGIC;
         ClockData                     : OUT STD_LOGIC;
         Data                          : INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         CRCOK                         : OUT STD_LOGIC;
         SendAnswer                    : OUT STD_LOGIC;
         ASCIIOut                      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         StartConvertA2H               : OUT STD_LOGIC;
         HexOut                        : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
         StartConvertH2A               : OUT STD_LOGIC;    
         FSPImgWriteEnable             : OUT STD_LOGIC;
         FSPImgWriteData               : OUT STD_LOGIC;
         FSPImgReadEnable              : OUT STD_LOGIC;
         FSPImgReadData                : OUT STD_LOGIC;
         FSPImgMakeLoadImageInProgress : OUT STD_LOGIC
       );
END COMPONENT;

COMPONENT hex2ascii_decoder
   PORT(
         Clock          : IN STD_LOGIC;
         Reset          : IN STD_LOGIC;
         Hex            : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
         StartConvert   : IN STD_LOGIC;
         ASCII          : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         ConversionDone : OUT STD_LOGIC
       );
END COMPONENT;

COMPONENT highspeedswitch
   PORT(
         Clock                   : IN STD_LOGIC;
         Reset                   : IN STD_LOGIC;
         USIHighSpeedEnable      : IN STD_LOGIC;
         SetUSIMode              : IN STD_LOGIC;
         SetUSIToDefaults        : IN STD_LOGIC;
         USI_TxIdle              : IN STD_LOGIC;
         USI_TxFIFOnEmpty        : IN STD_LOGIC;
         USIInHighSpeedMode      : OUT STD_LOGIC;
         ChangeDonePulse         : OUT STD_LOGIC
         );
END COMPONENT;

COMPONENT receivedata
   PORT(
         Clock                         : IN STD_LOGIC;
         Reset                         : IN STD_LOGIC;
         USIInHighSpeedMode            : IN STD_LOGIC;
         USI_RxData                    : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         USI_RxFIFODataNew             : IN STD_LOGIC;
         USI_RxFIFOnEmpty              : IN STD_LOGIC;
         MD_Host_TunnelData            : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         MD_Host_Strobe                : IN STD_LOGIC;
         FSPImgMakeLoadImageInProgress : IN STD_LOGIC;
         Host_MD_RxReadEnable          : OUT STD_LOGIC;
         Host_MD_Acknowledge           : OUT STD_LOGIC;
         ReceivedData                  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         NewDataAvailable              : OUT STD_LOGIC
       );
END COMPONENT;

COMPONENT transmitdata
   PORT(
         Clock                   : IN STD_LOGIC;
         Reset                   : IN STD_LOGIC;
         USIInHighSpeedMode      : IN STD_LOGIC;
         USI_TxFIFODataFetched   : IN STD_LOGIC;
         USI_TxFIFOFull          : IN STD_LOGIC;
         MD_Host_Acknowledge     : IN STD_LOGIC;
         SendByteASCII           : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         NewDataTransferable     : IN STD_LOGIC;
         Host_MD_TxData          : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         Host_MD_TunnelData      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         Host_MD_Strobe          : OUT STD_LOGIC;
         Host_MD_TxWriteEnable   : OUT STD_LOGIC;
         TransmitterFree         : OUT STD_LOGIC
       );
END COMPONENT;

SIGNAL   A2HConversionDone             :  STD_LOGIC;
SIGNAL   A2HError                      :  STD_LOGIC;
SIGNAL   ASCII_H2A_to_ICH              :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   ASCII_ICH_to_A2H              :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   FIFOStartpointRAMData         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   FIFOStartpointRAMWRAddr       :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   FIFOStartpointRAMWRenable     :  STD_LOGIC;
SIGNAL   FSPImgMakeLoadImageInProgress :  STD_LOGIC;
SIGNAL   H2AConversionDone             :  STD_LOGIC;
SIGNAL   Hex_A2H_to_ICH                :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL   Hex_ICH_to_H2A                :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL   NewDataAvailable              :  STD_LOGIC;
SIGNAL   NewDataTransferable           :  STD_LOGIC;
SIGNAL   ReceivedData                  :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   SendByteASCII                 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   SetUSIToDefaults              :  STD_LOGIC;
SIGNAL   StartConvertA2H               :  STD_LOGIC;
SIGNAL   StartConvertH2A               :  STD_LOGIC;
SIGNAL   TransmitterFree               :  STD_LOGIC;
SIGNAL   USIInHighSpeedMode_Wire       :  STD_LOGIC;


BEGIN 


FSPImgDataOut <= SendByteASCII;



b2v_inst_ASCII2Hex_Decoder : ascii2hex_decoder
PORT MAP(
         Clock          => Clock,
         Reset          => Reset,
         StartConvert   => StartConvertA2H,
         ASCII          => ASCII_ICH_to_A2H,
         ConversionDone => A2HConversionDone,
         Error          => A2HError,
         Hex            => Hex_A2H_to_ICH
        );


b2v_inst_AutoFallBack : autofallback
GENERIC MAP(
            gCounts => 100
           ) 
PORT MAP(
         Clock                   => Clock,
         Reset                   => Reset,
         Enable                  => USIInHighSpeedMode_Wire,
         TriggerIncrementCounter => MD_USI_TxGetDataFromTxData,
         TriggerResetCounter     => MD_USI_RxPutDataToRxData,
         SetUSIToDefaults        => SetUSIToDefaults
        );


b2v_inst_BitRateControl : bitratecontrol
GENERIC MAP(
            gDefaultBitRate => "111"
           )
PORT MAP(
         Clock                   => Clock,
         Reset                   => Reset,
         BitRate                 => BitRate,
         SetNewBitRate           => SetNewBitRate,
         BitRateValid            => BitRateValid,
         SetDefaultBitdRate      => SetUSIToDefaults,
         USI_TxIdle              => USI_TxIdle,
         USI_TxFIFOnEmpty        => USI_TxFIFOnEmpty,
         CurrentBitRate          => CurrentBitRate,
         SetUSI                  => SetUSI,
         BitRateChangeDonePulse  => BitRateChangeDonePulse
        );


b2v_inst_FIFO_Startpoint_RAM : fifo_startpoint_ram
PORT MAP(
         wren        => FIFOStartpointRAMWRenable,
         clock       => Clock,
         data        => FIFOStartpointRAMData,
         rdaddress   => FIFOStartpointRAMRDAddr,
         wraddress   => FIFOStartpointRAMWRAddr,
         q           => FIFOStartpointRAMDataQ
        );


b2v_inst_FSP_Interconnectionhandler : fsp_interconnection
GENERIC MAP(
            gMainClockInHz          => gICHMainClockInHz,
            gModuleNumber           => gICHModuleNumber,
            gNumberOfFSPs           => gICHNumberOfFSPs,
            gTimeoutInSeconds       => gICHTimeoutInSeconds,
            gUseGenericModuleNumber => gICHUseGenericModuleNumber
           )
PORT MAP(
         Clock                         => Clock,
         Reset                         => Reset,
         ModuleNumber                  => ModuleNumber,
         USIInHighSpeedMode            => USIInHighSpeedMode_Wire,
         ReceivedData                  => ReceivedData,
         NewDataAvailable              => NewDataAvailable,
         TransmitterFree               => TransmitterFree,
         HexIn                         => Hex_A2H_to_ICH,
         H2AConvDone                   => H2AConversionDone,
         InputValueIsASCII             => InputValueIsASCII,
         ASCIIIn                       => ASCII_H2A_to_ICH,
         A2HConvDone                   => A2HConversionDone,
         A2HError                      => A2HError,
         RemainingBytes                => RemainingBytes,
         FSPAvailable                  => FSPAvailable,
         FSPReadOnly                   => FSPReadOnly,               
         FSPImgMakeImage               => FSPImgMakeImage,
         FSPImgBusy                    => FSPImgBusy,
         FSPImgReady                   => FSPImgReady,
         FSPImgLoadImage               => FSPImgLoadImage,
         FSPImgLoadImageAfterPowerUp   => FSPImgLoadImageAfterPowerUp,
         FSPImgReadDone                => FSPImgReadDone,
         SendByteASCII                 => SendByteASCII,
         NewDataTransferable           => NewDataTransferable,
         FIFOStartpointRAMWREnable     => FIFOStartpointRAMWRenable,
         FIFOStartpointRAMWRAddr       => FIFOStartpointRAMWRAddr,
         FIFOStartpointRAMData         => FIFOStartpointRAMData,
         ReceivedPID                   => ReceivedPID,
         ReceivedMA                    => ReceivedMA,
         ReceivedFSP                   => ReceivedFSP,
         Error                         => open,
         RWn                           => RWn,
         ClockData                     => ClockData,
         Data                          => Data,
         CRCOK                         => CRCOK,
         SendAnswer                    => SendAnswer,
         ASCIIOut                      => ASCII_ICH_to_A2H,
         StartConvertA2H               => StartConvertA2H,
         HexOut                        => Hex_ICH_to_H2A,
         StartConvertH2A               => StartConvertH2A,     
         FSPImgDataIn                  => FSPImgDataIn,
         FSPImgFSPNumber               => FSPImgFSPNumber,
         FSPImgWriteEnable             => FSPImgWriteEnable,
         FSPImgWriteData               => FSPImgWriteData,
         FSPImgReadEnable              => FSPImgReadEnable,
         FSPImgReadData                => FSPImgReadData,
         FSPImgMakeLoadImageInProgress => FSPImgMakeLoadImageInProgress
        );


b2v_inst_Hex2ASCII_Decoder : hex2ascii_decoder
PORT MAP(
         Clock          => Clock,
         Reset          => Reset,
         StartConvert   => StartConvertH2A,
         Hex            => Hex_ICH_to_H2A,
         ConversionDone => H2AConversionDone,
         ASCII          => ASCII_H2A_to_ICH
        );


b2v_inst_HighSpeedSwitch : highspeedswitch
PORT MAP(
         Clock                   => Clock,
         Reset                   => Reset,
         USIHighSpeedEnable      => USIHighSpeedEnable,
         SetUSIMode              => SetUSIMode,
         SetUSIToDefaults        => SetUSIToDefaults,
         USI_TxIdle              => USI_TxIdle,
         USI_TxFIFOnEmpty        => USI_TxFIFOnEmpty,
         USIInHighSpeedMode      => USIInHighSpeedMode_Wire
        );


b2v_inst_ReceiveData : receivedata
PORT MAP(
         Clock                         => Clock,
         Reset                         => Reset,
         USIInHighSpeedMode            => USIInHighSpeedMode_Wire,
         USI_RxFIFODataNew             => USI_RxFIFODataNew,
         USI_RxFIFOnEmpty              => USI_RxFIFOnEmpty,
         MD_Host_Strobe                => MD_Host_Strobe,
         FSPImgMakeLoadImageInProgress => FSPImgMakeLoadImageInProgress,
         MD_Host_TunnelData            => MD_Host_TunnelData,
         USI_RxData                    => USI_RxData,
         Host_MD_RxReadEnable          => Host_MD_RxReadEnable,
         Host_MD_Acknowledge           => Host_MD_Acknowledge,
         NewDataAvailable              => NewDataAvailable,
         ReceivedData                  => ReceivedData
        );


b2v_inst_TransmitData : transmitdata
PORT MAP(
         Clock                   => Clock,
         Reset                   => Reset,
         USIInHighSpeedMode      => USIInHighSpeedMode_Wire,
         USI_TxFIFODataFetched   => USI_TxFIFODataFetched,
         USI_TxFIFOFull          => USI_TxFIFOFull,
         MD_Host_Acknowledge     => MD_Host_Acknowledge,
         NewDataTransferable     => NewDataTransferable,
         SendByteASCII           => SendByteASCII,
         Host_MD_Strobe          => Host_MD_Strobe,
         Host_MD_TxWriteEnable   => Host_MD_TxWriteEnable,
         TransmitterFree         => TransmitterFree,
         Host_MD_TunnelData      => Host_MD_TunnelData,
         Host_MD_TxData          => Host_MD_TxData
        );

USIInHighSpeedMode <= USIInHighSpeedMode_Wire;

END bdf_type;