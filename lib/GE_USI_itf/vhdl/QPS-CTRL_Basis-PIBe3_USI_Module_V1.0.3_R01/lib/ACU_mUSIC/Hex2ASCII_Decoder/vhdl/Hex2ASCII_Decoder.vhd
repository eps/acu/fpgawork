LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : Hex2ASCII_Decoder                                                                               *
--*                                                                                                               *
--* Beschreibung: Der Decoder wandelt die am Eingang Hex[4..0] angelegten Hexadezimalwerte durch das Setzen der   *
--*               der Leitung 'StartConvert' = '1' in einen ASCII Wert und gibt diesen am Ausgang ASCII[7..0] aus.*
--*               Das Ende der Wandlung wird mittels des Signals 'ConversionDone' = '1' angezeigt.                *
--*               Hexadezimale Eingangswerte sind  : 0x0...0xF                                                    *
--*               ASCII Ausgangswerte sind         : 0x30..0x39 und 0x41..0x46                                    *
--*                                                                                                               *
--* Version     : 18.05.10 Born (DS)                                                                              *
--*               15.09.21 Generic ..Version entfernt (DS)                                                        *
--*                                                                                                               *
--* Logikelemente   : 13                                                                                          *
--* Register        : 13                                                                                          *
--* Zyklen/Wandlung :  2                                                                                          *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************

entity Hex2ASCII_Decoder is                                       -- Entity - Blockbeschreibung Ein- und Ausgaenge
----------------------------------------------------------------
   port 
   (
      Clock                      : in  std_logic;
      Reset                      : in  std_logic;

      Hex                        : in  std_logic_vector (3 downto 0);   -- Eingang fuer Hexadezimalwert
      ASCII                      : out std_logic_vector (7 downto 0);   -- Ausgnag fuer gewandelten ASCII-Wert

      StartConvert               : in  std_logic;                       -- Startet die Wandluing von Hex->ASCII wenn '1'
      ConversionDone             : out std_logic                        -- Wenn '1' ist Wandlung beendet, am Ausgang ASCII[7..0] steht gueltiger Wert
   );
end Hex2ASCII_Decoder;

architecture RTL of Hex2ASCII_Decoder is

-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer  pConvHEX2ASCII
-------------------------------------------------------------------
   type     tHex2ASCII              is array (0 to 15) of std_logic_vector(7 downto 0);
   constant cHex2ASCII : tHex2ASCII := (X"30",X"31",X"32",X"33",X"34",X"35",X"36",X"37",X"38",X"39",X"41",X"42",X"43",X"44",X"45",X"46") ;   

   signal   sHex                    : std_logic_vector (3 downto 0);
   signal   sASCII                  : std_logic_vector (7 downto 0);

   signal   sConversionDone         : std_logic ;
   signal   sStartConvert           : std_logic;
   

   begin

      pConvHex2ASCII : process (Clock, Reset)
      begin

         if (Reset = '1') then

            sASCII                  <= (others => '0');
            sConversionDone         <= '0';
            sStartConvert           <= '0';

         else
            if (Clock'event and Clock = '1') then 

               sHex              <= Hex;
               sStartConvert     <= StartConvert;
               sConversionDone   <= '0';

               if (sStartConvert = '1') then                        -- StartConvert erkannt
                  sASCII(7 downto 0)   <= cHex2ASCII(to_integer(unsigned(sHex(3 downto 0)) ));
                  sConversionDone      <= '1';
               end if ;
            end if ;
         end if ;

      end process pConvHex2ASCII;

   --Ausgabe
   ASCII(7 downto 0) <= sASCII (7 downto 0);
   ConversionDone    <= sConversionDone;

end RTL;
