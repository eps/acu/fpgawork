LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : FSP_SerialOutput                                                                                *
--*                                                                                                               *
--* Beschreibung: Dieses Modul stellt einen Ausgangs FSP (Fifo-Start-Punkt) zur Verfuegung.                       *
--*               Sollen Daten von der Peripherie eingelesen werden -> FSP_Input verwenden.                       *
--*               Dieser FSP ist mittels Generics konfigurierbar im Hinblick auf dessen Tiefe und Nummer.         *
--*               Die Tiefe ist dabei ein Vielfaches von Bytes.                                                   *
--*               FSP_Output ist ein seriell FSP, soll die Daten werden Byteweise an den Ausgang gelegt           *
--*               Sollen Daten hingegen parallel uebertragen werden -> FSP_Input/Output verwenden.                *
--*               Die Daten innerhalb des FSP werden hexadezimal vorgehalten.                                     *
--*               Beim Ruecklesen werden nicht die Werta us dem FSp gelesen, sondern von der nachgeschalteten     *
--*               Logic angefordert (RWn = '0').                                                                  *
--*                                                                                                               *
--* Version     : 11.10.10 Born (DS)                                                                              *
--*               18.02.14 Meldung 'Ignored Power-Up Level option on the following registers'                     *
--*                        -> 'Critical Warning (18010): Register xxx will power up to GND/High'                  *
--*                        durch gezieltes setzen eines Initwertes der betroffen Variable(n) beseitigt. (DS)      *
--*               27.09.18 Das Generic 'gFSPDepth' ist auf max. 16#FFF# beschraenkt. Problem hierbei ist, dass    *
--*                        die max. FSP Tiefe beginnend von 1...n gezaehlt wird. "Remaining Bytes" selbst ist     *
--*                        nur 11 Bit breit und kann daher nur Werte von max. 4095..0 ausgeben. Ein 'gFSPDepth'   *
--*                        von 4096 ist also gar nicht moeglich, da immer von 'gFSPDepth' abwaerts bis 0 gezaehlt *
--*                        wird. Wobei bei 0 selbst KEIN weiterer Datenwert ausgegeben wird. D.h. die max. Tiefe  *
--*                        dieses FSP ist ohnehin auf 'gFSPDepth = 4095' beschraenkt. (DS)                        *
--*              02.02.22   Generic '..Version' entfernt (DS)                                                     *
--*                                                                                                               *
--* Logikelemente   :                                                                                             *
--* Register        :                                                                                             *
--* Zyklen/Wandlung :                                                                                             *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************

-- Die Daten seitens des FSPInterconnectionHandlers liegen stets als 8 Bit Hexadezimalwert an und werden innerhalb von
-- FSP_SerialOutput auf dieser Breite belassen.

-- Funktion:
-- Ist das Modul nicht adressiert, sind die Ports 'RemainingBytes' und 'Data' in Tristate, 'Available' und 'ReadOnly' = '0'.
-- Die am Port 'Address' angelegte 8-Bit Adresse wird mit gFSPNumber verglichen. Ist diese stimmig, werden die Tristatports zugeschaltet. 
-- 'InsistOn_NewData_Available' wird '0', 'RemainingBytes' wird 'gFSPDepth'. Abhaengig von 'RWn' passiert nun folgendes:

-- 1. 'RWn = '0'': der Sender legt ein Datenbyte an 'Data' und setzt 'ClockData ' kurzzeitig auf '1'. 
--    Das Modul liest 'Data', schiebt die angelegten Daten zum Ausgang 'OutputToPeripheral' und 
--    reduziert 'RemainingBytes' um 1 und 'InsistOn_NewData_Available' wird fuer einen Takt '1'.
--    Sofern 'RemainingBytes > 0', legt der Sender (FSP_Interconnection) das naechste Byte an 'Data' 
--    und setzt 'ClockData' erneut. Ist 'RemainingBytes = 0', fuehrt der Sender (FSP_Interconnection) den CRC durch 
--    und setzt bei Erfolg das 'CRCOK'.
--    Da die Daten aber schon quasi seriell ausgegeben wurden, wird 'CRCOK' nicht mehr ausgewertet.

-- 2. 'RWn = '1'': der Leser (FSP_Interconnection) setzt 'ClockData' kurz auf '1'. Darauf fordert das Modul mit einem
--     kurzen '1' - '0' an 'InsistOn_NewData_Available' ein neues Byte an der Peripherie an. Die Peripherie legt das Byte an
--     den Eingang 'InputFromPeripheral' und setzt den Eingang 'NewDataFromPeripheral' auf '1'. Das Modul reicht dieses Byte an 
--     an 'Data' weiter. 'RemainingBytes' wird um 1 reduziert. Der Leser (FSP_Interconnection) holt das Byte ab
--     und setzt 'ClockData' erneut kurz auf '1' sofern 'RemainingBytes' nicht Null ist.

-- Nach dem selektieren per 'Address' muss der Leser warten bis 'Available' = '1' ist, dann kann's losgehen.


entity FSP_SerialOutput is                                           -- Entity - Blockbeschreibung Ein- und Ausgaenge
   generic 
   (
      gFSPName                   : string  :="FSP Name";                  -- Manueller Eintrag des FSP Namen
      gFSPNumber                 : integer range 16#FF# downto 1  := 42;  -- Nummer des FSP im Verbund (Konvergenz mit 'Address')
      gFSPDepth                  : integer range 16#FFF# downto 1  := 256 -- legt 'RemainingBytes' fest
   );
----------------------------------------------------------------
   port 
   (
      Clock                      : in     std_logic;
      Reset                      : in     std_logic;

      Address                    : in     std_logic_vector (7 downto 0);   -- korreliert die angelegte Adresse mit gFSPNumber wird FSP aktiviert
      Data                       : inout  std_logic_vector (7 downto 0);   -- Daten von/zum 'FSPInterconnectionHandler'
      RWn                        : in     std_logic;                       -- '1'=FSP gibt seine Daten an FSP-Handler, '0'=FSP erwartet Daten vom FSP-Handler 
      CRCOK                      : in     std_logic;                       -- Wenn '1' ist die Datenpruefsumme OK
   
      InsistOn_NewData_Available : out    std_logic;                       -- [WR] '1'=neue Daten im FSP, [RD] '1'=neue Daten von Peripherie anfordern
      OutputToPeripheral         : out    std_logic_vector (7 downto 0);   -- Port zur Peripherie
      InputFromPeripheral        : in     std_logic_vector (7 downto 0);   -- Port von Peripherie
      NewDataFromPeripheral      : in     std_logic;                       -- Signal, das neue Daten an 'InputFromPeripheral' anliegen

      RemainingBytes             : out    std_logic_vector (11 downto 0);  -- Anzahl noch vorhandener Bytes beim lesen, bzw. noch schreibbarer Bytes
      Available                  : out    std_logic;                       -- Wenn '1' signalisiert dies dem 'ICH', dass dieses FSP vorhanden ist
      ReadOnly                   : out    std_logic;                       -- Bleibt '0' wenn FSP selektiert wird, da dieser les- und schreibbar ist

      ClockData                  : in     std_logic                        -- Taktet einzelne Bytes in/aus den/dem FSP (Abh. von RWn)
   );
end FSP_SerialOutput;

architecture RTL of FSP_SerialOutput is

-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer  pFSP_Output
-------------------------------------------------------------------
  
   type     tState_FSP   is ( WAIT_FOR_ADDRESSING,
                              WAIT_FOR_CLOCK_DATA_HIGH,
                              WAIT_FOR_BYTE_READY,
                              HANDLE_DATA,
                              REDUCE_FSP_BYTE_COUNTER,
                              WAIT_FOR_CRC_OK, 
                              WAIT_FOR_DESELCT
                             );

   signal   sState_FSP : tState_FSP;

   signal sDataOut                     : std_logic_vector (7 downto 0);
   signal sOutputToPeripheral          : std_logic_vector (7 downto 0);   
   signal sFSPSelected                 : std_logic := '0';                       --wird '1' sobald die angelegte Adresse den FSP adressiert
   signal sFSPByteCounter              : integer range 0 to gFSPDepth-1 := gFSPDepth-1;
   signal sRemainingBytes              : integer range 0 to gFSPDepth := gFSPDepth;
   signal sInsistOn_NewData_Available  : std_logic;
   
   signal sEdgeDetArray                : std_logic_vector (1 downto 0);

   constant cFSPByteCounter            : integer := gFSPDepth-1;

   begin

      pFSP : process (Clock, Reset)
      begin

         if (Reset = '1') then

            sState_FSP           <= WAIT_FOR_ADDRESSING ;                     -- Statemachine
            sFSPByteCounter      <= cFSPByteCounter;                          -- Zaehler fuer Anzahl im stack_array eingetragender Bytes (da MSByte first => gFSPDepth-1)
            sFSPSelected         <= '0';
            sRemainingBytes      <= gFSPDepth;                                -- Zaehler fuer noch eintragbare/auslesbare Bytes
            sOutputToPeripheral  <= (others => '0');                          -- Ausgabelatch fuer Paralleldaten
            sDataOut             <= (others => '0');
            sEdgeDetArray        <= (others => '0');                          -- dient der Flankenerkennung von 'ClockData' 
         else
            if (rising_edge(Clock)) then 
            
               if (to_integer(unsigned(Address)) /= gFSPNumber) then          -- falsche Adresse -> verharren
                  sState_FSP        <= WAIT_FOR_ADDRESSING;
                  sFSPByteCounter   <= cFSPByteCounter;                       -- Bytezaehler auf maximalen Wert
                  sRemainingBytes   <= gFSPDepth; 
                  sFSPSelected      <= '0';
               else

                  case (sState_FSP) is

                     ------------------------------------------------------------------
                     when WAIT_FOR_ADDRESSING =>

                        if (to_integer(unsigned(Address)) = gFSPNumber) then           -- Wenn FSP adressiert, dann:
                           sInsistOn_NewData_Available   <= '0';
                           sFSPSelected                  <= '1';                       -- Selectinfo auf '1'
                           sState_FSP                    <= WAIT_FOR_CLOCK_DATA_HIGH;  -- in naechsten State
                        end if;

                     ------------------------------------------------------------------
                     when WAIT_FOR_CLOCK_DATA_HIGH =>

                        sEdgeDetArray  <= (sEdgeDetArray(0) & ClockData);           -- Flankenerkennung

                        if (sEdgeDetArray = "01") then                              -- steigende Flanke an ClockData erkannt 
                           if (RWn = '1') then                                      -- Daten von FSPInterconnectionHandler in FSP schreiben
                              sInsistOn_NewData_Available   <= '1';                 -- Peripherie -> neuen Daten anfordern
                              sState_FSP                    <= WAIT_FOR_BYTE_READY;
                           else                                                     -- Daten in den FSPInterconnectionHandler vom FSP lesen
                              sOutputToPeripheral           <= Data(7 downto 0);    -- empf. FSP Daten an Peripherie
                              sInsistOn_NewData_Available   <= '1';                 -- Peripherie die neuen Daten mitteilen
                              sRemainingBytes               <= sRemainingBytes - 1; -- 'RemainingBytes' schnellstmoeglich reduzieren
                              sState_FSP                    <= REDUCE_FSP_BYTE_COUNTER;
                           end if;

                        else
                           sState_FSP <= WAIT_FOR_CLOCK_DATA_HIGH;                  -- FSP noch adressiert, also warten
                        end if;

                     ------------------------------------------------------------------
                     when WAIT_FOR_BYTE_READY =>                                    -- warten auf 'NewDataFromPeripheral' = '1'

                        sInsistOn_NewData_Available   <= '0';                       -- Mitteilung an Peripherie loeschen

                        if (NewDataFromPeripheral = '1') then
                           sDataOut(7 downto 0) <= InputFromPeripheral(7 downto 0); -- Peripheriedaten an externen zum ICH Bus
                           sRemainingBytes      <= sRemainingBytes - 1;             -- 'RemainingBytes' schnellstmoeglich reduzieren
                           sState_FSP           <= REDUCE_FSP_BYTE_COUNTER;
                        end if;

                     ------------------------------------------------------------------
                     when REDUCE_FSP_BYTE_COUNTER =>

                        sInsistOn_NewData_Available   <= '0';                    -- Mitteilung an Peripherie loeschen
                        
                        if (sFSPByteCounter > 0) then
                           sFSPByteCounter <= sFSPByteCounter - 1;
                           sState_FSP      <= WAIT_FOR_CLOCK_DATA_HIGH;          -- weitere Bytes empfangen/ausgeben
                        else
                           if (RWn = '1') then
                              sState_FSP      <= WAIT_FOR_DESELCT;               -- alle Bytes ausgegeben -> auf Deselektion warten
                           else
                              sState_FSP      <= WAIT_FOR_CRC_OK;                -- empf. Daten auswerten sofern RWn = '0'
                           end if;

                        end if;

                     ------------------------------------------------------------------
                     when WAIT_FOR_CRC_OK =>

                        if (CRCOK = '1') then                                    -- Highpuls CRCOK erkannt 
                           --sOutputToPeripheral  <= sOutputToPeripheralTemp;
                           --sNewDataAvailable    <= '1';
                           sState_FSP           <= WAIT_FOR_DESELCT;
                        else
                           sState_FSP <= WAIT_FOR_CRC_OK;                        
                        end if;

                     ------------------------------------------------------------------
                     when WAIT_FOR_DESELCT =>

                        if (to_integer(unsigned(Address)) = gFSPNumber) then
                           sState_FSP <= WAIT_FOR_DESELCT;
                        else 
                           sState_FSP <= WAIT_FOR_ADDRESSING;
                        end if;                        

                     ------------------------------------------------------------------
                     when others => 
                        sState_FSP <= WAIT_FOR_ADDRESSING;

                  end case;
               end if;
            end if ;
         end if ;

      end process pFSP;


--------------------------------------------------------------------
--Diese Ausgaben sind TriState faehig 
--------------------------------------------------------------------
   pOutputMapping: process(sFSPSelected, RWn, sRemainingBytes, sDataOut )
   begin

      if (sFSPSelected = '1') then

         if (RWn = '1') then
            Data     <= sDataOut;
         else
            Data     <= (others => 'Z');
         end if;

         RemainingBytes <= std_logic_vector(to_unsigned(sRemainingBytes, 12));
         Available      <= '1';

      else
      
         RemainingBytes    <= (others => 'Z');
         Available         <= '0';
         Data              <= (others => 'Z');

      end if;

   end process pOutputMapping;

   ReadOnly                      <= '0';
   OutputToPeripheral            <= sOutputToPeripheral;
   InsistOn_NewData_Available    <= sInsistOn_NewData_Available;

end RTL;
