LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
--USE IEEE.STD_LOGIC_UNSIGNED.ALL;
--USE ieee.std_logic_signed.all;
use ieee.numeric_std.all;

--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : FSP_Output                                                                                      *
--*                                                                                                               *
--* Beschreibung: Dieses Modul stellt einen Ausgangs FSP (Fifo-Start-Punkt) zur Verfuegung.                       *
--*               Sollen Daten von der Peripherie eingelesen werden -> FSP_Input verwenden.                       *
--*               Dieser FSP ist mittels Genrics konfigurierbar im Hinblick auf dessen Tiefe und Nummer.          *
--*               Die Tiefe ist dabei ein Vielfaches von Bytes.                                                   *
--*               FSP_Output ist ein parallel FSP, soll heissen, alle Datenbit liegen zeitgleich am Ausgang.      *
--*               Sollen Daten hingegen seriell uebertragen werden -> FSP_Input/Output_Seriell verwenden.         *
--*               Die Daten innerhalb des FSP werden hexadezimal vorgehalten.                                     *
--*               Die Werte innerhalb des FSP koennen Rueckgelesen werden mit RWn = '0'.                          *
--*                                                                                                               *
--* Version     : 18.05.10 Born (DS)                                                                              *
--*               31.05.10 'RemainingBytes' auf 12 Bit erweitert (DS)                                             *
--*               07.02.11 'gReset' eingefuehrt, dadurch kann dem Ausgabeport 'OutputToPeripheral' ein Startwert  *
--*                        zugewiesen werden. Dieser kann im beliebigen Zahlenformat (16# 10# 2#) festgelgt       *
--*                        werden. (DS)                                                                           *
--*               16.01.12 'sNewDataAvailable' wurde beim Start, bzw. Reset nicht initialisiert. Vermutlich wurde *
--*                        das Signal vom Compiler auf '0' gesetzt, dies ist nun sicher definiert. (DS)           *
--*               17.01.12 'InputFromPeripheral', 'LoadDataFromPeripheral' als neue Eingangsports hinzugefuegt.   *
--*                        Hierdurch ist es moeglich das FSP zur Laufzeit durch die Peripherie mit Daten zu       *
--*                        laden. (DS)                                                                            *
--*               dd.mm.yy 'sNewDataAvailable' von State 'WAIT_FOR_ADDRESSING' in den Write Zweig von             *
--*                        'WAIT_FOR_CLOCK_DATA_HIGH' verlegt. Grund: Wurden Daten aus dem FSP gelesen, wurde     *
--*                        allein schon durch adressieren des FSP 'NewDataAvailable' = '0'. Bei einem Lesezugriff *
--*                        sollte sich dieses Signal aber nicht aendern, vielmehr nur bei einem Schreibzugriff.   *
--*                        An der jetzigen Position geschieht dies unabhaengig davon ob dieser erfolgreich ist    *
--*                        oder nicht.                                                                            *
--*               18.02.14 Meldung 'Ignored Power-Up Level option on the following registers'                     *
--*                        -> 'Critical Warning (18010): Register xxx will power up to GND/High'                  *
--*                        durch gezieltes setzen eines Initwertes der betroffen Variable(n) beseitigt. (DS)      *  
--*               15.09.21 Generic ..Version entfernt (DS)                                                        *
--*                                                                                                               *
--*                                                                                                               *
--* Logikelemente   : 96 (bei 24 Bit FSP) mit gUseInputFromPeripheral = '0', bzw. 98 bei ...Peripheral = '1'      *
--* Register        : 78 mit gUseInputFromPeripheral = '0', bzw. 80 bei ...Peripheral = '1'                       *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************

-- Die Daten seitens des FSPInterconnectionHandlers liegen stets als 8 Bit Hexadezimalwert an und werden innerhalb von
-- FSP_Output auf dessen FSP Tiefe gelegt.

-- Funktion:
-- Ist das Modul nicht adressiert, sind die Ports 'RemainingBytes' und 'Data' in Tristate, 'Available' und 'ReadOnly' = '0'.
-- Die am Port 'Address' angelegte 8-Bit Adresse wird mit gFSPNumber verglichen. Ist diese stimmig, werden die Tristatports zugeschaltet. 
-- 'NewDataAvailable' wird '0', 'RemainingBytes' wird 'gFSPDepth'. Abhaengig von 'RWn' passiert nun folgendes:
-- 1. 'RWn = '0'': der Sender legt ein Datenbyte an 'Data' und setzt 'ClockData ' kurzzeitig auf '1'. 
--    Das Modul liest 'Data', schiebt die angelegten Daten in ein Array mit der Tiefe 'gFSPDepth-1' und 
--    reduziert 'RemainingBytes' um 1. Sofern 'RemainingBytes > 0', legt der Sender das naechste Byte an 'Data' 
--    und setzt 'ClockData' erneut. Ist 'RemainingBytes = 0', fuehrt der Sender den CRC durch und setzt bei Erfolg 
--    das 'CRCOK'. Daraufhin legt das Modul die empf. Datenbytes als 'gFSPDepth  - Bytes' langes Datenwort 
--    an 'OutputToPeripheral' und setzt 'NewDataAvailable'.
-- 2. 'RWn = '1'': der Leser setzt 'ClockData' kurz auf '1' und das Modul legt das MSB des 'OutputToPeripheral' Ports 
--    an 'Data'. 'RemainingBytes' wird um 1 reduziert. Der Leser setzt 'ClockData' erneut kurz auf '1' sofern 'RemainingBytes'
--    nicht Null ist.

-- Nach dem selektieren per 'Address' muss der Leser warten bis 'Available' = '1' ist, dann kann's losgehen.

-- Voellig unabhaengig von obigen Funktionen koennen ab Version 0.5 mit den Ports 'InputFromPeripheral' und 'LoadDataFromPeripheral'
-- Daten aus der Peripherie das FSP zur Laufzeit fuellen. Dies ist vor allem da notwendig, wo z.B. Korrekturwerte in einem
-- FSP vorgehalten werden sollen (Offset, Gain...) die modulintern aus einem Speicher gelesen werden und das FSP fuellen.

entity FSP_Output is                                                 -- Entity - Blockbeschreibung Ein- und Ausgaenge
   generic 
   (
      gFSPName                : string  :="FSP Name";                   -- Manueller Eintrag des FSP Namen
      gFSPNumber              : integer range 16#FF# downto 1  := 1;    -- Nummer des FSP im Verbund (Konvergenz mit 'Address')
      gFSPDepth               : integer range 16#FFF# downto 0 := 3;    -- Tiefe des FSP als Vielfaches von Byte
      gUseInputFromPeripheral : std_logic := '0';                       -- Wenn '0' werden 'InputFromPeripheral' und 'LoadDataFromPeripheral' ignoriert
      gReset                  : std_logic_vector := X"000000"           -- Standard-Ausgabewert fuer 'OutputToPeripheral' nach dem 'Reset'
   );
----------------------------------------------------------------
   port 
   (
      Clock                      : in     std_logic;
      Reset                      : in     std_logic;

      Address                    : in     std_logic_vector (7 downto 0);   -- korreliert die angelegte Adresse mit gFSPNumber wird FSP aktiviert
      Data                       : inout  std_logic_vector (7 downto 0);   -- Daten von/zum 'FSPInterconnectionHandler'
      RWn                        : in     std_logic;                       -- '1'=FSP gibt seine Daten an FSP-Handler, '0'=FSP erwartet Daten vom FSP-Handler 
      CRCOK                      : in     std_logic;                       -- Wenn '1' ist die Datenpruefsumme OK, Daten an OutputToPeripheral

      InputFromPeripheral        : in     std_logic_vector (((gFSPDepth * 8)-1) downto 0); -- Port von der Peripherie
      LoadDataFromPeripheral     : in     std_logic;                       -- Ladeeingang -> Highflanke laedt Daten von InputFromPeripheral

      NewDataAvailable           : out    std_logic;                       -- '1'=neue Daten im FSP
      OutputToPeripheral         : out    std_logic_vector (((gFSPDepth * 8)-1) downto 0); -- Port zur Peripherie

      RemainingBytes             : out    std_logic_vector (11 downto 0);  -- Anzahl noch vorhandener Bytes beim lesen, bzw. noch schreibbarer Bytes
      Available                  : out    std_logic;                       -- Wenn '1' signalisiert dies dem 'ICH', dass dieses FSP vorhanden ist
      ReadOnly                   : out    std_logic;                       -- Bleibt '0' wenn FSP selektiert wird, da dieser les- und schreibbar ist

      ClockData                  : in     std_logic                        -- Taktet einzelne Bytes in/aus den/dem FSP (Abh. von RWn)
   );
end FSP_Output;

architecture RTL of FSP_Output is

-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer  pFSP_Output
-------------------------------------------------------------------
  
   type     tState_FSP   is ( WAIT_FOR_ADDRESSING,
                              WAIT_FOR_CLOCK_DATA_HIGH,
                              HANDLE_DATA,
                              REDUCE_FSP_BYTE_COUNTER,
                              WAIT_FOR_CRC_OK, 
                              WAIT_FOR_DESELECTION
                             );

   signal   sState_FSP : tState_FSP;

   subtype  drawer is std_logic_vector(7 downto 0);
   type     stack_array  is array(integer range gFSPDepth-1 downto 0) of drawer;
   signal   sDresser : stack_array;

   --Zugriff via sDresser(n)

   signal sDataOut                  : std_logic_vector (7 downto 0);
   signal sAddress                  : integer range 0 to 255 := 0;
   signal sOutputToPeripheralTemp   : std_logic_vector (((gFSPDepth * 8)-1) downto 0);   
   signal sOutputToPeripheral       : std_logic_vector (((gFSPDepth * 8)-1) downto 0);   
   signal sClockData                : std_logic;
   signal sCRCOK                    : std_logic;
   signal sFSPSelected              : std_logic := '0';                       --wird '1' sobald die angelegte Adresse den FSP andressiert
   signal sFSPByteCounter           : integer range 0 to gFSPDepth-1 := gFSPDepth-1;
   signal sRemainingBytes           : integer range 0 to gFSPDepth := gFSPDepth;
   signal sNewDataAvailable         : std_logic := '0';
   
   signal sEdgeDetArray             : std_logic_vector (1 downto 0);

   signal sLoadEdgeDetArray         : std_logic_vector (1 downto 0);

   constant cFSPByteCounter         : integer := gFSPDepth-1;

   begin

      pFSP : process (Clock, Reset)
      begin

         if (Reset = '1') then

            sState_FSP              <= WAIT_FOR_ADDRESSING ;                  -- Statemachine
            sAddress                <= 0;                                     -- FSP Nummer
            sFSPByteCounter         <= cFSPByteCounter;                       -- Zaehler fuer Anzahl im stack_array eingetragender Bytes (da MSByte first => gFSPDepth-1)
            sClockData              <= '0';
            sCRCOK                  <= '0';
            sFSPSelected            <= '0';
            sRemainingBytes         <= gFSPDepth;                             -- Zaehler fuer noch eintragbare/auslesbare Bytes
            sOutputToPeripheral     <= gReset;                                -- Ausgabelatch fuer Paralleldaten
            sNewDataAvailable       <= '0';
            sDataOut                <= (others => '0');
            sEdgeDetArray           <= (others => '0');                       -- dient der Flankenerkennung von 'ClockData' 
            sLoadEdgeDetArray       <= (others => '0');
         
         else

            if (Clock'event and Clock = '1') then 

               sAddress          <= to_integer(unsigned(Address));            -- angelegte Adresse lesen
               sClockData        <= ClockData;
               sCRCOK            <= CRCOK;

               if (gUseInputFromPeripheral = '1') then

                  sLoadEdgeDetArray <= (sLoadEdgeDetArray(0) & LoadDataFromPeripheral);

                  if (sLoadEdgeDetArray = "01") then                          -- Flanke an LoadEdge erkannt..
                     sOutputToPeripheral <= InputFromPeripheral;              -- ..Output direkt aendern
                  end if;
               else
                  sLoadEdgeDetArray <= (others => '0');
               end if;


               if (sAddress /= gFSPNumber) then                               -- falsche Adresse -> verharren
                  sState_FSP        <= WAIT_FOR_ADDRESSING;
                  sFSPByteCounter   <= cFSPByteCounter;                       -- Bytezaehler auf maximalen Wert
                  sRemainingBytes   <= gFSPDepth; 
                  sFSPSelected      <= '0';
               else

                  case (sState_FSP) is

                     ------------------------------------------------------------------
                     when WAIT_FOR_ADDRESSING =>

                        for i in 0 to gFSPDepth-1 loop                           --Dresser mit 'gReset' fuellen, bzw. 'sOutput..' halten
                           sDresser(i) <= sOutputToPeripheral(((i * 8)+7) downto (i * 8));
                        end loop;

                        if (sAddress = gFSPNumber) then                          -- Wenn FSP adressiert, dann:
                           sFSPSelected      <= '1';                             -- Selectinfo auf '1'
                           sState_FSP        <= WAIT_FOR_CLOCK_DATA_HIGH;        -- in naechsten State
                        end if;

                     ------------------------------------------------------------------
                     when WAIT_FOR_CLOCK_DATA_HIGH =>

                        sEdgeDetArray  <= (sEdgeDetArray(0) & sClockData);       -- Flankenerkennung

                        if (sEdgeDetArray = "01") then                           -- steigende Flanke an ClockData erkannt 
                           if (RWn = '1') then                                   -- Daten von FSPInterconnectionHandler in FSP schreiben
                              sDataOut(7 downto 0) <= sDresser(sFSPByteCounter); -- Daten interner Bus an externen Bus
                           else                                                  -- Daten in den FSPInterconnectionHandler vom FSP lesen
                              sNewDataAvailable         <= '0';                  -- hier loeschen und spaeter wieder setzen, sofern Daten erfolgreich geschrieben wurden (V0.6)
                              sDresser(sFSPByteCounter) <= Data(7 downto 0);     -- Daten externer Bus an internen Bus
                           end if;

                           sRemainingBytes   <= sRemainingBytes - 1;             -- 'RemainingBytes' schnellstmoeglich reduzieren
                           sState_FSP        <= REDUCE_FSP_BYTE_COUNTER;

                        else
                           sState_FSP <= WAIT_FOR_CLOCK_DATA_HIGH;               -- FSP noch adressiert, also warten
                        end if;

                     ------------------------------------------------------------------
                     when REDUCE_FSP_BYTE_COUNTER =>

                        if (sFSPByteCounter > 0) then
                           sFSPByteCounter <= sFSPByteCounter - 1;
                           sState_FSP      <= WAIT_FOR_CLOCK_DATA_HIGH;          -- weitere Bytes empfangen/ausgeben
                        else
                           if (RWn = '1') then
                              sState_FSP      <= WAIT_FOR_DESELECTION;               -- alle Bytes ausgegeben -> auf Deselektion warten
                           else
                              sState_FSP      <= WAIT_FOR_CRC_OK;                -- empf. Daten auswerten sofern RWn = '0'
                           end if;

                        end if;

                     ------------------------------------------------------------------
                     when WAIT_FOR_CRC_OK =>

                        if (sCRCOK = '1') then                                -- Highpuls CRCOK erkannt 
                           sOutputToPeripheral  <= sOutputToPeripheralTemp;
                           sNewDataAvailable    <= '1';
                           sState_FSP           <= WAIT_FOR_DESELECTION;
                        else
                           sState_FSP <= WAIT_FOR_CRC_OK;                        
                        end if;

                     ------------------------------------------------------------------
                     when WAIT_FOR_DESELECTION =>

                        if (sAddress = gFSPNumber) then
                           sState_FSP <= WAIT_FOR_DESELECTION;
                        else 
                           sState_FSP <= WAIT_FOR_ADDRESSING;
                        end if;                        

                     ------------------------------------------------------------------
                     when others =>

                        sState_FSP <= WAIT_FOR_ADDRESSING;

                  end case;
               end if;
            end if ;
         end if ;

      end process pFSP;


--------------------------------------------------------------------
--Array nach parallel Wandlung
--------------------------------------------------------------------      
      array_to_parallel_gen : for i in 0 to gFSPDepth-1 generate
         sOutputToPeripheralTemp(((i * 8)+7) downto (i * 8)) <= sDresser(i);
      end generate;

--------------------------------------------------------------------
--Diese Ausgaben sind TriState faehig 
--------------------------------------------------------------------
   pOutputMapping: process(sFSPSelected, RWn, sRemainingBytes, sDataOut )
   begin

      if (sFSPSelected = '1') then

         if (RWn = '1') then
            Data     <= sDataOut;
         else
            Data     <= (others => 'Z');
         end if;

         RemainingBytes <= std_logic_vector(to_unsigned(sRemainingBytes, 12));
         Available      <= '1';
         --ReadOnly       <= '1';

      else
      
         RemainingBytes    <= (others => 'Z');
         Available         <= '0';
         --ReadOnly          <= '0';
         Data              <= (others => 'Z');

      end if;

   end process pOutputMapping;

   ReadOnly             <= '0';
   OutputToPeripheral   <= sOutputToPeripheral;
   NewDataAvailable     <= sNewDataAvailable;

end RTL;
