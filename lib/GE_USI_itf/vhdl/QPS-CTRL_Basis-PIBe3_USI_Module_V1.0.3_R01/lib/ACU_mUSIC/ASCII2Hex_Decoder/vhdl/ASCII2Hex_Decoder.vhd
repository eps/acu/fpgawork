LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
--USE IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : ASCII2Hex_Decoder                                                                               *
--*                                                                                                               *
--* Beschreibung: Der Decoder wandelt die am Eingang ASCII[7..0] angelegten ASCII-Werte durch das Setzen          *
--*               der Leitung 'StartConvert' = '1' in einen Hex-Wert und gibt diesen am Ausgang Hex[4..0] aus.    *
--*               Das Signal 'StartConvert' darf dabei nur ein Puls von 1 Takt Laenge sein!                       *
--*               Das Ende der Wandlung wird mittels des Signals 'ConversionDone' = '1' angezeigt.                *
--*               ASCII Eingangswerte sind         : 0x30..0x39 und 0x41..0x46                                    *
--*               Hexadezimale Ausgangswerte sind  : 0x0...0xF                                                    *
--*                                                                                                               *
--* Version     : 18.05.10 Born (DS)                                                                              *
--*               15.09.21 Generic ..Version entfernt (DS)                                                        *
--*                                                                                                               *
--* Logikelemente   : 16                                                                                          *
--* Register        : 15                                                                                          *
--* Zyklen/Wandlung :  2                                                                                          *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************

entity ASCII2Hex_Decoder is                                             -- Entity - Blockbeschreibung Ein- und Ausgaenge
----------------------------------------------------------------
   port 
   (
      Clock                      : in  std_logic;
      Reset                      : in  std_logic;

      ASCII                      : in  std_logic_vector (7 downto 0);   -- Eingang fuer zu wandelnde ASCII-Wert
      Hex                        : out std_logic_vector (3 downto 0);   -- Ausgang fuer gewandelte Hexadezimalwert

      StartConvert               : in  std_logic;                       -- Startet die Wandlung von ASCII->Hex wenn '1'
      ConversionDone             : out std_logic;                       -- Wenn '1' ist Wandlung beendet, am Ausgang Hex[3..0] steht 
                                                                        -- gueltiger Wert, sofern 'Error' nicht '1'
      Error                      : out std_logic                        -- Wird '1' wenn der angelegte ASCII Wert nicht zulaessig ist
   );
end ASCII2Hex_Decoder;

architecture RTL of ASCII2Hex_Decoder is

-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer  pConvASCII2Hex_Decoder
-------------------------------------------------------------------

   signal   sHex                    : std_logic_vector (3 downto 0);
   signal   sASCII                  : std_logic_vector (7 downto 0);
   signal   sStartConvert           : std_Logic;
   signal   sConversionDone         : std_logic ;

   signal   sError                  : std_logic;

   begin

      pConvASCII2Hex_Decoder : process (Clock, Reset)
      begin

         if (Reset = '1') then

            sASCII                  <= (others => '0');
            sConversionDone         <= '0';
            sError                  <= '0';

         else
            if (Clock'event and Clock = '1') then 

               sStartConvert     <= StartConvert;
               sASCII            <= ASCII;
               sConversionDone   <= '0';

               if (sStartConvert = '1') then                        -- StartConvert (1 Takt Pulse!) erkannt

                  sError            <= '0';

                  case (sASCII) is

                     when X"30" =>
                        sHex <= X"0" ;
                     when X"31" =>
                        sHex <= X"1" ;
                     when X"32" =>
                        sHex <= X"2" ;
                     when X"33" =>
                        sHex <= X"3" ;
                     when X"34" =>
                        sHex <= X"4" ;
                     when X"35" =>
                        sHex <= X"5" ;
                     when X"36" =>
                        sHex <= X"6" ;
                     when X"37" =>
                        sHex <= X"7" ;
                     when X"38" =>
                        sHex <= X"8" ;
                     when X"39" =>
                        sHex <= X"9" ;
                     when X"41" =>
                        sHex <= X"A" ;
                     when X"42" =>
                        sHex <= X"B" ;
                     when X"43" =>
                        sHex <= X"C" ;
                     when X"44" =>
                        sHex <= X"D" ;
                     when X"45" =>
                        sHex <= X"E" ;
                     when X"46" =>
                        sHex <= X"F" ;
                     when others => 
                        sError <= '1';

                  end case;

                  sConversionDone      <= '1';

               end if ;

         end if ;
      end if ;

   end process pConvASCII2Hex_Decoder;

   --Ausgabe
   Hex(3 downto 0)   <= sHex(3 downto 0);
   ConversionDone    <= sConversionDone;
   Error             <= sError;

end RTL;
