LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : AutoFallBack                                                                                    *
--*                                                                                                               *
--* Beschreibung: siehe Funktion                                                                                  *
--*                                                                                                               *
--* Version     : 18.05.10 Born (DS)                                                                              *
--*               15.09.21 Namensapassungen für Ports durchgefuuehrt                                              *
--*                        Generic ..Version entfernt (DS)                                                        *
--*                                                                                                               *
--* Logikelemente   :  14                                                                                         *
--* Register        :  11                                                                                         *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************


-- Funktion:

-- Der AutoFallBack Zaehler dient dazu, einen Abriss der Hochgeschwindigkeitskommunikation zwischen dem Slavemodul
-- und der MFU zu erkennen. Im "normalen USI Betrieb" erfuellt AutoFallBack keine Funktion.
-- Werden Daten empfangen, wird dies vom USI Modul signalisiert. Das Abholen der Daten wird vom
-- Merger/Distributor erledigt, ebenso das Senden der HS-Daten. Jedesmal wenn der M/D Daten von der USI
-- liest, wird dessen Ausgang 'MD_USI_RxReadEnable' high. Beim Schreiben von Daten an die USI wird dessen
-- Ausgang 'MD_USI_TxWriteEnable' high.
-- Diese beiden Signale macht sich AutoFallBack zu nutze.
-- 'MD_USI_RxReadEnable' wird an den Eingang 'TriggerResetCounter', 
-- 'MD_USI_TxWriteEnable' wird an den Eingang 'TriggerIncrementCounter' gelegt.
-- Der Zaehler in AutoFallBack wird sobald das Signal an 'TriggerResetCounter' high wird zurueck gesetzt.
-- Mit einer low->high Flanke an 'TriggerIncrementCounter' hingegen um +1 erhoeht.
-- Reisst nun die Verbindung zwischen Slavemodul und MFU ab, versucht die USI weiterhin HS-Daten
-- an die MFU zu senden. Dabei wird 'TriggerIncrementCounter' stetig getriggert und der AutoFallBack Zaehler erhoeht.
-- Weil aber keine Daten mehr ueber die USI empfangen werden, wird der Eingang 'TriggerResetCounter' nicht mehr
-- bedient und der Zaehler lauft ueber, sobald er den Wert gCounts erreicht hat. In diesem Fall wird der
-- Ausgang 'SetUSIToDefaults' = '1'.
-- Wird der Eingang 'Enable' = '0', wird der Zaehler zurueck gesetzt. Der Eingang 'Enable' ist also in der Regel
-- mit dem Infosignal darueber zu verbinden, dass sich die USI im HS-Modus befindet.


entity AutoFallBack is                                            -- Entity - Blockbeschreibung Ein- und Ausgaenge
   generic 
   (
      gCounts              : integer := 100                       -- Anzahl der Zaehlungen bevor 'SetUSIToDefaults' = '1' wird
   );
----------------------------------------------------------------
   port 
   (
      Clock                      : in     std_logic;
      Reset                      : in     std_logic;

      Enable                     : in     std_logic;              -- Aktiviert das Modul bei '1', loescht den Zaehler bei '0'.
      TriggerIncrementCounter    : in     std_logic;              -- Erhoeht den Zaehler bei Flanke low->high 
      TriggerResetCounter        : in     std_logic;              -- loescht den Zaehler

      SetUSIToDefaults           : out    std_logic               -- Wird '1' wenn Zaehler ueberlaeuft
   );
end AutoFallBack;

architecture RTL of AutoFallBack is

-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer  AutoFallBack
-------------------------------------------------------------------

   signal   sTriggerIncrementEdgeDetArray    : std_logic_vector (1 downto 0);
   signal   sTriggerIncrementEdgeDet         : std_logic;
   signal   sSetBaudToDefault                : std_logic;

   signal   sAutoFallBackCounter             : integer range 0 to gCounts;
   constant cAutoFallBackCounterMax          : integer range 0 to gCounts := gCounts;

   begin
-------------------------------------------------------------------
-- AutoFallBack
-------------------------------------------------------------------

   -- Flankenerkennung TriggerIncrementCounter
   -- ========================================
   -- jedes 'MD_USI_TxWriteEnable' Signal (Daten senden) vom Merger erzeugt einen
   -- Puls am Einagng 'TriggerIncrementCounter'.
   -- Dadurch wird 'sTriggerIncrementEdgeDet' gesetzt un der 'AutoFallBack Zaehler' erhoeht.
   pTriggerIncrementEdgeDet: process (Clock, Reset)
   begin
     if (Reset = '1') then
         sTriggerIncrementEdgeDetArray   <= (others => '0') ;
         sTriggerIncrementEdgeDet         <= '0';
     elsif (Clock'event and Clock = '1') then
         sTriggerIncrementEdgeDetArray(1 downto 0) <= (sTriggerIncrementEdgeDetArray(0) & TriggerIncrementCounter);
         if (sTriggerIncrementEdgeDetArray = "01") then                    -- steigende Flanke
            sTriggerIncrementEdgeDet <= '1';
         else
            sTriggerIncrementEdgeDet <= '0';
         end if;
     end if;
   end process pTriggerIncrementEdgeDet;

   -- AutoFallBack Zaehler
   --=====================
   -- Der nachfolgende Zaehler wird entweder von 'sTriggerIncrementEdgeDet = '1'' (1 Takt) erhoeht
   -- oder dur 'TriggerResetCounter = '1'' (Zustand) geleoscht.
   -- Ist der maximale Werte von 'cAutoFallBackCounterMax' erreicht, wird
   -- 'sSetBaudToDefault = '1'' (1 Takt).
   pAutoFallBack : process (Reset, Clock)
   begin
      if (Reset = '1') then 
         sAutoFallBackCounter <=  0;
         sSetBaudToDefault    <= '0';
      else
         if (Clock'event AND Clock = '1') then 

            if (Enable = '1') then                                         -- Enable = '1' -> AutoFallBack aktiv -> ggf. zaehlen

               if (TriggerResetCounter = '0') then                         -- ..ResetConter = '0' -> zaehlen

                  if (sAutoFallBackCounter < cAutoFallBackCounterMax) then -- Maximalwert nicht erreicht

                     if (sTriggerIncrementEdgeDet = '1') then              -- Zaehler inkrement
                        sAutoFallBackCounter <= sAutoFallBackCounter + 1;
                     else
                        sAutoFallBackCounter <= sAutoFallBackCounter;
                     end if;

                  else
                     sAutoFallBackCounter <= sAutoFallBackCounter;         -- Zaehler stoppen
                     sSetBaudToDefault     <= '1';                         -- Baudgenerator ruecksetzen
                  end if;
               else                                                        -- ..ResetConter = '1' -> Zaehler loeschen
                  sSetBaudToDefault    <= '0';
                  sAutoFallBackCounter <= 0;
               end if;
            else                                                           -- Enable = '0' -> AutoFallBack deaktiv -> Zaehler loeschen
               sSetBaudToDefault    <= '0';
               sAutoFallBackCounter <= 0;

            end if;
         end if;
      end if;
   end process pAutoFallBack;

   SetUSIToDefaults <= sSetBaudToDefault;

end RTL;
