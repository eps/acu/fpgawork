LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

USE ieee.math_real.log2;
USE ieee.math_real.ceil;

LIBRARY work;
USE work.ACU_package.all;

--| dieses Modul speichert die ankommenden Interlocks bis diese ueber die Interlock Resetleitung quittiert werden
--| alle Interlocks, inkl. SumInterlock_n sind Low Activ, d.h. steht kein Interlock an, ist das Bit High
--| bei einwandfreier Funktion sind also alle Ausgangsports High
--------------------------------------------------------------------------------------
--This module stores the incoming interlocks until they are acknowledged via the interlock reset line
--| all interlocks, incl. SumInterlock_n are low activ, i.e. if there is no interlock, the bit is high
--| if working properly, all output ports are High


--| Versionskontrolle
--| ==================
--|            born (GS)
--| 20.11.08 konform benamt, dokumentiert (DS)
--| 22.06.12 Groesse der Interlocks in Byte anpassbar (J.Bley)
--| 04.07.12 Symbol editiert (DS)
--| 18.02.14 Meldung 'Ignored Power-Up Level option on the following registers' -> 'Critical Warning (18010): Register xxx will power up to GND/High'
--|          durch gezieltes setzen eines Initwertes der betroffen Variable(n) beseitigt. (DS)
--| 17.11.14 Geschaltete Maske fuer verzoegerte Interlocks eingefuehrt. (GS)  
--| 08.09.15 Removed Interlocks_n from the sensitivity list and changed the sTempInterlocks_n reset condition.(DR)
--| 21.09.15 Added Interlock arrival time sequence: The first five interlocks arrival sequence is stored. The sequence doesn't take care if the interlocks are still 
--|          present during the user check. The sequence is cleaned with the memory reset. (DR)
--| Version control
--| ==================
--|            born (GS)
--| 20.11.08 compliant benamt, documented (DS)
--| 22.06.12 Size of interlocks adjustable in bytes (J.Bley)
--| 04.07.12 Symbol edited (DS)
--| 18.02.14 Message 'Ignored Power-Up Level option on the following registers' -> 'Critical Warning (18010): Register xxx will power up to GND/High'
--|          by setting an initiate value of the affected variable(s) in a targeted manner. (DS)
--| 17.11.14 Switched mask for delayed interlocks introduced. (GS)  
--| 08.09.15 Removed Interlocks_n from the sensitivity list and changed the sTempInterlocks_n reset condition. (DR)
--| 21.09.15 Added Interlock arrival time sequence: The first five interlocks arrival sequence is stored. The sequence doesn't take care if the interlocks are still 
--|          present during the user check. The sequence is cleaned with the memory reset. (DR)



--| 12.05.16 OUT'InterlocksArrSeqNmb hinzugefuegt, da Interlockbit[0] nicht in InterlocksArrSeq[] als solcher erkennbar, der Biteintrag ist 0x00
--|          Ist nun z.B. OUT'InterlocksArrSeqNmb = 2 und InterlocksArrSeq[1] = 0x2 und InterlocksArrSeq[0] = 0x0 ist klar,
--|          dass insg. 2 Interlocks im Array stehen, naemlich Bit 2 und Bit 0. (DS)
--| 23.06.16 Sind Interlocks via 'InterlocksSwitchMaskPending_n' erst mit 'MaskSwitchDisable' aktiv, waere es denkbar, dass zuerst
--|          'MaskSwitchDisable' diese Interlocks wieder deaktiviert und erst dann das eigentliche Interlocksignal an 'Interlocks_n'
--|          ankommt. Daher ist nun der 'MaskSwitchDisableDelay_Timer' eingebaut, der die Abschaltung der via 'InterlocksSwitchMaskPending_n'
--|          gesetzten Interlockbits um 'gMaskSwitchDisableDelay_in_ns' verzoegert. (DS)
-----------------------------------------------------------------------------------------
--| 12.05.16 OUT'InterlocksArrSeqNmb added, because interlock bit[0] is not recognizable as such in interlocksArrSeq[], the bit entry is 0x00
--|          For example, if OUT'InterlocksArrSeqNmb = 2 and InterlocksArrSeq[1] = 0x2 and InterlocksArrSeq[0] = 0x0 it is clear
--|          That total. There are 2 interlocks in the array, namely bit 2 and bit 0. (DS)
--| 23.06.16 If interlocks via 'InterlocksSwitchMaskPending_n' are only active with 'MaskSwitchDisable', it would be conceivable that first
--|          'MaskSwitchDisable' deactivates these interlocks again and only then the actual interlock signal to 'Interlocks_n'
--|          Arrives. Therefore, the 'MaskSwitchDisableDelay_Timer' is now installed, which prevents the shutdown of the via 'InterlocksSwitchMaskPending_n'
--|          set interlock bits by 'gMaskSwitchDisableDelay_in_ns'. (DS)
-----------------------------------
--| 31.05.17 Meldung 'Ignored Power-Up Level option on the following registers' -> 'Critical Warning (18010): Register xxx will power up to GND/High'
--|          durch gezieltes setzen eines Initwertes der betroffen Variable(n) beseitigt. (DS)
--| 26.01.22 Generic ..Version entfernt
--|          'InterlocksSwitchMaskPending_n' umbenannt in 'InterlocksMaskConditonal_n' und
--|          'MaskSwitchDisable' umbenannt in 'ConditionalMaskDisable' (DS)
--------------------------------------------
--| 31.05.17 Message 'Ignored Power-Up Level option on the following registers' -> 'Critical Warning (18010): Register xxx will power up to GND/High'
--|          by setting an initiate value of the affected variable(s) in a targeted manner. (DS)
--| 01/26/22 Generic .. Version removed
--|          'InterlocksSwitchMaskPending_n' renamed to 'InterlocksMaskConditonal_n' and
--|          'MaskSwitchDisable' renamed to 'ConditionalMaskDisable' (DS)
-----------------------------------
--| INFO
--| =============
--| Was das Modul NICHT kann: Kommen mehrere Interlocks zeitgleich (also wirklich im gleichen Takt) an, wird nur ein Interlock im
--| InterlocksArrSeq[]-Array gespeichert. Und zwar das hochwertigste Bit. Kommen also z.B. die Bit 2,4 und 6 zeitgleich, wird nur Bit 6 abgelegt.
--| INFO
--| =============
--| What the module can NOT do: If several interlocks arrive at the same time (i.e. really in the same cycle), only one interlock in the
--| InterlocksArrSeq[] array. And the highest quality bit. If, for example, bits 2,4 and 6 come at the same time, only bit 6 is stored.

entity   ACU_InterlockMemory is
   generic  
   (
      gMainClockInHz                      : integer   := 100_000_000;
      gConditionalMaskDisableDelay_in_ns  : integer   := 500;           --wird 'MaskSwitchDisable' von H->L wird dessen tatsaechliche Deaktivierung modulintern um diese Zeit verzoegert
																								--if 'MaskSwitchDisable' of H->L its actual deactivation is delayed within the module at this time
      gByteWidthOfInterlockBus            : integer   := 3;
      gNrInterlocksArrSeqElements         : integer   := 5;
      --Nachfolgend muss eigentlich
      --gCalc                               : PARAMETER_UNKNOWN (erscheint als AUTO)   := ceil(log2(gNrInterlocksArrSeqElements))
      --stehen.
      --Quartus kann mit dieser Angabe aber KEIN Symbol erstellen. 
      --Daher gilt es nach der Symbolgenerierung die Parameter manuell im Symbol zu editieren.
		--------------------------------------------------------------------
		--Subsequent must actually
      --gCalc : PARAMETER_UNKNOWN (appears as AUTO) := ceil(log2(gNrInterlocksArrSeqElements))
      --stand.
      --Quartus can NOT create a symbol with this information. 
      --Therefore, it is necessary to edit the parameters manually in the symbol after symbol generation.
		-------------------------------------------------------------------------
      gCalc                               : integer   := 3
   );
PORT
   (
      Clock                               : in  std_logic  ;                                                   --| Systemtakt 100 MHz
      Reset                               : in  std_logic  ;                                                   --| --| Reinitializing module / Modul wird neu initialisiert/
      MemoryReset                         : in  std_logic  ;                                                   --|  --| stored interlocks are deleted / gespeicherte Interlocks werden geloescht/
      ConditionalMaskDisable              : in  std_logic  ;                                                   --| --| This pin switches between the mask and the direct interlocks / Mit diesem Pin wird zwischen der Maske und den Direkten Interlocks umgeschaltet/ 


      Interlocks_n                        : in  std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);   --| nByte Interlock
      InterlocksMaskMemorized_n           : in  std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);   --| --| nByte interlock mask for masking the inactive stored interlocks / nByte Interlock Maske zum ausmaskieren der inaktiven gespeicherten Interlocks/
		
                                                                                                               --| --| interlocks not to be observed must be masked with '1' /  nicht zu beachtende Interlocks muessen mit '1' maskiert sein /
																																					
      InterlocksMaskPending_n             : in  std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);   --| / --| nByte interlock mask for masking the inactive dynamic interlocks / nByte Interlock Maske zum ausmaskieren der inaktiven dynamischen Interlocks 
		
                                                                                                               --|  --| interlocks not to be observed must be masked with '1' / nicht zu beachtende Interlocks muessen mit '1' maskiert sein /
																																					
      InterlocksMaskConditonal_n          : in  std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);   --| --| nByte interlock mask for masking the time-controlled dynamic interlocks / nByte Interlock Maske zum ausmaskieren der zeitgesteuerten dynamischen Interlocks / 
		
                                                                                                               --| nicht zu beachtende Interlocks muessen mit '1' maskiert sein / --| interlocks not to be observed must be masked with '1'
																																					
      InterlocksPending_n                 : out std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);   --|  --| Currently pending interlocks, corresponding. Bit is '0' / Aktuell anstehende Interlocks, entspr. Bit ist '0' /
		
      InterlocksMemorized_n               : out std_logic_vector(gByteWidthOfInterlockBus * 8 - 1 downto 0);   --| --| stored interlock, corresponding. Bit is '0' / gespeicherte Interlock, entspr. Bit ist '0' / 
		
      InterlocksArrSeq                    : out array8bits(gNrInterlocksArrSeqElements-1 downto 0);            --| --| Interlocks arrival sequence: InterlocksArrSeq(gNrInterlocksArrSeqElements) / Interlocks arrival sequence: InterlocksArrSeq(gNrInterlocksArrSeqElements) /  
		
                                                                                                               --| is the first arrived and InterlocksArrSeq(0) is the last one.
      InterlocksArrSeqNmb                 : out std_logic_vector(gCalc-1 downto 0); 

      SumInterlock_n                      : out std_logic                                                      --| --| if '0' there is min. 1 interlock, if '1' no interlock / wenn '0' steht min. 1 Interlock an, wenn '1' kein Interlock/ 
   );

end ACU_InterlockMemory ;

ARCHITECTURE RTL OF ACU_InterlockMemory IS
--CONSTANT vector_length: integer := integer(ceil(log2(real (gNrInterlocksArrSeqElements)))); 

constant cInterlocks                         : integer      := gByteWidthOfInterlockBus * 8 - 1;

signal   sInterlocks_n                       : std_logic_vector   (cInterlocks downto 0);
signal   sTempInterlocks_n                   : std_logic_vector   (cInterlocks downto 0);
signal   sInterlocksMemorized_n              : std_logic_vector   (cInterlocks downto 0);
signal   sInterlocksMaskMemorized_n          : std_logic_vector   (cInterlocks downto 0);
signal   sInterlocksMaskPending_n            : std_logic_vector   (cInterlocks downto 0);
signal   sInterlocksMaskConditonal_n         : std_logic_vector   (cInterlocks downto 0);
signal   sSumInterlock_n                     : std_logic;

signal   sConditionalMaskDisable             : std_logic;

signal   InterlocksArrSeqInt                 : array8bits (gNrInterlocksArrSeqElements-1 downto 0);   --| Internal InterlocksArrSeq array.
signal   sCntPosition                        : unsigned (gNrInterlocksArrSeqElements-1 downto 0);
signal   sInterlocksMemorizedD1_n            : std_logic_vector   (cInterlocks downto 0);

signal   sInterlocksArrSeqNmb                : std_logic_vector ( integer((ceil(log2(real(gNrInterlocksArrSeqElements)))))-1 downto 0);

constant cConditionalMaskDisableDelay_TimerMaxVal : integer := integer( real(gMainClockInHz) / real(1_000_000_000) * real(gConditionalMaskDisableDelay_in_ns) ); --Maximum count size/ --Maximale Zaehlergoesse

signal   sConditionalMaskDisableDelay_Timer       : integer range cConditionalMaskDisableDelay_TimerMaxVal downto 0 := cConditionalMaskDisableDelay_TimerMaxVal; --actual counter for the delay/ --eigentlicher Zaehler fuer die Verzoegerung
signal   sConditionalMaskDisableDelay             : std_logic := '1';

begin

   assert (gConditionalMaskDisableDelay_in_ns >= 10)
      report "ACU_InterlockMemory -> gConditionalMaskDisableDelay_in_ns must be equal or higher than 10!"
   severity Error;

   --Ist 'sConditionalMaskDisable = '1'' wird der Timer scharf geschaltet, 'sConditionalMaskDisableDelay = '0'
   --Wird 'sConditionalMaskDisable = '0' laeuft der Timer bis 'cConditionalMaskDisableDelay_TimerMaxVal' und 'sConditionalMaskDisableDelay' wird '1'
	
	--If 'sConditionalMaskDisable = '1'' the timer is armed, 'sConditionalMaskDisableDelay = '0'
   --If 'sConditionalMaskDisable='0' the timer runs to 'cConditionalMaskDisableDelay_TimerMaxVal' and 'sConditionalMaskDisableDelay' becomes '1'

   pConditionalMaskDisableDelay_Timer : process (Clock, Reset)
   begin
      if (Reset = '1') then

         sConditionalMaskDisableDelay_Timer <= cConditionalMaskDisableDelay_TimerMaxVal;
         sConditionalMaskDisableDelay       <= '1';

      elsif (rising_edge(Clock)) then

         if (sConditionalMaskDisable = '1') then           --Maske ist deaktiviert
            sConditionalMaskDisableDelay_Timer <= 0;
            sConditionalMaskDisableDelay       <= '0';     --all interlock are allowed "normally" /  --alle Interlock werden "normal" zugelassen
         else                                         --Maske wird aktiviert... 
            if (sConditionalMaskDisableDelay_Timer < cConditionalMaskDisableDelay_TimerMaxVal) then
               sConditionalMaskDisableDelay_Timer <= sConditionalMaskDisableDelay_Timer + 1;  --...Timer laeuft ab...
               sConditionalMaskDisableDelay       <= '0';
            else
               sConditionalMaskDisableDelay_Timer <= cConditionalMaskDisableDelay_TimerMaxVal;
               sConditionalMaskDisableDelay       <= '1';                                --..und die Info mit mitgeteilt.
            end if;
         end if;

      end if;
   end process pConditionalMaskDisableDelay_Timer;


   pInterlockMemory : process (Clock, Reset)
   begin
      if (Reset = '1') then

         sInterlocksMemorized_n           <= (others => '1') ;       --| nByte Interlock auf OK setzen
         sSumInterlock_n                  <= '1' ;                   --| kein Interlock anstehend
         sInterlocks_n                    <= (others => '1');
         sInterlocksMaskMemorized_n       <= (others => '1');
         sInterlocksMaskPending_n         <= (others => '1');
         sInterlocksMaskConditonal_n      <= (others => '1');
         sConditionalMaskDisable          <= '0';
         sTempInterlocks_n                <= (others => '1');

         sInterlocksMemorizedD1_n         <= (others => '1') ;
         InterlocksArrSeqInt              <= (others =>(others => '0')); 
         sInterlocksArrSeqNmb             <= (others => '0');
         sCntPosition                     <= (others => '0');

      elsif (rising_edge(Clock)) then

         sInterlocksMaskMemorized_n       <= InterlocksMaskMemorized_n;
         sInterlocksMaskPending_n         <= InterlocksMaskPending_n;
         sInterlocksMaskConditonal_n      <= InterlocksMaskConditonal_n;
         sConditionalMaskDisable          <= ConditionalMaskDisable;

         sTempInterlocks_n                <= Interlocks_n ;
         sInterlocksMemorizedD1_n         <= sInterlocksMemorized_n;

         -- Wenn 'sMaskSwitchDisable'True ODER 'sMaskSwitchDisableDelay'False werden die Interlocks direkt weitergeleitet.
         -- Wenn 'sMaskSwitchDisable'False wird 'InterlocksSwitchMaskPending_n' verodert um die Interlocks zu deaktivieren.
         -- Dies dient um Interlocks erst verzoegert zu aktivieren (z.B. Hauptschuetz oder Spannungsueberwachungen)

			-- If 'sMaskSwitchDisable'True OR 'sMaskSwitchDisableDelay'False, the interlocks are forwarded directly.
         -- If 'sMaskSwitchDisable'False, 'InterlocksSwitchMaskPending_n' is used to disable the interlocks.
         -- This is used to activate interlocks only delayed (e.g. main protection or voltage monitoring)
			
         if (sConditionalMaskDisable= '1' OR sConditionalMaskDisableDelay = '0') then 
            sInterlocks_n  <= sTempInterlocks_n ;
         else
            sInterlocks_n  <= sTempInterlocks_n OR sInterlocksMaskConditonal_n;
         end if ;

         if (MemoryReset = '1') then                              --| delete any saved interlock / --| ggf. gespeicherte Interlock loeschen
            sInterlocksMemorized_n  <= (others => '1') ;          --| Set nByte Interlock to OK / --| nByte Interlock auf OK setzen
            sSumInterlock_n         <= '1' ;                      --| no interlock pending / --| kein Interlock anstehend
            InterlocksArrSeqInt     <= (others=>(others=>'0'));   --| It "behaves" like sInterlocksMemorized_n signal from the reset point of view.
            sInterlocksArrSeqNmb    <= (others=>'0'); 
            sCntPosition            <= (others=>'0'); 
         else

            for i in 0 to ((gByteWidthOfInterlockBus * 8) - 1) loop

               if (sInterlocks_n(i) = '0' and  sInterlocksMaskMemorized_n(i) = '0') then 
                  sInterlocksMemorized_n(i) <= '0' ;
               end if;

               if (sInterlocksMemorized_n(i) = '0' and  sInterlocksMaskMemorized_n(i) = '0') then   
                  sSumInterlock_n <= '0';
               end if;

            end loop;

         end if ;

         for i in 0 to ((gByteWidthOfInterlockBus * 8) - 1) loop
            if (sInterlocksMemorized_n(i)='0' and sInterlocksMemorizedD1_n(i)='1') then -- falling edge detection   

               -- Store the arrival sequence.

               if (sCntPosition < gNrInterlocksArrSeqElements) then
                  InterlocksArrSeqInt(gNrInterlocksArrSeqElements-1 downto 1) <=  InterlocksArrSeqInt(gNrInterlocksArrSeqElements-2 downto 0);
                  InterlocksArrSeqInt(0)                                      <=  std_logic_vector(to_unsigned(i,8));
                  sInterlocksArrSeqNmb                                        <=  std_logic_vector(to_unsigned(to_integer(unsigned(sInterlocksArrSeqNmb)) + 1, integer((ceil(log2(real(gNrInterlocksArrSeqElements))))) ) );
                  sCntPosition                                                <=  sCntPosition + 1;
               end if;
            end if; 
         end loop;

      end if ;

   end process pInterlockMemory;

   InterlocksPending_n    <= sInterlocks_n OR sInterlocksMaskPending_n ;
   InterlocksMemorized_n  <= sInterlocksMemorized_n ;
   SumInterlock_n         <= sSumInterlock_n ;
   InterlocksArrSeq       <= InterlocksArrSeqInt;
   InterlocksArrSeqNmb    <= sInterlocksArrSeqNmb;

end RTL ;
