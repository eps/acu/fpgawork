-------------------------------------------------------------------------------
--
-- Derek Schupp
--
-------------------------------------------------------------------------------
-- Versionskontrolle:
--          born
-- 31.03.09 State START_READ_WRITE modifiziert damit nicht permanenter Sprung zwischen START_READ_WRITE und START stattfindet (DS)
-- 13.01.10 Wechsel von generic'integer auf generic'real und auf Quartus V9.0 (DS)
-- 13.10.10 REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME muss mind. 100ns betragen (Datenblatt des M25P), daher eigener Zaehler dafuer, besser als
--          die bisherige Variante den RD, reps. WR Clock zu verwenden (DS)
-- 31.01.12 Wurde Kommando 'CMD_WC' durch Loeschen von 'ExecuteCommand' beendet und die Anzahl der zu schreibenden Bytes war nicht 256 oder ein
--          Vielfaches davon, wurde 'Busy' nicht gesetzt waehrend die Daten endgueltig ins Flash geschrieben wurden.
--          Dies sorgte dafuer, dass steuernde Module bereits ein neues Kommando anlegen konnten obwohl das vorherige noch nicht
--          beendet war. Dies sorgte fuer Fehlverhalten des Moduls bis hin zur vollstaendigen Niederlegung der Arbeit. (DS)
-- 07.10.13 Generic 'gMainClockInHz' von 'real' nach 'integer' und dafuer in der Berechnung einen Cast gesetzt. (DS)
-- 18.02.14 Meldung 'Ignored Power-Up Level option on the following registers' -> 'Critical Warning (18010): Register xxx will power up to GND/High'
--          durch gezieltes setzen eines Initwertes der betroffen Variable(n) beseitigt. (DS)
-- 15.06.15 'DiagVector' entfernt, Umstieg auf 'numeric_std' (DS)
-- 01.06.16 sCommand, sExecuteCommand, sWriteDataEnable, sReadDataEnable, sLSBFirst werden jetzt getaktet eingelesen.
--          Berechnung der Teiler fuer 'MaxFrequencyFor..' war falsch und fuehrte mitunter zu einem zu kleinen Wert und damit zu einem zu hohen Takt
--          Jetztige Berechung sollte das Problem beheben. 
--          gMainClockInHz -------------- ersetzt: gMainClockIn --------------> war integer, bleibt integer
--          gMaxFrequencyForReadingInHz - ersetzt: gMaxFrequencyForReadingIn -> war real, ist nun integer
--          gMaxFrequencyForWritingInHz - ersetzt: gMaxFrequencyForWritingIn -> war real, ist nun integer (DS)
-- 05.10.16 Ein <sCommand'CMD_NOP> zusammen mit einem <sExecuteCommand = '1'> bricht alle Vorgaenge ab und alle Signale werden neu initialisiert.
--          Dies soll undefinierte Anfangszustaende verhindern. (DS)
--02.02.22  Generic '..Version' entfernt. (DS)



-- Entity Definition

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.MATH_REAL.ALL;

entity EPCS_Control is

generic (
            gMainClockInHz                : integer   := 100000000;
            gMaxFrequencyForReadingInHz   : integer   :=  20000000;      --max. Lesegeschwindigkeit    20 MHz
            gMaxFrequencyForWritingInHz   : integer   :=  20000000       --max. Schreibgeschwindigkeit 20 MHz
        );

port (
            Clock                         : in  std_logic;                       --Systemtakt
            Reset                         : in  std_logic;                       --Reset
            Command                       : in  std_logic_vector(2 downto 0);    --Kommando
            ExecuteCommand                : in  std_logic;                       --Kommando ausfuehren
            Busy                          : out std_logic;                       --Modul beschaeftigt
            DataAddress                   : in  std_logic_vector(23 downto 0);   --Daten-Adresse
            WriteData                     : in  std_logic_vector(7 downto 0);    --zu schreibende Daten
            WriteDataEnable               : in  std_logic;                       --schreiben starten (Strobe)
            ReadData                      : out std_logic_vector(7 downto 0);    --zu lesende Daten
            ReadDataEnable                : in  std_logic;                       --lesen starten (Strobe)
            LSBFirst                      : in  std_logic;                       --LSB wird zuerst geschoben (lesen/schreiben), muss 1 sein beim schreiben von .rbf Files
            TO_EPCS_nCS                   : out std_logic;                       --CSn zum Flash
            TO_EPCS_ASDI                  : out std_logic;                       --Daten vom Flash
            TO_EPCS_DCLK                  : out std_logic;                       --Clock zum Flash
            FROM_EPCS_DATA                : in  std_logic                        --Daten zum Flash    
      );

   --Begrenzt die INteger auf einen Minimalwert von 1 (fuer die Zaehler)
   function limit_to_minimal_value(x : integer; min : integer) return integer is
   begin
      if x > min then
         return x;
      else
         return 1;
      end if;
   end limit_to_minimal_value;

--   function get_delay_in_ticks_ceil (clk_freq_in_hz : real; desired_delay_in_ns : real) return integer is
--   begin
--      return limit_to_minimal_value(integer(ceil(clk_freq_in_hz * desired_delay_in_ns / 1.0e+9))+1, 0);
--   end get_delay_in_ticks_ceil;

--   constant cCountTicksForReadingMaxValue   : integer := get_delay_in_ticks_ceil(real(gMainClockInHz), gMaxFrequencyForReadingInHz);
--   constant cCountTicksForWritingMaxValue   : integer := get_delay_in_ticks_ceil(real(gMainClockInHz), gMaxFrequencyForWritingInHz);
--   constant cCountTicksFornCSHigh           : integer := get_delay_in_ticks_ceil(real(gMainClockInHz), 100.0);

   constant cCountTicksForReadingMaxValue   : integer := limit_to_minimal_value(integer(ceil(real(gMainClockInHz) / real(gMaxFrequencyForReadingInHz) / 2.0)), 0);
   constant cCountTicksForWritingMaxValue   : integer := limit_to_minimal_value(integer(ceil(real(gMainClockInHz) / real(gMaxFrequencyForWritingInHz) / 2.0)), 0);
   constant cCountTicksFornCSHigh           : integer := limit_to_minimal_value(integer(ceil(real(gMainClockInHz) / 10_000_000.0)), 0);

end EPCS_Control;


architecture RTL of EPCS_Control is

type tStateType is (
                     START,
                     START_ERASE,
                     START_READ_WRITE,
                     START_READ_WRITE_ERASE_END,
                     
                     READ_SET_OPCODE,
                     READ_SET_ADR,
                     READ_START_READING,
                     READ_RECEIVE_DATA,
                     READ_WAIT_FOR_READ_DATA_ENABLE_LOW,
                     READ_WAIT_FOR_READ_DATA_ENABLE_HIGH,

                     WRITE_SET_ENABLE_OPCODE,
                     WRITE_ENABLE_nCS_HIGH,
                     WRITE_SET_WRITE_OPCODE,
                     WRITE_SET_ADR,
                     WRITE_DATA,
                     WRITE_WAIT_FOR_WRITE_DATA_ENABLE_LOW,
                     WRITE_CALCULATE_NEXT_ADR,
                     WRITE_POSSIBLY_CHANGE_SECTOR,
                     WRITE_INCREMENT_ADDRESS,
                     WRITE_WAIT_FOR_WRITE_DATA_ENABLE_HIGH,
                     WRITE_nCS_HIGH,
                     WRITE_SET_READ_STATUS_OPCODE,
                     WRITE_READ_STATUS,
                     WRITE_PROOF_STATUS,

                     ERASE_SET_WR_ENABLE_OPCODE,
                     ERASE_nCS_HIGH_AFTER_WR_ENABLE_OPCODE,
                     ERASE_SET_ERASE_OPCODE,
                     ERASE_SET_ADR,
                     ERASE_nCS_HIGH,
                     ERASE_SET_READ_STATUS_OPCODE,
                     ERASE_READ_STATUS,
                     ERASE_PROOF_STATUS,
                     ERASE_END_nCS_HIGH,

                     PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA,
                     PUT_BYTE_TO_FLASH_STATE2_SET_DCLK,
                     
                     GET_BYTE_FROM_FLASH_START_SET_DCLK,
                     GET_BYTE_FROM_FLASH_STATE1_CLR_DCLK,
                     GET_BYTE_FROM_FLASH_STATE2_SET_DCLK_RD_DATA,

                     REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME
                  );

signal sState                 : tStateType ;
signal sNextState             : tStateType ;

type   adr_byte_array_type is array(0 to 2) of std_logic_vector(7 downto 0);

signal sAddressByteArray : adr_byte_array_type;

signal sCountTicksForReading           : integer range 0 to cCountTicksForReadingMaxValue := cCountTicksForReadingMaxValue - 1;
signal sCountTicksForWriting           : integer range 0 to cCountTicksForWritingMaxValue := cCountTicksForWritingMaxValue - 1;
signal sCountTicksFornCSHigh           : integer range 0 to cCountTicksFornCSHigh := cCountTicksFornCSHigh;

signal sPutByteToFlashBitIndex         : integer range 7 downto 0;
signal sPutByteToFlash                 : std_logic_vector(7 downto 0);

signal sGetByteFromFlashBitIndex       : integer range 7 downto 0;
signal sGetByteFromFlash               : std_logic_vector(7 downto 0);

signal sAddressByteArrayIndex          : integer range 2 downto 0;
 
signal sDataAddress                    : std_logic_vector(23 downto 0);

signal sWriteData                      : std_logic_vector (7 downto 0);

signal sIncrementPage                  : std_logic;

signal sCommand                        : std_logic_vector(2 downto 0);
signal sExecuteCommand                 : std_logic;
signal sWriteDataEnable                : std_logic;
signal sReadDataEnable                 : std_logic;
signal sLSBFirst                       : std_logic;

-- FLASH commands
constant NOP   : std_logic_vector (7 downto 0) := X"FF";  -- no cmd to execute
constant WE    : std_logic_vector (7 downto 0) := X"06";  -- write enable
constant WD    : std_logic_vector (7 downto 0) := X"04";  -- write disable
constant RDSR  : std_logic_vector (7 downto 0) := X"05";  -- read status reg
constant WRSR  : std_logic_vector (7 downto 0) := X"01";  -- write stat. reg
constant RB    : std_logic_vector (7 downto 0) := X"03";  -- read bytes
constant WB    : std_logic_vector (7 downto 0) := X"02";  -- write bytes
constant ES    : std_logic_vector (7 downto 0) := X"D8";  -- erase sector
constant EB    : std_logic_vector (7 downto 0) := X"C7";  -- erase bulk 
constant RSID  : std_logic_vector (7 downto 0) := X"AB";  -- read silicon ID


-- Command
constant CMD_NOP  : std_logic_vector (2 downto 0) := "000"; -- NOP
constant CMD_EB   : std_logic_vector (2 downto 0) := "001"; -- Erase Bulk         - das gesamte Flash loeschen
constant CMD_ES   : std_logic_vector (2 downto 0) := "010"; -- Erase Sector       - nur den an 'DataAddress' angegeben Sektor loeschen
constant CMD_WSB  : std_logic_vector (2 downto 0) := "011"; -- Write single bytes - es wird jeweils nur ein einzelnes Bytes (an DataAddress) ins Flash geschrieben
constant CMD_WC   : std_logic_vector (2 downto 0) := "100"; -- Write continuously - es wird solange geschrieben, bis ExecuteCommand wieder low wird
constant CMD_RSB  : std_logic_vector (2 downto 0) := "101"; -- Read single bytes  - es wird jeweils nur ein einzelnes Bytes (an DataAddress) aus dem Flash gelesen
constant CMD_RC   : std_logic_vector (2 downto 0) := "110"; -- Read continuously  - es wird solange gelesen, bis ExecuteCommand wieder low wird
constant CMD_INIT : std_logic_vector (2 downto 0) := "111";

begin

   pEpcsControl : process( Clock, Reset )

   begin
      if (Reset = '1') then
         ReadData                   <= (others => '0');
         Busy                       <= '0';
         sState                     <= START;
         sNextState                 <= START;
         sAddressByteArrayIndex     <= 2;
         sCountTicksForWriting      <= cCountTicksForWritingMaxValue - 1;
         sCountTicksForReading      <= cCountTicksForReadingMaxValue - 1;
         sCountTicksFornCSHigh      <= cCountTicksFornCSHigh;     --entspr. min. 100ns
         sPutByteToFlashBitIndex    <= 7;                         --start mit MSB
         sGetByteFromFlashBitIndex  <= 7;                         --start mit MSB
         sPutByteToFlash            <= (others => '0');
         sGetByteFromFlash          <= (others => '0');
         sDataAddress               <= (others => '0');
         sAddressByteArray(0)       <= (others => '0');
         sAddressByteArray(1)       <= (others => '0');
         sAddressByteArray(2)       <= (others => '0');
         sWriteData                 <= (others => '0');
         sIncrementPage             <= '0';
         sCommand                   <= (others => '0');
         sExecuteCommand            <= '0';
         sWriteDataEnable           <= '0';
         sReadDataEnable            <= '0';
         sLSBFirst                  <= '0';
         TO_EPCS_nCS                <= '1';
         TO_EPCS_ASDI               <= '0';
         TO_EPCS_DCLK               <= '1';

      elsif (rising_edge(Clock)) then

         sCommand          <= Command;
         sExecuteCommand   <= ExecuteCommand;
         sWriteDataEnable  <= WriteDataEnable;
         sReadDataEnable   <= ReadDataEnable;
         sLSBFirst         <= LSBFirst;

         if ((sCommand = CMD_NOP) AND (sExecuteCommand = '1')) then
            ReadData                   <= (others => '0');
            Busy                       <= '0';
            sState                     <= START;
            sNextState                 <= START;
            sAddressByteArrayIndex     <= 2;
            sCountTicksForWriting      <= cCountTicksForWritingMaxValue - 1;
            sCountTicksForReading      <= cCountTicksForReadingMaxValue - 1;
            sCountTicksFornCSHigh      <= cCountTicksFornCSHigh;     --entspr. min. 100ns
            sPutByteToFlashBitIndex    <= 7;                         --start mit MSB
            sGetByteFromFlashBitIndex  <= 7;                         --start mit MSB
            sPutByteToFlash            <= (others => '0');
            sGetByteFromFlash          <= (others => '0');
            sDataAddress               <= (others => '0');
            sAddressByteArray(0)       <= (others => '0');
            sAddressByteArray(1)       <= (others => '0');
            sAddressByteArray(2)       <= (others => '0');
            sWriteData                 <= (others => '0');
            sIncrementPage             <= '0';
            TO_EPCS_nCS                <= '1';
            TO_EPCS_ASDI               <= '0';
            TO_EPCS_DCLK               <= '1';
         else

            case (sState) is
   -------------------------------------------------------------------------------------------------              
   ---------------------------|| START  SEQUENZ ||--------------------------------------------------
   -------------------------------------------------------------------------------------------------  
               ----------------------------------------------------------------------------
               when START =>

                  if (sExecuteCommand = '1') then

                     case (sCommand) is

                        ----------------------------------------------------------------------------
                        when CMD_NOP   =>                         -- Kein Kommando

                           sState      <= START;

                        ----------------------------------------------------------------------------
                        when CMD_EB =>                            -- Erase Bulk

                           sState      <= START_ERASE;

                        ----------------------------------------------------------------------------
                        when CMD_ES =>                            -- Erase Sector

                           sState      <= START_ERASE;

                        ----------------------------------------------------------------------------
                        when CMD_RSB =>                           -- Read single bytes, Read page

                           sState      <= START_READ_WRITE;

                        ----------------------------------------------------------------------------
                        when CMD_WSB =>                           -- Write single bytes, Write page

                           sState      <= START_READ_WRITE;

                        ----------------------------------------------------------------------------
                        when CMD_RC =>                            -- Read continuously

                           sState      <= START_READ_WRITE;

                        ----------------------------------------------------------------------------
                        when CMD_WC =>                            -- Write continuously

                           sState      <= START_READ_WRITE;

                        ----------------------------------------------------------------------------
                        when others    =>

                           sState      <= START;

                     end case;

                  else
                     sState <= START;
                  end if;

               ----------------------------------------------------------------------------
               when START_ERASE =>

                  TO_EPCS_nCS             <= '0';                             -- TO_EPCS_nCS auf Low
                  sDataAddress            <= DataAddress;                     -- Schreibadresse sichern (fuer Erase Sector)
                  Busy                    <= '1';                             -- Busy auf high
                  sState                  <= ERASE_SET_WR_ENABLE_OPCODE;

               ----------------------------------------------------------------------------
               when START_READ_WRITE   =>

                  if (sExecuteCommand = '1') then

                     if ( (sCommand = CMD_RSB) OR (sCommand = CMD_RC) )then      -- starte READING
                           sDataAddress      <= DataAddress;                     -- Leseadresse sichern
                           TO_EPCS_nCS       <= '0';                             -- TO_EPCS_nCS auf low
                           Busy              <= '1';                             -- Busy auf high
                           sState            <= READ_SET_OPCODE;

                     elsif ( (sCommand = CMD_WSB) OR (sCommand = CMD_WC) ) then  -- starte WRITING

                           sDataAddress      <= DataAddress;                     -- Schreibadresse sichern
                           TO_EPCS_nCS       <= '0';                             -- TO_EPCS_nCS auf Low
                           Busy              <= '1';                             -- Busy auf high
                           sState            <= WRITE_SET_ENABLE_OPCODE;
                     else
                        sState            <= START_READ_WRITE;
                     end if;

                  else
                     sState            <= START;
                  end if;

               ----------------------------------------------------------------------------
               when START_READ_WRITE_ERASE_END =>

                  TO_EPCS_nCS             <= '1';                             -- TO_EPCS_nCS auf high (deselekt)
                  Busy                    <= '0';                             -- Busy auf low

                  if ((sExecuteCommand = '1') OR (sReadDataEnable = '1') OR (sWriteDataEnable = '1'))then
                     sState <= START_READ_WRITE_ERASE_END;
                  else
                     sState <= START;
                  end if;


   -------------------------------------------------------------------------------------------------
   ----------------------------|| READ  SEQUENZ ||------ alle zugehoerigen STATES beginnen mit READ_
   -------------------------------------------------------------------------------------------------
               ----------------------------------------------------------------------------
               when READ_SET_OPCODE =>

                  sAddressByteArray(0) <=  sDataAddress(7 downto 0);
                  sAddressByteArray(1) <=  sDataAddress(15 downto 8);
                  sAddressByteArray(2) <=  sDataAddress(23 downto 16);

                  sPutByteToFlash      <= RB;                                       -- Parameter fuer sPutByteToFlash (READ_BYTES)
                  sState               <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;-- ausgeben
                  sNextState           <= READ_SET_ADR;                             -- State nach OPCODE Ausgabe

               ----------------------------------------------------------------------------
               when READ_SET_ADR =>                                                 -- Adresse ausgeben

                  sPutByteToFlash  <= sAddressByteArray(sAddressByteArrayIndex);    -- Parameter fuer sPutByteToFlash (n-tes Adressbytes)

                  if (sAddressByteArrayIndex > 0) then
                     sAddressByteArrayIndex  <= sAddressByteArrayIndex - 1;         -- Parameter fuer sPutByteToFlash (n+1 Adressbytes)
                     sNextState              <= READ_SET_ADR;                       -- State nach Adressausgabe
                  else
                     sAddressByteArrayIndex  <= 2;
                     sNextState              <= READ_WAIT_FOR_READ_DATA_ENABLE_LOW; -- State nach Adressausgabe
                  end if;

                  sState            <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;   -- Adressbyte ausgeben

               ----------------------------------------------------------------------------
               when READ_START_READING =>

                  sState               <= GET_BYTE_FROM_FLASH_START_SET_DCLK;       -- Byte lesen
                  sNextState           <= READ_RECEIVE_DATA;

               ----------------------------------------------------------------------------
               when READ_RECEIVE_DATA =>

                  if (sLSBFirst = '1') then
                     ReadData(0) <= sGetByteFromFlash(7);                           -- Byte ausgeben (LSB first)
                     ReadData(1) <= sGetByteFromFlash(6);
                     ReadData(2) <= sGetByteFromFlash(5);
                     ReadData(3) <= sGetByteFromFlash(4);
                     ReadData(4) <= sGetByteFromFlash(3);
                     ReadData(5) <= sGetByteFromFlash(2);
                     ReadData(6) <= sGetByteFromFlash(1);
                     ReadData(7) <= sGetByteFromFlash(0);
                  else
                     ReadData    <= sGetByteFromFlash;                              -- Byte ausgeben (MSB first)
                  end if;

                  sState         <= READ_WAIT_FOR_READ_DATA_ENABLE_LOW;

               ----------------------------------------------------------------------------
               when READ_WAIT_FOR_READ_DATA_ENABLE_LOW =>

                  Busy                 <= '0';                                      -- Busy auf low

                  if (sReadDataEnable = '1') then                                   -- ReadDataEnable ist noch aktiv -> warten auf low
                     sState    <= READ_WAIT_FOR_READ_DATA_ENABLE_LOW;
                  else
                     if (sExecuteCommand = '1') then                                -- Kommando ausfuehren ist noch aktiv
                        sState         <= READ_WAIT_FOR_READ_DATA_ENABLE_HIGH;      -- auf 'ReadDataEnable' warten
                     else
                        sState      <= START_READ_WRITE_ERASE_END;
                     end if;
                  end if;

               ----------------------------------------------------------------------------
               when READ_WAIT_FOR_READ_DATA_ENABLE_HIGH =>                          -- auf 'ReadDataEnable' high warten

                  if (sExecuteCommand = '1') then                                   -- Kommando ausfuehren ist noch aktiv
                     if (sReadDataEnable = '0') then                                -- ReadDataEnable wird wieder high
                        sState         <= READ_WAIT_FOR_READ_DATA_ENABLE_HIGH;      -- warten
                     else
                        Busy           <= '1';                                      -- Busy auf High
                        sState         <= READ_START_READING;                       -- naechstes Byte lesen
                     end if;
                  else
                     sState            <= START_READ_WRITE_ERASE_END;
                     TO_EPCS_nCS       <= '1';                                      -- TO_EPCS_nCS auf high
                  end if;


   ----------// end of READ SEQUENZ

   -------------------------------------------------------------------------------------------------
   ---------------------|| WRITE SEQUENZ ||------  alle zugehoerigen STATES beginnen mit WRITE_ ----
   -------------------------------------------------------------------------------------------------
               ----------------------------------------------------------------------------
               when WRITE_SET_ENABLE_OPCODE =>

                  sPutByteToFlash      <= WE;                                          -- Parameter fuer sPutByteToFlash (WRITE_ENABLE)
                  sState               <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;   -- OPCODE ausgeben
                  sNextState           <= WRITE_ENABLE_nCS_HIGH;                       -- State nach OPCODE Ausgabe

               ----------------------------------------------------------------------------
               when WRITE_ENABLE_nCS_HIGH =>                                           -- TO_EPCS_nCS <= '1' fuer kurzue Zeit -> noetig nach WRITE_ENABLE

                  TO_EPCS_nCS    <= '1';                                               -- TO_EPCS_nCS auf High
                  sState         <= REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME;
                  sNextState     <= WRITE_SET_WRITE_OPCODE;

               ----------------------------------------------------------------------------
               when WRITE_SET_WRITE_OPCODE =>

                  sPutByteToFlash      <= WB;                                          -- Parameter fuer sPutByteToFlash (WRITE_BYTES)
                  sAddressByteArray(0) <= sDataAddress(7 downto 0);
                  sAddressByteArray(1) <= sDataAddress(15 downto 8);
                  sAddressByteArray(2) <= sDataAddress(23 downto 16);
                  sState               <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;   -- OPCODE ausgeben
                  sNextState           <= WRITE_SET_ADR;                               -- State nach OPCODE Ausgabe

               ----------------------------------------------------------------------------
               when WRITE_SET_ADR =>

                  sPutByteToFlash      <= sAddressByteArray(sAddressByteArrayIndex);   -- Parameter fuer sPutByteToFlash (n-tes Adressbytes)
                  sState               <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;   -- ausgeben

                  if (sAddressByteArrayIndex > 0) then
                     sAddressByteArrayIndex  <= sAddressByteArrayIndex - 1;
                     sNextState              <= WRITE_SET_ADR;                         -- Parameter fuer sPutByteToFlash (n+1 Adressbytes)
                  else                                                                 -- letztes ADR BYTE
                     sAddressByteArrayIndex  <= 2;
                     sNextState              <= WRITE_WAIT_FOR_WRITE_DATA_ENABLE_LOW;  -- State nach Adressbyte Ausgabe
                  end if;

               ----------------------------------------------------------------------------
               when WRITE_DATA =>

                  if (sLSBFirst = '1') then
                     sPutByteToFlash(0)   <= sWriteData(7);                            -- Parameter fuer sPutByteToFlash (sWriteData) LSB first
                     sPutByteToFlash(1)   <= sWriteData(6);
                     sPutByteToFlash(2)   <= sWriteData(5);
                     sPutByteToFlash(3)   <= sWriteData(4);
                     sPutByteToFlash(4)   <= sWriteData(3);
                     sPutByteToFlash(5)   <= sWriteData(2);
                     sPutByteToFlash(6)   <= sWriteData(1);
                     sPutByteToFlash(7)   <= sWriteData(0);
                  else
                     sPutByteToFlash      <= sWriteData;                               -- Parameter fuer sPutByteToFlash (sWriteData) MSB first
                  end if;

                  if (sCommand = CMD_WC) then                                          -- weitere Datenbyte mit WriteDataEnable als Strobe ausgeben
                     sNextState        <= WRITE_CALCULATE_NEXT_ADR;                    -- neachste Adresse berechnen
                  else
                     sNextState        <= WRITE_nCS_HIGH;                              -- schreiben beenden und Statusregister lesen
                  end if;

                  sState               <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;   -- ausgeben

               ----------------------------------------------------------------------------
               when WRITE_CALCULATE_NEXT_ADR  =>

                  if (sDataAddress (7 downto 0) = X"FF") then                          -- eine Page ist immer dann voll, wenn die unteren 8 Bit der Adresse gesetzt sind
                     sIncrementPage <= '1';
                  else
                     sIncrementPage <= '0';
                  end if;
    
                  sState         <= WRITE_INCREMENT_ADDRESS;

               ----------------------------------------------------------------------------
               when WRITE_INCREMENT_ADDRESS  =>

                  sDataAddress         <= std_logic_vector(unsigned(sDataAddress) + 1);-- Adresse +1
                  sState               <= WRITE_POSSIBLY_CHANGE_SECTOR;

               ----------------------------------------------------------------------------
               when WRITE_POSSIBLY_CHANGE_SECTOR   =>                                  -- ggf. den Sektor wechseln

                  if(sIncrementPage = '1') then                                        -- neue Page per OPCODE mitteilen
                     sState            <= WRITE_nCS_HIGH;                              -- aktuelle Page abschliessen
                  else
                     sState            <= WRITE_WAIT_FOR_WRITE_DATA_ENABLE_LOW;        -- direkt auf WriteDataEnable low warten um naechstes Byte zu schrieben
                  end if;

               ----------------------------------------------------------------------------
               when WRITE_WAIT_FOR_WRITE_DATA_ENABLE_LOW =>

                  if(sIncrementPage = '1') then                                        -- neue Page per OPCODE mitteilen
                     sIncrementPage    <= '0';
                     TO_EPCS_nCS       <= '0';                                         -- TO_EPCS_nCS wieder auf low (wird zwischendurch wegen lesen des Status high)
                     sState            <= WRITE_SET_ENABLE_OPCODE;                     -- neuen Sector klar machen
                  else

                     Busy  <= '0';                                                     -- Busy auf low

                     if (sWriteDataEnable = '1') then                                  -- WriteDataEnable ist noch aktiv -> warten auf low
                        sState    <= WRITE_WAIT_FOR_WRITE_DATA_ENABLE_LOW;
                     else
                        if (sExecuteCommand = '1') then                                -- Kommando ausfuehren ist noch aktiv
                           sState   <= WRITE_WAIT_FOR_WRITE_DATA_ENABLE_HIGH;          -- auf WriteDataEnable high warten
                        else
                           sState   <= START_READ_WRITE_ERASE_END;
                        end if;

                     end if;

                  end if;

               ----------------------------------------------------------------------------
               when WRITE_WAIT_FOR_WRITE_DATA_ENABLE_HIGH =>                           -- auf 'WriteDataEnable' high warten      

                  if (sExecuteCommand = '1') then                                      -- Kommando ausfuehren ist noch aktiv

                     if (sWriteDataEnable = '1') then                                  -- WriteDataEnable wird wieder high

                        sWriteData        <= WriteData;                                -- zu schreibende Daten einlesen
                        Busy              <= '1';                                      -- Busy auf High
                        sState            <= WRITE_DATA;                               -- nur Byte schreiben

                     else
                        sState               <= WRITE_WAIT_FOR_WRITE_DATA_ENABLE_HIGH; -- warten
                     end if;
                  else                                                                 -- Kommando wird abgebrochen, Daten werden im Flash gespeichert...
                     Busy     <= '1';                                                  -- ...dazu Busy auf '1' und ...
                     sState   <= WRITE_nCS_HIGH;                                       -- ...folgend das Statusregister abfragen
                  end if;

               ----------------------------------------------------------------------------
               when WRITE_nCS_HIGH =>

                  TO_EPCS_nCS       <= '1';                                            -- TO_EPCS_nCS auf High
                  sState            <= REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME;
                  sNextState        <= WRITE_SET_READ_STATUS_OPCODE;

               ----------------------------------------------------------------------------
               when WRITE_SET_READ_STATUS_OPCODE =>

                  sPutByteToFlash   <= RDSR;                                           -- Parameter fuer sPutByteToFlash (READ_STATUS_REGISTER)
                  sState            <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;      -- ausgeben
                  sNextState        <= WRITE_READ_STATUS;                              -- State nach OPCODE Ausgabe

               ----------------------------------------------------------------------------
               when WRITE_READ_STATUS =>

                  sState            <= GET_BYTE_FROM_FLASH_START_SET_DCLK;
                  sNextState        <= WRITE_PROOF_STATUS;

               ----------------------------------------------------------------------------
               when WRITE_PROOF_STATUS =>

                  if (sGetByteFromFlash(0) = '0') then                                 -- Busy Bits im Status geloescht
                     TO_EPCS_nCS       <= '1';                                         -- TO_EPCS_nCS auf high
                     if (sCommand = CMD_WC) then                                       -- weitere Byte schreiben
                        sNextState  <= WRITE_WAIT_FOR_WRITE_DATA_ENABLE_LOW;           -- war 'sState', dazu muss per WriteDataEnable ein neues Byte vom Host kommen
                        sState      <= REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME;              -- NEU: nCS muss etwas High bleiben
                     else
                        sState         <= START_READ_WRITE_ERASE_END;                  -- schreiben beenden
                     end if;
                  else                                                                 -- Scheibvorgang noch nicht abgeschlossen, weiter Statusregister testen
                     sState            <= WRITE_READ_STATUS;
                  end if;


   ----------// end of WRITE SEQUENZ

   -----------------------------------------------------------------------------------------------
   --------------------||| ERASE SEQUENCE ||| -----------------------------------------------------
   --------------------| loescht entweder nur den Sektor in dem sich 'DataAddress' befindet oder das gesamte Flash  |----------------------------------
   --------------------| noetig um neue Daten ins Flash zu programmieren  |----------------------------------
               ----------------------------------------------------------------------------
               when ERASE_SET_WR_ENABLE_OPCODE =>

                  sPutByteToFlash      <= WE;                                          -- Parameter fuer sPutByteToFlash (WRITE_ENABLE)
                  sState               <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;   -- OPCODE ausgeben
                  sNextState           <= ERASE_nCS_HIGH_AFTER_WR_ENABLE_OPCODE;       -- State nach OPCODE Ausgabe

               ----------------------------------------------------------------------------
               when ERASE_nCS_HIGH_AFTER_WR_ENABLE_OPCODE =>                           -- TO_EPCS_nCS <= '1' fuer kurzue Zeit -> noetig nach WRITE_ENABLE

                  TO_EPCS_nCS          <= '1';                                         -- TO_EPCS_nCS auf High
                  sState               <= REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME;
                  sNextState           <= ERASE_SET_ERASE_OPCODE;

               ----------------------------------------------------------------------------------------------
               --Was hier noch fehlt ist das Lesen von StatusRegister(1) ob WEL-Bit im Anschluss gesetzt ist
               ----------------------------------------------------------------------------------------------

               ----------------------------------------------------------------------------
               when ERASE_SET_ERASE_OPCODE =>

                  if (sCommand = CMD_EB) then                                          -- komplettes Flash loeschen
                     sPutByteToFlash      <= EB;                                       -- Parameter fuer sPutByteToFlash (ERASE_BULK
                     sState               <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;-- OPCODE ausgeben
                     sNextState           <= ERASE_nCS_HIGH;                           -- State nach OPCODE Ausgabe
                  elsif (sCommand = CMD_ES) then
                     sPutByteToFlash      <= ES;                                       -- Parameter fuer sPutByteToFlash (ERASE_SECTOR)
                     sAddressByteArray(0) <= sDataAddress(7 downto 0);
                     sAddressByteArray(1) <= sDataAddress(15 downto 8);
                     sAddressByteArray(2) <= sDataAddress(23 downto 16);
                     sState               <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;-- OPCODE ausgeben
                     sNextState           <= ERASE_SET_ADR;                            -- State nach OPCODE Ausgabe
                  else
                     sState               <= START_READ_WRITE_ERASE_END;               -- falsches Kommando, nichts tun
                  end if;

               ----------------------------------------------------------------------------
               when ERASE_SET_ADR =>

                  sPutByteToFlash      <= sAddressByteArray(sAddressByteArrayIndex);   -- Parameter fuer sPutByteToFlash (n-tes Adressbytes)
                  sState               <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;   -- ausgeben

                  if (sAddressByteArrayIndex > 0) then
                     sAddressByteArrayIndex  <= sAddressByteArrayIndex - 1;
                     sNextState              <= ERASE_SET_ADR;                         -- Parameter fuer sPutByteToFlash (n+1 Adressbytes)
                  else                                                                 -- letztes ADR BYTE
                     sAddressByteArrayIndex  <= 2;
                     sNextState              <= ERASE_nCS_HIGH;                        -- State nach Adressbyte Ausgabe
                  end if;

               ----------------------------------------------------------------------------
               when ERASE_nCS_HIGH =>

                  TO_EPCS_nCS       <= '1';                                            -- TO_EPCS_nCS auf High
                  sState            <= REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME;
                  sNextState        <= ERASE_SET_READ_STATUS_OPCODE;

               ----------------------------------------------------------------------------
               when ERASE_SET_READ_STATUS_OPCODE =>

                  sPutByteToFlash   <= RDSR;                                           -- Parameter fuer sPutByteToFlash (READ_STATUS_REGISTER)
                  sState            <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;      -- OPCODE ausgeben
                  sNextState        <= ERASE_READ_STATUS;                              -- State nach OPCODE Ausgabe

               ----------------------------------------------------------------------------
               when ERASE_READ_STATUS =>

                  sState            <= GET_BYTE_FROM_FLASH_START_SET_DCLK;
                  sNextState        <= ERASE_PROOF_STATUS;

               ----------------------------------------------------------------------------
               when ERASE_PROOF_STATUS =>

                  if (sGetByteFromFlash(0) = '0') then                                 -- WIP(WriteInProgress)-Bit im Statusregister ist NICHT mher gesetzt -> Loeschen ist beendet
                     TO_EPCS_nCS          <= '1';                                      -- TO_EPCS_nCS auf High
                     sState               <= ERASE_END_nCS_HIGH;                       -- Loeschvorgang beendet
                  else                                                                 -- Loeschvorgang nicht abgeschlossen, weiter Statusregister testen
                     sState               <= ERASE_READ_STATUS;
                  end if;

               ----------------------------------------------------------------------------
               when ERASE_END_nCS_HIGH =>

                  TO_EPCS_nCS    <= '1';                                               -- TO_EPCS_nCS auf High

                  if(sCountTicksFornCSHigh > 0) then
                     sCountTicksFornCSHigh <= sCountTicksFornCSHigh - 1;
                     sState                     <= ERASE_END_nCS_HIGH;
                  else
                     sCountTicksFornCSHigh      <= cCountTicksFornCSHigh;
                     sState                     <= START_READ_WRITE_ERASE_END;
                  end if;

   ----------// end of ERASE SEQUENZ


   -----------------------------------------------------------------------------------------------
   --------------------||| PUT_BYTE_TO_FLASH SEQUENCE |||------------------------------------------------
   --------------------| sendet ein Byte gespeichert in "sPutByteToFlash" ans Flash |--------------------
   --------------------| und springt danach in den "sState" gespeichert in "sNextState" |----------------
   --------------------| Datenbits werden mit steigender DCLK Flanke in den EPCS eingelesen |------------

               ----------------------------------------------------------------------------
               when PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA =>        

                  TO_EPCS_DCLK               <= '0';                                            -- TO_EPCS_DCLK auf Low
                  TO_EPCS_ASDI               <= sPutByteToFlash(sPutByteToFlashBitIndex);       -- Bit ausgeben

                  if (sCountTicksForWriting > 0) then                                           -- warten
                     sCountTicksForWriting <= sCountTicksForWriting - 1;
                     sState                <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;
                  else
                     sCountTicksForWriting <= cCountTicksForWritingMaxValue - 1;
                     sState                <= PUT_BYTE_TO_FLASH_STATE2_SET_DCLK;
                  end if;              

               ----------------------------------------------------------------------------
               when PUT_BYTE_TO_FLASH_STATE2_SET_DCLK =>

                  TO_EPCS_DCLK         <= '1';                                                  -- TO_EPCS_DCLK auf High (EPCS liest Daten ein)

                  if (sCountTicksForWriting > 0) then                                           -- warten
                     sCountTicksForWriting      <= sCountTicksForWriting - 1;
                     sState                     <= PUT_BYTE_TO_FLASH_STATE2_SET_DCLK;
                  else                                                                          -- wechsle sState
                     sCountTicksForWriting      <= cCountTicksForWritingMaxValue - 1;

                     if (sPutByteToFlashBitIndex > 0) then
                        sPutByteToFlashBitIndex <= sPutByteToFlashBitIndex - 1;
                        sState                  <= PUT_BYTE_TO_FLASH_STATE1_CLR_DCLK_WR_DATA;
                     else                                                                       -- Byte geschrieben
                        sPutByteToFlashBitIndex <= 7;                                           -- Indizierung der naechsten Schreibsequenz mit MSB
                        sState                  <= sNextState;                                  -- Zurueck in die Hauptsequenz
                     end if;
                  end if;

   ------------- // end of PUT_BYTE_TO_FLASH SEQUENCE

   -------------------------------------------------------------------------------------------------
   --------------------||| GET_BYTE_FROM_FLASH SEQUENCE |||---------------------------------------------
   --------------------| Empfaengt ein Byte aus dem Flash |---------------------------------------------
   --------------------| und springt danach in den "sState" gespeichert in "sNextState" |----------------
   --------------------| Datenbits werden mit fallender DCLK Flanke aus dem EPCS ausgegeben

               ----------------------------------------------------------------------------
               when GET_BYTE_FROM_FLASH_START_SET_DCLK =>

                  TO_EPCS_DCLK         <= '1';                                         -- TO_EPCS_DCLK auf High

                  if (sCountTicksForReading > 0) then                                  -- warten
                     sCountTicksForReading    <= sCountTicksForReading - 1;
                     sState                   <= GET_BYTE_FROM_FLASH_START_SET_DCLK;
                  else
                     sCountTicksForReading    <= cCountTicksForReadingMaxValue - 1;
                     sState                   <= GET_BYTE_FROM_FLASH_STATE1_CLR_DCLK;
                  end if;

               ----------------------------------------------------------------------------
               when GET_BYTE_FROM_FLASH_STATE1_CLR_DCLK =>   

                  TO_EPCS_DCLK         <= '0';                                         -- TO_EPCS_DCLK auf Low  (EPCS gibt Daten aus)

                  if (sCountTicksForReading > 0) then                                  -- warten
                     sCountTicksForReading   <= sCountTicksForReading - 1;
                     sState                  <= GET_BYTE_FROM_FLASH_STATE1_CLR_DCLK;
                  else
                     sCountTicksForReading   <= cCountTicksForReadingMaxValue - 1;
                     sState                  <= GET_BYTE_FROM_FLASH_STATE2_SET_DCLK_RD_DATA;
                  end if;

               ----------------------------------------------------------------------------
               when GET_BYTE_FROM_FLASH_STATE2_SET_DCLK_RD_DATA =>

                  TO_EPCS_DCLK                                 <= '1';                 -- TO_EPCS_DCLK auf High
                  sGetByteFromFlash(sGetByteFromFlashBitIndex) <= FROM_EPCS_DATA;      -- Bit lesen

                  if (sCountTicksForReading > 0) then                                  -- warten
                     sCountTicksForReading   <= sCountTicksForReading - 1;
                     sState                  <= GET_BYTE_FROM_FLASH_STATE2_SET_DCLK_RD_DATA;
                  else
                     sCountTicksForReading   <= cCountTicksForReadingMaxValue - 1;

                     if (sGetByteFromFlashBitIndex > 0) then
                        sGetByteFromFlashBitIndex  <= sGetByteFromFlashBitIndex - 1;
                        sState                     <= GET_BYTE_FROM_FLASH_STATE1_CLR_DCLK;
                     else
                        sGetByteFromFlashBitIndex  <= 7;                               -- Indizierung der naechsten Lesesequenz mit MSB
                        sState                     <= sNextState;                      -- Zurueck in die Hauptsequenz
                     end if;
                  end if;     

   ----------- // end of GET_BYTE_FROM_FLASH

   -------------------------------------------------------------------------------------------------
   --------------------||| REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME SEQUENCE |||---------------------------------------------------
   --------------------| belaesst das TO_EPCS_nCS Signal fuer kurze Zeit auf High um eine Operation zu starten (nach OPCODE senden) |----------------------------
   --------------------| und springt danach in den "sState" gespeichert in "sNextState" |----------------             
               ----------------------------------------------------------------------------
               when REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME =>

                  if(sCountTicksFornCSHigh > 0) then                                   -- verharren in sState (noetig fuer TO_EPCS_nCS Delay) -> Benutzung von 'sCountTicksForReading' ist Zufallswahl
                     sCountTicksFornCSHigh <= sCountTicksFornCSHigh - 1;
                     sState                     <= REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME;
                  else
                     sCountTicksForReading      <= cCountTicksForReadingMaxValue - 1;
                     sCountTicksForWriting      <= cCountTicksForWritingMaxValue - 1;
                     sCountTicksFornCSHigh      <= cCountTicksFornCSHigh;
                     TO_EPCS_nCS                <= '0';                                -- TO_EPCS_nCS auf Low
                     sState                     <= sNextState;

                  end if;

                  
   ----------- // end of REMAIN_nCS_ON_HIGH_FOR_SHORT_TIME

            end case;
         end if;
      end if;
   end process pEpcsControl;

end RTL ;