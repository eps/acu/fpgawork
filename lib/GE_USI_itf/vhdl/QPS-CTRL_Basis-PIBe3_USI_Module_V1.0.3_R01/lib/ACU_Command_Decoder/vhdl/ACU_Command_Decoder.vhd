--  Description:
--  15.05.12   born
--  dd.mm.yy   Das 'Command' Kommando wurde dauerhaft an den Ausgaengen 'On, Off, Reset, NU' ausgegeben bis ein neue Kommando kam. 
--             Die Ausgabe erfolgt nun nur noch, solange das Kommando am Eingang 'Command' ansteht, danach fallen alle Ausgaenge auf '0' zurueck.
--  20.06.13   alle States muessen alle Ausgabeports abdecken, sonst bleibt bei einem direkten Wechsel von z.B. ON -> OFF das ON Kommando anstehen. (DS)
--  28.05.21   Neues Kommando 'Command_TriggerSomething'. (DS)
--  26.01.22   Multiplexer integriert, Generic ..Version entfernt (DS)

--             Total logic elements          11
--             Total combinational functions 11
--             Dedicated logic registers     9
--             Total registers               9



--============================================================================
 
LIBRARY ieee;                                              --Library Declaration
USE  ieee.std_logic_1164.all; 
USE  ieee.numeric_std.all;

------------------------------------------------------------------------------

ENTITY ACU_Command_Decoder IS                                --ENTITY Declaration
   PORT (  
            Clock                            : in std_logic;
            Reset                            : in std_logic;
            USIInHighSpeedMode               : in std_logic;
            Cmd_USIStandard                  : in std_logic_vector(3 downto 0);
            Cmd_USIHighSpeed                 : in std_logic_vector(3 downto 0);

            Command_ON                       : out std_logic;
            Command_OFF                      : out std_logic;
            Command_Reset                    : out std_logic;
            Command_ControllerLocked         : out std_logic;
            Command_TriggerSomething         : out std_logic
         );

--sCommand
--0      - keine Aktion
--1      - Geraet einschalten
--2      - Geraet ausschalten
--3      - Reset durchfuehren
--4      - Regler sperren (legacy)
--5      - TriggerSomething

Signal   sCommand                            : std_logic_vector(3 downto 0) ;

constant cCMDNoAction                        : std_logic_vector(3 downto 0) := X"0";
constant cCMDSwitchUnitOn                    : std_logic_vector(3 downto 0) := X"1";
constant cCMDSwitchUnitOff                   : std_logic_vector(3 downto 0) := X"2";
constant cCMDResetUnit                       : std_logic_vector(3 downto 0) := X"3";
constant cCMDControllerLocked                : std_logic_vector(3 downto 0) := X"4";
constant cCMDTriggerSomething                : std_logic_vector(3 downto 0) := X"5";

END ACU_Command_Decoder;


ARCHITECTURE RTL OF ACU_Command_Decoder IS

BEGIN

   PROCESS (Reset, Clock, USIInHighSpeedMode)

   BEGIN

   if (Clock'event and rising_edge(Clock))  then

      if (Reset = '1') then

         Command_ON                    <= '0';
         Command_OFF                   <= '0';
         Command_Reset                 <= '0';
         Command_ControllerLocked      <= '0';
         Command_TriggerSomething      <= '0';

      else

         iF USIInHighSpeedMode = '0' then
            sCommand(3 downto 0) <= Cmd_USIStandard(3 downto 0);
         else
            sCommand(3 downto 0) <= Cmd_USIHighSpeed(3 downto 0);
         end if;

         CASE sCommand IS

            when cCMDNoAction =>                --Power Up, kein Kommando
               Command_ON                  <= '0';
               Command_OFF                 <= '0';
               Command_RESET               <= '0';
               Command_ControllerLocked    <= '0';
               Command_TriggerSomething    <= '0';

            when cCMDSwitchUnitOn =>            --ON
               Command_ON                  <= '1';
               Command_OFF                 <= '0';
               Command_RESET               <= '0';
               Command_ControllerLocked    <= '0';
               Command_TriggerSomething    <= '0';
               
            when cCMDSwitchUnitOff =>           --OFF
               Command_ON                  <= '0';
               Command_OFF                 <= '1';
               Command_RESET               <= '0';
               Command_ControllerLocked    <= '0';
               Command_TriggerSomething    <= '0';

            when cCMDResetUnit =>               --RESET
               Command_ON                  <= '0';
               Command_OFF                 <= '0';
               Command_RESET               <= '1';
               Command_ControllerLocked    <= '0';
               Command_TriggerSomething    <= '0';

            when cCMDControllerLocked =>        --Regler sperren
               Command_ON                  <= '0';
               Command_OFF                 <= '0';
               Command_RESET               <= '0';
               Command_ControllerLocked    <= '1';
               Command_TriggerSomething    <= '0';

            when cCMDTriggerSomething =>        --TriggerSomething
               Command_ON                  <= '0';
               Command_OFF                 <= '0';
               Command_RESET               <= '0';
               Command_ControllerLocked    <= '0';
               Command_TriggerSomething    <= '1';

            when others =>                      --default
               Command_ON                  <= '0';
               Command_OFF                 <= '0';
               Command_RESET               <= '0';
               Command_ControllerLocked    <= '0';
               Command_TriggerSomething    <= '0';

         END CASE;

      end if;
   
   end if;

   end PROCESS;

END RTL;







