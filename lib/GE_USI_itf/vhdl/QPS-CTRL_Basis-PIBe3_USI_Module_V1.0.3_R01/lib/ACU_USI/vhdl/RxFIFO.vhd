LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

--V1.0   born

--V1.1   'RxFIFONewDataAtDataOutForMD' eingefuehrt, als 1 Takt Puls Information darueber, dass sich die Daten am
--       Ausgang 'RxFIFODataOut' geaendert haben. Dies dient z.B. der Daten abholenden Instanz
--       'ReceiveData' des mUSIC Moduls als Information, dass die Daten nun gueltig sind. 
--       'sReadEnableEdgeDetArray' von 3 Bit auf 2 Bit reduziert. Die Flankenerkennung ist notwendig, da
--       auch eine CPU ggf. das USI Modul triggern muss. In unseren Falle uebernimmt dies z.B. der NiosII in der MFU.
--       Es reicht aber eine einfache Erkennung, das Jitter innerhalb des FPGA sehr gering sein solten.
--       Einfache Erkennung wegen mUSIC eingefuehrt, damit 'ReceiveData-Modul' nur einen Puls schicken muss. (10.08.10 - DS)
--V1.2   'sRxFIFOnEmpty' wurde nie '0' wenn das oberste Bit im Array zwar wieder geloescht wurde, aber mind. noch ein weiteres
--       Bit im Array gesetzt war, durch ein nachfolgend empf. Byte. Dies war immer dann der Fall, wenn bei hohen Baudraten
--       (Bsp. 10 MBaud) ein grosser RxBuffer (>41 Byte) verwendet wurde. (14.08.12 - DS)
--V2.00  Ports und Signale der einzelnen Module sind teilweise mit neuen Namen versehen um deren Funktion klarer zu machen (15.10.12 - DS)
--V2.01  Der FIFO wird nun nicht nur beim 'Reset', sondern auch bei 'RxFIFOEnable' geloescht. Dies daher, damit ein 'RxFIFOEnable' = '0'
--       im Falle von 'RxFIFOOverflow' = '1' diese Info loeschen kann. 
--       Generell ist der ganze Code nun etwas gestrafft, die unnoetigen Statemachines sind ausgebaut
--       und das M/D <-> USI Handshake auf 1-Takt Pulse reduziert. (07.04.16 - DS)


entity RxFIFO is
   generic 
   (
      gRxFIFOVersion                : real      := 2.01;
      gRxFIFODepth                  : integer   := 32;
      gRxFIFOMaxWidth               : integer   := 8 
   );
   PORT  
   (  Clock                         : in  std_logic;
      Reset                         : in  std_logic;
      RxFIFOEnable                  : in  std_logic;
    
      RxFIFOGetDataFromDataIn       : in  std_logic;
      RxFIFODataFetchedFromDataIn   : out std_logic;
      RxFIFODataIn                  : in  std_logic_vector(gRxFIFOMaxWidth-1 downto 0);
                  
      RxFIFOPutDataToDataOut        : in  std_logic;
      RxFIFODataOut                 : out std_logic_vector(gRxFIFOMaxWidth-1 downto 0);

      RxFIFONewDataAtDataOutForMD   : out std_logic;  --1 Takt Pulse, wenn Daten sich an 'RxFIFODataOut' geaendert haben

      RxFIFOnEmpty                  : out std_logic;  --FIFO nicht leer
      RxFIFOAlmostFull              : out std_logic;  --nur noch ein Speicherplatz frei
      RxFIFOFull                    : out std_logic;  --FIFO voll    
      RxFIFOOverflow                : out std_logic   --Ueberlauf
   );

end RxFIFO;


--Beschreibung
--Daten werden von USI-Rx in den RxFIFO eingetragen durch:
--USI legt empf. Daten an RxFIFODataIn und sendet pos. Flanke an RxFIFOGetDataFromDataIn
--FIFO uebernimmt Daten und legt RxFIFODataFetchedFromDataIn auf High -> Low
--CPU o.a. erhalten Info ueber vorhandene Daten im FIFO mittels RFEmpty, RxFIFOAlmostFull, 
--RxFIFOFull und/oder RxFIFOOverflow.
--Durch pos. Flanke an RxFIFOPutDataToDataOut werden FIFO Daten an RxFIFODataOut gelegt


architecture RTL of RxFIFO is

   signal   sRxFIFOPutDataToDataOut_EdgeDet        : std_logic := '0';

   type     tRxFIFOArray is array (gRxFIFODepth-1 downto 0) of std_logic_vector(gRxFIFOMaxWidth-1 downto 0);
   signal   sRxFIFOArrayPointer                    : tRxFIFOArray;
   
   signal   sRxFIFOArrayPosFilled                  : std_logic_vector(gRxFIFODepth-1  downto 0);

   signal   sRxFIFOFull                            : std_logic := '0';

   signal   sRxFIFOPutDataToDataOut_EdgeDetArray   : std_logic_vector(1 downto 0);

   signal   sResetORnotRxFIFOEnable                : std_logic;

begin

   sResetORnotRxFIFOEnable <= (Reset OR (not RxFIFOEnable));

   --pos. Flanke an RxFIFOPutDataToDataOut (ext. Signal)
   --nur wenn RxFIFOEnable = '1'   
   --CPU fordert Daten aus FIFO an, FIFO legt Daten an RxFIFODataOut
   --====
   --INFO
   --====
   --In V1.1 Flankenerkennung von '011' auf '01' reduziert. Prinzipiell handelt es sich immer
   --um denselben Chip, weswegen die Flankenerkennung nicht den Stellenwert einer
   --extern Triggerung hat. Eigentlich wuerde auch ein 1 Takt Pukls reichen.

   pReadEnableEdgeDet: process (Clock, Reset)
      variable vRxFIFOPutDataToDataOut_EdgeDetArray : std_logic_vector(1 downto 0);
   begin
      if (Reset = '1') then
         vRxFIFOPutDataToDataOut_EdgeDetArray := (others => '0') ;
      elsif (rising_edge(Clock)) then
         if (RxFIFOEnable = '1') then
            vRxFIFOPutDataToDataOut_EdgeDetArray := (vRxFIFOPutDataToDataOut_EdgeDetArray(0) & RxFIFOPutDataToDataOut);
            if (vRxFIFOPutDataToDataOut_EdgeDetArray = "01") then --steigende Flanke
               sRxFIFOPutDataToDataOut_EdgeDet <= '1' ;
            else
               sRxFIFOPutDataToDataOut_EdgeDet <= '0' ;
            end if ;
         end if;
     end if ;    
   end process pReadEnableEdgeDet;

   --FIFO
   pRxFIFO : process (Clock, sResetORnotRxFIFOEnable)
   begin

      if (sResetORnotRxFIFOEnable = '1') then

         for i in 0 to gRxFIFODepth - 1 loop
            sRxFIFOArrayPointer(i) <= (others => '0');
         end loop;

         sRxFIFOArrayPosFilled         <= (others => '0'); 
         RxFIFODataOut                 <= (others => '0'); 
         RxFIFODataFetchedFromDataIn   <= '0';
         RxFIFOOverflow                <= '0';

      elsif (rising_edge(Clock)) then

         --#########################################################################################
         -- Daten einlesen
         -- ==============
         -- Kommt der 1 Takt Pulse 'RxFIFOGetDataFromDataIn = '1'' und ist der FIFO nicht voll, werden die Eingangsdaten
         -- uebernommen und in den FIFO geschrieben. 
         -- Dies wird mit einem Puls an 'RxFIFODataFetchedFromDataIn' quittiert.
         --#########################################################################################

         RxFIFODataFetchedFromDataIn   <= '0';

         --Daten einlesen
         if (RxFIFOGetDataFromDataIn = '1') then
            if (sRxFIFOArrayPosFilled(0) = '0') then                                --Daten einlesen
               sRxFIFOArrayPointer(0)(gRxFIFOMaxWidth-1 downto 0) <= RxFIFODataIn;
               sRxFIFOArrayPosFilled(0)                           <= '1';
               RxFIFOOverflow                                     <= '0';
               RxFIFODataFetchedFromDataIn                        <= '1';
            elsif (sRxFIFOFull = '1') then                                          -- RxFIFOOverflow Flag
               RxFIFOOverflow                                     <= '1';                               
            end if;
         end if;

         --Daten schieben
         for i in 1 to gRxFIFODepth - 1  loop
            if (sRxFIFOArrayPosFilled(i-1) = '1' AND sRxFIFOArrayPosFilled(i) = '0') then 
               sRxFIFOArrayPointer(i)(gRxFIFOMaxWidth-1 downto 0) <= sRxFIFOArrayPointer(i-1)(gRxFIFOMaxWidth-1 downto 0); 
               sRxFIFOArrayPosFilled(i)   <= '1'; 
               sRxFIFOArrayPosFilled(i-1) <= '0'; 
            end if;
         end loop;      

         --Daten an CPU, M/D o.a. ausgeben
         if (sRxFIFOPutDataToDataOut_EdgeDet = '1' and sRxFIFOArrayPosFilled(gRxFIFODepth - 1) = '1') then
            RxFIFODataOut                             <= sRxFIFOArrayPointer(gRxFIFODepth - 1)(gRxFIFOMaxWidth-1 downto 0);
            RxFIFONewDataAtDataOutForMD               <= '1';  -- Pulse
            sRxFIFOArrayPosFilled(gRxFIFODepth - 1)   <= '0';  -- Positions-Fuellanzeige loeschen -> damit in 'Daten schieben' geschoben wird
         else
            RxFIFONewDataAtDataOutForMD               <= '0';  -- Pulse
         end if;
         
      end if;
      
   end process pRxFIFO;

      -- INFO
   -- ===================
   -- Das Signal 'RxFIFOnEmpty' wird z.B. vom NiosII als Interruptquelle genutzt.
   -- Dieser Interrupt ist Flankengetriggert (steigende Flanke).
   -- Wird nun ein Wert aus dem RxFIFO abgeholt und ist der FIFO anschliessend NICHT leer,
   -- bleibt dieses Signal '1'. Das sorgt aber dafür, dass im NiosII die ISR zur Abholung der
   -- Daten nicht erneut aufgerufen wird. Es verbleiben also alle weiteren Daten im FIFO.
   -- Aus diesem Grund muss dieses Signal im Moment der Datenabholung fuer mind. einen Takt auf '0'
   -- und danach ggf. (sofern weitere Daten im FIFO sind) wieder auf '1' wechseln.
   -- Im HW-FIFO wird das Signal 'RxFIFOnEmpty' immer dann '1', wenn im Logik Schieberegister
   -- die oberste Position mit gueltigen Daten zur Abholung bereit gefuellt ist.
   -- Werden die Daten abgeholt, wird die Position zunaecht geleert. Das Signal 'RxFIFONewDataAtDataOutForMD'
   -- wird '1' und im naechsten Takt 'RxFIFOnEmpty' durch die nun leer oberste Position kurzzeitig '0'. 
   -- Danach wird ggf. der naechste Wert im HW-FIFO an die oberste Position nachgeschoben und 'RxFIFOnEmpty' wieder '1'.

   -- Vergleich zum RAM-FIFO
   -- ================================
   -- Das interne Signal 'sRxFIFOReadData' sorgt dafuer, dass die neuen Daten an den Ausgang 'RxFIFODataOut'
   -- gelegt werden. Das 'sRxFIFOReadData' Signal ist nur einen Takt lang und wird invers mit
   -- dem ebenfalls inversen 'sRxFIFOEmpty' UND verknuepft und als 'RxFIFOnEmpty' ausgegeben.
   -- Dadurch wird 'RxFIFOnEmpty' jedesmal einen Takt nach 'sRxFIFOReadData' sicher '0'. Dies geschieht
   -- zeitglich mit dem Signal dann fuer eine Taklt auf '1' stehenden Signal 'RxFIFONewDataAtDataOutForMD'.

   -- RFnEmpty Flag
   pRxFIFOnEmpty : process(Clock, sResetORnotRxFIFOEnable)
   begin
      if (sResetORnotRxFIFOEnable = '1') then
         RxFIFOnEmpty <= '0';
      elsif (rising_edge(Clock)) then
         if (sRxFIFOArrayPosFilled(gRxFIFODepth -1) = '1') then   --FIFO nicht leer
            RxFIFOnEmpty <= '1';                                  --Trigger fuer CPU
         else
            RxFIFOnEmpty <= '0';
         end if;
      end if;
   end process pRxFIFOnEmpty;
   
   -- RxFIFOAlmostFull Flag (Voll -1)
   pRxFIFOAlmostFull : process(Clock, sResetORnotRxFIFOEnable)
   begin
      if (sResetORnotRxFIFOEnable = '1') then
         RxFIFOAlmostFull <= '0';
      elsif (rising_edge(Clock)) then
         if (sRxFIFOArrayPosFilled(2) = '1' AND sRxFIFOArrayPosFilled(1) = '1' AND sRxFIFOArrayPosFilled(0) = '0') then
           RxFIFOAlmostFull <= '1';
         else
           RxFIFOAlmostFull <= '0';
         end if;
      end if;
   end process pRxFIFOAlmostFull;

   -- RxFIFOFull Flag
   pRxFIFOFull : process(Clock, sResetORnotRxFIFOEnable)
   begin
      if (sResetORnotRxFIFOEnable = '1') then
         sRxFIFOFull <= '0';
      elsif (rising_edge(Clock)) then
         if (sRxFIFOArrayPosFilled(1) = '1' AND sRxFIFOArrayPosFilled(0) = '1')  then
            sRxFIFOFull <= '1';
         else
            sRxFIFOFull <= '0';
         end if;
      end if;
   end process pRxFIFOFull;

   RxFIFOFull <= sRxFIFOFull;

end RTL;

