LIBRARY ieee;
USE ieee.std_logic_1164.all;
use IEEE.MATH_REAL.ALL;
use ieee.numeric_std.all;

LIBRARY altera_mf;
USE altera_mf.altera_mf_components.all;


--V1.0   born (15.10.12 - DS)
--V1.1   Der FIFO wird nun nicht nur beim 'Reset', sondern auch bei 'RxFIFOEnable' geloescht. Dies daher, damit ein 'RxFIFOEnable' = '0'
--       im Falle von 'RxFIFOOverflow' = '1' diese Info loeschen kann. 
--       Generell ist der ganze Code nun etwas gestrafft, die unnoetigen Statemachines sind ausgebaut
--       und das M/D <-> USI Handshake auf 1-Takt Pulse reduziert. (07.04.16 - DS)

entity RxRamFIFO is
   generic 
   (
      gRxRamFIFOVersion             : real      := 1.1;
      gRxFIFODepth                  : integer   := 32;
      gRxFIFOMaxWidth               : integer   := 8 
   );
   PORT
   (  
      Clock                         : in  std_logic;
      Reset                         : in  std_logic;
      RxFIFOEnable                  : in  std_logic;

      RxFIFOGetDataFromDataIn       : in  std_logic;                                      --L->H-Flanke, dass neue Daten an 'RxFIFODataIn[]' anliegen
      RxFIFODataFetchedFromDataIn   : out std_logic;                                      --L->H dass FIFO die Daten von 'RxFIFODataIn[]' uebernommen hat
      RxFIFODataIn                  : in  std_logic_vector(gRxFIFOMaxWidth-1 downto 0);   --Daten die im FIFO gespeichert werden sollen
                  
      RxFIFOPutDataToDataOut        : in  std_logic;                                      --L->H-Flanke werden die FIFO Daten am Ausgang 'RxFIFODataOut[]' augegeben
      RxFIFODataOut                 : out std_logic_vector(gRxFIFOMaxWidth-1 downto 0);   --Daten die aus dem FIFO wieder ausgegeben werden

      RxFIFONewDataAtDataOutForMD   : out std_logic;  --1 Takt Pulse, wenn Daten sich an 'RxFIFODataOut[]' geaendert haben

      RxFIFOnEmpty                  : out std_logic;  --FIFO nicht leer                   (-> almost_empty)
      RxFIFOAlmostFull              : out std_logic;  --nur noch ein Speicherplatz frei   (-> almost_full)
      RxFIFOFull                    : out std_logic;  --FIFO voll                         (-> full)
      RxFIFOOverflow                : out std_logic   --Ueberlauf                         (-> nicht vorhanden, selbst bauen)
   );

end RxRamFIFO;


--Beschreibung
--Daten werden von USI-Rx in den RxRamFIFO eingetragen durch:
--USI legt empf. Daten an RxFIFODataIn und sendet pos. Flanke an RxFIFOGetDataFromDataIn
--FIFO uebernimmt Daten und legt RxFIFODataFetchedFromDataIn auf High -> Low
--CPU o.a. erhalten Info ueber vorhandene Daten im FIFO mittels RFEmpty, RxFIFOAlmostFull, 
--RxFIFOFull und/oder RxFIFOOverflow.
--Durch pos. Flanke an RxFIFOPutDataToDataOut (z.B. durch die CPU) werden FIFO Daten an RxFIFODataOut gelegt


architecture RTL of RxRamFIFO is

   component scfifo
   generic (
      lpm_width               : natural := gRxFIFOMaxWidth;                               --Breite von 'data[]' und 'q[]'
      lpm_widthu              : natural := natural(ceil(log2(real(gRxFIFODepth))));    --Breite von 'usedw[]'
      lpm_numwords            : natural := gRxFIFODepth;                                  --Tiefe des FIFO, mind. 4
      lpm_showahead           : string := "OFF";            --autom. Voranzeige des ersten FIFO Eintrags ohne 'rdreq'
      lpm_type                : string := "scfifo";         --SingleClockFIFO
      overflow_checking       : string := "ON";             --Deaktiviert den Port 'wrreq' wenn FIFO voll
      underflow_checking      : string := "ON";             --Deaktiviert den Port 'rdreq' wenn FIFO leer
      use_eab                 : string := "ON";             --Wenn "ON" wird RAM, sonst Logik fuer den FIFO-Speicher verwendet
      add_ram_output_register : string := "OFF";            --obsolet bei CyclonII und neuer
      almost_full_value       : natural := gRxFIFODepth-1;  --gespeicherte Werte >= 'almost_full_value' -> 'almost_full'-Port => '1'
      almost_empty_value      : natural := 2;               --gespeicherte Werte <  'almost_empty_value' -> 'almost_empty'-Port => '1'
      allow_rwcycle_when_full : string := "OFF";            --erlaubt kombinierte Lese-/Schreibzyklen bei vollem FIFO
      intended_device_family  : string := "CYCLONE III";    --nur fuer Simulation
      lpm_hint                : string := "UNUSED"          --nur fuer Simulation
   );
   port(
      clock                   : in  std_logic;                                --System Clock
      aclr                    : in  std_logic := '0';                         --Asynchron Reset
      sclr                    : in  std_logic := '0';                         --Synchron Reset
      data                    : in  std_logic_vector(lpm_width-1 downto 0);   --Daten Eingang
      wrreq                   : in  std_logic;                                --Daten von 'data[]' uebernehmen
      rdreq                   : in  std_logic;                                --Daten an q[] ausgeben
      q                       : out std_logic_vector(lpm_width-1 downto 0);   --Daten Ausgang
      empty                   : out std_logic;                                --FIFO ist leer
      almost_empty            : out std_logic;                                --'1' wenn 'usedw' <  'almost_empty_value'
      almost_full             : out std_logic;                                --'1' wenn 'usedw' >= 'almost_full_value'
      full                    : out std_logic;                                --'1' wenn 'usedw' == 'gRxFIFODepth'
      usedw                   : out std_logic_vector(lpm_widthu-1 downto 0)   --Gesamtzahl der im FIFO befindlicher Eintraege
   );
   end component;

   signal   sRxFIFOPutDataToDataOut_EdgeDet  : std_logic := '0';
   signal   sRxFIFOWriteData                 : std_logic := '0';
   signal   sRxFIFOReadData                  : std_logic := '0';
   signal   sRxFIFOEmpty                     : std_logic := '0';
   signal   sRxFIFOnEmpty                    : std_logic := '0';   
   signal   sRxFIFOFull                      : std_logic := '0';   
   signal   sResetORnotRxFIFOEnable          : std_logic;

begin

   sResetORnotRxFIFOEnable <= (Reset OR (not RxFIFOEnable));

   RxRamFIFO: scfifo
   port map
   (
      clock          => Clock,                        --System Clock
      aclr           => sResetORnotRxFIFOEnable,      --Asynchron Reset
      sclr           => '0',                          --Synchron Reset
      data           => RxFIFODataIn,                 --Daten Eingang
      wrreq          => sRxFIFOWriteData,             --Daten von 'data[]' uebernehmen
      rdreq          => sRxFIFOReadData,              --Daten an q[] ausgeben
      q              => RxFIFODataOut,                --Daten Ausgang
      empty          => sRxFIFOEmpty,                 --'1' wenn FIFO leer ist
      almost_empty   => open, --sRxFIFOAlmostEmpty,   --'1' wenn 'usedw' <  'almost_empty_value'
      almost_full    => RxFIFOAlmostFull,             --'1' wenn 'usedw' >= 'almost_full_value'
      full           => sRxFIFOFull,                  --'1' wenn 'usedw' == 'gRxFIFODepth'
      usedw          => open --sUsedFIFOWords         --Gesamtzahl der im FIFO befindlicher Eintraege
   );

   --#########################################################################################
   -- pos. Flanke an 'RxFIFOPutDataToDataOut' (ext. Signal) erkennen, sofern RxFIFOEnable = '1'
   -- CPU oder mUSIc fordert Daten aus FIFO an, FIFO legt Daten an RxFIFODataOut.
   --#########################################################################################

   pRxFIFOPutDataToDataOut_EdgeDet: process (Clock, Reset)
      variable vRxFIFOPutDataToDataOut_EdgeDetArray : std_logic_vector(1 downto 0);
   begin
      if (Reset = '1') then
         vRxFIFOPutDataToDataOut_EdgeDetArray := (others => '0') ;
      elsif (Clock'event and Clock = '1') then
         if (RxFIFOEnable = '1') then
            vRxFIFOPutDataToDataOut_EdgeDetArray := (vRxFIFOPutDataToDataOut_EdgeDetArray(0) & RxFIFOPutDataToDataOut ) ;
            if (vRxFIFOPutDataToDataOut_EdgeDetArray = "01") then --steigende Flanke
               sRxFIFOPutDataToDataOut_EdgeDet <= '1' ;
            else
               sRxFIFOPutDataToDataOut_EdgeDet <= '0' ;
            end if ;
         end if;
     end if ;    
   end process pRxFIFOPutDataToDataOut_EdgeDet; 

   --#########################################################################################
   -- FIFO
   --#########################################################################################

   pRxRamFIFO : process (Clock, Reset, sResetORnotRxFIFOEnable)
   begin

      if (sResetORnotRxFIFOEnable = '1') then

         RxFIFODataFetchedFromDataIn   <= '0';
         RxFIFOOverflow                <= '0';
         RxFIFONewDataAtDataOutForMD   <= '0';
         sRxFIFOReadData               <= '0';
         sRxFIFOWriteData              <= '0';

      elsif (rising_edge(Clock)) then

         --#########################################################################################
         -- Daten einlesen
         -- ==============
         -- Kommt der 1 Takt Pulse 'RxFIFOGetDataFromDataIn = '1'' und ist der FIFO nicht voll, werden die Eingangsdaten
         -- uebernommen und in den FIFO geschrieben. 
         -- Dies wird mit einem Puls an 'RxFIFODataFetchedFromDataIn' quittiert.
         --#########################################################################################

         --'sRxFIFOWriteData' darf nur einen Takt '1' sein, sonst wuerde mit jedem weiteren 'Clock'
         --der Inhalt von 'RxFIFODataIn' immer weiter in den FIFO (und damit in den naechsten freien
         --Speicher) des FIFO  geschrieben werden

         sRxFIFOWriteData              <= '0';           --Daten vom Eingang im FIFO speichern
         RxFIFODataFetchedFromDataIn   <= '0';

         if (RxFIFOGetDataFromDataIn = '1') then         --1 Takt Pulse vom USIRxTx -> neue Daten am Eingang
            if (sRxFIFOFull = '0') then                  --FIFO nicht voll
               sRxFIFOWriteData              <= '1';     --Daten vom Eingang im FIFO speichern
               RxFIFODataFetchedFromDataIn   <= '1';     --1 Takt Pulse an USIRxTx -> Daten gelesen und gespeichert anzeigen
               RxFIFOOverflow                <= '0';     --Overflow Info loeschen
             else
               RxFIFOOverflow    <= '1';                 --FIFO uberfuellt
            end if;
         end if;

         --#########################################################################################
         -- Daten ausgeben (an CPU, M/D o.a.)
         -- =================================
         -- Wird eine Flanke am Port 'RxFIFOPutDataToDataOut' erkannt und ist der FIFO nicht leer, dann
         -- einen Wert ausgeben.
         --#########################################################################################

         --'sRxFIFOReadData' darf nur einen Takt '1' sein, sonst wuerde mit jedem weiteren 'Clock'
         --der naechste Inhalt von 'RxFIFODataIn' immer weiter in den FIFO (und damit in den naechsten freien
         --Speicher) des FIFO  geschrieben werden         
         
          sRxFIFOReadData               <= '0';
          RxFIFONewDataAtDataOutForMD   <= '0';

         if ( (sRxFIFOPutDataToDataOut_EdgeDet = '1') and (sRxFIFOnEmpty = '1') ) then
            sRxFIFOReadData                  <= '1';
         end if;

         if (sRxFIFOReadData = '1') then                    --Damit Daten und 'RxFIFONewDataAtDataOutForMD' gleichzeit anliegen
            RxFIFONewDataAtDataOutForMD      <= '1';        --1 Takt Pulse, dass neue Daten vom FIFO ausgegeben werden
         end if;

      end if;

   end process pRxRamFIFO;

   -- INFO
   -- ===================
   -- Das Signal 'RxFIFOnEmpty' wird z.B. vom NiosII als Interruptquelle genutzt.
   -- Dieser Interrupt ist Flankengetriggert (steigende Flanke).
   -- Wird nun ein Wert aus dem RxFIFO abgeholt und ist der FIFO anschliessend NICHT leer,
   -- bleibt dieses Signal '1'. Das sorgt aber dafÃƒÆ’Ã‚Â¼r, dass im NiosII die ISR zur Abholung der
   -- Daten nicht erneut aufgerufen wird. Es verbleiben also alle weiteren Daten im FIFO.
   -- Aus diesem Grund muss dieses Signal im Moment der Datenabholung fuer mind. einen Takt auf '0'
   -- und danach ggf. (sofern weitere Daten im FIFO sind) wieder auf '1' wechseln.
   -- Das Signal 'sRxFIFOReadData' sorgt dafuer, dass die neuen Daten an den Ausgang 'RxFIFODataOut'
   -- gelegt werden. Das 'sRxFIFOReadData' Signal ist nur einen Takt lang und wird invers mit
   -- dem ebenfalls inversen 'sRxFIFOEmpty' UND verknuepft und als 'RxFIFOnEmpty' ausgegeben.
   -- Dadurch wird 'RxFIFOnEmpty' jedesmal einen Takt nach 'sRxFIFOReadData' sicher '0'. Dies geschieht
   -- zeitglich mit dem Signal dann fuer eine Takt auf '1' stehenden Signal 'RxFIFONewDataAtDataOutForMD'.

   -- Vergleich zum HW-FIFO aus Logik
   -- ======================================
   -- Im HW-FIFO wird das Signal 'RxFIFOnEmpty' immer dann '1', wenn im Logik Schieberegister
   -- die oberste Position mit gueltigen Daten zur Abholung bereit gefuellt ist.
   -- Werden die Daten abgeholt, wird die Position zunaecht geleert. Das Signal 'RxFIFONewDataAtDataOutForMD'
   -- wird '1' und im naechsten Takt 'RxFIFOnEmpty' durch die nun leere oberste Position kurzzeitig '0'. 
   -- Danach wird ggf. der naechste Wert im HW-FIFO an die oberste Position nachgeschoben und 'RxFIFOnEmpty' wieder '1'.

   sRxFIFOnEmpty  <= ((not sRxFIFOEmpty) AND (not sRxFIFOReadData));

   RxFIFOFull     <= sRxFIFOFull;
   RxFIFOnEmpty   <= sRxFIFOnEmpty;

end RTL;
