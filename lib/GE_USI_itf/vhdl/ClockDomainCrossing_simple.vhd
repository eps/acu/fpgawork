-- ClockDomainCrossing_simple.vhd
-- 
-- (c) 2021-2022 GE Energy Power Conversion
-- written by: <andreas.berthe@ge.com>
--
-- synchronize data vector which changes infrequently
--  (use a DC FIFO otherwise ...)

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity ClockDomainCrossing_simple is
	generic ( widthVector : integer range 1 to 1023;
             useRAM      : std_logic := '0' );
	port (
		ClockIn           : in std_logic;
		ClockOut          : in std_logic;
		Reset             : in std_logic;
		--
		DataIn            :  in std_logic_vector(widthVector-1 downto 0);
		DataOut           : out std_logic_vector(widthVector-1 downto 0) := (others => '0')
	);
end;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

architecture behavioral of ClockDomainCrossing_simple is

	signal DataIn_reg    : std_logic_vector(widthVector-1 downto 0) := (others => '0');
	
	type RAM_Data_t is array(NATURAL RANGE <>) of std_logic_vector(widthVector-1 downto 0);
	
	signal RAM_Data   : RAM_Data_t(7 downto 0);
	signal RAM_wrAddr : integer range 0 to 7 := 0;
	signal RAM_rdAddr : integer range 0 to 7 := 0;
	
	attribute altera_attribute : string;
--	attribute altera_attribute of ClockDomainCrossing_simple : entity is "-to \"*ClockDomainCrossing_simple:*|RAM_Data\" -name ALLOW_ANY_RAM_SIZE_FOR_RECOGNITION on";
--	attribute altera_attribute of ClockDomainCrossing_simple : signal is "-name RAM_Data -name ALLOW_ANY_RAM_SIZE_FOR_RECOGNITION on";

	signal DataIn_captured    : std_logic := '0';
	signal DataIn_captured_reg: std_logic := '0';
	signal DataIn_captured_sync_clkOut: std_logic_vector(2 downto 0) := (others => '0');
	
	signal DataIn_captured_ack : std_logic := '0';
	signal DataIn_captured_ack_sync_clockIn: std_logic_vector(1 downto 0) := (others => '0');

	signal Reset_clockIn  : std_logic_vector(1 downto 0) := (others => '1');
	signal Reset_clockOut : std_logic_vector(1 downto 0) := (others => '1');
	signal ResetWaitCnt : integer range 0 to 7 := 0;
	
	type CaptureStates_t is (WAIT_RESET, IDLE, WAIT_ACK);
	signal CaptureState : CaptureStates_t := WAIT_RESET;
	
begin

	p_reg_DataIn: process(ClockIn, Reset_clockIn) is
	begin
	
		if Reset_clockIn(1) = '1' then
			CaptureState    <= WAIT_RESET;
			ResetWaitCnt    <= 0;
			DataIn_reg      <= (others => '0');
			DataIn_captured <= '0';
			DataIn_captured_ack_sync_clockIn <= (others => '0');
	
		elsif rising_edge(ClockIn) then
		
			DataIn_captured_ack_sync_clockIn <= DataIn_captured_ack_sync_clockIn(0) & DataIn_captured_ack;
		
			case CaptureState is
			
				when WAIT_RESET =>
					ResetWaitCnt <= ResetWaitCnt+1;
					if (ResetWaitCnt = 6) then
						CaptureState <= IDLE;
					end if;
			
				when IDLE =>
					-- needed for safe clock domain crossing
					if DataIn_captured_ack_sync_clockIn(1) = '0' then
						DataIn_reg      <= DataIn;
						DataIn_captured <= '1';
						CaptureState    <= WAIT_ACK;
					end if;
					
				when WAIT_ACK =>
					if DataIn_captured_ack_sync_clockIn(1) = '1' then
						DataIn_captured <= '0';
						CaptureState <= IDLE;
					end if;
					
				when others =>
					CaptureState <= IDLE;
					
			end case;

		end if;

	end process;
	
	p_rst_clockIn: process(ClockIn) is
	begin
	
		if rising_edge(ClockIn) then
			Reset_clockIn <= Reset_clockIn(0) & Reset;
		end if;
	
	end process;
	
	p_rst_clockOut: process(ClockOut) is
	begin
	
		if rising_edge(ClockOut) then
			Reset_clockOut <= Reset_clockOut(0) & Reset;
		end if;
	
	end process;
	
g_noRAM: if useRAM = '0' generate

	-- use registers instead of RAM:

	p_sync_DataIn: process(ClockOut, Reset_clockOut(1)) is
	begin
	
		if Reset_clockOut(1) = '1' then
			DataIn_captured_sync_clkOut <= (others => '0');
			DataOut                     <= (others => '0');
			DataIn_captured_ack         <= '0';
			
		elsif rising_edge(ClockOut) then
			
			DataIn_captured_sync_clkOut <= DataIn_captured_sync_clkOut(1 downto 0) & DataIn_captured;
			
			if DataIn_captured_sync_clkOut(1) = '1' then
				DataOut             <= DataIn_reg;
				DataIn_captured_ack <= '1';
			else
				DataIn_captured_ack <= '0';
			end if;
			
		end if;
	
	end process;
	
end generate;
	
g_withRAM: if useRAM = '1' generate

	-----------------------------------
	-- when configured for using RAM:
	p_writeRAM : process(ClockIn, Reset_clockIn) is
		variable v_wrEn : std_logic := '0';
	begin
	
		if Reset_clockIn(1) = '1' then
			RAM_wrAddr <= 0;
			DataIn_captured_reg <= '0';
	
		elsif(rising_edge(ClockIn)) then
		
			DataIn_captured_reg <= DataIn_captured;
		
			if DataIn_captured_reg = '0' AND DataIn_captured = '1' then
				v_wrEn := '1';
			else
				v_wrEn := '0';
			end if;
			
			if v_wrEn = '1' then
				RAM_Data(RAM_wrAddr) <= DataIn; -- ok to directly use DataIn here when RAM is used! -- DataIn_reg;
				RAM_wrAddr           <= (RAM_wrAddr+1) MOD 8; -- for sim.
			end if;
			
		end if;
		
	end process;
	
	p_readRAM: process(ClockOut, Reset_clockOut) is
		variable v_rdEn : std_logic := '0';
	begin
	
		if Reset_clockOut(1) = '1' then
			DataIn_captured_sync_clkOut <= (others => '0');
			--DataOut                     <= (others => '0');
			DataIn_captured_ack         <= '0';
			
			RAM_rdAddr <= 0;
			
		elsif(rising_edge(ClockOut)) then 
			
			v_rdEn := '0';
			
			DataIn_captured_sync_clkOut <= DataIn_captured_sync_clkOut(1 downto 0) & DataIn_captured_reg; -- need to use DataIn_captured_reg - in case the ClockIn domain is much slower.
			
			if DataIn_captured_sync_clkOut(1) = '1' then
				DataIn_captured_ack <= '1';
			else
				DataIn_captured_ack <= '0';
			end if;
			
			if DataIn_captured_sync_clkOut(2 downto 1) = "01" then
				v_rdEn := '1';
			end if;
			
			if v_rdEn = '1' then
				RAM_rdAddr <= (RAM_rdAddr+1) MOD 8; -- for sim.
				DataOut    <= RAM_Data(RAM_rdAddr);
			end if;
			
		end if;
		
	end process;
	
end generate;

end behavioral;