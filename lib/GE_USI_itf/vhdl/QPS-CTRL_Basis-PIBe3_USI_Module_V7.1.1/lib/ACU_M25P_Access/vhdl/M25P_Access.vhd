library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


--M25P_Access

--History
--========
--12.11.12 born (DS)
--02.02.22 Generic '..Version' entfernt (DS)


--Hin und wieder ist es noetig die Inhalte von FSPs in einem nicht fluechtigen Speicher (Flash) zu sichern und ggf. im Einschaltmoment des Moduls zu
--laden und das FSP damit zu fuellen. Dies sind z.B. Kalibrierdaten fuer einen ADC (Offset/Gain). Fuer diese Aufgabe gibt es dieses Modul.

--Zu Beginn muss dieses Modul herausfinden, wo der naechste freie Speicherplatz im Flash zu finden ist. Geloeschte Flashzellen sind mit 0xFF gefuellt.
--Es wird ein Suchlauf, beginnend ab "gFSPImgStartAddress" durchgefuehrt. Ist 'FSPImgUseSectorAddress' = '1' wird die Sectoradresse am
--'FSPImgSectorAddress' Eingang benutzt. Die Sektoradresse bildet das MSB der Gesamtadresse.
--Die Daten im seriellen Flash sind ebefalls, wie alle USI Informationen
--in ASCII abgelegt. Wird der erste 0xFF Eintrag gefunden, ist dies der erste freie Eintrag, ab dem weitere FSP Inhalte gesichert werden koennen.
--D.h. um die Verwaltung der Adressen kuemmert sich dieses Modul und von extern kann lediglich die Startadresse angegeben werden.
--Der Ausgang 'FSPImgBusy' wird '0' sobald der Suchlauf abgeschlossen und das Modul bereit ist weitere FSP Daten zu Sichern. Die
--aktuelle Adresse im Flash wird dabei ueber 'ECDataAddress' ausgegeben.
--Die zu sichernden Daten muessen ueber den Eingangsport 'FSPImgDataIn' angelegt und mit 'FSPImgWriteData' geschrieben werden. Die Reihenfolge der
--Daten ist dabei USI String konform einzuhalten: 

--STX PID PID MA FSP FSP - Data - PP PP ETX

--mit STX     = 0x02
--    PID PID = WR

--Daten loeschen:
--==============
--L->H Flanke an 'FSPImgEraseAll' loescht den Sektor der die Adresse 'gFSPImgStartAddress' beinhaltet.
--Ist 'FSPImgUseSectorAddress' = '1' wird die Sectoradresse am 'FSPImgSectorAddress' Eingang benutzt.
--'FSPImgBusy' ist '1' bis die der Loeschvorgang abgeschlossen ist.

--Daten schreiben
--===============
--Eine '1' an 'FSPImgWriteEnable' startet die Schreibfunktion.
--Ist 'FSPImgUseSectorAddress' = '1' startet zunaechst ein Suchlauf im 'FSPImgSectorAddress' und es wird die naechste freie, 
--beschreibbare Adresse gesucht und an 'ECDataAddress' ausgegeben.
--Ist 'FSPImgUseSectorAddress' = '0' wird die letzte Schreibadresse + 1 als naechste freie, beschreibbare Adresse
--an 'ECDataAddress' ausgegeben. 'FSPImgBusy' ist '1' bis diese Initialisierung bzw. Suche abgeschlossen ist.
--Eine L->H Flanke an 'FSPImgWriteData' uebertraegt die Daten von 'FSPImgDataIn' ins Flash.
--'FSPImgBusy' ist '1' bis die der Schreibvorgang abgeschlossen ist.
--Die naechste L->H Flanke an 'FSPImgWriteData' uebernimmt das naechste Datenbyte an 'FSPImgDataIn'.
--Beenden des Schreibvorgangs durch loeschen von 'FSPImgWriteEnable'. Noch nicht gespeicherte Daten
--werden ins Flash geschrieben, 'FSPImgBusy' ist '1' bis der Schreibvorgang abgeschlossen ist.

--Daten lesen
--===========
--Eine '1' an 'FSPImgReadEnable' startet die Lesefunktion.
--Ist 'FSPImgUseSectorAddress' = '1' wird die erste Adresse im 'FSPImgSectorAddress' angewaehlt und an 'ECDataAddress' ausgegeben.
--Ist 'FSPImgUseSectorAddress' = '0' wird 'gFSPImgStartAddress' als erste Adresse an 'ECDataAddress' ausgegeben.
--'FSPImgBusy' ist '1' bis die Initialisierung abgeschlossen ist.
--Eine L->H Flanke an 'FSPImgReadData' startet den Lesevorgang.
--'FSPImgBusy' ist '1' bis die der Lesevorgang abgeschlossen ist.
--Die gelesenen Daten liegen nun an 'FSPImgDataOut' an.
--Die naechste L->H Flanke an 'FSPImgReadData' startet das Lesen des naechsten Datenbytes.
--Beenden des Lesevorgangs durch loeschen von 'FSPImgReadEnable'. 
--'FSPImgBusy' ist '1' bis die der Lesevorgang abgeschlossen ist.
--Erreicht die Ausleseadresse die letzte Schreibadresse (sind die ausgelesenen Daten also 0xFF)
--wird 'FSPImgReadDone' = '1'.

entity M25P_Access is

generic
(
   gNoInitSearchRun           : std_logic                      := '1';            --wenn '1' wird der PowerUp Suchlauf nicht durchgefuehrt
   gFSPImgStartAddress        : std_logic_vector(23 downto 0)  := X"000000"       -- gibt die Startadresse des FSPImg Sektors an
);
port
(
   Clock                   :  In    std_logic;
   Reset                   :  In    std_logic;

   --Ein-/Ausgaenge zur uebergeordneten Instanz (FSP-Interconnectionhandler)

   FSPImgEraseAll          : in     std_logic;                    -- L->H Flanke loescht alle Daten innerhalb des FSPImg Sektors
   FSPImgReady             : out    std_logic;                    -- wird '1' sobald der Suchlauf nach PowerUp oder Loeschen des FSPImg Sektors abgeschlossen ist und neue oder weitere FSP Daten im Flash gesichert werden koennen
                                                                  -- Wird mit 'FSPImg*Enable' Lesen/Schreiben aktiviert, wird dieser Port '0' und bleibt es bis 'FSPImg*Enable' wieder deaktiviert wurde 
                                                                  -- und das Modul fuer eine neue Aktivierung bereit ist.
                                                                  -- Beim Lesen ist dies unmittelbar, beim Schreiben vergeht einige Zeit, da die Daten aus dem Flashzwischenspeicher ins Flash uebertragen werden
                                                                  -- Dies dauert, solange bleibt dieser Port '0'.
                                                                  -- Wird mit 'FSPImgEraseAll' der Sektor geloescht, wird dieser Port '0' und bleibt es bis der Loeschvorgang abgeschlossen ist 
   FSPImgUseSectorAddress  : in     std_logic;
   FSPImgSectorAddress     : in     std_logic_vector(7 downto 0);
   FSPImgDataIn            : in     std_logic_vector(7 downto 0); -- ein zu sicherndes Datenbyte wird hier angelegt
   FSPImgWriteEnable       : in     std_logic;                    -- Wenn '1' koennen Daten ins Flash geschrieben werden
   FSPImgWriteData         : in     std_logic;                    -- L->H Flanke, das an 'FSPImgDataIn' angelegte Datenbyte wird im Flash gesichert   
   FSPImgBusy              : out    std_logic;                    -- waehrend des Suchlaufs, beim Loeschen, Schreiben oder Lesen des Flash ist dieser Port '1' -> Zugriffe sollten vermieden werden
                                                                  -- Im Gegensatz zu 'FSPImgReady' wird dieser Port aber sofort wieder '0', wenn ein neues 'FSPImg*Data' Kommando
                                                                  -- ausgefuehrt werden kann.
   FSPImgReadEnable        : in     std_logic;                    -- Wenn '1' koennen Daten aus dem Flash gelesen werden
   FSPImgReadData          : in     std_logic;                    -- Ist 'FSPImgRWn' = '1' und an diesem Port erfolgt eine L->H Flanke wird ein Byte aus dem Flash ausgelesen 
   FSPImgDataOut           : out    std_logic_vector(7 downto 0); -- hier werden die ausgelesenen Datenbytes ausgegeben
   FSPImgReadDone          : out    std_logic;                    -- Wird '1' sobald das letzte Datenbyte ausgelesen wurde, die Leseadresse also der Schreibadresse entspricht

   --Ein-/Ausgaenge zur untergeordneten Instanz (EPCS-Control)

   ECCommand               : Out    std_logic_vector(2 downto 0); -- Kommandos fuer EPCS Control
   ECExecuteCommand        : out    std_logic;                    -- L->H Flanke laesst EPCS Control das Kommando von ECCommand ausfuehren

   ECDataAddress           : out    std_logic_vector(23 downto 0);-- Die Startadresse ab der Bytes gelesen oder geschrieben werden sollen
   ECDataOut               : Out    std_logic_vector(7 downto 0);
   ECWriteDataEnable       : Out    std_logic;                    -- L->H Flanke laesst EPCS Control die zu schreibenden Daten (FSPImgDataIn->ECDataOut) uebernehmen
   
   ECReadDataEnable        : Out    std_logic;                    -- L->H Flanke laesst EPCS Control die zu lesenden Daten (ECDataIn->FSPImgDataOut) ausgeben
   ECDataIn                : In     std_logic_vector(7 downto 0); -- Ausgelesene Daten von EPCS Control                                        

   ECBusy                  : In     std_logic                     -- Wenn '1' ist EPCS Control bei der Arbeit
);

End M25P_Access;

Architecture rtl of M25P_Access is

-- Command
constant CMD_NOP  : std_logic_vector (2 downto 0) := "000"; -- NOP
constant CMD_EB   : std_logic_vector (2 downto 0) := "001"; -- Erase Bulk         - das gesamte Flash loeschen
constant CMD_ES   : std_logic_vector (2 downto 0) := "010"; -- Erase Sector       - nur den an 'DataAddress' angegeben Sektor loeschen
constant CMD_WSB  : std_logic_vector (2 downto 0) := "011"; -- Write single bytes - es wird jeweils nur ein einzelnes Bytes (an DataAddress) ins Flash geschrieben
constant CMD_WC   : std_logic_vector (2 downto 0) := "100"; -- Write continuously - es wird solange geschrieben, bis ExecuteCommand wieder low wird
constant CMD_RSB  : std_logic_vector (2 downto 0) := "101"; -- Read single bytes  - es wird jeweils nur ein einzelnes Bytes (an DataAddress) aus dem Flash gelesen
constant CMD_RC   : std_logic_vector (2 downto 0) := "110"; -- Read continuously  - es wird solange gelesen, bis ExecuteCommand wieder low wird
constant CMD_INIT : std_logic_vector (2 downto 0) := "111";


Signal   sFSPImgWriteEnableEdgeDetArray   :  std_logic_vector(1 downto 0);
Signal   sFSPImgWriteDataEdgeDetArray     :  std_logic_vector(1 downto 0);
Signal   sFSPImgReadEnableEdgeDetArray    :  std_logic_vector(1 downto 0);
Signal   sFSPImgReadDataEdgeDetArray      :  std_logic_vector(1 downto 0);
Signal   sFSPImgEraseAllEdgeDetArray      :  std_logic_vector(1 downto 0);

signal   sECDataAddress                   :  integer range 0 to 16777216;
signal   sECNextWriteAddress              :  integer range 0 to 16777216;

constant cWaitStateCounterMax             : integer := 7;
signal   sWaitStateCounter                : integer range 0 to cWaitStateCounterMax;


type     tM25P_Access   is (  SEEK_NEXT_WRITE_ADDRESS_START,
                              SEEK_NEXT_WRITE_ADDRESS_SET_READ_ENABLE,
                              SEEK_NEXT_WRITE_ADDRESS_CLR_READ_ENABLE,
                              SEEK_NEXT_WRITE_ADDRESS_READ_DATA,
                              WAIT_FOR_BUSY_HIGH,
                              WAIT_FOR_BUSY_LOW,
                              WAIT_STATE,
                              WAIT_FOR_START,
                              WRITE_BYTES_INIT,
                              WRITE_BYTES,
                              READ_BYTES
                           );

Signal   sState      :        tM25P_Access;
Signal   sNextState  :        tM25P_Access;

Begin

M25P_Control : Process (Reset, Clock, sECDataAddress)
   Begin
      If ( Reset = '1') Then

         sFSPImgWriteDataEdgeDetArray        <= (others => '0');
         sFSPImgReadDataEdgeDetArray         <= (others => '0');
         sFSPImgEraseAllEdgeDetArray         <= (others => '0');
         sFSPImgReadEnableEdgeDetArray       <= (others => '0');
         sFSPImgWriteEnableEdgeDetArray      <= (others => '0');
         sECDataAddress                      <= 0;
         sECNextWriteAddress                 <= 0;
         FSPImgDataOut                       <= (others => '0');
         ECDataOut                           <= (others => '0');
         ECCommand                           <= CMD_NOP;
         ECExecuteCommand                    <= '0';
         FSPImgBusy                          <= '0';
         FSPImgReady                         <= '0';
         FSPImgReadDone                      <= '0';
         ECWriteDataEnable                   <= '0';
         ECReadDataEnable                    <= '0';
         sWaitStateCounter                   <= 0;
         sState                              <= SEEK_NEXT_WRITE_ADDRESS_START;
         sNextState                          <= SEEK_NEXT_WRITE_ADDRESS_START;
      Else

         if rising_edge(Clock)  Then
                                
            sFSPImgEraseAllEdgeDetArray         <= sFSPImgEraseAllEdgeDetArray(0)      & FSPImgEraseAll;    --Sektor loeschen
            sFSPImgWriteEnableEdgeDetArray      <= sFSPImgWriteEnableEdgeDetArray(0)   & FSPImgWriteEnable; --Daten schreiben aktivieren
            sFSPImgReadEnableEdgeDetArray       <= sFSPImgReadEnableEdgeDetArray(0)    & FSPImgReadEnable;  --Daten lesen aktivieren
            
            sFSPImgWriteDataEdgeDetArray        <= sFSPImgWriteDataEdgeDetArray(0)     & FSPImgWriteData;   --Datenbyte schreiben
            sFSPImgReadDataEdgeDetArray         <= sFSPImgReadDataEdgeDetArray(0)      & FSPImgReadData;    --Datenbyte lesen

            case (sState) is

               ---------------------------------------            
               when SEEK_NEXT_WRITE_ADDRESS_START =>                 --den ersten freien Speicherplatz im Flashsektor finden

                  if (gNoInitSearchRun = '0') then                   --nur wenn Suchlauf gewuenscht

                     FSPImgBusy        <= '1';                          --Modul ist beschaeftigt...
                     FSPImgReady       <= '0';                          --...und noch nicht bereit auf Anforderungen zu reagieren

                     if (FSPImgUseSectorAddress = '0') then
                        sECDataAddress    <= to_integer(unsigned(gFSPImgStartAddress));            --Ab dieser Adresse beginnt der Suchlauf
                     else
                        sECDataAddress    <= to_integer(unsigned(FSPImgSectorAddress & X"0000"));  --Ab dieser Adresse beginnt der Suchlauf
                     end if;

                     ECCommand         <= CMD_RC;                       --EPCS-Control soll fortlaufend Daten lesen
                     ECExecuteCommand  <= '1';                          --EPCS-Control soll Kommando ausfuehren
                     sState            <= WAIT_FOR_BUSY_HIGH;
                     sNextState        <= SEEK_NEXT_WRITE_ADDRESS_SET_READ_ENABLE;
                  else
                     sState            <= WAIT_FOR_START;
                     sNextState        <= WAIT_FOR_START;
                  end if;

               ---------------------------------------            
               when SEEK_NEXT_WRITE_ADDRESS_SET_READ_ENABLE =>

                  ECReadDataEnable  <= '1';                          --Sorgt dafuer, dass EPCS-Control die Daten an der aktuellen Adresse ausgibt
                  sState            <= SEEK_NEXT_WRITE_ADDRESS_CLR_READ_ENABLE;

               ---------------------------------------            
               when SEEK_NEXT_WRITE_ADDRESS_CLR_READ_ENABLE =>

                  ECReadDataEnable  <= '0';                          --jetzt warten bis ECPS-Control 'Busy' wieder low wird, dann liegen die Daten an
                  sState            <= WAIT_FOR_BUSY_HIGH;
                  sNextState        <= SEEK_NEXT_WRITE_ADDRESS_READ_DATA;

               ---------------------------------------            
               --EPCS-Control fuehrt ein Autoinkrement der Adresse durch, wird also /= X"FF" gelesen muss auch dieses Modul die Adresse um +1 erhoehen
               --wird hingegen =X"FF" gelesen muss die Adresse da bleiben wo sie ist, da dies die naechste zu beschreibende Adresse ist
               when SEEK_NEXT_WRITE_ADDRESS_READ_DATA =>
                  
                  if(ECDataIn /= X"FF") then                         --an dieser Speicherstelle steht kein 0xFF (also vermutlich FSP-Daten)
                     sECDataAddress       <= sECDataAddress + 1;
                     sState               <= SEEK_NEXT_WRITE_ADDRESS_SET_READ_ENABLE;
                  else                                               --an dieser Speicherstelle steht 0xFF (also vermutlich leer)
                     sECNextWriteAddress  <= sECDataAddress;         --Adresse als naechste Speicherstelle sichern
                     ECCommand            <= CMD_NOP;                --Kein Kommando an EPCS-Control
                     ECExecuteCommand     <= '0';
                     sState               <= WAIT_STATE;
                  end if;

               ---------------------------------------
               when  WAIT_FOR_BUSY_HIGH =>

                  ECWriteDataEnable <= '0';                          --werden Daten geschrieben, kann das 'Write' Strobe hier schon geloescht werden
                  ECReadDataEnable  <= '0';                          --werden Daten gelesen, kann das 'Read' Strobe hier schon geloescht werden

                  if (ECBusy = '0') then                             --nach einem Kommando wird EPCS-Control 'Busy' High
                     sState <= WAIT_FOR_BUSY_HIGH;
                  else
                     sState <= WAIT_FOR_BUSY_LOW;
                  end if;

               ---------------------------------------
               when  WAIT_FOR_BUSY_LOW =>                            --Kommando abgearbeitet

                  if (ECBusy = '1') then
                     sState      <= WAIT_FOR_BUSY_LOW;
                  else
                     sState      <= sNextState;
                  end if;

               ---------------------------------------
               when  WAIT_STATE =>                                   --die Deselect Time (tSHSL) des M25P ist min. 100 ns, daher kann es noetig sein etwas zu warten

                  ECExecuteCommand     <= '0';
                  ECCommand            <= CMD_NOP;

                  if(sWaitStateCounter < cWaitStateCounterMax) then
                     sWaitStateCounter <= sWaitStateCounter + 1;
                  else
                     sWaitStateCounter <= 0;

                     if (FSPImgWriteEnable = '1') then      --es sollen Daten geschrieben werden, der Suchlauf ist beendet, nun das Scheiben initialisieren
                        sState         <= WRITE_BYTEs_INIT;
                     else
                        sState         <= WAIT_FOR_START;
                     end if;

                  end if;

               ---------------------------------------
               when  WAIT_FOR_START =>                               --Hier muss per Read/Write/Erase Enable entschieden werden was zu tun ist

                  FSPImgBusy           <= '0';                       --Modul ist nicht mehr Busy
                  FSPImgReady          <= '1';                       --Modul ist bereit neue Daten zu schreiben/zu lesen
                  FSPImgReadDone       <= '0';
                  ECExecuteCommand     <= '0';
                  ECCommand            <= CMD_NOP;

                  --Sektor loeschen
                  if (sFSPImgEraseAllEdgeDetArray = "01") AND (sFSPImgWriteEnableEdgeDetArray = "00") AND (sFSPImgReadEnableEdgeDetArray = "00") then        --Sektor loeschen
                     FSPImgBusy           <= '1';                          --Modul ist beschaeftigt...
                     FSPImgReady          <= '0';                          --...und nicht bereit auf Anforderungen zu reagieren

                     if (FSPImgUseSectorAddress = '0') then
                        sECDataAddress       <= to_integer(unsigned(gFSPImgStartAddress));   --Sektor der dieser Adresse beinhaltet loeschen
                        sECNextWriteAddress  <= to_integer(unsigned(gFSPImgStartAddress));   --dies ist auch die neue Startadresse zum beschreiben
                     else
                        sECDataAddress       <= to_integer(unsigned(FSPImgSectorAddress & X"0000")) ;   --diesen Sektor loeschen
                        sECNextWriteAddress  <= to_integer(unsigned(FSPImgSectorAddress & X"0000")) ;   --1. Byte dieses Sektors ist die neue Startadresse zum beschreiben
                     end if;

                     ECCommand            <= CMD_ES;                       --EPCS-Control soll einen Sektor loeschen
                     ECExecuteCommand     <= '1';                          --EPCS-Control soll Kommando ausfuehren
                     sState               <= WAIT_FOR_BUSY_HIGH;
                     sNextState           <= WAIT_STATE;
                  --neue Daten schreiben
                  elsif (sFSPImgEraseAllEdgeDetArray = "00") AND (sFSPImgWriteEnableEdgeDetArray = "01") AND (sFSPImgReadEnableEdgeDetArray = "00") then     --Bytes fortlaufend schreiben aktivieren
                     FSPImgBusy        <= '1';                          --Modul ist beschaeftigt...
                     FSPImgReady       <= '0';                          --...und nicht bereit auf Anforderungen zu reagieren

                     if (FSPImgUseSectorAddress = '0') then
                        sECDataAddress    <= sECNextWriteAddress;       --Daten schreiben wird ab dieser Adresse vorbereitet
                        sState            <= WRITE_BYTES_INIT;
                     else
                        sECDataAddress    <= to_integer(unsigned(FSPImgSectorAddress & X"0000")) ;   --diesen Sektor nach naechstem freien Eintrag durchsuchen und ab dort schreiben
                        sState            <= SEEK_NEXT_WRITE_ADDRESS_START;   --Suchlauf starten und danach Schreiben ab der ersten freien Adresse
                     end if;

                  --Daten lesen
                  elsif (sFSPImgEraseAllEdgeDetArray = "00") AND (sFSPImgWriteEnableEdgeDetArray = "00") AND (sFSPImgReadEnableEdgeDetArray = "01") then     --Bytes fortlaufend lesen aktivieren
                     FSPImgBusy        <= '1';                          --Modul ist beschaeftigt...
                     FSPImgReady       <= '0';                          --...und nicht bereit auf Anforderungen zu reagieren

                     if (FSPImgUseSectorAddress = '0') then
                        sECDataAddress    <= to_integer(unsigned(gFSPImgStartAddress));  --Daten lesen wird ab dieser Adresse vorbereitet
                     else
                        sECDataAddress    <= to_integer(unsigned(FSPImgSectorAddress & X"0000")) ;   --Daten lesen wird ab Adresse 0 in diesem Sektor vorbereitet
                     end if;

                     ECCommand         <= CMD_RC;                       --EPCS-Control soll Daten kontinuierlich lesen
                     ECExecuteCommand  <= '1';                          --EPCS-Control soll Kommando ausfuehren
                     sState            <= WAIT_FOR_BUSY_HIGH;
                     sNextState        <= READ_BYTES;

                  else
                     sState        <= WAIT_FOR_START;
                  end if;

               ---------------------------------------
               when WRITE_BYTES_INIT =>

                  ECCommand         <= CMD_WC;                       --EPCS-Control soll Daten kontinuierlich schreiben
                  ECExecuteCommand  <= '1';                          --EPCS-Control soll Kommando ausfuehren                  
                  sState            <= WAIT_FOR_BUSY_HIGH;
                  sNextState        <= WRITE_BYTES;

               ---------------------------------------
               when WRITE_BYTES =>

                  FSPImgBusy           <= '0';                          --Modul ist bereit...
                  ECDataOut            <= FSPImgDataIn;                 --Daten einlesen und bereitstellen
                  sECNextWriteAddress  <= sECDataAddress;

                  if(FSPImgWriteEnable = '1') then                      --nur solange Daten schreiben, wie WriteEnbale auch '1' ist

                     if(sFSPImgWriteDataEdgeDetArray = "01") then       --Flanke erkannt
                        FSPImgBusy        <= '1';                       --Modul ist beschaeftigt...
                        ECWriteDataEnable <= '1';                       --EPCS-Control veranlassen die Daten zu schreiben
                        sECDataAddress    <= sECDataAddress + 1;        --Adressen-Auto-Inkrement mitfuehren  
                        sState            <= WAIT_FOR_BUSY_HIGH;
                        sNextState        <= WRITE_BYTES;
                     else
                        FSPImgBusy        <= '0';
                        ECWriteDataEnable <= '0';
                        sECDataAddress    <= sECDataAddress;
                        sState            <= WRITE_BYTES;
                     end if;
                  else                                                  --Beim Beenden des Schreibvorgangs werden noch nicht
                     FSPImgBusy        <= '1';                          --   gespeicherte Daten ins Flash gesichert. Dies kann einige Zeit
                     ECWriteDataEnable <= '0';                          --   dauern und wird mittels ECBusy von EPCS-Control
                     sECDataAddress    <= sECDataAddress;               --   angezeigt. Solange darf kein erneuter Zugriff auf's EPCS/M25P erfolgen.
                     ECExecuteCommand  <= '0';
                     ECCommand         <= CMD_NOP;
                     sState            <= WAIT_FOR_BUSY_HIGH;
                     sNextState        <= WAIT_STATE;
                  end if;

               ---------------------------------------
               when READ_BYTES =>

                  FSPImgBusy        <= '0';                             --Modul ist bereit...
                  FSPImgDataOut     <= ECDataIn;                        --Daten einlesen und bereitstellen

                  if (sECDataAddress >= sECNextWriteAddress) then
                     FSPImgReadDone <= '1';
                  else
                     FSPImgReadDone <= '0';
                  end if;

                  if (FSPImgReadEnable = '1') then                      --nur solange Daten lesen, wie ReadEnbale auch '1' ist

                     if(sFSPImgReadDataEdgeDetArray = "01") then        --Flanke erkannt
                        FSPImgReady       <= '0';
                        FSPImgBusy        <= '1';                       --Modul ist beschaeftigt...
                        ECReadDataEnable  <= '1';                       --EPCS-Control veranlassen die Daten zu lesen
                        sECDataAddress    <= sECDataAddress + 1;        --Adressen-Auto-Inkrement mitfuehren  
                        sState            <= WAIT_FOR_BUSY_HIGH;
                        sNextState        <= READ_BYTES;
                     else
                        FSPImgBusy        <= '0';
                        ECReadDataEnable  <= '0';
                        sECDataAddress    <= sECDataAddress;
                        sState            <= READ_BYTES;
                     end if;
                  else 
                     FSPImgBusy        <= '1';                          --Beim beenden des Lesekommandos erfolgt die im EPCS/M25P unmittelbar
                     ECReadDataEnable  <= '0';                          --es wird kein Busy vom EPCS-Control mehr signalisiert
                     sECDataAddress    <= sECDataAddress;               --lediglich der tSHSL Wait-State ist zu beachten
                     ECExecuteCommand  <= '0';
                     ECCommand         <= CMD_NOP;
                     sState            <= WAIT_STATE;
                  end if;

               ---------------------------------------
               when others =>
                  sState            <= WAIT_FOR_START;

            end case;
         end if;
      End If;

      ECDataAddress     <= std_logic_vector(to_unsigned(sECDataAddress,24));

   End Process;
END rtl;