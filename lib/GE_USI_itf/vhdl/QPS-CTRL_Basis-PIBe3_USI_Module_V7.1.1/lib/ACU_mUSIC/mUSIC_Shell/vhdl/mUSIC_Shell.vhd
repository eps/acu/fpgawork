-- CREATED     "Thu Sep 09 08:43:40 2021"

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

LIBRARY work;

ENTITY mUSIC_Shell IS
GENERIC (
         gMainClockInHz                                        : INTEGER := 100000000;
         gM25PFSPImgStartAddress                               : STD_LOGIC_VECTOR(23 DOWNTO 0) := "000000000000000000000000";
         gM25PNoInitSearchRun                                  : STD_LOGIC := '0';
         gmCoreModuleNumber                                    : INTEGER := 1;
         gmCoreNumberOfFSPs                                    : INTEGER := 4;
         gmCoreTimeoutInSeconds                                : INTEGER := 1;
         gmCoreUseGenericModuleNumber                          : STD_LOGIC := '1';
         gMDHighSpeedPort_ByteIndexForNewDataTransmittedPulse  : unsigned(15 downto 0) := X"0000";
         gMDNumberOfHighSpeedReceiverSlots                     : INTEGER := 1;
         gMDNumberOfHighSpeedTransmitterSlots                  : INTEGER := 1;
         gMDUse_HighSpeedPort_AcceptNewDataRisingEdge          : BIT_VECTOR(3 downto 0) := "0000";
         gUSIRxFIFODepth                                       : INTEGER := 50;
         gUSITxFIFODepth                                       : INTEGER := 4;
         gUSIUseRAMForRxFIFO                                   : INTEGER := 0;
         gUSIUseRAMForTxFIFO                                   : INTEGER := 0
        );
   PORT (
         Clock                                        :  IN    STD_LOGIC;
         Reset                                        :  IN    STD_LOGIC;
         --mShell
         LED_Control                                  :  OUT   STD_LOGIC_VECTOR(3 DOWNTO 0);
         --USI
         USIRxSerial                                  :  IN    STD_LOGIC;
         USIControlStatus_Tri                         :  OUT   STD_LOGIC_VECTOR(15 DOWNTO 0);
         USITxSerial                                  :  OUT   STD_LOGIC;
         USITxDriverEnable                            :  OUT   STD_LOGIC;
         --M/D
         MDSelect_HighSpeedSlot                       :  IN    STD_LOGIC_VECTOR(1 DOWNTO 0);
         MDHighSpeedPort_In                           :  IN    STD_LOGIC_VECTOR(gMDNumberOfHighSpeedTransmitterSlots*32-1 DOWNTO 0);
         MDHighSpeedPort_AcceptNewDataRisingEdge      :  IN    STD_LOGIC_VECTOR(gMDNumberOfHighSpeedTransmitterSlots-1 DOWNTO 0);
         MDHighSpeedSlotFreeWheel                     :  IN    STD_LOGIC;
         MDHighSpeedPort_Out                          :  OUT   STD_LOGIC_VECTOR(gMDNumberOfHighSpeedReceiverSlots*32-1 DOWNTO 0);
         MDHighSpeedPort_NewDataReceivedPulse         :  OUT   STD_LOGIC_VECTOR(gMDNumberOfHighSpeedReceiverSlots-1 DOWNTO 0);
         MDHighSpeedPort_NewDataTransmittedPulse      :  OUT   STD_LOGIC_VECTOR(gMDNumberOfHighSpeedTransmitterSlots-1 DOWNTO 0);
         --mCore
         ModuleNumber                                 :  IN    STD_LOGIC_VECTOR(3 DOWNTO 0);
         USIHighSpeedEnable                           :  IN    STD_LOGIC;
         SetUSIMode                                   :  IN    STD_LOGIC;
         BitRate                                      :  IN    STD_LOGIC_VECTOR(2 DOWNTO 0);
         SetNewBitRate                                :  IN    STD_LOGIC;
         InputValueIsASCII                            :  IN    STD_LOGIC;
         RemainingBytes                               :  IN    STD_LOGIC_VECTOR(11 DOWNTO 0);
         FSPAvailable                                 :  IN    STD_LOGIC_VECTOR(gmCoreNumberOfFSPs DOWNTO 0);
         FSPReadOnly                                  :  IN    STD_LOGIC_VECTOR(gmCoreNumberOfFSPs DOWNTO 0);
         FIFOStartpointRAMRDAddr                      :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgMakeImage                              :  IN    STD_LOGIC;
         FSPImgFSPNumber                              :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgLoadImage                              :  IN    STD_LOGIC;
         FSPImgLoadImageAfterPowerUp                  :  IN    STD_LOGIC;
         USIInHighSpeedMode                           :  OUT   STD_LOGIC;
         ReceivedFSP                                  :  OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
         RWn                                          :  OUT   STD_LOGIC;
         Data                                         :  INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         ClockData                                    :  OUT   STD_LOGIC;
         CRCOK                                        :  OUT   STD_LOGIC;
         FIFOStartpointRAMDataQ                       :  OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);
         --M25P_Access
         FSPImgEraseAll                               :  IN    STD_LOGIC;
         FSPImgUseSectorAddress                       :  IN    STD_LOGIC;
         FSPImgSectorAddress                          :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgReady                                  :  OUT   STD_LOGIC;
         --EPCS_Control
         FROM_EPCS_DATA                               :  IN    STD_LOGIC;
         TO_EPCS_nCS                                  :  OUT   STD_LOGIC;
         TO_EPCS_ASDI                                 :  OUT   STD_LOGIC;
         TO_EPCS_DCLK                                 :  OUT   STD_LOGIC
       );
END mUSIC_Shell;

ARCHITECTURE bdf_type OF mUSIC_Shell IS

COMPONENT epcs_control
GENERIC (
         gMainClockInHz              : INTEGER;
         gMaxFrequencyForReadingInHz : INTEGER;
         gMaxFrequencyForWritingInHz : INTEGER
        );
   PORT(
         Clock            : IN  STD_LOGIC;
         Reset            : IN  STD_LOGIC;
         ExecuteCommand   : IN  STD_LOGIC;
         WriteDataEnable  : IN  STD_LOGIC;
         ReadDataEnable   : IN  STD_LOGIC;
         LSBFirst         : IN  STD_LOGIC;
         FROM_EPCS_DATA   : IN  STD_LOGIC;
         Command          : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
         DataAddress      : IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
         WriteData        : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         Busy             : OUT STD_LOGIC;
         TO_EPCS_nCS      : OUT STD_LOGIC;
         TO_EPCS_ASDI     : OUT STD_LOGIC;
         TO_EPCS_DCLK     : OUT STD_LOGIC;
         ReadData         : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
       );
END COMPONENT;

COMPONENT m25p_access
GENERIC (
         gFSPImgStartAddress  : STD_LOGIC_VECTOR(23 DOWNTO 0);
         gNoInitSearchRun     : STD_LOGIC
        );
   PORT(
         Clock                  : IN  STD_LOGIC;
         Reset                  : IN  STD_LOGIC;
         FSPImgEraseAll         : IN  STD_LOGIC;
         FSPImgUseSectorAddress : IN  STD_LOGIC;
         FSPImgWriteEnable      : IN  STD_LOGIC;
         FSPImgWriteData        : IN  STD_LOGIC;
         FSPImgReadEnable       : IN  STD_LOGIC;
         FSPImgReadData         : IN  STD_LOGIC;
         ECBusy                 : IN  STD_LOGIC;
         ECDataIn               : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgDataIn           : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgSectorAddress    : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgReady            : OUT STD_LOGIC;
         FSPImgBusy             : OUT STD_LOGIC;
         FSPImgReadDone         : OUT STD_LOGIC;
         ECExecuteCommand       : OUT STD_LOGIC;
         ECWriteDataEnable      : OUT STD_LOGIC;
         ECReadDataEnable       : OUT STD_LOGIC;
         ECCommand              : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
         ECDataAddress          : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
         ECDataOut              : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         FSPImgDataOut          : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
       );
END COMPONENT;

COMPONENT music_core
GENERIC (
         gICHMainClockInHz            : INTEGER;
         gICHModuleNumber             : INTEGER;
         gICHNumberOfFSPs             : INTEGER;
         gICHTimeoutInSeconds         : INTEGER;
         gICHUseGenericModuleNumber   : STD_LOGIC
        );
   PORT
   (
      Clock                         :  IN    STD_LOGIC;
      Reset                         :  IN    STD_LOGIC;
      ModuleNumber                  :  IN    STD_LOGIC_VECTOR(3 DOWNTO 0);
      USIHighSpeedEnable            :  IN    STD_LOGIC;
      SetUSIMode                    :  IN    STD_LOGIC;
      BitRate                       :  IN    STD_LOGIC_VECTOR(2 DOWNTO 0);
      SetNewBitRate                 :  IN    STD_LOGIC;
      BitRateValid                  :  IN    STD_LOGIC;
      InputValueIsASCII             :  IN    STD_LOGIC;
      RemainingBytes                :  IN    STD_LOGIC_VECTOR(11 DOWNTO 0);
      FSPAvailable                  :  IN    STD_LOGIC_VECTOR(gICHNumberOfFSPs DOWNTO 0);
      FSPReadOnly                   :  IN    STD_LOGIC_VECTOR(gICHNumberOfFSPs DOWNTO 0);
      FIFOStartpointRAMRDAddr       :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
      USI_RxFIFOnEmpty              :  IN    STD_LOGIC;
      USI_RxData                    :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
      USI_RxFIFODataNew             :  IN    STD_LOGIC;
      USI_TxIdle                    :  IN    STD_LOGIC;
      USI_TxFIFOnEmpty              :  IN    STD_LOGIC;
      USI_TxFIFOFull                :  IN    STD_LOGIC;
      USI_TxFIFODataFetched         :  IN    STD_LOGIC;
      MD_USI_TxGetDataFromTxData    :  IN    STD_LOGIC;
      MD_USI_RxPutDataToRxData      :  IN    STD_LOGIC;
      MD_Host_TunnelData            :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
      MD_Host_Strobe                :  IN    STD_LOGIC;
      MD_Host_Acknowledge           :  IN    STD_LOGIC;
      FSPImgMakeImage               :  IN    STD_LOGIC;
      FSPImgFSPNumber               :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
      FSPImgLoadImage               :  IN    STD_LOGIC;
      FSPImgLoadImageAfterPowerUp   :  IN    STD_LOGIC;
      FSPImgDataIn                  :  IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
      FSPImgReadDone                :  IN    STD_LOGIC;
      FSPImgBusy                    :  IN    STD_LOGIC;
      FSPImgReady                   :  IN    STD_LOGIC;
      USIInHighSpeedMode            :  OUT   STD_LOGIC;
      CurrentBitRate                :  OUT   STD_LOGIC_VECTOR(2 DOWNTO 0);
      SetUSI                        :  OUT   STD_LOGIC;
      BitRateChangeDonePulse        :  OUT   STD_LOGIC;
      ReceivedPID                   :  OUT   STD_LOGIC_VECTOR(15 DOWNTO 0);
      ReceivedMA                    :  OUT   STD_LOGIC_VECTOR(3 DOWNTO 0);
      ReceivedFSP                   :  OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
      RWn                           :  OUT   STD_LOGIC;
      SendAnswer                    :  OUT   STD_LOGIC;
      Data                          :  INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      ClockData                     :  OUT   STD_LOGIC;
      CRCOK                         :  OUT   STD_LOGIC;
      FIFOStartpointRAMDataQ        :  OUT   STD_LOGIC_VECTOR(31 DOWNTO 0);
      Host_MD_TunnelData            :  OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
      Host_MD_Strobe                :  OUT   STD_LOGIC;
      Host_MD_Acknowledge           :  OUT   STD_LOGIC;
      Host_MD_RxReadEnable          :  OUT   STD_LOGIC;
      Host_MD_TxWriteEnable         :  OUT   STD_LOGIC;
      Host_MD_TxData                :  OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
      FSPImgWriteEnable             :  OUT   STD_LOGIC;
      FSPImgWriteData               :  OUT   STD_LOGIC;
      FSPImgReadEnable              :  OUT   STD_LOGIC;
      FSPImgReadData                :  OUT   STD_LOGIC;
      FSPImgDataOut                 :  OUT   STD_LOGIC_VECTOR(7 DOWNTO 0)
   );
END COMPONENT;

COMPONENT usi
GENERIC (
         gRxFIFODepth      : INTEGER;
         gTxFIFODepth      : INTEGER;
         gUseRAMForRxFIFO  : INTEGER;
         gUseRAMForTxFIFO  : INTEGER;
         gWidth            : INTEGER
        );
   PORT(
         Clock                : IN  STD_LOGIC;
         Reset                : IN  STD_LOGIC;
         ManualReset          : IN  STD_LOGIC;
         USI_Select           : IN  STD_LOGIC;
         USI_Enable           : IN  STD_LOGIC;
         EnableTriStatePorts  : IN  STD_LOGIC;
         SetBitRate_RxOnOff   : IN  STD_LOGIC;
         GenBreak             : IN  STD_LOGIC;
         TxGetDataFromTxData  : IN  STD_LOGIC;
         RxEnable             : IN  STD_LOGIC;
         RxPutDataToRxData    : IN  STD_LOGIC;
         RxSerial             : IN  STD_LOGIC;
         TxData               : IN  STD_LOGIC_VECTOR(gWidth-1 DOWNTO 0);
         BitRate              : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
         TxDriverEnable       : OUT STD_LOGIC;
         TxSerial             : OUT STD_LOGIC;
         RxData               : OUT STD_LOGIC_VECTOR(gWidth-1 DOWNTO 0);
         RxData_Tri           : OUT STD_LOGIC_VECTOR(gWidth-1 DOWNTO 0);
         ControlStatus_Tri    : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
         MergDistHandshake    : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
         ResponseRegister_Tri : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
       );
END COMPONENT;

COMPONENT usi_merger_distributor
GENERIC (
         gHighSpeedPort_ByteIndexForNewDataTransmittedPulse : unsigned(15 downto 0);
         gNumberOfHighSpeedReceiverSlots                    : INTEGER;
         gNumberOfHighSpeedTransmitterSlots                 : INTEGER;
         gUse_HighSpeedPort_AcceptNewDataRisingEdge         : BIT_VECTOR(3 downto 0);
         gUSIMergerDistributorWatchdogCounts                : INTEGER
        );
   PORT(
         Clock                                       : IN  STD_LOGIC;
         Reset                                       : IN  STD_LOGIC;
         MD_Enable                                   : IN  STD_LOGIC;
         MD_Select                                   : IN  STD_LOGIC;
         ReceiveFirstBeforeSend                      : IN  STD_LOGIC;
         Host_MD_TxData                              : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         Host_MD_TxWriteEnable                       : IN  STD_LOGIC;
         Host_MD_RxReadEnable                        : IN  STD_LOGIC;
         Host_MD_TunnelData                          : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         Host_MD_Strobe                              : IN  STD_LOGIC;
         Host_MD_Acknowledge                         : IN  STD_LOGIC;
         USI_MD_RxData                               : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         USI_MD_TxFIFOFull                           : IN  STD_LOGIC;
         USI_MD_RxFIFOnEmpty                         : IN  STD_LOGIC;
         USI_MD_USITxFIFODataFetchedFromDataInFromMD : IN  STD_LOGIC;
         USI_MD_USIRxFIFONewDataAtDataOutForMD       : IN  STD_LOGIC;
         Select_HighSpeedSlot                        : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
         HighSpeedPort_In                            : IN  STD_LOGIC_VECTOR(gMDNumberOfHighSpeedTransmitterSlots*32-1 DOWNTO 0);
         HighSpeedPort_AcceptNewDataRisingEdge       : IN  STD_LOGIC_VECTOR(gMDNumberOfHighSpeedTransmitterSlots-1 DOWNTO 0);
         HighSpeedSlotFreeWheel                      : IN  STD_LOGIC;
         MDisEnabled                                 : OUT STD_LOGIC;
         MD_USI_TxData                               : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         MD_USI_TxGetDataFromTxData                  : OUT STD_LOGIC;
         MD_USI_RxPutDataToRxData                    : OUT STD_LOGIC;
         MD_Host_TunnelData_Tri                      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         MD_Host_Strobe_Tri                          : OUT STD_LOGIC;
         MD_Host_Acknowledge_Tri                     : OUT STD_LOGIC;
         MD_Host_NewHdShkNotCarried_Tri              : OUT STD_LOGIC;
         HighSpeedPort_Out                           : OUT STD_LOGIC_VECTOR(gMDNumberOfHighSpeedReceiverSlots*32-1 DOWNTO 0);
         HighSpeedPort_NewDataReceivedPulse          : OUT STD_LOGIC_VECTOR(gMDNumberOfHighSpeedReceiverSlots-1 DOWNTO 0);
         HighSpeedPort_NewDataTransmittedPulse       : OUT STD_LOGIC_VECTOR(gMDNumberOfHighSpeedTransmitterSlots-1 DOWNTO 0);
         MD_USI_Watchdog                             : OUT STD_LOGIC
       );
END COMPONENT;

SIGNAL   CurrentBitRate                         :  STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL   ECBusy                                 :  STD_LOGIC;
SIGNAL   ECReadData                             :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   FSPImgBusy                             :  STD_LOGIC;
SIGNAL   FSPImgDataIn                           :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   FSPImgDataOut                          :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   FSPImgReadData                         :  STD_LOGIC;
SIGNAL   FSPImgReadDone                         :  STD_LOGIC;
SIGNAL   FSPImgReadEnable                       :  STD_LOGIC;
SIGNAL   FSPImgReady_Wire                       :  STD_LOGIC;
SIGNAL   FSPImgWriteData                        :  STD_LOGIC;
SIGNAL   FSPImgWriteEnable                      :  STD_LOGIC;
SIGNAL   Host_MD_Acknowledge                    :  STD_LOGIC;
SIGNAL   Host_MD_RxReadEnable                   :  STD_LOGIC;
SIGNAL   Host_MD_Strobe                         :  STD_LOGIC;
SIGNAL   Host_MD_TunnelData                     :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   Host_MD_TxData                         :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   Host_MD_TxWriteEnable                  :  STD_LOGIC;
SIGNAL   MD_Host_Acknowledge_Tri                :  STD_LOGIC;
SIGNAL   MD_Host_Strobe_Tri                     :  STD_LOGIC;
SIGNAL   MD_Host_TunnelData_Tri                 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   MD_USI_RxPutDataToRxData               :  STD_LOGIC;
SIGNAL   MD_USI_TxData                          :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   MD_USI_TxGetDataFromTxData             :   STD_LOGIC;
SIGNAL   RxData_Tri                             :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   SetUSI                                 :  STD_LOGIC;
SIGNAL   USIInHighSpeedMode_Wire                :  STD_LOGIC;
SIGNAL   USIMergDistHandshake                   :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL   USIResponseRegister_Tri                :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL   ExecuteCommand                         :  STD_LOGIC;
SIGNAL   WriteDataEnable                        :  STD_LOGIC;
SIGNAL   ReadDataEnable                         :  STD_LOGIC;
SIGNAL   LSBFirst                               :  STD_LOGIC;
SIGNAL   Command                                :  STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL   DataAddress                            :  STD_LOGIC_VECTOR(23 DOWNTO 0);
SIGNAL   WriteData                              :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL   VCC_Wire                               :  STD_LOGIC;
SIGNAL   GND_Wire                               :  STD_LOGIC;

SIGNAL   Led_Control_Wire                       :  STD_LOGIC_VECTOR(3 DOWNTO 0);

BEGIN

   VCC_Wire <= '1';
   GND_Wire <= '0';

   Led_Control_Wire <= (MD_USI_TxGetDataFromTxData & MD_USI_RxPutDataToRxData & MD_Host_Strobe_Tri & Host_MD_Strobe);

   LED_Control <= Led_Control_Wire;


b2v_inst_EPCS_Control : epcs_control
GENERIC MAP(
            gMainClockInHz              => gMainClockInHz,
            gMaxFrequencyForReadingInHz => 20000000,
            gMaxFrequencyForWritingInHz => 20000000
           )
PORT MAP(
         Clock             => Clock,
         Reset             => Reset,
         ExecuteCommand    => ExecuteCommand,
         WriteDataEnable   => WriteDataEnable,
         ReadDataEnable    => ReadDataEnable,
         LSBFirst          => LSBFirst,
         FROM_EPCS_DATA    => FROM_EPCS_DATA,
         Command           => Command,
         DataAddress       => DataAddress,
         WriteData         => WriteData,
         Busy              => ECBusy,
         TO_EPCS_nCS       => TO_EPCS_nCS,
         TO_EPCS_ASDI      => TO_EPCS_ASDI,
         TO_EPCS_DCLK      => TO_EPCS_DCLK,
         ReadData          => ECReadData
        );


b2v_inst_M25P_Access : m25p_access
GENERIC MAP(
            gFSPImgStartAddress  => gM25PFSPImgStartAddress,
            gNoInitSearchRun     => gM25PNoInitSearchRun
           )
PORT MAP(
         Clock                   => Clock,
         Reset                   => Reset,
         FSPImgEraseAll          => FSPImgEraseAll,
         FSPImgUseSectorAddress  => FSPImgUseSectorAddress,
         FSPImgWriteEnable       => FSPImgWriteEnable,
         FSPImgWriteData         => FSPImgWriteData,
         FSPImgReadEnable        => FSPImgReadEnable,
         FSPImgReadData          => FSPImgReadData,
         ECBusy                  => ECBusy,
         ECDataIn                => ECReadData,
         FSPImgDataIn            => FSPImgDataOut,
         FSPImgSectorAddress     => FSPImgSectorAddress,
         FSPImgReady             => FSPImgReady_Wire,
         FSPImgBusy              => FSPImgBusy,
         FSPImgReadDone          => FSPImgReadDone,
         ECExecuteCommand        => ExecuteCommand,
         ECWriteDataEnable       => WriteDataEnable,
         ECReadDataEnable        => ReadDataEnable,
         ECCommand               => Command,
         ECDataAddress           => DataAddress,
         ECDataOut               => WriteData,
         FSPImgDataOut           => FSPImgDataIn
        );


b2v_inst_mUSIC_Core : music_core
GENERIC MAP(
            gICHMainClockInHz            => gMainClockInHz,
            gICHModuleNumber             => gmCoreModuleNumber,
            gICHNumberOfFSPs             => gmCoreNumberOfFSPs,
            gICHTimeoutInSeconds         => gmCoreTimeoutInSeconds,
            gICHUseGenericModuleNumber   => gmCoreUseGenericModuleNumber
           )
PORT MAP(
         Clock                         => Clock,
         Reset                         => Reset,
         ModuleNumber                  => ModuleNumber,
         USIHighSpeedEnable            => USIHighSpeedEnable,
         SetUSIMode                    => SetUSIMode,
         BitRate                       => BitRate,
         SetNewBitRate                 => SetNewBitRate,
         BitRateValid                  => USIResponseRegister_Tri(13),
         InputValueIsASCII             => InputValueIsASCII,
         RemainingBytes                => RemainingBytes,
         FSPAvailable                  => FSPAvailable,
         FSPReadOnly                   => FSPReadOnly,
         FIFOStartpointRAMRDAddr       => FIFOStartpointRAMRDAddr,
         USI_RxFIFOnEmpty              => USIResponseRegister_Tri(4),
         USI_RxData                    => RxData_Tri,
         USI_RxFIFODataNew             => USIResponseRegister_Tri(15),
         USI_TxIdle                    => USIResponseRegister_Tri(11),
         USI_TxFIFOnEmpty              => USIResponseRegister_Tri(0),
         USI_TxFIFOFull                => USIResponseRegister_Tri(2),
         USI_TxFIFODataFetched         => USIResponseRegister_Tri(14),
         MD_USI_TxGetDataFromTxData    => MD_USI_TxGetDataFromTxData,
         MD_USI_RxPutDataToRxData      => MD_USI_RxPutDataToRxData,
         MD_Host_TunnelData            => MD_Host_TunnelData_Tri,
         MD_Host_Strobe                => MD_Host_Strobe_Tri,
         MD_Host_Acknowledge           => MD_Host_Acknowledge_Tri,
         FSPImgMakeImage               => FSPImgMakeImage,
         FSPImgFSPNumber               => FSPImgFSPNumber,
         FSPImgLoadImage               => FSPImgLoadImage,
         FSPImgLoadImageAfterPowerUp   => FSPImgLoadImageAfterPowerUp,
         FSPImgDataIn                  => FSPImgDataIn,
         FSPImgReadDone                => FSPImgReadDone,
         FSPImgBusy                    => FSPImgBusy,
         FSPImgReady                   => FSPImgReady_Wire,
         USIInHighSpeedMode            => USIInHighSpeedMode_Wire,
         CurrentBitRate                => CurrentBitRate,
         SetUSI                        => SetUSI,
         BitRateChangeDonePulse        => open,
         ReceivedPID                   => open,
         ReceivedMA                    => open,
         ReceivedFSP                   => ReceivedFSP,
         RWn                           => RWn,
         SendAnswer                    => open,
         Data                          => Data,
         ClockData                     => ClockData,
         CRCOK                         => CRCOK,
         FIFOStartpointRAMDataQ        => FIFOStartpointRAMDataQ,
         Host_MD_TunnelData            => Host_MD_TunnelData,
         Host_MD_Strobe                => Host_MD_Strobe,
         Host_MD_Acknowledge           => Host_MD_Acknowledge,
         Host_MD_RxReadEnable          => Host_MD_RxReadEnable,
         Host_MD_TxWriteEnable         => Host_MD_TxWriteEnable,
         Host_MD_TxData                => Host_MD_TxData,
         FSPImgWriteEnable             => FSPImgWriteEnable,
         FSPImgWriteData               => FSPImgWriteData,
         FSPImgReadEnable              => FSPImgReadEnable,
         FSPImgReadData                => FSPImgReadData,
         FSPImgDataOut                 => FSPImgDataOut
        );


b2v_inst_USI : usi
GENERIC MAP(
            gRxFIFODepth      => gUSIRxFIFODepth,
            gTxFIFODepth      => gUSITxFIFODepth,
            gUseRAMForRxFIFO  => gUSIUseRAMForRxFIFO,
            gUseRAMForTxFIFO  => gUSIUseRAMForTxFIFO,
            gWidth      => 8
           )
PORT MAP(
         Clock                => Clock,
         Reset                => Reset,
         ManualReset          => Reset,
         USI_Select           => VCC_Wire,
         USI_Enable           => VCC_Wire,
         EnableTriStatePorts  => VCC_Wire,
         SetBitRate_RxOnOff   => SetUSI,
         GenBreak             => GND_Wire,
         TxGetDataFromTxData  => MD_USI_TxGetDataFromTxData,
         RxEnable             => VCC_Wire,
         RxPutDataToRxData    => MD_USI_RxPutDataToRxData,
         RxSerial             => USIRxSerial,
         TxData               => MD_USI_TxData,
         BitRate              => CurrentBitRate,
         TxDriverEnable       => USITxDriverEnable,
         TxSerial             => USITxSerial,
         RxData               => open,
         RxData_Tri           => RxData_Tri,
         ControlStatus_Tri    => USIControlStatus_Tri,
         MergDistHandshake    => USIMergDistHandshake,
         ResponseRegister_Tri => USIResponseRegister_Tri
        );


b2v_inst_USI_Merger_Distributor : usi_merger_distributor
GENERIC MAP(
            gHighSpeedPort_ByteIndexForNewDataTransmittedPulse => gMDHighSpeedPort_ByteIndexForNewDataTransmittedPulse,
            gNumberOfHighSpeedReceiverSlots                    => gMDNumberOfHighSpeedReceiverSlots,
            gNumberOfHighSpeedTransmitterSlots                 => gMDNumberOfHighSpeedTransmitterSlots,
            gUse_HighSpeedPort_AcceptNewDataRisingEdge         => gMDUse_HighSpeedPort_AcceptNewDataRisingEdge,
            gUSIMergerDistributorWatchdogCounts                => 0
           )
PORT MAP(
         Clock                                        => Clock,
         Reset                                        => Reset,
         MD_Enable                                    => USIInHighSpeedMode_Wire,
         MD_Select                                    => VCC_Wire,
         ReceiveFirstBeforeSend                       => VCC_Wire,
         Host_MD_TxData                               => Host_MD_TxData,
         Host_MD_TxWriteEnable                        => Host_MD_TxWriteEnable,
         Host_MD_RxReadEnable                         => Host_MD_RxReadEnable,
         Host_MD_TunnelData                           => Host_MD_TunnelData,
         Host_MD_Strobe                               => Host_MD_Strobe,
         Host_MD_Acknowledge                          => Host_MD_Acknowledge,
         USI_MD_RxData                                => RxData_Tri,
         USI_MD_TxFIFOFull                            => USIMergDistHandshake(0),
         USI_MD_RxFIFOnEmpty                          => USIMergDistHandshake(1),
         USI_MD_USITxFIFODataFetchedFromDataInFromMD  => USIMergDistHandshake(2),
         USI_MD_USIRxFIFONewDataAtDataOutForMD        => USIMergDistHandshake(3),
         Select_HighSpeedSlot                         => MDSelect_HighSpeedSlot,
         HighSpeedPort_In                             => MDHighSpeedPort_In,
         HighSpeedPort_AcceptNewDataRisingEdge        => MDHighSpeedPort_AcceptNewDataRisingEdge,
         HighSpeedSlotFreeWheel                       => MDHighSpeedSlotFreeWheel,
         MDIsEnabled                                  => open,
         MD_USI_TxData                                => MD_USI_TxData,
         MD_USI_TxGetDataFromTxData                   => MD_USI_TxGetDataFromTxData,
         MD_USI_RxPutDataToRxData                     => MD_USI_RxPutDataToRxData,
         MD_Host_TunnelData_Tri                       => MD_Host_TunnelData_Tri,
         MD_Host_Strobe_Tri                           => MD_Host_Strobe_Tri,
         MD_Host_Acknowledge_Tri                      => MD_Host_Acknowledge_Tri,
         MD_Host_NewHdShkNotCarried_Tri               => open,
         HighSpeedPort_Out                            => MDHighSpeedPort_Out,
         HighSpeedPort_NewDataReceivedPulse           => MDHighSpeedPort_NewDataReceivedPulse,
         HighSpeedPort_NewDataTransmittedPulse        => MDHighSpeedPort_NewDataTransmittedPulse,
         MD_USI_Watchdog                              => open
        );

USIInHighSpeedMode   <= USIInHighSpeedMode_Wire;
FSPImgReady          <= FSPImgReady_Wire;


END bdf_type;