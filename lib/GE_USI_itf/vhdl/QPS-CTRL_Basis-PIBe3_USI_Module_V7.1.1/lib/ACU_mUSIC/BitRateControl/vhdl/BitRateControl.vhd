LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : BitRateContol                                                                                   *
--*                                                                                                               *
--* Beschreibung: siehe Funktion                                                                                  *
--*                                                                                                               *
--* Version     : 18.05.10 Born (DS)                                                                              *
--*               17.10.13 'sBitRateOut' wurde im Zustand 'Reset' auf "000" gesetzt. Wird nun auf                 *
--*                        'gDefaultBitRate' gesetzt. Der 'StartUpCounter' zur verzoegerten Ausgabe der           *
--*                        Erstinitialisierung der USI wurde entfernt, da er ohnehin durch das Zaehlen nur bis 1  *
--*                        keinerlei Funktion mehr besass. (DS)                                                   *
--*               15.09.21 Umbennat von BaudRateControl nach BitRateControl                                       *
--*                        Namensapassungen für Ports durchgefuuehrt                                              *
--*                        Generic ..Version entfernt (DS)                                                        *
--*                                                                                                               *
--* Logikelemente   : 34                                                                                          *
--* Register        : 28                                                                                          *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************


-- Funktion:

-- Nachdem Reset wird zunaechst die 'gDefaultBitRate' gelesen und als zulaessige Bitrate in die USI eingetragen.
-- Dazu wird 'CurrentBitRate' auf 'gDefaultBitRate' gesetzt und 'SetUSI' = '1'. Hierdurch ubernimmt das USI Modul die
-- Standardbitrate.

-- Das Modul verarbeitet nur die Bitrate nicht die Bitrate.
-- Weil die Bitrate bisweilen immer 8 Bit ist, liegt diese fest verdrahtet am USI Modul an.
-- Die zu verwendende Bitrate ist in 3 Bit codiert
-- 000 -> 25    MBit
-- 001 -> 20    MBit
-- 010 -> 16,6  MBit
-- 011 -> 10    MBit
-- 100 ->  5    MBit
-- 101 ->  2    MBit
-- 110 ->  1    MBit
-- 111 -> 115,2 kBit
-- Diese drei Bit liegen am Eingang 'BitRate'.
-- Der Eingang 'BitRateValid' wird seitens des USI Moduls auf high gelegt, wenn dieses auf
-- eine gueltige Bitrate gesetzt ist.
-- Sobald am Eingang 'SetNewBitRate' eine low->high Flanke erkannt wird, wird die Information
-- von 'BitRate' uebernommen, an den Ausgang 'CurrentBitRate' gelegt und der Ausgang 'SetUSI' auf high gelegt.
-- Das USI Modul setzt 'BitRateValid' auf low und aendert seine Bitrate entsprechend. Anschliessend
-- setzt es 'BitRateValid' wieder auf high. Nun setzt 'BitRateControl' den Ausgang 'SetUSI' wieder auf low.
-- Die Bitrate wurde geaendert.
-- Wird der Eingang 'SetDefaultBitdRate' high, wird 'CurrentBitRate' auf 'gDefaultBitRate' (115,2 kBit (111)) gesetzt und
-- das USI Modul auf diesen Wert gelegt egal was an 'BitRate' ansteht.

-- ACHTUNG: Das USI Modul reagiert nur auf Aenderungen wenn 'USISet' am USI-Modul explizit
--          verwendet wird. D.h. nach einem Reset ist der Empfaenger des USI-Moduls zunaechst ausgeschaltet.
--          Soll dieser aktiviert werden, reicht es nicht den USI-Modul Eingang 'RxEnable' auf '1' zu setzen.
--          Es muss zusaetzlich 'USISet' bedient werden. Dabei wird aber auch die Bitrate uebernommen.
--          Das Modul 'BitRateControl' legt nach dem Reset die 'gDefaultBitRate' an 'CurrentBitRate' und bedient
--          'USISet', dadurch wird der Receiver des USI-Moduls auch aktiviert, sofern der Eingang RxEnable des USI-Moduls auf '1' liegt.


-- Wichtige Info zur Umschaltung der Bitrate:
-- ===========================================
-- Das Umschalten der Bitrate darf erst erfolgen, wenn das Modul die Antwort auf den neuen Bitratenwunsch gesendet hat.
-- Daher wird 'SendAnswer', 'USI_TxIdle' und 'USI_TxFIFOnEmpty' ueberwacht.
-- Zunaechst wird 'SetNewBitRate = '1'', dadurch weiss das Modul, dass eine neue Bitrate gewuenscht wird.
-- Als naechstes wird 'USI_TxIdle = '0'' und 'USI_TxFIFOnEmpty = '1'', dies zeigt an, dass der Transmitter arbeitet.
-- Reagiert wird hierbei auf 'USI_TxIdle = '0''.
-- Da 'USI_TxIdle' nach jedem uebertragenen Byte fuer ca. 2 Takte high wird, und 'USI_TxFIFOnEmpty' fuer ebenfalls ca. 1-2 Takte
-- low werden kann, muss die Bedingung zur abgeschlossenen Uebertragung 'USI_TxIdle = '1'' und 'USI_TxFIFOnEmpty = '0''
-- fuer wenigstens 2 Takte anstehen. Erst dann darf die Bitrate geaendert werden.


entity BitRateControl is                                                     -- Entity - Blockbeschreibung Ein- und Ausgaenge
   generic 
   (
      gDefaultBitRate       : std_logic_vector (2 downto 0) := B"111"        -- Standardbitrate (hier 115,2 kBit)
   );
----------------------------------------------------------------
   port 
   (
      Clock                      : in     std_logic;
      Reset                      : in     std_logic;

      BitRate                    : in     std_logic_vector (2 downto 0);      -- vom Benutzer gewuenschte Bitrate
      CurrentBitRate             : out    std_logic_vector (2 downto 0);      -- Sollbitrate fuer USI Modul

      SetNewBitRate              : in     std_logic;                          -- L->H-Flanke startet den Bitratenwechsel auf BitRate
      BitRateValid               : in     std_logic;                          -- Wenn '1' ist die neue Bitrate vom USI Modul eingestellt

      SetDefaultBitdRate         : in     std_logic;                          -- L->H Flanke setzt CurrentBitRate auf gDefaultBitRate
      SetUSI                     : out    std_logic;                          -- L->H-Flanke veranlasst USI zum Bitratenwechsel

      USI_TxIdle                 : in     std_logic;                          -- Wenn '1' ist USI Sender im Leerlauf
      USI_TxFIFOnEmpty           : in     std_logic;                          -- Wenn '1' sind noch Daten im USI Sender FIFO

      BitRateChangeDonePulse     : out    std_logic                           -- L->H->L Pulse wenn Statemachine durchlaufen ist
   );
end BitRateControl;

architecture RTL of BitRateControl is

-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer  BitRateContol
-------------------------------------------------------------------

   type     tState_BitRateContol is (    PO_RESET,
                                          WAIT_FOR_SET_NEW_BIT_RATE_HIGH,
                                          WAIT_FOR_IDLE_LOW,
                                          WAIT_ANSWER_SEND,
                                          SET_USI_HIGH,
                                          WAIT_FOR_BIT_RATE_VALID_LOW,
                                          SET_USI_LOW,
                                          WAIT_FOR_BIT_RATE_VALID_HIGH
                                 );

   signal   sState_BitRateContol : tState_BitRateContol;

   signal   sEdgeDetArray           : std_logic_vector (1 downto 0);
   signal   sEdgeDet                : std_logic;
   signal   sEdgeDetDefaultArray    : std_logic_vector (1 downto 0);
   signal   sEdgeDetDefault         : std_logic;

   signal   sSetBitRateToDefault   : std_logic;
   signal   sBitRateValid          : std_logic;
   signal   sSetUSI                 : std_logic;
   signal   sBitRateIn             : std_logic_vector (2 downto 0);
   signal   sBitRateOut            : std_logic_vector (2 downto 0);
   signal   sChangeDonePulse        : std_logic;

   signal   sUSI_TxIdle             : std_logic;
   signal   sUSI_TxFIFOnEmpty       : std_logic;

   signal   sWaitCounter            : integer range 3 downto 0 := 0;    -- Warten bis Bitratenumschaltbedingung sicher erfuellt ist
   constant cWaitCounterMax         : integer := 3;                     -- In diesem Fall mind. 3 Takte

--   constant cStartUpCounterMax      : integer := 1;      -- obsolet??
--   signal   sStartUpCounter         : integer range 0 to cStartUpCounterMax := 0;

   begin

-------------------------------------------------------------------
-- Flankenerkennungen
-------------------------------------------------------------------

   -- Flankenerkennung pEdgeDet (SetNewBitRate)
   -- ==========================================
   -- Wird eine Flanke erkannt sorgt dies dafuer, dass der Prozess
   -- 'pBitRateControl' die am Eingang 'BitRate' anliegende Bitrate
   -- im USI Modul setzt.
   pEdgeDet: process (Clock, Reset)
   begin
     if (Reset = '1') then
         sEdgeDetArray     <= (others => '0') ;
         sEdgeDet          <= '0';
     elsif (Clock'event and Clock = '1') then
         sEdgeDetArray(1 downto 0) <= (sEdgeDetArray(0) & SetNewBitRate);
         if (sEdgeDetArray = "01") then                                       --steigende Flanke
            sEdgeDet <= '1';
         else
            sEdgeDet <= '0';
         end if ;
     end if ;    
   end process pEdgeDet;


   -- Flankenerkennung pEdgeDet (sSetBitRateToDefault)
   -- =================================================
   -- Wird eine Flanke erkannt sorgt dies dafuer, dass der prozess
   -- 'pBitRateControl' die im Generic 'gDefaultBitRate' eingetragende Bitrate
   -- im USI Modul setzt.
   pEdgeDetDefault: process (Clock, Reset)
   begin
     if (Reset = '1') then
         sEdgeDetDefaultArray     <= (others => '0') ;
         sEdgeDetDefault          <= '0';
     elsif (Clock'event and Clock = '1') then
         sEdgeDetDefaultArray(1 downto 0) <= (sEdgeDetDefaultArray(0) & sSetBitRateToDefault);
         if (sEdgeDetDefaultArray = "01") then                                --steigende Flanke
            sEdgeDetDefault <= '1';
         else
            sEdgeDetDefault <= '0';
         end if ;
     end if ;    
   end process pEdgeDetDefault;

   ------------------------------------------------------
   -- pBitRateControl State Machine
   ------------------------------------------------------ 

   pBitRateContol : process (Clock, Reset)
   begin
      if (Reset = '1') then 
         
         sChangeDonePulse        <= '0';
         sBitRateOut            <= gDefaultBitRate;
         sSetUSI                 <= '0';
         sWaitCounter            <= 0;
         sState_BitRateContol   <= PO_RESET;
      else
         if (Clock'event and Clock = '1') then 

            sSetBitRateToDefault   <= SetDefaultBitdRate;
            sBitRateValid          <= BitRateValid;
            sBitRateIn             <= BitRate;                               -- Bitrate lesen
            sUSI_TxIdle             <= USI_TxIdle;      
            sUSI_TxFIFOnEmpty       <= USI_TxFIFOnEmpty;

   
            if (sEdgeDetDefault = '1') then                                      -- globles Ruecksetzten auf gDefaultBitRate bei '1'
               sBitRateOut            <= gDefaultBitRate;
               sState_BitRateContol   <= SET_USI_HIGH;
            else

               case (sState_BitRateContol) is

                  when PO_RESET  =>                                              -- nach dem Reset warten, dann die 'gDefaultBitRate' ausgeben

--                     if (sStartUpCounter < cStartUpCounterMax) then
--                        sStartUpCounter <= sStartUpCounter + 1;
--                     else
--                        sStartUpCounter         <= sStartUpCounter;
                        sBitRateOut            <= gDefaultBitRate;
                        sState_BitRateContol   <= SET_USI_HIGH;
--                     end if;

                  when WAIT_FOR_SET_NEW_BIT_RATE_HIGH =>                        -- warten auf L->H-'SetNewBitRate'

                     if (sEdgeDet = '1') then
                        sBitRateOut            <= sBitRateIn;                  -- Bitrate eintragen
                        --sState_BitRateContol   <= SET_USI_HIGH;
                        sState_BitRateContol   <= WAIT_FOR_IDLE_LOW;
                     else
                        sChangeDonePulse        <= '0';
                        sSetUSI                 <= '0';
                        sState_BitRateContol   <= WAIT_FOR_SET_NEW_BIT_RATE_HIGH;
                     end if;

                  when WAIT_FOR_IDLE_LOW  =>                                     -- Ein USI_TxIdle = '0' signalisiert, dass der
                                                                                 -- Sender die Arbeit aufgenommen hat
                     if (sUSI_TxIdle <= '0') then
                        sState_BitRateContol   <= WAIT_ANSWER_SEND;
                     else
                        sState_BitRateContol   <= WAIT_FOR_IDLE_LOW;
                     end if;

                  when WAIT_ANSWER_SEND   =>                                     -- warten bis Antwort gesendet wurde

                     if (sWaitCounter < cWaitCounterMax) then                    -- Zaehler nicht abgelaufen
                        if (sUSI_TxIdle = '1' and sUSI_TxFIFOnEmpty = '0') then
                           sWaitCounter <= sWaitCounter + 1;
                        else
                           sWaitCounter <= 0;
                        end if;
                     else
                        sWaitCounter            <= 0;
                        sState_BitRateContol   <= SET_USI_HIGH;
                     end if;

                  when SET_USI_HIGH =>                                           -- den Ausgang SetUSI auf High...

                     sSetUSI                 <= '1';                             -- ...dadurch uebernimmt das USI Modul die neue Bitrate
                     sState_BitRateContol   <= WAIT_FOR_BIT_RATE_VALID_LOW;

                  when WAIT_FOR_BIT_RATE_VALID_LOW =>                           -- noetig, weil USI Modul auf "011" testet, also...

                     if (sBitRateValid = '1') then                              -- ...insg. 2 Highzyklen benoetigt
                        sState_BitRateContol   <= WAIT_FOR_BIT_RATE_VALID_LOW; -- das USI Modul loescht 'BitRateValid'
                     else                          
                        sState_BitRateContol   <= SET_USI_LOW;
                     end if;

                  when SET_USI_LOW =>                                            -- den Ausgang SetUSI wieder auf low
                     sSetUSI                 <= '0';
                     sState_BitRateContol   <= WAIT_FOR_BIT_RATE_VALID_HIGH;
   
                  when WAIT_FOR_BIT_RATE_VALID_HIGH =>                           -- warten bis 'BitRateValid' wieder High

                     if (sBitRateValid = '1') then                              -- das USI Modul hat neue Bitrate eingestellt
                        sChangeDonePulse        <= '1';
                        sState_BitRateContol   <= WAIT_FOR_SET_NEW_BIT_RATE_HIGH;
                     else
                        sState_BitRateContol   <= WAIT_FOR_BIT_RATE_VALID_HIGH;
                     end if;

                  when others =>

                     sState_BitRateContol <= WAIT_FOR_SET_NEW_BIT_RATE_HIGH;

               end case;   --case sState_BitRateContol is

            end if;  --SetBitRateToDafult = '1') then 
         end if;  --if (Clock'event and Clock = '1') then 

      end if;  --if (Reset = '1') then 

   end process pBitRateContol;

   BitRateChangeDonePulse   <= sChangeDonePulse;
   SetUSI            <= sSetUSI;
   CurrentBitRate       <= sBitRateOut;

end RTL;
