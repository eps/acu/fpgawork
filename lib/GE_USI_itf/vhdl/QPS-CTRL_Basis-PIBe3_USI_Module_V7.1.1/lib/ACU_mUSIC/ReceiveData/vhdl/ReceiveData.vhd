LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : Receive Data (Teil des Interconnection Handler - ICH)                                           *
--*                                                                                                               *
--* Beschreibung: Der nachfolgende Prozess holt empfangene Daten vom USI, bzw. MD-Modul ab. Dabei wird jeweils ein*
--*               Byte gelesen. Dieses wird in 'sReceivedData' geschrieben und das Flag 'sNewDataAvailable' = '1'.*
--*               Beim einlesen der Daten wird unterschieden ob diese per USI-Normalmodus direkt vom USI-Modul    *
--*               kommen oder mittels USI-HiSpeed ueber den M/D extrahiert werden.                                *
--*                                                                                                               *
--* Version     : 18.05.10 Born (DS)                                                                              *
--*               10.08.10 'Port 'USI_RxFIFODataNew' hinzu gefuegt. An diesem Port wird per 1 Takt Puls           *
--*                        signalisiert, dass die Daten am Port 'USI_RxData' sich geaendert haben und nun vom     *
--*                        Modul eingelesen werden koennen. Dies ersetzt 'WaitState' Zaehler. (DS)                *
--*               dd.mm.yy 'FSPImgMakeLoadImageInProgress' Port neu hinzugefuegt. 'NewDataAvailable' darf erst '1'*
--*                        werden um nachfolgenden Instanzen zu signalisieren, dass am Port 'ReceivedData' neue   *
--*                        Daten abgeholt werden koennen, wenn die nachfolgende Instanz auch dazu in der Lage ist.*
--*                        Dieses Modul wartet nun auf '0' an 'FSPImgMakeLoadImageInProgress' bevor ggf.          *
--*                        der naechste empfangene Wert an 'ReceivedData' ausgegeben und 'NewDataAvailable'       *
--*                        gesetzt wird. Bisher wurde die Ausgabe einfach freilaufend durchgefuehrt, ohne         *
--*                        Ruecksicht darauf, ob die nachfolgende Instanz diese ueberhaupt abholen kann. Da i.d.R.*
--*                        der FSP_Interconnectionhandler auf diese Modul folgt und dieser nun auch ein ext. Flash*
--*                        bedienen kann, ist es moeglich, dass dieser evtl. zurzeit nicht auf dieses Modul       *
--*                        reagieren kann. Das 'Receive Data'-Modul wuerde aber ohne diese neue Rueckmeldung      *
--*                        munter Daten aus dem USI RxBuffer lesen und an den ICH schicken, ohne Wissen darum, ob *
--*                        dieser die Daten auch abgeholt hat. Der ICH gibt KEINE Rueckmeldung ob er die Daten    *
--*                        abgeholt hat. Geschwindigkeitstechnisch ist er aber in der Lage dies zu tun, bevor     *
--*                        dieses Modul das naechste empf. Byte ausgeben kann. Daher entfaellt ein Handshake.     *
--*                        INFO/TIP:                                                                              *
--*                        =========                                                                              *
--*                        'FSPImgMakeLoadImageInProgress' koennte aber genau fuer diesen Handshake mit einer     *
--*                        langsamen nachfolgene Instanz verwendet werden.                                        *
--*                        Wird 'NewDataAvailable' = '1' und setzt die nachfolgende Instanz                       *
--*                        'FSPImgMakeLoadImageInProgress' auf '1', wartet das Modul mit dem Abruf der naechsten  *
--*                        Daten aus dem Rx_Buffer bis 'FSPImgMakeLoadImageInProgress' wieder '0' wird.           *
--*               dd.mm.yy Diverse extra Zwischen-s*-Signale die ohnehin nur FPG intern genutzt werden entfernt   *
--*                        und deren Eingaenge stattdessen direkt abgefragt.                                      *
--*               15.09.21 Namensapassungen für Ports durchgefuuehrt                                              *
--*                        Generic ..Version entfernt (DS)                                                        *
--*                                                                                                               *
--*                                                                                                               *
--* Logikelemente   : 19                                                                                          *
--* Register        : 19                                                                                          *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************


entity ReceiveData is                                                   -- Entity - Blockbeschreibung Ein- und Ausgaenge
----------------------------------------------------------------
   port 
   (
      Clock                         : in  std_logic;
      Reset                         : in  std_logic;

--########################################################################
--# Kommunikationsports vom/zum USI-Modul und vom/zum Merger/Distributor #
--########################################################################

      USIInHighSpeedMode            : in  std_logic;                       -- wenn '1' = USI-HiSpeed, wenn '0' = USI-Normalmodus

      USI_RxData                    : in  std_logic_vector(7 downto 0);    -- vom USI-Modul empf. Daten (USI-Normalmodus)
                                                                           -- gesendet (USI-Normalmodus)
      USI_RxFIFODataNew             : in  std_logic;                       -- 1 Takt Puls zeigt Aenderung am Port 'USI_RxData' an
      Host_MD_RxReadEnable          : out std_logic;                       -- wenn '1' werden die Daten an 'USI_RxData' eingelesen (USI-Normalmodus)


      USI_RxFIFOnEmpty              : in  std_logic;                       -- wenn'1'stehen Daten zur Abholung im USI Modul bereit. 'Host_MD_RxReadEnable' darf '1'
                                                                           -- werden um diese abzuholen (USI-Normalmodus)

      MD_Host_TunnelData            : in  std_logic_vector(7 downto 0);    -- vom USI-Modul empf. Daten (USI-HiSpeed)
      MD_Host_Strobe                : in  std_logic;                       -- wenn '1' liegen an 'MD_Host_TunnelData' neue Daten an (USI-HiSpeed)
                                                                           -- wird wieder '0' wenn 'MD_Host_Acknowledge_Out' = '1' wird und die MFU diese Info
                                                                           -- empf. hat. Erst dann 'MD_Host_Acknowledge_Out' wieder auf '0' und Zyklus beginnt ggf. von vorne.

      Host_MD_Acknowledge           : out std_logic;                       -- wenn '1' signalisiert des Modul den Empf. eines Tunnelbytes (USI-HiSpeed)
                                                                           -- wenn '0' darf das naechste zu sendende Byte von der MFU geschickt werden
                                                                           -- wird wieder '0' wenn 'MD_Host_Strobe' = '0' wird und die MFU
                                                                           -- dadurch signalisiert das Acknowledge erhalten zu haben

--########################################################################
--# Kommunikationsports von/zur Peripherie usw.                          #
--########################################################################

      ReceivedData                  : out std_logic_vector(7 downto 0);    -- hier wird das empf. Byte (USI_Normalmodus oder HiSpeed) zur weiteren Verarbeitung
                                                                           -- bereit gestellt
      NewDataAvailable              : out std_logic;                       -- Puls L->H neue Daten an 'ReceivedData' vorhanden
      FSPImgMakeLoadImageInProgress : in  std_logic                        -- Solange dieses Signal '1' ist keine Daten von USI/MD einlesen

   );
end ReceiveData;


architecture RTL of ReceiveData is

-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer pReceiveUSIData
-------------------------------------------------------------------
type     tState_ReceiveData is   (  WAIT_FOR_DATA, 
                                    SET_RX_READ_ENABLE,
                                    READ_COMMON_DATA, 
                                    READ_TUNNEL_DATA,
                                    WAIT_FOR_STROBE_LOW
                                 );

signal sState_ReceiveData : tState_ReceiveData;

signal   sHost_MD_RxReadEnable                  : std_logic;
signal   sNewDataAvailable                      : std_logic;
signal   sHost_MD_Acknowledge_Out               : std_logic;
signal   sReceivedData                          : std_logic_vector(7 downto 0);

begin

   pReceiveData : process (Clock, Reset )
   begin
      if (Reset = '1') then 
         sState_ReceiveData               <= WAIT_FOR_DATA ;
         sHost_MD_RxReadEnable            <= '0';
         sNewDataAvailable                <= '0';
         sHost_MD_Acknowledge_Out         <= '0';
         sReceivedData                    <= (others => '0');

      else

         if (Clock'event and Clock = '1') then

            case (sState_ReceiveData) is

               ----------------------------------------------
               when WAIT_FOR_DATA =>                                    -- auf empf. Daten warten

                  sHost_MD_RxReadEnable            <= '0' ;             -- Pulse zum Daten lesen auf low
                  sNewDataAvailable                <= '0' ;             -- Flag auf low

                  if (USIInHighSpeedMode = '0') then                   -- wenn normaler Modus, kein USI-HiSpeed, dann...
                     if (USI_RxFIFOnEmpty = '1') and (FSPImgMakeLoadImageInProgress = '0') then   -- ...auf RxFIFOnEmpty reagieren
                        sState_ReceiveData <= SET_RX_READ_ENABLE ;
                     else
                        sState_ReceiveData <= WAIT_FOR_DATA ;
                     end if ;
                  else                                                  -- wenn USI_HiSpeed Modus, dann...
                     if (MD_Host_Strobe = '1') and (FSPImgMakeLoadImageInProgress = '0') then  -- ...auf Strobe Signal reagieren
                        sState_ReceiveData <= READ_TUNNEL_DATA;
                     else
                        sState_ReceiveData <= WAIT_FOR_DATA ;
                     end if;
                  end if;

               ----------------------------------------------
               when SET_RX_READ_ENABLE =>                               -- RxDataEnable auf High, USI legt Byte aus Empf.-FIFO an Port an

                  sHost_MD_RxReadEnable   <= '1' ;
                  sNewDataAvailable       <= '0' ;
                                                                        -- bis die Daten sicher an USI_RxDataIn anliegen dauert es ein paar Takte
                                                                        -- weil das USI Modul auf die steigende Flanke von sHost_MD_RxReadEnable
                                                                        -- reagieren muss (z.Zt. min. 2 Zyklen - '01')
                                                                        -- INFO: diese Flankenerkennung entspricht auch einem einfachen Puls, muss
                                                                        -- aber als Flankenerkennung bestehen, da auch die Nios2 CPU das USI Modul
                                                                        -- bedient und diese keinen 1 Takt Puls senden kann.
                  if(USI_RxFIFODataNew = '1') then
                     sState_ReceiveData  <= READ_COMMON_DATA ;
                  end if ;

               ----------------------------------------------
               when READ_COMMON_DATA =>                                 -- RxDataEnable auf Low, USI bereitet ggf. die Ausgabe weiterer Daten vor

                  sHost_MD_RxReadEnable   <= '0' ;

                  sReceivedData           <= USI_RxData ;
                  sNewDataAvailable       <= '1' ;                      -- durch '1' wird A2H-Wandlung der empf. Daten getriggert
                  sState_ReceiveData      <= WAIT_FOR_DATA;

               ----------------------------------------------
               when READ_TUNNEL_DATA =>                                 -- alternativ werden Daten bei HiSpeed per Tunnel empfangen
                                                                        -- diese stehen sofort an, wenn '..Strobe_In' = '1'
                  sReceivedData              <= MD_Host_TunnelData;  -- Daten an '...TunnelData_In' einlesen
                  sNewDataAvailable          <= '1' ;                   -- durch '1' wird A2H-Wandlung der empf. Daten getriggert
                  sHost_MD_Acknowledge_Out   <= '1';                    -- dem Sender mitteilen, dass Daten abgeholt wurden
                  sState_ReceiveData         <= WAIT_FOR_STROBE_LOW;

               ----------------------------------------------

               when WAIT_FOR_STROBE_LOW =>                              -- Sender legt Strobe='0', dann ggf. neue Daten auf den Bus und Strobe='1'

                  sNewDataAvailable          <= '0' ;

                  if (MD_Host_Strobe = '0') then
                     sHost_MD_Acknowledge_Out   <= '0';                 -- Empfaenger loescht die Bestaetigung, dass er Daten abgeholt hat
                     sState_ReceiveData         <= WAIT_FOR_DATA;
                  end if;

               ----------------------------------------------
               when others =>

                  sState_ReceiveData <= WAIT_FOR_DATA ;

            end case;
         end if ;
      end if ; 
   end process pReceiveData;

   --Asynchrone Ausgabe
   Host_MD_RxReadEnable    <= sHost_MD_RxReadEnable;
   Host_MD_Acknowledge <= sHost_MD_Acknowledge_Out;
   Host_MD_RxReadEnable    <= sHost_MD_RxReadEnable;
   NewDataAvailable        <= sNewDataAvailable;
   ReceivedData            <= sReceivedData;

end RTL;
   