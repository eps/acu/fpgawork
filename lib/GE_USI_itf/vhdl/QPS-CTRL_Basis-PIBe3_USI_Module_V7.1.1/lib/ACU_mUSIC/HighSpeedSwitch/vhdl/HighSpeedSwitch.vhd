LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : HighSpeedSwitch                                                                                 *
--*                                                                                                               *
--* Beschreibung: siehe Funktion                                                                                  *
--*                                                                                                               *
--* Version     : 11.05.10 Born (DS)                                                                              *
--*               08.06.10 SetUSISpeed in SetUSIMode umbeannt (DS)                                                *
--*               15.09.21 Namensapassungen für Ports durchgefuuehrt                                              *
--*                        Generic ..Version entfernt (DS)                                                        *
--*                                                                                                               *
--* Logikelemente   : 21                                                                                          *
--* Register        : 18                                                                                          *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************


-- Funktion:
-- =========
-- Das Modul schaltet zwischen USI-HighSpeed und USI-Normal um.
-- Durch 'USIHighSpeedEnable = '1'' und einem Puls an 'SetUSIMode = '1'' wird der
-- Ausgabeport 'USIInHighSpeedMode = '1'' sobald die Antwort auf den Aenderungswunsch
-- gesendet wurde. Wie beim Module 'BaudRateControl' wird auch hierbei 'USI_TxIdle' und 'USI_TxFIFOnEmpty' 
-- ueberwacht. Ist hingegen 'USIHighSpeedEnable = '0'' wird auch der Ausgang 'USIInHighSpeedMode = '0'.
-- Unabhaengig von einer Antwort wird 'USIInHighSpeedMode' auch immer '0', sobald der Eingang
-- 'SetUSIToDefaults = '1'' wird. Dieses Signal wird vom 'AutoFallBack' Modul erzeugt und erzwingt
-- eine Ruecksetzung auf eine Standard-Baurate und in den normalen USI Modus.


-- Wichtige Info zur Umschaltung zwischen HighSpeed und Normal USI:
-- ================================================================
-- Das Umschalten darf erst erfolgen, wenn das Modul die Antwort auf die neue Kommunikationsart gesendet hat.
-- 'USI_TxIdle' und 'USI_TxFIFOnEmpty' ueberwacht.
-- Zunaechst wird 'SetUSIMode = '1'', dadurch weiss das Modul, dass eine neue Kommunikationsart gewuenscht wird.
-- Als naechstes wird 'USI_TxIdle = '0'' und 'USI_TxFIFOnEmpty = '1'', dies zeigt an, dass der Transmitter arbeitet.
-- Reagiert wird hierbei auf 'USI_TxIdle = '0''.
-- Da 'USI_TxIdle' nach jedem uebertragenen Byte fuer ca. 2 Takte high wird, und 'USI_TxFIFOnEmpty' fuer ebenfalls ca. 1-2 Takte
-- low werden kann, muss die Bedingung zur abgeschlossenen Uebertragung 'USI_TxIdle = '1'' und 'USI_TxFIFOnEmpty = '0''
-- fuer wenigstens 2 Takte anstehen. Erst dann darf die Kommunikationsart geaendert werden.


entity HighSpeedSwitch is                                                     -- Entity - Blockbeschreibung Ein- und Ausgaenge
----------------------------------------------------------------
   port 
   (
      Clock                      : in     std_logic;
      Reset                      : in     std_logic;

      USIHighSpeedEnable         : in     std_logic;                          -- vom Benutzer gewuenschte Kommunikationsart
      USIInHighSpeedMode         : out    std_logic;                          -- Sollkommunikation fuer USI Modul

      SetUSIMode                 : in     std_logic;                          -- L->H-Flanke startet den Wechsel

      SetUSIToDefaults           : in     std_logic;                          -- L->H Flanke setzt auf normalen USI-Modus
      USI_TxIdle                 : in     std_logic;                          -- Wenn '1' ist USI Sender im Leerlauf
      USI_TxFIFOnEmpty           : in     std_logic;                          -- Wenn '1' sind noch Daten im USI Sender FIFO

      ChangeDonePulse            : out    std_logic                           -- L->H->L Puls wenn Statemachine durchlaufen ist
   );
end HighSpeedSwitch;

architecture RTL of HighSpeedSwitch is

-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer  HighSpeedSwitch
-------------------------------------------------------------------

   type     tState_HighSpeedSwitch is (   WAIT_FOR_START_CONDITION,
                                          WAIT_FOR_IDLE_LOW,
                                          WAIT_ANSWER_SEND,
                                          SET_SPEED
                                 );

   signal   sState_HighSpeedSwitch : tState_HighSpeedSwitch;

   signal   sEdgeDetArray           : std_logic_vector (1 downto 0);
   signal   sEdgeDet                : std_logic;
   signal   sEdgeDetDefaultArray    : std_logic_vector (1 downto 0);
   signal   sEdgeDetDefault         : std_logic;

   signal   sSetBaudRateToDefault   : std_logic;
   signal   sBaudRateValid          : std_logic;
   signal   sUSIInHighSpeedMem      : std_logic;
   signal   sUSIInHighSpeedIn       : std_logic;
   signal   sUSIInHighSpeedOut      : std_logic;
   signal   sChangeDonePulse        : std_logic;

   signal   sUSI_TxIdle             : std_logic;
   signal   sUSI_TxFIFOnEmpty       : std_logic;

   signal   sWaitCounter            : integer range 3 downto 0 := 0;    -- Warten bis Antwort sicher gesendet ist
   constant cWaitCounterMax         : integer := 3;                     -- In diesem Fall mind. 3 Takte

   begin

-------------------------------------------------------------------
-- Flankenerkennungen
-------------------------------------------------------------------

   -- Flankenerkennung pEdgeDet (SetUSIMode)
   -- ==========================================
   -- Wird eine Flanke erkannt sorgt dies dafuer, dass der Prozess
   -- 'pHighSpeedSwitch' die am Eingang 'USIHighSpeedEnable' anliegende Information
   -- an 'USIInHighSpeedMode' setzt
   pEdgeDet: process (Clock, Reset)
   begin
     if (Reset = '1') then
         sEdgeDetArray     <= (others => '0') ;
         sEdgeDet          <= '0';
     elsif (Clock'event and Clock = '1') then
         sEdgeDetArray(1 downto 0) <= (sEdgeDetArray(0) & SetUSIMode);
         if (sEdgeDetArray = "01") then                                       --steigende Flanke
            sEdgeDet <= '1';
         else
            sEdgeDet <= '0';
         end if ;
     end if ;    
   end process pEdgeDet;


   -- Flankenerkennung pEdgeDet (SetUSIToDefaults)
   -- =================================================
   -- Wird eine Flanke erkannt sorgt dies dafuer, dass der Prozess
   -- 'pHighSpeedSwitch' den Ausgang 'USIInHighSpeedMode' auf '0' setzt.
   pEdgeDetDefault: process (Clock, Reset)
   begin
     if (Reset = '1') then
         sEdgeDetDefaultArray     <= (others => '0') ;
         sEdgeDetDefault          <= '0';
     elsif (Clock'event and Clock = '1') then
         sEdgeDetDefaultArray(1 downto 0) <= (sEdgeDetDefaultArray(0) & SetUSIToDefaults);
         if (sEdgeDetDefaultArray = "01") then                                --steigende Flanke
            sEdgeDetDefault <= '1';
         else
            sEdgeDetDefault <= '0';
         end if ;
     end if ;    
   end process pEdgeDetDefault;

   ------------------------------------------------------
   -- pHighSpeedSwitch State Machine
   ------------------------------------------------------ 

   pHighSpeedSwitch : process (Clock, Reset)
   begin
      if (Reset = '1') then 
         
         sChangeDonePulse           <= '0';
         sUSIInHighSpeedOut         <= '0';
         sWaitCounter               <= 0;
         sState_HighSpeedSwitch     <= WAIT_FOR_START_CONDITION;
      else
         if (Clock'event and Clock = '1') then 

            sUSIInHighSpeedIn       <= USIHighSpeedEnable;
            sUSI_TxIdle             <= USI_TxIdle;      
            sUSI_TxFIFOnEmpty       <= USI_TxFIFOnEmpty;
   
            if (sEdgeDetDefault = '1') then                                      -- globles Ruecksetzten auf Normalen USI Modus
               sUSIInHighSpeedMem       <= '0';
               sState_HighSpeedSwitch   <= SET_SPEED;
            else

               case (sState_HighSpeedSwitch) is

                  -------------------------------------------------------------------
                  when WAIT_FOR_START_CONDITION =>                               -- warten auf L->H-'SetUSIMode'

                     sChangeDonePulse           <= '0';

                     if (sEdgeDet = '1') then
                        sUSIInHighSpeedMem      <= sUSIInHighSpeedIn;           -- Wunsch eintragen
                        sState_HighSpeedSwitch  <= WAIT_FOR_IDLE_LOW;
                     else
                        sChangeDonePulse        <= '0';
                        sState_HighSpeedSwitch  <= WAIT_FOR_START_CONDITION;
                     end if;

                  -------------------------------------------------------------------
                  when WAIT_FOR_IDLE_LOW  =>                                     -- Ein USI_TxIdle = '0' signalisiert, dass der
                                                                                 -- Sender die Arbeit aufgenommen hat
                     if (sUSI_TxIdle <= '0') then
                        sState_HighSpeedSwitch  <= WAIT_ANSWER_SEND;
                     else
                        sState_HighSpeedSwitch  <= WAIT_FOR_IDLE_LOW;
                     end if;

                  when WAIT_ANSWER_SEND   =>                                     -- warten bis Antwort gesendet wurde

                     if (sWaitCounter < cWaitCounterMax) then                    -- Zaehler nicht abgelaufen
                        if (sUSI_TxIdle = '1' and sUSI_TxFIFOnEmpty = '0') then
                           sWaitCounter         <= sWaitCounter + 1;
                        else
                           sWaitCounter         <= 0;
                        end if;
                     else
                        sWaitCounter            <= 0;
                        sState_HighSpeedSwitch  <= SET_SPEED;
                     end if;

                  when SET_SPEED =>                                              -- den Ausgang setzen

                     sUSIInHighSpeedOut       <= sUSIInHighSpeedMem;             -- ...dadurch uebernimmt das Modul den Wunsch
                     sChangeDonePulse         <= '1';
                     sState_HighSpeedSwitch   <= WAIT_FOR_START_CONDITION;

                  when others =>

                     sState_HighSpeedSwitch <= WAIT_FOR_START_CONDITION;

               end case;   --case sState_HighSpeedSwitch is

            end if;  --if (sEdgeDetDefault = '1') then 
         end if;  --if (Clock'event and Clock = '1') then 

      end if;  --if (Reset = '1') then 

   end process pHighSpeedSwitch;

   ChangeDonePulse   <= sChangeDonePulse;
   USIInHighSpeedMode <= sUSIInHighSpeedOut;

end RTL;
