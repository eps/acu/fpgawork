LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;
--use ieee.numeric_std.all

--08.02.11  Born (DS)
--07.03.11  'FSP_Data' von 'BiDir' auf 'In', da nie Daten ausgegeben werden (DS)
--12.08.13  Umlaute aus den Dokumentationstexten entfernt und durch 'ae', ue' 'oe' ersetzt (DS)
--08.10.13  'sClockDataEdgeDetArray' war anfangs uninitialisiert, ist es nun mit '00' (DS)
--15.09.21  Generic .Version entfernt (DS)

--Der 'ChecksumBuilder' ist Bestandteil von mUSIC, kann aber auch autonom verwendet werden
--Es handelt es sich dabei im Prinzip um ein Addierwerk, dass die ankommenden und vom
--Interconnection Handler von ASCII nach HEX gewandelten Daten auf dem 'FSP_Data'-Bus 
--mithoert, addiert und abschliessend mit der ebenfalls in einem eigenstaendigen FSP empfangenen Pruefsumme vergleicht.
--Das Addierwerk ist dabei 25 Bit breit, wobei das 25te Bit das Carrybit darstellt.
--Die Summe wird als Summand 2 dem Addierwerk wieder zugefuehrt, d.h. immer die unteren 24 Bit der Summe
--gehen wieder in die naechste Summenbildung ein.
--Die Summe wird geloescht durch loeschen des Eingangs 'ResetSum_n'.
--Nachdem alle Werte ueber 'FSP_Data' empfangen und aufaddiert wurden
--wird die mittels eigenem FSP empfangene 24 Bit Pruefsumme an den Eingang 'RcvdChecksumValue' gelegt.
--Der Ausgang 'ChecksumValid' wird immer dann auf '1' gesetzt, wenn 'RcvdChecksumValue' und die errechnete Pruefsumme gleich sind.
--Die errechnete Pruefsumme wird stets am Ausgang 'CalculatedChecksum' ausgegeben und kann darueber ggf. zurueck gelesen werden.

--Problem: Zu beachten ist dabei, dass durch das setzen des FSP13[7] Bit die Daten der Pruefsumme nicht mehr in die 
--         Pruefsummenberechnung des Moduls mit eingehen. Dies haette sonst zur Folge, dass sich die errechnete Pruefsumme 
--         wieder aendern wuerde. Die ganze Mimik wuerde sich also quasi selbst in den Schwanz beissen. Daher ist vor dem 
--         senden des 'FSP058_ParameterChecksumValue' das FSP13[7] und damit der Port 'ResetSum_n' wieder auf '1' zu 
--         setzen. Dann werden die Daten nicht aufaddiert.

entity ChecksumBuilder is

   generic  
   (
      gFSPNumberToOmit           : integer range 16#FF# downto 1  := 13 --wird dieses FSP adressiert werden die Daten nicht in die Pruefsumme aufaddiert
   );

   port
   (
      Clock                   : in     std_logic;
      Reset                   : in     std_logic;

      ResetSum_n              : in     std_logic;                     --H->L Flanke, aufsummierte Pruefsumme loeschen, 'ChecksumValid' auf '0'
      FSP_Data                : in     std_logic_vector(7 downto 0);  --Eingang fuer FSP Daten
      ClockData               : in     std_logic;                     --L->H->L (1 Pulse!) Daten an 'FSP_Data' uebernehmen und aufaddieren
      RWn                     : in     std_logic;                     --Pruefsummenbilder laeuft nur, wenn RWn = '0'
      
      RcvdChecksumValue       : in     std_logic_vector(23 downto 0); --Empf. Pruefsumme zum vergleichen mit der errechneten
      Address                 : in     std_logic_vector(7 downto 0);  --Vergleichsadresse um das FSP 'gFSPNumberToOmit' von der Prueefsummenbildung auszunehmen
      ChecksumValid           : out    std_logic;                     --empf. und errechnete Pruefsumme sind konsistent

      CalculatedChecksum      : out    std_logic_vector(23 downto 0)  --Ausgabe der errechneten Pruefsumme nach jeder Addition
   );
end ChecksumBuilder ;

ARCHITECTURE RTL OF ChecksumBuilder is


signal   sResetSumEdgeDetArray      : std_logic_vector (1 downto 0);
signal   sClockDataEdgeDetArray     : std_logic_vector (1 downto 0);
signal   sCalculatedChecksum        : std_logic_vector (24 downto 0);

begin

--------------------------------------------------------------------
--Pruefsummenbildung 
--------------------------------------------------------------------

   pChecksum : process (Clock, Reset)
   begin

      if (Reset = '1') then 
         sClockDataEdgeDetArray  <= (others => '0');
         sResetSumEdgeDetArray   <= (others => '0');         
         sCalculatedChecksum     <= (others => '0');
         ChecksumValid           <= '1';
      else
         if (Clock'event AND Clock = '1') then

            --FSP_Data     <= (others => 'Z');                      --(obsolet V0.2) FSP_Data als Ausgang ist immer Hi-Z

            if (RcvdChecksumValue(23 downto 0) = sCalculatedChecksum(23 downto 0)) then
               ChecksumValid    <= '1';
            else
               ChecksumValid    <= '0';
            end if;

            if (RWn = '0') then

               sResetSumEdgeDetArray(1 downto 0) <= sResetSumEdgeDetArray(0) & ResetSum_n;
               sClockDataEdgeDetArray(1 downto 0) <= sClockDataEdgeDetArray(0) & ClockData;

               if (sResetSumEdgeDetArray = "10") then             --H->L Flanke erkannt
                  sCalculatedChecksum     <= (others => '0');     --alte errechnete Pruefsumme loeschen
               else
                  if ((sClockDataEdgeDetArray = "01") and (Address /= gFSPNumberToOmit)) then  --L->H Flanke Addition durchfuehren, wenn FSP in Pruefsumme eingehen darf
                     if (ResetSum_n = '0') then                   --ResetSum_n = '0'
                        sCalculatedChecksum(24 downto 0) <= ('0' & sCalculatedChecksum(23 downto 0)) + ('0' & "0000" & FSP_Data(7 downto 0));
                     else
                        sCalculatedChecksum(24 downto 0) <= sCalculatedChecksum(24 downto 0);
                     end if;
                  end if;
               end if;
            else
               sCalculatedChecksum  <= sCalculatedChecksum;
            end if;
         end if;  
      end if ;
   end process pChecksum ;

   CalculatedChecksum(23 downto 0)     <= sCalculatedChecksum(23 downto 0);

end ARCHITECTURE RTL; 

--signal count : std_logic_vector(7 downto 0);
--count <= std_logic_vector(unsigned(count) + 1);