LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : TransmitData (Teil des Interconnection Handler - ICH)                                           *
--*                                                                                                               *
--* Beschreibung: Der nachfolgende Prozess gibt zu sendende Daten an das USI-, bzw. MD-Modul. Dabei wird jeweils  *
--*               ein Byte ausgegeben. Ist der Port 'TransmitterFree' = '1' wird das Byte vom ICH an              *
--*               'SendByteASCII' gelegt und 'NewDataTransferable' wird 'high'.                                   *
--*               'TransmitData' holt das Byte ab und unterscheidet ob dieses per USI-Normalmodus direkt vom      *
--*               USI-Modul gesendet wird oder mittels USI-HiSpeed ueber den M/D ausgegeben wird.                 *
--*                                                                                                               *
--* Version     : 18.05.10 Born (DS)                                                                              *
--*               10.08.10 'Port 'USI_TxFIFODataFetched' hinzu gefuegt. An diesem Port wird per 1 Takt Puls       *
--*                        signalisiert, dass die Daten vom Port 'Host_MD_TxData' vom USI Modul eingelesen    *
--*                        wurden. Dies ersetzt 'WaitState' Zaehler. (DS)                                         *
--*               10.12.12 'DiagVector' entfernt (DS)                                                             *
--*               dd.mm.yy Diverse extra Zwischen-s*-Signale die ohnehin nur FPG intern genutzt werden entfernt   *
--*                        und deren Eingaenge stattdessen direkt abgefragt.                                      *
--*               15.09.21 Namensapassungen für Ports durchgefuuehrt                                              *
--*                        Generic ..Version entfernt (DS)                                                        *
--*                                                                                                               *
--*                                                                                                               *
--* Logikelemente   : 39                                                                                          *
--* Register        : 35                                                                                          *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************



entity TransmitData is                                                   -- Entity - Blockbeschreibung Ein- und Ausgaenge
----------------------------------------------------------------
   port 
   (
      Clock                      : in  std_logic;
      Reset                      : in  std_logic;

--########################################################################
--# Kommunikationsports vom/zum USI-Modul und vom/zum Merger/Distributor #
--########################################################################

      USIInHighSpeedMode         : in  std_logic;                       -- wenn '1' = USI-HiSpeed, wenn '0' = USI-Normalmodus

      Host_MD_TxData             : out std_logic_vector(7 downto 0);    -- vom USI-Modul zu sendende Daten (USI-Normalmodus)

      Host_MD_TunnelData         : out std_logic_vector(7 downto 0);    -- vom USI-Modul mittels M/D zu sendende Daten (USI-HiSpeed)

      USI_TxFIFODataFetched      : in std_logic;                        -- 1 Takt Puls zeigt an, dass das USI Modul die Daten von 'Host_MD_TxData' eingelesen hat

      Host_MD_Strobe             : out std_logic;                       -- wenn '1' liegen an 'Host_MD_TunnelData' neue Daten an (USI-HiSpeed)
                                                                        -- wird wieder '0' wenn 'MD_Host_Acknowledge' = '1' wird und die MFU diese Info
                                                                        -- empf. hat. Erst dann 'Host_MD_Strobe' wieder auf '0' und Zyklus beginnt ggf. von vorne.

      Host_MD_TxWriteEnable      : out std_logic ;                      -- wenn '1' werden die Daten von 'Host_MD_TxData' an 'USI_TxData_In' eingelesen (USI-Normalmodus)

      USI_TxFIFOFull             : in  std_logic;                       -- wenn '1' koennen keine Daten an 'Host_MD_TxData' ausgegeben werden, weil
                                                                        -- der TxFIFO im USI Modul noch voll ist (USI-Normalmodus)

      MD_Host_Acknowledge        : in  std_logic;                       -- wenn '1' hat die MFU den Empf. eines Tunnelbytes signalisiert (USI-HiSpeed)
                                                                        -- wenn '0' darf das naechste zu sendende Byte an 'MD_Host_TunnelData_Out' gelegt werden
                                                                        -- wird wieder '0' wenn 'MD_Host_Strobe_Out' = '0' wird und die MFU diese Info ebenfalls
                                                                        -- empf. hat.

--########################################################################
--# Kommunikationsports von/zur Peripherie ICH usw.                      #
--########################################################################

      SendByteASCII              : in  std_logic_vector(7 downto 0);    -- hier wird das zu sendende Byte (USI_Normalmodus oder HiSpeed) zur weiteren Verarbeitung
                                                                        -- vom ICH bereit gestellt
      NewDataTransferable        : in  std_logic;                       -- Wenn '1' hat der ICH ein neues Byte an 'SendByteASCII' angelegt
      TransmitterFree            : out std_logic                        -- Wenn '1' darf der ICH ein neues Byte an 'SendByteASCII' anlegen und 'TransmitData' auf '1' legen
   );
end TransmitData;


architecture RTL of TransmitData is


-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer pReceiveUSIData
-------------------------------------------------------------------
type     tState_TransmitData is   ( WAIT_FOR_START_CONDITION, 
                                    DECIDE,
                                    WAIT_FOR_DATA_FETCHED, 
                                    CLEAR_TX_WRITE_ENABLE,
                                    WAIT_FOR_ACKNOWLEDGE
                                 );

signal   sState_TransmitData : tState_TransmitData;

signal   sSendByteASCII                         : std_logic_vector(7 downto 0);

begin

   pTransmitData : process (Clock, Reset)
   begin
      if (Reset = '1') then      

         Host_MD_Strobe         <= '0';
         Host_MD_TxData         <= (others => '0');
         Host_MD_TunnelData    <= (others => '0');
         Host_MD_TxWriteEnable      <= '0';
         TransmitterFree            <= '0';
         sSendByteASCII             <= (others => '0');         
         sState_TransmitData        <= WAIT_FOR_START_CONDITION ;

      else

         if (rising_edge(Clock)) then

            case (sState_TransmitData) is

               ----------------------------------------------------------------------
               when WAIT_FOR_START_CONDITION =>

                  TransmitterFree  <= '1';                                -- zeigt dem ICH an, dass dieser ein neues Byte schicken kann

                  if (NewDataTransferable = '1') then                      -- legt der ICH fuer einen Takt (Puls) auf 'High'
                     sSendByteASCII       <= SendByteASCII;                -- ASCII Byte einlesen
                     TransmitterFree      <= '0';
                     sState_TransmitData  <= DECIDE;
                  end if;

              ----------------------------------------------------------------------
              when DECIDE =>

                  if (USIInHighSpeedMode = '0') then                       -- entscheiden ob USI in HighSpeed oder Normalmodus ist
                     if (USI_TxFIFOFull = '0') then                        -- USI Transmitter FIFO ist nicht voll
                        Host_MD_TxData         <= sSendByteASCII;      -- Byte an Port fuer USI Versand anlegen
                        Host_MD_TxWriteEnable      <= '1' ;                -- USI mitteilen, dass neue Daten anliegen
                        Host_MD_TunnelData    <= X"00";               -- nicht noetig
                        sState_TransmitData        <= WAIT_FOR_DATA_FETCHED;
                     end if;

                  else                                                     --HighSpeed mit Tunnel fuer 'langsame' CPU Daten
 
                     if (MD_Host_Acknowledge = '0') then
                        Host_MD_TunnelData    <= sSendByteASCII;      -- Byte an Port fuer M/D Versand anlegen
                        Host_MD_TxData         <= X"00";               -- nicht noetig
                        Host_MD_Strobe         <= '1';                 -- Host mitteilen, dass neue Daten anliegen
                        sState_TransmitData        <= WAIT_FOR_ACKNOWLEDGE;
                     end if;
                  end if;


               ----------------------------------------------------------------------
               when WAIT_FOR_DATA_FETCHED =>

                  -- Hier gilt das Gleiche wie beim Empfaenger
                  -- Es muss ein paar Takte gewartet werden, bis das USI-Modul den Wert abgeholt hat (Flankenerkennung usw.)
                  -- daher die Wartezeit. Besser waehre ein sauberes Handshake -> Einbauen in neuem Release!
                  -- VORALLEM IST GARNICHT KLAR, OB DAS USI MODUL DEN WERT UEBERHAUPT ABGEHOLT HAT!
                  -- Ausserdem: Sollte der TxFIFO voll sein, kann diese Meldung zu spaet kommen und das TxModul schon den naechsten Wert ans USI-
                  -- Modul ausgegeben haben.

                  Host_MD_TxWriteEnable                  <= '0'; -- fuer USI-Modul nur 1 Puls zur Flankenerkennung (Array hat nur 2 Bit)

                  if(USI_TxFIFODataFetched = '1') then
                     TransmitterFree                     <= '1';           -- zeigt dem ICH an, dass dieser ein neues Byte schicken kann
                     sState_TransmitData                 <= WAIT_FOR_START_CONDITION ;
                  end if;

               ----------------------------------------------------------------------
               when WAIT_FOR_ACKNOWLEDGE =>                                --HighSpeed Modus

                  if (MD_Host_Acknowledge = '1') then                   --Host hat den Empfang der neuen Daten bestaetigt
                     Host_MD_Strobe   <= '0';                          --Strobesignal wieder zuruecknehmen
                     TransmitterFree      <= '1';                          -- zeigt dem ICH an, dass dieser ein neues Byte schicken kann
                     sState_TransmitData  <= WAIT_FOR_START_CONDITION;
                  end if;

               ----------------------------------------------------------------------
               when others =>

                  sState_TransmitData <= WAIT_FOR_START_CONDITION ;

            end case;

         end if;  -- if (Clock'event and Clock = '1') then 

      end if;  -- if (Reset = '1') then 

   end process pTransmitData;

end RTL;
   