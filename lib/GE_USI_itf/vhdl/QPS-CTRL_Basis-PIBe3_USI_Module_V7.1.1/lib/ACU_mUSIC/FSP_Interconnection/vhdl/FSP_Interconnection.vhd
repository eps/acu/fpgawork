LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;              --std_logic_vector(to_unsigned(..))


--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : FSP_Interconnection (Handler - ICH)                                                             *
--*                                                                                                               *
--* Beschreibung: Diese Modul stellt die Verknuepfungzentrale zwischen den einzelnen Modulen der mUSIC dar.       *
--*                                                                                                               *
--* Version     : 18.05.10 Born (DS)                                                                              *
--*               27.05.10 'sTransmitterFree' aus den vielen Einzelstates der sSendAnswer Statemachine in den     *
--*                        State WAIT_FOR_DATA_FETCHED verlegt (DS)                                               *
--*               28.05.10 FSP Suchlauf eingebaut (DS)                                                            *
--*               31.05.10 'RemainingBytes' auf 12 Bit erweitert (DS)                                             *
--*               11.08.10 Globales Timeout eingebaut. Sofern die USI Kommunikation einen unvollstaendigen        *
--*                        String schickt, wird nun nach 'gTimeoutInSeconds' der Empfang abgebrochen und auf ein  *
--*                        erneutes 'StartOfText' gewartet. (DS)                                                  *
--*               13.01.11 Die Fehlercodes wurden als negatives Komplement zurueck gegeben. Dies ist Quatsch,     *
--*                        weil seitens der MFU keine Pruefung und Konvertierung stattfindet. Da die Werte direkt *
--*                        an den Benutzer weitergereicht werden, sind diese nun positiv. (DS)                    *
--*               25.01.11 In 'SEND_ANSWER' konnte es passieren, dass 'cERR_CRCError' gesendet wurde, wenn der    *
--*                        CRC im Stream selbst gefehlt hat und sofort nach den Daten das ETX kam.                *
--*                        'RemainingBytes' war korrekter Weise = 0, aber 'CRCCompair /= CRC'. Dies wird nun in   *
--*                        einem weiteren 'elsif' abgefragt und in diesem Falle korrekt 'cERR_DataLengthError'    *
--*                        gesendet (DS)                                                                          *
--*               17.01.12 Die Ausgabe von 'ReceivedFSP' und 'Data' erfolgte nicht auf einen Schlag, sondern      *
--*                        Nibbleweise. Soll heissen: zuerst aenderte sich das MSB Nibble und im naechsten        *
--*                        Schritt das LSB Nibble. Dies sorgte dafuer, dass der Ausgang 'ReceivedFSP' als         *
--*                        Zwischenschritt FSP Nummern ausgab die nicht adressiert waren. Ebenso hat sich der     *
--*                        Ausgang 'Data' verhalten. Beim Ausgang 'Data' ist dies kein Problem gewesen, da die    *
--*                        Daten ohnehin erst mit 'ClockData' ihre Gueltigkeit erhalten. Dies ist nun mit einem   *
--*                        Zwischenregister zur Speicherung des MSB Nibble bis das LSB Nibble sauber ansteht      *
--*                        behoben. (DS)                                                                          *
--*               13.11.12 Das Modul ist nun in der Lage auf Kommando ein Abbild einzelner FSPs in einem externen *
--*                        Speicher (z.B. serielles Flash M25P) abzulegen und dieses auch wieder zu laden.        *
--*                        Zum Sichern des FSPs im ext. Speicher muss am Port 'FSPImgFSPNumber' die FSP Nummer    *
--*                        angelegt und an Port 'FSPImgMakeImage' der Zustand '1'  detektiert werden. Daraufhin   *
--*                        werden die Daten des FSPs abgefragt und ueber den Port 'SendByteASCII' ausgegeben.     *
--*                        Gestartet wird der Schreibvorgang mit einer '1' am Ausgang 'FSPImgWriteEnable'. Jedes  *
--*                        neue Datenbyte 'SendByteASCII' wird dabei mit einem Puls an 'FSPImgWriteData'          *
--*                        signalisiert. Die Ausgabe ist abhaengig vom Eingang 'FSPImgBusy'. Ist dieser Port '1'  *
--*                        wird gewartet bis dieser wieder '0' ist. Das Format der ausgegeben Daten ist dabei USI *
--*                        konform, also in ASCII:                                                                *
--*                        STX PID PID MA FSP FSP - Data - PP PP ETX, mit PID PID = WR.                           *
--*                        Dadurch ist es moeglich die Daten zu einem spaeteren Zeitpunkt aus dem ext. Speicher   *
--*                        zurueckzulesen und so zu tun als waeren diese ueber USI empfangen worden.              *
--*                        Soll das Abbild zurueckgelesen werden, muss an 'FSPImgLoadImage' der Zustand '1'       *
--*                        erkannt werden. Daraufhin werden die Daten aus dem ext. Speicher gelesen und so in     *
--*                        FSP-Interconnection verarbeitet, als ob diese via USI empfangen worden waeren. Der     *
--*                        Port 'FSPImgReadEnable' wird gesetzt, danach taktet 'FSPImgReadData' die Daten aus dem *
--*                        ext. Speicher, liest diese ueber 'FSPImgDataIn' ein und verteilt diese an den          *
--*                        richtigen FSP.                                                                         *
--*                        Wird 'FSPImgReadDone' = '1' ist das letzte Datenbyte gelesen und der ICH faellt in den *
--*                        WAIT_FOR_START_CONDITION  (DS)                                                         *
--*               07.10.13 Das FIFOStartpointRAM war nur 128 x 32 Bit gross. Da aber 256 FSP moeglich sind ist es *
--*                        nun 256 x 32 Bit. Diese Aenderung betrifft auch die MDS Instanzen. 'sDataOut' war      *
--*                        anfangs nicht initialisiert, ist nun X"00". (DS)                                       *
--*               15.09.21 Generic ..Version entfernt (DS)                                                        *
--*                                                                                                               *
--*                                                                                                               *
--* Logikelemente   : 561 (gNumberOfFSPs = 63)                                                                    *
--* Register        : 291                                                                                         *
--*                                                                                                               *
--*****************************************************************************************************************

-- Die Daten seitens des FSPInterconnectionHandlers liegen stehts als 8 Bit Hexadezimalwert zum/vom FSP an und werden innerhalb von
-- FSP_Output auf dessen FSP Tiefe gelegt.

-- 1. Ueber die USI werden Daten empfangen, die an ein FSP gelegt werden sollen:
--    -ICH empfaengt STX/ENQ[0x02/0x05], W[0x57], R[0x52], MA[0x31..0x38], FSP[0x0000...0x4646]
--    -stimmen alle Angaben, wird FSP adressiert und die nachfolgenden Daten von ASCII->HEX gewandelt und in
--     FSP eingetragen. Dabei wird aus den empf. Daten fortwaehrend ein CRC gebildet. Wird 'RemainingBytes' vom FSP -> X"000" 
--     muessen noch 2 Byte CRC seitens des Sender geschickt und empfangen werden. Der berechnete wird mit dem empf. CRC verglichen und bei pos. Ergebnis 'CRCOK'->'1'.
-- 2. Ueber die USI sollen Daten gelesen werden:
--    -ICH empfaengt STX/ENQ[0x02/0x05], R[0x52], D[0x44], MA[0x31..0x38], FSP[0x0000...0x4646], ETX[0x03]
--    -stimmen alle Angaben, wird FSP addressiert und nachfolgend dessen Inhalt HEX->ASCII gewandelt und ueber die
--     USI ausgebeben. Der CRC wird dabei aus den Hexwerten VOR der Hex->ASCII Wandlung gebildet. Abgeschlossen mit CRC und ETX/EOT.

-- Zum CRC: 
-- ========
-- Der CRC wird als XOR Verknuepfung aus den Datenbytes in Hexform gebildet. D.h. werden z.B. 4 Byte Hex gesendet, geschieht dies als
-- 8 Byte ASCII. Der CRC Wert wird aber durch XOR Verknuepfung der 4 Byte Hex erzeugt un der daraus gebildete CRC ebenfalls als 2 Byte
-- ASCII gesendet.


-- Seek and Find (FSP_SeekAndFind)
-- ===============================
-- Dabei handelt es sich um einen 'automatischen Suchlauf' zur Bestimmung aller an den Interconnectionhandler
-- angebundenen FSPs. Dieser Suchlauf dient dazu Informationen ueber die vorhandenen FSPs in den MDS einzubinden.

-- FSPImg
-- =======
-- Die Steuerung der FSPImg* Eingangsports wird ueber ein FSP vorgenommen. Das Problem ist, dass dieses FSP vom Host programmiert werden muss
-- und FSP_Interconnection darauf zunaechst die entsprechende Antwort an den Host sendet.
-- Solange die Antwort noch nicht gesendet ist, darf kein Image geschrieben oder gelesen werden, weil dabei
-- teilweise die gleichen States zur Anwendung kommen. Ausserdem gilt, dass eine Flanke an 'FSPImgMakeImage', bzw. 'FSPImgLoadImg'
-- nicht erkannt werden kann, sofern 'sState_CheckDataConformity' nicht auf 'WAIT_FOR_START_CONDITION' steht.
-- Wird das 'FSPImgFSP' mit gueltigen Daten gesetzt, so dass an den Eingaengen eine Flanke erzeugt wird, ist 'sState_CheckDataConformity'
-- aber mitunter noch nicht nach 'WAIT_FOR_START_CONDITION' zurueckgekehrt.
-- Daher wird die Starterkennung wie folgt erzeugt:
-- Das 'FSPImg*Image' wird mittels FSP gesetzt, d.h. noch waehrend die Antwort an den Host gesendet wird, wird der korrespondierende
-- Eingangsport '1'. Kehrt 'sState_CheckDataConformity' nun nach 'WAIT_FOR_START_CONDITION' zurueck, wird der Eingansport ueberprueft.
-- Ist dieser ungleich dem vorherigen Zustand und '1' wird die 'Make', bzw. 'Load' Funktion gestartet aber nicht ausgefuehrt.
-- Die Ausfuehrung beginnt erst dann, wenn 'sState_SendAnswer' ebenfalls wieder nach 'WAIT_FOR_START_CONDITION'
-- zurueckgekehrt ist.

entity FSP_Interconnection is                                                    -- Entity - Blockbeschreibung Ein- und Ausgaenge
   generic 
   (
      gTimeoutInSeconds                   : integer := 1;
      gMainClockInHz                      : integer := 100_000_000;
      gUseGenericModuleNumber             : std_logic := '1';                    --Wenn '1' wird 'gModuleNumber' verwendet, wenn '0' ModuleNumber'
      gModuleNumber                       : integer range 16#F#  downto 0 := 1;
      gNumberOfFSPs                       : integer range 16#FF# downto 0 := 63  -- Anzahl der FSP, bestimmt Breite von FSPAvailable und FSPReadOnly
   );
----------------------------------------------------------------
   port 
   (
      Clock                      : in    std_logic;
      Reset                      : in    std_logic;

--########################################################################
--# I/O Ports                                                            #
--########################################################################

      --# Global
      ModuleNumber                  : in  std_logic_vector(3 downto 0);       -- alternative Modulnummer
      USIInHighSpeedMode            : in  std_logic;                          -- wenn '1' = USI-HiSpeed, wenn '0' = USI-Normalmodus

      --# Receive
      NewDataAvailable              : in  std_logic;                          -- Wenn '1' wurde ein neues ASCII Byte empfangen und kann eingelesen werden
      ReceivedData                  : in  std_logic_vector(7 downto 0);       -- vom USI-Modul empf. Daten

      ReceivedPID                   : out std_logic_vector(15 downto 0);      -- Das empf. PID (RD/WR), setzt 'sRWn'
      ReceivedMA                    : out std_logic_vector(3 downto 0);       -- Die empf. Modulnummer
      ReceivedFSP                   : out std_logic_vector(7 downto 0);       -- Der empf. FSP, adressiert den FSP
      RWn                           : out std_logic;                          -- Wird durch 'sReceivedPID' gesetzt, wenn '1' -> der Host will Daten lesen

      Error                         : out std_logic_vector(7 downto 0);       -- Tritt ein Fehler auf ist dieser Wert /= 0 und wird an den Host gesendet

      --# Hilfe
      SendAnswer                    : out std_logic;                          -- Wenn'1' wird die Statemachine zum generieren der Antwort gestartet

      -- Transmitter
      SendByteASCII                 : out std_logic_vector(7 downto 0);       -- Das von HEX nach ASCII gewandelte Nibble zum senden an den Host
      NewDataTransferable           : out std_logic;                          -- Wenn '1' kann Transmitter neues ASCII Byte an Host senden
      TransmitterFree               : in  std_logic;                          -- Wenn '1' kann ein neuer Wert an den Host uebertragen werden

      -- FSP
      FSPAvailable                  : in  std_logic_vector(gNumberOfFSPs downto 0);   -- Wenn ein Bit '1' ist dieses FSP existent
      FSPReadOnly                   : in  std_logic_vector(gNumberOfFSPs downto 0);   -- Wenn ein Bit '1' ist der adressierte FSP nur lesbar. Schreiben darauf erzeugt einen Fehlercode
      InputValueIsASCII             : in  std_logic;                          -- Wenn '1' darf der Wert an 'Data' nicht mehr von HEX nach ASCII gewandelt werden
      Data                          : inout std_logic_vector( 7 downto 0);    -- Bidirektionaler Datenein-/ausgang vom/zum FSP
      ClockData                     : out std_logic;                          -- Wenn '1' werden Daten am Ausgang 'Data' in den FSP uebernommen
      RemainingBytes                : in  std_logic_vector(11 downto 0);      -- Anzahl noch freier Bytes im FSP
      CRCOK                         : out std_logic;                          -- Wenn '1' war CRC erfolgreich, Daten an Peripherie ausgeben

      -- A2H
      ASCIIOut                      : out std_logic_vector(7 downto 0);       -- Muss der empf. ASCII gewandelt werde, so ist dies der Ausgang zum ASCII->Hex Dekoder
      StartConvertA2H               : out std_logic;                          -- Wenn '1' -> Start der ASCII nach HEX Konvertierung (Pulse!)
      A2HConvDone                   : in  std_logic;                          -- Wenn '1' ist die Wandlung abgeschlossen, es steht ein neuer Hex-Wert an 'HexIn'
      A2HError                      : in  std_logic;                          -- Wenn '1' war der ASCII Wert zur Wandlung nicht gueltig
      HexIn                         : in  std_logic_vector(3 downto 0);       -- Muss der empf. ASCII gewandelt werde, so ist dies der Eingang vom ASCII->Hex Dekoder

      -- H2A
      HexOut                        : out std_logic_vector(3 downto 0);       -- Hexnibble Ausgang zum H2A Converter
      StartConvertH2A               : out std_logic;                          -- Wenn '1' -> Start der Hex nach ASCII Konvertierung (Pulse!)
      H2AConvDone                   : in  std_logic;                          -- Wenn '1' -> Hex nach ASCII Konverteirung abgeschlossen (Pulse!)
      ASCIIIn                       : in  std_logic_vector(7 downto 0);       -- ASCII Eingang vom H2A Converter

      -- FIFO Startpunkt RAM
      FIFOStartpointRAMData         : out std_logic_vector(31 downto 0);      -- Ausgabe der Daten an das FIFO Startpunkt RAM
      FIFOStartpointRAMWRAddr       : out std_logic_vector( 7 downto 0);      -- Adresse des FIFO Startpunkt RAM
      FIFOStartpointRAMWREnable     : out std_logic;                          -- WR enable Signal des FIFO Startpunkt RAM

      -- Generate/Load FSP Image (Abbild von FSPs in einen ext. Speicher sichern, bzw. von dort laden)

      FSPImgMakeImage               : in  std_logic;                          --L->H Flanke sichert die Daten aus dem 'FSPImgFSPNumber' FSP
      FSPImgWriteEnable             : out std_logic;                          --Wenn '1' startet der Flash-Handler den Schreibvorgang
      FSPImgFSPNumber               : in  std_logic_vector(7 downto 0);       --Die Daten dieses FSPs sollen im ext. Speicher gesichert werden
      FSPImgBusy                    : in  std_logic;                          --Wenn '1' muss FSP-ICH warten bis ext. Flashhandler bereit ist (lesen/schreiben/loeschen)
      FSPImgWriteData               : out std_logic;                          --L->H Pulse als Info darueber, dass neue Daten an 'SendByteASCII' zum sichern anstehen

      FSPImgReady                   : in  std_logic;                          --Initzugriffe auf das Speichermodul sind nur erlaubt, wenn dieser Port '1' ist (FSPImgReadEnable/FSPImgWriteEnable)

      FSPImgLoadImage               : in  std_logic;                          --L->H Flanke startet das Laden von FSP Daten aus dem ext. Speicher
      FSPImgLoadImageAfterPowerUp   : in  std_logic;                          --Wenn '1' wird nach dem PowerUp/Reset ein evtl. vorhandenes Image
                                                                              --aus dem ext. seriellen Speicher geladen

      FSPImgReadEnable              : out std_logic;                          --Wenn '1' startet der Flash-Handler den Lesevorgang
      FSPImgReadData                : out std_logic;                          --L->H Pulse als Info darueber, dass neue Daten an 'FSPImgDataIn' erwartet werden
      FSPImgDataIn                  : in  std_logic_vector(7 downto 0);       --Die vom ext. Flash gelesenen Daten werden hier angelegt
      FSPImgReadDone                : in  std_logic;                          --beim Lesen vom ext. Flash wurde das letzte Datenbyte gelesen

      FSPImgMakeLoadImageInProgress : out std_logic                           -- Ist '1', wenn Daten ins ext. Flash geschrieben, bzw. aus dem ext. Flash geladen 
                                                                              -- werden, der ICH kann keine Daten ueber USI auswerten, diese muessen solange im USIRxBuffer
                                                                              -- verbleiben.

   );
end FSP_Interconnection;


architecture RTL of FSP_Interconnection is

   -------------------------------------------------------------------
   -- Konstanten fuer Fehlermeldungen
   -------------------------------------------------------------------
   --die MFU gibt intern Fehlermeldungan als negative Werte zurueck, die Asugabe zum Benutzer
   --erfolgt aber mit den positiven Komplenetwerten. Die Fehlercodes der Module werden durch die
   --MFU nur hindurch gereicht und dabei nicht geprueft. Aus diesem Grund werden die Fehlercodes
   --der Module sosft als positive Fehlermeldungen zurueck gegeben und nicht als negatives Komplement.
   
   constant cERR_FSPNotAvailable    : std_logic_vector (7 downto 0) := X"57"; --X"A9"; -- FSP nicht vorhanden (-87)
   constant cERR_DataLengthError    : std_logic_vector (7 downto 0) := X"55"; --X"AB"; -- Datenlaenge stimmt nicht mit FSP Tiefe ueberein (-85)
   constant cERR_NoDataPermitted    : std_logic_vector (7 downto 0) := X"50"; --X"B0"; -- keine Daten erlaubt, FSP soll gelesen werden (-80)
   constant cERR_PIDError           : std_logic_vector (7 downto 0) := X"04"; --X"FC"; -- PID nicht verstanden, MA stimmt aber PID ist nicht bekannt (-04)
   constant cERR_A2HConversion      : std_logic_vector (7 downto 0) := X"4B"; --X"B5"; -- Fehler bei der ASCII nach Hex Konvertierung, Daten nicht ASCII (-75)
   constant cERR_FSPReadOnly        : std_logic_vector (7 downto 0) := X"53"; --X"AD"; -- FSP kann nur gelesen und nicht beschrieben werden (-83)
   constant cERR_CRCError           : std_logic_vector (7 downto 0) := X"0C"; --X"F4"; -- Fehler bei der CRC Bildung (-12)

   -------------------------------------------------------------------
   -- Signale, Typen, Konstanten fuer Statemcachine 'tState_CheckDataConformity'
   -------------------------------------------------------------------
   type     tState_CheckDataConformity is  ( INITIAL_SEEK,              -- Start Suchlauf
                                             SEEK,
                                             WAIT_STATE,
                                             WAIT_STATE_1,
                                             WAIT_STATE_2,
                                             WAIT_STATE_3,
                                             WAIT_STATE_4,
                                             READ_FSP_INFO,
                                             SET_WR,
                                             DEL_WR,
                                             INCREMENT,
                                             SET_WR_1,
                                             DEL_WR_1,
                                             WAIT_STATE_5,
                                             WAIT_STATE_6,               -- Ende Suchlauf 
                                             WAIT_FOR_START_CONDITION,
                                             FSP_IMG_START_MAKE,
                                             FSP_IMG_START_LOAD,
                                             FSP_IMG_CONFIRM_STX,
                                             RECEIVE_STX,
                                             RECEIVE_PID,
                                             CHECK_PID_CONFORMITY,
                                             RECEIVE_MA,
                                             CONFIRM_MA,
                                             RECEIVE_FURTHER_DATA,
                                             SEND_ANSWER, 
                                             WAIT_FOR_FSP_IMAGE_DONE
                                           );

   signal   sState_CheckDataConformity       : tState_CheckDataConformity;

   signal   sUSIInHighSpeedMode           : std_logic;
   signal   sModuleNumber                 : std_logic_vector( 3 downto 0);
   signal   sNewDataAvailable             : std_logic;
   signal   sReceivedData                 : std_logic_vector( 7 downto 0);

   signal   sReceivedFSPAndDataMSBBuffer  : std_logic_vector (3 downto 0);
   signal   sReceivedPID                  : std_logic_vector(15 downto 0);
   signal   sReceivedMA                   : std_logic_vector( 3 downto 0);
   signal   sReceivedFSP                  : std_logic_vector( 7 downto 0);
   signal   sError                        : std_logic_vector( 7 downto 0);
   signal   sSendAnswer                   : std_logic;
   signal   sRWn                          : std_logic;

   signal   sSyncStartByte                : std_logic_vector( 7 downto 0);
   signal   sSyncEndByte                  : std_logic_vector( 7 downto 0);

   signal   sClockData                    : std_logic;
   signal   sRemainingBytes               : std_logic_vector (11 downto 0);
   signal   sMemRemainingBytes            : std_logic_vector (11 downto 0);
   signal   sCRCOK                        : std_logic;
   signal   sCRCCompare                   : std_logic_vector ( 7 downto 0);
   signal   sCRC                          : std_logic_vector ( 7 downto 0);
   signal   sCRCReceived                  : std_logic;
   signal   sCalcCRCPulse                 : std_logic;
   signal   sEnableCRCPulse               : std_logic;

   signal   sTimeoutCounter               : integer range 0 to (gMainClockInHz * gTimeoutInSeconds) := 0;   -- V0.3
   constant cTimeoutCounterMax            : integer := (gMainClockInHz * gTimeoutInSeconds);                -- V0.3 nach 1 Sekunde erfolgt ein Timeout
   signal   sTimeoutCounterRunning        : std_logic := '0';                             -- V0.3 wenn '1' laeuft Timeout Zaehler
   signal   sTimeoutCounterReset          : std_logic := '0';                             -- V0.3 wenn '1' wird Timeout Zaehler geloescht

   -------------------------------------------------------------------------
   -- Signale, Typen, Konstanten fuer FSPImg*
   -------------------------------------------------------------------------

   signal sFSPImgMakeImage                : std_logic;
   signal sFSPImgLoadImage                : std_logic;


   signal sFSPImgMakeImagePrev            : std_logic;
   signal sFSPImgLoadImagePrev            : std_logic;
   
   signal sFSPImgMakeImageInProgress      : std_logic;   --Wenn '1' werden ausgelesenen FSP Daten nicht an den USI Transmitter ubergeben, sondern im ext. Speicher gesichert
   signal sFSPImgMakeImageFlashSavesData  : std_logic;   --Wenn '1' ist der Abbildprozess abgeschlossen

   signal sFSPImgLoadImageInProgress      : std_logic;   --Wenn '1' werden Daten nicht ueber USI, sondern aus dem ext. Speicher empfangen
   signal sFSPImgLoadImageAfterPowerUp    : std_logic;   --Wenn '1' wird nach dem Suchlauf das Image der FSPs aus dem ext. seriellen Flash geladen

   signal sFSPImgDataLoadedFromFlash      : std_logic_vector(7 downto 0);
   signal sFSPImgNewDataAvailable         : std_logic;

   type     tState_FSPImgLoad is (  WAIT_FOR_START_CONDITION,
                                    WAIT_FOR_FSP_IMG_BUSY_HIGH,
                                    WAIT_FOR_FSP_IMG_BUSY_LOW,
                                    FSP_IMG_READ_DATA_HIGH,
                                    FSP_IMG_READ_DATA_LOW
                                 );

   signal   sState_FSPImgLoad       : tState_FSPImgLoad;
   signal   sNextState_FSPImgLoad   : tState_FSPImgLoad;

   -------------------------------------------------------------------------
   -- Signale, Typen, Konstanten fuer FSP_SeekAndFind
   -------------------------------------------------------------------------

   --signal sRAMAddress               : integer range (gNumberOfFSPs+1) downto 0 := 0;
   signal sFSPSeekAddress                 : integer range 0 to (gNumberOfFSPs+1) := 0;
   signal sNumbersOfFoundFSPs             : integer range 0 to gNumberOfFSPs := 0;
   
   -- FIFO Startpunkt RAM
   signal sFIFOStartpointRAMData          : std_logic_vector(31 downto 0);      -- Ausgabe der Daten an das FIFO Startpunkt RAM
   signal sFIFOStartpointRAMWRAddr        : std_logic_vector( 7 downto 0);      -- Adresse des FIFO Startpunkt RAM
   signal sFIFOStartpointRAMWREnable      : std_logic;                          -- WR enable Signal des FIFO Startpunkt RAM

   -------------------------------------------------------------------
   -- Signale, Typen, Konstanten fuer Statemachine 'sState_ConvertA2H'
   -------------------------------------------------------------------

   type tState_ConvertA2H is (   WAIT_FOR_START_CONDITION,
                                 WAIT_FOR_A2H_CONV_DONE
                             );

   signal sState_ConvertA2H : tState_ConvertA2H;

   signal   sNewASCIIValue          : std_logic;                           -- Wenn '1', startet die Statemachine fuer den ASCII-Hex Dekoder
   signal   sASCIIOut               : std_logic_vector( 7 downto 0);
   signal   sStartConvertA2H        : std_logic;
   signal   sA2HConvDone            : std_logic;
   signal   sA2HError               : std_logic;
   signal   sNewHexValue            : std_logic;
   signal   sHexIn                  : std_logic_vector( 3 downto 0);

   -------------------------------------------------------------------
   -- Signale, Typen, Konstanten fuer Statemcachine 'sState_NibbleIs''
   -------------------------------------------------------------------

   type tState_NibbleIs is (  FSP_MSB,
                              FSP_LSB,
                              DATA_MSB,
                              DATA_LSB
                           );

   signal sState_NibbleIs : tState_NibbleIs;

   signal   sFSPAvailable        : std_logic;
   signal   sFSPReadOnly         : std_logic;
   
   -------------------------------------------------------------------
   -- Signale, Typen, Konstanten fuer Statemcachine 'sState_ReceivePID'
   -------------------------------------------------------------------

   type tState_ReceivePID is (   PID_MSB,
                                 PID_LSB
                              );

   signal sState_ReceivePID : tState_ReceivePID;

   -------------------------------------------------------------------
   -- Signale, Typen, Konstanten fuer Statemcachine 'sState_SendAnswer'
   -------------------------------------------------------------------

   type     tState_SendAnswer is  ( WAIT_FOR_START_CONDITION, 
                                    FSP_IMG_SEND_STX,
                                    SEND_MA,
                                    DECIDE,
                                    SEND_ACK,
                                    SEND_FSP_MSB,
                                    SEND_FSP_LSB,
                                    SEND_END_SYNC,
                                    SEND_NACK,
                                    SEND_ERR_MSB,
                                    SEND_ERR_LSB,
                                    SEND_CRC_MSB,
                                    SEND_CRC_LSB,
                                    COMP_REMAINING_BYTES,
                                    CLOCK_DATA_HIGH,
                                    CLOCK_DATA_LOW,
                                    READ_DATA,
                                    CONV_H2A,
                                    WAIT_FOR_H2A_CONV_DONE,
                                    SEND_DATA,
                                    WAIT_FOR_FSP_IMG_BUSY_HIGH,
                                    WAIT_FOR_FSP_IMG_BUSY_LOW,
                                    WAIT_FOR_DATA_FETCHED,
                                    SEND_PID_MSB,
                                    SEND_PID_LSB
                                  );

   signal   sState_SendAnswer       : tState_SendAnswer;
   signal   sState_SendAnswerNext   : tState_SendAnswer;

   signal   sHEXOut                 : std_logic_vector (3 downto 0);
   signal   sStartConvertH2A        : std_logic;
   signal   sH2AConvDone            : std_logic;  
   signal   sASCIIIn                : std_logic_vector (7 downto 0);
   signal   sNibbleCounter          : std_logic := '0';  

   signal   sSendByteASCII          : std_logic_vector (7 downto 0);
   signal   sTransmitterFree        : std_logic;
   signal   sNewDataTransferable    : std_logic := '0';

   -------------------------------------------------------------------
   -- Signale, Typen, Konstanten fuer alles Uebrige
   -------------------------------------------------------------------

   signal   sDataOut                : std_logic_vector(7 downto 0) := X"00";

begin

   --###########################################################################################################
   --#                                                                                                         #
   --# Funktion:                                                                                               #
   --# ---------                                                                                               #
   --# Testet die empfangenen Daten auf Ihre Konformitaet:                                                     #
   --# - ob ein StartSyncByte empfangen wird                                                                   #
   --# - ob ein gueltiges PID empfangen wird                                                                   #
   --# - ob diese ueberhaupt fuer dieses Modul bestimmt sind                                                   #
   --# - und eine Auswertung vorgenommen werden soll.                                                          #
   --#                                                                                                         #
   --# Daten ueber die USI empfangen und einem FSP zuordnen                                                    #
   --# ====================================================                                                    #
   --# Nach jedem empf. Byte wird der Port 'NewDataAvailable' = '1' und in 'ReceiveData' steht das neue        #
   --# Datenbyte als 8 Bit ASCII Wert.                                                                         #
   --# Jeder Empfang beginnt mit 'sSyncStartByte' (0x02, bzw. 0x05) und endet mit 'sSyncEndByte' (0x03, bzw.   #
   --# 0x04). Diese Synctrigger sind ASCII, werden auch mit ASCII verglichen und deshalb nicht in Hex          #
   --# gewandelt. Dem Startbyte folgt der PID (Packet Identifier), dieser ist 'WR' (0x5244) oder 'RD' (0x5752),#
   --# auch diese Infos werden nicht in Hex gewandelt. Dadurch wird 'sRWn' gesetzt. Nun folgt die Moduladresse #
   --# als 1 Byte ASCII. Ab hier werden alle Bytes in Hex gewandelt. MA wird mit 'gModulNumber' verglichen.    #
   --# Ist das Modul nicht gemeint, faellt die StateMachine in den 'WAIT_FOR_START_CONDITION' State und wartet #
   --# auf ein neues StartSync. Ist das Ergebnis stimmig, wird mit den nachfolgenden FSP Bytes (2 Stueck)      #
   --# 'sReceivedFSP' gefuellt. Ist 'sRWn' = '0' werden die nachfolgenden Daten nach deren Wandlung ueber      #
   --# 'sHEXOut' an den FSP ausgegeben und in diesen mit 'sClockData' hinein getaktet. Alle empf. Daten werden #
   --# dabei XOR verknuepft fuer den CRC Test. 'RemainingBytes' wird dabei vom FSP reduziert. Ist              #
   --# 'RemainingBytes' = 0 werden die naechsten 2 empf. ASCII Datenbytes als CRC gewertet und nach deren      #
   --# Wandlung zu einem Byte Hex mit der errechneten Pruefsumme verglichen. Wird darauf 'sSyncEndByte'        #
   --# gelesen und ist der CRC Test erfoglreich wird 'CRCOK' = '1'. Der FSP gibt die neuen Daten an die        #
   --# Peripherie aus.                                                                                         #
   --#                                                                                                         #
   --# Antworten auf das Schreiben von Daten in einen FSP                                                      #
   --# ==================================================                                                      #
   --# 'sSendAnswer' wird ebenfalls '1' und der Prozess zur Antwort generiert diese.                           #
   --#                                                                                                         #
   --# Daten ueber die USI verschicken                                                                         #
   --# ===============================                                                                         #
   --# Sollen Daten gelesen werden verlaeuft alles genauso, nur darf nach dem FSP nur noch ein 'SyncEndByte'   #
   --# empfangen werden. Daraufhin werden mit 'sClockData' die Daten aus dem FSP getaktet, nach ASCII gewandelt#
   --# und gesendet. Dabei wird ebenfalls ein CRC gebildet, welches sofern 'RemainingBytes' = X"000" ist       #
   --# an den Stream angehaengt und dieser mit 'SyncEndByte' abgeschlossen wird.                               #
   --#                                                                                                         #
   --# Im Fehlerfall                                                                                           #
   --# =============                                                                                           #
   --# Tritt ein Fehler auf, wird anstelle eines ACK oder der Daten ein NACK mit Fehlercode gesendet.          #
   --#                                                                                                         #
   --# V1.0 born (18.02.2010 - DS)                                                                             #
   --#                                                                                                         #
   --###########################################################################################################

   -- nachfolgender Prozess ist nicht getaktet

   pMulti_OR: process (FSPAvailable, FSPReadOnly)
   begin
      sFSPReadOnly         <= '0';
      sFSPAvailable        <= '0';

      for i in gNumberOfFSPs downto 0 loop
         if (FSPAvailable(i) = '1') then
            sFSPAvailable <= '1';
         end if;
         if (FSPReadOnly(i) = '1') then
            sFSPReadOnly <= '1';
         end if;
         
      end loop;
   end process pMulti_OR;

      -- der restliche Prozess ist getaktet
   pCheckDataConformity : process (Clock, Reset)
   begin

      if (Reset = '1') then 

         --# 'tState_CheckDataConformity'
         sState_CheckDataConformity    <= INITIAL_SEEK;                    -- FSP Suchlauf

         sUSIInHighSpeedMode           <= '0';                             -- Wenn '1' ist die USI im HighSpeed Modus
         sModuleNumber                 <= (others => '0');
         sNewDataAvailable             <= '0';                             -- Wenn '1' wurde ein neues ASCII Byte empfangen und kann eingelesen werden
         sReceivedData                 <= (others => '0');                 -- Hier steht das eigentliche empf. ASCII Byte

         sReceivedFSPAndDataMSBBuffer  <= (others => '0');                 -- Zwischenspeicher fuer das MSB von FSP und Daten
         sReceivedPID                  <= (others => '0');                 -- Das empf. PID (RD/WR), setzt 'sRWn'
         sReceivedMA                   <= (others => '0');                 -- Die empf. Modulnummer
         sReceivedFSP                  <= (others => '0');                 -- Der empf. FSP, adressiert den FSP
         sError                        <= (others => '0');                 -- Tritt ein Fehler auf ist dieser Wert /= 0 und wird an den Host gesendet
         sSendAnswer                   <= '0';                             -- Wenn'1' wird die Statemachine zum generieren der Antwort gestartet
         sRWn                          <= '1';                             -- Wird durch 'sReceivedPID' gesetzt, wenn '1' -> der Host will Daten lesen

         sSyncStartByte                <= X"02";                           -- Dieser Wert ist abhaengig von 'sUSIInHighSpeedMode', nach Reset ist USI zunaechst im normalen Modus
         sSyncEndByte                  <= X"03";                           -- Dieser Wert ist abhaengig von 'sUSIInHighSpeedMode', nach Reset ist USI zunaechst im normalen Modus

         --# 'FSPImg'

         FSPImgWriteEnable             <= '0';
         FSPImgWriteData               <= '0';

         FSPImgReadEnable              <= '0';
         FSPImgReadData                <= '0';

         sFSPImgMakeImage              <=  '0';
         sFSPImgLoadImage              <=  '0';

         sFSPImgMakeImagePrev          <= '0';
         sFSPImgLoadImagePrev          <= '0';

         sFSPImgMakeImageInProgress    <= '0';
         sFSPImgMakeImageFlashSavesData<= '0';
         
         sFSPImgLoadImageInProgress    <= '0';
         sFSPImgLoadImageAfterPowerUp  <= '0';

         sFSPImgDataLoadedFromFlash    <= (others => '0');
         sFSPImgNewDataAvailable       <= '0';

         --# 'sState_FSPImgLoad'
         sState_FSPImgLoad             <= WAIT_FOR_START_CONDITION;

         --# 'SeekAndFind'
         sFSPSeekAddress               <= 0;
         sNumbersOfFoundFSPs           <= 0;

          -- 'FIFO Startpunkt RAM'
         sFIFOStartpointRAMData        <= (others =>'0');
         sFIFOStartpointRAMWRAddr      <= (others =>'0');
         sFIFOStartpointRAMWREnable    <= '0';
   
         --# 'sState_NibbleIs'
         sState_NibbleIs               <= FSP_MSB;                         -- Der NibbleState innerhalb des States 'RECEIVE_FURTHER_DATA' von 'sState_CheckDataConformity'

         --# 'sState_ConvertA2H'
         sState_ConvertA2H             <= WAIT_FOR_START_CONDITION ;       -- Statemachine fuer die Kontrolle der Wandlung
         sNewASCIIValue                <= '0';                             -- Wenn '1', startet die Statemachine fuer den ASCII-Hex Dekoder
         sASCIIOut                     <= (others => '0');                 -- Muss der empf. ASCII gewandelt werde, so ist dies der Ausgang zum ASCII->Hex Dekoder
         sStartConvertA2H              <= '0';                             -- Wenn '1' -> Start der ASCII nach HEX Konvertierung (Pulse!)
         sA2HConvDone                  <= '0';                             -- Wenn '1' ist die Wandlung abgeschlossen, es steht ein neuer Hex-Wert an 'HexIn'
         sNewHexValue                  <= '0';                             -- Wenn '1' ist die Wandlung abgeschlossen, es steht ein neuer Hex-Wert an 'HexIn' der verarbeitet werden kann
         sHexIn                        <= (others => '0');                 -- Muss der empf. ASCII gewandelt werde, so ist dies der Eingang vom ASCII->Hex Dekoder
         sA2HError                     <= '0';                             -- Wenn '1' war der ASCII Wert zur Wandlung nicht gueltig

         --# 'sState_ReceivePID'
         sState_ReceivePID             <= PID_MSB;                         -- Unterstatemachine im State 'RECEIVE_PID' von 'sState_CheckDataConformity' zum PID Empfang

         --# 'sState_AssignDataToFSP'
         sClockData                    <= '0';                             -- Wenn '1' werden Daten am Ausgang 'Data' in den FSP uebernommen
         sRemainingBytes               <= (others =>'0');                  -- Anzahl noch freier Bytes im FSP
         sMemRemainingBytes            <= (others =>'0');                  -- dient dem Vergleich ob sich 'RemainingBYtes' geaendert hat
         sCRCOK                        <= '0';                             -- Wenn '1' war CRC erfolgreich, Daten an Peripherie ausgeben
         sCRCCompare                   <= (others =>'0');                  -- hier wird die CRC Pruefsumme der empf. Bytes gebildet
         sCRC                          <= (others =>'0');                  -- hier drin steht die empf. CRC Pruefsumme
         sCRCReceived                  <= '0';                             -- Wenn '1' wurde vermeindliche CRC Pruefsumme empfangen

         --# 'sState_SendAnswer'
         sState_SendAnswer       <= WAIT_FOR_START_CONDITION;              -- Statemachine fuer das Senden der Antwort an den Host
         sState_SendAnswerNext   <= WAIT_FOR_START_CONDITION;              -- Statemachine fuer das Senden der Antwort an den Host

         sHEXOut                 <= (others =>'0');                        -- Hexnibble Ausgang zum H2A Converter
         sStartConvertH2A        <= '0';                                   -- Wenn '1' -> Start der Hex nach ASCII Konvertierung (Pulse!)
         sH2AConvDone            <= '0';                                   -- Wenn '1' -> Hex nach ASCII Konverteirung abgeschlossen (Pulse!)
         sASCIIIn                <= (others =>'0');                        -- ASCII Eingang vom H2A Converter

         sNibbleCounter          <= '0';                                   -- Zaehler unteres/oberes Nibbel fuer den Wandelvorgang, wenn '1' -> unteres Nibble

         sSendByteASCII          <= (others =>'0');                        -- Das von HEX nach ASCII gewandelte Nibble zum senden an den Host
         sTransmitterFree        <= '0';                                   -- Wenn '1' kann ein neuer Wert an den Host uebertragen werden
         sNewDataTransferable    <= '0';                                   -- Wenn '1' kann Transmitter neues ASCII BYte an Host senden

         sEnableCRCPulse         <= '0';                                   -- Dies ermoeglicht den CalcCRCPulse beim Daten senden
         sCalcCRCPulse           <= '0';                                   -- Dies ist der eigentlich CalcCRCPulse 

         --Timeout
         sTimeoutCounter         <= 0;
         sTimeoutCounterRunning  <= '0';
         sTimeoutCounterReset    <= '0';

      else
         if (Clock'event and Clock = '1') then


            if(gUseGenericModuleNumber = '1') then
               sModuleNumber <= std_logic_vector(to_unsigned(gModuleNumber, 4));
            else
               sModuleNumber <= ModuleNumber;
            end if;

            if (sFSPImgLoadImageInProgress = '0') then                  --Daten werden via USI empfangen
               sReceivedData        <= ReceivedData;
               sNewDataAvailable    <= NewDataAvailable;
            else                                                        --Daten werden aus dem ext. Flash geladen
               sReceivedData        <= sFSPImgDataLoadedFromFlash;
               sNewDataAvailable    <= sFSPImgNewDataAvailable;
            end if;

            sUSIInHighSpeedMode  <= USIInHighSpeedMode;
            sHexIn               <= HexIn;
            sTransmitterFree     <= TransmitterFree;


            if (sFSPImgLoadImageInProgress = '0') then                  --Daten werden via USI empfangen
               if (sUSIInHighSpeedMode = '1') then                      -- USI-HiSpeed Modus
                  sSyncStartByte             <= X"05";
                  sSyncEndByte               <= X"04";
               else                                                     -- USI_Normalmodus
                  sSyncStartByte             <= X"02";
                  sSyncEndByte               <= X"03";
               end if;
            else                                                        --Daten werden aus dem ext. Flash geladen, in jedem Fall Standard-USI
               sSyncStartByte             <= X"02";
               sSyncEndByte               <= X"03";
            end if;

            --TimeoutCounter
            --Wird ein unvolstaendiger USI String empfangen, bricht der Empfang nach der TimeOut Zeit ab und es wird auf
            --ein neues STX gewartet

            if (sTimeoutCounterRunning = '1') then                         --Timer laeuft
               if(sTimeoutCounterReset = '0') then                         --es liegt kein Reset an
                  if(sTimeoutCounter < cTimeoutCounterMax) then            --Zaehler noch nicht abgelaufen
                     sTimeoutCounter <= sTimeoutCounter + 1;               --Zaehler erhoehen
                  else
                     sTimeoutCounter            <= sTimeoutCounter;        --Zaehlwert halten
                     sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;
                  end if;
               else                                                         --Zaehler Reset
                  sTimeoutCounter <= 0;
               end if;
            else                                                           --Timer laeuft NICHT
               if(sTimeoutCounterReset = '0') then
                  sTimeoutCounter <= sTimeoutCounter;                      --Zaehlwert halten
               else
                  sTimeoutCounter <= 0;                                    --Reset
               end if;
            end if;


         --#####################################################################################################
         --# Die nachfolgenden Statemachine pCheckDataConformity liest die empf. Daten ein und prueft diese    #
         --# auf Stimmigkeit, ggf. wird 'sError' gesetzt.                                                      #
         --# Abschliessend startet diese Statemachine die Statemachine 'pState_SendAnswer' zum senden der      #
         --# Antwort.                                                                                          #
         --#                                                                                                   #
         --# sRWn = '1':                                                                                       #
         --# -----------                                                                                       #
         --# STX PID PID MA FSP FSP ETX                                                                        #
         --#            \------------------- 'cERR_PIDError' (falsches PID aber noch MA testen vor NACK)       #
         --#                \--------------- stimmt MA wird 'cERR_PIDError' gesendet                           #
         --#                       \-------- 'cERR_FSPNotAvailable' (FSP nicht vorhanden)                      #
         --#                                                                                                   #
         --#####################################################################################################

            case (sState_CheckDataConformity) is

            --###########################################################################################################
            --# Seek and Find:                                                                                          #
            --# --------------                                                                                          #
            --# Nach dem Reset zaehlt die 'sState_CheckDataConformity' Statemachine alle moeglichen FSP Adressen        #
            --# (0..255) durch und gibt diese ueber 'ReceivedFSP' aus. Dabei wird jedes mal das 'Available' und         #
            --# 'ReadOnly' auf '1' getestet und wenn erfolgreich das 'RemainingBytes gelesen. Ziel ist es, die          #
            --# Gesamtanzahl alle angebundenen und vorhandenen FSPs zu ermitteln und Informationen ueber deren Groesse  #
            --# zu sammeln. Diese Informationen werden dann ueber die MDS hinaus kommuniziert.                          #
            --# Zurzeit werden die gesammelten Erkenntnisse in einem RAM Bereich abgelegt und koennen von dort ueber    #
            --# das MDS Modul eingelesen und ausgegeben werden. Sinnvoller waehre eine Pre Flow Geschichte, bei der     #
            --# vor dem Compilieren mitels Script die Parameterbloecke der FSP Instanzen ausgelesen und hardwired als   #
            --# VHDL Flie abgelegt werden.                                                                              #
            --#                                                                                                         #
            --# Der FSP Startpunkt Descriptor hat mindestens folgende Tiefe (Offset):                                   #
            --# 1 Byte 'bDescriptorType'  -> 0x2  (Hex), 0x32 (ASCII)                                                   #
            --# 2 Byte 'wOffset'          -> 0xyz (Hex), 0xyy 0xyy (ASCII)                                              #
            --# --> mindestens 3 Byte. Mit jedem gefundenen FSP erhoeht sich dessen Tiefe (Offset) um weitere 8 Byte,   #
            --# da jeder folgende Eintrag 32 Bit (Hex), 8 Byte (ASCII) breit ist.                                       #
            --###########################################################################################################

            -- RAM Mapping
            -- Address     Content
            -- 0x00        Gesamtanzahl der nachfolgenden Eintraege
            -- 0x01        erstes gefundenes FSP (NICHT die MDS, diese wartet bis Suchlauf beendet ist bevor sie sich melden kann)
            -- 0x02        zweites gefundenes FSP
            -- 0x03        drittes gefundenes FSP
            -- ...
            -- 0xnn        n-tes gefundenes FSP

               ----------------------------------------------------
               when INITIAL_SEEK =>

                  sFSPSeekAddress            <= 1;    -- dies ist der FSP Adressenzaehler (0..gNumberOfFSPs)
                  sNumbersOfFoundFSPs        <= 0;    -- summiert die Gesamtanzahl aller gefundenen FSPs auf (dient der spaeteren Offsetberechnung im Descriptor)
                  sState_CheckDataConformity <= SEEK;

               ----------------------------------------------------
               when SEEK   =>

                  sReceivedFSP               <= std_logic_vector(to_unsigned(sFSPSeekAddress, 8));  -- FSP Nummer ausgeben
                  sState_CheckDataConformity <= WAIT_STATE;

               ----------------------------------------------------
               when WAIT_STATE   =>                   -- mind. 4 Takte warten bis FSP Infos sicher anliegen

                  sState_CheckDataConformity <= WAIT_STATE_1;

               ----------------------------------------------------
               when WAIT_STATE_1   =>

                  sState_CheckDataConformity <= WAIT_STATE_2;

               ----------------------------------------------------
               when WAIT_STATE_2   =>

                  sState_CheckDataConformity <= WAIT_STATE_3;

               ----------------------------------------------------
               when WAIT_STATE_3   =>

                  sState_CheckDataConformity <= WAIT_STATE_4;

               ----------------------------------------------------
               when WAIT_STATE_4 =>

                  sState_CheckDataConformity <= READ_FSP_INFO;

               ----------------------------------------------------
               when READ_FSP_INFO =>               -- Wird 'sAvailable' = '1' gibt es dieses FSP -> auslesen und Daten speichern

                  if(sFSPImgMakeImageInProgress <= '0') then              --keine Imageerstellung in Betrieb, normaler Init-Suchlauf
                     if (sFSPAvailable = '1') then
                        sFIFOStartpointRAMWRAddr   <= std_logic_vector(to_unsigned(sNumbersOfFoundFSPs+1, 8));    -- RAM Adresse/Daten anlagen
                        sFIFOStartpointRAMData     <= X"0" & std_logic_vector(to_unsigned(sFSPSeekAddress,8)) & sRemainingBytes & "0000000" & sFSPReadOnly;
                        sState_CheckDataConformity <= SET_WR;                                                     -- speichern
                     else
                        sState_CheckDataConformity <= INCREMENT;
                     end if;
                   else
                     if(sFSPAvailable = '1') then                                --Der FSP ist vorhanden
                        sSendAnswer                <= '1';                       --Image des ausgewaehlten FSP ins Flash kopieren...
                        sState_CheckDataConformity <= WAIT_FOR_FSP_IMAGE_DONE;   --...und warten bis dieser Vorgang abgeschlossen ist
                     else                                                        --der FSP ist nicht vorhanden -> nichts tun
                        sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;
                     end if;
                  end if;
               ----------------------------------------------------
               when SET_WR =>

                  sNumbersOfFoundFSPs           <= sNumbersOfFoundFSPs + 1;   -- Anzahl gefundener FSPs erst hier erhoehen, damit Adresse stimmt
                  sFIFOStartpointRAMWREnable    <= '1';
                  sState_CheckDataConformity    <= DEL_WR;

               ----------------------------------------------------
               when DEL_WR =>

                  sFIFOStartpointRAMWREnable    <= '0';
                  sState_CheckDataConformity    <= INCREMENT;

               ----------------------------------------------------
               when INCREMENT =>

                  if (sFSPSeekAddress < gNumberOfFSPs) then            -- noch nicht alle moeglischen FSP abgeklappert
                     sFSPSeekAddress            <= sFSPSeekAddress + 1;
                     sState_CheckDataConformity <= SEEK;
                  else                                                  -- wenn alle FSPs durch, Gesamtzahl der gefundenen FSPs sichern
                     sFSPSeekAddress            <= sFSPSeekAddress;
                     sFIFOStartpointRAMWRAddr   <= (others => '0');
                     sFIFOStartpointRAMData     <= X"000000" & std_logic_vector(to_unsigned(sNumbersOfFoundFSPs, 8)); --Gesamtzahl der Eintraege sichern
                     sState_CheckDataConformity <= SET_WR_1;
                  end if;

               ----------------------------------------------------
               when SET_WR_1 =>

                  sFIFOStartpointRAMWREnable    <= '1';
                  sState_CheckDataConformity    <= DEL_WR_1;

               ----------------------------------------------------
               when DEL_WR_1 =>

                  sFIFOStartpointRAMWREnable    <= '0';
                  sState_CheckDataConformity    <= WAIT_STATE_5;

               -- Problem: sobald sReceived_FSP wieder X"0" wird, fuehlt sich die MDS adressiert und liest die RAM Adresse X"00" aus
               -- an der die Gesamtanzahl aller FSP Eintraege zu finden ist um fuer die Ausgabe dieser Daten bereit zu sein.
               -- Da aber im letzten Zyklus dieser Wert erst ins RAM eingetragen wird
               -- muss an dieser Stelle noch kurz gewartet werden, damit dieser Wert auch sicher dort steht.

               ----------------------------------------------------
               when WAIT_STATE_5 =>

                  sReceivedFSP                  <= (others => '0');
                  sState_CheckDataConformity    <= WAIT_STATE_6;


               ----------------------------------------------------
               when WAIT_STATE_6 =>

                  sFSPImgLoadImageAfterPowerUp  <= FSPImgLoadImageAfterPowerUp;
                  sState_CheckDataConformity    <= WAIT_FOR_START_CONDITION;

               --####### Ab hier ist dann der Bootlauf abgeschlossen #######--

               ----------------------------------------------------
               when WAIT_FOR_START_CONDITION =>                                           -- STX/ENQ Startbedingung empfangen (keine Wandlung)

                  sState_ConvertA2H          <= WAIT_FOR_START_CONDITION ;
                  sState_ReceivePID          <= PID_MSB;
                  sSendAnswer                <= '0';
                  sNewASCIIValue             <= '0';
                  sCRCOK                     <= '0';
                  sCalcCRCPulse              <= '0';
                  sTimeoutCounterRunning     <= '0';
                  sTimeoutCounterReset       <= '1';
                  FSPImgReadEnable           <= '0';
                  sFSPImgMakeImageInProgress <= '0';
                  sFSPImgLoadImageInProgress <= '0';

                  if (FSPImgMakeImage /= sFSPImgMakeImagePrev) then              --Aenderung erkannt
                     sFSPImgMakeImagePrev <= FSPImgMakeImage;                    --merken
                     if (FSPImgMakeImage = '1') then
                        sFSPImgMakeImage <= '1';
                     else
                        sFSPImgMakeImage <= '0';
                     end if;
                  end if;
                  
                  if (FSPImgLoadImage /= sFSPImgLoadImagePrev) then              --Aenderung erkannt
                     sFSPImgLoadImagePrev <= FSPImgLoadImage;                    --merken
                     if (FSPImgLoadImage = '1') then
                        sFSPImgLoadImage <= '1';
                     else
                        sFSPImgLoadImage <= '0';
                     end if;
                  end if;

                  --Liegen neue Daten an 'NewDataAvailable' hat die Zuweisung dieser Daten, bzw. die Abarbeitung dieses Kommandos
                  --Vorrang gegenueber 'FSPImg*Image'.

                  --Werden waehrend dem Schreiben oder Lesen eines FSPImg Daten ueber USI empfangen, muessen diese in einem ausreichend grossen
                  --USIRx Buffer gehalten werden, bis der ICH diese abarbeiten kann.

                  --Wurden Daten ueber USI empfangen waehrend der ICH FSPImg bearbeitet und ist der ICH noch mit dem Senden
                  --dieser Daten beschäftigt (sState_SendAnswer /= WAIT_FOR_START_CONDITION) darf dieser Prozess nicht auf
                  --'FSPImgMake*/Load*' reagieren, da dies ebenfalls den 'sState_SendAnswer'-Prozess benoetigt

                  if (sNewDataAvailable = '1') AND (sReceivedData = sSyncStartByte) then        --Daten ueber USI empfangen

                     -- Problem: wurde FSP0 (MDS) adressiert und soll erneut ausgelesen werden, muss zwischendurch ein anderes
                     -- FSP adressiert werden um durch die Deselektion des MDS-FSP dessen Ausgang 'RemainingBytes' wieder auf
                     -- einen gueltigen Wert /= 0 zu setzen. Daher diese Behelfsloesung, die jedes mal wenn 'sReceivedFSP = '0''
                     -- war, also i.d.R. die MDS ausgelesen wurde (aber auch nach dem Reset) das FSP 255 addressiert.

                     if (sReceivedFSP = X"00") then
                        sReceivedFSP               <= (others => '1');
                     else
                        sReceivedFSP               <= (others => '0');                    -- wenn X"00" wird FSP deselektiert
                     end if;

                     sState_CheckDataConformity    <= RECEIVE_STX;

                  else
                     if ((FSPImgReady = '1') AND (sState_SendAnswer = WAIT_FOR_START_CONDITION))  then   --FSPImgReady = '1', nur dann Zugriffe auf M25P erlaubt

                        if (sFSPImgMakeImage = '1') AND (sFSPImgLoadImage = '0') then       --FSP MakeImage ausfuehren

                           --sFSPImgMakeImage           <= '0';
                           sState_CheckDataConformity <= FSP_IMG_START_MAKE;

                        elsif ( (sFSPImgLoadImageAfterPowerUp = '1') OR ((sFSPImgMakeImage = '0') AND (sFSPImgLoadImage = '1') )) then  --FSP LoadImage ausfuehren

                           --sFSPImgLoadImage              <= '0';
                           --sFSPImgLoadImageAfterPowerUp  <= '0';
                           sState_CheckDataConformity    <= FSP_IMG_START_LOAD;
                        else
                           sState_CheckDataConformity    <= WAIT_FOR_START_CONDITION;
                        end if;
                     else
                        sState_CheckDataConformity    <= WAIT_FOR_START_CONDITION;
                     end if;
                  end if;

                  ----------------------------------------------------
                  when FSP_IMG_START_MAKE =>

                    if (sFSPImgMakeImage = '1') then
                        if (sState_SendAnswer = WAIT_FOR_START_CONDITION) then

                           if (sReceivedFSP = X"00") then
                              sReceivedFSP               <= (others => '1');
                           else
                              sReceivedFSP               <= (others => '0');        -- wenn X"00" wird FSP deselektiert
                           end if;

                           sFSPImgMakeImage           <= '0';
                           sFSPImgMakeImageInProgress <= '1';              --Imageerstellung in Betrieb
                           sFSPSeekAddress            <= to_integer(unsigned(FSPImgFSPNumber));  --Von diesem FSP eine Sicherung anlegen (hex.)
                           sRWn                       <= '1';              --Read Data

                           sState_CheckDataConformity <= SEEK;             --im naechsten Takt wird 'ReceivedFSP' zugewiesen und im uebernaechsten Takt werden
                                                                        --'ReceivedFSP' und 'RWn' gesetzt, das FSP benoetigt aber mind. noch
                                                                        --einen weiteren Takt zur Ausgabe von 'Available'. bzw. 'RemainingBytes'
                                                                        --Nur wenn dieses Daten gueltig sind (vorallem 'Available') duerfen weitere
                                                                        --Zugriffe auf den FSP erfolgen.
                                                                        --Da der Init-Suchlauf schon Wait_States nutz, werden diese auch hier nochmals
                                                                        --verwendet. Antwortet der FSP mit 'Available', wird vom Suchlauf 'SendAnswer' = '1' gesetzt,
                                                                        --in 'WAIT_FOR_FSP_IMAGE_DONE' gesprungen und dort auf das Ende der Datenschreiberei ins
                                                                        --Flash gewartet
                        else
                           sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;
                        end if;
                     else
                        sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;
                     end if;

                  ----------------------------------------------------
                  when FSP_IMG_START_LOAD =>

                     if (sFSPImgLoadImage = '1') OR (sFSPImgLoadImageAfterPowerUp  = '1') then
                        if (sState_SendAnswer = WAIT_FOR_START_CONDITION) then

                           if (sReceivedFSP = X"00") then
                              sReceivedFSP               <= (others => '1');
                           else
                              sReceivedFSP               <= (others => '0');        -- wenn X"00" wird FSP deselektiert
                           end if;

                           sFSPImgLoadImageAfterPowerUp  <= '0';
                           sFSPImgLoadImage              <= '0';
                           FSPImgReadEnable              <= '1';
                           sFSPImgLoadImageAfterPowerUp  <= '0';
                           sFSPImgLoadImageInProgress    <= '1';
                           sState_CheckDataConformity    <= FSP_IMG_CONFIRM_STX;
                        else
                           sState_CheckDataConformity    <= FSP_IMG_START_LOAD;
                        end if;
                     else
                        sState_CheckDataConformity    <= WAIT_FOR_START_CONDITION;
                     end if;

                  ----------------------------------------------------
                  when FSP_IMG_CONFIRM_STX =>                  --der erste Wert aus dem Flash muss ein STX sein, sonst garnichts machen

                     sState_ConvertA2H          <= WAIT_FOR_START_CONDITION ;
                     sState_ReceivePID          <= PID_MSB;
                     sSendAnswer                <= '0';
                     sNewASCIIValue             <= '0';
                     sCRCOK                     <= '0';
                     sCalcCRCPulse              <= '0';
                     sTimeoutCounterRunning     <= '0';
                     sTimeoutCounterReset       <= '1';

                     if (sFSPImgLoadImageInProgress = '1') then

                        if (sNewDataAvailable = '1') then --and (sReceivedData = sSyncStartByte) then
--                           sState_CheckDataConformity <= RECEIVE_STX;
--                        else
                           if(sReceivedData = sSyncStartByte) then
                              sState_CheckDataConformity <= RECEIVE_STX;
                           else
                              sState_CheckDataConformity <= WAIT_FOR_START_CONDITION; --FSP_IMG_CONFIRM_STX;
                           end if;
                        else
                           sState_CheckDataConformity <= FSP_IMG_CONFIRM_STX;
                        end if;
                     else
                        sState_CheckDataConformity    <= WAIT_FOR_START_CONDITION;
                     end if;

                  ----------------------------------------------------
                  when RECEIVE_STX =>

                     sTimeoutCounterRunning        <= '1';
                     sTimeoutCounterReset          <= '0';

                     sError                        <= (others => '0');
                     sReceivedMA                   <= (others => '0');

                     sRemainingBytes               <= (others => '0');
                     sMemRemainingBytes            <= (others => '0');
                     sReceivedFSPAndDataMSBBuffer  <= (others => '0');
                     sCRCCompare                   <= (others => '0');                    -- gebildete Pruefsumme aus Daten
                     sCRC                          <= (others => '0');                    -- empf. CRC Pruefsumme
                     sCRCReceived                  <= '0';                                -- Wenn '1', vermeindliche Pruefsumme empf. -> Vergleich durchfuehrbar
                     sCalcCRCPulse                 <= '0';
                     sState_ReceivePID             <= PID_MSB;
                     sState_CheckDataConformity    <= RECEIVE_PID;

               ----------------------------------------------------
               when RECEIVE_PID =>                                                        -- PID empfangen (RD/WR) (keine Wandlung)

                  if (sNewDataAvailable = '1') then                                       -- neue Daten empfangen

                     if (sReceivedData /= sSyncEndByte) then                              -- kein ETX/EOT erkannt

                        case (sState_ReceivePID) is

                           ----------------------------------------------------
                           when PID_MSB =>                                                -- PID MSB
                              sReceivedPID(15 downto 8)  <= sReceivedData;
                              sState_ReceivePID          <= PID_LSB;
                              sState_CheckDataConformity <= RECEIVE_PID;

                           ----------------------------------------------------
                           when PID_LSB =>                                                -- PID LSB
                              sReceivedPID(7 downto 0)   <= sReceivedData;
                              sState_ReceivePID          <= PID_MSB;
                              sState_CheckDataConformity <= CHECK_PID_CONFORMITY;         -- auf Konformität testen

                           ----------------------------------------------------
                           when others =>
                              sState_ReceivePID          <= PID_MSB;
                              sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;     -- Auf neue Startbedingung warten

                        end case;

                     else
                        sState_CheckDataConformity    <= WAIT_FOR_START_CONDITION;        -- Auf neue Startbedingung warten
                     end if;
                  end if;

               ----------------------------------------------------
               when CHECK_PID_CONFORMITY =>

                  case (sReceivedPID) is

                     ----------------------------------------------------
                     when X"5244" =>                                                      -- PID "RD"
                        sRWn                          <= '1';
                        sState_CheckDataConformity    <= RECEIVE_MA;        

                     ----------------------------------------------------
                     when X"5752" =>                                                      -- PID "WR"
                        sRWn                          <= '0';
                        sState_CheckDataConformity    <= RECEIVE_MA;

                     ----------------------------------------------------
                     when others => 
                        sError                        <= cERR_PIDError;                   -- PID Error Code (Es wurde ein unbekanntes PID empfangen) 
                        sState_CheckDataConformity    <= RECEIVE_MA;                      -- trotzdem noch testen ob dieses Modul gemeint war

                  end case;

               ----------------------------------------------------
               when RECEIVE_MA =>                                                         -- jetzt kommt Moduladresse (ModulNummer)

                  sNewASCIIValue       <= '0';

                  if (sNewDataAvailable = '1') then                                       -- neue Daten empfangen

                     if (sReceivedData /= sSyncEndByte) then                              -- kein ETX/EOT erkannt
                        sASCIIOut                  <= sReceivedData;                      -- von ASCII nach Hex wandeln (MA ist ein Byte ASCII -> 4 Bit Hex)
                        sNewASCIIValue             <= '1';                                -- Startet die Wandlung (nur 1 Takt Puls!)
                        sState_CheckDataConformity <= CONFIRM_MA;                         -- Modulnummer kontrollieren
                     else
                        sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;        
                     end if;
                  end if;

               ----------------------------------------------------
               when CONFIRM_MA =>                                             

                  sNewASCIIValue             <= '0';                                      -- wieder auf low (Wandlung wird mit nur einem Puls gestartet!)
                  
                  if (sNewHexValue = '1') then                                            -- gewandelte MA vergleichen
                     --if (sHexIn = std_logic_vector(to_unsigned(gModuleNumber, 4)) )then   -- Modul ist gemeint, weitere Daten dekodieren
                     if (sHexIn = sModuleNumber) then                                     -- Modul ist gemeint, weitere Daten dekodieren
                        sReceivedMA                <= sHexIn;
                        sState_NibbleIs            <= FSP_MSB;
                        sState_CheckDataConformity <= RECEIVE_FURTHER_DATA;
                     else
                        if (sError /= X"00") then                                         -- Fehler bei PID
                           sState_CheckDataConformity    <= SEND_ANSWER;                  -- NACK senden
                        else
                           sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;        -- Modul nicht gemeint
                        end if;
                     end if;
                  end if;


               -- Als weitere Daten kommen zumindest noch zwei ASCII-Byte FSP (bei Leseanforderung), andernfalls folgen zusaetzlich noch
               -- die Daten und die Pruefsumme.
               -- Ist 'sNewDataAvailable = '1'' wird auf 'sSyncEndByte' geprueft. Wenn dieses erkannt wird, wird getestet ob 'sRWn = '1''
               -- und 'snFSPAvailbale /= '0''. d.h es soll ein FSP gelesen werden, dieser ist aber nicht vorhanden -> Fehler senden.
               -- Ist 'snFSPAvailbale = '0'' -> FSP Inhalt senden.
               -- Ist 'sRWn = '0'' sollen Daten in den FSP geschrieben werden. Ist dies bisher noch nicht geschehen, ist 'sCRCReceived = '0''
               -- und es wird ein Fehler gesendet.

               ----------------------------------------------------
               when RECEIVE_FURTHER_DATA =>

                  sNewASCIIValue             <= '0';                                      -- wieder auf low (Wandlung wird mit nur einem Puls gestartet!)
                  sCalcCRCPulse              <= '0';                                      -- CRC Bildung beenden wenn in DATA_MSB/DATA_LSB gestartet

                  if (sNewDataAvailable = '1') then                                       -- neue Daten empfangen

                     if (sReceivedData /= sSyncEndByte) then                              -- kein ETX/EOT erkannt...
                        if (sCRCReceived = '1') then                                      -- ...aber vermeindliche Pruefsumme bereits empfangen
                           sError                        <= cERR_CRCError;
                           sState_CheckDataConformity    <= SEND_ANSWER;                  -- NACK senden
                        else
                           sASCIIOut                     <= sReceivedData;                -- von ASCII nach Hex wandeln (MA ist ein Byte ASCII -> 4 Bit Hex)
                           sNewASCIIValue                <= '1';
                           sState_CheckDataConformity    <= RECEIVE_FURTHER_DATA;
                        end if;
                     else                                                                 -- ETX empfangen...
                        if (sRWn = '1') then                                              -- ...und es sollen Daten gelesen werden...
                           if (sFSPAvailable = '0') then                                  -- ...aber FSP nicht verfuegbar
                              sError                     <= cERR_FSPNotAvailable;         -- ...Fehler generieren.
                           end if;
                        else                                                              -- ...es sollen Daten geschrieben werden...
                           if (sCRCReceived = '0') then                                   -- ...aber es wurde noch keine Pruefsumme empfangen...
                              sError                     <= cERR_DataLengthError;         -- ...Fehler generieren.
                           end if;
                        end if;

                        sState_CheckDataConformity       <= SEND_ANSWER;                  -- NACK senden

                     end if;
                  end if;

                  if (sNewHexValue = '1') then                                            -- ASCII Wert wurde in Hex gewandelt

                     if (sError /= X"00") then                                            -- Wert war kein ASCII Wert
                        sState_CheckDataConformity    <= SEND_ANSWER;                     -- NACK senden

                     else
                        case (sState_NibbleIs) is

                           ----------------------------------------------------
                           when FSP_MSB =>

                              sReceivedFSPAndDataMSBBuffer  <=sHexIn;                     --FSP MSB in Zwischenregister
                              sState_NibbleIs               <= FSP_LSB;

                           ----------------------------------------------------
                           when FSP_LSB =>

                              sReceivedFSP(7 downto 0)   <= sReceivedFSPAndDataMSBBuffer & sHexIn; -- FSP MSB und LSB zusammensetzen und ausgeben
                              sState_NibbleIs            <= DATA_MSB;

                           ----------------------------------------------------
                           when DATA_MSB =>

                              sClockData     <= '0';

                              if (sFSPAvailable = '0') then
                                 sError                        <= cERR_FSPNotAvailable;
                                 sState_CheckDataConformity    <= SEND_ANSWER;               -- NACK senden
                              else

                                 if (sRWn = '0') then                                        -- nur bei sRWn = '0' sind nun Daten zulaessig
                                    if (sFSPReadOnly = '1') then                             -- FSP ist nur lesbar
                                       sError <= cERR_FSPReadOnly;                           -- keine Schreibanforderung fuer dieses FSP erlaubt
                                    else
                                       if (sRemainingBytes /= X"000") then                   -- noch Platz im FSP -> Daten
                                          sReceivedFSPAndDataMSBBuffer  <= sHexIn;           -- Daten MSB in Zwischenregister
                                          sCalcCRCPulse                 <= '1';              -- CRC bilden da Daten empfangen
                                       else
                                          sCRC(7 downto 4)              <= sHexIn;           -- andernfalls CRC
                                       end if;  
                                    end if;
                                 else
                                    sError <= cERR_NoDataPermitted;                          -- keine Daten erlaubt bei Leseanforderung
                                 end if;
                              end if;

                              sState_NibbleIs            <= DATA_LSB;

                           ----------------------------------------------------
                           when DATA_LSB =>

                              if (sRemainingBytes /= X"000") then                            -- noch Platz im FSP -> Daten
                                 sDataOut(7 downto 0)       <= sReceivedFSPAndDataMSBBuffer & sHexIn;-- Daten MSB und LSB zusammensetzen und ausgeben
                                 sCalcCRCPulse              <= '1';                          -- CRC bilden da Daten empfangen
                                 sClockData                 <= '1';                          -- Daten in FSP takten, Pulsgesteuert, nur 1 Takt!
                              else
                                 sCRC(3 downto 0)           <= sHexIn;                       -- andernfalls CRC
                                 sCRCReceived               <= '1';                          -- CRC empfangen
                              end if;

                              sState_NibbleIs            <= DATA_MSB;

                           ----------------------------------------------------
                           when others =>

                              sState_NibbleIs            <= FSP_MSB;

                        end case;   --case (sState_NibbleIs) is

                     end if;  --if (sError /= X"00") then

                  else
                     sClockData     <= '0';

                  end if;  --if (sNewHexValue = '1') then 

               ----------------------------------------------------
               when SEND_ANSWER =>                                                        -- Startet das Senden der Antwort

                  -- sRWn = '0'
                  -- wird beim schreiben von Daten in ein FSP ein 'SyncEndByte' (0x03, bzw. 0x04) empfangen und 'RemainingBytes' ist
                  -- noch nicht 0, d.h. also noch Platz im FSP oder wird beim schreiben von Daten in ein FSP kein 'SyncEndByte' empfangen 
                  -- und 'RemainingBytes' ist schon 0, wird ein 'Datenlaengenfehler' erzeugt
                  -- Wird hingegen beim schreiben von Daten in ein FSP ein 'SyncEndByte' empfangen und 'RemainingBytes' =  0,
                  -- wird 'CRCOK' = '1' und ein Acknowledge erzeugt.
                  -- soll ein FSP geschrieben werden, dass nur gelesen werden kann wird 'sError' gesetzt
                  -- sRWn = '1'
                  -- sollen Daten gelesen werden duerfen keine Daten vom Host gesendet werden, sonst wird 'sError' gesetzt

                  if (sRWn = '0') then
                     if ( (sReceivedData  = sSyncEndByte) AND (sRemainingBytes /= X"000") ) OR ( (sReceivedData /= sSyncEndByte) AND (sRemainingBytes = X"000") ) then
                        sError         <= cERR_DataLengthError;                           -- Fehler bei Datenlaenge
                     elsif ( (sReceivedData = sSyncEndByte) AND (sRemainingBytes = X"000") ) then

                        if (sCRCReceived = '1') AND (sCRCCompare = sCRC) then             -- wurden alle Daten empf. und CRC ebenfalls -> Vergleichen
                           sCRCOK         <= '1';                                         -- CRCOK fuer Datenuebergabe                 
                        elsif (sCRCReceived = '1') AND (sCRCCompare /= sCRC) then         -- alle Daten empfangen, CRC ebenfalls aber CRC Test schlaegt fehl
                           sCRCOK         <= '0';
                           sError         <= cERR_CRCError;                               -- CRC Fehler -> NACK senden
                        else                                                              -- kein CRC empf. aber alle Daten
                           sCRCOK         <= '0';
                           sError         <= cERR_DataLengthError;                        -- Datenlaengenfehler -> NACK senden
                        end if;

                     end if;
                  end if;

                  if (sFSPImgLoadImageInProgress = '0') then                              --Normale USI Kommunikation
                     sSendAnswer                <= '1';                                   --Antwort senden
                     sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;
                  else                                                                    --Beim Laden der Daten aus dem Flash -> kein Antwort

                     if (FSPImgReadDone = '1') then                                       --alle Daten aus dem ext. Flash gelesen
                        sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;
                     else                                                                 --noch Daten im ext. Flash zum lesen
                        sFSPImgLoadImage           <= '1';
                        sState_CheckDataConformity <= FSP_IMG_START_LOAD;                 --auf neuen Start vom Flash warten
                     end if;
                  end if;

               ----------------------------------------------------
               when WAIT_FOR_FSP_IMAGE_DONE =>

                  sSendAnswer                <= '0';

                  if (sFSPImgMakeImageInProgress = '0') then --AND (FSPImgBusy = '0') then 
                     sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;
                  end if;

               ----------------------------------------------------
               when others =>

                  sState_CheckDataConformity <= WAIT_FOR_START_CONDITION;

            end case;   --case (sState_CheckDataConformity) is

            --###########################################################################################################
            --#                                                                                                         #
            --# Funktion:                                                                                               #
            --# ---------                                                                                               #
            --# Dieses Statemachine liest Daten aus dem ext. Flashspeicher und fuettert dieses Modul damit.             #
            --#                                                                                                         #
            --# V1.0 born (21.11.2012 - DS)                                                                             #
            --#                                                                                                         #
            --###########################################################################################################

            case (sState_FSPImgLoad) is

               ----------------------------------------------------
               when WAIT_FOR_START_CONDITION =>

                  sFSPImgNewDataAvailable    <= '0';

                  if (sFSPImgLoadImageInProgress = '1') AND (FSPImgReadDone = '0') then   --Daten aus dem ext. Flash auslesen
                     sFSPImgDataLoadedFromFlash <= (others => '0');
                     sState_FSPImgLoad          <= WAIT_FOR_FSP_IMG_BUSY_HIGH;
                     sNextState_FSPImgLoad      <= FSP_IMG_READ_DATA_HIGH;
                  end if;

              ----------------------------------------------------
               when WAIT_FOR_FSP_IMG_BUSY_HIGH =>

                  FSPImgReadData             <= '0';

                  if(FSPImgBusy = '1') then
                     sState_FSPImgLoad <= WAIT_FOR_FSP_IMG_BUSY_LOW;
                  end if;

               ----------------------------------------------------
               when WAIT_FOR_FSP_IMG_BUSY_LOW =>

                  if(FSPImgBusy = '0') then
                     sState_FSPImgLoad <= sNextState_FSPImgLoad;
                  end if;

               ----------------------------------------------------
               when FSP_IMG_READ_DATA_HIGH =>

                  sFSPImgNewDataAvailable <= '0';

                  FSPImgReadData          <= '1';
                  sState_FSPImgLoad       <= WAIT_FOR_FSP_IMG_BUSY_HIGH;
                  sNextState_FSPImgLoad   <= FSP_IMG_READ_DATA_LOW;

               ----------------------------------------------------
               when FSP_IMG_READ_DATA_LOW =>

                  sFSPImgDataLoadedFromFlash <= FSPImgDataIn;              --Daten in Empfangsbuffer
                  sFSPImgNewDataAvailable    <= '1';                       --Signalisieren, dass neue Daten da sind

                  if (FSPImgReadDone = '0') then                              --es gibt noch Daten
                     sState_FSPImgLoad          <= FSP_IMG_READ_DATA_HIGH;    --weitere Daten lesen
                  else
                     sState_FSPImgLoad          <= WAIT_FOR_START_CONDITION;
                  end if;

               ----------------------------------------------------
               when others =>

                  sState_FSPImgLoad <= WAIT_FOR_START_CONDITION;

            end case;

 

            --###########################################################################################################
            --# Pruefsumme:                                                                                             #
            --# -----------                                                                                             #
            --# Nachfolgendes bildet die Pruefsumme.                                                                    #
            --# Die Pruefsumme wird dabei nur von den eigentlichen Nutzdaten gebildet, d.h. STX, PID, MA, FSP und EXT   #
            --# fliessen nicht in die Pruefsumme ein.                                                                   #
            --# Die Pruefsummen werden mit den ASCII Werten gebildet, da z.B. FSP0 (der MDS) auch ASCII Zeichen direkt  #
            --# aus gibt.
            --###########################################################################################################

            if (sCalcCRCPulse = '1') then
               if (sRWn ='0') then                                -- werden Daten empf. und diese in den FSP getaktet, damit Pruefsumme bilden
                  sCRCCompare <= sCRCCompare XOR sReceivedData;   -- Pruefsumme aus empf. ASCII Datenbytes
               else                                               -- werden Daten gesendet und diese aus dem FSP getaktet, damit Pruefsumme bilden
                  sCRCCompare <= sCRCCompare XOR sSendByteASCII;  -- Pruefsumme aus zu sendenden ASCII Datenbytes
               end if;
            end if;

            --###########################################################################################################
            --#                                                                                                         #
            --# Funktion:                                                                                               #
            --# ---------                                                                                               #
            --# Diese Statemachine fuehrt die Wandlung von ASCII nach Hexadezimal durch.                                #
            --# dabei wird der 8 Bit ASCII Wert an 'sASCIIOut' gewandelt und steht als 4 Bit Hexwert an 'sHexIn' an     #
            --#                                                                                                         #
            --# V1.0 born (19.02.2010 - DS)                                                                             #
            --#                                                                                                         #
            --###########################################################################################################

            sA2HConvDone   <= A2HConvDone;                                    -- einlesen
            sA2HError      <= A2HError;

            case (sState_ConvertA2H) is

               ----------------------------------------------------
               when WAIT_FOR_START_CONDITION =>

                  sNewHexValue      <= '0';
                  sStartConvertA2H  <= '0';

                  if(sNewASCIIValue = '1') then                               -- neuen Wert zum Wandeln in 'sASCII_Out'
                     sStartConvertA2H     <= '1';                             -- StartConvertA2H auf high legen (1 Takt Puls!)
                     sState_ConvertA2H    <= WAIT_FOR_A2H_CONV_DONE;
                  end if;

               ----------------------------------------------------
               when WAIT_FOR_A2H_CONV_DONE =>                                 -- warten bis Wandlung abgeschlossen ist

                  sStartConvertA2H     <= '0';                                -- StartConvertA2H auf low legen

                  if (sA2HConvDone = '1') then                                -- Fehler beim Wandeln
                     if(sA2HError = '1') then
                        sError            <= cERR_A2HConversion;              
                     else
                        sHexIn            <= HexIn;                           -- gewandelten 4 Bit Wert lesen                    
                     end if;

                     sNewHexValue         <= '1';                             -- Vorhandensein eines neuen Hex Wertes bekannt machen
                     sState_ConvertA2H    <= WAIT_FOR_START_CONDITION;

                  end if;

               ----------------------------------------------------
               when others =>

                  sState_ConvertA2H <= WAIT_FOR_START_CONDITION;

            end case;   --case (sState_ConvertA2H) is


         --###########################################################################################################
         --# Die nachfolgende Statemachine sendet die Antwort an den Host. Bei einem Schreibvorgang -> Acknowledge,  #
         --# bei einem Lesevorgang die gewuenschten Daten.                                                           #
         --# Es sind drei Arten von Antworten moeglich:                                                              #
         --# Acknowledge   : STX MA ACK FSP FSP ETX                                                                  #
         --# No Acknwoledge: STX MA NACK ERR ERR ETX                                                                 #
         --# Daten         : STX MA FSP FSP --Daten-- CRC CRC ETX                                                    #
         --#                                                                                                         #
         --# Bekanntes Problem: Ausgabe des FSP LSB, bzw. ERR LSB dauert nur einen Takt, dann folgt ETX/EOT,         #
         --# ggf. Abhilfe noetig durch SEND_DATA Verlaengerung um einen Takt (18.2.10 - DS)                          #
         --#                                                                                                         #
         --###########################################################################################################

            sASCIIIn          <= ASCIIIn;
            sH2AConvDone      <= H2AConvDone;
            sRemainingBytes   <= RemainingBytes;

            case (sState_SendAnswer) is

               ----------------------------------------------------
               when WAIT_FOR_START_CONDITION =>                            -- Abhaenging von 'sUSIInHighSpeed' das StartSyncByte senden

                  sNewDataTransferable    <= '0';                          -- Wenn '1' teilt dies dem Transmitter neue Daten mit
                  sMemRemainingBytes      <= sRemainingBytes;              -- RemainingBytes einlesen fuer spaeteren Wartevergelich

                  if (sSendAnswer = '1') then                              -- Startrigger zum senden der Antwort
                     if (sFSPImgMakeImageInProgress = '0') then            -- Normale Antwort ueber USI versenden
                        sSendByteASCII          <= sSyncStartByte;         -- Byte ist ASCII, keine Wandlung noetig
                        sCRCCompare             <= (others =>'0');         -- CRC Summe zunaechst loeschen
                        sState_SendAnswer       <= SEND_DATA;              -- Byte senden
                        sState_SendAnswerNext   <= SEND_MA;                -- danach die Moduladresse senden
                     else                                                  -- Reihenfolge STX PID PID MA FSP FSP und dann -Data- PP PP ETX, alle Daten in ASCII, STX immer 0x02, ETX immer 0x03
                        if(sFSPAvailable = '1') then                                      -- Es gibt das FSP
                           sFSPImgMakeImageFlashSavesData   <= '0';                       -- Imageerstellung  noch nicht abgeschlossen
                           sSendByteASCII                   <= X"02";                     -- Byte ist ASCII, keine Wandlung noetig
                           sCRCCompare                      <= (others =>'0');            -- CRC Summe zunaechst loeschen
                           FSPImgWriteEnable                <= '1';
                           sState_SendAnswer                <= WAIT_FOR_FSP_IMG_BUSY_HIGH;-- Schreiben des Flash aktivieren
                           sState_SendAnswerNext            <= FSP_IMG_SEND_STX;          -- danach STX senden
                        else
                           sFSPImgMakeImageInProgress <= '0';              --FSP nicht vorhanden -> Imagebildung abgeschlossen da nicht moeglich
                        end if;
                     end if;
                  end if;

               ----------------------------------------------------
               when FSP_IMG_SEND_STX =>

                  sState_SendAnswer       <= SEND_DATA;           -- Byte senden
                  sState_SendAnswerNext   <= SEND_PID_MSB;        -- danach die PID MSB senden

               ----------------------------------------------------
               when SEND_MA =>

                  --sHexOut                 <= std_logic_vector(to_unsigned(gModuleNumber, 4));  -- MA Byte ist HEX, Wandlung noetig
                  sHexOut                 <= sModuleNumber;
                  sState_SendAnswer       <= CONV_H2A;                     -- Byte wandeln und senden
                  sState_SendAnswerNext   <= DECIDE;                       -- danach entscheiden was noch kommt

               ----------------------------------------------------
               when DECIDE =>

                  if (sRWn = '0') then                                     -- Daten in FSP geschrieben...
                     if (sError = X"00") then                              -- ...und kein Fehler
                        sState_SendAnswer       <= SEND_ACK;
                     else                                                  -- ...Fehler
                        sState_SendAnswer       <= SEND_NACK;
                     end if;
                  else                                                     -- Daten sollen aus FSP gelesen werden...
                     if (sError = X"00") then                              -- ...Anfrage verstanden
                        sState_SendAnswer       <= SEND_FSP_MSB;           -- ...erst FSP, dann Daten senden
                     else                                                  -- ...Anfrage nicht verstanden
                        sState_SendAnswer       <= SEND_NACK;              -- ...NACK senden
                     end if;
                  end if;

               ----------------------------------------------------
               when SEND_ACK =>

                  sSendByteASCII          <= X"06";                        -- Byte ist ASCII, keine Wandlung noetig
                  sState_SendAnswer       <= SEND_DATA;                    -- Byte senden
                  sState_SendAnswerNext   <= SEND_FSP_MSB;                 -- danach FSP MSB senden

               ----------------------------------------------------
               when SEND_FSP_MSB =>

                  sHexOut                 <= sReceivedFSP(7 downto 4);     -- Nibble ist HEX, Wandlung noetig
                  sState_SendAnswer       <= CONV_H2A;                     -- Byte wandeln und senden
                  sState_SendAnswerNext   <= SEND_FSP_LSB;                 -- danach FSP LSB senden

               ----------------------------------------------------
               when SEND_FSP_LSB =>

                  sHexOut                 <= sReceivedFSP(3 downto 0);     -- Nibble ist HEX, Wandlung noetig
                  sState_SendAnswer       <= CONV_H2A;                     -- Byte wandeln und senden

                  if (sRWn = '0') then                                     -- wurde ein FSP beschrieben...
                     sState_SendAnswerNext   <= SEND_END_SYNC;             -- ...danach EndSyncByte senden
                  else
                     sState_SendAnswerNext   <= COMP_REMAINING_BYTES;      -- ...andernfalls die FSP Daten senden
                  end if;

               ----------------------------------------------------
               when SEND_END_SYNC =>

                  if (sFSPImgMakeImageInProgress = '0') then               -- Normale Antwort ueber USI versenden
                     sSendByteASCII          <= sSyncEndByte;              -- Byte ist ASCII, keine Wandlung noetig
                     sState_SendAnswer       <= SEND_DATA;                 -- Byte senden
                     sState_SendAnswerNext   <= WAIT_FOR_START_CONDITION;  -- danach auf Neustart warten
                  else
                     sSendByteASCII          <= X"03";                     -- Byte ist ASCII, keine Wandlung noetig
                     --sFSPImgMakeImageFlashSavesData    <= '1';                       -- letztes Zeichen
                     sState_SendAnswer       <= SEND_DATA;                 -- Byte senden
                     sState_SendAnswerNext   <= WAIT_FOR_START_CONDITION;  -- danach auf Neustart warten
                  end if;

               ----------------------------------------------------
               when SEND_NACK =>

                  sSendByteASCII          <= X"15";                        -- Byte ist ASCII, keine Wandlung noetig
                  sState_SendAnswer       <= SEND_DATA;                    -- Byte senden
                  sState_SendAnswerNext   <= SEND_ERR_MSB;                 -- danach Error MSB senden

               ----------------------------------------------------
               when SEND_ERR_MSB =>

                  sHexOut                 <= sError(7 downto 4);           -- Nibble ist HEX, Wandlung noetig
                  sState_SendAnswer       <= CONV_H2A;                     -- Byte wandeln und senden
                  sState_SendAnswerNext   <= SEND_ERR_LSB;                 -- danach Error LSB senden

               ----------------------------------------------------
               when SEND_ERR_LSB =>

                  sHexOut                 <= sError(3 downto 0);           -- Nibble ist HEX, Wandlung noetig
                  sState_SendAnswer       <= CONV_H2A;                     -- Byte wandeln und senden
                  sState_SendAnswerNext   <= SEND_END_SYNC;                -- danach EndSyncByte senden

               ----------------------------------------------------
               when SEND_CRC_MSB =>                                        -- MSB der CRC Summe

                  sHexOut                 <= sCRCCompare(7 downto 4);      -- Nibble ist HEX, Wandlung noetig
                  sState_SendAnswer       <= CONV_H2A;                     -- Byte wandeln und senden
                  sState_SendAnswerNext   <= SEND_CRC_LSB;                 -- danach Error LSB senden

               ----------------------------------------------------
               when SEND_CRC_LSB =>                                        -- LSB der CRC Summe

                  sHexOut                 <= sCRCCompare(3 downto 0);      -- Nibble ist HEX, Wandlung noetig
                  sState_SendAnswer       <= CONV_H2A;                     -- Byte wandeln und senden
                  sState_SendAnswerNext   <= SEND_END_SYNC;                -- danach EndSyncByte senden

               ----------------------------------------------------
               when COMP_REMAINING_BYTES =>

                  if (sRemainingBytes > X"000" ) then                      -- noch Daten im FSP
                     sClockData           <= '1';                          -- naechstes Datenbyte an FSP Ausgang -> ICH Eingang
                     sState_SendAnswer    <= CLOCK_DATA_LOW;
                  else                                                     -- andernfalls SyncEndByte senden
                     sState_SendAnswer    <= SEND_CRC_MSB;
                  end if;

               ----------------------------------------------------
               when CLOCK_DATA_LOW =>

                  sClockData              <= '0';

                  if (sMemRemainingBytes /= sRemainingBytes) then         -- warten bis RemainingBytes minus 1, dann liegen neue Daten aus FSP an
                     sMemRemainingBytes      <= sRemainingBytes;
                     sState_SendAnswer       <= READ_DATA;
                  end if;

               ----------------------------------------------------
               when READ_DATA =>                                           -- oberes oder unteres Nibbel lesen

                  if (InputValueIsASCII = '1') then                        -- an 'Data' anliegendes Byte ist bereits ein ASCII -> keine Wandlung!

                     sSendByteASCII          <= Data(7 downto 0);          -- Byte ist ASCII, keine Wandlung noetig
                     sEnableCRCPulse         <= '1';                       -- Dies ermoeglicht den CRCPulse beim Daten senden
                     sState_SendAnswer       <= SEND_DATA;                 -- Byte senden
                     sState_SendAnswerNext   <= COMP_REMAINING_BYTES;      -- danach testen ob FSP leer

                  else
                     if (sNibbleCounter = '0' ) then                       -- 1. Nibble erfassen
                        sHexOut                 <= Data(7 downto 4);       -- Nibble ist HEX, Wandlung noetig
                        sNibbleCounter          <= '1';
                        sEnableCRCPulse         <= '1';                    -- Dies ermoeglicht den CRCPulse beim Daten senden
                        sState_SendAnswer       <= CONV_H2A;               -- Byte wandlen und senden
                        sState_SendAnswerNext   <= READ_DATA;              -- zweites Nibbel lesen und senden
                     else                                                  -- 1. Nibble erfassen
                        sHexOut                 <= Data(3 downto 0);       -- Nibble ist HEX, Wandlung noetig
                        sNibbleCounter          <= '0';
                        sEnableCRCPulse         <= '1';                    -- Dies ermoeglicht den CRCPulse beim Daten senden
                        sState_SendAnswer       <= CONV_H2A;               -- Byte wandlen und senden
                        sState_SendAnswerNext   <= COMP_REMAINING_BYTES;   -- danach testen ob FSP leer
                     end if;
                  end if;

               ----------------------------------------------------
               when CONV_H2A =>

                  sCalcCRCPulse     <= '0';                                -- Beendet die CRC Bildung
                  sStartConvertH2A  <= '1';                                -- StartConvertH2A auf high legen
                  sState_SendAnswer <= WAIT_FOR_H2A_CONV_DONE;

               ----------------------------------------------------
               when WAIT_FOR_H2A_CONV_DONE =>

               --Takteverlaengerung bei ERR LSB und FSP LSB notwendig !!!!

                  sStartConvertH2A  <= '0';                                -- StartConvertH2A auf low zurueck

                  if (sH2AConvDone = '1') then                             -- warten bis Wandlung abgeschlossen ist
                     sSendByteASCII    <= sASCIIIn;                        -- gewandelten Wert lesen 
                     sState_SendAnswer <= SEND_DATA;                       -- Wert senden
                  end if;

--               ----------------------------------------------------
--               when DUMMY_CLOCK =>                                         -- Dummy Clock ist notwendig aus folgendem Grund:
--                                                                           -- wird 'sNewDataTransferable' in 'SEND_DATA' gesetzt, werden 3 Takte
--                  sState_SendAnswer <= SEND_DATA;                          -- benoetigt, bis 'sTransmitterFree' sicher auf '0' ist.
--                                                                           -- wird ein ASCII Byte direkt in 'sSendByteASCII' kopieret, z.B. ACK(0x06) oder NACK(0x15)
--                                                                           -- und danach 'SEND_DATA' wieder aufgerufen, ist dies mitunter etwas schneller als diese
--                                                                           -- 3 Takte, daher dieser eine 'Dummy'takt

--               ----------------------------------------------------
--               when DUMMY_CLOCK_2 =>                                       -- Dummy Clock_2 ist notwendig aus folgendem Grund:
--                                                                           -- wird 'sNewDataTransferable' in 'SEND_DATA' gesetzt, werden 3 Takte
--                  sState_SendAnswer <= DUMMY_CLOCK;                        -- benoetigt, bis 'sTransmitterFree' sicher auf '0' ist.
--                                                                           -- 
--                                                                           -- und danach 'SEND_DATA' wieder aufgerufen, ist dies mitunter etwas schneller als diese
--                                                                           -- 2 Takte, daher sind 2 'Dummy'takte notwendig

               ----------------------------------------------------
               when SEND_DATA =>

                  if(sFSPImgMakeImageInProgress = '0') then                   --kein FSP Image in ext. Flash, sondern ueber USI kommunizieren
                     if (sTransmitterFree = '1') then                         -- warten bis Transmitter frei ist

                        if (sEnableCRCPulse = '1') then                       -- Dies ermoeglicht den CRCPulse beim Daten senden
                           sCalcCRCPulse     <= '1';                          -- Start die CRC Bildung ausschliesslich wenn Datenbytes gesendet werden
                           sEnableCRCPulse   <= '0';
                        end if;

                        sNewDataTransferable    <= '1';                       -- Wenn '1' teilt dies dem Transmitter neue Daten mit
                        sState_SendAnswer       <= WAIT_FOR_DATA_FETCHED;
                     end if;
                  else
                     if (FSPImgBusy = '0') then                               -- warten bis Zugriffe auf ext. Speicher moeglich sind

                        if (sEnableCRCPulse = '1') then                       -- Dies ermoeglicht den CRCPulse beim Daten senden
                           sCalcCRCPulse     <= '1';                          -- Start die CRC Bildung ausschliesslich wenn Datenbytes gesendet werden
                           sEnableCRCPulse   <= '0';
                        end if;

                        FSPImgWriteData      <= '1';                          -- Wenn '1' teilt dies dem ext. Speicher Handler neue Daten mit
                        sState_SendAnswer    <= WAIT_FOR_FSP_IMG_BUSY_HIGH;
                     else
                        sState_SendAnswer    <= SEND_DATA;
                     end if;
                  end if;

               ----------------------------------------------------
               when WAIT_FOR_FSP_IMG_BUSY_HIGH =>

                  FSPImgWriteData      <= '0';
                  sCalcCRCPulse        <= '0';                                -- Beendet die CRC Bildung

                  if (FSPImgBusy = '1') then
                     sState_SendAnswer       <= WAIT_FOR_FSP_IMG_BUSY_LOW;
                  end if;

               ----------------------------------------------------
               when WAIT_FOR_FSP_IMG_BUSY_LOW =>

                  FSPImgWriteData     <= '0';

                  if (FSPImgBusy = '0') then
                     if(sFSPImgMakeImageFlashSavesData = '0') then                              --FSPImgMakeImage ist noch nicht ganz abgeschlossen...
                        if (sFSPImgMakeImageInProgress = '1') AND (sSendByteASCII = X"03") then --..aber ETX wurde gesendet
                           FSPImgWriteEnable                <= '0';                             --FSPImgWriteEnable loeschen, danach sichert Flash die Daten endgueltig
                           sFSPImgMakeImageFlashSavesData   <= '1';                             --dies signalisieren
                           sState_SendAnswerNext            <= WAIT_FOR_START_CONDITION;
                           sState_SendAnswer                <= WAIT_FOR_FSP_IMG_BUSY_HIGH;      --erneut auf Ende des FSPImgBusy warten
                        else
                           sState_SendAnswer       <= sState_SendAnswerNext;                    --...ETX wurde noch nicht gesendet -> mit naechstem Zeichen weiter machen
                        end if;
                     else                                                                       --FSPImgMakeImage ist abgeschlossen
                        sFSPImgMakeImageFlashSavesData   <= '0';
                        sFSPImgMakeImageInProgress       <= '0';                                --Imagebildung abgeschlossen
                        sState_SendAnswer                <= sState_SendAnswerNext;              --Alle Daten wurde im Flash gesichert
                     end if;
                  end if;

               ----------------------------------------------------
               when WAIT_FOR_DATA_FETCHED =>                               -- ist notwendig aus folgendem Grund:
                                                                           -- wird 'sNewDataTransferable' in 'SEND_DATA' gesetzt, werden 3 Takte
                                                                           -- benoetigt, bis 'sTransmitterFree' sicher auf '0' ist.
                                                                           -- wird ein ASCII Byte direkt in 'sSendByteASCII' kopieret, z.B. ACK(0x06) 
                                                                           -- oder NACK(0x15) oder wird nach ERR_LSB, bzw. FSP_LSB das 'EndSyncByte' gesendet
                                                                           -- und danach 'SEND_DATA' wieder aufgerufen, ist dies mitunter etwas schneller als
                                                                           -- diese 3 Takte, daher wird gewartet bis das Byte sicher abgeholt wurde
                  sCalcCRCPulse        <= '0';                             -- Beendet die CRC Bildung

                  if (sTransmitterFree = '0') then
                     sNewDataTransferable    <= '0';
                     sState_SendAnswer       <= sState_SendAnswerNext;
                  end if;

               ----------------------------------------------------
               when SEND_PID_MSB =>

                  sSendByteASCII          <= X"57";                  -- Byte ist ASCII, keine Wandlung noetig
                  sState_SendAnswer       <= SEND_DATA;              -- Byte senden
                  sState_SendAnswerNext   <= SEND_PID_LSB;           -- danach die PID LSB senden

               ----------------------------------------------------
               when SEND_PID_LSB =>

                  sSendByteASCII          <= X"52";                  -- Byte ist ASCII, keine Wandlung noetig
                  sState_SendAnswer       <= SEND_DATA;              -- Byte senden
                  sState_SendAnswerNext   <= SEND_MA;                -- danach die Moduladresse senden

               ----------------------------------------------------
               when others =>
                  sState_SendAnswer <= WAIT_FOR_START_CONDITION;

            end case;


         end if;  --if (Clock'event and Clock = '1') then

      end if;  --if (Reset = '1') then

   end process pCheckDataConformity;


   --------------------------------------------------------------------
   --Diese Ausgaben sind TriState fähig 
   --------------------------------------------------------------------
   pOutputMapping: process(sRWn, sDataOut)
   begin

      if (sRWn = '0') then             -- ist Write aktive (RWn = '0') Daten ausgeben
         Data     <= sDataOut;
      else                             -- ist Read aktiv (RWn = '1') Daten einlesen, dazu Port in Hi-Z
         Data     <= (others => 'Z');
      end if;

   end process pOutputMapping;

   --------------------------------------------------------------------
   -- Asynchrone Ausgabe
   --------------------------------------------------------------------

   FIFOStartpointRAMData         <=   sFIFOStartpointRAMData;
   FIFOStartpointRAMWRAddr       <=   sFIFOStartpointRAMWRAddr;
   FIFOStartpointRAMWREnable     <=   sFIFOStartpointRAMWREnable;

   FSPImgMakeLoadImageInProgress <= sFSPImgMakeImageInProgress OR sFSPImgLoadImageInProgress;

   ASCIIOut                      <= sASCIIOut;
   Error                         <= sError;
   SendAnswer                    <= sSendAnswer;
   ReceivedPID                   <= sReceivedPID;
   RWn                           <= sRWn;
   StartConvertA2H               <= sStartConvertA2H; 

   ReceivedMA                    <= sReceivedMA;
   ReceivedFSP                   <= sReceivedFSP;

   SendByteASCII                 <= sSendByteASCII;

   ClockData                     <= sClockData;
   CRCOK                         <= sCRCOK;

   HexOut                        <= sHexOut;
   StartConvertH2A               <= sStartConvertH2A;
   NewDataTransferable           <= sNewDataTransferable;

end RTL;
