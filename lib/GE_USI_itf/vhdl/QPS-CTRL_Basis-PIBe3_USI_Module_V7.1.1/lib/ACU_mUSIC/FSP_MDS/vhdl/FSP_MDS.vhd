LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
--USE IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

--*****************************************************************************************************************
--*                                                                                                               *
--* Name        : FSP_MDS                                                                                         *
--*                                                                                                               *
--* Beschreibung: Dieses Modul stellt den MDS (ModulDescriptorStructure) FSP (Fifo-Start-Punkt) zur Verfuegung.   *
--*               Dieser FSP ist mittels Generics konfigurierbar im Hinblick auf dessen Tiefe und Inhalte.        *
--*               Die FSP Nummer ist immer 0x00!                                                                  *
--*               Das MDS FSP ist nur lesbar.                                                                     *
--*                                                                                                               *
--* Version     : 26.01.10 Born (DS)                                                                              *
--*               31.05.10 Ausgabe des FIFO Startpunktdescriptors und 'RemainingBytes' auf 12 Bit erweitert (DS)  *
--*               16.02.11 'SerialNumber' im Hinblick auf 1-wire Seriennummern als S/N ID von 16 auf 48 Bit       *
--*                        (6 Byte) erweitert. (DS)                                                               *
--*               07.10.13 FIFO_Startpoint_RAM ist nun 256 x 32 Bit, da max. 256 FSP moeglich sind. Analog wurde  *
--*                        FSP_Interconnectionhandler geandert und daurch auch mUSIc_Core und mUSIc_Shell. (DS)   *
--*               08.10.14 Wurde das Modul deselektiert war 'OutputValueIsASCII' Hi-Z. In Wahrheit ist das FPGA   *
--*                        aber nicht in der lage intern diesen Zustand zu erzeugen. Daher wird der Compiler      *
--*                        stattdessen einen Selektor/Multiplexer einbauen. Bei Altera ist dabei dieses Signal    *
--*                        bei Deselektierung "genug" Low, damit der 'FSP_Interconnectionhandler' kuenftige       *
--*                        Datenwerte als HEX versteht und vor dem Senden in ASCII wandelt. Bei Xilinx ist das    *
--*                        Signal hingegen nicht "genug" Low und der 'FSP_Interconnectionhandler' wandelt die     *
--*                        eingehenden HEX-Daten nicht in ASCII weil ein "quasi High" am Ausgang                  *
--*                        'OutputValueIsASCII' des 'FSP_MDS' ihm signalisert die Daten seien schon ASCII. Wird   *
--*                        'FSP_MDS' deselektiert, wird 'OutputValueIsASCII' nun '0'. Dies ist kein Problem, da   *
--*                        kein weiteres Modul diesen Port nutzt und Hi-Z sich dadurch eruebrigt. (DS)            *
--*               08.03.17 Bit7 der Attribute hinzugefügt. In der MDS wird dadurch vermerkt, ob es sich um ein    *
--*                        Factory- oder ApplicationImage handelt. Generic 'bDescriptorType' entfernt, da dieser  *
--*                        immer '1' ist. (DS)                                                                    *
--*                                                                                                               *
--*                        Total logic elements            316                                                    *
--*                        Total combinational functions   308                                                    *
--*                        Dedicated logic registers       77                                                     *
--*                        Total registers                 77                                                     *
--*                                                                                                               *
--*               20.06.18 xSWxx heisst jetzt xFWxx, sonst aendert sich nix. (DS)                                 * 
--*               15.09.21 Generic ..Version entfernt (DS)                                                        *           
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*                                                                                                               *
--*****************************************************************************************************************

-- Die Daten am FSPInterconnectionHandlers liegen stehts als 8 Bit Hexadezimalwert, bzw. ASCII an.

-- Funktion:
-- =========
-- Ist das Modul nicht adressiert, sind die Ports 'RemainingBytes' und 'Data' in Tristate, 'Available' und 'ReadOnly' = '0'.
-- Die am Port 'Address' angelegte 8-Bit Adresse wird mit gFSPNumber verglichen. Ist diese stimmig, werden die Tristatports zugeschaltet. 
-- 'RemainingBytes' wird 'cNmbOutputBytes', 'Available'= '1' und 'ReadOnly' = '1'.
-- Der Leser setzt 'ClockData' kurz auf '1' und das Modul legt das MSB des Descriptors 
-- an 'Data'. 'RemainingBytes' wird um 1 reduziert. Der Leser setzt 'ClockData' erneut kurz auf '1' sofern 'RemainingBytes'
-- nicht Null ist.
-- Nach dem selektieren per 'Address' muss der Leser warten bis 'RemainingBytes' /= '0' und 'Available'= '1' ist, 
-- dann kann's losgehen.

-- WICHTIG:
-- ========
-- Der String 'sDescriptor' ist ein ASCII String dessen Hexadezimalrepresentanten ausserhalb des Bereichs liegen,
-- den der HEX2ASCII Decoder erstellen kann. Dieser ist lediglich tauglich 0..9 und A..F in ASCII zu wandeln, also Nibble in Hex nach ASCII.
-- Da der String aber schon ASCII ist, wird bei dessen Ausgabe die Wandlung im FSPInterconnectionHandler nicht durchgefuehrt.
-- Damit dies geschieht wird der Ausgang : 'OutputValueIsASCII' = '1'.
-- Aehnliches gilt fuer die nur Nibbel grossen Werte von 'bDescriptorType' und 'bDeviceMaxSpeed'. Da diese zur Ausgabe 
-- entweder schon als ASCII vorliegen muessen um Bytelaenge zu besitzen oder aber bei der Wandlung durch den
-- FSPInterconnectionHandler zwei ASCII Zeichen (naemlich 0x30 und 0xyy) ergeben wuerden, werden auch diese schon als
-- ASCII Zeichen ausgegeben.
-- Auch der End Descriptor ist davon betroffen. Dieser liegt der Einfachheit halber komplett in ASCII vor
-- wird also an den ohnehin schon als ASCII ausgegebenen 'sDescription' einfach angehaengt.


-- Gesamtlaenge der MDS
-- ====================
-- Die Laenge der MDS ist abhaengig von verschiedenen Faktoren. Die Laenge des Modul Descriptor (0x01) ist fix, bis auf die Laenge des
-- Eintrags 'sDescription'. Der End Descriptor (0x09) ist fix. Die Anzahl der FSP Descriptoren (0x02) ist abhaengig von der Anzahl
-- der im Modul vorhandenen FSPs. Der FSP Descripor selbst ist fix.


entity FSP_MDS is                                                                -- Entity - Blockbeschreibung Ein- und Ausgaenge
   generic 
   (
      gFSPName                         : string  := "FSP00 MDS";                 -- FSP00 ist immmer der MDS

      -- Integer bedeutet hier eine Angabe der Eintraege in Dezimal (Bsp. lProductID = 86(D) wird bei der Ausgabe zu 0x56(H)

      --bDescriptorType                  : integer range 16#F# downto 0    := 1;   -- Descriptortyp 1=Moduldescriptor (4 Bit Hex -> 1 Byte ASCII)
      --wOffset                          : integer range 16#FF# downto 0   := 86;  -- Offset (wird automatisch berechnet) zum naechsten Descriptor (1 Byte Hex -> 2 Byte ASCII)
      wModuleClass                     : integer range 16#FF# downto 0   := 1;   -- Modul-Klassen-Code (1 Byte Hex -> 2 Byte ASCII)
      wModuleSubClass                  : integer range 16#FF# downto 0   := 1;   -- Modul-Subklassen-Code (1 Byte Hex -> 2 Byte ASCII)
      wVendorID                        : integer range 16#FFFF# downto 0 := 1;   -- Hersteller ID (1 Byte Hex -> 2 Byte ASCII)
      lProductID                       : integer range 16#FFFF# downto 0 := 86;  -- Produkt ID (1 Byte Hex -> 2 Byte ASCII)
      wUSI                             : integer range 16#FF# downto 0   := 17;  -- USI Spec. Version (1 Byte Hex (als BCD ,z.B.: 1.0 -> entspr. 16(H)) -> 2 Byte ASCII)
      bDeviceMaxSpeed                  : integer range 16#F# downto 0    := 7;   -- 4 Bit Hex -> 1 Byte ASCII
                                                                                 -- #define USI_BAUDRATE_25M   0x00
                                                                                 -- #define USI_BAUDRATE_20M   0x01
                                                                                 -- #define USI_BAUDRATE_16M   0x02
                                                                                 -- #define USI_BAUDRATE_10M   0x03
                                                                                 -- #define USI_BAUDRATE_5M    0x04
                                                                                 -- #define USI_BAUDRATE_2M    0x05
                                                                                 -- #define USI_BAUDRATE_1M    0x06
                                                                                 -- #define USI_BAUDRATE_115K  0x07

      wAttributes                      : integer range 127 downto 0   := 2;      -- [Bit 1] = 0 => Modul unterstuetzt nur den Normalen USI Modus 
                                                                                 -- [Bit 1} = 1 => Modul unterstuetzt auch den HighSpeed Tunnel Modus(1 Byte Hex -> 2 Byte ASCII)
      gAttrib_Bit7_nFac_Applic_Image   : std_logic := '1';                       -- [Bit 7] der Attribute => 0 = FactoryImage, 1= AppilcationImage
                                                                     
      lMaxPower                        : integer range 16#FFFF# downto 0 := 0;   -- wegen Fremdspeisung entfaellt Angabe des Stroms (2 Byte Hex -> 4 Byte ASCII)
      sDescription                     : string  := "ModulDescriptionStrinmg"    -- Modulname als String
   );
----------------------------------------------------------------
   port 
   (
      Clock                   : in     std_logic;
      Reset                   : in     std_logic;

      sSerial                 : in     std_logic_vector(47 downto 0);   -- Seriennummer (extern per Seriennummermodul) (6 Byte Hex -> 12 Byte ASCII)
      wHWMajorRelease         : in     std_logic_vector( 7 downto 0);   -- HW-Hauptversionsnummer (extern per VersionControl) (1 Byte Hex -> 2 Byte ASCII)
      lHWMinorRelease         : in     std_logic_vector(15 downto 0);   -- HW-Unterversionsnummer (extern per VersionControl) (2 Byte Hex -> 4 Byte ASCII)
      sHWDate                 : in     std_logic_vector(23 downto 0);   -- HW-Versionsdatum (extern per VersionControl) (3 Byte Hex -> 6 Byte ASCII)
      wFWMajorRelease         : in     std_logic_vector( 7 downto 0);   -- SW-Hauptversionsnummer (extern per VersionControl) (1 Byte Hex -> 2 Byte ASCII)
      lFWMinorRelease         : in     std_logic_vector(15 downto 0);   -- SW-Unterversionsnummer (extern per VersionControl) (2 Byte Hex -> 4 Byte ASCII)
      sFWDate                 : in     std_logic_vector(23 downto 0);   -- SW-Versionsdatum (extern per VersionControl) (3 Byte Hex -> 6 Byte ASCII)

      FIFOStartpointRAMDataQ  : in     std_logic_vector(31 downto 0);   -- Daten des FSP Startpunkt Decriptor
      FIFOStartpointRAMRDAddr : out    std_logic_vector( 7 downto 0);   -- Adressen des FSP Startpunkt Descriptor

      Address                 : in     std_logic_vector (7 downto 0);   -- korreliert die angelegte Adresse mit cFSPNumber wird FSP aktiviert
      Data                    : out    std_logic_vector (7 downto 0);   -- Daten von/zum 'FSPInterconnectionHandler' 

      OutputValueIsASCII      : out    std_logic;                       -- Wenn '1' ist der Wert an 'Data' bereits ASCII (gilt bei 'bDescriptor' und 'bDeviceMaxSpeed')

      RemainingBytes          : out    std_logic_vector (11 downto 0);  -- Anzahl noch vorhandener Bytes beim lesen
      Available               : out    std_logic;                       -- Wenn '1' signalisiert dies dem 'ICH', dass dieses FSP vorhanden ist
      ReadOnly                : out    std_logic;                       -- Wird immer '1' wenn MDS-FSP selektiert, da dieser nur lesbar ist
      ClockData               : in     std_logic                        -- Taktet einzelne Bytes aus dem FSP

   );

   --String to std_logic_vector
   function fct_s2slv(s: string) return std_logic_vector is
      
      constant ss: string(1 to s'length) := s;              -- Aufpassen, dass der Eingangsstring eine sinnvolle Laenge hat
      variable answer: std_logic_vector(1 to 8*s'length);   -- Variable um das Ergebnis aufzunehmen
      variable p: integer;                                  -- Variable um die 'schlimme' Ergebniskalkulation zu vereinfachen
      variable c: integer;                                  -- Variable fuer das jeweiligen ASCII code des Charakters

      begin

         for i in ss'range loop                             -- Charakter lesen
            p := 8*i;                                       -- Index des ganz rechten Bit des 8-bit Vektor Wertes
            c := character'pos(ss(i));                      -- Charakter ASCII Code ermitteln 
            answer(p-7 to p) := std_logic_vector(to_unsigned(c,8));
         end loop;

         return answer;
      end;
--
--   --Character To Int
--   function fct_c2i(arg : character) return natural is
--      begin
--      return character'pos(arg);
--   end;
--
--   --Character To std_logic_vector
--   function fct_c2slv(arg : character) return std_logic_vector is
--      begin
--         return std_logic_vector(to_unsigned(fct_c2i(arg), 8));
--      end;

end FSP_MDS;

--Beispiel:
--=========
--                  I  n   t   e   r   l   o   c   k   s   y   s   t   e   m       0x390x330x450x4F0x44
--[0]...     [31] [32][33][34][35][36][37][38][39][40][41][42][43][44][45][46][47][48][49][50][51][52]
--\_____  _____/  \__________________________  _____________________________/   |   \_______  _______/|
--      \/                                   \/                                 |           \/        |
--cNmbAllHEX_ASCIIBytesBody                cNmbDescChar         StartpunktDescriptor   cNmbASCIIBytesEndDesc
--\_________________________________________________  ________________________________________________/
--                                                  \/
--                                            cNmbOutputBytes
--
-- Gesamtanzahl der Ausgabebytes ist z.B. 49, aber die Indizierung ist von 0..48.
-- 'cNmbOutputBytes' dient intern der Berechnung von Indexen und entspricht daher der Summe aller Einzelbytes
--

-- Im obigen Beispiel: Nach Ausgabe von (cNmbAllHEX_ASCIIBytesBody + cNmbDescChar) erfolgt bei erreichen des Index [43]
-- die Ausgabe des FSP Startpunkt Descriptor, bevor die restlichen Bytes von 'cNmbOutputBytes' in Form des EndDescritpors ausgegeben werden.
-- Die Werte des FSP Startpunkt Descritpors befinden sich als Hexwerte im RAM und werden von dort ausgelesen.
-- Solange diese ausgegeben werden bleibt der index bei [43].

-- An den Modul Descriptor wird noch der End Descriptor angehaengt, dieser ist zusaetzliche
-- 6 Byte Lang (0x39 0x30 0x33 0x45 0x4F 0x44)
--                |  |       | \------------/------ "EOD"   (ASCII!)
--                |  \-------/--------------------- Offset von 3
--                \-------------------------------- Descriptor Typ (0x09 fuer End Descriptor) (ASCII!)

-- Am Anfang ist 'sFSPByteCounter' = 0:
-- Ist 'sFSPByteCounter' < (cNmbAllHEX_ASCIIBytesBody + cNumbDescChar) wird MDS ausgegeben
-- Ist 'sFSPByteCounter' = (cNmbAllHEX_ASCIIBytesBody + cNumbDescChar) wird StartpunktDescriptor ausgegeben
-- Ist 'sFSPByteCounter' > (cNmbAllHEX_ASCIIBytesBody + cNumbDescChar) wird EndDescriptor ausgegeben
 
architecture RTL of FSP_MDS is

-------------------------------------------------------------------
-- Signale, Typen, Konstanten fuer  pFSP_MDS
-------------------------------------------------------------------
   -- Anzahl der Datenbytes in HEX/teilweise in ASCII bis zum sDescriptor
   -- Dabei sind ALLE Bytes des Bodys beruecksichtigt ([0]..[31])
   constant cNmbAllHEX_ASCIIBytesBody  : integer := 32;
   -- In rein ASCII sind dies 59 Bytes, weil manche schon in ASCII vorliegen
   -- Beruecksichtigt die Tatsache, dass die Werte aus 'cNmbAllHEX_ASCIIBytesBody' teilweise schon als
   -- ASCII-Zeichen vorliegen und dass die eigentlichen Nutzbytes erst nach dem 'wcOffset' Eintrag beginnen.
   --D.h. hier wird erst ab Beginn der nutzbytes gezaehlt, also ohne Desc.Type und Offset und
   --alle Zeichen in ASCII
   constant cNmbASCIIBytesBody      : integer := 59;  -- ohne 'bDescriptorType' und 'wOffset'
   -- sDescription in std_logic_vector
   constant cDescription            : std_logic_vector( ((sDescription'length*8)-1) downto 0) := fct_s2slv(sDescription);
   -- Anzahl der Zeichen in sDescription
   constant cNmbDescChar            : integer := sDescription'length;
   -- 6 Bytes schon in ASCII fuer End Descriptor
   constant cNmbASCIIBytesEndDesc   : integer := 6;

   -- Anzahl aller Bytes INCL. sDescriptor(cNmbDescChar)
   --constant cNmbOutputBytes         : integer := cNmbAllHEX_ASCIIBytesBody + cNmbDescChar + cNmbASCIIBytesEndDesc;
   constant cNmbOutputBytes         : integer := cNmbAllHEX_ASCIIBytesBody + cNmbDescChar + cNmbASCIIBytesEndDesc + 1; -- + FIFOStartpunktDesc. Dummy Byte

   constant cwOffset                : integer := cNmbASCIIBytesBody + cNmbDescChar;

   subtype  drawer is std_logic_vector(7 downto 0);
   type     sDesc_Array  is array(integer range cNmbOutputBytes-1 downto 0) of drawer;
   signal   sDresser : sDesc_Array;

   signal   sOutputValueIsASCII : std_logic := '0';

   constant clProductID : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(lProductID, 16));
   constant clMaxPower  : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(lMaxPower, 16));

   type     tState_FSP   is ( WAIT_FOR_ADDRESSING,
                              INIT,
                              WAIT_FOR_CLOCK_DATA_HIGH,
                              HANDLE_DATA,
                              INCREMENT_FSP_BYTE_COUNTER,
                              OUTPUT_FSD,
                              WAIT_FOR_DESELCT
                             );

   signal   sState_FSP : tState_FSP;

   signal sDataOut                  : std_logic_vector (7 downto 0);
   signal sAddress                  : integer range 0 to 255 := 0;
   signal sEdgeDetArray             : std_logic_vector (1 downto 0);
   signal sFSPSelected              : std_logic := '0';                 --wird '1' sobald die angelegte Adresse den FSP andressiert
   signal sFSPByteCounter           : integer range 0 to 4095;          --cNmbOutputBytes - 1;
   signal sRemainingBytes           : integer range 0 to 4095;          --cNmbOutputBytes;

   signal sFIFOStartpointRAMRDAddr  : integer range 0 to 255 := 0;            -- zur Zeit sind die FSPs auf 255 - 1 (Summe an Adresse 0) beschraenkt
   signal sNumbers                  : integer range 0 to 255 := 0;
   signal sIndex                    : integer range 0 to 7 := 0;

   constant cFSPNumber              : integer := 0;


   begin

      pFSP : process (Clock, Reset)
      begin

         if (Reset = '1') then

            sState_FSP                 <= WAIT_FOR_ADDRESSING;                -- Statemachine
            sAddress                   <= 0;                                  -- FSP Nummer
            sFSPByteCounter            <= 0;                                  -- Zaehler fuer Anzahl im sDesc_Array eingetragender Bytes
            sEdgeDetArray              <= (others => '0');                    -- dient der Flankenerkennung von 'ClockData' 
            sRemainingBytes            <= 0;                                  -- Zaehler fuer noch auslesbare Bytes (Gesamtanzahl aller Bytes)
            sDataOut                   <= (others => '0');
            sOutputValueIsASCII        <= '0';
            sFIFOStartpointRAMRDAddr   <= 0;
            sNumbers                   <= 0;
            sIndex                     <= 0;
         else
            if (Clock'event and Clock = '1') then 

               sAddress                <= to_integer(unsigned(Address));      -- angelegte Adresse lesen

               if (sAddress /= cFSPNumber) then                               -- falsche Adresse -> verharren
                  sState_FSP                 <= WAIT_FOR_ADDRESSING;
                  sFSPByteCounter            <= 0;                            -- Bytezaehler auf erstes Ausgabebyte
                  sRemainingBytes            <= 0;
                  sFSPSelected               <= '0';
                  sOutputValueIsASCII        <= '0';
                  sFIFOStartpointRAMRDAddr   <= 0;                            -- FIFO StartpunktRAMadresse auf Gesamteintrag

               else

                  case (sState_FSP) is

                     -----------------------------------------------------------------
                     when WAIT_FOR_ADDRESSING =>

                        if (sAddress = cFSPNumber) then                       -- Wenn FSP adressiert, dann:
                           sNumbers                 <= to_integer(unsigned(FIFOStartpointRAMDataQ(7 downto 0)) ); -- Eintraege auslesen
                           sState_FSP               <= INIT;
                        end if;

                     -----------------------------------------------------------------
                     when INIT =>

                           sFSPSelected             <= '1';                                        -- Selectinfo auf '1'
                           sFIFOStartpointRAMRDAddr <= 1;                                          -- FIFO StartpunktRAMadresse auf 1. FIFO Eintrag
                           sDresser(30)             <= X"00";                                      -- Anzahl unterstuetzter FSPs MSB
                           sDresser(31)             <= std_logic_vector(to_unsigned(sNumbers, 8)); -- Anzahl unterstuetzter FSPs LSB
                           sRemainingBytes          <= cNmbOutputBytes - 1 + (6 * sNumbers);       -- Anzahl aller Ausgabebytes
                           --sRemainingBytes          <= cNmbOutputBytes + (6 * sNumbers);       -- Anzahl aller Ausgabebytes
                           sState_FSP               <= WAIT_FOR_CLOCK_DATA_HIGH;                   -- in naechsten State

                     -----------------------------------------------------------------
                     when WAIT_FOR_CLOCK_DATA_HIGH =>

                        sEdgeDetArray  <= (sEdgeDetArray(0) & ClockData);     -- Flankenerkennung

                        if (sEdgeDetArray = "01") then                        -- steigende Flanke an ClockData erkannt 
                           sState_FSP <= HANDLE_DATA;
                        else
                           sState_FSP <= WAIT_FOR_CLOCK_DATA_HIGH;            -- FSP noch adressiert, also warten
                        end if;

                     -----------------------------------------------------------------
                     when HANDLE_DATA =>

                        if (sFSPByteCounter < (cNmbAllHEX_ASCIIBytesBody + cNmbDescChar)) then     -- MDS ausgeben 
                           sDataOut(7 downto 0) <= sDresser(sFSPByteCounter);                   -- Daten interner Bus an externen Bus

                           if ( (sFSPByteCounter = 0) or (sFSPByteCounter = 26) or (sFSPByteCounter >= cNmbAllHEX_ASCIIBytesBody) ) then
                              sOutputValueIsASCII  <= '1';
                           else
                              sOutputValueIsASCII  <= '0';
                           end if;

                           sState_FSP           <= INCREMENT_FSP_BYTE_COUNTER;

                        elsif (sFSPByteCounter = (cNmbAllHEX_ASCIIBytesBody + cNmbDescChar)) then  -- StartpunktDescriptor ausgeben
                           sOutputValueIsASCII     <= '0';
                           sState_FSP              <= OUTPUT_FSD;                               -- FIFOStartpunkt Descriptor ausgeben

                        elsif (sFSPByteCounter > (cNmbAllHEX_ASCIIBytesBody + cNmbDescChar)) then  -- EndDescriptor ausgeben
                           sDataOut(7 downto 0) <= sDresser(sFSPByteCounter);                   -- Daten interner Bus an externen Bus
                           sOutputValueIsASCII  <= '1';
                           sState_FSP           <= INCREMENT_FSP_BYTE_COUNTER;
                        else
                           sDataOut <=X"FF";
                        end if;

                        sRemainingBytes      <= sRemainingBytes - 1;

                     -----------------------------------------------------------------
                     when INCREMENT_FSP_BYTE_COUNTER =>

                        --if (sFSPByteCounter < cNmbOutputBytes - 1) then       -- Indizierung ist Gesamtanzahl aller Bytes - 1 (Bsp.:[0]..[41])
                        if (sFSPByteCounter <= cNmbOutputBytes) then          -- Indizierung ist Gesamtanzahl aller Bytes - 1 (Bsp.:[0]..[41])
                           sFSPByteCounter <= sFSPByteCounter + 1;
                           sState_FSP      <= WAIT_FOR_CLOCK_DATA_HIGH;       -- weitere Bytes ausgeben
                        else
                           sState_FSP      <= WAIT_FOR_DESELCT;               -- alle Bytes ausgegeben -> auf Deselektion warten
                        end if;

                     -----------------------------------------------------------------
                     when OUTPUT_FSD =>

                        case (sIndex) is

                           when 0 =>
                              sDataOut(7 downto 0) <=  X"32";        -- Descriptor ID
                              sOutputValueIsASCII  <= '1';
                              sIndex      <= sIndex + 1;
                           when 1 =>
                              sDataOut(7 downto 0) <=  X"08";        -- Descriptor Offset
                              sOutputValueIsASCII  <= '0';
                              sIndex      <= sIndex + 1;
                           when 2 =>
                              sDataOut(7 downto 0) <=  FIFOStartpointRAMDataQ(31 downto 24);
                              sIndex      <= sIndex + 1;
                           when 3 =>
                              sDataOut(7 downto 0) <=  FIFOStartpointRAMDataQ(23 downto 16);
                              sIndex      <= sIndex + 1;
                           when 4 =>
                              sDataOut(7 downto 0) <=  FIFOStartpointRAMDataQ(15 downto 8);
                              sIndex      <= sIndex + 1;
                           when 5 =>
                              sDataOut(7 downto 0) <=  FIFOStartpointRAMDataQ(7 downto 0);
                              sIndex      <= 0;

                              if (sNumbers > 1) then
                                 sFIFOStartpointRAMRDAddr <= sFIFOStartpointRAMRDAddr + 1;
                                 sNumbers                 <= sNumbers - 1;
                              else
                                 sFSPByteCounter <= sFSPByteCounter + 1;
                              end if;

                           when others => NULL;

                        end case;

                        sState_FSP  <= WAIT_FOR_CLOCK_DATA_HIGH;

                     -----------------------------------------------------------------
                     when WAIT_FOR_DESELCT =>

                        if (sAddress = cFSPNumber) then
                           sState_FSP <= WAIT_FOR_DESELCT;
                        else 
                           sState_FSP <= WAIT_FOR_ADDRESSING;
                        end if;                        

                     when others => 
                        sState_FSP <= WAIT_FOR_ADDRESSING;

                  end case;
               end if;  --if (sAddress /= cFSPNumber) then                               -- falsche Adresse -> verharren
            end if;  --if (Clock'event and Clock = '1') then 
         end if;  --if (Reset = '1') then

      end process pFSP;


--------------------------------------------------------------------
-- Fuellen von sModDescContent
-------------------------------------------------------------------- 

 --character'val(conv_integer<HEXWERT>)
   
   sDresser(0)   <= X"31"; --std_logic_vector((to_unsigned(bDescriptorType, 8)) + X"30"); -- Descriptortyp 1=Moduldescriptor (4 Bit Hex -> 1 Byte ASCII)
   sDresser(1)   <= std_logic_vector(to_unsigned(cwOffset, 8));                  -- Offset (wird automatisch berechnet) zum naechsten Descriptor (1 Byte Hex -> 2 Byte ASCII)
   sDresser(2)   <= std_logic_vector(to_unsigned(wModuleClass, 8));              -- Modul-Klassen-Code (1 Byte Hex -> 2 Byte ASCII)
   sDresser(3)   <= std_logic_vector(to_unsigned(wModuleSubClass, 8));           -- Modul-Subklassen-Code (1 Byte Hex -> 2 Byte ASCII)
   sDresser(4)   <= sSerial(47 downto 40);                                       -- Seriennummer (extern per Seriennummermodul) (2 Byte Hex -> 4 Byte ASCII)
   sDresser(5)   <= sSerial(39 downto 32);
   sDresser(6)   <= sSerial(31 downto 24);
   sDresser(7)   <= sSerial(23 downto 16);
   sDresser(8)   <= sSerial(15 downto  8);
   sDresser(9)   <= sSerial( 7 downto  0);
   sDresser(10)  <= std_logic_vector(to_unsigned(wVendorID, 8));                 -- Hersteller ID (1 Byte Hex -> 2 Byte ASCII)
    
   sDresser(11)  <= clProductID(15 downto 8);                                    -- Produkt ID (1 Byte Hex -> 2 Byte ASCII)
   sDresser(12)  <= clProductID(7 downto 0);                                     -- Produkt ID (1 Byte Hex -> 2 Byte ASCII)
   
   sDresser(13)   <= wHWMajorRelease(7 downto 0);                                -- HW-Hauptversionsnummer (extern per VersionControl) (1 Byte Hex -> 2 Byte ASCII)
   sDresser(14)   <= lHWMinorRelease(15 downto 8);                               -- HW-Unterversionsnummer (extern per VersionControl) (2 Byte Hex -> 4 Byte ASCII)
   sDresser(15)   <= lHWMinorRelease( 7 downto 0);
   sDresser(16)   <= sHWDate(23 downto 16);                                      -- HW-Versionsdatum (extern per VersionControl) (3 Byte Hex -> 6 Byte ASCII)
   sDresser(17)   <= sHWDate(15 downto  8);
   sDresser(18)   <= sHWDate( 7 downto  0);
   sDresser(19)   <= wFWMajorRelease(7 downto 0);                                -- SW-Hauptversionsnummer (extern per VersionControl) (1 Byte Hex -> 2 Byte ASCII)
   sDresser(20)   <= lFWMinorRelease(15 downto 8);                               -- SW-Unterversionsnummer (extern per VersionControl) (2 Byte Hex -> 4 Byte ASCII)
   sDresser(21)   <= lFWMinorRelease( 7 downto 0);
   sDresser(22)   <= sFWDate(23 downto 16);                                      -- SW-Versionsdatum (extern per VersionControl) (3 Byte Hex -> 6 Byte ASCII)
   sDresser(23)   <= sFWDate(15 downto  8);
   sDresser(24)   <= sFWDate( 7 downto  0);
   sDresser(25)   <= std_logic_vector(to_unsigned(wUSI, 8));                     -- USI Spec. Version (1 Byte Hex (als BCD ,z.B.: 1.0) -> 2 Byte ASCII)
   sDresser(26)   <= std_logic_vector((to_unsigned(bDeviceMaxSpeed, 8)) + X"30");-- 16MBaud (4 Bit Hex -> 1 Byte ASCII)
   sDresser(27)   <= gAttrib_Bit7_nFac_Applic_Image & std_logic_vector(to_unsigned(wAttributes, 7)); -- MSB => nFactory- oder ApplicationImage
                                                                                                     -- Modulattribute 0x02 -> USI-Highspeed + fremdgespeist (1 Byte Hex -> 2 Byte ASCII)
   sDresser(28)   <= clMaxPower(15 downto 8);                                    -- wegen Fremdspeisung entfaellt Angabe des Stroms (2 Byte Hex -> 4 Byte ASCII)
   sDresser(29)   <= clMaxPower( 7 downto 0);

   --Nachfolgendes wird bei Selektion gefuellt (lNmbsOfFSPs)
   --sDresser(30)             <= X"00";                                         -- Anzahl unterstuetzter FSPs MSB
   --sDresser(31)             <= std_logic_vector(to_unsigned(sNumbers, 8));    -- Anzahl unterstuetzter FSPs LSB


--------------------------------------------------------------------
-- Wandlung von cDescriptor in ein Array
-------------------------------------------------------------------- 
--cDescriptor = "Hello World"
--sDresser wird von hinten gefuellt, d.h. beginnen mit 'd' an sDresser(max)
--sDresser (max) (max-1) (max-2) (max-3) (max-4) ...
--          [d]    [l]     [r]     [o]     [w]   ...
--
-- (Bsp.: [26]..[40], [40] = cNmbOutputBytes - cNmbASCIIBytesEndDesc - 1)

      conv_data_to_array_gen : for i in 0 to cNmbDescChar-1 generate
         --sDresser((cNmbOutputBytes-cNmbASCIIBytesEndDesc-1)-i) <= cDescription(((i * 8)+7) downto (i * 8));
         sDresser((cNmbOutputBytes-cNmbASCIIBytesEndDesc-2)-i) <= cDescription(((i * 8)+7) downto (i * 8)); -- -2 w/ zusaetzlichem FIFO Startpunkt Desc. Dummy Byte
      end generate;

--------------------------------------------------------------------
-- Anhaengen des End Descriptors
--------------------------------------------------------------------
   sDresser(cNmbOutputBytes - 7) <= X"00" ;    -- Dummyeintrag fuer Ausgabe des FIFO Startpunkt Desc., dieser wird NICHT ausgegeben, sondern dient der Positionsfindung

   sDresser(cNmbOutputBytes - 6) <= X"39" ;    -- END DESCRIPTOR
   sDresser(cNmbOutputBytes - 5) <= X"30" ;    -- Offset bis zum Ende des Enddescriptors    
   sDresser(cNmbOutputBytes - 4) <= X"33" ;   
   sDresser(cNmbOutputBytes - 3) <= X"45" ;    -- E   
   sDresser(cNmbOutputBytes - 2) <= X"4F" ;    -- O
   sDresser(cNmbOutputBytes - 1) <= X"44" ;    -- D 

--------------------------------------------------------------------
--Diese Ausgaben sind u.a. TriState faehig 
--------------------------------------------------------------------
   pOutputMapping: process(sFSPSelected, sRemainingBytes, sDataOut, sOutputValueIsASCII )
   begin

      if (sFSPSelected = '1') then

         Data                 <= sDataOut;
         RemainingBytes       <= std_logic_vector(to_unsigned(sRemainingBytes, 12));
         Available            <= '1';
         ReadOnly             <= '1';
         OutputValueIsASCII   <= sOutputValueIsASCII;

      else

         Data                 <= (others => 'Z');
         RemainingBytes       <= (others => 'Z');
         Available            <= '0';
         ReadOnly             <= '0';
         OutputValueIsASCII   <= '0';

      end if;

   end process pOutputMapping;

   --Asynchrone Ausgabe
   FIFOStartpointRAMRDAddr <= std_logic_vector(to_unsigned(sFIFOStartpointRAMRDAddr, 8));


end RTL;
