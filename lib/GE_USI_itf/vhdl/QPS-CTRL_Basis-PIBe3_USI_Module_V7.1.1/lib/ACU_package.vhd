-----------------------------------------------------------------------------------
-- File name: ACU_package                                                        --
--                                                                               --
-- Author   : D.Rodomonti                                                        --
-- Date     : 27/01/2015                                                         --
--                                                                               --
-- Comments : This package collects all components, functions, procedures and    --
--            type declarations usefull in several ACU system files              --
--                                                                               --
-- History  : Start up version 27/01/2015                                        --
-----------------------------------------------------------------------------------
--            25/08/2015: Added bitWiseOR component.                             --
-----------------------------------------------------------------------------------
--            Added array8bits declaration for Intelock MEmory component21.09.15 --
-----------------------------------------------------------------------------------
--            Added External RAM controller record 27.10.15                      --
-----------------------------------------------------------------------------------
--            Added Altera Remote Update record 01.06.16                         --
-----------------------------------------------------------------------------------
--            Added FSP_InputRAM array 08/07/2019                                --
-----------------------------------------------------------------------------------
--            Changed ToRUStatusFSP,FromAltRemoteUpdate, ToAltRemoteUpdate       --
--            records 16/12/2019                                                 --
-----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_signed.all;
--USE ieee.numeric_std.all;

LIBRARY work;
USE work.all;

package ACU_package is
  
  --
  -- This procedure provides the C2 input value in its output
  -- (input and outputs are 20 bits longs)
  procedure C2Conv (signal Val2Convert   : in std_logic_vector(19 downto 0);
                    signal ValConverted  : out std_logic_vector(19 downto 0));
  procedure C2Conv24b (signal Val2Convert   : in std_logic_vector(23 downto 0);
                       signal ValConverted  : out std_logic_vector(23 downto 0));
 
  --
  -- This component implements the bitwise or between the two input signals.
  component  bitWiseOR IS
    generic
     (
       nBits    : integer range 0 to 128
     );  
     PORT
     (
       inSig1    : in std_logic_vector(nBits-1 downto 0);
       inSig2    : in std_logic_vector(nBits-1 downto 0);
       outSig    : out std_logic_vector(nBits-1 downto 0)
     );
  end component bitWiseOR;
  
  --
  -- Array declaration 
  type array2bits   is array (natural range <>) of std_logic_vector(1 downto 0);
  type array3bits   is array (natural range <>) of std_logic_vector(2 downto 0);
  type array8bits   is array (natural range <>) of std_logic_vector(7 downto 0);
  type array12bits  is array (natural range <>) of std_logic_vector(11 downto 0);
  type array14bits  is array (natural range <>) of std_logic_vector(13 downto 0);
  type array16bits  is array (natural range <>) of std_logic_vector(15 downto 0);
  type array20bits  is array (natural range <>) of std_logic_vector(19 downto 0);
  type array24bits  is array (natural range <>) of std_logic_vector(23 downto 0);
  type array28bits  is array (natural range <>) of std_logic_vector(27 downto 0);
  type array32bits  is array (natural range <>) of std_logic_vector(31 downto 0);
  type array48bits  is array (natural range <>) of std_logic_vector(47 downto 0);
  type array64bits  is array (natural range <>) of std_logic_vector(63 downto 0);
  type array160bits is array (natural range <>) of std_logic_vector(159 downto 0);

  type u_array12bits  is array (natural range <>) of unsigned(11 downto 0);
  type u_array14bits  is array (natural range <>) of unsigned(13 downto 0);
  
  type i_array      is array (natural range <>) of integer;
                                                                                 
  type arrayFSP_InputRAM is array (natural range <>) of array8bits(1 downto 0);  -- array8bits single location (1) => FSP Nr
                                                                                 --			       (0) => FSP Depth
  --
  -- Record declaration
  type ExtRAM_Bus is 
    record
      --Addr : std_logic_vector(6 downto 0);                                     --!!! TO CHANGE IN THE OFFICIAL RELEASE (22 downto 0)  -- Address.
      Addr : std_logic_vector(22 downto 0);                                     --!!! TO CHANGE IN THE OFFICIAL RELEASE (22 downto 0)  -- Address.
      --Dt   : std_logic_vector(7 downto 0);                                    -- Data. this bus is bidirectional so I can't include it in the record.
      WEn  : std_logic;                                                         -- Write enable signal. Active low. During the read action this signal is stuck to one.
      CS1n : std_logic;                                                         -- Chip Select 1 active low.
      CS2  : std_logic;                                                         -- Chip Select 2 active high.
      OEn  : std_logic;                                                         -- Output Enable active low. During the write action this signal is stuck to one.
  end record;


  type ToRUStatusFSP is
    record
      MSM_State          : std_logic_vector(1 downto 0);                        -- Master State Machine State
                                                                                --     00 => Factory mode
										--     01 => Application mode
										--     10 => Application mode with Master State MAchine User Watchdog Timer Enabled
										--     11 => Not valid.
      Cd_early           : std_logic;                                           -- Force Early Configuration Done check (active high)
      WDTimeOut          : std_logic_vector(28 downto 0);                       -- Watchdog time out value. The WD counter is updated every 100ns (internal ck freq=10MHz)
      WDEnable           : std_logic;                                           -- Watchdog Enable(active high)
      BootAddress        : std_logic_vector(31 downto 0);                       -- EPCS configuration boot address
      ForceIntOsc        : std_logic;                                           -- Force the internal oscillator as startup state machine clock option bit.
      ReconfTriggerCond  : std_logic_vector(4 downto 0);                        -- Reconfiguration trigger condition
                                                                                --   bit 4 => external configuration reset (nCONFIG) 
										--   bit 3 => CRC error
										--   bit 2 => nSTATUS asserted by an external device
										--   bit 1 => Watchdog Timeout
										--   bit 0 => configuration reset triggered from logic array
  end record;
  
  type FromAltRemoteUpdate is
    record
      busy               : std_logic;                                           -- when high it is not possible to read/write parameter to altremote_update block
      dataOut            : std_logic_vector(31 downto 0);                       -- data read parameter 
  end record;
  
  type ToAltRemoteUpdate is
    record
      readSource         : std_logic_vector(1 downto 0);                        -- This signal specifies the state from which a parameter value is read:
                                                                                --    00 => Current State
										--    01 => Previous State Register 1 Contents in Status Register
										--    10 => Previous State Register 2 Contents in Status Register
										--    11 => Illegal
      param              : std_logic_vector(2 downto 0);                        -- parameter value to read or to write:
                                                                                --   000 => MSM current state (read only)
										--   001 => Force Early Configuration Done check
										--   010 => Watchdog time out value(29 bits when Rd, 12 when write)
										--   011 => Watchdog Enable
										--   100 => EPCS configuration boot address
										--   101 => Illegal
										--   110 => Force the internal oscillator as startup state machine clock option bit
										--   111 => Reconfiguration trigger condition (read only).
      RdParam            : std_logic;                                           -- Read param command.
      WrParam            : std_logic;                                           -- Write param command.
      dataIn             : std_logic_vector(31 downto 0);                       -- data write parameter.
      reconf             : std_logic;                                           -- Reconfiguration trigger signal
      
  end record;
 
 end ACU_package;

--package body ACU_package is
  
--  procedure C2Conv (signal Val2Convert   : in std_logic_vector(19 downto 0);
--                    signal ValConverted  : out std_logic_vector(19 downto 0)) is		  
--  begin
--    ValConverted  <= not(Val2Convert(19)) & (not(Val2Convert(18 downto 0)) + conv_std_logic_vector(1,19));
--    --ValConverted  <= not(Val2Convert(19)) & std_logic_vector( unsigned( not(Val2Convert(18 downto 0)) ) + to_unsigned(1,19) );
--  end C2Conv;
    
--end package body ACU_package;

package body ACU_package is

  procedure C2Conv (signal Val2Convert   : in std_logic_vector(19 downto 0);
		              signal ValConverted  : out std_logic_vector(19 downto 0)) is		
    begin
      ValConverted  <= not(Val2Convert(19)) & (not(Val2Convert(18 downto 0)) + conv_std_logic_vector(1,19));
  
  end C2Conv;
  
  procedure C2Conv24b (signal Val2Convert   : in std_logic_vector(23 downto 0);
                       signal ValConverted  : out std_logic_vector(23 downto 0)) is		  
   begin
      ValConverted  <= not(Val2Convert(23)) & (not(Val2Convert(22 downto 0)) + conv_std_logic_vector(1,23));

  end C2Conv24b;
    
end package body ACU_package;



