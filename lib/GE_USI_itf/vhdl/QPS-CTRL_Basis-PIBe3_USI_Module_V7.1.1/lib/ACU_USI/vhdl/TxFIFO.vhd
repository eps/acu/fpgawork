LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- dd.mm.yy  born
--10.05.10  sWriteEnableEdgeDetArray von 3 Bit auf 2 Bit reduziert. Die Flankenerkennung ist notwendig, da
--          auch eine CPU ggf. das USI Modul triggern muss. In unseren Falle uebernimmt dies z.B. der NiosII in der MFU.
--          Es reicht aber eine einfache Erkennung, das Jitter innerhalb des FPGA sehr gering sein solten.
--          Einfache Erkennung wegen mUSIC eingefuehrt, damit 'TransmitData-Modul' nur einen Puls schicken muss. (DS)
--10.08.10  'TxDataFetched' eingefuehrt. Dieses Signal wird 1 Takt Puls high, wenn die Daten in den FIFO eingelesen sind.
--          und dient der Ruekmeldung, dass die fuellende Instanz (z.B. mUSIC 'TransmitData')
--          die Daten sicher abgeliefert hat. (DS)
--17.01.11 'sTxFIFOState' um einen State reduziert. Durch den Wegfall des States 'STOP' und die Integrierung von 
--          'sTxFIFOEnable' in 'IDLE' anstelle 'COMMIT' wird 'USITXRx' nun einen Takt schneller darueber informiert,
--          das ein neues Byte zum senden anliegt. Dies verkuerzt das STOP-BIT um einen Takt. (DS)
--14.08.12 'sTxFIFOnEmpty' wurde nie '0' wenn das oberste Bit im Array zwar wieder geloescht wurde, aber mind. noch ein weiteres
--          Bit im Array gesetzt war, durch ein nachfolgend empf. Byte. Siehe auch RxFIFO, dort war es das gleiche Problem (DS)
--15.10.12  Ports und Signale der einzelnen Module sind teilweise mit neuen Namen versehen um deren Funktion klarer zu machen (DS)
--dd.mm.yy  Bisweilen wurde die Anzahl der im TxFIFO Befindlichen Werte zur Generierug der 'TxAlmostFull' und 'TxFull' Flags
--          durch einen Zaehler 'sNumberOfEntriesInTxFIFO' ermittelt. Dieser konnte zeitgleich auf- und abgezaehlt werden.
--          Dadurch konnte es passieren, dass der Zaehler einen Zaehlvorgang (auf oder ab) nicht erfasste und folglich falsche
--          Informationen enthielt (Zaehlung nach negativ oder noch Werte im TxFIFO obwohl dieser leer war). Dies ist nun behoben
--          indem nicht der Zaehler direkt erhoeht oder erniedrigt wird, sondern 'sCountNumberOfEntriesInTxFIFOUp' bzw.
--          'sCountNumberOfEntriesInTxFIFODown' und diese Signale erst beim naechsten Takt den Zaehler bedienen. Dabei wird
--          unterschieden ob nur eines der Zaehlsignale bedient wurde oder beide zeitgleich.
--          Dieser Vorgang dauert 2 Takte, daher erfolgt die Ausgabe von 'TxAlmostFull' und 'TxFull' auch um diese 2 Takte verzoegert.
--          ALTERNATIV: Die Ausgabe von 'TxAlmostFull' und 'TxFull' ist hier zusaetzlich nun realisiert wie beim RxFIFO. Dabei werden
--          die Fuellpositionen im TxFIFO Schieberegister geprueft und zur Ermittlung von 'TxAlmostFull' und 'TxFull' verwendet.
--          Dadurch erfolgt die Ausgabe bereits einen Takt frueher.
--          Warum dies hier anders als im RxFIFO realisiert wurde kann ich nicht mehr sagen. Evtl. hat es damit zu tun, dass wenn die Fuellung des
--          TxFIFO sehr schnell erfolgt 'sTxFIFOArrayPosFilled(2)(1)(0)' faelschlich fuer einen oder 2 Takte die 
--          Info generieren konnen, dass 'TxAlmostFull' oder 'TxFull'. Allerdings erfolgt das Schieben der TxFIFO Fuellpositionen
--          bei jedem Tkt. D.h. Wurde Pos(0) gefuellt und an Pos(1), bzw. dann an Pos(2) geschoben und zwischenzeitlich Pos(0) neu gefuellt,
--          so muesste dieses Neubefuellen in nur 3-4 Takten erfolgen. Wie auch immer, der bisherige und nun korrigierte Zaehler ist nun auskommentiert
--          und die Ausgabe von 'TxAlmostFull' und 'TxFull' mittels Fuellpositionserkennung ist nun aktiv. (02.07.13 - DS)
--01.04.16  'sTxFIFOEnable' hatte ueberhaupt nichts mit 'TxFIFOEnable' zu tun, wieso auch immer? Stattdessen wurde das Signal 'sTxFIFOEnable'
--          mit 'TxFIFONewDataOnDataOut' verknuepft.
--          Dies uebernimmt nun 'sTxFIFONewDataOnDataOut'. 'sTxFIFOEnable' ist entfallen. (DS)

entity TxFIFO is
   generic 
   (
      gTxFIFODepth                  : integer := 4;
      gTxFIFOMaxWidth               : integer := 8
   );
   PORT
   (  Clock                         : in  std_logic;
      Reset                         : in  std_logic;
      TxFIFOEnable                  : in  std_logic;

      TxFIFOGetDataFromDataIn       : in  std_logic;
      TxFIFODataIn                  : in  std_logic_vector(gTxFIFOMaxWidth-1 downto 0);
      TxFIFODataFetchedFromDataIn   : out std_logic;

      TxFIFOTransmitterFreeInfo     : in  std_logic;  --wird von USI-Tx gesetzt, wenn Senderegister frei

      TxFIFODataOut                 : out std_logic_vector(gTxFIFOMaxWidth-1 downto 0);    
      TxFIFONewDataOnDataOut        : out std_logic;  --muss TxFIFO setzen um USI-Tx zum senden zu veranlassen

      TxFIFOnEmpty                  : out std_logic;  --FIFO nicht leer
      TxFIFOAlmostFull              : out std_logic;  --nur noch ein Speicherplatz frei
      TxFIFOFull                    : out std_logic;  --FIFO voll    
      TxFIFOOverflow                : out std_logic   --Ueberlauf
   );

end TxFIFO;


--Beschreibung

--                    Daten --> TxFIFO --> USIRxTx --> USI Kabel
--                                 |
--  TxFIFODataFetchedFromDataIn <--/


--Daten werden von CPU u.a.(Host) in den TxFIFO eingetragen durch:
--Host legt Daten an 'TxFIFODataIn' an und mit pos. Flanke an 'TxFIFOGetDataFromDataIn' werden diese in den FIFO uebernehmen.
--USI-Tx legt 'TxFIFOTransmitterFreeInfo' auf '1' wenn dessen Senderegister frei ist und
--wenn noch Daten in TxFIFO sind, legt TxFIFO diese Daten an 'TxFIFODataOut' an
--und setzt 'TxFIFONewDataOnDataOut' auf '1' -> '0'
--'TxDataFetched' wird 1-Takt Puls high, wenn die Daten in den FIFO eingelesen sind.
--Dies dient der Rueckmeldung, dass die fuellende Instanz (z.B. mUSIC 'TransmitData')
--die Daten sicher abgeliefert hat.

architecture RTL of TxFIFO is

   type     tTxFIFOState  is (
                                 IDLE,
                                 COMMIT
                             );
   signal   sTxFIFOState     : tTxFIFOState;

   type     tTxFIFOArray is array (gTxFIFODepth - 1 downto 0) of std_logic_vector(gTxFIFOMaxWidth-1 downto 0);
   
   signal   sTxFIFOArrayPointer        : tTxFIFOArray;
   signal   sTxFIFOArrayPosFilled      : std_logic_vector(gTxFIFODepth-1  downto 0);

   signal   sTxFIFOGetDataFromDataIn_EdgeDet  : std_logic := '0';                         --steigende Flanke an 'WriteEnable' erkannt

   signal   sTxFIFOFull                : std_logic := '0';                                --FIFO voll
   signal   sTxFIFOOverflow            : std_logic := '0';                                --FIFO Ueberlauf

   signal   sTxFIFONewDataOnDataOut    : std_logic := '0';


begin

   --pos. Flanke an TxFIFOGetDataFromDataIn
   --nur wenn TxFIFOEnable ='1'
   --CPU legt Daten an TxFIFODataIn, pos. Flanke uebernimmt diese in FIFO
   
   pWriteEnableEdgeDet: process (Clock, Reset)
      variable vTxFIFOGetDataFromDataIn_EdgeDetArry : std_logic_vector(1 downto 0);
   begin
      if (Reset = '1') then
         vTxFIFOGetDataFromDataIn_EdgeDetArry := (others => '0');
      elsif (Clock'event and Clock = '1') then
         if (TxFIFOEnable = '1') then
            vTxFIFOGetDataFromDataIn_EdgeDetArry := (vTxFIFOGetDataFromDataIn_EdgeDetArry(0) & TxFIFOGetDataFromDataIn );
            if (vTxFIFOGetDataFromDataIn_EdgeDetArry = "01") then --steigende Flanke
               sTxFIFOGetDataFromDataIn_EdgeDet <= '1';
            else
               sTxFIFOGetDataFromDataIn_EdgeDet <= '0';
            end if ;
         end if;
      end if ;
   end process pWriteEnableEdgeDet;

   --FIFO
   pTxFIFO : process (Clock, Reset)
   begin

      assert (gTxFIFODepth >= 3)
         report "Die Tiefe des USI-TxFIFOs muss mit 'gTxFIFODepth' mindestens 3 sein, aktuell ist diese auf " & integer'image(gTxFIFODepth) & " eingestellt"
      severity error;

      if (Reset = '1') then

         for i in 0 to gTxFIFODepth-1 loop
            sTxFIFOArrayPointer(i) <= (others => '0');
         end loop;

         sTxFIFOOverflow            <= '0';

         sTxFIFOArrayPosFilled      <= (others => '0'); 
         sTxFIFONewDataOnDataOut    <= '0';
         sTxFIFOState               <= IDLE;

      elsif (rising_edge(Clock)) then

         --### Daten in FIFO eintragen ###
         --===============================
         --Traegt die Daten am TxFIFO Eingang in das TxFIFO Schieberegister ein
         ----------------------------------------------------------------------
         if (sTxFIFOGetDataFromDataIn_EdgeDet = '1' AND sTxFIFOArrayPosFilled(0) = '0') then
            sTxFIFOArrayPointer(0)(gTxFIFOMaxWidth-1 downto 0) <= TxFIFODataIn ;
            sTxFIFOArrayPosFilled(0)                           <= '1'; 
            sTxFIFOOverflow                                    <= '0';
            TxFIFODataFetchedFromDataIn                        <= '1';                          -- 1 Takt Pulse, dass Daten uebernommen wurden
         elsif (sTxFIFOGetDataFromDataIn_EdgeDet = '1' AND sTxFIFOFull = '1') then              -- TxFIFOOverflow Flag
            sTxFIFOOverflow               <= '1';
            TxFIFODataFetchedFromDataIn   <= '0';
         else
            TxFIFODataFetchedFromDataIn   <= '0';                                               -- 1 Takt Pulse, dass Daten uebernommen wurden
         end if;

         --### Daten schieben ###
         --======================
         --Schiebt die Daten im TxFFIFO mit jedem Takt eine Position weiter von links nach rechts
         ----------------------------------------------------------------------------------------
         for i in 1 to gTxFIFODepth - 1  loop
            if (sTxFIFOArrayPosFilled(i-1) = '1' AND sTxFIFOArrayPosFilled(i) = '0') then
               sTxFIFOArrayPointer(i)(gTxFIFOMaxWidth-1 downto 0) <= sTxFIFOArrayPointer(i-1)(gTxFIFOMaxWidth-1 downto 0); 
               sTxFIFOArrayPosFilled(i)                           <= '1'; 
               sTxFIFOArrayPosFilled(i-1)                         <= '0'; 
            end if;

         end loop;

         --### Ausgabe State Machine ###
         --=============================
         --diese Statemachine gibt die im FIFO befindlichen Daten an die nachfolgende Instanz aus
         ----------------------------------------------------------------------------------------
         case sTxFIFOState is

            when IDLE            => if (sTxFIFOArrayPosFilled(gTxFIFODepth - 1) = '1') then     --es ist noch mind. 1 Wert im FIFO
                                       
                                       if (TxFIFOTransmitterFreeInfo = '1') then                --Die nachfolgende Instanz (Sender) ist bereit
                                          TxFIFODataOut                             <= sTxFIFOArrayPointer(gTxFIFODepth - 1)(gTxFIFOMaxWidth-1 downto 0);
                                          sTxFIFOArrayPosFilled(gTxFIFODepth - 1)   <= '0';
                                          sTxFIFONewDataOnDataOut                   <= '1';
                                          sTxFIFOState                              <= COMMIT;
                                       end if;
                                    end if;

            when COMMIT          => if (TxFIFOTransmitterFreeInfo = '0') then       --warten dass die nachfolgende Instanz de Daten quittiert
                                       sTxFIFONewDataOnDataOut <= '0';
                                       sTxFIFOState            <= IDLE;
                                    end if;

            --sTxFIFONewDataOnDataOut = '1' sorgt dafuer, dass USITxRx den Sendewert abholt
            --Beim Abholen setzt USIRxTx TxFIFOTransmitterFreeInfo = '0', solange Senderegister nicht frei ist
            --D.h. sTxFIFOState muss solange im State COMMIT verbleiben bis USITxRx die Abholung des Wertes
            --mit TxFIFOTransmitterFreeInfo = '0' bestaetigt hat

         end case;

      end if;

   end process pTxFIFO;

   --### TxFIFOnEmpty Flag ###
   --=========================
   pTxFIFOnEmpty : process(Clock, Reset)
   begin
      if (Reset = '1') then
         TxFIFOnEmpty <= '0';
      elsif (rising_edge(Clock)) then
         if (sTxFIFOArrayPosFilled(gTxFIFODepth -1) = '1') then --FIFO nicht leer
            TxFIFOnEmpty <= '1';
         else
            TxFIFOnEmpty <= '0';
         end if;
      end if;
   end process pTxFIFOnEmpty;

   --### TxFIFOAlmostFull Flag (Voll -1) ###
   --=======================================
   pTxFIFOAlmostFull : process(Clock, Reset)
   begin
      if (Reset = '1') then
         TxFIFOAlmostFull <= '0';
      elsif (rising_edge(Clock)) then
         if (sTxFIFOArrayPosFilled(2) = '1' AND sTxFIFOArrayPosFilled(1) = '1' AND sTxFIFOArrayPosFilled(0) = '0') then
           TxFIFOAlmostFull <= '1';
         else
           TxFIFOAlmostFull <= '0';
         end if;
      end if;
   end process pTxFIFOAlmostFull;
   
   -- ### RxFIFOFull Flag ###
   --========================
   pTxFIFOFull : process(Clock, Reset)
   begin
      if (Reset = '1') then
         sTxFIFOFull <= '0';
      elsif (rising_edge(Clock)) then
         if (sTxFIFOArrayPosFilled(1) = '1' AND sTxFIFOArrayPosFilled(0) = '1')  then
            sTxFIFOFull <= '1';
         else
            sTxFIFOFull <= '0';
         end if;
      end if;
   end process pTxFIFOFull;

   TxFIFOFull              <= sTxFIFOFull;
   TxFIFOOverflow          <= sTxFIFOOverflow;
   TxFIFONewDataOnDataOut  <= sTxFIFONewDataOnDataOut;

end RTL;

