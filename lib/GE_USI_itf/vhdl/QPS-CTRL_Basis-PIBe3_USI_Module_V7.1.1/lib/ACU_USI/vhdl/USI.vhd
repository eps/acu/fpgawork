LIBRARY ieee;
USE ieee.std_logic_1164.all; 
use std.textio.all;
use ieee.numeric_std.all;

LIBRARY altera_mf;
USE altera_mf.altera_mf_components.all;


--History
--dd.mm.yy  born
--04.03.08  Tx8BitMode added (DS)
--05.03.08  Tx8BitMode removed
--          generic controlled bus width implemented (DS)
--07.03.08  bitorder while transmitting changed, now lsb will be tranfered first instead of msb (DS)
--dd.mm.yy  Code cleaned
--dd.mm.yy  sUSIRxTx_TxIdle and sUSIRxTx_RxIdle in module USIRxTx added,
--          furthermore these signals are inserted to ResponseRegister
--dd.mm.yy  Code cleaned
--dd.mm.yy  Baud/Bit Rate Selector : State Machine implemented
--          Baud/Bit Selector and Baud Rate Generator combined
--          Bugfix sBitRate
--          BitRateValid added
--dd.mm.yy  Code cleaned
--22.04.08  variables changed to signals in module USIRxTx.vhd (DS)
--          Array for edgedetection extended to 3 values
--          various timing errors cleaned -> accelerted timing behaviour
--29.05.08  Receive and Transmit state machine fitted with global seperated baud clocks
--          in exchange of individual counters in each step to increase/gain speed (DS)
--30.05.08  Bugfix for BitCenterDetection, sAdjustCenterBit implemented, 
--          instead of waitstates in module TxFIFO -> waiting for TxFIFOTransmitterFreeInfo signal, Baudrates changed (DS)
--05.06.08  Signal and varible names fitted to uniform names in all modules
--          Hi-Z outputs implemented when USI module isn't selected
--          Code cleaned   (DS)
--24.06.08  TxFIFONewDataOnDataOut, RxData_Tri, RxData and MergDistHandshake implemented (DS)
--09.07.08  EnableTriStatePorts implemented (DS)
--22.07.08  ControlStatus_Tri implemented (DS)
--04.08.08  ResetControl, ManualReset implemented (DS)
--dd.mm.yy  DiagVec set
--24.03.09  V2.16a renamed to V2.161 and RxDiagVec set (DS)
--6.04.09   TxFIFO hat Flags (TxFIFOOverflow, TxFIFOAlmostFull, TxFIFOFull) nicht geloescht beim Reset
--          DiagPin in USIRxTx entfernt und DiagVec in USIRxTxDiagVec umbenannt (DS)
--dd.mm.yy  Instanzen in TxFIFO, RxFIFO und USIRxTx benannt, dies vereinfacht die Simulation (07.05.10 - DS)
--10.08.10  'TxFIFODataFetchedFromDataIn' und 'RxFIFONewDataAtDataOutForMD' als neue Signale des TxFIFO, bzw. RxFIFO Moduls.
--          Diese 1 Takt Pulse dienen dazu die Instanzen 'TransmitData', bzw. 'ReceiveData' des mUSIC Moduls
--          darueber zu informieren, dass die Daten sicher in den TxFIFO eingetragen wurden, bzw. das die vom
--          RxFIFO gelieferten Daten nun gueltig sind. Die beiden neuen Informationen sind im 'ResponseRegister'
--          in den Bits 14 und 15 untergebracht. (DS)
--05.10.10 'RxEnableStatus' wurde nicht vom USIRxTx Modul nach aussen kommuniziert, sondern nur der 
--          Zustand des 'RxEnable' Signals auf das 'ControlStatus_Tri' gelegt. Beim ruecklesen
--          des 'ControlStatus_Tri' wurde folglich der Empfaengerstatus (EIN/AUS) nicht korrekt angezeigt,
--          sondern Bit(9) war nur '1' wenn 'RxEnableStatus' auf '1' war. Wird nun aus dem USIRxTx Modul
--          als Status zurueck geliefert. (DS)
--21.02.12  'GenBreak' eingefuehrt. Eine L -> H Flanke erzeugt einen 'Break' => die TxSerial Leitung wird fuer 150us auf Low gelegt.
--          Dies erkennt der Empfaenger und setzt die Baudrate auf 115,2kBaud Standardbaudrate zurueck. Dies ist notwendig,
--          wenn sich eine Empfaenger-USI NICHT im HighSpeed-Modus und NICHT in der Standardbaudrate befindet umd diese wieder auf 115,2kBaud
--          zur Neuinitialisierung zu setzen. (DS)
--07.08.12  'ControlStatus_Tri' korrigiert an neue USI_Select Positionen, 'RxDiagVec' auf 32 Bit erweitert und Debugging
--          dafuer aktiviert. USIRxTx ist nun V1.5 -> 'GenBreak' Dauer ist nicht 150.000 Zyklen sondern nur 15.000 Zyklen 
--          fuer 1,5us Dauer( - DS)
--14.08.12  'RxFIFO' in V.1.2 und 'TxFIFO' in V1.4 eingebaut. Die '..FIFOnEmpty' Flags wurden nie '0' wenn das oberste Bit im 
--          Array zwar wieder geloescht wurde, aber mind. noch ein weiteres Bit im Array gesetzt war, durch ein nachfolgend 
--          empf. Byte. (DS)
--15.10.12  Alternativ zu den Hardware FIFOs von RxSerial und TxSerial koennen nun auch RAM FIFOs verwendet werden.
--          Ports und Signale der einzelnen Module sind teilweise mit neuen Namen versehen um deren Funktion klarer zu machen (DS)
--10.12.12  'RxDiagVec' und 'USIRxTxDiagVec' entfernt (DS)
--27.08.13  Neue Instanz von USIRxTx eingebaut (DS)
--18.02.14  Meldung 'Ignored Power-Up Level option on the following registers' -> 'Critical Warning (18010): Register xxx will power up to GND/High'
--          durch gezieltes setzen eines Initwertes der betroffen Variable(n) beseitigt. (DS)
--28.09.15  Die RxSerial/TxSerial Buffer werden nun sobald 'SetBitRate_RxOnOff' = '1' geloescht. (DS)
--23.02.16  MergDistHandshake auf 4 Bit vergroessert: TxFIFOFull, RxFIFOnEmpty, TxFIFODataFetchedFromDataIn, RxFIFONewDataAtDataOut (DS)
--13.04.16  An den Eingaengen 'RxFIFOEnable' der RxFIFOs (RAM und HW) liegt anstelle 'USI_Enable' nun "sUSIEnableANDRxReceiverEnablesRxEnableStatus'. Dadurch
--          lassen die RxFIFOs sich nun auch mit dem 'sUSIRxTx_RxEnableStatus' Ausgangssignal des 'USIRxTx-Moduls' in Ruecksetzen.
--          Es muss lediglich der Receiver ueber das 'USI_Control_Register' (den 'USI_Control_PIO') im 'USIRxTx-Moduil' abgeschaltet werden.
--          Dies kann z.B. dann erforderlich sein, wenn 'RxOverflow' oder 'RxCollision' gesetzt wurde. 
--          USIRxTx Aenderung im Hinblick auf die Baudratenumschaltung (DS)
--11.06.19  Der Transmitter erhaelt ein zusaetzliches 'TxDriverEnable'. Wird immer '1' wenn Daten ueber 'TxSerial' verschickt werden.
--          Dafuer ist der 'DiagBaudClk' Port entfallen. (DS)
--16.09.21  Ports umbenannt, Generics fuer ..Version und einstellbare Anzahl von Transferbits entfernt (sind ohnehin immer 8) (DS)


ENTITY USI IS
   GENERIC
   (
      gUseRAMForRxFIFO  : integer := 1;
      gUseRAMForTxFIFO  : integer := 0;
      gRxFIFODepth      : integer := 10;
      gTxFIFODepth      : integer := 10;
      gWidth            : integer := 8
   );
   port
   (
      Clock                   :  in  std_logic;
      Reset                   :  in  std_logic;
      ManualReset             :  in  std_logic;                                  --Reset den die CPU ausfuehrt und der nur gilt, wenn USI_Select = 1

      USI_Select              :  in  std_logic;                                  --Wenn '1' kann Host Aenderungen an der USI Konfiguration vornehmen
      USI_Enable              :  in  std_logic;                                  --Wenn '1' ist diese USI aktiviert (Transmit, sofern eingeschaltet Receive)
      EnableTriStatePorts     :  in  std_logic;                                  --Die TriState Ausgaenge RxData_Tri und ResponseRegister_Tri werden aktiviert

      BitRate                 :  in  std_logic_vector(2 downto 0);
      SetBitRate_RxOnOff      :  in  std_logic;                                  --L -> H Baud/Bitraten, Receiver ein usw. uebernehmen

      GenBreak                :  in  std_logic;                                 -- L -> H Flanke loest den 150 us Breakpulse zum BaudratenReset aus

      TxGetDataFromTxData     :  in  std_logic;                                  --Pos. Flanke -> TxDatIn uebernehmen
      TxData                  :  in  std_logic_vector(gWidth-1 downto 0);

      RxEnable                :  in   std_logic;                                 --Hi -> Empfaenger aktiv
      RxPutDataToRxData       :  in   std_logic;                                 --Pos. Flanke -> RxData_Tri/RxData Daten anlegen  
      RxData_Tri              :  out  std_logic_vector(gWidth-1 downto 0);       --Tristatefaehig zur CPU
      RxData                  :  out  std_logic_vector(gWidth-1 downto 0);       --nicht Tristatefaehig zum Merger/Distributor

      ResponseRegister_Tri    :  out  std_logic_vector (15 downto 0);            --Zusammenfassung aller Registerflags
      MergDistHandshake       :  out  std_logic_vector (3 downto 0);             --Gibt TxFIFOFull, RxFIFOnEmpty, TxFIFODataFetchedFromDataIn, RxFIFONewDataAtDataOut gesondert fuer Merger/Distributor aus (nicht TriState faehig)
      ControlStatus_Tri       :  out  std_logic_vector (15 downto 0);            --Istzustand des USI_CTL_PIO

      TxDriverEnable          :  out  std_logic := '0';                          --wird immer '1' wenn ueber 'TxSerial' Daten verschickt werden
      RxSerial                :  in   std_logic;

      TxSerial                :  out  std_logic := '0'
);
END USI;

--===========================================================================

--ControlStatus_Tri
--Zeigt IST Zustaende an
-- xxxx|xxxx|xxxx|xxxx
-- |||| |||| |||| ||||
-- |||| |||| |||| |||\----|                     0x0000
-- |||| |||| |||| ||\-- bit_rate[2..0]          ...
-- |||| |||| |||| |\------|                     0x0007
-- |||| |||| |||| \------- USI_Select_0         0x0008
-- |||| |||| |||\--------- USI_Select_1         0x0010
-- |||| |||| ||\---------- USI_Select_2         0x0020
-- |||| |||| |\----------- USI_Select_3         0x0040
-- |||| |||| \------------ n.u.                 0x0080
-- |||| |||\-------------- TxGetDataFromTxData  0x0100 ACHTUNG-> Daten senden FLAG!!! Also nur kurz '1'!!
-- |||| ||\--------------- RxEnable             0x0200 (Receiver eingeschaltet wenn '1')
-- |||| |\---------------- RxPutDataToRxData    0x0400 (Daten von Receiver an CPU)
-- |||| \----------------- USIReset             0x0800
-- |||\------------------- n.u.                 0x1000
-- ||\-------------------- n.u.                 0x2000   
-- |\--------------------- GenBreak             0x4000
-- \---------------------- SetBitRate_RxOnOff   0x8000

--===========================================================================
ARCHITECTURE RTL OF USI IS 

--**************************************************************************
-- Komponenten fuer TxFIFO, entweder mit RAM Zellen oder mit Logikzellen
--**************************************************************************

   component TxRamFIFO
      generic 
      (
         gTxFIFODepth                  : integer;
         gTxFIFOMaxWidth               : integer
      );
      PORT
      (  Clock                         : in  std_logic;
         Reset                         : in  std_logic;
         TxFIFOEnable                  : in  std_logic;
         
         TxFIFODataIn                  : in  std_logic_vector(gWidth-1 downto 0);
         TxFIFOGetDataFromDataIn       : in  std_logic;         
         TxFIFODataFetchedFromDataIn   : out std_logic;  --Rueckmeldung, dass Daten an DataIn eingelesen wurden

         TxFIFOTransmitterFreeInfo     : in  std_logic;  --wird von USIRxTx gesetzt, wenn Senderegister frei ist und neue Daten vom FIFO angenommen werden koennen
         TxFIFODataOut                 : out std_logic_vector(gWidth-1 downto 0);    
         TxFIFONewDataOnDataOut        : out std_logic;  --muss TxFIFO setzen um USI-TxSerial zum senden zu veranlassen
      
         TxFIFOnEmpty                  : out std_logic;  --FIFO nicht leer
         TxFIFOAlmostFull              : out std_logic;  --nur noch ein Speicherplatz frei
         TxFIFOFull                    : out std_logic;  --FIFO voll    
         TxFIFOOverflow                : out std_logic   --Ueberlauf
   );
   end component;

   component TxFIFO
      generic 
      (
         gTxFIFODepth                  : integer;
         gTxFIFOMaxWidth               : integer
      );
      PORT
      (  Clock                         : in  std_logic;
         Reset                         : in  std_logic;
         TxFIFOEnable                  : in  std_logic;
         
         TxFIFODataIn                  : in  std_logic_vector(gWidth-1 downto 0);            
         TxFIFOGetDataFromDataIn       : in  std_logic;
         TxFIFODataFetchedFromDataIn   : out std_logic;  --Rueckmeldung, dass Daten an DataIn eingelesen wurden

         TxFIFOTransmitterFreeInfo     : in std_logic;   --wird von USIRxTx gesetzt, wenn Senderegister frei ist und neue Daten vom FIFO angenommen werden koennen
         TxFIFODataOut                 : out std_logic_vector(gWidth-1 downto 0);    
         TxFIFONewDataOnDataOut        : out std_logic;  --muss TxFIFO setzen um USI-TxSerial zum senden zu veranlassen
      
         TxFIFOnEmpty                  : out std_logic;  --FIFO nicht leer
         TxFIFOAlmostFull              : out std_logic;  --nur noch ein Speicherplatz frei
         TxFIFOFull                    : out std_logic;  --FIFO voll    
         TxFIFOOverflow                : out std_logic   --Ueberlauf
   );
   end component;

--**************************************************************************
-- Komponente fuer USIRxTx
--**************************************************************************

   component USIRxTx 
      generic 
      (
         gWidth      : integer
      );
      port 
      (
         Clock                   : in  std_logic;                          --Systemclock
         Reset                   : in  std_logic;                          --aktive High
         USI_Select              : in  std_logic;                          --aktive High
         SetBitRate_RxOnOff      : in  std_logic;                          --L -> H Baud/Bitraten, Receiver ein usw. uebernehmen

----------------------------------------------------------------
         BitRate                          : in  std_logic_vector(2 downto 0);                --8 versch. Geschwindigkeiten

         CurrentBitRate                   : out std_logic_vector(2 downto 0);                --gibt die aktuell eingestellte Baudrate aus fuer ControlStatus_Tri

         BitRateValid                     : out std_logic;                                   --High, wenn Aenderungen an Baud/Bitrate erfolgreich durchgefuehrt
----------------------------------------------------------------
         GenBreak                         : in std_logic;                                    --Low->HighFlanke loest den 150 us Breakpulse zum BaudratenReset aus
----------------------------------------------------------------
         TxData                           : in  std_logic_vector(gWidth-1 downto 0);         --Ausgaberegister
         TxGetDataFromTxDataAndTransmit   : in  std_logic;                                   --Transmit Enable
         TxTransmitterFreeInfo            : out std_logic;                                   --Daten gesendet, Ausgaberegister frei

         TxIdle                           : out std_logic;                                   --zeigt an, dass der Sender im Leerlauf ist
         RxIdle                           : out std_logic;                                   --zeigt an, dass der Empfaenger im Leerlauf ist

         TxFIFOnEmpty                     : in  std_logic;                                   --zeigt an, dass der TxFIFO nicht leer ist
         
         RxData                           : out std_logic_vector(gWidth-1 downto 0);         --Empfangsregister
         RxEnable                         : in  std_logic;                                   --Receive Enable
         RxEnableStatus                   : out std_logic;                                   --Receive Enable Status
         RxNewDataAvailOnRxData           : out std_logic;                                   --Daten empfangen, Receiveregister voll
         RxFramingError                   : out std_logic;                                   --Fehler beim Datenempfang (kein Stoppbit erkannt)
         RxCollision                      : out std_logic;                                   --Receive Collision
         RxDataFetchedFromRxData          : in  std_logic;                                   --Anforderung vom RxFIFO: kopiert das Empfangsregister in das 
                                                                                             --Ausgaberegister und setzt RxNewDataAvailOnRxData und/oder RxError zurueck
                                                                                             --und/oder RxError zurueck
         TxDriverEnable                   : out std_logic;                                   --wird immer '1' wenn ueber 'TxSerial' Daten verschickt werden

         RxSerial                         : in  std_logic;                                   --Serieller Eingang
         TxSerial                         : out std_logic                                    --Serieller Ausgang
      );
   end component;

--**************************************************************************
-- Komponenten fuer RxFIFO, entweder mit RAM Zellen oder mit Logikzellen
--**************************************************************************

   component RxFIFO
      generic 
      (
         gRxFIFODepth                  : integer;
         gRxFIFOMaxWidth               : integer
      );
      PORT
      (  Clock                         : in  std_logic;
         Reset                         : in  std_logic;   
         RxFIFOEnable                  : in  std_logic;
         
         RxFIFODataIn                  : in  std_logic_vector(gWidth-1 downto 0);
         RxFIFOGetDataFromDataIn       : in  std_logic;
         RxFIFODataFetchedFromDataIn   : out std_logic;
         
         RxFIFODataOut                 : out std_logic_vector(gWidth-1 downto 0);
         RxFIFOPutDataToDataOut        : in  std_logic;         
         RxFIFONewDataAtDataOutForMD   : out std_logic;

         RxFIFOnEmpty                  : out std_logic;  --FIFO nicht leer
         RxFIFOAlmostFull              : out std_logic;  --nur noch ein Speicherplatz frei
         RxFIFOFull                    : out std_logic;  --FIFO voll    
         RxFIFOOverflow                : out std_logic   --Ueberlauf
   );
   end component;

   component RxRamFIFO
      generic 
      (
         gRxFIFODepth                  : integer;
         gRxFIFOMaxWidth               : integer
      );
      PORT
      (  Clock                         : in  std_logic;
         Reset                         : in  std_logic;   
         RxFIFOEnable                  : in  std_logic;
         
         RxFIFODataIn                  : in  std_logic_vector(gWidth-1 downto 0);
         RxFIFOGetDataFromDataIn       : in  std_logic;
         RxFIFODataFetchedFromDataIn   : out std_logic;         

         RxFIFODataOut                 : out std_logic_vector(gWidth-1 downto 0);
         RxFIFOPutDataToDataOut        : in  std_logic;         
         RxFIFONewDataAtDataOutForMD   : out std_logic;

         RxFIFOnEmpty                  : out std_logic;  --FIFO nicht leer
         RxFIFOAlmostFull              : out std_logic;  --nur noch ein Speicherplatz frei
         RxFIFOFull                    : out std_logic;  --FIFO voll    
         RxFIFOOverflow                : out std_logic   --Ueberlauf
   );
   end component;


   --Hilfsignale

   signal sResetORManualReset                      : std_logic; -- := '0';
   signal sManualReset                             : std_logic;

   signal sUSIRxTxToTxFIFO_TxTransmitterFreeInfo            : std_logic :='0';                        --Transmitter frei Signal von USIRxTx nach TxFIFO
   signal sTxFIFOToUSIRxTx_TxData                           : std_logic_vector (gWidth-1 downto 0);   --Daten von TxFIFO nach USIRxTx
   signal sTxFIFOToUSIRxTx_TxGetDataFromTxDataAndTransmit   : std_logic :='0';                        --Neue Daten zum senden vorhanden von TxFIFO nach USIRxTx

   signal sRxFIFOToUSIRxTx_RxDataFetchedFromRxDataIn  : std_logic :='0';                        --Daten uebernommen Signal von RxFIFO nach USIRxTx
   signal sUSIRxTxToRxFIFO_RxData                     : std_logic_vector(gWidth-1 downto 0);    --Daten von USIRxTx nach RxFIFO
   
   signal sRxData                               : std_logic_vector(gWidth-1 downto 0);    --Datenausgabe vom RxFIFO an die Umwelt
  
   --USI ResponseRegister
   signal sUSIResponseRegisterTri               : std_logic_vector(15 downto 0);

   --USI ControlStatus
   signal sUSIControlStatusTri                  : std_logic_vector (15 downto 0);

   --Hilfsignale fuer USIControlState
   signal sCurrentBitRate                       : std_logic_vector (2 downto 0);

   --Gesondertes, nicht TriState faehiges TxFIFOFull, RxFIFOnEmpty, TxFIFODataFetchedFromDataIn, RxFIFONewDataAtDataOut fuer Merger/Distributor
   signal sMergDistHandshake                    : std_logic_vector (3 downto 0);
   
   signal sTxFIFOnEmpty                         : std_logic := '0';
   signal sTxFIFOAlmostFull                     : std_logic := '0';
   signal sTxFIFOFull                           : std_logic := '0';
   signal sTxFIFOOverflow                       : std_logic := '0';
   
   signal sRxFIFOnEmpty                         : std_logic := '0';
   signal sRxFIFOAlmostFull                     : std_logic := '0';
   signal sRxFIFOFull                           : std_logic := '0';
   signal sRxFIFOOverflow                       : std_logic := '0';
   
   signal sUSIRxTx_RxCollision                  : std_logic := '0';
   signal sUSIRxTx_RxFramingError               : std_logic := '0';
   signal sUSIRxTxToRxFIFO_NewDataAtRxData      : std_logic := '0';

   signal sUSIRxTx_TxIdle                       : std_logic := '1';
   signal sUSIRxTx_RxIdle                       : std_logic := '1';

   signal sUSIRxTX_BitRateValid                 : std_logic := '1';
   
   signal sTxFIFODataFetchedFromTxDataIn        : std_logic := '0';
   signal sRxFIFONewDataAtDataOut               : std_logic := '0';

   signal sUSIRxTx_RxEnableStatus               : std_logic := '0';

   signal sUSIEnableANDsRxEnableStatus          : std_logic;

   --Konstanten fuer ResponseRegister
   constant cTxFIFOnEmpty                       : integer := 0;   --0
   constant cTxFIFOAlmostFull                   : integer := 1;   --1
   constant cTxFIFOFull                         : integer := 2;   --2
   constant cTxFIFOOverflow                     : integer := 3;   --4
   
   constant cRxFIFOnEmpty                       : integer := 4;   --8
   constant cRxFIFOAlmostFull                   : integer := 5;   --16
   constant cRxFIFOFull                         : integer := 6;   --32
   constant cRxFIFOOverflow                     : integer := 7;   --64
   
   constant cUSIRxTx_RxCollision                : integer := 8;   --128
   constant cUSIRxTx_RxFramingError             : integer := 9;   --256
   constant cUSIRxTx_NewDataAtRxData            : integer := 10;  --512    
   constant cUSIRxTx_TxIdle                     : integer := 11;  --1024
   constant cUSIRxTx_RxIdle                     : integer := 12;  --2048
   constant cUSIRxTx_BitRateValid               : integer := 13;  --4096

   constant cTxFIFODataFetchedFromTxDataIn      : integer := 14;  --8192
   constant cRxFIFONewDataAtDataOut             : integer := 15;  --16384

   -- USI_RESPONSE_PIO
   -- xxxx|xxxx|xxxx|xxxx
   -- |||| |||| |||| ||||
   -- |||| |||| |||| |||\----TxFIFOnEmpty                      #define USI_TX_FIFOnEmpty                 0x0001   --Tx..FIFO
   -- |||| |||| |||| ||\-----TxFIFOAlmostFull                  #define USI_TX_FIFOAlmostFull             0x0002   --Tx..FIFO
   -- |||| |||| |||| |\------TxFIFOFull                        #define USI_TX_FIFOFull                   0x0004   --Tx..FIFO
   -- |||| |||| |||| \-------TxFIFOOverflow                    #define USI_TX_FIFOOverflow               0x0008   --Tx..FIFO
   -- |||| |||| |||\---------RxFIFOnEmpty                      #define USI_RX_FIFOnEmpty                 0x0010   --Rx..FIFO  -> ISR Trigger
   -- |||| |||| ||\----------RxFIFOAlmostFull                  #define USI_RX_FIFOAlmostFull             0x0020   --Rx..FIFO
   -- |||| |||| |\-----------RxFIFOFull                        #define USI_RX_FIFOFull                   0x0040   --Rx..FIFO
   -- |||| |||| \------------RxFIFOOverflow                    #define USI_RX_FIFOOverflow               0x0080   --Rx..FIFO
   -- |||| |||\--------------USIRxTx_RxCollision               #define USI_RXTX_RX_Collision             0x0100   --USIRxTx
   -- |||| ||\---------------USIRxTx_RxFramingError            #define USI_RXTX_RX_FramingError          0x0200   --USIRxTx
   -- |||| |\----------------USIRxTx_NewDataAtRxData           #define USI_RXTX_NewDataAtRxData          0x0400   --USIRxTx   ->(intern)-> RxFIFO
   -- |||| \-----------------USIRxTx_TxIdle                    #define USI_RXTX_TX_IDLE                  0x0800   --USIRxTx
   -- |||\-------------------USIRxTx_RxIdle                    #define USI_RXTX_RX_IDLE                  0x1000   --USIRxTx
   -- ||\--------------------USIRxTx_BitRateValid              #define USI_RXTX_BitRateValid             0x2000   --USIRxTx
   -- |\---------------------TxFIFODataFetchedFromDataIn       #define USI_TX_FIFODataFetchedFromDataIn  0x4000   --Tx..FIFO
   -- \----------------------RxFIFONewDataAtDataOut            #define USI_RX_FIFONewDataAtDataOut       0x8000   --Rx..FIFO


   -- MergDistHandshake
   -- xxx
   -- ||||
   -- |||\-----TxFIFOFull
   -- ||\------RxFIFOnEmpty
   -- |\-------TxFIFODataFetchedFromTxDataIn
   -- \--------RxFIFONewDataAtDataOut

begin 

   assert NOT(gWidth > 32)
      report " gWidth kann maximal 32 sein!"
   severity ERROR;

--------------------------------------------------------------------
---  RESPONSEREGISTER   
--------------------------------------------------------------------

   USIResponseRegister: process(Clock, sResetORManualReset)
   begin    
      if (sResetORManualReset = '1') then
         sUSIResponseRegisterTri <= (others => '0');
         sMergDistHandshake   <= (others => '0');
      elsif (Clock'event and Clock ='1') then

         --TxFIFOnEmpty (0)
         --Der Tx FIFO ist NICHT leer.
         sUSIResponseRegisterTri(cTxFIFOnEmpty)                      <= sTxFIFOnEmpty;

         --TxFIFOAlmostFull (1)
         --Der Tx FIFO ist FAST voll.
         sUSIResponseRegisterTri(cTxFIFOAlmostFull)                  <= sTxFIFOAlmostFull;

         --TxFIFOFull (2)
         --Der Tx FIFO IST voll.
         sUSIResponseRegisterTri(cTxFIFOFull)                        <= sTxFIFOFull;
         sMergDistHandshake(0)                                       <= sTxFIFOFull;

         --TxFIFOOverflow (3)
         --Der Tx FIFO ist UEBERGELAUFEN.
         sUSIResponseRegisterTri(cTxFIFOOverflow)                    <= sTxFIFOOverflow;

         --RxFIFOnEmpty, wird als IRQ im uController benutzt (4)
         --Der Rx FIFO ist NICHT leer.
         sUSIResponseRegisterTri(cRxFIFOnEmpty)                      <= sRxFIFOnEmpty;
         sMergDistHandshake(1)                                       <= sRxFIFOnEmpty;

         --RxFIFOAlmostFull (5)
         --Der Rx FIFO ist FAST voll.
         sUSIResponseRegisterTri(cRxFIFOAlmostFull)                  <= sRxFIFOAlmostFull;

         --RxFIFOFull (6)
         --Der Rx FIFO ist VOLL.
         sUSIResponseRegisterTri(cRxFIFOFull)                        <= sRxFIFOFull;

         --RxFIFOOverflow (7)
         --Der Rx FIFO ist UEBERGELAUFEN.
         sUSIResponseRegisterTri(cRxFIFOOverflow)                    <= sRxFIFOOverflow;

         --RxCollision (8)
         --Der empf. Wert (t-1) wurde NICHT rechtzeitig vor eintreffen des Wertes (t) vom RxFifo aus USIRxTx angeholt. 
         sUSIResponseRegisterTri(cUSIRxTx_RxCollision)               <= sUSIRxTx_RxCollision;

         --RxFramingError (9)
         --Ein Stoppbit wurde NICHT erkannt.
         sUSIResponseRegisterTri(cUSIRxTx_RxFramingError)            <= sUSIRxTx_RxFramingError;

         --RxNewDataAvailOnRxData (10)
         --USIRxTx hat neue Daten für den RxFifo zu Abholung bereit.
         sUSIResponseRegisterTri(cUSIRxTx_NewDataAtRxData)           <= sUSIRxTxToRxFIFO_NewDataAtRxData;

         --Transmitter im Leerlauf (11)
         --Der Tx ist im LEERLAUF.
         sUSIResponseRegisterTri(cUSIRxTx_TxIdle)                    <= sUSIRxTx_TxIdle;

         --Receiver im Leerlauf (12)
         --Der Rx ist im LEERLAUF.
         sUSIResponseRegisterTri(cUSIRxTx_RxIdle)                    <= sUSIRxTx_RxIdle;

         --Bitrate gueltig (13)
         --Die Bitrate wird gerade gewechselt und die neue ist NOCH NICHT abschliessend eingerichtet.
         sUSIResponseRegisterTri(cUSIRxTx_BitRateValid)              <= sUSIRxTX_BitRateValid;

         --TxFIFO hat die angelegten Daten uebernommen (14)
         --Der Tx Fifo hat die Daten am USI Modul Eingang TxData uebernommen.
         sUSIResponseRegisterTri(cTxFIFODataFetchedFromTxDataIn)     <= sTxFIFODataFetchedFromTxDataIn;
         sMergDistHandshake(2)                                       <= sTxFIFODataFetchedFromTxDataIn;

         --RxFIFO hat gueltige Daten am USI Modul Ausgang 'RxData/RxData_Tri' (15)
         sUSIResponseRegisterTri(cRxFIFONewDataAtDataOut)            <= sRxFIFONewDataAtDataOut;
         sMergDistHandshake(3)                                       <= sRxFIFONewDataAtDataOut;

      end if;

   end process USIResponseRegister;

--------------------------------------------------------------------
---  CONTROLSTATUS
--   Dieser Port informiert die Nios2 CPU ueber den aktuellen Status der USI.
--   Der Port ist als TriState ausgefuehrt und wird immer dann zugeschaltet,
--   wenn das USI Modul mittels 'USI_Enable' ODER 'USI_Select' UND 'EnableTriStatePorts'
--   selektiert wird.
--------------------------------------------------------------------

   USIControlStatus: process(Clock, sResetORManualReset)
   begin

      if (sResetORManualReset = '1') then
         sUSIControlStatusTri  <= (others => '0');
      elsif (Clock'event and Clock ='1') then

         sUSIControlStatusTri(2 downto 0)    <= sCurrentBitRate(2 downto 0);                       --BitRate
         sUSIControlStatusTri(3)             <= USI_Select;                                        --USI_Select_0;
         sUSIControlStatusTri(4)             <= USI_Select;                                        --USI_Select_1;
         sUSIControlStatusTri(5)             <= USI_Select;                                        --USI_Select_2;
         sUSIControlStatusTri(6)             <= USI_Select;                                        --USI_Select_3;
         sUSIControlStatusTri(7)             <= '0';                                               --n.u.
         sUSIControlStatusTri(8)             <= TxGetDataFromTxData;                               --TxGetDataFromTxData
         sUSIControlStatusTri(9)             <= sUSIRxTx_RxEnableStatus;                           --USIRxTX_RxEnableStatus (EIN'1'/AUS'0')
         sUSIControlStatusTri(10)            <= RxPutDataToRxData;                                 --RxPutDataToRxData
         sUSIControlStatusTri(11)            <= '0';                                               --n.u. (Reset wuerde ohnehin nicht angezeigt)
         sUSIControlStatusTri(12)            <= '0';                                               --n.u.
         sUSIControlStatusTri(13)            <= '0';                                               --n.u.
         sUSIControlStatusTri(14)            <= GenBreak;                                          --GenBreak
         sUSIControlStatusTri(15)            <= SetBitRate_RxOnOff;                                --SetBitRate_RxOnOff

      end if;

   end process USIControlStatus;

--------------------------------------------------------------------
--Diese Ausgaben sind nicht TriState faehig und gehen zum Merger/Distributor
--------------------------------------------------------------------

   MergDistHandshake <= sMergDistHandshake;
   RxData             <= sRxData;

--------------------------------------------------------------------
--Diese Ausgabe ist TriState faehig und geht an die CPU
--------------------------------------------------------------------

   OutputMapping: process(Clock, sResetORManualReset)
   begin    
      if (sResetORManualReset = '1') then
         RxData_Tri            <= (others =>'Z');              --Hi-Z wenn USI nicht selektiert
         ResponseRegister_Tri  <= (others =>'Z');
         ControlStatus_Tri     <= (others =>'Z');
      elsif (Clock'event and Clock ='1') then
         if ( (USI_Enable = '1' OR USI_Select = '1') AND EnableTriStatePorts = '1') then   --nur Ausgabe wenn USI selektiert oder aktiviert, sonst Hi-Z
            RxData_Tri            <= sRxData;
            ResponseRegister_Tri  <= sUSIResponseRegisterTri;
            ControlStatus_Tri     <= sUSIControlStatusTri;
         else
            RxData_Tri            <= (others =>'Z');           --Hi-Z wenn USI nicht selektiert
            ResponseRegister_Tri  <= (others =>'Z');
            ControlStatus_Tri     <= (others =>'Z');
         end if;
      end if;
   end process OutputMapping;

--------------------------------------------------------------------
--Reset Kontrolle
--------------------------------------------------------------------
   pManualReset: process(Clock, Reset)
   begin
      if (Reset = '1') then
         sManualReset <= '1';
      elsif (Clock'event AND Clock ='1') then
         if (USI_Select = '1' AND ManualReset = '1') then
            sManualReset <= '1';
         else
            sManualReset <= '0';
         end if;
      end if;
   end process pManualReset;

   --nachfolgendes noetig, weil Modelsim "Reset => sReset OR sManualReset" als direktes Portmapping nicht mag
   sResetORManualReset <= Reset OR sManualReset;

--------------------------------------------------------------------
--Transmit FIFO
--------------------------------------------------------------------
TX_With_RAM : if (gUseRAMForTxFIFO = 1) generate

   b2v_TxRamFIFO : TxRamFIFO
   GENERIC MAP
   (
      gTxFIFODepth                  => gTxFIFODepth,
      gTxFIFOMaxWidth               => gWidth
   )
   PORT MAP
   (
      Clock                         => Clock,
      Reset                         => sResetORManualReset,
      TxFIFOEnable                  => USI_Enable,
      TxFIFOGetDataFromDataIn       => TxGetDataFromTxData,
      TxFIFODataIn                  => TxData,
      TxFIFODataFetchedFromDataIn   => sTxFIFODataFetchedFromTxDataIn,                    --out
      TxFIFOTransmitterFreeInfo     => sUSIRxTxToTxFIFO_TxTransmitterFreeInfo,
      TxFIFODataOut                 => sTxFIFOToUSIRxTx_TxData,                           --out
      TxFIFONewDataOnDataOut        => sTxFIFOToUSIRxTx_TxGetDataFromTxDataAndTransmit,   --out
      TxFIFOnEmpty                  => sTxFIFOnEmpty,                                     --out
      TxFIFOAlmostFull              => sTxFIFOAlmostFull,                                 --out
      TxFIFOFull                    => sTxFIFOFull,                                       --out
      TxFIFOOverflow                => sTxFIFOOverflow                                    --out
   );
end generate;

TX_Without_RAM : if (gUseRAMForTxFIFO = 0) generate

   b2v_TxFIFO : TxFIFO
   GENERIC MAP
   (
      gTxFIFODepth                  => gTxFIFODepth,
      gTxFIFOMaxWidth               => gWidth
   )
   PORT MAP
   (
      Clock                         => Clock,
      Reset                         => sResetORManualReset,
      TxFIFOEnable                  => USI_Enable,
      TxFIFOGetDataFromDataIn       => TxGetDataFromTxData,
      TxFIFODataIn                  => TxData,
      TxFIFODataFetchedFromDataIn   => sTxFIFODataFetchedFromTxDataIn,                    --out
      TxFIFOTransmitterFreeInfo     => sUSIRxTxToTxFIFO_TxTransmitterFreeInfo,
      TxFIFODataOut                 => sTxFIFOToUSIRxTx_TxData,                           --out
      TxFIFONewDataOnDataOut        => sTxFIFOToUSIRxTx_TxGetDataFromTxDataAndTransmit,   --out
      TxFIFOnEmpty                  => sTxFIFOnEmpty,                                     --out
      TxFIFOAlmostFull              => sTxFIFOAlmostFull,                                 --out
      TxFIFOFull                    => sTxFIFOFull,                                       --out
      TxFIFOOverflow                => sTxFIFOOverflow                                    --out
   );
end generate;

--------------------------------------------------------------------
--Sende-/Empfangsbaustein
--------------------------------------------------------------------

   b2v_USIRxTx : USIRxTx
   GENERIC MAP
   (
      gWidth                           => gWidth
   )
   PORT MAP
   (
      Clock                            => Clock,
      Reset                            => sResetORManualReset,
      USI_Select                       => USI_Select,
      SetBitRate_RxOnOff               => SetBitRate_RxOnOff,

      BitRate                          => BitRate,
      CurrentBitRate                   => sCurrentBitRate,        --out
      BitRateValid                     => sUSIRxTX_BitRateValid,  --out

      GenBreak                         => GenBreak,

      TxData                           => sTxFIFOToUSIRxTx_TxData,
      TxGetDataFromTxDataAndTransmit   => sTxFIFOToUSIRxTx_TxGetDataFromTxDataAndTransmit,
      TxTransmitterFreeInfo            => sUSIRxTxToTxFIFO_TxTransmitterFreeInfo,   --out

      TxFIFOnEmpty                     => sTxFIFOnEmpty,

      TxIdle                           => sUSIRxTx_TxIdle,                    --out
      RxIdle                           => sUSIRxTx_RxIdle,                    --out

      RxData                           => sUSIRxTxToRxFIFO_RxData,            --out
      RxEnable                         => RxEnable,
      RxEnableStatus                   => sUSIRxTx_RxEnableStatus,            --out
      RxNewDataAvailOnRxData           => sUSIRxTxToRxFIFO_NewDataAtRxData,   --out
      RxFramingError                   => sUSIRxTx_RxFramingError,            --out
      RxCollision                      => sUSIRxTx_RxCollision,               --out
      RxDataFetchedFromRxData          => sRxFIFOToUSIRxTx_RxDataFetchedFromRxDataIn,

      TxDriverEnable                   => TxDriverEnable,         --out

      RxSerial                         => RxSerial,
      TxSerial                         => TxSerial                --out
   );


--------------------------------------------------------------------
--Receive FIFO
--------------------------------------------------------------------

   --nachfolgendes noetig, weil Modelsim "RxFIFOEnable => sUSIRxTx_RxEnableStatus AND USI_Enable" als direktes Portmapping nicht mag
   sUSIEnableANDsRxEnableStatus <= sUSIRxTx_RxEnableStatus AND USI_Enable;

RX_With_RAM : if (gUseRAMForRxFIFO = 1) generate

   b2v_RxRamFIFO : RxRamFIFO
   GENERIC MAP
   (
      gRxFIFODepth                  => gRxFIFODepth,
      gRxFIFOMaxWidth               => gWidth
   )
   PORT MAP
   (
      Clock                         => Clock,
      Reset                         => sResetORManualReset,
      RxFIFOEnable                  => sUSIEnableANDsRxEnableStatus,
      RxFIFOGetDataFromDataIn       => sUSIRxTxToRxFIFO_NewDataAtRxData,
      RxFIFONewDataAtDataOutForMD   => sRxFIFONewDataAtDataOut,      --out
      RxFIFODataFetchedFromDataIn   => sRxFIFOToUSIRxTx_RxDataFetchedFromRxDataIn, --out
      RxFIFODataIn                  => sUSIRxTxToRxFIFO_RxData,
      RxFIFOPutDataToDataOut        => RxPutDataToRxData,
      RxFIFODataOut                 => sRxData,                      --out
      RxFIFOnEmpty                  => sRxFIFOnEmpty,                --out
      RxFIFOAlmostFull              => sRxFIFOAlmostFull,            --out
      RxFIFOFull                    => sRxFIFOFull,                  --out
      RxFIFOOverflow                => sRxFIFOOverflow               --out
   );
end generate;

RX_Without_RAM : if (gUseRAMForRxFIFO = 0) generate

   b2v_RxFIFO : RxFIFO
   GENERIC MAP
   (
      gRxFIFODepth                  => gRxFIFODepth,
      gRxFIFOMaxWidth               => gWidth
   )
   PORT MAP
   (
      Clock                         => Clock,
      Reset                         => sResetORManualReset,
      RxFIFOEnable                  => sUSIEnableANDsRxEnableStatus,
      RxFIFOGetDataFromDataIn       => sUSIRxTxToRxFIFO_NewDataAtRxData,
      RxFIFONewDataAtDataOutForMD   => sRxFIFONewDataAtDataOut,      --out
      RxFIFODataFetchedFromDataIn   => sRxFIFOToUSIRxTx_RxDataFetchedFromRxDataIn, --out
      RxFIFODataIn                  => sUSIRxTxToRxFIFO_RxData,
      RxFIFOPutDataToDataOut        => RxPutDataToRxData,
      RxFIFODataOut                 => sRxData,                      --out
      RxFIFOnEmpty                  => sRxFIFOnEmpty,                --out
      RxFIFOAlmostFull              => sRxFIFOAlmostFull,            --out
      RxFIFOFull                    => sRxFIFOFull,                  --out
      RxFIFOOverflow                => sRxFIFOOverflow               --out
   );
end generate;
END; 
