LIBRARY ieee;
USE ieee.std_logic_1164.all;
use IEEE.MATH_REAL.ALL;
use ieee.numeric_std.all;

LIBRARY altera_mf;
USE altera_mf.altera_mf_components.all;

--V1.0   born (15.10.12 - DS)
--V2.0   Unoetige Staemachine durch if->else erstetzt, dadruch Code etwas gestrafft (14.04.16 -DS)
--V2.1   'sTxFIFOStateWrite' konnte entfallen, wurde ohnehin nicht benuztzt (01.07.16 - DS)

entity TxRamFIFO is
   generic 
   (
      gTxRamFIFOVersion             : real      := 2.00;
      gTxFIFODepth                  : integer   := 32;
      gTxFIFOMaxWidth               : integer   := 8 
   );
   PORT
   (
      Clock                         : in  std_logic;
      Reset                         : in  std_logic;
      TxFIFOEnable                  : in  std_logic;

      TxFIFOGetDataFromDataIn       : in  std_logic;                                      --L->H-Flanke uebernimmt die Daten am TxFIFODataIn[] Port
      TxFIFODataIn                  : in  std_logic_vector(gTxFIFOMaxWidth-1 downto 0);   --Daten die im FIFO von der CPU oder Hardware zum senden zwischengespeichert werden sollen
      TxFIFODataFetchedFromDataIn   : out std_logic;                                      --1 Takt Pulse, dass Daten von TxFIFODataIn[] in den TxRamFIFO uebernommen wurden

      TxFIFOTransmitterFreeInfo     : in  std_logic;                                      --'1' wenn das USI Senderegister von USIRxTx frei ist und der TxRamFIFO neue Daten liefern kann      
      TxFIFONewDataOnDataOut        : out std_logic;                                      --'1' sorgt dafuer, dass USITxRx den Sendewert an TxFIFODataOut[] abholt
      
      TxFIFODataOut                 : out std_logic_vector(gTxFIFOMaxWidth-1 downto 0);   --Daten die aus dem FIFO an USIRxTx wieder ausgegeben werden      
   
      TxFIFOnEmpty                  : out std_logic;  --FIFO nicht leer                   (-> almost_empty)
      TxFIFOAlmostFull              : out std_logic;  --nur noch ein Speicherplatz frei   (-> almost_full)
      TxFIFOFull                    : out std_logic;  --FIFO voll                         (-> full)
      TxFIFOOverflow                : out std_logic   --Ueberlauf                         (-> nicht vorhanden)
   );

end TxRamFIFO;


--Beschreibung
--============

--                    Daten --> TxFIFO --> USIRxTx --> USI Kabel
--                                 |
--  TxFIFODataFetchedFromDataIn <--/

--Daten werden von CPU u.a.(Host) in den TxFIFO eingetragen durch:
--Host legt Daten an 'TxFIFODataIn' an und wird mit pos. Flanke an 'TxFIFOGetDataFromDataIn' in FIFO uebernommen.

--USIRxTx legt 'TxFIFOTransmitterFreeInfo = '1'' wenn dessen Senderegister frei ist,
--wenn noch Daten in TxFIFO legt TxFIFO Daten an TxFIFODataOut an und setzt 'TxFIFONewDataOnDataOut' auf '1' -> '0'

--'TxFIFODataFetchedFromDataIn' wird 1 Takt Puls high, wenn die Daten in den FIFO eingelesen sind.
--Dies dient der Ruekmeldung, dass die fuellende Instanz (z.B. mUSIC 'TransmitData') die Daten sicher abgeliefert hat.

architecture RTL of TxRamFIFO is

   component scfifo
   generic (
      lpm_width               : natural := gTxFIFOMaxWidth;                            --Breite von 'data[]' und 'q[]'
      lpm_widthu              : natural := natural(ceil(log2(real(gTxFIFODepth))));    --Breite von 'usedw[]'
      lpm_numwords            : natural := gTxFIFODepth;                               --Tiefe des FIFO, mind. 4
      lpm_showahead           : string := "OFF";            --autom. Voranzeige des ersten FIFO Eintrags ohne 'rdreq'
      lpm_type                : string := "scfifo";         --SingleClockFIFO
      overflow_checking       : string := "ON";             --Deaktiviert den Port 'wrreq' wenn FIFO voll
      underflow_checking      : string := "ON";             --Deaktiviert den Port 'rdreq' wenn FIFO leer
      use_eab                 : string := "ON";             --Wenn "ON" wird RAM, sonst Logik fuer den FIFO-Speicher verwendet
      add_ram_output_register : string := "OFF";            --obsolet bei CyclonII und neuer
      almost_full_value       : natural := gTxFIFODepth-2;  --gespeicherte Werte >= 'almost_full_value' -> 'almost_full'-Port => '1'
      almost_empty_value      : natural := 2;               --gespeicherte Werte <  'almost_empty_value' -> 'almost_empty'-Port => '1'
      allow_rwcycle_when_full : string := "OFF";            --erlaubt kombinierte Lese-/Schreibzyklen bei vollem FIFO
      intended_device_family  : string := "CYCLONE III";    --nur fuer Simulation
      lpm_hint                : string := "UNUSED"          --nur fuer Simulation
   );
   port(
      clock                   : in  std_logic;                                --System Clock
      aclr                    : in  std_logic := '0';                         --Asynchron Reset
      sclr                    : in  std_logic := '0';                         --Synchron Reset
      data                    : in  std_logic_vector(lpm_width-1 downto 0);   --Daten Eingang
      wrreq                   : in  std_logic;                                --Daten von 'data[]' uebernehmen
      rdreq                   : in  std_logic;                                --Daten an q[] ausgeben
      q                       : out std_logic_vector(lpm_width-1 downto 0);   --Daten Ausgang
      empty                   : out std_logic;                                --FIFO ist leer
      almost_empty            : out std_logic;                                --'1' wenn 'usedw' <  'almost_empty_value'
      almost_full             : out std_logic;                                --'1' wenn 'usedw' >= 'almost_full_value'
      full                    : out std_logic;                                --'1' wenn 'usedw' == 'gTxFIFODepth'
      usedw                   : out std_logic_vector(lpm_widthu-1 downto 0)   --Gesamtzahl der im FIFO befindlicher Eintraege
   );
   end component;

   type     tTxFIFOState  is (IDLE, COMMIT);
   signal   sTxFIFOStateRead   : tTxFIFOState;

   signal   sTxFIFOGetDataFromDataIn_EdgeDet  : std_logic := '0';
   signal   sTxFIFOWriteData           : std_logic := '0';
   signal   sTxFIFOReadData            : std_logic := '0';
   signal   sTxFIFOEmpty               : std_logic := '0';

   signal   sTxFIFOFull                : std_logic := '0';

begin

   TxRamFIFO: scfifo
   port map
   (
      clock          => Clock,                     --System Clock
      aclr           => Reset,                     --Asynchron Reset
      sclr           => '0',                       --Synchron Reset
      data           => TxFIFODataIn,              --Daten Eingang
      wrreq          => sTxFIFOWriteData,          --Daten von 'data[]' uebernehmen
      rdreq          => sTxFIFOReadData,           --Daten an q[] ausgeben
      q              => TxFIFODataOut,             --Daten Ausgang
      empty          => sTxFIFOEmpty,              --'1' wenn FIFO leer ist
      almost_empty   => open, --sTxFIFOAlmostEmpty,  --'1' wenn 'usedw' <  'almost_empty_value'
      almost_full    => TxFIFOAlmostFull,          --'1' wenn 'usedw' >= 'almost_full_value'
      full           => sTxFIFOFull,               --'1' wenn 'usedw' == 'gRxFIFODepth'
      usedw          => open --sUsedFIFOWords       --Gesamtzahl der im FIFO befindlicher Eintraege
   );

   --#########################################################################################
   -- pos. Flanke an TxFIFOGetDataFromDataIn (ext. Signal) erkennen, sofern TxFIFOEnable = '1'
   -- CPU oder HW legt Daten an TxFIFODataIn, pos. Flanke uebernimmt diese in den FIFO
   --#########################################################################################

   pWriteEnableEdgeDet: process (Clock, Reset)
      variable vTxFIFOGetDataFromDataIn_EdgeDetArray : std_logic_vector(1 downto 0);
   begin
      if (Reset = '1') then
         vTxFIFOGetDataFromDataIn_EdgeDetArray := (others => '0') ;
      elsif (Clock'event and Clock = '1') then
         if (TxFIFOEnable = '1') then
            vTxFIFOGetDataFromDataIn_EdgeDetArray := (vTxFIFOGetDataFromDataIn_EdgeDetArray(0) & TxFIFOGetDataFromDataIn ) ;
            if (vTxFIFOGetDataFromDataIn_EdgeDetArray = "01") then --steigende Flanke
               sTxFIFOGetDataFromDataIn_EdgeDet <= '1' ;
            else
               sTxFIFOGetDataFromDataIn_EdgeDet <= '0' ;
            end if ;
         end if;
     end if ;    
   end process pWriteEnableEdgeDet; 

   --#########################################################################################
   -- FIFO
   --#########################################################################################

   pTxRamFIFO : process (Clock, Reset)
   begin

      if (Reset = '1') then

         TxFIFODataFetchedFromDataIn   <= '0';
         TxFIFONewDataOnDataOut        <= '0';
         TxFIFOOverflow                <= '0';
         sTxFIFOWriteData              <= '0';
         sTxFIFOStateRead              <= IDLE;

      elsif (Clock'event AND Clock = '1') then

         --#########################################################################################
         -- Daten einlesen
         -- ==============
         -- Kommt das Signal 'sTxFIFOGetDataFromDataIn_EdgeDet = '1'' und ist der FIFO nicht voll, werden die Eingangsdaten
         -- uebernommen und in den FIFO geschrieben. Dies wird mit einem Puls an 'TxFIFODataFetchedFromDataIn' signalisiert.
         --#########################################################################################

         sTxFIFOWriteData              <= '0';              --Daten vom Eingang im FIFO speichern
         TxFIFODataFetchedFromDataIn   <= '0';
      
         if (sTxFIFOGetDataFromDataIn_EdgeDet = '1') then   --neue Daten am Eingang
            if (sTxFIFOFull = '0') then                     --FIFO nicht voll
               sTxFIFOWriteData              <= '1';        --Daten vom Eingang im FIFO speichern
               TxFIFOOverflow                <= '0';        --Overflow Info loeschen
               TxFIFODataFetchedFromDataIn   <= '1';        --Daten gelesen und gespeichert anzeigen
             else
               TxFIFOOverflow    <= '1';                    --FIFO uberfuellt
            end if;
         end if;

         --#########################################################################################
         -- Daten ausgeben (an USIRxTx)
         -- =================================
         --USIRxTx legt 'TxFIFOTransmitterFreeInfo' = '1'', wenn dessen Senderegister frei zum senden neuer Daten ist.
         --Ist 'sTxFIFOEmpty = '0'', also nicht leer, wird 'sTxFIFOReadData = '1'', wodurch der FIFO
         --einen Wert aus gibt. Dies wird USIRxTx mit einem 'TxFIFONewDataOnDataOut = '1'' signalisiert.
         --Beim Abholen setzt USIRxTx 'TxFIFOTransmitterFreeInfo = '0'', solange Senderegister nicht frei ist
         --D.h. 'sTxFIFOStateRead' muss solange im State COMMIT verbleiben bis USITxRx die Abholung des Wertes
         --mit 'TxFIFOTransmitterFreeInfo = '1'' bestaetigt hat
         
         --#########################################################################################

         case (sTxFIFOStateRead) is

            ---------------------------------------------------------------------
            when IDLE =>      

               TxFIFONewDataOnDataOut  <= '0';        --wenn '1' -> veranlasst USITxRx den Sendewert an TxFIFODataOut[] abzuholen

               if ((TxFIFOTransmitterFreeInfo = '1') and (sTxFIFOEmpty = '0')) then    --Sender ist bereit und FIFO ist nicht leer
                  sTxFIFOReadData         <= '1';                                      --FIFO zur Datenausgabe veranlassen
                  TxFIFONewDataOnDataOut  <= '0';                                      
                  sTxFIFOStateRead        <= COMMIT;
               else
                  sTxFIFOReadData         <= '0';
                  
                  sTxFIFOStateRead        <= IDLE;
               end if;

             ---------------------------------------------------------------------
             when COMMIT =>   

               sTxFIFOReadData         <= '0';
               TxFIFONewDataOnDataOut  <= '1';           --um eine Takt verzoegert, weil die Ausgabe der Daten aus dem RAM auch eine Takt benoetigen

               if (TxFIFOTransmitterFreeInfo = '0') then
                  TxFIFONewDataOnDataOut  <= '0';
                  sTxFIFOStateRead        <= IDLE;
               end if;

         end case;

      end if;

   end process pTxRamFIFO;

   TxFIFOnEmpty   <= not sTxFIFOEmpty;
   TxFIFOFull     <= sTxFIFOFull;

end RTL;
