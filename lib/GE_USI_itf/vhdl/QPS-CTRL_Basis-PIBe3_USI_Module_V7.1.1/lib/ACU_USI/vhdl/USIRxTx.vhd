LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Dieses Modul setzt eine 'Clock' von 100MHz voraus!

--dd.mm.yy  born
--dd.mm.yy  im sTxState <= DEL_STOP wird TxIdle nicht zurueck auf '1' gesetzt, sondern erst in in sTxState <= IDLE.
--05.10.10  'RxEnableStatus' neu (DS)
--17.01.11  'sBitGenTransmitterPreClock' eingefuehrt. Das STOP-BIT dauerte durch das angebundenen Handshake zwischen
--          zu lange, dadurch wurde 'USTXRx' und 'TxFIFO' ein paar Takte laenger als notwendig, weil es durch
--          'TxTransmitterFreeInfo' beeinflusst wurde. 'TxTransmitterFreeInfo' kommt nun durch diese PreClock 3 Takt frueher, wodurch das STOP-BIT
--          um genau diese drei Takt verkuerzt wird. Dadurch ist ein Stopbit jetzt genauso lang wie ein Datenbit.
--          Ausserdem ist in 'TxFIFO' noch ein Takt eingespart. (DS)
--21.02.12  'GenBreak' eingefuehrt. Ist eine USI NICHT im HighSpeed-Modus, aber AUCH NICHT auf 115,2kBit Standarddatenrate
--          eingestellt, kann der Host diese USI nach einem Verbindungsabbruch nicht reaktivieren ohne umstaendlich alle Bitraten
--          durchzuklappern und auf eine Antwort zu hoffen. Daher gibt es nun den 'GenBreak' Port. Wird daran eine Flanke erkannt,
--          wird der Ausgabeport 'TxSerial' fuer 150us (150.000 Zyklen) auf Low gelegt. Der Empfaenger kann dies erkennen und legt
--          dadurch die Bitrate auf 115,2kBit fest. Da USIRxTx auf der Gegenseite den Empfaenger spielt ist auch der
--          Timer zur Erkennung des Breaks hier eingebaut. (DS)
--dd.mm.yy  'GenBreak' Dauer ist nicht 150.000 Zyklen sondern nur 15.000 Zyklen fuer 1,5us Dauer
--15.10.12  Ports und Signale der einzelnen Module sind teilweise mit neuen Namen versehen um deren Funktion klarer zu machen (DS)
--dd.mm.yy  Durch einen 'Break' (USIRx >= 150 us low und dann L->H Flanke) soll ein Neuinitialisieren der USI ausgeloest werden.
--          Der unter V1.4 eingebaute Timer zur Erkennung des Break ist dabei nur die halbe Loesung, denn wird 'sBreakDetected' = '1' 
--          muss natuerlich auch der USIRx-Vorgang abgebrochen werden und der "Break" darf nicht als
--          empf. Datenwert an den Ausgabeport 'RxData' geleitet werden. 
--          Der Empfang eines Bytes benoetigt abhaenhgig von der Bitrate wie folgt:
--           115,2 kBit   -> 86,8   us
--           1     MBit   -> 10     us
--           2     MBit   ->  5     us
--           5     MBit   ->  2     us
--          10     MBit   ->  1     us
--          16.6   MBit   ->  0,602 us
--          20     MBit   ->  0,5   us
--          25     MBit   ->  0,4   us
--          Der Break benoetigt also in JEDEM Fall laenger als der empf. eines Bytes.
--          Folglich ist folgendes zu tun:
--          Wird das vermeindliche Stoppbit empfangen und 'RxSerial' ist noch immer '0'
--          darf daraus KEIN 'sRxFramingError' erzeugt werden sondern es muss getestet werden ob es sich um einen
--          Break handelt.
--18.02.14  Meldung 'Ignored Power-Up Level option on the following registers' -> 'Critical Warning (18010): Register xxx will power up to GND/High'
--          durch gezieltes setzen eines Initwertes der betroffen Variable(n) beseitigt. (DS)
--27.08.13  'sRxIdle' und 'sRxFramingError' wurden ggf. im 'tRxState' = STOP gesetzt. Dies sollte aber besser in 'FRAMING_ERROR_OR_BREAK' erfolgen,
--          wenn der Receiver auch wirklich wieder frei ist (dazu muss in jedem Fall "RxSerial' = IdleHigh werden) falls auch wirklich
--          ein Framing Error vorliegt und kein Break.
--          Daher wird die USIRx-Statemachine nun zusaetzlich durch 'sBreakDetected' zurueck auf die Reset-Werte gesetzt. (DS)
--13.04.16  Zusaetzlicher Input 'TxFIFOnEmpty', damit die Umschaltung der Bitrate sicher erst dann erfolgt, wenn der  TxFIFO
--          auch wirklich leer ist. Siehe hierzu fuer Details auch 'pBdBitRateSelector'. ( - DS)
--11.06.19  Der Transmitter erhaelt ein zusaetzliches 'TxDriverEnable'. Wird immer '1' wenn Daten ueber 'TxSerial' verschickt werden. 
--          Dafuer ist der 'DiagBitClk' Port entfallen. (DS)
--16.09.21  Ports umbenannt, Generics fuer ..Version und einstellbare Anzahl von Transferbits entfernt (sind ohnehin immer 8) (DS)

-- Entity - Blockbeschreibung Ein- und Ausgaenge
entity USIRxTx is
   generic 
   (
      gWidth      : integer := 8
   );
----------------------------------------------------------------
   port (
-- Clock, Reset, Select
            Clock                            : in  std_logic;    --Systemclock
            Reset                            : in  std_logic;    --aktive High
            USI_Select                       : in  std_logic;    --aktive High
            SetBitRate_RxOnOff               : in  std_logic;    --L -> H Bitraten, Receiver ein usw. uebernehmen
----------------------------------------------------------------
-- Bit Raten Generator
            BitRate                          : in  std_logic_vector(2 downto 0); --8 versch. Geschwindigkeiten

            CurrentBitRate                   : out std_logic_vector(2 downto 0); --gibt die aktuell eingestellte Bitrate aus fuer USIControlStatus

            BitRateValid                     : out std_logic;                    --High, wenn Aenderungen an Bitrate erfolgreich durchgefuehrt
----------------------------------------------------------------
-- Generate/Detect Break
            GenBreak                         : in std_logic;                     -- HighFlanke loest den 150 us Breakpulse zum BitratenReset aus
----------------------------------------------------------------
-- UART                        
            TxData                           : in  std_logic_vector(gWidth-1 downto 0);   --Ausgaberegister
            TxGetDataFromTxDataAndTransmit   : in  std_logic;                             --Transmit Enable (1 Puls!)
            TxTransmitterFreeInfo            : out std_logic;                             --Daten gesendet, Ausgaberegister frei

            TxFIFOnEmpty                     : in  std_logic;                       --zeigt an, dass der TxFIFO nicht leer ist
            
            TxIdle                           : out std_logic;                       --zeigt an, dass der Sender im Leerlauf ist
            RxIdle                           : out std_logic;                       --zeigt an, dass der Empfaenger im Leerlauf ist
                        
            RxData                           : out std_logic_vector(gWidth-1 downto 0);   --Empfangsregister
            RxEnable                         : in  std_logic;                       --Receive Enable
            RxEnableStatus                   : out std_logic;                       --Receive Enable Status, wenn '1' Modulempfaenger EIN
            RxNewDataAvailOnRxData           : out std_logic;                       --Daten empfangen, Receiveregister voll
            RxFramingError                   : out std_logic;                       --Fehler beim Datenempfang
            RxCollision                      : out std_logic;                       --Receive Collision
            RxDataFetchedFromRxData          : in  std_logic;                       --1 Takt Pulse Info vom RxFIFO: Daten von 'RxData' in RxFIFO uebernommen 
                                                                                    --und/oder setzt 'RxFramingError' und 'sRxCollision' zurueck

            TxDriverEnable                   : out std_logic;                       --wird immer '1' wenn ueber 'TxSerial' Daten verschickt werden

            RxSerial                         : in  std_logic;                       --Serieller Eingang
            TxSerial                         : out std_logic                        --Serieller Ausgang
      );
end USIRxTx;


-- RTL Register Transfer Level von USI
ARCHITECTURE RTL OF USIRxTx IS
----------------------------------------------------------------
-- Bit Raten Generator

   --sBitRateDivider <= tatsaechlicher Wert -1 (1 Zyklus fuer Nachladen)   
   constant cBitRateDivider25M  : integer :=      3; --    25   MBit
   constant cBitRateDivider20M  : integer :=      4; --    20   MBit
   constant cBitRateDivider16M  : integer :=      5; --    16,6 MBit
   constant cBitRateDivider10M  : integer :=      9; --    10   MBit
   constant cBitRateDivider5M   : integer :=     19; --     5   MBit
   constant cBitRateDivider2M   : integer :=     49; --     2   MBit
   constant cBitRateDivider1M   : integer :=     99; --     1   MBit
   constant cBitRateDivider115K : integer :=    867; --115200    Bit
   

   --sAdjustCenterBit <= sBitRateDivider/2 -1 (und dann abrunden)    
   constant cAdjustBitCenter25M  : integer :=      0; --    25   MBit   
   constant cAdjustBitCenter20M  : integer :=      0; --    20   MBit
   constant cAdjustBitCenter16M  : integer :=      1; --    16,6 MBit
   constant cAdjustBitCenter10M  : integer :=      3; --    10   MBit
   constant cAdjustBitCenter5M   : integer :=      8; --     5   MBit
   constant cAdjustBitCenter2M   : integer :=     23; --     2   MBit
   constant cAdjustBitCenter1M   : integer :=     48; --     1   MBit
   constant cAdjustBitCenter115K : integer :=    432; --115200   Bit
   
   constant cNumberOfBits        : integer :=      7;  --Anzahl Bit je Datum

   --Signale
   signal   sMemBitRate          : std_logic_vector (2 downto 0);          --Merker fuer gew. Bitrate
   signal   sBitRateDivider      : integer range   870 downto 0 := cBitRateDivider115K;    --Teiler fuer SysClock
   signal   sAdjustCenterBit     : integer range   440 downto 0 := cAdjustBitCenter115K;

   signal   sBitClkCounter       : integer range   870 downto 0 := 0;      --Zaehler Flanke<->Flanke BitClock
   signal   sBitEnable           : std_logic := '0';                       --Enable Signal neuer Zyklus

   signal   sSetBitRate_RxOnOff_EdgeDet   : std_logic := '0';
   signal   sCurrentBitRate               : std_logic_vector  (2 downto 0);

   --Bitrate wird extern per 5 Bit Port eingestellt, wobei 0->1 Bit und 31->32 Bit entspricht
   
   signal   sBitRateValid        : std_logic;

   type     tBitRateSelectState is (
                                    BIT_RATE_VALID,
                                    SET_NEW_Bit_RATE
                                   );

   signal   sBitRateSelectState : tBitRateSelectState;

-------------------------------------------------------------------
-- USI TRANSMITTER
-------------------------------------------------------------------
-- Statemachine fuer Transmitter    
   type   tTxState  is (
                        IDLE,
                        START,
                        SEND,
                        SET_STOP,
                        DEL_STOP
                       );

   signal   sTxSerial   : std_logic := '1';  --serielles Ausgangssignal
     
   signal   sTxTransmitterFreeInfo  : std_logic := '1';  --Sendepuffer leer, kann neu geladen werden
   
-------------------------------------------------------------------
-- Signale fuer Transmitter
   signal   sTxState                         : tTxState;                                  --aktueller Zustand Transmit Statemachine
   signal   sTxBitIndex                      : integer range gWidth downto 0 := 0;        --Transmit Bitindex (Bit 31..0)
   signal   sTxSampleCount                   : integer range 870 downto 0 := 0;           --Bitdrate
   signal   sTxDriverEnable                  : std_logic;                                 --wird immer '1' wenn Datan ueber TxSerial' verschickt werden
   constant cTxTransmitterFreeDelayCounterMax: integer := 10;
   signal   sTxTransmitterFreeDelayCounter   : integer range cTxTransmitterFreeDelayCounterMax downto 0;
   
-------------------------------------------------------------------
-- USI RECEIVER
-------------------------------------------------------------------
-- Statemachine fuer Receive
   type     tRxState   is (
                           WAIT_FOR_HIGH_LEVEL,
                           WAIT_FOR_LOW_LEVEL,
                           START,
                           RECEIVE,
                           STOP,
                           FRAMING_ERROR_OR_BREAK
                          );
-------------------------------------------------------------------
-- Signale fuer Receiver   

   signal   sRxDataReadEdgeDet: std_logic :='0';

   signal   sTxBus,                                                  --Transmit Register
            sRxBus            : std_logic_vector(gWidth-1 downto 0); --Receive Register 

   signal   sRxIdle,
            sTxIdle           : std_logic := '1';

   signal   sRxSerial       : std_logic;

   signal   sRxState          : tRxState;                            --aktueller Zustand Receive Statemachine
   signal   sRxBitIndex       : integer range gWidth downto 0 := 0;  --Receive Bit Index          
   signal   sRxSampleCount    : integer range 440 downto 0 := 0;     --Sample bis zur Mitte des Bit
                    
   signal   sRxRegFree        : std_logic := '1';        --wenn Daten aus RxRegister abgeholt wurden
                                                         --wurden Daten nicht abgeholt und zwischenzeitlich 
                                                         --neue Daten vollstaendig empfangen wird RxCollision
                                                         --und RxFramingError gesetzt.
   
   signal   sRxEnableStatus   : std_logic := '0';

   -------------------------------------------------------------------
   -- Bitratengenerator Receiver
   signal sBitGenReceiverSampleCount : integer range 870 downto 0 := 1;
   signal sBitGenReceiverEnable      : std_logic := '1';
   signal sBitGenReceiverClock       : std_logic := '0';

   -------------------------------------------------------------------
   -- Bitratengenerator Transmitter
   signal sBitGenTransmitterSampleCount : integer range 870 downto 0 := 0;
   signal sBitGenTransmitterEnable      : std_logic := '1';
   signal sBitGenTransmitterClock       : std_logic := '0';
   signal sBitGenTransmitterPreClock    : std_logic := '0';

   -------------------------------------------------------------------
   -- Statemachine fuer GenBreak
   type     tState_GenBreak   is (
                                  IDLE,
                                  TIMER_RUN
                                 );

   -- Flankendetektor 'GenBreak'
   -- GenBreak ein paar Takte laenger als DetBreak, um Taktfehler zwischen Sender und
   -- Empfaenger zu kompensieren.
   signal   sGenBreakEdgeDetArray   : std_logic_vector (2 downto 0);
   signal   sGenBreak               : std_logic := '1';
   signal   sGenBreak_Timer         : integer range 0 to 15050:= 0;
   constant cGenBreak_TimerMax      : integer := 15050 ;
   signal   sState_GenBreak         : tState_GenBreak;

   -------------------------------------------------------------------
   -- DetBreak
   signal   sBreakDetected          : std_logic;
   signal   sDetBreak_Timer         : integer range 0 to 15000 := 0;
   constant cDetBreak_TimerMax      : integer := 15000;

   -------------------------------------------------------------------
   -- Flankendetektor 'SetBitRate_RxOnOff'
   signal sSetBitRate_RxOnOff_EdgeDetArray       : std_logic_vector(2 downto 0);

   -------------------------------------------------------------------
   -------------------------------------------------------------------

   begin

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      --                                                          +
      --   GenBreak Flanke erkennen /Timer                        +
      --                                                          +
      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      pGenBreakEdgeDet: process (Clock, Reset)
      begin
         if (Reset = '1') then
            sGenBreakEdgeDetArray   <= (others => '0');
            sGenBreak               <= '1';
            sState_GenBreak         <= IDLE;
            sGenBreak_Timer         <= 0;
         elsif (rising_edge(Clock)) then

            case(sState_GenBreak) is

               --------------------------------------------------------------------------------------------
               when IDLE =>

                  sGenBreakEdgeDetArray <= (sGenBreakEdgeDetArray(1 downto 0) & GenBreak );  --Port erfassen

                  sGenBreak <= '1';                               --GenBreak noch nicht aktiv, UND-Verknuepft mit TxSerial-Ausgabe

                  if (USI_Select = '1' and sGenBreakEdgeDetArray = "011") then --steigende Flanke erkannt und USI selektiert
                     sState_GenBreak <= TIMER_RUN;                --GenTimer starten
                  else
                     sState_GenBreak <= IDLE;                     --anderfalls warten
                  end if;

               --------------------------------------------------------------------------------------------
               when TIMER_RUN =>

                  sGenBreak <= '0';                               --GenBreak ist aktiv, TxSerial Ausgabe ist dauerhaft Low, UND-Verknuepft mit TxSerial-Ausgabe

                  if (sGenBreak_Timer < cGenBreak_TimerMax) then  --GenTimer noch nicht abgelaufen
                     sGenBreak_Timer <= sGenBreak_Timer + 1;      --GenTimer erhoehen
                     sState_GenBreak <= TIMER_RUN;
                  else                                            --GenTimer ist abgelaufen
                     sGenBreak_Timer <= 0;                        --Timer loeschen
                     sState_GenBreak <= IDLE;                     --auf Neustart warten
                  end if;

               when others =>
                  sState_GenBreak <= IDLE;

            end case;

        end if ;
      end process pGenBreakEdgeDet;

      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      --                                                          +
      --   DetBreak Timer                                         +
      --                                                          +
      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      pDetBreakTimer: process (Clock, Reset)
      begin
         if (Reset = '1') then
            sDetBreak_Timer   <= 0;
            sBreakDetected    <= '0';
         elsif (rising_edge(Clock)) then
                          
            if (sRxSerial = '0') then                             --auf seriellem Eingangsport ist Low
               if (sDetBreak_Timer < cDetBreak_TimerMax) then     --DetTimer nicht abgelaufen
                  sDetBreak_Timer   <= sDetBreak_Timer + 1;       --DetTimer erhoehen
                  sBreakDetected    <= '0';                       --noch kein Break erkannt
               else                                               --DetTimer ist abgelaufen
                  --sDetBreak_Timer   <= 0;                       --DetTimer loeschen
                  sDetBreak_Timer   <= sDetBreak_Timer;           --DetTimer stoppen
                  sBreakDetected    <= '1';                       --Break erkannt, Bitraten Reset durchfuehren
               end if;
            else
               sDetBreak_Timer   <= 0;                            --auf seriellem Eingang ist High
               sBreakDetected    <= '0';                          --DetTimer loeschen
            end if;
         end if ;
      end process pDetBreakTimer;


      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      --                                                          +
      --   BdBitSetRate Flanke erkennen.                          +
      --                                                          +
      --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      pSetBitRate_RxOnOff_EdgeDet: process (Clock, Reset)
      begin
        if (Reset = '1') then
            sSetBitRate_RxOnOff_EdgeDetArray <= (others => '0') ;
        elsif (rising_edge(Clock)) then
            sSetBitRate_RxOnOff_EdgeDetArray <= (sSetBitRate_RxOnOff_EdgeDetArray(1 downto 0) & SetBitRate_RxOnOff);
            if (sSetBitRate_RxOnOff_EdgeDetArray = "011") then --steigende Flanke
               sSetBitRate_RxOnOff_EdgeDet <= '1';
            else
               sSetBitRate_RxOnOff_EdgeDet <= '0';
            end if ;
        end if ;    
      end process pSetBitRate_RxOnOff_EdgeDet;

      --+++++++++++++++++++++++++++++++++++++
      --                                    +
      -- Bit Raten Selektor                 +
      --                                    +
      --+++++++++++++++++++++++++++++++++++++

      pBitRateSelector: process (Reset, Clock, sBreakDetected)
      begin         
         if (Reset = '1' or sBreakDetected = '1') then
         
            sMemBitRate             <= "111";   --115200 Bit
            sCurrentBitRate         <= "111";   --115200 Bit
            sBitRateDivider         <= cBitRateDivider115K; 
            sAdjustCenterBit        <= cAdjustBitCenter115K; 
            sBitRateValid           <= '1';
            sBitRateSelectState     <= BIT_RATE_VALID;            
            
         elsif (rising_edge(Clock)) then
          
            if (USI_Select = '1' and sSetBitRate_RxOnOff_EdgeDet = '1') then   --neue Bitrate gewuenscht

               sMemBitRate    <= BitRate;
               sBitRateValid  <= '0';

            else

               case (sBitRateSelectState) is

                  --Bitraten duerfen sich nur aendern, wenn keine Daten gesendet oder empfangen werden.
                  --'sBitRateValid' wird '0', wenn 'USI_Select = '1'' UND 'sSetBitRate_RxOnOff_EdgeDet = '1''
                  --'sRxIdle' wird '1', wenn das letzte Bit eines seriellen Byte empfangen wurde und an den RxFIFO gegeben wird
                  --'sTxIdle' wird '1', wenn wenn der Transmitter beim senden das letzte Bit eines Bytes verschickt hat
                  --'TxFIFOnEmpty' wird '0', wenn der TxFIFO leer ist
                  --'TxGetDataFromTxDataAndTransmit' wird '0', wenn USIRxTx das Byte aus dem TxFIFO auch sicher abgeholt hat
                  --'sRxIdle' und 'sTxIdle' reichen NICHT aus, um sicher zu sein, dass auch alle im TxFIFO befindlichen
                  --Bytes noch sicher verschickt sind, bevor die Bitrate sich aendern darf. Es ist zusaetzlich das Signal                  
                  --'TxFIFOnEmpty' noetig um sicher zus tellen, dass der TxFIFO auch leer ist. Aber auch das ist noch nicht
                  --ausreichend, das beim letzten Byte aus dem TxFIFO dieser zwar leer ist, aber von USIRxTx noch nicht
                  --abgeholt und verschickt wurde. Dazu ist zusaetzlich noch 'TxGetDataFromTxDataAndTransmit' noetig um dies 
                  --sicher zu stellen. Wird das letzte Byte aus dem TxFIFO ausgegeben wird 'TxFIFOnEmpty' = '0', und
                  --'TxGetDataFromTxDataAndTransmit' wird '1'. Dann wird das letzte Byte verschickt, 'TxGetDataFromTxDataAndTransmit'
                  --wird dadurch wieder '0', jedoch 'sTxIdle' wird '1'. Erst wenn das Byte komplett verschickt wurde, wird
                  --'sTxIdle' wieder '0'. Dann darf sich die Bitrate aendern.
                  --Ohne 'TxFIFOnEmpty' und 'TxGetDataFromTxDataAndTransmit' in der if-Abfrage im State 'BIT_RATE_VALID'
                  --(vor V2.03) wurde die Bitrate umgeschaltet, obwohl noch Werte im TxFIFO waren. Bei einem Verbindungsabbruch ist
                  --dies nicht weiter fatal, aber unschoen. Die noch verbliebenen Werte im TxFIFO wurden dann mit der
                  --neuen Bitrate noch verschickt. Bei einem Verbindungsabbruch waren dies dann 115kBit.
                  --Bei HigHSpeed kann ohnehin nicht einfach eine Bitratenumschaltung erfolgen. Prinzipiell waere
                  --dies zwar moeglich, da aber auch der Host umgeschaltet werden muss und dies bei einem Stream
                  --ohne Pause doch recht unschoen ist, schaltet dieser HighSpeed zunaechst aus und wechselt dann die Bitrate.
                  
                  --------------------------------------------------------------------------------------------
                  when BIT_RATE_VALID =>

                     if (sBitRateValid = '0' AND sRxIdle = '1' AND sTxIdle ='1' AND TxFIFOnEmpty = '0' AND TxGetDataFromTxDataAndTransmit = '0') then
                        sBitRateSelectState <= SET_NEW_Bit_RATE;
                     else
                        sBitClkCounter <= sBitRateDivider;
                     end if;

                  --------------------------------------------------------------------------------------------
                  when  SET_NEW_Bit_RATE  => 

                     sCurrentBitRate   <= sMemBitRate;

                     case sMemBitRate is

                        when "000"  =>  sBitRateDivider <= cBitRateDivider25M;  --    25   MBit
                                        sAdjustCenterBit <= cAdjustBitCenter25M;

                        when "001"  =>  sBitRateDivider <= cBitRateDivider20M;  --    20   MBit
                                        sAdjustCenterBit <= cAdjustBitCenter20M;

                        when "010"  =>  sBitRateDivider <= cBitRateDivider16M;   --   16,6 MBit
                                        sAdjustCenterBit <= cAdjustBitCenter16M;

                        when "011"  =>  sBitRateDivider <= cBitRateDivider10M;   --   10   MBit
                                        sAdjustCenterBit <= cAdjustBitCenter10M;

                        when "100"  =>  sBitRateDivider <= cBitRateDivider5M;   --     5   MBit
                                        sAdjustCenterBit <= cAdjustBitCenter5M;

                        when "101"  =>  sBitRateDivider <= cBitRateDivider2M;   --     2 MBit
                                        sAdjustCenterBit <= cAdjustBitCenter2M; 

                        when "110"  =>  sBitRateDivider <= cBitRateDivider1M;   --     1 MBit
                                        sAdjustCenterBit <= cAdjustBitCenter1M;

                        when "111"  =>  sBitRateDivider <= cBitRateDivider115K; --115200 Bit
                                        sAdjustCenterBit <= cAdjustBitCenter115K;

                        when others =>  sBitRateDivider <= cBitRateDivider115K; --115200 Bit
                                        sAdjustCenterBit <= cAdjustBitCenter115K;

                     end case;

                     sBitRateValid       <= '1';
                     sBitRateSelectState <= BIT_RATE_VALID;
               
               end case;
            end if;
         end if;
      
      end process pBitRateSelector;

      BitRateValid   <= sBitRateValid;
      CurrentBitRate <= sCurrentBitRate;


      --+++++++++++++++++++++++++++++++++++++
      --                                    +
      -- Receiver enable/disable            +
      --                                    +
      --+++++++++++++++++++++++++++++++++++++

      pReceiverEnableDisable: process (Reset, Clock)
      begin
         if (Reset = '1') then
            sRxEnableStatus <= '0';
         elsif (rising_edge(Clock)) then
          
            if (USI_Select = '1' and sSetBitRate_RxOnOff_EdgeDet = '1') then
               sRxEnableStatus <= RxEnable;                    --Receiver ein-/ausschalten
            else
               sRxEnableStatus <= sRxEnableStatus;
            end if;
         
         end if;

      end process pReceiverEnableDisable;

  
      --+++++++++++++++++++++++++++++++++++++
      --                                    +
      -- Bitgenerator Receiver              +
      --                                    +
      --+++++++++++++++++++++++++++++++++++++

      pBitGenReceiver: process (Reset, Clock)
      begin

         if (Reset = '1') then

            sBitGenReceiverSampleCount <= 1; -- 1 Zyklus wird zum Nachladen benoetigt, daher steht Zaehler im State START schon auf 1
                                             -- Wuerde mit 0 initialisiert wird im aller ersten Durchlauf nach Reset der Bittakt um 1 Zylus nach 
                                             -- hinten verschoben und ein Takt mehr gezaehlt
         elsif (rising_edge(Clock)) then
          
            if ((sRxState = RECEIVE) OR (sRxState = STOP)) then

               if (sBitGenReceiverSampleCount < sBitClkCounter) then  
                  sBitGenReceiverSampleCount  <= sBitGenReceiverSampleCount + 1; --Bitzaehler plus 1
                  sBitGenReceiverClock        <= '0';
               else 
                  sBitGenReceiverSampleCount  <= 0;                             --Bitzaehler neu laden
                  sBitGenReceiverClock        <= '1';
               end if;
            else
               sBitGenReceiverSampleCount <= sBitGenReceiverSampleCount;
            end if;
         end if;

      end process pBitGenReceiver;

      --+++++++++++++++++++++++++++++++++++++
      --                                    +
      -- Bitgenerator Transmitter           +
      --                                    +
      --+++++++++++++++++++++++++++++++++++++

      pBitGenTransmitter: process (Reset, Clock)
      begin
         if (Reset = '1') then

            sBitGenTransmitterSampleCount <= 0;

         elsif (rising_edge(Clock)) then

            if (sTxState /= IDLE) then

               if (sBitGenTransmitterSampleCount < sBitClkCounter) then  
                  sBitGenTransmitterSampleCount  <= sBitGenTransmitterSampleCount + 1; --Bitzaehler plus 1
                  sBitGenTransmitterClock        <= '0';
               else
                  sBitGenTransmitterSampleCount  <= 0;                                --Bitzaehler neu laden
                  sBitGenTransmitterClock        <= '1';
               end if;

               if (sBitGenTransmitterSampleCount = sBitClkCounter - 3) then         --Kurzer Pulse 1 Takt bevor der Zaehler abgelaufen ist
                  sBitGenTransmitterPreClock     <= '1';                            --um das Signal 'TxTransmitterFreeInfo' etwas frueher kommen zu lassen
               else                                                                 --und dadurch das STOP-BIT zu verkuerzen
                  sBitGenTransmitterPreClock     <= '0';
               end if;

            else
               sBitGenTransmitterSampleCount <= 0;  --damit Zaehler bei IDLE sicher genullt wird und wieder genau ein Bit zaehlt
            end if;
         end if;

      end process pBitGenTransmitter;


      --+++++++++++++++++++++++++++++++++++++++
      --                                      +
      --   UART Empfaenger                    +
      --                                      +
      --+++++++++++++++++++++++++++++++++++++++
            
      pReceiveData: process (Clock, Reset, sBitGenReceiverClock, RxSerial, sBreakDetected)
      begin
         if (Reset = '1') then
         
            sRxState                         <= WAIT_FOR_HIGH_LEVEL;
            sRxIdle                          <= '1';  --Receiver ist im IDLE Mode, Bitrate darf geaendert werden

            sRxBus                           <= (others =>'0');
            RxData                           <= (others =>'0');
            RxNewDataAvailOnRxData           <= '0';
            RxCollision                      <= '0';
            RxFramingError                   <= '0';

            sRxRegFree                       <= '1';
            sRxSampleCount                   <= 0;
            sRxBitIndex                      <= 0;  --Bit Index fuer Empfangsregister

         elsif (rising_edge(Clock)) then

            sRxSerial <= RxSerial;

            --++++++++++++++++++++++++++++++
            --                             +
            --   Receive State Machine.    +
            --                             +
            --++++++++++++++++++++++++++++++

            --das empfangene Bit soll moeglichst mittig abgetastet werden, d.h.
            --wenn die fallende Flanke des Starbit erkannt wurde wird ein Timer 
            --zunaechst so geladen, dass am Ende des Startbit von START nach
            --RECEIVE gewechselt wird. Dann wird der Timer so geladen, dass er stets etwa
            --mittig des folgenden Bit abgelaufen ist.

            --Signalisierung vom RxFIFO, dass dieser die Daten abgeholt hat

            if (RxDataFetchedFromRxData = '1') then
               RxData                           <= (others =>'0'); -- >> koennte auch entfallen, dann bleiben Daten aus Ausgag eben anstehen! <<
               sRxRegFree                       <= '1';
               RxCollision                      <= '0';
               RxFramingError                   <= '0';
            else
               sRxRegFree                       <= sRxRegFree;
            end if;

            case (sRxState) is

               -----------------------------------------------------------------------------
               when WAIT_FOR_HIGH_LEVEL   =>

                  RxNewDataAvailOnRxData <= '0';

                  if ((sRxEnableStatus = '1') AND (sBitRateSelectState = BIT_RATE_VALID)) then                   
                     if (sRxSerial = '1') then
                        sRxState <= WAIT_FOR_LOW_LEVEL;
                     end if;
                  end if;

               -----------------------------------------------------------------------------
               when WAIT_FOR_LOW_LEVEL    =>

                  if (sRxSerial = '0') then                              -- Fallende Flanke = Startbit
                     sRxBitIndex    <= 0;
                     sRxIdle        <= '0';
                     sRxState       <= START;

                     if (sMemBitRate /= "000") then
                        sRxSampleCount <= sAdjustCenterBit;                -- Halbe Bitbreite
                     else
                        sRxSampleCount <= 0;

                     end if;
                  end if;

               -----------------------------------------------------------------------------
               when START  =>

                  if (sRxSampleCount > 0) then                             --Mitte des Start Bit  
                     sRxSampleCount <= sRxSampleCount - 1;
                  else
                     sRxSampleCount <= sRxSampleCount;
                     sRxState       <= RECEIVE;
                  end if;

               -----------------------------------------------------------------------------
               when RECEIVE   =>

                  if (sBitGenReceiverClock = '1') then
                     sRxBus(sRxBitIndex)  <= sRxSerial;

                     if (sRxBitIndex < cNumberOfBits) then
                        sRxBitIndex  <= sRxBitIndex+1;
                     else
                        sRxState    <= STOP;
                     end if;
                  end if;

               -----------------------------------------------------------------------------
               --Es sind folgende Faelle moeglich:
               --1. Ein Stoppbit wurde erkannt     -> UND Ausgaberegister wurde von nachfolgender Instanz gelesen
               --                                     -'sRxRegFree' auf '0'
               --                                     -'sRxBus' an 'RxData' ausgeben
               --                                     -'RxNewDataAvailOnRxData' auf '1'
               --                                     -'sRxIdle' auf '1' = Receiver ist im Leerlauf
               --                                  -> UND Ausgaberegister wurde von nachfolgender Instanz NICHT gelesen
               --                                     -'RxCollision' auf '1'  
               --                                     -'sRxIdle' auf '1' = Receiver ist im Leerlauf
               --2. Es wurde KEIN Stoppbit erkannt -> -Es kann ein "Break" empfangen werden oder es handelt sich um einen "FramingError".
               --                                     -in den State <= FRAMING_ERROR_OR_BREAK springen, dort ggf. 'RxFramingError' setzen wenn KEIN Break erkannt wird               

               when STOP      =>

                  if (sBitGenReceiverClock = '1') then
                     if (sRxSerial = '1') then                                      --Teste Stop Bit (erfolgreich)
                        if (sRxRegFree = '1') then                                  --letzer RxString aus Register abgeholt
                           sRxRegFree                       <= '0';                 --RxSerial-Ausgaberegister wurde von nachfolgender Instanz ausgelesen
                           RxData                           <= sRxBus;              --empf. Wert ins RxSerial-Ausgaberegister kopieren
                           RxNewDataAvailOnRxData           <= '1';                 --1 Takt Pulse als Info fuer RxFIFO, dass dieser Daten abholt
                           sRxIdle                          <= '1';                 --Receiver ist im Leerlauf, Bitraten koennen ggf. geandert werden
                           sRxState                         <= WAIT_FOR_HIGH_LEVEL; --ggf. auf naechtes Startbit warten
                        else                                                        --letzer RxString NICHT aus Register abgeholt             
                           RxCollision                      <= '1';                 --Collision    
                           sRxIdle                          <= '1';                 --Receiver ist im Leerlauf, Bitraten koennen ggf. geandert werden 
                           sRxState                         <= WAIT_FOR_HIGH_LEVEL; --ggf. auf naechtes Startbit warten                                       
                        end if;
                     else
                        sRxState          <= FRAMING_ERROR_OR_BREAK;
                     end if;
                  end if;
         
               -----------------------------------------------------------------------------
               -- Zunaechst ggf. warten bis der 'DetBreakTimer' abgelaufen ist
               -- Wird '1' auf 'RxSerial' erkannt BEVOR 'DetBreakTimer' abgelaufen ist -> 'RxFramingError' auf '1'
               -- andernfalls nur in den 'WAIT_FOR_HIGH_LEVEL' State springen.
               -- In jedem Fall 'sRxIdle' auf '1' => Receiver ist im Leerlauf

               --Etwas unschoen: Das Ruecksetzen von 'RxFramingError' erfolgt automatisch, sobald das naechste gueltige Byte empfangen wird.

               when FRAMING_ERROR_OR_BREAK =>

                  if (sBreakDetected = '0') then                     --sofern noch KEIN Break erkannt (RxSerial < 150us low)
                     if (sRxSerial = '1') then                       --wird RxSerial-Bit = '1' und noch KEIN Break erkannt -> Nur Framing Error
                        RxFramingError    <= '1';                    --Stopbit nicht erkannt
                        sRxIdle           <= '1';                    --Receiver ist im Leerlauf, Bitraten koennen ggf. geandert werden 
                        sRxState          <= WAIT_FOR_HIGH_LEVEL;    --ggf. auf naechtes Startbit warten                                       
                     else
                        sRxState <= FRAMING_ERROR_OR_BREAK;
                     end if;
                  else                                               --es wurde ein Break erkannt (RxSerial > 150us low)
                     sRxIdle           <= '1';                       --Receiver ist im Leerlauf, Bitraten koennen ggf. geandert werden 
                     RxFramingError    <= '0';                       --FramingError wieder loeschen
                     sRxState          <= WAIT_FOR_HIGH_LEVEL;       --ggf. auf naechtes Startbit warten     
                  end if;

            end case;

         end if; --(clock)

      end process pReceiveData;


      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      --                                                                 +
      --   Daten den zugehoerigen externen Ports zuweisen                +
      --                                                                 +
      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      RxIdle  <= sRxIdle;

      --+++++++++++++++++++++++++++++++++++++++
      --                                      +
      --   UART Sender                        +
      --                                      +
      --+++++++++++++++++++++++++++++++++++++++   

      pTransmitData: process (Clock, Reset)
      begin
         if (Reset = '1') then

            sTxState                         <= IDLE;
            sTxIdle                          <= '1';        --Transmitter ist im IDLE Mode, Bitrate darf geaendert werden
            sTxBus                           <= (others=>'0');
            sTxSerial                        <= '1';
            sTxTransmitterFreeInfo           <= '1';
            sTxBitIndex                      <=  0;
            sTxDriverEnable                  <= '0';
            sTxTransmitterFreeDelayCounter   <= cTxTransmitterFreeDelayCounterMax;

         elsif (rising_edge(Clock)) then

            --+++++++++++++++++++++++++++++++
            --                              +
            --   Transmit State Machine.    +
            --                              +
            --+++++++++++++++++++++++++++++++

            --Der Highzustand von 'sTxDriverEnable' wird nach dem Ende einer Uebertragung 
            --um 'cTxTransmitterFreeDelayCounterMax' Zyklen verlaengert
            
            if (sTxState = IDLE and sTxTransmitterFreeDelayCounter > 0 and sTxDriverEnable = '1') then            
               sTxTransmitterFreeDelayCounter <= sTxTransmitterFreeDelayCounter - 1;             
            else
               sTxTransmitterFreeDelayCounter <= sTxTransmitterFreeDelayCounter;                             
            end if;

            case sTxState is

               -----------------------------------------------------------------------------
               when IDLE   =>

                  if (TxGetDataFromTxDataAndTransmit = '1') then
                     sTxBus                 <= TxData;        --Daten von Senderegister (CPU Seite) in Transmitregister (Logikseite)
                     sTxTransmitterFreeInfo  <= '0';
                     sTxIdle                 <= '0';
                     sTxDriverEnable         <= '1';
                     sTxState                <= START;
                  else
                     sTxTransmitterFreeInfo  <= '1';
                     sTxIdle                 <= '1';

                     if sTxTransmitterFreeDelayCounter = 0 then
                        sTxDriverEnable      <= '0';              
                     end if;     

                  end if;

               -----------------------------------------------------------------------------
               when START  =>

                  --if (sBitRateValid = '1') then
                  if (sBitRateSelectState = BIT_RATE_VALID) then
                     sTxSerial      <= '0';                 --Start Bit.
                     sTxBitIndex    <= 0;
                     sTxState       <= SEND;
                  end if;

               -----------------------------------------------------------------------------
               when SEND   =>

                  if (sBitGenTransmitterClock = '1') then

                     sTxSerial   <= sTxBus(sTxBitIndex);    --LSB first, max. 32 Data Bits.
                       
                     if (sTxBitIndex < cNumberOfBits) then
                        sTxBitIndex <= sTxBitIndex + 1;
                     else
                        sTxState    <= SET_STOP;
                     end if;
                  else
                     sTxBitIndex    <= sTxBitIndex ;

                  end if;

               -----------------------------------------------------------------------------
               when SET_STOP  =>

                  if (sBitGenTransmitterClock = '1') then
                     sTxSerial      <= '1';                 --Stop Bit.
                     sTxState       <= DEL_STOP;
                  end if;

               -----------------------------------------------------------------------------
               when DEL_STOP  =>

                  if (sBitGenTransmitterPreClock = '1') then    -- Stop Bit gesendet
                     sTxTransmitterFreeInfo         <= '1';     -- war auskommentiert (110510 - DS)
                     sTxIdle                        <= '1';
                     sTxTransmitterFreeDelayCounter <= cTxTransmitterFreeDelayCounterMax;
                     sTxState                       <= IDLE;
                  end if;

         end case;
            --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            --                                                                  +
            --   Temporaere Variablen zuweisen.                                 +
            --                                                                  +
            --+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

         end if;

      end process pTransmitData;

      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      --                                                                 +
      --   Daten den zugehoerigen externen Ports zuweisen                +
      --                                                                 +
      --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      RxEnableStatus          <= sRxEnableStatus;
      TxSerial                <= sTxSerial AND sGenBreak;
      TxTransmitterFreeInfo   <= sTxTransmitterFreeInfo;
      TxIdle                  <= sTxIdle;
      TxDriverEnable          <= sTxDriverEnable;

end RTL;