LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--History
--dd.mm.yy  born (DS)
--dd.mm.yy  Code cleaned, Ports/Variables renamed (DS)
--11.07.08  MD_Host_NewHdShkNotCarried_Tri implemented (DS)
--21.07.08  WaitStateTimer implemented (DS)
--21.07.08  WaitStateTimer deleted (DS)
--30.07.08  don't care massages eliminated (DS)
--05.08.08  MDisEnabled implemented (DS)
--06.08.08  Run to PO_RESET if disabled implemented (DS)
--07.08.08  MD_USI_Watchdog implemented (DS)
--11.08.08  Error in MD_USI_Watchdog fixed, gUSIMergerDistributorWatchdogCounts implemented (DS)
--13.10.08  Highspeedkanal auf 32 Bit erweitert (DS)
--13.01.10  Wechsel von generic'integer auf generic'real und auf Quartus V9.0 (DS)
--17.05.10  Ausgabe der TunnelDaten und Handshake von Array auf Vektor umgestellt. Dadurch ist das Ausgabearray und die Wandlung
--          der Tunneldaten um 4 Byte im Array geschrumpft. -> Neue Signale sind "TunnelData" und "TunnelHandshake".
--          Grund: eine Wandlung dieser Daten ist nicht notwendig, da diese entweder schon als ASCII vorliegen (Daten)
--          oder einfacher per Bitaddition (Handshake) erzeugt werden koennen.
--          Umsteg auf "ieee.numeric_std.all;"  (DS)
--20.05.10  Das Tunnelbyte bei USI HighSpeed war (sofern keine Uebertragung stattgefunden hat) 0x00. Dies ist kein
--          gueltiges ASCII Zeichen. Jetzt ist es 0x30. (DS)
--09.08.10   Merger/Distributor ueberarbeitet und redundante Objekte entfernt. (DS)
--20.12.10  "Ext_Bits_In" und "Ext_Bits_Out" eingefuehrt. Das Handshakebyte benutzte bisweilen nur
--          die beiden unteren Bits des repraesentierten Nibbles. Die beiden freien Bit sind nun als
--          zusaetzliche Bits fuer erweiternde Kommunikation herausgefuehrt. (DS)
--19.02.13  Im 'sRxInterpretMemorizedUSIDataState = RECEIVE_USI_DATA_STREAM' wird 'sReceivedByteCounter >= cUSI_MaxStreamLength' getestet.
--          Ein HighSpeedStream hat 12 Bytes (STX + 8xASCII Daten + Tunnel + Handshake + ETX).
--          Wird der MD auf HighSpeed umgeschaltet und ist 'ReceiveBeforeSend' aktiv, muss der MD zuerst einen
--          HighSpeedString des Host empfangen, bevor er selbst das Senden beginnt. 'cUSI_MaxStreamLength' war 12, das fuehrte
--          dazu, dass die Bedingung 'sReceivedByteCounter >= cUSI_MaxStreamLength' bereits erfuellt war, bevor ETX empfangen wurde,
--          d.h. die Statemachine ist in den State 'WAIT_FOR_STX' zurueck gesprungen und erst dann wurde das ETX empfangen.
--          Da der Empfang des ETX im State 'RECEIVE_USI_DATA_STREAM' allerdings dafuer sorgt, dass der MD mit dem Senden von Daten ueberhaupt beginnt,
--          konnte der MD das Senden nicht starten, sofern ein empfangener HighSpeedString nach dem Umschalten in den HighSpeedModus das Senden des MD aktivieren sollte.
--          Ein kurzer Standard USI String (<12 Byte) hingegen hat das Senden aktiviert, da im String das ETX erkannt uwrde, wahrend sich die Statemachine noch
--          im State 'RECEIVE_USI_DATA_STREAM' befand.
--          Dies funktioniert nun richtig, indem auf ''sReceivedByteCounter = cUSI_MaxStreamLength' geprueft wird und das ETX nicht mehr zur eigentlichen
--          Streamlaenge gezaehlt wird.
--          Ausserdem wird beim Empfang von ETX nun geprueft, ob 'cUSI_MaxStreamLength' erfuellt ist, denn nur dann sollte das HighSpeedStreamen des
--          MD aktiviert werden.
--          'gUSIMergerDistributorWatchdogCounts' = 0 deaktiviert nun die WatchDog Funktion, da der Watchdog nur in der MFU Verwendnung findet und in Slavemodulen
--          das Modul 'AutoFallBack' im 'MUSIc-Core' die Aufgabe uebernimmt.
--          'DiagVector' aus dem Projekt entfernt.
--          'HighSpeedPort_NewDataStrobe' als neuen Ausgangsport angelegt. (DS)
--04.03.13  'HighSpeedPort_NewDataStrobe' auf 2 bit erweitert.
--          Bit[0] = Strobe HighSpeedData Receive,
--          Bit[1] = Strobe HighSpeedData Transmit (DS)
--29.05.13 'HighSpeedPort_NewDataPulse' als neuen Eingangsport und zueghoeriges Generic 'gUse_HighSpeedPort_NewDataPulse' angelegt.
--          Ist 'gUse_HighSpeedPort_NewDataPulse' gesetzt muss an 'HighSpeedPort_NewDataPulse' ein L->H Pulse neue Daten ankuendigen
--          damit der MD diese uebernimmt und transferiert, anderfalls erfolgt die Uebernahme wie bisher freilaufend. (DS)
--18.02.14  Meldung 'Ignored Power-Up Level option on the following registers' -> 'Critical Warning (18010): Register xxx will power up to GND/High'
--          durch gezieltes setzen eines Initwertes der betroffen Variable(n) beseitigt. (DS)
--15.09.14  'sHighSpeedPort_In' war nach RESET undefiniert, ist nun X"00000000". War 'gUse_HighSpeedPort_NewDataPulse = '1' aber es kam
--          zunaechst kein Pulse, wurde 'sRawSendHEXDataArray' immer mit [XX] geladen. Unscheon! (DS)

--          Total logic elements : 590  mit WatchdogCounts = 500  -> 573 mit WatchdogCounts = 0
--          Total registers      : 454       -"-                  -> 439

--23.02.16  'HighSpeedPort_NewDataStrobe[0]' welches den Empf. neuer HighSpeed Daten anzeigt war ein Takt zu frueh, d.h. es wurden erst
--          neue Daten signalisiert und einen Takt spaeter auch wirklich an 'HighSpeedPort_Out' ausgegeben. ( - DS)
--          >>>> Nachfolgendes ist zwar eingebaut, aber dann doch nicht aktiviert (auskommentiert und die vorherige Loesung noch aktiv),
--          >>>> da komischer Weise bei der Handshakeloesung hin und wieder das Laden von Parametern fehl schlaegt.
--          >>>> Die MFU bleibt dann in einer Schleife beim auslesen der getunnelten Daten haengen:
--          >>>> Neuer Port: 'USI_MD_USIRxFIFONewDataAtDataOut' signalisiert, dass mittels USI empf. Daten auf dem USI_RxFIFO sicher am USI
--          >>>> 'RxDataOut' anliegen und dadurch sicher in dem M/D an 'USI_MD_RxData_In' eingelesen werden koennen. Dadurch entfaellt die unsichere
--          >>>> Warteschleife mit 'sUSI_RxReadEnableWaitStateCounter'.
--          >>>> Neuer Port 'USI_MD_USITxFIFODataFetchedFromDataInFromMD' signalisiert, dass die vom MD an die USI ausgegeben daten zum
--          >>>> Versenden ueber die USI vom USI TxFIFO aufgenommen wurden.

--          Total logic elements          569
--          Total combinational functions 350
--          Dedicated logic registers     433
--          Total registers               433

--15.04.16  In der Statemachine'sSendUSIStreamState im State'POSSIBLY_CLEAR_NEW_HDSHK_NOT_CARRIED und State'when others
--          war in der if() Abfrage (sCarringNewHandshake <= '1'), muss aber korrekt "=" sein. Dies sorgte dafuer, dass die Bedingung IMMER
--          erfuellt war und damit 'MD_Host_NewHdShkNotCarried_Tri' zu frueh auf '0' gesetzt wurde. Dadurch
--          war der Sender faelschlich in der Lage ebenfalls zu frueh neue Daten anzulegen und die Strobe/Acknowledge
--          Eingange zu bedienen. Durch die Reduzierung von Takten wegen der Einfuehrung des erweiterten Handshakes zwischen
--          M/D und USI (wie unter 1.8 beschrieben) fuehrte dies mitunter zum Aufhaengen des M/D.
--          Ursache: 'sStrobLevelChanged' und 'sAcknowledgeLevelChanged' konnten sich aendern, obwohl der M/D
--          nicht selektiert war. Die Erkennungen sind nun vom Prozess 'pHEX2ASCII' nach 'pSendUSIStream' verlegt.
--          Port 'Ext_Bits_Out' war im Falle von M/D Disable Triste (warum auch immer?), ist nun nicht mehr der Fall.
--          'pControlUSIState' ist entfallen, es wird direkt auf 'MD_Enable' reagiert. (DS)

--          Total logic elements          540
--          Total combinational functions 335
--          Dedicated logic registers     411
--          Total registers               411


--21.08.20  Die Ueberarbeitete 1.9 von Mario Guenther getestet.
--          Marios Optimierung sind leider die versendeten Handshake-Signale "wegoptimiert" worden.
--          Sind jetzt wieder drin.
--          Neues 'gHighSpeedPort_ByteIndexForNewDataStrobeTransmit' Generic gibt an, bei welchem der 12 HighSpeed-Bytes die gerade verschickt werden das
--          'sHighSpeedPort_NewDataStrobeTransmit' Signal auf 'High' gesetzt wird.
--          Bisher geschah dies bei Byte[1] (Indexzaehlung bei 0 beginnend), also dem ersten konvertierten HighSpeed-Datenbyte nach STX.
--          Nun kann jedes beliebige Byte im HighSpee-Stream gewaehlt werden.
--          Per Standard ist das nun Byte[0], also STX.

--10.06.21  Erweiterung M/D auf bis zu 4 HighSpeed-Slots
--           Signale zum besseren Verstaendnis umbenannt:
--             HighSpeedPort_NewDataPulse                          ->  HighSpeedPort_AcceptNewDataRisingEdge
--             sHighSpeedPort_NewDataPulseArray                    ->  sHighSpeedPort_AcceptNewDataRisingEdgeArray
--             gUseHighSpeedPort_NewDataPulse                      ->  gUse_HighSpeedPort_AcceptNewDataRisingEdge
--             HighSpeedPort_NewDataStrobe                         ->  getrennt zu   HighSpeedPort_NewDataTransmittedPulse  und  HighSpeedPort_NewDataReceivedPulse
--             gHighSpeedPort_ByteIndexForNewDataStrobeTransmit    ->  gHighSpeedPort_ByteIndexForNewDataTransmittedPulse
--           neue Generics:
--             gNumberOfHighSpeedTransmitterSlots                  erweitert Anzahl der Ports von HighSpeedPort_In und HighSpeedPort_NewDataTransmittedStrobe
--             gNumberOfHighSpeedReceiverSlots                     erweitert Anzahl der Ports von HighSpeedPort_Out und HighSpeedPort_NewDataReceivedStrobe und HighSpeedPort_NewDataPulse
--           neue Input Ports
--             HighSpeedSlotFreeWheel                              HighSpeed-Daten freilaufend reihum oder einzeln
--             Select_HighSpeedSlot                                Auswahhl der jew. HighSpeed-Daten bei HighSpeedSlotFreeWheel = 0 (nicht freilaufend, sondern einzeln)
--           ehemalige Ports entfallen, nur noch interne Nutzung
--             Ext_Bits_In
--             Ext_Bits_Out
--           (MG)

--24.06.21  Etwas gestrafft, unnoetige States und Variablen entfernt. (DS)


--###########################################################################################################
--# Info                                                                                                    #
--# ----                                                                                                    #
--# <<< FreeWheel >>>                                                                                       #
--# Das Einlesen der Daten an HighSpeedPort_In() erfolgt, wenn das STX bereits verschickt wird.             #
--# Vom Einlesen der Daten bis zur Weitergabe des ersten Datenbytes an den TxFIFO vergehen 0,41us.          #
--# Insgesmat dauert es vom Erfassen der Daten bis zu deren Bereitstellung auf der Empfaengerseite 8,47us.  #
--# <<< _AcceptNewDataRisingEdge >>>                                                                        #
--# Die kuerzeste erreichbare Zeitspanne vom '_AcceptNewDataRisingEdge' bis zur Bereitstellung auf der      #
--# Empfaengerseite betraegt 8,51us. Die Flanke erfolgt exakt EINEN Takt bevor HEX->ASCII Konvertierung der #
--# der Daten begonnen wird.                                                                                #
--# Im unguenstigsten Moment betraegt diese Zeitspanne 14,5us. Die Flanke erfolgt exakt in dem Takt in      #
--# dem die HEX->ASCII Konvertierung der vorherigen Daten begonnen wird.                                    #
--###########################################################################################################

entity USI_Merger_Distributor is

   generic
   (
      gUSIMergerDistributorWatchdogCounts                : integer := 0;
      gNumberOfHighSpeedTransmitterSlots                 : integer := 1;  -- 1 to 4
      gNumberOfHighSpeedReceiverSlots                    : integer := 1;  -- 1 to 4
      gUse_HighSpeedPort_AcceptNewDataRisingEdge         : bit_vector(3 downto 0) := "0000";
      gHighSpeedPort_ByteIndexForNewDataTransmittedPulse : unsigned(15 downto 0) := X"0000" --Sets the time the HighSpeedPort_NewDataTransmittedPulse is active in relation to serial transfer
   );

   port
   (
      Clock                                        : in  std_logic;
      Reset                                        : in  std_logic;

      MD_Enable                                    : in  std_logic;                       --Wenn High ist Merger/Distributor aktiv
      MD_Select                                    : in  std_logic;                       --Wenn High sind _Tunnel_Data_In/Out[7..0], _Acknow_In/Out, Strobe_In/Out aktiv
                                                                                          --andernfalls 0(In) bzw. Hi-Z(Out)

      ReceiveFirstBeforeSend                       : in  std_logic;                       --Wenn High muss zunaechst ein String empf. werden bevor das
                                                                                          --Modul mit dem Senden beginnt (fuer Slave)

      Host_MD_TxData                               : in  std_logic_vector(7 downto 0);    --Daten von CPU -> USI, wenn Merger/Distributor passiv (Transparent)

      Host_MD_TxWriteEnable                        : in  std_logic;                       --USI TxWriteEnable von CPU->USI (Transparent)
      Host_MD_RxReadEnable                         : in  std_logic;                       --USI RxReadEnable  von CPU->USI (Transparent)

      MD_Host_TunnelData_Tri                       : out std_logic_vector (7 downto 0);   --Daten fuer die CPU (Aktivmodus)
      Host_MD_TunnelData                           : in  std_logic_vector (7 downto 0);   --Daten von der CPU  (Aktivmodus)

      Host_MD_Strobe                               : in  std_logic;                       --CPU hat neue Daten fuer Slave (Aktivmodus)
      MD_Host_Strobe_Tri                           : out std_logic;                       --Slave hat neue Daten fuer CPU (Aktivmodus)

      Host_MD_Acknowledge                          : in  std_logic;                       --CPU hat Daten abgeholt       (Aktivmodus)
      MD_Host_Acknowledge_Tri                      : out std_logic;                       --Slave hat Daten abgeholt     (Aktivmodus)

      MD_Host_NewHdShkNotCarried_Tri               : out std_logic;                       --'1' wenn Strobe oder Ack. Pegel wechseln und noch nicht gesendet sind

      USI_MD_RxData                                : in  std_logic_vector(7 downto 0);    --Paralleldaten vom USI Modul
      MD_USI_TxData                                : out std_logic_vector(7 downto 0);    --Paralleldaten zum USI Modul

      USI_MD_TxFIFOFull                            : in  std_logic;                       --wenn High, warten bis wieder Platz im AusgabeFIFO ist
      USI_MD_RxFIFOnEmpty                          : in  std_logic;                       --wenn High, Daten am Receiver vorhanden

      USI_MD_USITxFIFODataFetchedFromDataInFromMD  : in  std_logic;                       --Der USI TxFIFO hat die vom M/D gelieferten Daten zum Senden uebernommen
      USI_MD_USIRxFIFONewDataAtDataOutForMD        : in  std_logic;                       --neue Daten im USI RxFIFO und damit auch an 'USI_RxDataOut' und 'USI_MD_RxData'

      MD_USI_TxGetDataFromTxData                   : out std_logic;                       --L->H Daten von USI_DataOut in Transmitter FIFO
      MD_USI_RxPutDataToRxData                     : out std_logic;                       --L->H Daten von Receiver FIFO in USI_DataIn einlesen

      HighSpeedPort_Out                            : out std_logic_vector ((gNumberOfHighSpeedReceiverSlots*32)-1 downto 0);  --Hochgeschwindigkeitsparallelport Empfaenger
      HighSpeedPort_In                             : in  std_logic_vector ((gNumberOfHighSpeedTransmitterSlots*32)-1 downto 0);  --Hochgeschwindigkeitsparallelport Sender

      HighSpeedPort_AcceptNewDataRisingEdge        : in  std_logic_vector(gNumberOfHighSpeedTransmitterSlots-1 downto 0); --siehe V1.7

      HighSpeedPort_NewDataReceivedPulse           : out std_logic_vector(gNumberOfHighSpeedReceiverSlots-1 downto 0); --Strobepulse, wenn neue Daten an 'HighSpeedPort_Out'
      HighSpeedPort_NewDataTransmittedPulse        : out std_logic_vector(gNumberOfHighSpeedTransmitterSlots-1 downto 0); --Strobepulse, wenn neue Daten vom 'HighSpeedPort_In'versendet werden

      MDisEnabled                                  : out std_logic;                       --Wenn High->Rueckmeldung, dass MD in HighSpeed ist.
      MD_USI_Watchdog                              : out std_logic;                       --Wenn High->Rueckmeldung, dass Highspeed Kommunikation zw. Host und Modul unterbrochen

      HighSpeedSlotFreeWheel                       : in  std_logic;                       -- HighSpeed-Daten freilaufend reihum (bei '1') oder einzeln (bei '0')
      Select_HighSpeedSlot                         : in  std_logic_vector(1 downto 0)     -- Auswahhl der jew. HighSpeed-Daten bei HighSpeedSlotFreeWheel = 0 (nicht freilaufend, sondern einzeln) (1 ... 4:  "00", "01", "10" oder "11")
   );


end  USI_Merger_Distributor;

architecture RTL of USI_Merger_Distributor is

   --Signale, Types und Konstanten kommen hier her

   signal   sMD_USI_TxData_Out      : std_logic_vector (7 downto 0);

   signal   sHost_Strobe_Out        : std_logic;
   signal   sHost_Acknowledge_Out   : std_logic;

   type     tMemoryArray8BitSend       is array (0 to 9) of std_logic_vector(7 downto 0);
   type     tMemoryArray8BitRaw        is array (0 to 4) of std_logic_vector(7 downto 0);
   type     tMemoryArray8BitConverted  is array (0 to 9) of std_logic_vector(7 downto 0);

   signal   sSendDataArray                : tMemoryArray8BitSend;
   signal   sRawSendHEXDataArray          : tMemoryArray8BitRaw;
   signal   sConvertedSendASCIIDataArray  : tMemoryArray8BitConverted;

   signal   sHighSpeedPort_NewDataReceivedPulse    : std_logic;
   signal   sHighSpeedPort_NewDataTransmittedPulse : std_logic;

   signal   sHighSpeedCurrentTransmitter  : integer range 3 downto 0 := 0;  -- Zeigt den aktuell laufenden HS Transmitter Slot (0...3)
   signal   sHighSpeedCurrentReceiver     : integer range 3 downto 0 := 0;  -- Zeigt den aktuell laufenden HS Receiver Slot (0...3)

-------------------------------------------------------------------------
-- Signale, Variable, Konstanten, States fuer Prozess : pRxMemorizeReceivedUSIData
-------------------------------------------------------------------------
-- Statemachine fuer pRxMemorizeReceivedUSIData
   type     tRxMemorizeReceivedUSIDataState is (
                                                WAIT_FOR_DATA,
                                                SET_RX_READ_ENABLE,
                                                READ_DATA
                                                );
   signal   sRxMemorizeReceivedUSIDataState : tRxMemorizeReceivedUSIDataState;


   signal   sMD_USI_RxReadEnable                   : std_logic;
   signal   sMemorizedUSIDataReady                 : std_logic;
   signal   sUSI_RxReadEnableWaitStateCounter      : integer range 6 downto 0 := 0;
   signal   sUSI_MD_RxData_In                      : std_logic_vector (7 downto 0);

   constant cUSI_RxReadEnableWaitStateCounterValue : integer range 6 downto 0 := 6;

-------------------------------------------------------------------------
-- Signale, Variable, Konstanten, States fuer Prozess : pRxInterpretMemorizedUSIData
-------------------------------------------------------------------------

-- Statemachine fuer pRxInterpretMemorizedUSIData

   type     tRxInterpretMemorizedUSIDataState is (
                                                      WAIT_FOR_STX,
                                                      RECEIVE_USI_DATA_STREAM,
                                                      FEED_LATCHES
                                                 );
   signal   sRxInterpretMemorizedUSIDataState  : tRxInterpretMemorizedUSIDataState;

   signal   sReceivedByteCounter          : integer range  200 downto 0 := 0; --zaehlt die Anzahl der empf. Byte um ggf. einen Timeout zu erzeugen

   signal   sHighSpeedDataTemp            : std_logic_vector (31 downto 0);
   signal   sHighSpeedData_Out            : std_logic_vector (31 downto 0);

   signal   sSelected_Rx_HighSpeedSlot    : std_logic_vector (1 downto 0);

   signal   sMD_Host_TunnelData_OutTemp   : std_logic_vector (7 downto 0);
   signal   sMD_Host_TunnelData_Out       : std_logic_vector (7 downto 0);

   signal   sDecodedNibble                : std_logic_vector (3 downto 0);

   signal   sMD_Host_Strobe_OutTemp       : std_logic;
   signal   sMD_Host_Strobe_Out           : std_logic;

   signal   sMD_Host_Acknowledge_OutTemp  : std_logic;
   signal   sMD_Host_Acknowledge_Out      : std_logic;

   --maximal zulaessige Streamlaenge, dient dem Timeout falls ein ETX nicht erkannt wurde
   constant cUSI_MaxStreamLength          : integer := 10;

   signal   sStreamingEnable              : std_logic;

-------------------------------------------------------------------------
-- Signale, Variable, Konstanten, States fuer Prozess : pSendUSIStream
-------------------------------------------------------------------------

-- Statemachine fuer pSendUSIStream

   type     tSendUSIStreamState is (
                                       WAIT_FOR_ENABLING_SENDER,
                                       PREPARE_SOME_SIGNAL_STUFF_BEFORE_TRANSMITTING_FIRST_HS_DATA_BYTE,
                                       WAIT_HEX2ASCII_CONVERSION_DONE,
                                       COMMIT_DATA_TO_SEND_DATA_ARRAY,
                                       COMMIT_DATA_BYTE_BY_BYTE_TO_SENDER,
                                       WAIT_FOR_TX_FIFO_DATA_FETCHED,
                                       INCREMENT_INDEX,
                                       POSSIBLY_CLEAR_NEW_HDSHK_NOT_CARRIED
                                    );

   signal   sSendUSIStreamState : tSendUSIStreamState;


   signal   sMD_USI_TxWriteEnable         : std_logic;
   signal   sUSISendStreamWaitStates      : integer range 7 downto 0 := 0;

   constant cUSISendStreamWaitStatesValue : integer range 7 downto 0 := 7;

   signal   sUSISendByteIndex             : integer range 20 downto 0 := 0;
   constant cUSISendByteIndexMaxValue     : integer := 11; --Maximale Streamlaenge beim Senden

   signal   sCarringNewHandshake          : std_logic;
   signal   sMD_Host_NewHdShkNotCarried   : std_logic;


-------------------------------------------------------------------------
-- Signale, Variable, Konstanten, States fuer Prozess : pHEX2ASCII
-------------------------------------------------------------------------

   type     tHex2ASCIIState is ( PREPARE_READ_PORTS,
                                 READ_PORTS,
                                 CONVERT_ASCII,
                                 WAIT_FOR_DATA_GRAB
                               );

   signal   sHex2ASCIIState : tHex2ASCIIState;

   signal   sSendDataArrayValid                         : std_logic;

   signal   sHighSpeedPort_In                           : std_logic_vector((gNumberOfHighSpeedTransmitterSlots*32)-1 downto 0);    -- latched HighSpeed-Data
   signal   sHost_MD_TunnelData_In                      : std_logic_vector (7 downto 0);

   signal   sHost_MD_Strobe_In_Mem                      : std_logic;
   signal   sHost_MD_Acknowledge_In_Mem                 : std_logic;

   signal   sStrobeLevelChanged                         : std_logic;
   signal   sAcknowledgeLevelChanged                    : std_logic;

   signal   sStartNextHex2ASCIIConversion               : std_logic := '0';

   signal   sTunnelData                                 : std_logic_vector(7 downto 0);
   signal   sSelected_Tx_HighSpeedSlot                  : std_logic_vector(1 downto 0);

   signal   sHighSpeedPort_AcceptNewDataRisingEdgeArray : std_logic_vector((gNumberOfHighSpeedTransmitterSlots*2)-1 downto 0);  -- gNumberOfHighSpeedTransmitterSlots x 2 Bit fuer Edge Detection

   --Umwandlung HEX in ASCII
   type     tHEX2ASCII  is array (0 to 15) of std_logic_vector(7 downto 0);
   constant cHEX2ASCII : tHEX2ASCII := (X"30",X"31",X"32",X"33",X"34",X"35",X"36",X"37",X"38",X"39",X"41",X"42",X"43",X"44",X"45",X"46");


-------------------------------------------------------------------------
-- Signale, Variable, Konstanten, States fuer Prozess : pMD_USI_Watchdog
-------------------------------------------------------------------------

signal   sMD_USI_WatchdogCounter             : integer range 0 to gUSIMergerDistributorWatchdogCounts; --uebersteigt die Anzahl Sendzyklen die Anzahl Empf. zyklen um diesen Wert wird MD_USI_Watchdog high
constant cMD_USI_WatchdogCounterMax          : integer := gUSIMergerDistributorWatchdogCounts;

signal   sMD_USI_Watchdog                    : std_logic;

signal   sMD_USI_TxWriteEnableEdgeDetArray   : std_logic_vector(1 downto 0);

signal   sMD_USI_TxWriteEnableEdgeDet        : std_logic;


constant cTestCounterMax   : integer := 20;

begin


-------------------------------------------------------------------
-- MD_USI_Watchdog
-------------------------------------------------------------------
--****************** INFO *****************
--Die Watchdog Funktion wird nur in der MFU verwendet, in Slavemodulen ist sie obsolet seit mUSIc, da es das Modul
--'AutoFallBack' gibt, dass genau diese Funktion global uebernimmt und die USI in die Standardbaudrate zurueck setzt.
--*****************************************

--Kann das Modul keine Highspeeddaten mehr empfangen (Leitungsunterbrechung, Fehler usw.) wird MD_USI_Watchdog aktiv.
--Wenn das USI Modul keine Highspeeddaten mehr empfaengt, wird vom M/D das Signal 'sMD_USI_RxReadEnable' nicht mehr bedient, weil
--USI_MD_RxFIFOnEmpty low bleibt.
--Bei jedem Sendevorgang des M/D wird der Ausgang 'MD_USI_TxGetDataFromTxData' jedoch weiter getriggert.
--Dabei wird ein Zaehler inkrementiert, den nur 'sMD_USI_RxReadEnable' loeschen kann.
--Uebersteigt der Zaehlerwert den 'sMD_USI_WatchdogCounterMax' Wert,
--wird der Ausgang MD_USI_Watchdog High und die Peripherie/CPU/das USI Modul dadurch in Kenntnis gesetzt.


   --Flankenerkennung sMD_USI_TxWriteEnable           --Daten ueber das USI senden
   pMD_USI_TxWriteEnableEdgeDet: process (Clock, Reset)
   begin
     if (Reset = '1') then
         sMD_USI_TxWriteEnableEdgeDetArray <= (others => '0') ;
         sMD_USI_TxWriteEnableEdgeDet      <= '0';
     elsif (rising_edge(Clock)) then

         if (gUSIMergerDistributorWatchdogCounts > 0) then

            sMD_USI_TxWriteEnableEdgeDetArray(1 downto 0) <= (sMD_USI_TxWriteEnableEdgeDetArray(0) & sMD_USI_TxWriteEnable );

            if (sMD_USI_TxWriteEnableEdgeDetArray = "01") then --steigende Flanke
               sMD_USI_TxWriteEnableEdgeDet <= '1';
            else
               sMD_USI_TxWriteEnableEdgeDet <= '0';
            end if;
         else
            sMD_USI_TxWriteEnableEdgeDetArray <= (others => '0') ;
            sMD_USI_TxWriteEnableEdgeDet      <= '0';
         end if;

     end if;

   end process pMD_USI_TxWriteEnableEdgeDet;

   --Problem: Hin und wieder faellt das M/D, bzw. USI Modul nicht in den Defaultmodus wenn HighSpeed abrupt unterbrochen wird (schon geloest???)

   --MD_USI_Watchdog Zaehler
   pMD_USI_Watchdog : process (Reset, Clock)
   begin
      if (Reset = '1') then
         sMD_USI_WatchdogCounter <=  0;
         sMD_USI_Watchdog        <= '0';
      else
         if (rising_edge(Clock)) then

            if (gUSIMergerDistributorWatchdogCounts > 0) then

               if (MD_Enable = '1') then                                            --Highspeed Streamingmode aktiv

                  if (sMD_USI_RxReadEnable = '0') then                              --M/D bedient diese Leitung nicht, es sind z. Zt. keine Daten beim USI Modul abholbereit

                     if (sMD_USI_WatchdogCounter < cMD_USI_WatchdogCounterMax) then --Maximalwert nicht erreicht

                        if (sMD_USI_TxWriteEnableEdgeDet = '1') then                --Zaehler inkrement
                           sMD_USI_WatchdogCounter <= sMD_USI_WatchdogCounter + 1;
                        else
                           sMD_USI_WatchdogCounter <= sMD_USI_WatchdogCounter;
                        end if;

                     else

                        sMD_USI_WatchdogCounter <= sMD_USI_WatchdogCounter;
                        sMD_USI_Watchdog        <= '1';
                     end if;  --else -> if (sMD_USI_WatchdogCounter < cMD_USI_WatchdogCounterMax) then
                  else

                     sMD_USI_Watchdog        <= '0';
                     sMD_USI_WatchdogCounter <=  0;
                  end if;  --else -> if (sMD_USI_RxReadEnable = '0') then
               else

                  sMD_USI_Watchdog        <= '0';
                  sMD_USI_WatchdogCounter <= 0;

               end if;  --else -> if (MD_Enable = '1') then
            else
               sMD_USI_WatchdogCounter <=  0;
               sMD_USI_Watchdog        <= '0';
            end if;  --else -> if (gUSIMergerDistributorWatchdogCounts > 0) then
         end if;  --else -> if (Clock'event AND Clock = '1') then
      end if;  --else -> if (Reset = '1') then
   end process pMD_USI_Watchdog;

   MD_USI_Watchdog <=  sMD_USI_Watchdog;

-------------------------------------------------------------------
-- Portzuweisungen (seit Version 1.9 OHNE Clock)
-------------------------------------------------------------------

   --pAssignment : process(Clock, Reset)
   pAssignment : process(Reset, MD_Enable, Host_MD_TxData, Host_MD_TxWriteEnable, Host_MD_RxReadEnable, sMD_USI_TxData_Out, sMD_USI_TxWriteEnable, sMD_USI_RxReadEnable)
   begin
      if (Reset = '1') then
         MD_USI_TxData    <= (others =>'0');
         MD_USI_TxGetDataFromTxData <= '0';
         MD_USI_RxPutDataToRxData  <= '0';
      else
         if (MD_Enable = '0') then                 --Merger/Distributor ist passiv, Daten von USI werden direkt an die CPU weitergeleitet
            MD_USI_TxData              <= Host_MD_TxData;
            MD_USI_TxGetDataFromTxData <= Host_MD_TxWriteEnable;
            MD_USI_RxPutDataToRxData   <= Host_MD_RxReadEnable;

         else                                      --Merger/Distributor ist aktiv, CPU Ports muessen Hi-Z werden, damit CPU ggf. auf weitere USI zugreifen kann
            MD_USI_TxData              <= sMD_USI_TxData_Out;
            MD_USI_TxGetDataFromTxData <= sMD_USI_TxWriteEnable;
            MD_USI_RxPutDataToRxData   <= sMD_USI_RxReadEnable;

         end if;
      end if;
   end process pAssignment;


---------------------------------------------------------------------
-- Daten vom USI Modul abholen und in USI_Merger_Distributor einlesen
-- Dieser Prozess ist nur aktiv, sofern der M/D auf TRANSFER (MD_Enable = '1') steht,
-- andernfalls werden die Daten einfach durch den M/D hindurch gereicht. (Transparent)
---------------------------------------------------------------------

   pRxMemorizeReceivedUSIData : process (Clock, Reset)
   begin
      if (Reset = '1') then

         sMD_USI_RxReadEnable                <= '0';
         sMemorizedUSIDataReady              <= '0';
         sRxMemorizeReceivedUSIDataState     <= WAIT_FOR_DATA;

      else
         if (rising_edge(Clock)) then

            --Abbruch wenn Merger/Distributor von USI_Control abgeschaltet wird
            if (MD_Enable = '0') then
               sMD_USI_RxReadEnable                <= '0';
               sMemorizedUSIDataReady              <= '0';
               sRxMemorizeReceivedUSIDataState     <= WAIT_FOR_DATA;
            else

               case (sRxMemorizeReceivedUSIDataState) is

                  -----------------------------------------------------------------------------
                  when WAIT_FOR_DATA =>

                     if (MD_Enable = '1') then                          --Ist M/D aktiv (MD_Enable = '1', TRANSFER), warten bis
                        sMD_USI_RxReadEnable                <= '0';     --Daten im RxFIFO des USI Moduls sind.
                        sMemorizedUSIDataReady              <= '0';

                        if (USI_MD_RxFIFOnEmpty = '1') then            --Es sind Daten im Receiver FIFO des USI Moduls vorhanden
                           sRxMemorizeReceivedUSIDataState <= SET_RX_READ_ENABLE;
                        end if ;
                     end if;

                  -----------------------------------------------------------------------------
                  when SET_RX_READ_ENABLE =>                            --Der M/D holt diese ab, dazu...

                     sMD_USI_RxReadEnable    <= '1';                    --...Leseanforderung setzen und warten bis Daten sicher an USI_DataIn anliegen...
                     sMemorizedUSIDataReady  <= '0';                    --teilt spaeter 'pRxInterpretMemorizedUSIData' mit, dass neue Daten vorhanden sind

                     -- Problem:
                     -- ========
                     -- Wird 'USI_MD_USIRxFIFONewDataAtDataOutForMD' nicht '1', bleibt 'sSendUSIStreamState' in diesem State,
                     -- bis 'MD_Enable = '0'' wird.
                     -- Ggf. sollte daher ein Timeout-Zaehler eingebaut werden

                     if (USI_MD_USIRxFIFONewDataAtDataOutForMD = '1') then
                        sRxMemorizeReceivedUSIDataState <= READ_DATA;
                     else
                        sRxMemorizeReceivedUSIDataState <= SET_RX_READ_ENABLE;
                     end if;

                  -----------------------------------------------------------------------------
                  when READ_DATA =>                                     --...dann die Daten einlesen.

                     sUSI_MD_RxData_In    <= USI_MD_RxData;             --Daten an USI_DataIn einlesen
                     sMD_USI_RxReadEnable <= '0' ;                      --Leseanforderung zurueck nehmen

                     sMemorizedUSIDataReady              <= '1';        --'pRxInterpretMemorizedUSIData' mitteilen, dass neue Daten vorhanden sind
                     sRxMemorizeReceivedUSIDataState     <= WAIT_FOR_DATA;

                  -----------------------------------------------------------------------------
                  when others =>

                     sRxMemorizeReceivedUSIDataState <= WAIT_FOR_DATA ;

               end case;   --case (sRxMemorizeReceivedUSIDataState) is

            end if;  --if (MD_Enable = '0') then

         end if ;

      end if ;

   end process pRxMemorizeReceivedUSIData;


-------------------------------------------------------------------
-- Auswerten des empf. Streams und Zuweisung an die Ports
-- Ein HS Stream besteht aus 12 Bytes
-- STX(1), HSBytes1..4(8), TunnelByte(1), Strob/Ack(1), ETX(1)
-- Zusaetzlich werden die 'Ext_Bits' empfangen.
-- Diese sind im 'HandshakeASCIIByte', rep. im 'HandshakeHEXNibble' in
-- den oberen 2 Bit codiert.
-------------------------------------------------------------------
   pRxInterpretMemorizedUSIData : process (Clock, Reset, sHighSpeedCurrentReceiver, sHighSpeedData_Out)
   begin
      if (Reset = '1') then
         sReceivedByteCounter                <= 0;  --zaehlt die Anzahl der empf. Byte um ggf. einen Timeout zu erzeugen

         sMD_Host_TunnelData_OutTemp         <= (others => '0');
         sMD_Host_TunnelData_Out             <= (others => '0');

         sHighSpeedDataTemp                  <= (others => '0');
         sHighSpeedData_Out                  <= (others => '0');

         HighSpeedPort_Out                   <= (others => '0');

         sSelected_Rx_HighSpeedSlot          <= (others => '0');

         sMD_Host_Acknowledge_OutTemp        <= '0';
         sMD_Host_Acknowledge_Out            <= '0';

         sMD_Host_Strobe_OutTemp             <= '0';
         sMD_Host_Strobe_Out                 <= '0';

         sHighSpeedPort_NewDataReceivedPulse <=  '0';

         sDecodedNibble                      <= (others => '0');

         sStreamingEnable                    <= '0';

         sRxInterpretMemorizedUSIDataState   <= WAIT_FOR_STX;
      else
         if (rising_edge(Clock)) then

            --Abbruch wenn Merger/Distributor von USI_Control abgeschaltet wird
            if (MD_Enable = '0') then

               sReceivedByteCounter                <= 0;  --zaehlt die Anzahl der empf. Byte um ggf. einen Timeout zu erzeugen

               sMD_Host_TunnelData_OutTemp         <= (others => '0');
               sMD_Host_TunnelData_Out             <= (others => '0');

               sHighSpeedDataTemp                  <= (others => '0');
               sHighSpeedData_Out                  <= (others => '0');

               sSelected_Rx_HighSpeedSlot          <= (others => '0');

               sMD_Host_Acknowledge_OutTemp        <= '0';
               sMD_Host_Acknowledge_Out            <= '0';

               sMD_Host_Strobe_OutTemp             <= '0';
               sMD_Host_Strobe_Out                 <= '0';

               sHighSpeedPort_NewDataReceivedPulse <= '0';

               sDecodedNibble                      <= (others => '0');

               sStreamingEnable                    <= '0';

               sRxInterpretMemorizedUSIDataState   <= WAIT_FOR_STX;
            else

               case (sRxInterpretMemorizedUSIDataState) is

                  -----------------------------------------------------------------------------
                  when WAIT_FOR_STX =>                                                             --warten dass StartOfText erkannt wird

                     sReceivedByteCounter                <= 0;
                     sHighSpeedPort_NewDataReceivedPulse <= '0';

                     if ( (sUSI_MD_RxData_In = X"02") and (sMemorizedUSIDataReady = '1') ) then    --StateMachine : pRxMemorizeReceivedUSIData  (neue Daten vorhanden)
                        sRxInterpretMemorizedUSIDataState <= RECEIVE_USI_DATA_STREAM;              --STX wurde erkannt
                     else
                        sRxInterpretMemorizedUSIDataState <= WAIT_FOR_STX;
                     end if ;

                  -----------------------------------------------------------------------------
                  when RECEIVE_USI_DATA_STREAM =>

                     if (sMemorizedUSIDataReady = '1') then                                  --weiteres Byte empfangen

                        if (sReceivedByteCounter > cUSI_MaxStreamLength) then                --maximal zulaessige Streamlaenge ueberschritten (ETX nicht erkannt)
                           sRxInterpretMemorizedUSIDataState  <= WAIT_FOR_STX;               --wieder auf STX warten
                        else

                           if (sUSI_MD_RxData_In = X"03") then                               --ETX erkannt

                              if (sReceivedByteCounter = cUSI_MaxStreamLength) then

                                 sHighSpeedData_Out  <= sHighSpeedDataTemp;                  --Den Ausgabelatches zuweisen

                                 case (sSelected_Rx_HighSpeedSlot) is
                                   when "00" =>
                                     sHighSpeedCurrentReceiver <= 0;
                                   when "01" =>
                                     sHighSpeedCurrentReceiver <= 1;
                                   when "10" =>
                                     sHighSpeedCurrentReceiver <= 2;
                                   when "11" =>
                                     sHighSpeedCurrentReceiver <= 3;
                                   when others =>
                                     sHighSpeedCurrentReceiver <= 0;
                                 end case;

                                 sMD_Host_TunnelData_Out             <= sMD_Host_TunnelData_OutTemp;
                                 sMD_Host_Strobe_Out                 <= sMD_Host_Strobe_OutTemp;
                                 sMD_Host_Acknowledge_Out            <= sMD_Host_Acknowledge_OutTemp;
                                 sHighSpeedPort_NewDataReceivedPulse <= '1';

                                 sStreamingEnable                    <= '1';                 --M/D hat Daten erhalten und darf nun auch Streamen (siehe auch 'pSendUSIStream')

                              else                                                           --sonst-> nichts tun
                                 sHighSpeedData_Out                  <= sHighSpeedData_Out;
                                 sSelected_Rx_HighSpeedSlot          <= sSelected_Rx_HighSpeedSlot;
                                 sMD_Host_TunnelData_Out             <= sMD_Host_TunnelData_Out;
                                 sMD_Host_Strobe_Out                 <= sMD_Host_Strobe_Out;
                                 sMD_Host_Acknowledge_Out            <= sMD_Host_Acknowledge_Out;
                                 sHighSpeedPort_NewDataReceivedPulse <= '0';

                                 sStreamingEnable                    <= sStreamingEnable;

                              end if;

                              sRxInterpretMemorizedUSIDataState      <= WAIT_FOR_STX;

                           else                                                              --Die empf. Daten werden nun unmittelbar von ASCII->Hex gewandelt,
                                                                                             --waehrend ggf. weitere Daten empfangen werden

                              if ( (sUSI_MD_RxData_In /= X"03") AND (sReceivedByteCounter = cUSI_MaxStreamLength) ) then
                                 sRxInterpretMemorizedUSIDataState  <= WAIT_FOR_STX;
                              else

                                 sReceivedByteCounter <= sReceivedByteCounter + 1 ;

                                 case (sUSI_MD_RxData_In) is

                                    when X"30" =>
                                       sDecodedNibble <= X"0" ;
                                    when X"31" =>
                                       sDecodedNibble <= X"1" ;
                                    when X"32" =>
                                       sDecodedNibble <= X"2" ;
                                    when X"33" =>
                                       sDecodedNibble <= X"3" ;
                                    when X"34" =>
                                       sDecodedNibble <= X"4" ;
                                    when X"35" =>
                                       sDecodedNibble <= X"5" ;
                                    when X"36" =>
                                       sDecodedNibble <= X"6" ;
                                    when X"37" =>
                                       sDecodedNibble <= X"7" ;
                                    when X"38" =>
                                       sDecodedNibble <= X"8" ;
                                    when X"39" =>
                                       sDecodedNibble <= X"9" ;
                                    when X"41" =>
                                       sDecodedNibble <= X"A" ;
                                    when X"42" =>
                                       sDecodedNibble <= X"B" ;
                                    when X"43" =>
                                       sDecodedNibble <= X"C" ;
                                    when X"44" =>
                                       sDecodedNibble <= X"D" ;
                                    when X"45" =>
                                       sDecodedNibble <= X"E" ;
                                    when X"46" =>
                                       sDecodedNibble <= X"F" ;
                                    when others => null ;

                                 end case;

                                 sRxInterpretMemorizedUSIDataState     <= FEED_LATCHES;

                              end if;  --else -> if ( (sUSI_MD_RxData_In /= X"03") AND (sReceivedByteCounter = cUSI_MaxStreamLength) ) then

                           end if;  --else -> if (sUSI_MD_RxData_In = X"03") then

                        end if;  --else -> if (sReceivedByteCounter > cUSI_MaxStreamLength) then

                     else
                        sReceivedByteCounter                <= sReceivedByteCounter;
                        sRxInterpretMemorizedUSIDataState   <= RECEIVE_USI_DATA_STREAM;
                     end if;  --else -> if (sMemorizedUSIDataReady = '1') then

                  -----------------------------------------------------------------------------
                  when FEED_LATCHES =>

                        case (sReceivedByteCounter) is

                           when 1 =>
                              sHighSpeedDataTemp(31 downto 28) <= sDecodedNibble;            --HighSpeed Daten muessen von ASCII nach Hex gewandelt werden
                           when 2 =>
                              sHighSpeedDataTemp(27 downto 24) <= sDecodedNibble;
                           when 3 =>
                              sHighSpeedDataTemp(23 downto 20) <= sDecodedNibble;
                           when 4 =>
                              sHighSpeedDataTemp(19 downto 16) <= sDecodedNibble;
                           when 5 =>
                              sHighSpeedDataTemp(15 downto 12) <= sDecodedNibble;
                           when 6 =>
                              sHighSpeedDataTemp(11 downto  8) <= sDecodedNibble;
                           when 7 =>
                              sHighSpeedDataTemp( 7 downto  4) <= sDecodedNibble;
                           when 8 =>
                              sHighSpeedDataTemp( 3 downto  0) <= sDecodedNibble;
                           when 9 =>
                              sMD_Host_TunnelData_OutTemp <= sUSI_MD_RxData_In;              --TunnelByte (kommt als ASCII und wird als solches weiter verarbeitet - keine Konvertierung!)
                           when 10  =>                                                       --Strobe/Acknowledge und Ext_Bits_Out
                              sMD_Host_Strobe_OutTemp         <= sDecodedNibble(0);          --Bit(0)    - Strobe
                              sMD_Host_Acknowledge_OutTemp    <= sDecodedNibble(1);          --Bit(1)    - Ack
                              sSelected_Rx_HighSpeedSlot      <= sDecodedNibble(3 downto 2); --Bit(3..2) - Ext_Bits_Out

                           when others => null;

                        end case;

                        if (sReceivedByteCounter = cUSI_MaxStreamLength) then

                          -- Ist fuer den auszugebenden Port (entspr. Vorgabe der empfangenen sSelected_Rx_HighSpeedSlot) keine Receiver-Slot vorhanden (gNumberOfHighSpeedReceiverSlots zu klein)
                          -- so wird der empfangene Wert verworfen

                          case ( sDecodedNibble(3 downto 2) ) is    -- sSelected_Rx_HighSpeedSlot

                            when "00" =>    -- Uebertragung kam vom 1. Port und den gibt es immer
                              sRxInterpretMemorizedUSIDataState     <= RECEIVE_USI_DATA_STREAM; -- normale Ausgabe der HS-Daten
                            when "01" =>    -- Uebertragung kam vom 2. Port
                              if (gNumberOfHighSpeedReceiverSlots < 2) then sRxInterpretMemorizedUSIDataState     <= WAIT_FOR_STX;            -- keine Ausgabe, warten auf naechsten Stream
                              else                                          sRxInterpretMemorizedUSIDataState     <= RECEIVE_USI_DATA_STREAM; -- normale Ausgabe der HS-Daten
                              end if;
                            when "10" =>    -- Uebertragung kam vom 3. Port
                              if (gNumberOfHighSpeedReceiverSlots < 3) then sRxInterpretMemorizedUSIDataState     <= WAIT_FOR_STX;            -- keine Ausgabe, warten auf naechsten Stream
                              else                                          sRxInterpretMemorizedUSIDataState     <= RECEIVE_USI_DATA_STREAM; -- normale Ausgabe der HS-Daten
                              end if;
                            when "11" =>    -- Uebertragung kam vom 4. Port
                              if (gNumberOfHighSpeedReceiverSlots < 4) then sRxInterpretMemorizedUSIDataState     <= WAIT_FOR_STX;            -- keine Ausgabe, warten auf naechsten Stream
                              else                                          sRxInterpretMemorizedUSIDataState     <= RECEIVE_USI_DATA_STREAM; -- normale Ausgabe der HS-Daten
                              end if;
                            when others => null;

                          end case;

                        else

                          sRxInterpretMemorizedUSIDataState <= RECEIVE_USI_DATA_STREAM;

                        end if;

                  -----------------------------------------------------------------------------
                  when others =>
                     sRxInterpretMemorizedUSIDataState <= WAIT_FOR_STX ;

               end case;   --case (sRxInterpretMemorizedUSIDataState) is
            end if;  --if (MD_Enable = '0') then
         end if ; --if (Clock'event AND Clock = '1') then


         -- noch Zuweisen der gerade empfangenen 32 bit Daten sHighSpeedData_Out an den zugeh. Receiver Slot am Ausgang HighSpeedPort_Out (1 bis 4 Receiver-Slots)
         HighSpeedPort_Out( ((sHighSpeedCurrentReceiver+1)*32)-1 downto  (sHighSpeedCurrentReceiver*32) )     <= sHighSpeedData_Out;


      end if ; --if (Reset = '1') then
   end process pRxInterpretMemorizedUSIData;


-------------------------------------------------------------------
-- Will die CPU nicht mit diesem MD kommunizieren Ports auf Hi-Z
-- Die Eingangsports Host_MD_TunnelData, _Strobe_In und _Acknowledge_In
-- werden dann 0 -> siehe pHEX2ASCII
-------------------------------------------------------------------

   pTunnelEnable : process (Clock, Reset)
   begin
      if (Reset = '1') then
         MD_Host_TunnelData_Tri           <= (others => 'Z');
         MD_Host_Strobe_Tri               <= 'Z';
         MD_Host_Acknowledge_Tri          <= 'Z';
         MD_Host_NewHdShkNotCarried_Tri   <= 'Z';
      else
         if (rising_edge(Clock)) then

            if (MD_Select = '1') then    --Host will Daten an MD_Host_TunnelData_Tri lesen

               MD_Host_TunnelData_Tri           <= sMD_Host_TunnelData_Out;
               MD_Host_Strobe_Tri               <= sMD_Host_Strobe_Out;
               MD_Host_Acknowledge_Tri          <= sMD_Host_Acknowledge_Out;
               MD_Host_NewHdShkNotCarried_Tri   <= sMD_Host_NewHdShkNotCarried;

            else                          --Host will Daten eines anderen Merger/Distriburor, daher muessen Ausgaenge Hi-Z sein
               MD_Host_TunnelData_Tri           <= (others => 'Z');
               MD_Host_Strobe_Tri               <= 'Z';
               MD_Host_Acknowledge_Tri          <= 'Z';
               MD_Host_NewHdShkNotCarried_Tri   <= 'Z';
            end if;

         end if;
      end if;
   end process pTunnelEnable;


-------------------------------------------------------------------
-- Zu sendende Daten von den Ports holen, Codieren und Streamen
-------------------------------------------------------------------

   pSendUSIStream : process (Clock, Reset)
   begin

      assert ( to_integer(unsigned(gHighSpeedPort_ByteIndexForNewDataTransmittedPulse(15 downto 12))) < 12)
         report "Der Zeitpunkt des HighSpeedPort_NewDataTransmittedPulse(3) einstellbar ueber gHighSpeedPort_ByteIndexForNewDataTransmittedPulse darf nur die Werte zw. 0 und 11 annehmen, hier aber auf " & integer'image( to_integer(unsigned(gHighSpeedPort_ByteIndexForNewDataTransmittedPulse(15 downto 12))) ) & " gesetzt !"
      severity error;
      assert ( to_integer(unsigned(gHighSpeedPort_ByteIndexForNewDataTransmittedPulse(11 downto  9))) < 12)
         report "Der Zeitpunkt des HighSpeedPort_NewDataTransmittedPulse(3) einstellbar ueber gHighSpeedPort_ByteIndexForNewDataTransmittedPulse darf nur die Werte zw. 0 und 11 annehmen, hier aber auf " & integer'image( to_integer(unsigned(gHighSpeedPort_ByteIndexForNewDataTransmittedPulse(11 downto  9))) ) & " gesetzt !"
      severity error;
      assert ( to_integer(unsigned(gHighSpeedPort_ByteIndexForNewDataTransmittedPulse( 8 downto  7))) < 12)
         report "Der Zeitpunkt des HighSpeedPort_NewDataTransmittedPulse(3) einstellbar ueber gHighSpeedPort_ByteIndexForNewDataTransmittedPulse darf nur die Werte zw. 0 und 11 annehmen, hier aber auf " & integer'image( to_integer(unsigned(gHighSpeedPort_ByteIndexForNewDataTransmittedPulse( 8 downto  4))) ) & " gesetzt !"
      severity error;
      assert ( to_integer(unsigned(gHighSpeedPort_ByteIndexForNewDataTransmittedPulse( 3 downto  0))) < 12)
         report "Der Zeitpunkt des HighSpeedPort_NewDataTransmittedPulse(3) einstellbar ueber gHighSpeedPort_ByteIndexForNewDataTransmittedPulse darf nur die Werte zw. 0 und 11 annehmen, hier aber auf " & integer'image( to_integer(unsigned(gHighSpeedPort_ByteIndexForNewDataTransmittedPulse( 3 downto  0))) ) & " gesetzt !"
      severity error;

      assert ( (gNumberOfHighSpeedTransmitterSlots > 0) and (gNumberOfHighSpeedTransmitterSlots < 5) )
         report "Die Anzahl der HighSpeedTransmitterSlots darf nur die Werte zw. 1 und 4 annehmen, hier aber auf " & integer'image(gNumberOfHighSpeedTransmitterSlots) & " gesetzt !"
      severity error;

      assert ( (gNumberOfHighSpeedReceiverSlots > 0) and (gNumberOfHighSpeedReceiverSlots < 5) )
         report "Die Anzahl der HighSpeedReceiverSlots darf nur die Werte zw. 1 und 4 annehmen, hier aber auf " & integer'image(gNumberOfHighSpeedReceiverSlots) & " gesetzt !"
      severity error;


      if (Reset = '1') then

         sUSISendByteIndex                      <=  0;
         sMD_USI_TxWriteEnable                  <= '0';
         sCarringNewHandshake                   <= '0';
         sMD_Host_NewHdShkNotCarried            <= '0';
         sHighSpeedPort_NewDataTransmittedPulse <= '0';
         sHost_MD_Strobe_In_Mem                 <= '0';
         sHost_MD_Acknowledge_In_Mem            <= '0';
         sStrobeLevelChanged                    <= '0';
         sAcknowledgeLevelChanged               <= '0';
         sStartNextHex2ASCIIConversion          <= '0';
         sMD_USI_TxData_Out                     <= (others => '0');
         HighSpeedPort_NewDataTransmittedPulse  <= (others => '0');
         HighSpeedPort_NewDataReceivedPulse     <= (others => '0');
         sSendUSIStreamState                    <= WAIT_FOR_ENABLING_SENDER;

         for i in 0 to 9 loop
            sSendDataArray(i) <= (others => '0');
         end loop;

      else

         if (rising_edge(Clock)) then

            --Abbruch wenn Merger/Distributor von USI_Control abgeschaltet wird
            if (MD_Enable = '0') then
               sUSISendByteIndex                      <=  0;
               sMD_USI_TxWriteEnable                  <= '0';
               sCarringNewHandshake                   <= '0';
               sMD_Host_NewHdShkNotCarried            <= '0';
               sHighSpeedPort_NewDataTransmittedPulse <= '0';
               sHost_MD_Strobe_In_Mem                 <= '0';
               sHost_MD_Acknowledge_In_Mem            <= '0';
               sStrobeLevelChanged                    <= '0';
               sAcknowledgeLevelChanged               <= '0';
               sMD_USI_TxData_Out                     <= (others => '0');
               HighSpeedPort_NewDataTransmittedPulse  <= (others => '0');
               HighSpeedPort_NewDataReceivedPulse     <= (others => '0');
               sSendUSIStreamState                    <= WAIT_FOR_ENABLING_SENDER;
            else

               sHighSpeedPort_NewDataTransmittedPulse <= '0';
               sStartNextHex2ASCIIConversion          <= '0';
               sMD_USI_TxWriteEnable                  <= '0';

               --was nun folgt dient der Rueckmeldung an die CPU
               --Es wird getestet, ob sich der Zustand von Strobe/Acknowledge seitens des Host geaendert hat.
               --Wenn ja, wird das entsprechende sStrobeLevelChanged/sAcknowledgeLevelChanged Signal gesetzt
               --Sobald die Strobe/Acknowledgeinformartionen mindestens 1x gesendet wurden, werden diese
               --Signale wieder low.

               if (Host_MD_Strobe /= sHost_MD_Strobe_In_Mem) then
                  if (sMD_Host_NewHdShkNotCarried ='0') then
                     sHost_MD_Strobe_In_Mem  <= Host_MD_Strobe;
                     sStrobeLevelChanged     <= '1';
                  end if;
               else
                  if ( (sStrobeLevelChanged = '1') AND (sCarringNewHandshake = '1') ) then
                      sStrobeLevelChanged <= '0';
                  else
                     sStrobeLevelChanged <= sStrobeLevelChanged;
                  end if;
               end if;

               if (Host_MD_Acknowledge /= sHost_MD_Acknowledge_In_Mem) then
                  if (sMD_Host_NewHdShkNotCarried ='0') then
                     sHost_MD_Acknowledge_In_Mem   <= Host_MD_Acknowledge;
                     sAcknowledgeLevelChanged      <= '1';
                  end if;
               else
                  if ( (sAcknowledgeLevelChanged = '1') AND (sCarringNewHandshake = '1') ) then
                     sAcknowledgeLevelChanged   <= '0';
                  else
                     sAcknowledgeLevelChanged   <= sAcknowledgeLevelChanged;
                  end if;
               end if;

               --siehe State:"PREPARE_SOME_SIGNAL_STUFF_BEFORE_TRANSMITTING_FIRST_HS_DATA_BYTE" fuer Details zu 'sMD_Host_NewHdShkNotCarried'
               if ( (sStrobeLevelChanged ='1') OR (sAcknowledgeLevelChanged ='1') ) then
                  sMD_Host_NewHdShkNotCarried   <= '1';
               end if;

               -----------------------------------------------------------------------------
               case (sSendUSIStreamState) is

                  --Der M/D darf erst mit dem Streamen beginnen, wenn dieser einen ersten Stream empfangen hat
                  --Dies deshalb, weil im Host (in der Regel der MFU) die CPU zunaechst den StreamingMode aktiviert und
                  --auf eine positive Rueckmeldung wartet. Diese wird ausgewertet. Ist alles OK, wird der ReceiveInterrupt
                  --fuer die benutzte USI deaktiviert und nicht mehr ausgewertet. Die CPU leitet das Streaming ein und der Slave darf ebenfalls
                  --streamen.

                  --Da der Host (die MFU) sofort senden moechte, muss zusaetzlich der Port 'ReceiveFirstBeforeSend' ausgewertet werden.
                  --Ist dieser High muss zunaechst das oben beschriebene passieren, wenn Low beginnt das Modul sofort zu senden.

                  -----------------------------------------------------------------------------
                  --So lange MD_Enable = '0' ist, bleiben wir in diesem State.
                  when WAIT_FOR_ENABLING_SENDER =>

                     if (ReceiveFirstBeforeSend = '1') then

                        if(sStreamingEnable = '1') then                          --wird '1' wenn der erste String vollstaendig empfangen wurde
                           sSendUSIStreamState           <= PREPARE_SOME_SIGNAL_STUFF_BEFORE_TRANSMITTING_FIRST_HS_DATA_BYTE;
                        else
                           sSendUSIStreamState <= WAIT_FOR_ENABLING_SENDER;
                        end if;

                     else
                        sSendUSIStreamState           <= PREPARE_SOME_SIGNAL_STUFF_BEFORE_TRANSMITTING_FIRST_HS_DATA_BYTE;
                     end if;

                  -----------------------------------------------------------------------------
                  --Diverse Dinge vorbereiten, bevor das erste HS DatenBytes versendet wird.
                  when PREPARE_SOME_SIGNAL_STUFF_BEFORE_TRANSMITTING_FIRST_HS_DATA_BYTE =>

                     sUSISendByteIndex          <=  0;

                     --'sMD_Host_NewHdShkNotCarried' wird vom M/D am Ausgang 'MD_Host_NewHdShkNotCarried_Tri'
                     --kommuniziert und wird '1', sobald eines der internen Signale 'sStrobeLevelChanged'
                     --oder 'sAcknowledgeLevelChanged' High wird. Dies ist immer dann der Fall,
                     --wenn sich seitens des Senders an den Eingaengen 'Host_MD_Strobe' oder
                     --'Host_MD_Acknowledge' der Zustand geaendert hat. Wegen der Tunnelmimik
                     --muss jede Aenderung des Eingansgzustandes (egal ob Strobe oder Acknowledge) auch sicher
                     --an den Empfaenger kommuniziert werden. Um dies sicher zustellen, wird das Signal
                     --'sCarringNewHandshake' auf '1' gesetzt und erst wieder '0' wenn mindestens
                     --ein HS_Stream mit der geanderten Strobe/Acknowledge Information versendet wurd.

                     if (sMD_Host_NewHdShkNotCarried  = '1') then
                        sCarringNewHandshake <= '1';
                     else
                        sCarringNewHandshake <= '0';
                     end if;


                     -- Vorgabe von Derek Schupp Telefonat 23.06.2021:
                     -- Moeglicherweise ist Select_HighSpeedSlot > Anzahl der Ports, das koennte es geben. In so einem Fall senden wir die HS-Daten immer "00000000"
                     -- und setzen alles fest auf Transmitter-Port 0
                     if ( (HighSpeedSlotFreeWheel = '0')   and   (to_integer( unsigned(Select_HighSpeedSlot) ) > (gNumberOfHighSpeedTransmitterSlots - 1))   ) then
                        sHighSpeedCurrentTransmitter <= 0;
                     end if;

                     sSendUSIStreamState           <= COMMIT_DATA_BYTE_BY_BYTE_TO_SENDER;

                  -----------------------------------------------------------------------------
                  --Das Signal 'sSendDataArrayValid' wird im Prozess 'pHEX2ASCII' bedienet.
                  --Jedes Mal wenn der 'HighSpeedPort_In' gelesen wird, wird 'sSendDataArrayValid = '0'' und bleibt es,
                  --bis die Daten eingelesen, nach HEX gewandelt und im 'sConvertedSendASCIIDataArray()' zu finden sind.
                  when WAIT_HEX2ASCII_CONVERSION_DONE =>

                     --Ist der Wandlungsprozess (Hex -> ASCII) beendet, wird sSendDataArrayValid' = '1'
                     --Wir warten hier bis sSendDataArrayValid' = '1' wird und anzeigt, dass die Wandlung abgeschlossen ist .
                    if (sSendDataArrayValid  = '1') then

                      -- Conversion done
                      --sSendDataArray(0) <= X"02";
                      sSendDataArray(0)  <= sConvertedSendASCIIDataArray(0);   --HiSpeedPort(31..28)     2.Byte ASCII
                      sSendDataArray(1)  <= sConvertedSendASCIIDataArray(1);   --HiSpeedPort(27..24)     3.Byte ASCII
                      sSendDataArray(2)  <= sConvertedSendASCIIDataArray(2);   --HiSpeedPort(23..20)     4.Byte ASCII
                      sSendDataArray(3)  <= sConvertedSendASCIIDataArray(3);   --HiSpeedPort(19..16)     5.Byte ASCII
                      sSendDataArray(4)  <= sConvertedSendASCIIDataArray(4);   --HiSpeedPort(15..12)     6.Byte ASCII
                      sSendDataArray(5)  <= sConvertedSendASCIIDataArray(5);   --HiSpeedPort(11.. 8)     7.Byte ASCII
                      sSendDataArray(6)  <= sConvertedSendASCIIDataArray(6);   --HiSpeedPort( 7.. 4)     8.Byte ASCII
                      sSendDataArray(7)  <= sConvertedSendASCIIDataArray(7);   --HiSpeedPort( 3.. 0)     9.Byte ASCII
                      sSendDataArray(8)  <= sTunnelData;                       --Tunnelbyte kommt seitens Host schon als ASCII daher keine Konvertierung (9)(10) 10.Byte ASCII
                      sSendDataArray(9) <= sConvertedSendASCIIDataArray(9);   --CPU_Strobe/Acknowledge 11.Byte ASCII
                      --sSendDataArray(11) <= X"03";                             --ETX                    12.Byte ASCII

                      sSendUSIStreamState <= COMMIT_DATA_BYTE_BY_BYTE_TO_SENDER;
                    else
                      sSendUSIStreamState <= WAIT_HEX2ASCII_CONVERSION_DONE;
                    end if;

                  -----------------------------------------------------------------------------
                  when COMMIT_DATA_BYTE_BY_BYTE_TO_SENDER =>

                    -- Problem:
                    -- ========
                    -- Wird 'sUSI_MD_TxFIFOFull' nicht '0', bleibt 'sSendUSIStreamState' in diesem State,
                    -- bis 'MD_Enable = '0'' wird.

                     if (USI_MD_TxFIFOFull = '0') then           --warten bis Platz im Tx FIFO

                        if (sUSISendByteIndex  = 0 ) then

                           sMD_USI_TxData_Out   <= X"02";          --STX direkt versenden

                        elsif (sUSISendByteIndex  < cUSISendByteIndexMaxValue) then

                           sMD_USI_TxData_Out   <= sSendDataArray(sUSISendByteIndex-1);

                        else  --  >= cUSISendByteIndexMaxValue

                           sMD_USI_TxData_Out   <= X"03";          --ETX direkt versenden

                        end if;

                        sMD_USI_TxWriteEnable   <= '1';       --..dazu Tx Enable setzen
                        sSendUSIStreamState     <= WAIT_FOR_TX_FIFO_DATA_FETCHED;

                     else
                        sSendUSIStreamState  <= COMMIT_DATA_BYTE_BY_BYTE_TO_SENDER;
                     end if;

                  -----------------------------------------------------------------------------
                  when WAIT_FOR_TX_FIFO_DATA_FETCHED =>           -- Daten in Ausgabe FIFO schieben..


                     --sHighSpeedCurrentTransmitter 3..0
                     --gHighSpeedPort_ByteIndexForNewDataTransmittedPulse(15..0)
                     
                     --(sHighSpeedCurrentTransmitter*4)+3 .. (sHighSpeedCurrentTransmitter*4)
                     --3 = 15..12
                     --2 = 11.. 8
                     --1 =  7.. 4
                     --0 =  3.. 0

                     --if (sUSISendByteIndex = to_integer(unsigned(gHighSpeedPort_ByteIndexForNewDataTransmittedPulse)) ) then  -- InfoStrobe Ausgang 'NewDataStrobeTransmit' setzen...
                     if (sUSISendByteIndex = to_integer(unsigned(gHighSpeedPort_ByteIndexForNewDataTransmittedPulse((sHighSpeedCurrentTransmitter*4)+3 downto (sHighSpeedCurrentTransmitter*4)))) ) then  -- InfoStrobe Ausgang 'NewDataStrobeTransmit' setzen...
                        sHighSpeedPort_NewDataTransmittedPulse <= '1';
                     end if;

                     -- Problem:
                     -- ========
                     -- Wird 'USI_MD_USITxFIFODataFetchedFromDataInFromMD' nicht '1', bleibt 'sSendUSIStreamState' in diesem State,
                     -- bis 'MD_Enable = '0'' wird.
                     -- Ggf. sollte daher ein Timeout-Zaehler eingebaut werden

                     if (USI_MD_USITxFIFODataFetchedFromDataInFromMD = '1') then --warten bis TxFIFO im USI Modul den Empf. der Daten quittiert
                        sSendUSIStreamState <= INCREMENT_INDEX;
                     end if;

                  -----------------------------------------------------------------------------
                  when INCREMENT_INDEX =>

                     if sHighSpeedPort_NewDataTransmittedPulse = '1' then

                        -- Transmitter Slot festlegen bei FreeWheel AUS vor dem Transfer
                        if ( HighSpeedSlotFreeWheel = '0' ) then
                          -- Nicht freilaufend, es wird nur ein bestimmter HS-Wert ueber Select_HighSpeedSlot festgelegt

                          -- Vorgabe von Derek Schupp Telefonat 23.06.2021:
                          -- Moeglicherweise ist Select_HighSpeedSlot > Anzahl der Ports, das koennte es geben. In so einem Fall senden wir die HS-Daten immer "00000000"
                          -- und setzen alles fest auf Transmitter-Port 0
                          case (Select_HighSpeedSlot) is
                            when "00" =>
                              sHighSpeedCurrentTransmitter <= 0;

                            when "01" =>
                              if ( gNumberOfHighSpeedTransmitterSlots > 1 ) then
                                sHighSpeedCurrentTransmitter <= 1;
                              else
                                sHighSpeedCurrentTransmitter <= 0;
                              end if;

                            when "10" =>
                              if ( gNumberOfHighSpeedTransmitterSlots > 2 ) then
                                sHighSpeedCurrentTransmitter <= 2;
                              else
                                sHighSpeedCurrentTransmitter <= 0;
                              end if;

                            when "11" =>
                              if ( gNumberOfHighSpeedTransmitterSlots > 3 ) then
                                sHighSpeedCurrentTransmitter <= 3;
                              else
                                sHighSpeedCurrentTransmitter <= 0;
                              end if;

                            when others =>
                              sHighSpeedCurrentTransmitter <= 0;

                          end case;
                        end if;
                     end if;

                     if (sUSISendByteIndex  <  cUSISendByteIndexMaxValue)  then --...und ggf. den Index der zu sendenden Daten erhoehen

                        sUSISendByteIndex   <= sUSISendByteIndex + 1;

                        if (sUSISendByteIndex = 0 ) then
                          -- Hex2ASCCI Datenkonvertierung starten wir erst, wenn STX schon in den Versand geht (Optimierung Latenz-Zeit)
                           sStartNextHex2ASCIIConversion <= '1';        --Hex2ACSII Konvertierung starten
                           sSendUSIStreamState           <= WAIT_HEX2ASCII_CONVERSION_DONE;
                        else
                           sSendUSIStreamState <= COMMIT_DATA_BYTE_BY_BYTE_TO_SENDER;
                        end if;

                     else
                        -- Transmitterslot bei FreeWheel EIN hochzaehlen erst nach dem Transfer
                        if ( HighSpeedSlotFreeWheel = '1' ) then
                          -- freilaufend
                          -- ... naechster Slot
                          -- sHighSpeedCurrentTransmitter: 0,1,2,3
                          if ( (sHighSpeedCurrentTransmitter + 1) < gNumberOfHighSpeedTransmitterSlots ) then
                            sHighSpeedCurrentTransmitter <= sHighSpeedCurrentTransmitter + 1;
                          else
                            sHighSpeedCurrentTransmitter <= 0;
                          end if;
                        end if;

                        sSendUSIStreamState <= POSSIBLY_CLEAR_NEW_HDSHK_NOT_CARRIED;
                     end if;

                  -----------------------------------------------------------------------------
                  when POSSIBLY_CLEAR_NEW_HDSHK_NOT_CARRIED =>

                     --Jetzt wurde, sofern zu Beginn des Transfers 'sCarringNewHandshake' gesetzt war
                     --(siehe hierzu State:"PREPARE_SOME_SIGNAL_STUFF_BEFORE_TRANSMITTING_FIRST_HS_DATA_BYTE": 'sCarringNewHandshake' kann nur '1' sein,
                     --wenn in diesem State auch 'sMD_Host_NewHdShkNotCarried' bereits '1' war)
                     --eine Aenderung im Strobe/Acknowledge mindestens 1x im HS_Stream verschickt.
                     --Daher kann hier in diesem Falle 'sMD_Host_NewHdShkNotCarried' wieder auf '0'
                     --gesetzt werden. Dies signalisiert dem Sender, dass eine Aenderung im Strobe/Acknowledge
                     --verschickt wurde und der Sender den naechsten Tunnelwert anlegen kann.
                     --'sCarringNewHandshake' wird dann im State:"PREPARE_SOME_SIGNAL_STUFF_BEFORE_TRANSMITTING_FIRST_HS_DATA_BYTE auf '0' gesetzt.

                     if ( (sMD_Host_NewHdShkNotCarried  = '1') AND  (sCarringNewHandshake = '1') )then
                        sMD_Host_NewHdShkNotCarried  <= '0';
                     end if;

                     sSendUSIStreamState        <= PREPARE_SOME_SIGNAL_STUFF_BEFORE_TRANSMITTING_FIRST_HS_DATA_BYTE;

                  -----------------------------------------------------------------------------
                  when others =>

                     if ( (sMD_Host_NewHdShkNotCarried  = '1') AND (sCarringNewHandshake = '1') )then
                        sMD_Host_NewHdShkNotCarried  <= '0';
                     end if;

                     sSendUSIStreamState        <= PREPARE_SOME_SIGNAL_STUFF_BEFORE_TRANSMITTING_FIRST_HS_DATA_BYTE;

               end case;   --case sSendUSIStreamState is

            end if;  --if (MD_Enable = '0') then


            -- nur genau den einen Transmitterpuls des entspr. Slots aktivieren
            HighSpeedPort_NewDataTransmittedPulse <= (others => '0');
            HighSpeedPort_NewDataTransmittedPulse( sHighSpeedCurrentTransmitter )  <= sHighSpeedPort_NewDataTransmittedPulse;

            -- nur genau den einen Receiverpuls des entspr. Slots aktivieren
            HighSpeedPort_NewDataReceivedPulse <= (others => '0');
            HighSpeedPort_NewDataReceivedPulse( sHighSpeedCurrentReceiver )        <= sHighSpeedPort_NewDataReceivedPulse;


         end if;  --if (Clock'event AND Clock = '1') then
      end if;  --if (Reset = '1') then
   end process;

-------------------------------------------------------------------
-- Umwandeln zu sendender Daten an den Ports von Hex in ASCII
-------------------------------------------------------------------

--HexByte            in Nibbel   konvertiert
--0 MSB HighSpeed    2           (0)(1)
--1     HighSpeed    2           (2)(3)
--2     HighSpeed    2           (4)(5)
--3 LSB HighSpeed    2           (6)(7)
--4 TunnelData       2           keine Konvertierung, Wert kommt schon als ASCII (8)(9)
--5 Handshake        1           nur 1 Nibble (10) Konvertierung, dies ist nun noetig, das 'Ext_Bits_In' in HandshakeNibble
--                               kodiert werden.
--                               Bisher war keine Konvertierung notwendig, da nur das untere Nibbel gesendet wird,
--                               und nur Bit 0 und 1 benutzt wurden. Der Wert war zumindest immer 0x30
--                               bei Strobe = 0x31, bei Ack = 0x32. Diese Bit wurden direkt hinzugefuegt oder geloescht.


   pHEX2ASCII : process (Clock, Reset)
   begin
      if (Reset = '1') then

         sHEX2ASCIIState                              <= PREPARE_READ_PORTS;
         sSendDataArrayValid                          <= '0';
         sHighSpeedPort_In                            <= (others => '0');
         sTunnelData                                  <= X"30";

         sHighSpeedPort_AcceptNewDataRisingEdgeArray  <= (others => '0');
         sSelected_Tx_HighSpeedSlot                   <= (others => '0');

         for i in 0 to 3 loop
            sRawSendHEXDataArray(i) <= (others => '0');
         end loop;

         for i in 0 to 9 loop
            sConvertedSendASCIIDataArray(i) <= (others => '0');
         end loop;

      else

         if (rising_edge(Clock)) then

            sSendDataArrayValid     <= '0';
                        
            --Ist 'gUse_HighSpeedPort_AcceptNewDataRisingEdge(x)' = '0' werden die Datan an 'HighSpeedPort_In(x)' nach jedem Sendevorgang eingelesen.
            --Ist 'gUse_HighSpeedPort_AcceptNewDataRisingEdge(x)' = '1' werden die Datan an 'HighSpeedPort_In(x)' durch eine steigende Flanke an 
            --                                                      'sHighSpeedPort_AcceptNewDataRisingEdgeArray(x)' eingelesen.


            --pos. Flanke an 'HighSpeedPort_AcceptNewDataRisingEdge' erkennen (Fill 2 bit Shifter for each Port every clock)
            for i in 0 to (gNumberOfHighSpeedTransmitterSlots - 1) loop

              sHighSpeedPort_AcceptNewDataRisingEdgeArray(2*i+1 downto 2*i) <= sHighSpeedPort_AcceptNewDataRisingEdgeArray(2*i) & HighSpeedPort_AcceptNewDataRisingEdge(i);

              if ( gUse_HighSpeedPort_AcceptNewDataRisingEdge(i) = '1' ) then
                -- Uebernahme per Pulse vom entspr. Slot
                if ( sHighSpeedPort_AcceptNewDataRisingEdgeArray( (i*2)+1 downto (i*2) ) = "01" ) then
                  sHighSpeedPort_In( (i+1)*32-1 downto i*32 ) <= HighSpeedPort_In( (i+1)*32-1 downto i*32 );
                end if;
              else
                -- Wenn FreeWhele = '1' => Uebernahme ohne Pulse, also immer bei jedem Takt
                sHighSpeedPort_In( (i+1)*32-1 downto i*32 ) <= HighSpeedPort_In( (i+1)*32-1 downto i*32 );                    
              end if;

            end loop;

            -- Ist 'HighSpeedSlotFreeWheel'  = '1' werden alle verfuegbaren HighSpeed Slots reihum immer wieder uebertragen.
            -- Ist 'HighSpeedSlotFreeWheel'  = '0' wird immer nur der an 'Select_HighSpeedSlot' anliegende Kanal verschickt.

            -- Vorgabe von Derek Schupp Telefonat 23.06.2021:
            -- Moeglicherweise ist bei FreeWheel Select_HighSpeedSlot > Anzahl der Ports, das koennte es geben. In so einem Fall senden wir die HS-Daten immer "00000000"
            -- und setzen alles fest auf Transmitter-Port 0, uebertragen somit immer Slot 0 mit Daten "00000000"
            if ( (HighSpeedSlotFreeWheel = '0')   and   (to_integer( unsigned(Select_HighSpeedSlot) ) > (gNumberOfHighSpeedTransmitterSlots - 1))   ) then
              -- Selected Slot > Number of Slots
              sHighSpeedPort_In(31 downto 0)  <=  X"00000000";
              sSelected_Tx_HighSpeedSlot <=  Select_HighSpeedSlot;
            else
              case (sHighSpeedCurrentTransmitter) is
                when 0 =>
                  sSelected_Tx_HighSpeedSlot <= "00";
                when 1 =>
                  sSelected_Tx_HighSpeedSlot <= "01";
                when 2 =>
                  sSelected_Tx_HighSpeedSlot <= "10";
                when 3 =>
                  sSelected_Tx_HighSpeedSlot <= "11";
                when others =>
                  sSelected_Tx_HighSpeedSlot <= "00";
              end case;
            end if;

            --Der Wandlungsprozess des HighSpeed-Eingansports auf HighSpeed Daten erfolgt fortlaufend
            --Das Gleiche gilt fuer die 'Ext_Bits' die innerhalb des 'HandshakeBytes' kodiert werden.
            --D.h. Sofern der 'MD' nicht von CPU/Host selektiert wurde, kann das 'Handshakebyte' nur die Zustaende:
            --00xx (0x30), 01xx (0x34), 10xx (0x38) und 11xx (0x43) annehmen

            case (sHex2ASCIIState) is

               -----------------------------------------------------------------------------
               when PREPARE_READ_PORTS =>

                  -- We need one clock here for  sHighSpeedPort_In  and  sSelected_Tx_HighSpeedSlot  assignement
                  if (sStartNextHex2ASCIIConversion = '1') then
                    sHex2ASCIIState   <= READ_PORTS;
                  end if;

               -----------------------------------------------------------------------------
               when READ_PORTS =>

                   --sSendDataArrayValid     <= '0';

                   sRawSendHEXDataArray(0) <= sHighSpeedPort_In( ((sHighSpeedCurrentTransmitter+1)*32)- 1 downto ((sHighSpeedCurrentTransmitter+1)*32)- 8 );    --2. und 3. Byte ASCII
                   sRawSendHEXDataArray(1) <= sHighSpeedPort_In( ((sHighSpeedCurrentTransmitter+1)*32)- 9 downto ((sHighSpeedCurrentTransmitter+1)*32)-16 );    --4. und 5. Byte ASCII
                   sRawSendHEXDataArray(2) <= sHighSpeedPort_In( ((sHighSpeedCurrentTransmitter+1)*32)-17 downto ((sHighSpeedCurrentTransmitter+1)*32)-24 );    --6. und 7. Byte ASCII
                   sRawSendHEXDataArray(3) <= sHighSpeedPort_In( ((sHighSpeedCurrentTransmitter+1)*32)-25 downto ((sHighSpeedCurrentTransmitter+1)*32)-32 );    --8. und 9. Byte ASCII

                   --Die Wandlung und Ausgabe der Tunneldaten und der zugehoerigen Handshakeinformationen erfolgt erst,
                   --wenn der MD auch seitens des Host selektiert wurde

                   if (MD_Select = '1') then             --Host will Daten an MD_Host_TunnelData_Tri lesen
                      sTunnelData               <= Host_MD_TunnelData;
                      sRawSendHEXDataArray(4)   <= X"0" & sSelected_Tx_HighSpeedSlot & sHost_MD_Acknowledge_In_Mem & sHost_MD_Strobe_In_Mem;
                   else                                   --Host will Daten eines anderen Merger/Distributor, daher muessen Eingaenge 0 sein
                      sTunnelData               <= X"30";
                      sRawSendHEXDataArray(4)   <= X"0" & sSelected_Tx_HighSpeedSlot & "00";
                   end if;

                   sHex2ASCIIState   <= CONVERT_ASCII;

               -----------------------------------------------------------------------------
               when CONVERT_ASCII =>

                  sConvertedSendASCIIDataArray(0) <= cHEX2ASCII(to_integer(unsigned(sRawSendHEXDataArray(0)(7 downto 4))));
                  sConvertedSendASCIIDataArray(1) <= cHEX2ASCII(to_integer(unsigned(sRawSendHEXDataArray(0)(3 downto 0))));
                  sConvertedSendASCIIDataArray(2) <= cHEX2ASCII(to_integer(unsigned(sRawSendHEXDataArray(1)(7 downto 4))));
                  sConvertedSendASCIIDataArray(3) <= cHEX2ASCII(to_integer(unsigned(sRawSendHEXDataArray(1)(3 downto 0))));
                  sConvertedSendASCIIDataArray(4) <= cHEX2ASCII(to_integer(unsigned(sRawSendHEXDataArray(2)(7 downto 4))));
                  sConvertedSendASCIIDataArray(5) <= cHEX2ASCII(to_integer(unsigned(sRawSendHEXDataArray(2)(3 downto 0))));
                  sConvertedSendASCIIDataArray(6) <= cHEX2ASCII(to_integer(unsigned(sRawSendHEXDataArray(3)(7 downto 4))));
                  sConvertedSendASCIIDataArray(7) <= cHEX2ASCII(to_integer(unsigned(sRawSendHEXDataArray(3)(3 downto 0))));

                  sConvertedSendASCIIDataArray(9) <= cHEX2ASCII(to_integer(unsigned(sRawSendHEXDataArray(4)(3 downto 0))));

                  sSendDataArrayValid  <= '1';
                  sHex2ASCIIState      <= WAIT_FOR_DATA_GRAB;

               -----------------------------------------------------------------------------
               when WAIT_FOR_DATA_GRAB =>

                  if (sStartNextHex2ASCIIConversion = '0') then
                    sHex2ASCIIState <= PREPARE_READ_PORTS;
                  end if;

               -----------------------------------------------------------------------------
               when others =>

                  sHex2ASCIIState <= PREPARE_READ_PORTS;

            end case;

         end if ;
      end if ;
   end process pHEX2ASCII;

   --Rueckmeldung an Host dass MD eingschaltet und im HighSpeed Modus ist (Status)
   MDisEnabled <= MD_Enable;


end architecture RTL;