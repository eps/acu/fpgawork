--------------------------------------------------------------------------------
-- (C) GE Power Conversion - 2022															--
--																										--
-- The copyright in this software is the property of GE Power Conversion.		--
--																										--
-- It is supplied on the express terms that it may not be copied, used or		--
-- disclosed to others for any purpose except as authorised by the above		--
-- named Company.																					--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- File 		: Module_USI.vhd																	--
-- Date 		: 09 Dec 2022																		--
-- Author 	: Britto A																			--
-- Project 	: GSI																					--
-- Board 	: 																						--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Modifications :																				--	 
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity Module_USI is	
	port(
		Reset											:  in  std_logic; 
		Clk											:  in  std_logic;
		LatchAllReadData							:  IN  STD_LOGIC;

		--USI Module inputs
      USIRxSerial                         :  IN  STD_LOGIC;
		FWMajorRelease      						:	IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		FWMinorRelease        					:	IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
		FWDate           							:	IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
			
		--Signals from Peripheral to USI
		Measurements								:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  Measurements to MFU via HS 
		Status 										:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  PC status to MFU via HS
		ModuleStatus								:  IN	  STD_LOGIC_VECTOR(79 DOWNTO 0); -- 10 bytes FSP001 input
		ModuleWarnings								:  IN   STD_LOGIC_VECTOR(79 DOWNTO 0); -- 10 bytes FSP002 input
		ModuleInterlocks_n						:  IN   STD_LOGIC_VECTOR(79 DOWNTO 0); -- 10 bytes FSP0004 input 
		CurrentValue1								:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP060 input
		CurrentValue2								:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP061 input
		CurrentValue3								:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP062 input
		CurrentValue4								:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP063 input
		CurrentValue5								:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP064 input
		CurrentValue6								:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP065 input
		CurrentValue7								:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0); -- 2 bytes  FSP066 input			

		--Trip line
		TripLineTx									: in std_logic;
		
		--Interlocks inputs
		ClrFault										: in std_logic;
		PIBlinkWdog									: in std_logic;
		InterlockIn_TC								: in std_logic;
		InterlockIn_LC1							: in std_logic;
		InterlockIn_LC2							: in std_logic;
		InterlockIn_DC1							: in std_logic;
		InterlockIn_DC2							: in std_logic;
		WarningIn_DelI1							: in std_logic;
		WarningIn_DelI2							: in std_logic;
		InterlockIn_QDS1							: in std_logic;
		InterlockIn_QDS2							: in std_logic;
		InterlockIn_QDS1_Model					: in std_logic;
		InterlockIn_QDS2_Model					: in std_logic;
		InterlockIn_MPS1_Model					: in std_logic;
		InterlockIn_MPS2_Model					: in std_logic;
			
		--USI Module outputs
	   USITxSerial                        	:  OUT  STD_LOGIC;
		USIInHighSpeedMode						:  OUT  STD_LOGIC;
		
		--Signals from USI to Peripheral
		Command										:  OUT  STD_LOGIC_VECTOR( 7 DOWNTO 0); -- 1 byte Received latched command via HS
		USIConfig									:  OUT  STD_LOGIC_VECTOR( 7 DOWNTO 0); -- 1 byte USI configuration from FSP012
		AnalogValSelector							:  OUT  STD_LOGIC_VECTOR( 7 DOWNTO 0);	-- 1 byte selector for analog values FSP067 output 		
		SetvalueToleranceFactor					:  OUT  STD_LOGIC_VECTOR(23 DOWNTO 0); -- 3 bytes Set value for Tolerance factor FSP068 output 

		--Trip line
		TripLineRx									: out std_logic;

		--Interlocks outputs
		Interlock_MPS1								: out std_logic;
		Interlock_MPS2								: out std_logic;
		Interlock_MPS1_Model						: out std_logic;
		Interlock_MPS2_Model						: out std_logic;
		Interlock_QDS1_Model						: out std_logic;
		Interlock_QDS2_Model						: out std_logic;
		Interlock_LC								: out std_logic;
		Fault_LC										: out std_logic;
		Interlock_DC								: out std_logic;
		Fault_DC										: out std_logic;	
		Warning_DelI								: out std_logic;
		Fault_Warning_DelI						: out std_logic;
		Interlock_QDS1								: out std_logic;
		Interlock_QDS2								: out std_logic
	);
end Module_USI;

architecture a of Module_USI is
	
	signal ClkPLL 									: std_logic;
	signal Measurements_int					: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal Status_int							: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal ModuleStatus_int					: STD_LOGIC_VECTOR(79 DOWNTO 0);
	signal ModuleWarnings_int				: STD_LOGIC_VECTOR(79 DOWNTO 0);
	signal ModuleInterlocks_n_int			: STD_LOGIC_VECTOR(79 DOWNTO 0); 
	signal CurrentValue1_int					: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal CurrentValue2_int					: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal CurrentValue3_int					: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal CurrentValue4_int					: STD_LOGIC_VECTOR(15 DOWNTO 0); 
	signal CurrentValue5_int					: STD_LOGIC_VECTOR(15 DOWNTO 0); 
	signal CurrentValue6_int					: STD_LOGIC_VECTOR(15 DOWNTO 0); 
	signal CurrentValue7_int					: STD_LOGIC_VECTOR(15 DOWNTO 0); 		
	signal Command_int							: STD_LOGIC_VECTOR( 7 DOWNTO 0);
	signal USIConfig_int						: STD_LOGIC_VECTOR( 7 DOWNTO 0);
	signal AnalogValSelector_int			: STD_LOGIC_VECTOR( 7 DOWNTO 0);	 		
	signal SetvalueToleranceFactor_int	: STD_LOGIC_VECTOR(23 DOWNTO 0); 
	signal LatchAllReadData_int			: STD_LOGIC_VECTOR(1 DOWNTO 0);
	
	------------------------------------------------------------------------
	-- Component Declaration
	------------------------------------------------------------------------
	COMPONENT module
		PORT
		(      
			Reset                      		:  IN   STD_LOGIC;
			Clock                  				:  IN   STD_LOGIC;
			LatchAllReadData						:  IN  STD_LOGIC;

			USIRxSerial               			:  IN   STD_LOGIC;
			FWMajorRelease      					:	IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
			FWMinorRelease        				:	IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
			FWDate           						:	IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
			Measurements							:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0);
			Status 									:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0);
			ModuleStatus							:  IN	STD_LOGIC_VECTOR(79 DOWNTO 0);
			ModuleWarnings							:  IN   STD_LOGIC_VECTOR(79 DOWNTO 0);
			ModuleInterlocks_n					:  IN   STD_LOGIC_VECTOR(79 DOWNTO 0); 
			CurrentValue1							:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0);
			CurrentValue2							:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0);
			CurrentValue3							:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0);
			CurrentValue4							:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0);
			CurrentValue5							:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0);
			CurrentValue6							:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0);
			CurrentValue7							:  IN   STD_LOGIC_VECTOR(15 DOWNTO 0);			

			USITxSerial                  		:  OUT  STD_LOGIC;
			USIInHighSpeedMode					:  OUT  STD_LOGIC;
			Command									:  OUT  STD_LOGIC_VECTOR( 7 DOWNTO 0);
			USIConfig								:  OUT  STD_LOGIC_VECTOR( 7 DOWNTO 0);
			AnalogValSelector						:  OUT  STD_LOGIC_VECTOR( 7 DOWNTO 0); 		
			SetvalueToleranceFactor				:  OUT  STD_LOGIC_VECTOR(23 DOWNTO 0)  
 		);
	END COMPONENT;

	component Interlocks
		port(
			Reset							: in std_logic; 
			Clk							: in std_logic;
			ClrFault						: in std_logic;
			USICommand					: in std_logic_vector(7 downto 0);
			PIBlinkWdog					: in std_logic;
			InterlockIn_TC				: in std_logic;
			InterlockIn_LC1			: in std_logic;
			InterlockIn_LC2			: in std_logic;
			InterlockIn_DC1			: in std_logic;
			InterlockIn_DC2			: in std_logic;
			WarningIn_DelI1			: in std_logic;
			WarningIn_DelI2			: in std_logic;
			InterlockIn_QDS1			: in std_logic;
			InterlockIn_QDS2			: in std_logic;

			InterlockIn_QDS1_Model	: in std_logic;
			InterlockIn_QDS2_Model	: in std_logic;
			InterlockIn_MPS1_Model	: in std_logic;
			InterlockIn_MPS2_Model	: in std_logic;
			TripLineTx					: in std_logic;
			
			Interlock_MPS1				: out std_logic;
			Interlock_MPS2				: out std_logic;
			Interlock_MPS1_Model		: out std_logic;
			Interlock_MPS2_Model		: out std_logic;
			Interlock_QDS1_Model		: out std_logic;
			Interlock_QDS2_Model		: out std_logic;
			Interlock_LC				: out std_logic;
			Fault_LC						: out std_logic;
			Interlock_DC				: out std_logic;
			Fault_DC						: out std_logic;
			Warning_DelI				: out std_logic;
			Fault_Warning_DelI		: out std_logic;
			Interlock_QDS1				: out std_logic;
			Interlock_QDS2				: out std_logic;
			TripLineRx					: out std_logic		
			);
	end component;

	component PLL_USI
		PORT
		(
			areset						: IN STD_LOGIC  := '0';
			inclk0						: IN STD_LOGIC  := '0';
			c0								: OUT STD_LOGIC ;
			locked						: OUT STD_LOGIC 
		);
	end component;

	component ClockDomainCrossing_simple is
		generic ( widthVector : integer range 1 to 1023
					);
		port (
			ClockIn        		   : in std_logic;
			ClockOut     				: in std_logic;
			Reset       				: in std_logic;
			DataIn     					: in std_logic_vector(widthVector-1 downto 0);
			DataOut    					: out std_logic_vector(widthVector-1 downto 0) := (others => '0')
		);
		end component;	
		
begin
	------------------------------------------------------------------------
	-- Component Mapping
	------------------------------------------------------------------------ 
	inst_Module : Module
		PORT MAP(
			Reset         					=>  Reset,
			Clock              			=>  ClkPLL,
			LatchAllReadData				=>  LatchAllReadData_int(0),

			USIRxSerial         			=>  USIRxSerial,
			FWMajorRelease      			=>  FWMajorRelease,
			FWMinorRelease        		=>	 FWMinorRelease,
			FWDate           				=>	 FWDate,		
			Measurements					=>  Measurements_int,
			Status 							=>  Status_int,
			ModuleStatus					=>  ModuleStatus_int,
			ModuleWarnings					=>  ModuleWarnings_int,
			ModuleInterlocks_n			=>  ModuleInterlocks_n_int,
			CurrentValue1					=>  CurrentValue1_int,
			CurrentValue2					=>  CurrentValue2_int,
			CurrentValue3					=>  CurrentValue3_int,
			CurrentValue4					=>  CurrentValue4_int,
			CurrentValue5					=>  CurrentValue5_int,
			CurrentValue6					=>  CurrentValue6_int,
			CurrentValue7					=>  CurrentValue7_int,
			USITxSerial         			=>  USITxSerial,
			USIInHighSpeedMode			=>  USIInHighSpeedMode,

			Command							=>  Command_int,
			USIConfig						=>  USIConfig_int,
			AnalogValSelector				=>  AnalogValSelector_int,
			SetvalueToleranceFactor		=> SetvalueToleranceFactor_int			
			);

	inst_Interlocks : Interlocks
		port map(
			Reset								=>	Reset,  
			Clk								=> Clk, 
			ClrFault							=> ClrFault, 
			USICommand						=> Command_int,
			PIBlinkWdog						=> PIBlinkWdog, 
			InterlockIn_TC					=> InterlockIn_TC, 
			InterlockIn_LC1				=> InterlockIn_LC1,
			InterlockIn_LC2				=> InterlockIn_LC2,
			InterlockIn_DC1				=> InterlockIn_DC1,
			InterlockIn_DC2				=> InterlockIn_DC2,
			WarningIn_DelI1				=> WarningIn_DelI1,
			WarningIn_DelI2				=> WarningIn_DelI2,
			InterlockIn_QDS1				=> InterlockIn_QDS1,
			InterlockIn_QDS2				=> InterlockIn_QDS2,
			InterlockIn_QDS1_Model		=> InterlockIn_QDS1_Model,
			InterlockIn_QDS2_Model		=> InterlockIn_QDS2_Model,
			InterlockIn_MPS1_Model		=> InterlockIn_MPS1_Model,
			InterlockIn_MPS2_Model		=> InterlockIn_MPS2_Model,
			TripLineTx						=> TripLineTx,
			
			Interlock_MPS1					=> Interlock_MPS1,
			Interlock_MPS2					=> Interlock_MPS2,
			Interlock_MPS1_Model			=> Interlock_MPS1_Model,
			Interlock_MPS2_Model			=> Interlock_MPS2_Model,
			Interlock_QDS1_Model			=> Interlock_QDS1_Model,
			Interlock_QDS2_Model			=> Interlock_QDS2_Model,
			Interlock_LC					=> Interlock_LC,
			Fault_LC							=> Fault_LC,
			Interlock_DC					=> Interlock_DC,
			Fault_DC							=> Fault_DC,
			Warning_DelI					=> Warning_DelI,
			Fault_Warning_DelI			=> Fault_Warning_DelI,
			Interlock_QDS1					=> Interlock_QDS1,
			Interlock_QDS2					=> Interlock_QDS2,
			TripLineRx						=> TripLineRx
		);

	inst_PLL_USI : PLL_USI
		port map(
			areset					=> Reset,
			inclk0					=> Clk,
			c0							=> ClkPLL,
			locked					=> open 
		);
	-------------------------------------------------------
	--Clock transfer for data from CPU (30 MHz --> 100 MHz)
	-------------------------------------------------------
	Clk_Measurements : ClockDomainCrossing_simple
		generic map( 
			widthVector => 16
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => Measurements,
			DataOut           => Measurements_int
		);

	Clk_Status : ClockDomainCrossing_simple
		generic map( 
			widthVector => 16
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => Status,
			DataOut           => Status_int
		);	

	Clk_ModuleStatus : ClockDomainCrossing_simple
		generic map( 
			widthVector => 80
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => ModuleStatus,
			DataOut           => ModuleStatus_int
		);

	Clk_ModuleWarnings : ClockDomainCrossing_simple
		generic map( 
			widthVector => 80
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => ModuleWarnings,
			DataOut           => ModuleWarnings_int
		);

	Clk_ModuleInterlocks : ClockDomainCrossing_simple
		generic map( 
			widthVector => 80
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => ModuleInterlocks_n,
			DataOut           => ModuleInterlocks_n_int
		);

	Clk_CurrentValue1 : ClockDomainCrossing_simple
		generic map( 
			widthVector => 16
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => CurrentValue1,
			DataOut           => CurrentValue1_int
		);

	Clk_CurrentValue2 : ClockDomainCrossing_simple
		generic map( 
			widthVector => 16
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => CurrentValue2,
			DataOut           => CurrentValue2_int
		);

	Clk_CurrentValue3 : ClockDomainCrossing_simple
		generic map( 
			widthVector => 16
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => CurrentValue3,
			DataOut           => CurrentValue3_int
		);

	Clk_CurrentValue4 : ClockDomainCrossing_simple
		generic map( 
			widthVector => 16
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => CurrentValue4,
			DataOut           => CurrentValue4_int
		);

	Clk_CurrentValue5 : ClockDomainCrossing_simple
		generic map( 
			widthVector => 16
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => CurrentValue5,
			DataOut           => CurrentValue5_int
		);

	Clk_CurrentValue6 : ClockDomainCrossing_simple
		generic map( 
			widthVector => 16
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => CurrentValue6,
			DataOut           => CurrentValue6_int
		);

	Clk_CurrentValue7 : ClockDomainCrossing_simple
		generic map( 
			widthVector => 16
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => CurrentValue7,
			DataOut           => CurrentValue7_int
		);

	-----------------------------------------------------
	--Clock transfer for data to CPU (100 MHz --> 30 MHz)
	-----------------------------------------------------	
	Clk_Command : ClockDomainCrossing_simple
		generic map( 
			widthVector => 8
					)
		port map(
			ClockIn           => ClkPLL,	
			ClockOut          => Clk, 
			Reset             => Reset, 
			DataIn            => Command_int,
			DataOut           => Command
		);

	Clk_USIConfig : ClockDomainCrossing_simple
		generic map( 
			widthVector => 8
					)
		port map(
			ClockIn           => ClkPLL,	
			ClockOut          => Clk, 
			Reset             => Reset, 
			DataIn            => USIConfig_int,
			DataOut           => USIConfig
		);

	Clk_AnalogValSelector : ClockDomainCrossing_simple
		generic map( 
			widthVector => 8
					)
		port map(
			ClockIn           => ClkPLL,	
			ClockOut          => Clk, 
			Reset             => Reset, 
			DataIn            => AnalogValSelector_int,
			DataOut           => AnalogValSelector
		);

	Clk_SetvalueToleranceFactor : ClockDomainCrossing_simple
		generic map( 
			widthVector => 24
					)
		port map(
			ClockIn           => ClkPLL,	
			ClockOut          => Clk, 
			Reset             => Reset, 
			DataIn            => SetvalueToleranceFactor_int,
			DataOut           => SetvalueToleranceFactor
		);

	----------------------------------------------------------
	--Clock transfer for internal signals (30 MHz --> 100 MHz)
	----------------------------------------------------------
	Clk_LatchAllReadData : ClockDomainCrossing_simple
		generic map( 
			widthVector => 2
					)
		port map(
			ClockIn           => Clk,	
			ClockOut          => ClkPLL, 
			Reset             => Reset, 
			DataIn            => LatchAllReadData & LatchAllReadData,
			DataOut           => LatchAllReadData_int
		);


----------------------------------------------------------
end a;