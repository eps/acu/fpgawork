--------------------------------------------------------------------------------
-- (C) GE Power Conversion - 2022															--
--																										--
-- The copyright in this software is the property of GE Power Conversion.		--
--																										--
-- It is supplied on the express terms that it may not be copied, used or		--
-- disclosed to others for any purpose except as authorised by the above		--
-- named Company.																					--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- File 		: Interlocks.vhd																	--
-- Date 		: 09 Dec 2022																		--
-- Author 	: Britto A																			--
-- Project 	: GSI																					--
-- Board 	: 																						--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Modifications :																				--	
-- 10.02.23 - A.Britto - Added QDS interlock into MPS interlock					--  
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity Interlocks is	
	port(
		Reset							: in std_logic; 
		Clk							: in std_logic;
		ClrFault						: in std_logic;
		USICommand					: in std_logic_vector(7 downto 0);
		PIBlinkWdog					: in std_logic;
		InterlockIn_TC				: in std_logic;
		InterlockIn_LC1			: in std_logic;
		InterlockIn_LC2			: in std_logic;
		InterlockIn_DC1			: in std_logic;
		InterlockIn_DC2			: in std_logic;
		WarningIn_DelI1			: in std_logic;
		WarningIn_DelI2			: in std_logic;
		InterlockIn_QDS1			: in std_logic;
		InterlockIn_QDS2			: in std_logic;

		InterlockIn_QDS1_Model	: in std_logic;
		InterlockIn_QDS2_Model	: in std_logic;
		InterlockIn_MPS1_Model	: in std_logic;
		InterlockIn_MPS2_Model	: in std_logic;
		TripLineTx					: in std_logic;
		
		Interlock_MPS1				: out std_logic;
		Interlock_MPS2				: out std_logic;
		Interlock_MPS1_Model		: out std_logic;
		Interlock_MPS2_Model		: out std_logic;
		Interlock_QDS1_Model		: out std_logic;
		Interlock_QDS2_Model		: out std_logic;
		Interlock_LC				: out std_logic;
		Fault_LC						: out std_logic;
		Interlock_DC				: out std_logic;
		Fault_DC						: out std_logic;
		Warning_DelI				: out std_logic;
		Fault_Warning_DelI		: out std_logic;
		Interlock_QDS1				: out std_logic;
		Interlock_QDS2				: out std_logic;
		TripLineRx					: out std_logic
	);
end Interlocks;

architecture a of Interlocks is
		
	signal sPIBlinkWdog				: std_logic;
	signal sInterlock					: std_logic;
	signal snInterlock				: std_logic;
	signal sInterlock_LC				: std_logic;
	signal sInterlock_DC				: std_logic;
	signal sWarning_DelI				: std_logic;
	signal sInterlock_QDS1			: std_logic;
	signal sInterlock_QDS2			: std_logic;
	signal sInterlock_MPS1_Model	: std_logic;
	signal sInterlock_MPS2_Model	: std_logic;
	signal sCommandReset				: std_logic;

begin
	
	------------------------------------------------------------------------
	-- Process
	------------------------------------------------------------------------ 	
	Process_Interlocks: process (Clk, Reset)
   begin
		if (Reset = '1') then
			sPIBlinkWdog			<= '0';
			snInterlock				<= '0'; 
			sInterlock_LC			<= '0';
			sInterlock_DC			<= '0';
			sInterlock_QDS1		<= '0';
			sInterlock_QDS2		<= '0';
			sWarning_DelI			<= '0';
			sInterlock_MPS1_Model<= '0';
			sInterlock_MPS2_Model<= '0';
			
			Fault_LC					<= '0';
			Fault_DC					<= '0';
			Fault_Warning_DelI	<= '0';
			
		elsif (rising_edge(Clk)) then
			if(PIBlinkWdog = '1') then
				sPIBlinkWdog		<= '1';
			elsif(ClrFault = '1') then
				sPIBlinkWdog		<= '0';
			end if;
			
			if(InterlockIn_LC1 = '0' and InterlockIn_LC2 = '0') then
				sInterlock_LC		<= '1';
			elsif(ClrFault = '1') then
				sInterlock_LC 		<= '0';
			end if;
			
			if(InterlockIn_LC1 /= InterlockIn_LC2 ) then
				Fault_LC				<= '1';
			elsif(ClrFault = '1') then
				Fault_LC				<= '0';
			end if;
			
			if(InterlockIn_DC1 = '0' and InterlockIn_DC2 = '0') then
				sInterlock_DC		<= '1';
			elsif(ClrFault = '1') then
				sInterlock_DC		<= '0';
			end if;
			
			if(InterlockIn_DC1 /= InterlockIn_DC2) then
				Fault_DC				<= '1';
			elsif(ClrFault = '1') then
				Fault_DC				<= '0';
			end if;

			if(WarningIn_DelI1 = '0' and WarningIn_DelI2 = '0') then
				sWarning_DelI		<= '1';
			else
				sWarning_DelI		<= '0';
			end if;
			
			if(WarningIn_DelI1 /= WarningIn_DelI2) then
				Fault_Warning_DelI	<= '1';
			elsif(ClrFault = '1') then
				Fault_Warning_DelI	<= '0';
			end if;	
			
			if(InterlockIn_QDS1 = '0') then
				sInterlock_QDS1	<= '1';
			elsif(ClrFault = '1') then
				sInterlock_QDS1	<= '0';
			end if;
			
			if(InterlockIn_QDS2 = '0') then
				sInterlock_QDS2	<= '1';
			elsif(ClrFault = '1') then
				sInterlock_QDS2	<= '0';
			end if;			
				
			if(sInterlock = '1') then
				snInterlock		<= '0';
			elsif(ClrFault = '1') then
				snInterlock		<= '1';
			end if;
			
			if(InterlockIn_MPS1_Model = '0') then
				sInterlock_MPS1_Model		<= '1';
			elsif(ClrFault = '1') then
				sInterlock_MPS1_Model		<= '0';
			end if;
			
			if(InterlockIn_MPS2_Model = '0') then
				sInterlock_MPS2_Model		<= '1';
			elsif(ClrFault = '1') then
				sInterlock_MPS2_Model		<= '0';
			end if;
			
		end if; 
	end process;


	------------------------------------------------------------------------
	-- Direct Instantiation
	------------------------------------------------------------------------	
	sCommandReset		<= '1' when USICommand = x"03"	-- 0x03 is Reset Command
								else '0';
	sInterlock			<= InterlockIn_TC or sPIBlinkWdog or sInterlock_LC or sInterlock_DC or sInterlock_QDS1 or sInterlock_QDS2;--(not TripLineTx);
	Interlock_MPS1		<= snInterlock and (not sWarning_DelI) and TripLineTx;
	Interlock_MPS2		<= snInterlock and (not sWarning_DelI) and TripLineTx;
	Interlock_LC		<= sInterlock_LC;
	Interlock_DC		<= sInterlock_DC;
	Interlock_QDS1		<= sInterlock_QDS1;
	Interlock_QDS2		<= sInterlock_QDS2;
	Warning_DelI		<= sWarning_DelI;

	TripLineRx			<= (TripLineTx and snInterlock) or sCommandReset;
	Interlock_QDS1_Model<= InterlockIn_QDS1_Model;
	Interlock_QDS2_Model<= InterlockIn_QDS2_Model;
	Interlock_MPS1_Model<= sInterlock_MPS1_Model;
	Interlock_MPS2_Model<= sInterlock_MPS2_Model;

	
end a;