//============================================================================
// Name        : FPGA_ReconfWrap.cpp
// Author      : drodomon
// Version     :
// Copyright   : Your copyright notice
// Description : This program wraps the FPGA_Reconf.cpp one.
//               The wrap is necessary for:
//					* Polling the OCR FPGA reconfiguration command
//                    (USI2 HPS_FPGAreconfCmd).
//					* Monitoring the FPGA reconfiguration status and reporting
//                    it to the USI via OCR register (HPS2USI_FPGAreconfStatus).
//============================================================================

#include <iostream>
#include <numeric>
#include <cassert>
#include <sstream>

#include <fcntl.h>					// POSIX: "PROT_WRITE", "MAP_SHARED", ...
#include <sys/mman.h>				// POSIX: memory maping
#include <unistd.h>					// POSIX: for closing the Linux driver access

#include <string>
#include <time.h>

#include "bRegAcc.h"
#include <fstream>

// Register definition
//OCR
#define H2F_LW_AXI_BASE				0xFF200000
#define OCR_DT_TRANS_ST_OFFSET		0x4
#define OCR_FPGA_RECONF_CMD_OFFSET	0x8
#define OCR_FPGA_RECONF_ST_OFFSET	0xC

//OCR DATA MASK
#define RECONF_A_MASK				0x00000001
#define RECONF_F_MASK				0x00000002

//FPGA MNGR
#define FPGAMGRREGS_STAT			0xFF706000

//FPGA MNGR DATA MASK
#define FPGA_UM						0x00000004

int main() {

    int fd{};           						// file descriptor
    void* pMemMap_v;							// void pointer returning from the mmap

    // Open Linux Memory Driver port
    fd = open("/dev/mem", (O_RDWR | O_SYNC));

     // Check that the opening was successful
    if (fd < 0)
    {
        std::cout << "Error: Failed to open the memory driver!\n";
    }

    // I map the complete HW memory (1G) from 0xc0000000 to 0xffffffff.
    pMemMap_v = mmap( NULL, MAP_SIZE, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE & ~MAP_MASK);

    if (pMemMap_v == MAP_FAILED)
    {
        std::cout << "\n Error:  Failed to open the memory mapped interface\n";
        close(fd);
    }

    close(fd);

    // Object instances
    bRegAcc regObject{};			// Basic Register Access class(bRegAcc)

    uint32_t RdValue{0};			// tmp variable for the regObject.getReadValue() return value

	printf("Set OCR_FPGA_RECONF_ST_OFFSET\n");
	(void)  ((int) system( (const char*) "bregacc wr 0xFF20000C 0x00000002 0x0000000F"));

	printf("Start FPGA reconfiguration\n");
	printf("Please wait..\n\n");
	(void)  ((int) system( (const char*) "fpgarecfg -f /mnt/data/socfpgaA.rbf"));
	//(void)  ((int) system( (const char*) "fpgarecfg -f /mnt/data/ACU_MCVEVP_A.rbf"));//For debug only

    // READ FPGA MNGR STAT
    printf("Set OCR_FPGA_RECONF_ST_OFFSET\n");
    regObject.setCmd("rd");
    regObject.setAddr((uint32_t)(FPGAMGRREGS_STAT));
    regObject.ExeRdWr(false, pMemMap_v);
    RdValue= regObject.getReadValue();

    //Check for 0x4 FPGA in User Mode
    if (RdValue & FPGA_UM) {
    	// FPGA in user mode
    	(void)  ((int) system( (const char*) "bregacc wr 0xFF20000C 0x00000004 0x0000000F"));
    	printf("FPGA in user mode!\n");

    	// Copy the file into the /mnt/fat partition (overwrite)
    	(void)  ((int) system((const char*)  "cp -rf /mnt/data/socfpgaA.rbf /mnt/fat/socfpga.rbf"));

    	//NOTE:!!! Maybe here a check if the copy was succesfully executed

    	// Set file saved
    	regObject.setCmd("wr");
    	regObject.setAddr((uint32_t)(H2F_LW_AXI_BASE+OCR_DT_TRANS_ST_OFFSET));
    	regObject.setValue((uint32_t)(0x4));
    	regObject.setMask((uint32_t)(0x4));
    	regObject.ExeRdWr(false, pMemMap_v);

    } else {
    	printf("Something went wrong. Reconfiguring the FPGA with the factory image\n");
    	printf("Please wait..\n\n");

    	(void)  ((int) system( (const char*) "fpgarecfg -r"));

       	// Check again the FPGA MNGR STAT
       	regObject.setCmd("rd");
        regObject.setAddr((uint32_t)(FPGAMGRREGS_STAT));
        regObject.ExeRdWr(false, pMemMap_v);
        RdValue= regObject.getReadValue();

        // Write the FPGA MNGR STAT into OCR_FPGA_RECONF_ST_OFFSET register
        regObject.setCmd("wr");
        regObject.setAddr((uint32_t)(H2F_LW_AXI_BASE+OCR_FPGA_RECONF_ST_OFFSET));
        regObject.setValue((uint32_t)(RdValue));
        regObject.setMask((uint32_t)(0xffffffff));
        regObject.ExeRdWr(false, pMemMap_v);
 	}

    // Clear the Reconfig command
    (void)  ((int) system( (const char*) "bregacc wr 0xFF200008 0x00000000 0x0000000F"));

    // Read the SysID
    (void)  ((int) system( (const char*) "bregacc rd 0xFF201110"));

    std::cout << "deinit\n";
    // deinit
    munmap((void*) pMemMap_v, MAP_SIZE);


    return 0;
}
