#include <iostream>
#include <numeric>
#include <cassert>
#include <sstream>

#include <fcntl.h>					// POSIX: "PROT_WRITE", "MAP_SHARED", ...
#include <sys/mman.h>				// POSIX: memory maping
#include <unistd.h>					// POSIX: for closing the Linux driver access

#include "bRegAcc.h"

int main()
{

    bRegAcc regObject{};


    //We are mapping the complete HW memory space (1GB) from address 0xc0000000 to 0xffffffff
    int fd{};           // file descriptor
    void* pMemMap_v;    // void pointer variable used as return value for the mmap command (man mmap for more info)

    // Open Linux Memory Driver port
    fd = open("/dev/mem", (O_RDWR | O_SYNC));

    // Check that the opening was successful
    if (fd < 0)
    {
      std::cout << "Error: Failed to open the memory driver!\n";
      return -1;
    }

    pMemMap_v = mmap( NULL, MAP_SIZE, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE);

    if (pMemMap_v == MAP_FAILED)
    {
      std::cout << "\n Error:  Failed to open the memory mapped interface\n";
      close(fd);
      return -1;
    }


    while (regObject.getStatus()) {

        std::cout << "Enter a command <rd,wr,q(quit)>\n";

        regObject.getCmdFromUser();

        if (regObject.getCmd()=="q") {
            std::cout << "Bye!\n";
            break;
        }

        std::cout << "Enter an address <0xff200000>\n";
        regObject.setAddr(regObject.getStrAndReturnUint32());

        if (regObject.getCmd()=="wr") {
            std::cout << "Enter a value <0x12345678>\n";
            regObject.setValue(regObject.getStrAndReturnUint32());
            std::cout << "Enter a mask <0x0000ffff>\n";
            regObject.setMask(regObject.getStrAndReturnUint32());
        }

        regObject.printReg();
        regObject.ExeRdWr(true, (uint32_t*) pMemMap_v);

    }

    // deinit
    munmap((void*) pMemMap_v, MAP_SIZE);
    close(fd);

    return 0;
}
