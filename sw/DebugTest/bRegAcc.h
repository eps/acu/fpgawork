



#ifndef BREGACC_H
#define BREGACC_H

#include <iostream>
#include <numeric>

//Constants to do mmap and get access to FPGA and HPS peripherals
#define HPS_FPGA_BRIDGE_BASE 0xC0000000
#define HW_REGS_BASE ( HPS_FPGA_BRIDGE_BASE )

#define MAP_SIZE ( 0x40000000 )
#define MAP_MASK (MAP_SIZE - 1)


class bRegAcc {

    private:
	    uint32_t m_addr{};
	    uint32_t m_val{};
	    uint32_t m_mask{0xffffffff};
	    std::string m_cmd2exe{};
        bool m_status{true};         // true=Ok, false= something went wrong.
        uint32_t m_rdVal{};          // Location read value

    public:
        // Constructor default
        bRegAcc()=default;

        bool getStatus();
        std::string getCmd();
        uint32_t getReadValue();
        void getCmdFromUser();
        uint32_t getStrAndReturnUint32();

        void setCmd(const std::string& cmd);
        void setAddr(const uint32_t& addr);
        void setValue(const uint32_t& val);
        void setMask(const uint32_t& mask);

        void printReg();
        bool checkIfInputIsHex(std::string input);
        void printReadValue();
        void ExeRdWr(const bool& PrintOut,void* pMemMap_v);
};

#endif
