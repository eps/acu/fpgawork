//============================================================================
// Name        : HsBackStatusGen.cpp
// Author      : drodomon
// Version     : 0.0
// Copyright   :
// Description : This program generates the PSU status information sent back
//               via USI to the MFU.
//               The generation is  based on the command received via USI,
//               the trip-line status (both incoming and outgoing) and the
//               PSU actual state.
//               There is just one parameter to set in order to enable the
//               terminal printout. By default it is disabled.
//============================================================================
#include "myFunctions.h"

#include <iostream>
#include <chrono>
#include <thread>

using namespace std;

int main() {

	int i=0;
	uint32_t *p;


    // Infinite loop with 5 seconds iteration time
	while (true)
	{
		cout << "iteration nr " << i << endl;

		// 5 seconds timer setting
		thread timer([]() {
			this_thread::sleep_for(std::chrono::seconds(5));
			cout << "Polling.."<<endl;
		});
		std::cout << "Wait for next reading..." << std::endl;
		timer.join(); //Before program finishes. Basically "Wait for thread end"


		// As soon as the iteration timer expires, the registers are polled
		p = pollingReg();
		cout << "Decode received string" << endl;


		// The read registers values are decoded below
		//Mon
		bool decMon=decodeMon(*(p + 0));

		//Command
		bool receivedCmd[3]={false, false,false}; // On,Off,Reset
		// Based on the read values, the bool array will be set and used for the status generation
		if (*(p + 1)==1) {
			receivedCmd[0]=true;//On command received
		} else if (*(p + 1)==2) {
			receivedCmd[1]=true;//Off command received
		} else if (*(p + 1)==3) {
			receivedCmd[2]=true;//Reset command received
		} else {
			for (int k=0;k<3;k++) {
				receivedCmd[k]= false;
			}
		}

		//Old Status
		bool oldStatus[7]={false, false,false,false, false,false}; 	// Off 0x2,
																	// LoadingCApacitorBank 0x3
																	// Switching On 0x4
																	// On 0x7
																	// Switching Off 0x8
																	// Reset 0xB
		// Based on the read values, the bool array will be set and used for the status generation
		if (*(p + 2)==2) {
			oldStatus[0]=true;//Off state
		} else if (*(p + 2)==3) {
			oldStatus[1]=true;//Loading Capacitor Bank state
		} else if (*(p + 2)==4) {
			oldStatus[2]=true;//Switching On state
		} else if (*(p + 2)==7) {
			oldStatus[3]=true;//On state
		} else if (*(p + 2)==8) {
			oldStatus[4]=true;//Switching Off state
		} else if (*(p + 2)==11) {
			oldStatus[5]=true;//Reset state
		} else {
			for (int k=0;k<6;k++) {
				oldStatus[k]= false;
			}
		}

		// Status generation
		cout << "Write HS back status register" << endl;

		// Parameter array
		char* HsBckOffState[4]= {"wr","0xff200110","0x00000002","0x0000ffff"};     // Write HSbackStatus Off state
		char* HsBckRstState[4]= {"wr","0xff200110","0x0000000B","0x0000ffff"};     // Write HSbackStatus Reset state
		char* HsBckLCBState[4]= {"wr","0xff200110","0x00000003","0x0000ffff"};     // Write HSbackStatus Load capacitor bank state
		char* HsBckSOnState[4]= {"wr","0xff200110","0x00000004","0x0000ffff"};     // Write HSbackStatus Switching On state
		char* HsBckOnState[4]=  {"wr","0xff200110","0x00000007","0x0000ffff"};     // Write HSbackStatus On state
		char* HsBckSOffState[4]={"wr","0xff200110","0x00000008","0x0000ffff"};     // Write HSbackStatus Switching off state
		char* SetClr[4]={"wr","0xff2000b0","0x00000010","0x00000010"};     // Set clear
		char* UsetClr[4]={"wr","0xff2000b0","0x00000000","0x00000010"};     // Unset clear

		// check for trip-line active
		if (!(decMon)) {
			// Set Off state because there is a tripline active
			if (bregacc(4,HsBckOffState)<0) {
				cout << "Error writing HsBckOffState" << endl;
				return -1;
			}
		} else {
			// No tripline active

			if (oldStatus[3] && receivedCmd[1]) {
				// the actual status is On and an off command is received-> Switching off procedure
				// Set Switching Off state
				if (bregacc(4,HsBckSOffState)<0) {
					cout << "Error writing HsBckSOffState" << endl;
					return -1;
				}

				// Wait for two seconds (it could be more or less)
				thread IntTimer([]() {
					this_thread::sleep_for(std::chrono::seconds(2));
					cout << "Set Off state"<<endl;
				});
				cout << "Waitng 2 sec.." << std::endl;
				IntTimer.join();

				// Set Off state
				if (bregacc(4,HsBckOffState)<0) {
					cout << "Error writing HsBckOffState" << endl;
					return -1;
				}
			} else if (oldStatus[0] && receivedCmd[0]) {
				// the actual status is Off and an on command is received-> Switching on procedure
				// Set Loading capacitor bank state
				if (bregacc(4,HsBckLCBState)<0) {
					cout << "Error writing HsBckLCBState" << endl;
					return -1;
				}
				// Wait for three seconds (it could be more or less)
				thread IntTimer([]() {
					this_thread::sleep_for(std::chrono::seconds(3));
					cout << "Set Switching On state"<<endl;
				});
				std::cout << "Waitng 3 sec.." << std::endl;
				IntTimer.join();

				// Set Switching On state
				if (bregacc(4,HsBckSOnState)<0) {
					cout << "Error writing HsBckSOnState" << endl;
					return -1;
				}
				// Wait for four seconds (it could be more or less)
				thread IntTimer1([]() {
					this_thread::sleep_for(std::chrono::seconds(4));
					cout << "Set On state"<<endl;
				});
				std::cout << "Waitng 4 sec.." << std::endl;
				IntTimer1.join();

				// Set On state
				if (bregacc(4,HsBckOnState)<0) {
					cout << "Error writing HsBckOnState" << endl;
					return -1;
				}
			}
		}

		// Reset command Received: It has to be detected and executed independently of everything
		if(receivedCmd[2]) {
			// Set the ClrFault signal
			if (bregacc(4,SetClr)	<0) {
				cout << "Error writing SetClr" << endl;
				return -1;
			}
			// Unset the ClrFault signal
			if (bregacc(4,UsetClr)	<0) {
				cout << "Error writing UsetClr" << endl;
				return -1;
			}
			// Set Reset command state
			if (bregacc(4,HsBckRstState)<0) {
				cout << "Error writing HsBckRstState" << endl;
				return -1;
			}
			// Wait for one second (it could be more or less)
			thread IntTimer([]() {
				this_thread::sleep_for(std::chrono::seconds(1));
				cout << "Set back the old status info"<<endl;
			});
			std::cout << "Waitng 1 sec.." << std::endl;
			IntTimer.join();

			// Set the old status back (it can be on or off only)
			if (oldStatus[3]){
				if (bregacc(4,HsBckOnState)<0) {
					cout << "Error writing HsBckOnState" << endl;
					return -1;
				}
			} else if (oldStatus[0]) {
				if (bregacc(4,HsBckOffState)<0) {
					cout << "Error writing HsBckOffState" << endl;
					return -1;
				}
			} else {
				cout << "Error writing Old status back" << endl;
				return -1;
			}
		}
		i++;
	}

	return 0;
}
