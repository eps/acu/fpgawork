//============================================================================
// Name        : StWrnInt.cpp
// Author      : drodomon
// Version     :
// Copyright   : Your copyright notice
// Description : This program drives the 80 bits status, warning and iterlock
//               inputs setting them first one by one (one hot style).
//============================================================================
#include "myFunctions.h"

#include <iostream>
#include <chrono>
#include <thread>
#include <cmath>
#include <sstream>
#include <iomanip>
#include <ios>
#include <cstring>

using namespace std;

int main(int argc, char** argv) {

	if (argc > 2 || argc == 1) {
		cout << "StWrnInt <St/Wrn/Int>" << endl;
		cout << "\t St-> Status bit manipulated" << endl;
		cout << "\t Wrn -> Warning bit manipulated" << endl;
		cout << "\t Int -> Interlock bit manipulated" << endl;

	} else {
		// Change Mux selector
		char* SelSt[4]= {"wr","0xff2000b0","0x00000001","0x00000007"};     // Select status mux
		char* SelWrn[4]= {"wr","0xff2000b0","0x00000002","0x00000007"};     // Select warning mux
		char* SelInt[4]= {"wr","0xff2000b0","0x00000004","0x00000007"};     // Select interlocks mux

		// Select the mux to drive
		if ((std::string(argv[1]) == "St")) {
			cout << "Status mux selected"<< endl;
			if (bregacc(4,SelSt)<0) {
				cout << "Error writing SelSt" << endl;
				return -1;
			}
		} else if ((std::string(argv[1]) == "Wrn")) {
			cout << "Warning mux selected"<< endl;
			if (bregacc(4,SelWrn)<0) {
				cout << "Error writing SelWrn" << endl;
				return -1;
			}
		} else if ((std::string(argv[1]) == "Int")) {
			cout << "Interlock mux selected"<< endl;
			if (bregacc(4,SelInt)<0) {
				cout << "Error writing SelInt" << endl;
				return -1;
			}
		}

		// GEnerate the one hot sequence for the 16x5 (80b are split in 5 registers 16b each)input signals
        uint32_t val=0;
        // 5 registers adresses
		char* STWRNINT[5]={"0xff200100","0xff2000f0","0xff2000e0","0xff2000d0","0xff2000c0"};

		for (int i=0;i<5;i++){
			// Char array to modify
			char* Wr80b[4]= {"wr","0xff200100","0x00000000","0x0000ffff"};
			// Change in the char array the register address
			Wr80b[1]=STWRNINT[i];

			// Clear the register
			if (bregacc(4,Wr80b)<0) {
				cout << "Error writing Wr80b" << endl;
 		  	    return -1;
			}
            /*cout << "i=" << i << endl;
			for (int y=0;y<4;y++) {
				cout << "Wr80b["<<y<<"]= "<<Wr80b[y] << endl;
			}
            */
			for (int k=0;k<16;k++) {
			  // val=2^k
			  val=pow(2,k);
			  // int to hex string conversion with 8 symbol extension
			  std::stringstream stream;
			  stream <<"0x"<< setfill('0')<<setw(8)<< std::hex << val;
			  std::string result( stream.str() );

			  // String to char conversion
			  char src[10]; // 8+ "0x"= 10 symbols length
			  strcpy(src, result.c_str());
			  // Change in the char array the value to write
			  Wr80b[2]=src;
			  /*
			  cout << "k=" << k << endl;
			  for (int y=0;y<4;y++) {
				cout << "Wr80b["<<y<<"]= "<<Wr80b[y] << endl;
			  }
			  */
			  // Write value
			  if (bregacc(4,Wr80b)<0) {
				  cout << "Error writing Wr80b" << endl;
			  	  return -1;
			  }

			  // Wait 2 seconds
			  thread IntTimer([]() {
			  		this_thread::sleep_for(std::chrono::seconds(2));
			  		cout << "Next!"<<endl;
			  });
			  cout << "Waitng 2 sec.." << std::endl;
			  IntTimer.join();
			}

		}

	}
	return 0;
}
