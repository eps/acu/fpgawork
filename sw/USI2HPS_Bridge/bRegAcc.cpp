/*
 * bRegAcc.cpp
 *
 *  Created on: Aug 14, 2023
 *      Author: drodomon
 */
#include <iostream>
#include <numeric>
#include <cassert>
#include <sstream>

#include <fcntl.h>					// POSIX: "PROT_WRITE", "MAP_SHARED", ...
#include <sys/mman.h>				// POSIX: memory maping
#include <unistd.h>					// POSIX: for closing the Linux driver access

#include "bRegAcc.h"

bool bRegAcc::getStatus() {
    return m_status;
};

void bRegAcc::setStatus(const bool& St) {
    m_status=St;
};

std::string bRegAcc::getCmd(){
            return m_cmd2exe;
};

uint32_t bRegAcc::getReadValue(){
	return m_rdVal;
};

void bRegAcc::setCmd(const std::string& cmd){
	if (cmd!="rd" && cmd!="wr") {
		std::cout << "Error: Wrong command. Select between <rd,wr>. Default rd set\n";
		m_cmd2exe="rd";
	} else {
		m_cmd2exe=cmd;
	}
};


void bRegAcc::setAddr(const uint32_t& addr){
            m_addr=addr;
};
void bRegAcc::setValue(const uint32_t& val){
            m_val=val;
};
void bRegAcc::setMask(const uint32_t& mask){
            m_mask=mask;
};
void bRegAcc::printReg(){
            std::cout<<"Register action:\n";
            std::cout<<"\t Command to execute: \t\t\t "<< m_cmd2exe<<".\n";
            std::cout<<"\t\t @Address: \t\t\t 0x"<< std::hex << m_addr<<".\n";
            if (m_cmd2exe=="wr") {
                std::cout<<"\t\t Value to write: \t\t 0x"<< std::hex << m_val << ".\n";
                std::cout<<"\t\t With the following mask: \t 0x"<< std::hex << m_mask << ".\n";
            }
};
void bRegAcc::getCmdFromUser(){
            std::cin >> m_cmd2exe;
            std::cin.clear();
            if (m_cmd2exe!="rd" && m_cmd2exe!="wr" && m_cmd2exe!="q") {
                std::cout << "Error: Wrong command. Select between <rd,wr,q>\n";
                m_status=false;
            }
};
//Check if a string is hexadecimal
bool bRegAcc::checkIfInputIsHex(std::string input) {
	        if (input.length() < 1) return false;

	        // remove suffix "0x"
	        if ((input.find_first_of("0x",0) == 0))
	        {
		        input.replace(0, 2, "");
	        }

            uint16_t i = 0;
	        for (i = 0; i < input.length(); i++)
        	{
		        if (i != input.find_first_of("0123456789abcdefABCDEF", i)) break;
	        }
	        if (i == input.length()) return true;

	        return false;
};
uint32_t bRegAcc::getStrAndReturnUint32() {
            std::string tmpStr{};
            uint32_t tmpRetVal{};
            std::cin >> tmpStr;
            std::cin.clear();

            //check length
            //std::cout << "strLength= " <<tmpStr.length() << "\n";
            std::size_t xPos=tmpStr.find_first_of('x');
            if (xPos!=std::string::npos){
                tmpStr= tmpStr.substr(xPos+1,tmpStr.length()-xPos-1);
            }

            if(tmpStr.length()>8) {
                std::cout << "Error: Too long hex value. 32b values allowed\n";
                m_status=false;
            } else {
                if (checkIfInputIsHex(tmpStr)) {
                    std::stringstream buffer(tmpStr);
                    buffer >> std::hex >> tmpRetVal;

                    //return tmpRetVal;
                 } else {
                    std::cout << "Error: The entered value is not an hex\n";
                    m_status=false;
                };
            };

            return tmpRetVal;
};
void bRegAcc::printReadValue(){
            std::cout<< "Read address: " << m_addr << "\t; Value: " << m_rdVal << ".\n";
};

// This is the function that executes the register read/write action.
// There are two parameters: * the boolean PrintOut to enable or disable the console print out.
//                           * the void pointer to the virtual memory space (mmaps return value)
void bRegAcc::ExeRdWr(const bool& PrintOut, void* pMemMap_v) {

			uint32_t* pMemMap;  // Pointer to the mapped virtual memory register

			/*
			int fd{};           // file descriptor
            void* pMemMap_v;    // void pointer variable used as return value for the mmap command (man mmap for more info)

            // Open Linux Memory Driver port
            fd = open("/dev/mem", (O_RDWR | O_SYNC));

             // Check that the opening was successful
            if (fd < 0)
	        {
                std::cout << "Error: Failed to open the memory driver!\n";
                m_status=false;
	        }
            // I map the complete page where the address is located.
            // In this way I have not to deal with some strange address calculation for the virtual memory pointer (I simply make addr&mask)
            //	ex:
            //		addr=0xff201110 => 	offset		=0xff201000;
            //							pointer		=0x00000110 + pMemMap_v;


            //std::cout << "HW Address= " << std::hex << m_addr << "\n";
            //std::cout << "MAP_MASK= "   << std::hex << MAP_MASK << "\n";
            //uint32_t fuffa=m_addr & ~MAP_MASK;
            //std::cout << "map offset= " << std::hex << fuffa << "\n";
            //std::cout << "MAP_SIZE= "   << std::hex << MAP_SIZE << "\n";

            pMemMap_v = mmap( NULL, MAP_SIZE, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, m_addr & ~MAP_MASK);

            if (pMemMap_v == MAP_FAILED)
	        {
		        std::cout << "\n Error:  Failed to open the memory mapped interface\n";
                close(fd);
                m_status=false;
	        }

			*/

            //std::cout << "\n";
            //std::cout << "\n";
            //std::cout << "Virtual memory start Address(pMemMap_v)= " << std::hex << pMemMap_v << "\n";
            //fuffa=m_addr & MAP_MASK;
            //std::cout << "Virtual memory offset(m_addr & MAP_MASK)= " << std::hex << fuffa << "\n";
            //std::cout << "\n";
            //std::cout << "\n";
            //uint32_t* ptrTest= (uint32_t*)(pMemMap_v +(m_addr & MAP_MASK));
            //std::cout << "(uint32_t*)(pMemMap_v +(m_addr & MAP_MASK))= " << std::hex << ptrTest << "\n";


            // Set the pointer to the register address to read: returned pointer from mmap + virtual memory offset (m_addr & MAP_MASK).
            // Please note that a uint32_t conversion is necessary to have the pointer with the same data type it is pointing at.
            pMemMap = (uint32_t*)(pMemMap_v +(m_addr & MAP_MASK));

            // Check if the pointer is pointing at something
            if (pMemMap==0)
            {
                std::cout << "\nError: Failed to allocate a pointer to the memory mapped Address Space\n";
                //munmap((void*) pMemMap_v, 0x4);
                //close(fd);
                m_status=false;
            }

            if (m_cmd2exe=="wr") {
                if (PrintOut) {
                    std::cout << "Writing...\n";
                    std::cout << "   Address:     " << m_addr <<std::hex<<" [0x"<< m_addr<<"]"<<std::dec<<".\n";
                    std::cout << "   Value:       " << m_val <<std::hex<<" [0x"<< m_val<<"]"<<std::dec<<".\n";
                    std::cout << "   Mask:        " << m_mask<<std::hex<<" [0x"<< m_mask<<"]"<<std::dec<<".\n";
                }
                //*ptrMemMapped = Dt2Wr & Dt2WrMask;
                *pMemMap = (*pMemMap & ~m_mask) | (m_val & m_mask);
                if (PrintOut) {
                	std::cout << "Done! \n";
                }
            } else {
                //Read
       	        m_rdVal = *pMemMap;
                if (PrintOut) {
                    std::cout << "Reading...\n";
                    std::cout << "   Address:     " << m_addr <<std::hex<<" [0x"<< m_addr<<"]"<<std::dec<<".\n";
                    std::cout << "   Value:       " << m_rdVal <<std::hex<<" [0x"<< m_rdVal<<"]"<<std::dec<<".\n";
                    std::cout << "Done! \n";
                }
            }
            /*
            // deinit
            munmap((void*) pMemMap_v, MAP_SIZE);
            close(fd);
            */
};
