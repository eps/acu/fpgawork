//============================================================================
// Name        : USI2HPS_Bridge.cpp
// Author      : drodomon
// Version     :
// Copyright   : Your copyright notice
// Description : This program polls two OCR registers and, based on their content,
//               it calls the DtTransfer function or the FPGA_ReconfWrap one.
//============================================================================

#include <iostream>
#include <numeric>
#include <cassert>
#include <sstream>

#include <fcntl.h>					// POSIX: "PROT_WRITE", "MAP_SHARED", ...
#include <sys/mman.h>				// POSIX: memory maping
#include <unistd.h>					// POSIX: for closing the Linux driver access

#include <string.h>
#include <time.h>

#include "bRegAcc.h"
#include <fstream>

// Register definition
//FIFO
//#define H2F_AXI_BASE				0xC0000000
//#define FIFO_CSR_FILL_LEVEL_OFFSET 	0x0
//#define FIFO_CSR_I_STATUS 			0x4
//#define FIFO_CSR_EVENT 				0x8
//#define FIFO_CSR_INTERRUPT 			0xC
//#define FIFO_CSR_ALMOST_FULL 		0x10
//#define FIFO_CSR_ALMOST_EMPTY 		0x14
//#define FIFO_DATA_OFFSET 			0x20

//OCR
#define H2F_LW_AXI_BASE				0xFF200000
#define OCR_DT_TRANS_CFG_OFFSET		0x0
#define OCR_FPGA_RECONF_CMD_OFFSET	0x8

//OCR DATA MASK
#define READY_DT_TRANS_CFG_MASK		0x02000000
#define RECONF_A_MASK				0x00000001

int main() {

    int fd{};           						// file descriptor
    void* pMemMap_v;							// void pointer returning from the mmap
    uint32_t RdValue{0};						// tmp variable for the regObject.getReadValue() return value

    const uint32_t Reg2Poll[2]{(uint32_t) H2F_LW_AXI_BASE+OCR_DT_TRANS_CFG_OFFSET,(uint32_t) H2F_LW_AXI_BASE+OCR_FPGA_RECONF_CMD_OFFSET}; // DtTransf first


    // The solution proposed by Tim to make system flexible with the temp string works
    // so it will be good to adapt that everywhere in the code.
    //std::string tmpStr{};

    // Open Linux Memory Driver port
    fd = open("/dev/mem", (O_RDWR | O_SYNC));

     // Check that the opening was successful
    if (fd < 0)
    {
        std::cout << "Error: Failed to open the memory driver!\n";
    }

    // I map the complete HW memory (1G) from 0xc0000000 to 0xffffffff.
    pMemMap_v = mmap( NULL, MAP_SIZE, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE & ~MAP_MASK);

    if (pMemMap_v == MAP_FAILED)
    {
        std::cout << "\n Error:  Failed to open the memory mapped interface\n";
        close(fd);
    }

    close(fd);

    // Object instances
    bRegAcc regObject{};			// Basic Register Access class(bRegAcc)


    //Read in an infinite loop the register defined in the Reg2Poll array
    regObject.setCmd("rd");
    printf("Polling registers..\n");

    while(true) {
    	for(int i=0;i<2;i++) {
    		regObject.setAddr(Reg2Poll[i]);
    		regObject.ExeRdWr(false, pMemMap_v);
    		RdValue= regObject.getReadValue();

    		switch (i) {
    		  case 0:
    			  if(RdValue & READY_DT_TRANS_CFG_MASK) {
    				  printf("Running dtTrasnfer\n");
    				  //tmpStr="dtTrasnfer " + std::to_string(RdValue);
    				  //(void)  ((int) system(tmpStr.c_str()));
    				  (void)  ((int) system((const char*)  "dttransfer"));
    				  printf("Data tranfer completed\n");
    				  printf("Polling registers..\n");
    			  }
    			  break;
    		  case 1:
    			  if(RdValue & RECONF_A_MASK) {
    				  printf("Running FPGAreconfWrap");
    				  (void)  ((int) system((const char*)  "fpgarecfgwrap"));
    				  printf("FPGA reconfiguration completed\n");
    				  printf("Polling registers..\n");
    			  }
    			  break;
    		}
    	}
    }

	return 0;
}
