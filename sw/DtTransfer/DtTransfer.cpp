//============================================================================
// Name        : DtTransfer.cpp
// Author      : drodomon
// Version     :
// Copyright   : Your copyright notice
// Description : Endless loop that monitors the OCR USI2HPS_DtTransCfg and
//               USI2HPS_FPGA_ReconfCmd.
//============================================================================

#include <iostream>
#include <numeric>
#include <cassert>
#include <sstream>

#include <fcntl.h>					// POSIX: "PROT_WRITE", "MAP_SHARED", ...
#include <sys/mman.h>				// POSIX: memory maping
#include <unistd.h>					// POSIX: for closing the Linux driver access

#include <string>
#include <time.h>

#include "bRegAcc.h"
#include <fstream>

// Register definition
//FIFO
#define H2F_AXI_BASE				0xC0000000
//#define FIFO_CSR_FILL_LEVEL_OFFSET 	0x0
#define FIFO_CSR_I_STATUS 			0x4
//#define FIFO_CSR_EVENT 				0x8
//#define FIFO_CSR_INTERRUPT 			0xC
//#define FIFO_CSR_ALMOST_FULL 		0x10
//#define FIFO_CSR_ALMOST_EMPTY 		0x14
#define FIFO_DATA_OFFSET 			0x20
//OCR
#define H2F_LW_AXI_BASE				0xFF200000
#define OCR_DT_TRANS_CFG_OFFSET		0x0
#define OCR_DT_TRANS_ST_OFFSET		0x4
//#define OCR_FPGA_RECONF_CMD_OFFSET	0x8
//#define OCR_FPGA_RECONF_ST_OFFSET	0xC
//#define OCR_DT_FROM_USI_OFFSET		0x10

//PIO
#define PIO_OFFSET		            0x1100
#define PIO_FIFO_RESET_MASK         0x80

//OCR DATA MASK
#define CLEAR_DT_TRANS_ST_MASK		0x00000007
#define READY_DT_TRANS_ST_MASK		0x00000001
//#define END_DT_TRANS_ST_MASK		0x00000002
//#define SAVED_DT_TRANS_ST_MASK		0x00000004

#define READY_DT_TRANS_CFG_MASK		0x02000000
#define FILE_SIZE_MASK				0x00FFFFFF
#define FILE_TYPE_MASK				0xF0000000
#define RBF_FILE_MASK				0x00000000
#define TXT_FILE_MASK				0x10000000
#define INFO_FILE_MASK				0x20000000
#define LOG_FILE_MASK				0x30000000


class dDtTransCfg {
	private:
		int m_nrByte{};
		std::string m_fileExt{".txt"};

	public:
		// Constructor default
		dDtTransCfg()=default;

		void decodeDtTransCfg(const uint32_t& value);
		const int& getNrByte();
		std::string& getFileExt();
};

void dDtTransCfg::decodeDtTransCfg(const uint32_t& value){
	m_nrByte= value & FILE_SIZE_MASK;
	switch(value & FILE_TYPE_MASK){
		case RBF_FILE_MASK:
			m_fileExt=".rbf";
			break;
		case TXT_FILE_MASK:
			m_fileExt=".txt";
			break;
		case INFO_FILE_MASK:
			m_fileExt=".info";
			break;
		case LOG_FILE_MASK:
			m_fileExt=".log";
			break;
		default:
			m_fileExt=".rbf";
			break;
	};
};

const int& dDtTransCfg::getNrByte(){
	return m_nrByte;
};
std::string& dDtTransCfg::getFileExt(){
	return m_fileExt;
};

void clearCmd(bRegAcc& regObject, uint32_t const addr,uint32_t const mask,void*& pMemMap_v) {
	//Set OCR_DT_TRANS_ST_OFFSET
	//printf("Set OCR_DT_TRANS_ST_OFFSET\n");
	regObject.setCmd("wr");
	regObject.setAddr(addr);
	regObject.setValue((uint32_t)(0x0));
	regObject.setMask(mask);
	regObject.ExeRdWr(false, pMemMap_v);
}

int main(int argc, const char* argv[]) {

    int fd{};           						// file descriptor
    void* pMemMap_v;							// void pointer returning from the mmap
    std::ofstream myfile;						// file (written in the emmc) variable definition

    // NOTE:	The file size to be stored is fetched in the OCR_DT_TRANS_CFG_OFFSET.
    //			Because the FIFO location is 4B deep, the number of read action to perform in order to fetch
    //			the complete file are FileSize/4.
    //			The result could contain decimals symbols, so for this reason the mod4 is evaluated as well.
    //			If mod4>0, the division result is incremented by one unit and the last myfile streaming size will be
    //			equal to mod4.
    //			In case mod4=0, then FileSize in a multiple of 4 and the last myfile streaming size will be equal to
    //			previous ones(4).
    int fileSize{0};
    int fileSize_mod4{0};
    int cntFileSize{0};							// file size counter


   	// Open Linux Memory Driver port
   	fd = open("/dev/mem", (O_RDWR | O_SYNC));

   	// Check that the opening was successful
   	if (fd < 0)
   	{
   		std::cout << "Error: Failed to open the memory driver!\n";
   	}

   	// I map the complete HW memory (1G) from 0xc0000000 to 0xffffffff.
   	pMemMap_v = mmap( NULL, MAP_SIZE, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE & ~MAP_MASK);

   	if (pMemMap_v == MAP_FAILED)
   	{
   		std::cout << "\n Error:  Failed to open the memory mapped interface\n";
   		close(fd);
   	}

   	close(fd);

   	// Object instances
   	bRegAcc regObject{};			// Basic Register Access class(bRegAcc)
   	dDtTransCfg decodeCfg{};

   	uint32_t RdValue{0};			// tmp variable for the regObject.getReadValue() return value


    //Setup the OCR_DT_TRANS_CFG_OFFSET polling in order to check when a new data transfer has to start
    regObject.setCmd("rd");
    regObject.setAddr((uint32_t)(H2F_LW_AXI_BASE+OCR_DT_TRANS_CFG_OFFSET));
   	regObject.ExeRdWr(false, pMemMap_v);
   	RdValue= regObject.getReadValue();

   	decodeCfg.decodeDtTransCfg(RdValue);


   	printf("File extension: %s \n", decodeCfg.getFileExt().c_str());
   	printf("File size: %d\n",decodeCfg.getNrByte());
   	fileSize=decodeCfg.getNrByte()/4;
   	printf("File size as number of FIFO rd actions: %d\n", fileSize);
	fileSize_mod4=decodeCfg.getNrByte()%4;
   	printf("Remaining: %d\n", fileSize_mod4);

	if (fileSize_mod4>0) {
		fileSize++;
	} else {
		fileSize_mod4=4;
	}
   	printf("File size resized: %d\n", fileSize);

    uint32_t* data=new uint32_t [fileSize];		// this buffer is used as temp storage buffer because,
       											// when the FIFO is written with 2Msps,
       											// the direct write action to file is too slow
    											// and the FIFO gets full.

   	//Clear OCR_DT_TRANS_ST_OFFSET
   	printf("Clear OCR_DT_TRANS_ST_OFFSET \n");
   	regObject.setCmd("wr");
   	regObject.setAddr((uint32_t)(H2F_LW_AXI_BASE+OCR_DT_TRANS_ST_OFFSET));
   	regObject.setValue((uint32_t)(0x0));
   	regObject.setMask((uint32_t)(CLEAR_DT_TRANS_ST_MASK));
   	regObject.ExeRdWr(false, pMemMap_v);


   	//open the file where the data will be stored
   	if(decodeCfg.getFileExt()==".rbf") {
   	   	myfile.open("/mnt/data/socfpgaA"+decodeCfg.getFileExt(), std::ios::out|std::ios::binary);
   	} else {
   	   	myfile.open ("/mnt/data/DataFromFPGA"+decodeCfg.getFileExt(), std::ios::out|std::ios::binary);
   	}

   	if (!myfile) {
       	printf("Error opening the file\n");
       	clearCmd(regObject, (uint32_t)(H2F_LW_AXI_BASE+OCR_DT_TRANS_CFG_OFFSET),(uint32_t)(READY_DT_TRANS_CFG_MASK),pMemMap_v);
       	return -1;
    }
    printf("File opened \n");

    //Set HPS ready to receive data
    printf("Set HPS ready to receive data\n");

    regObject.setValue((uint32_t)(0x1));
    regObject.setMask((uint32_t)(READY_DT_TRANS_ST_MASK));
    regObject.ExeRdWr(false, pMemMap_v);

    /*
     * NOTE: 	Unfortunately it seems that acting with an object for the FIFO status and data feching
     * 			takes too much time and the FIFO gets full.
     * 			For this reason, the FIFO reset release, the status and the data fetching are done "locally"
     * 			through pMemMap variable. (maybe the code can be better written and everything would be
     * 			possible via objects).
     */
    cntFileSize=0;

    // The FIFO will stay under reset when the data transfer is not enabled
    printf("Release FIFO and fetch the data\n");
    uint32_t* pMemMap = (uint32_t*)(pMemMap_v +((uint32_t)((H2F_LW_AXI_BASE+PIO_OFFSET) & MAP_MASK)));
    *pMemMap= (uint32_t)0x80;

    // loop till the amount of data to fetch is reached or till the FIFO gets full.
    while (cntFileSize<fileSize) {
       	// Fetch the status
        pMemMap = (uint32_t*)(pMemMap_v +((uint32_t)((H2F_AXI_BASE+FIFO_CSR_I_STATUS) & MAP_MASK)));
        if((*pMemMap & 0x1) > 0){
   	    	//Check for FIFO full first
   	    	printf("FIFO FULL....\n");
   	    	//regObject.setStatus(false);
   	    	myfile.close();
   	    	//break;
   	       	clearCmd(regObject, (uint32_t)(H2F_LW_AXI_BASE+OCR_DT_TRANS_CFG_OFFSET),(uint32_t)(READY_DT_TRANS_CFG_MASK),pMemMap_v);
   	    	return -1;
   	    } else if((*pMemMap & 0x2)==0){
   	       	// Fetch the data
           	data[cntFileSize]=*((uint32_t*)(pMemMap_v +((uint32_t)((H2F_AXI_BASE+FIFO_DATA_OFFSET) & MAP_MASK))));
   	       	cntFileSize++;
   	    }
    }

    printf("Data transfer completed \n\n");

    //Move the data[] array to the file
    for(int i=0;i<fileSize;i++){
    	pMemMap=&data[i];
    	if (i<fileSize-1) {
    		myfile.write((char *) pMemMap,4);
        } else {
        	myfile.write((char *) pMemMap,fileSize_mod4);
        }

    }
    delete[] data;

    myfile.close();
    printf("File closed\n");

    if(!myfile.good()) {
       	printf("Error occurred at writing time!\n");
       	clearCmd(regObject, (uint32_t)(H2F_LW_AXI_BASE+OCR_DT_TRANS_CFG_OFFSET),(uint32_t)(READY_DT_TRANS_CFG_MASK),pMemMap_v);
    	return -1;
    }

    // Set data received
    regObject.setCmd("wr");
    regObject.setAddr((uint32_t)(H2F_LW_AXI_BASE+OCR_DT_TRANS_ST_OFFSET));
    regObject.setValue((uint32_t)(0x2));
    regObject.setMask((uint32_t)(0x2));
    regObject.ExeRdWr(false, pMemMap_v);

/*
    //NOTE!!: Move the copy of the rbf file in the FPGA_ReconfWrap so that I don't copy corrupted files in the /mnt/fat partition!!
    if (decodeCfg.getFileExt()==".rbf") {
    	//NOTE;!!! In order to reduce the data transfer time when the source is USI, it could be a good idea to send the file compressed.
    	//         In that case, all the extension checks have to be performed after the file extraction.

    	//NOTE:!!! Maybe here a file size check before copying it to the fat partition

        // Copy the file into the /mnt/fat partition (overwrite)
        (void)  ((int) system((const char*)  "cp -rf /mnt/data/socfpgaA.rbf /mnt/fat/socfpga.rbf"));

        //NOTE:!!! Maybe here a check if the copy was succesfully executed

        // Set file saved
        regObject.setValue((uint32_t)(0x4));
        regObject.setMask((uint32_t)(0x4));
        regObject.ExeRdWr(false, pMemMap_v);
    }
*/

    // Clear cmd
   	clearCmd(regObject, (uint32_t)(H2F_LW_AXI_BASE+OCR_DT_TRANS_CFG_OFFSET),(uint32_t)(READY_DT_TRANS_CFG_MASK),pMemMap_v);

    //Reset the FIFO
    printf("Reset FIFO\n\n");
    regObject.setCmd("wr");
    regObject.setAddr((uint32_t)(H2F_LW_AXI_BASE+PIO_OFFSET));
    regObject.setValue((uint32_t)(0x0));		//Set
    regObject.setMask((uint32_t)(PIO_FIFO_RESET_MASK));
    regObject.ExeRdWr(false, pMemMap_v);

   	std::cout << "deinit\n";
   	// deinit
   	munmap((void*) pMemMap_v, MAP_SIZE);

	return 0;
}
