//============================================================================
// Name        : FPGA_Reconf.cpp
// Author      : drodomon
// Version     :
// Copyright   : Your copyright notice
// Description : It reconfigures the FPGA with an application or a factory
//               image. The Robin Sebastian FPGA-writeConfig file was used as
//               template:
//      https://github.com/robseb/rstools/blob/main/FPGA-writeConfig/main.cpp
//============================================================================

#define VERSION "1.00"

extern "C"
{
volatile void* __hps_virtualAdreess_FPGAMGR;
volatile void* __hps_virtualAdreess_FPGAMFRDATA;
volatile int __fd;
}

#include <cstdio>
#include "alt_fpga_manager.h"
#include "hps.h"
#include <string.h>
#include <fstream>
#include <iostream>
#include <thread>					// Required for putting task to sleep
#include <chrono>					// Required for putting task to sleep



#define H2F_LW_AXI_BASE				0xFF200000

#define OCR_FPGA_RECONF_CMD_OFFSET	0x8
#define OCR_FPGA_RECONF_ST_OFFSET	0xC



//using namespace std;


// Function to check if the rbf file (application or factory) is available at the user/sw specified path
bool is_file_exist(const char* fileName)
{
	std::ifstream infile(fileName);
	return infile.good();
}

// NOTE: in the fuction below it is shown how to call a binary file already installed in the file system inside another program
//       (FPGA_Reconf.cpp calls bregacc binary file to write the Reset Manager Module  addresses and perform the different reset actions).

/*
*   @brief               	Perform HPS to FPGA Reset
*	@param	ConsloeOutput	Print Status Output to Console
* 	@param	reset_typ		1 = FPGA Warm Reset
*							2 = FPGA Cold Reset
*							3 = LW HPS-to-FPGA Bridge Reset
*							4 = HPS-to-FPGA Bridge Reset
*							5 = FPGA-to-HPS Bridge Reset
*   @return                 success
*/

bool performHPStoFPGAReset(bool ConsloeOutput, uint8_t reset_typ)
{
	// Print the Inteted Reset Operation
	if (ConsloeOutput)
	{
		switch(reset_typ)
		{
			case 1: printf("#    Performing HPS-to-FPGA Warm Reset  (h2f_rst_n = 1,0)\n"); break;
			case 2: printf("#    Performing HPS-to-FPGA Cold Reset  (h2f_cold_rst_n = 1,0)\n"); break;

			case 3: printf("#    Performing a reset on the LightWeight HPS-to-FPGA Bridge\n"); break;
			case 4: printf("#    Performing a reset on the HPS-to-FPGA Bridge\n"); break;
			case 5: printf("#    Performing a reset on the FPGA-to-HPS Bridge\n"); break;
			default:
				printf("[ERROR]  Unkown Reset Type to perform!\n"); break;
				return false;
		}
	}

	// RESET =1

	// Perform Cold or Warm FPGA Reset
	if(reset_typ ==1) 		(void)  ((int) system( (const char*) "bregacc wr 0xFFD05020 0x00000060 0x00000060"));
	else if(reset_typ ==2)  (void)  ((int) system( (const char*) "bregacc wr 0xFFD05020 0x00000070 0x00000070"));
	// Perform a Bridge Reset
	else if(reset_typ==3)	(void)  ((int) system((const char*)  "bregacc wr 0xFFD0501C 0x00000002 0x00000002"));
	else if(reset_typ==4)	(void)  ((int) system((const char*)  "bregacc wr 0xFFD0501C 0x00000001 0x00000001"));
	else if(reset_typ==5)	(void)  ((int) system((const char*)  "bregacc wr 0xFFD0501C 0x00000004 0x00000004"));

	// Wait 50ms
	// C++11: Put this task to sleep
	std::this_thread::sleep_until(std::chrono::system_clock::now() + \
		std::chrono::milliseconds(50));

	// RESET =0

	// Perform Cold or Warm FPGA Reset
	if(reset_typ ==1) 		(void)  ((int) system( (const char*) "bregacc wr 0xFFD05020 0x00000000 0x00000060"));
	else if(reset_typ ==2)  (void)  ((int) system( (const char*) "bregacc wr 0xFFD05020 0x00000000 0x00000070"));
	// Perform a Bridge Reset
	else if(reset_typ==3)	(void)  ((int) system((const char*)  "bregacc wr 0xFFD0501C 0x00000000 0x00000002"));
	else if(reset_typ==4)	(void)  ((int) system((const char*)  "bregacc wr 0xFFD0501C 0x00000000 0x00000001"));
	else if(reset_typ==5)	(void)  ((int) system((const char*)  "bregacc wr 0xFFD0501C 0x00000000 0x00000004"));



	if(ConsloeOutput)
		printf( "[SUCCESS] Reset performed\n");
	else
		printf( "1");

	return true;
}

// This is the function that executes the altera API related to the FPGA configuration (FPGA Manager Module)
bool writeFPGAconfig(const char* configFileAdress, bool withOutput)
{
	/////////ceck vailed FPGA status  /////////

	/// check if the input file exist
	if (!is_file_exist(configFileAdress))
	{
		if (withOutput)
			printf( "[ ERROR ] The selected config file does not exsist!\n");
		return false;
	}

	/// Load the FPGA configuration file
	if (withOutput)
		printf( "[ INFO ] Start writing the new FPGA configuration\n");

	// Open rbf config and load them to an binary buffer into the Memory
	FILE* f = fopen(configFileAdress, "rb");
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);

	char* buf = new char[fsize + 1];
	fread(buf, 1, fsize, f);
	fclose(f);

	// Start to write the FPGA Configuration
	ALT_STATUS_CODE status = alt_fpga_configure(buf, fsize);

	if (status != ALT_E_SUCCESS)
	{
		if (withOutput)
			printf( "[ ERROR ] Writing the FPGA configuration failed\n");
		return false;
	}
	else
	{
		if (withOutput)
			printf( "[ SUCCESS ] The FPGA runs now with the new configuration\n");

		// Reset all Bridges
		if (withOutput)
			printf( "[ INFO] Performing a reset on all Bridge Interfaces\n");

		for (uint8_t i=3; i<6;i++)
		{
			performHPStoFPGAReset(withOutput,i);
		}

		// Perform COLD FPGA Reset
		performHPStoFPGAReset(withOutput,2);
		return true;
	}

	return false;
}



int main(int argc, const char* argv[])
{
//	performHPStoFPGAReset(true,2);


	///////// init the Virtual Memory for I/O access /////////
	// Here the FPGA manager control and the FPGA manager data address spaces are mapped to the virtual memory
	__VIRTUALMEM_SPACE_INIT();

	/////////	 init the FPGA Manager	 /////////
	alt_fpga_init();

	///////// Take the right to controll the FPGA /////////
	alt_fpga_control_enable();

	// This action I didn't understand because the stat is not afterwards neither eveluated
	ALT_FPGA_STATE_t stat = alt_fpga_state_get();

	// The if loop below calls the writeFPGAconfig function above passing a static file in case the factory image has to be restored or the file specified by the user
	// (the application image).

	// change to a new selected FPGA configuration
	if ((argc > 2) && (std::string(argv[1]) == "-f"))
	{
		bool withOutput = !((argc > 3) && (std::string(argv[3]) == "-b"));
		bool res = writeFPGAconfig(argv[2], withOutput);

		if (!withOutput) std::cout << res ? 1 : 0;
	}
	// restore the the boot up configuration
	else if ((argc > 1) && (std::string(argv[1]) == "-r"))
	{
		bool withOutput = !((argc > 2) && (std::string(argv[2]) == "-b"));
		bool res = writeFPGAconfig("/mnt/fat/socfpgaF.rbf", withOutput);
		if (!withOutput) std::cout << res ? 1 : 0;
	}
	else
	{
		printf( "	Command to change the FPGA fabric configuration\n");
		printf( "	FPGA-writeConfig -f [config rbf file path] {-b [optional]}\n");
		printf( "		change the FPGA config with a selected .rbf file\n");
		printf( "	FPGA-writeConfig -r {-b [optional]}\n");
		printf( "		restore to the boot up FPGA configuration\n");
		printf( "		this conf File is located: /mnt/data/socfpgaF.rbf\n");
		printf( "		suffix: -b -> only decimal result output\n");
		printf( "						Error:  0\n");
		printf( "						Succses:1\n");
		printf("Vers.%d: \n",atoi(VERSION));
		printf("Copyright (C) 2020-2022 rsyocto GmbH & Co. KG\n");

	}


	// Give the right to controll the FPGA
	alt_fpga_control_disable();

	// free the dynamic access memory
	__VIRTUALMEM_SPACE_DEINIT();


	return 0;
}
