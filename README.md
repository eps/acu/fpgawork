# FPGAWORK tree
1. [lib](https://git.gsi.de/eps/acu/fpgawork/-/tree/main/lib): 
   * This subfolder contains all the single vhdl modules that can be considered as instances in the project.
      * Chip drivers(SPI, I2C, ..)
      * Data tranceivers (USI, ACU_SerialCom, ..)
2. [prj](https://git.gsi.de/eps/acu/fpgawork/-/tree/main/prj):
   * This subfolder contains the quartus projects. A quartus project can be considered as a collection of instances connected each other in order to accomplish all the features requested by a specific FPGA/SoC FW.
     * ACU_MCVEVP contains the development FW loaded on the [Aries MCVEVP – Intel PSG Cyclone V Evaluation Platform](https://www.aries-embedded.com/evaluation-kit/fpga/cyclone-v-intel-fpga-mcvevp-hsmc-pmod).
     * ACU_KdriverTest is a playground for Linux kernel driver development. Also this project is relative to the  [Aries MCVEVP – Intel PSG Cyclone V Evaluation Platform](https://www.aries-embedded.com/evaluation-kit/fpga/cyclone-v-intel-fpga-mcvevp-hsmc-pmod).